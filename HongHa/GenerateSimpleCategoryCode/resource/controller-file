package com.viettel.backend.restful;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.viettel.backend.domain.@domain-name@;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.@domain-name@Info;
import com.viettel.backend.dto.SimpleCategoryFileCreateInfo;
import com.viettel.backend.restful.core.Envelope;
import com.viettel.backend.restful.core.Meta;
import com.viettel.backend.restful.core.ResourceNotFoundException;
import com.viettel.backend.service.@domain-name@Service;

@Controller
@RequestMapping("/api")
public class @domain-name@Controller {

    @Autowired
    private @domain-name@Service service;

    // UPDATE
    @RequestMapping(value = "/@domain-name-lower-only-letter@/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody @Valid SimpleCategoryFileCreateInfo dto) {
        @domain-name@Info info = service.update(id, dto, @domain-name@.class);
        return new Envelope(info).toResponseEntity(HttpStatus.OK);
    }

    // NEW
    @RequestMapping(value = "/@domain-name-lower-only-letter@", method = RequestMethod.PUT)
    public ResponseEntity<?> create(@RequestBody @Valid SimpleCategoryFileCreateInfo dto) {
        @domain-name@Info info = service.create(dto, @domain-name@.class);
        return new Envelope(info).toResponseEntity(HttpStatus.OK);
    }

    // DETAIL
    @RequestMapping(value = "/@domain-name-lower-only-letter@/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> detail(@PathVariable String id) {
        @domain-name@Info info = service.findOne(id);

        if (info == null) {
            throw new ResourceNotFoundException("@domain-name@ not found");
        }

        return new Envelope(info).toResponseEntity(HttpStatus.OK);
    }

    // DISACTIVE
    @RequestMapping(value = "/@domain-name-lower-only-letter@/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> disactive(@PathVariable String id) {
        service.delete(id);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    // LIST
    @RequestMapping(value = "/@domain-name-lower-only-letter@", method = RequestMethod.GET)
    public ResponseEntity<?> list(@RequestParam(value = "q", required = false, defaultValue = "") String searchText,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        if (page == null || page < 0) {
            page = 1;
        }

        if (size == null || size < 1) {
            size = 10;
        }

        PageRequest pageRequest = new PageRequest(page - 1, size, null);
        long count = service.countByNameLikeAndActiveIsTrue(searchText);
        ListJson<@domain-name@Info> cats = new ListJson<@domain-name@Info>(service.findByNameLikeAndActiveIsTrue(searchText,
                pageRequest), count);
        return new Envelope(cats).toResponseEntity(HttpStatus.OK);
    }

}
