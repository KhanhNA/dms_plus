/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.File;
import log.Logger;
import util.AddTemplateToFile;
import util.CheckPathExist;
import util.ConvertDomainName;
import util.CreateFileFromTemplate;
import util.ReplaceEngine;

/**
 *
 * @author trungkh
 */
public class Main {
    
    public static void main(String[] args) {
        
        boolean useFile = true;
        boolean useDetail = false;
        
        String domain_name = "CompanyCategory";
        
        String destinationPath = "/Users/trungkh/Documents/vinacomin";
        String backendPath = "/backend/src/main/java";
        String viewPath = "/webview";

        generate(domain_name, destinationPath, backendPath, viewPath, useFile, useDetail, false, false);
        
    }

    public static String generateV2(String domain_name, String destinationPath, String backendPath, String viewPath, boolean useDetail, boolean serverOnly, boolean viewOnly) {
        String domainPackagePath = "/com/viettel/backend/domain";
        String controllerPackagePath = "/com/viettel/backend/restful";
        
        ReplaceEngine replaceEngine = new ReplaceEngine();
        replaceEngine.put("@domain-name@", domain_name);
        replaceEngine.put("@domain-name-lower@", ConvertDomainName.convertToLower(domain_name));
        replaceEngine.put("@domain-name-lower-only-letter@", ConvertDomainName.convertToLowerOnlyLetter(domain_name));
        replaceEngine.put("@domain-name-only-letter@", ConvertDomainName.convertToOnlyLetter(domain_name));
        
        if (CheckPathExist.check(destinationPath + backendPath) && CheckPathExist.check(destinationPath + viewPath))
        {
            if (!viewOnly) {
                //DOMAIN
                if (!CheckPathExist.check(destinationPath + backendPath + domainPackagePath)) {
                    (new File(destinationPath + backendPath + domainPackagePath)).mkdirs();
                }
                CreateFileFromTemplate.create("domain", destinationPath + backendPath + domainPackagePath + "/" + domain_name + ".java", replaceEngine);

                //CONTROLLER
                if (!CheckPathExist.check(destinationPath + backendPath + controllerPackagePath)) {
                    (new File(destinationPath + backendPath + controllerPackagePath)).mkdirs();
                }
                CreateFileFromTemplate.create("controller-new", destinationPath + backendPath + controllerPackagePath + "/" + domain_name + "Controller.java", replaceEngine);
            }
            //VIEW
            if (!serverOnly) {
                //CTRL
                if (!CheckPathExist.check(destinationPath + viewPath + "/mvc/controller")) {
                    (new File(destinationPath + viewPath + "/mvc/controller")).mkdirs();
                }
                if (useDetail) {
                    CreateFileFromTemplate.create("ctrl-detail", destinationPath + viewPath + "/mvc/controller" + "/" + ConvertDomainName.convertToLower(domain_name) + "-ctrl.js", replaceEngine);
                    //DETAIL CTRL
                    CreateFileFromTemplate.create("detail-ctrl", destinationPath + viewPath + "/mvc/controller" + "/" + ConvertDomainName.convertToLower(domain_name) + "-detail-ctrl.js", replaceEngine);

                } else {
                    CreateFileFromTemplate.create("ctrl", destinationPath + viewPath + "/mvc/controller" + "/" + ConvertDomainName.convertToLower(domain_name) + "-ctrl.js", replaceEngine);
                    //POPUP CTRL
                    CreateFileFromTemplate.create("popup-ctrl", destinationPath + viewPath + "/mvc/controller" + "/" + ConvertDomainName.convertToLower(domain_name) + "-detail-popup-ctrl.js", replaceEngine);        
                }

                //HTML
                if (!CheckPathExist.check(destinationPath + viewPath + "/mvc/view")) {
                    (new File(destinationPath + viewPath + "/mvc/view")).mkdirs();
                }
                if (useDetail) {
                    CreateFileFromTemplate.create("html-detail", destinationPath + viewPath + "/mvc/view" + "/" + ConvertDomainName.convertToLower(domain_name) + ".html", replaceEngine);
                    //POPUP HTML
                    CreateFileFromTemplate.create("detail-html", destinationPath + viewPath + "/mvc/view" + "/" + ConvertDomainName.convertToLower(domain_name) + "-detail.html", replaceEngine);
                } else {
                    CreateFileFromTemplate.create("html", destinationPath + viewPath + "/mvc/view" + "/" + ConvertDomainName.convertToLower(domain_name) + ".html", replaceEngine);
                    //POPUP HTML
                    CreateFileFromTemplate.create("popup-html", destinationPath + viewPath + "/mvc/view" + "/" + ConvertDomainName.convertToLower(domain_name) + "-detail-popup.html", replaceEngine);
                }

                //FACTORY
                if (!CheckPathExist.check(destinationPath + viewPath + "/mvc/factory")) {
                    (new File(destinationPath + viewPath + "/mvc/factory")).mkdirs();
                }
                CreateFileFromTemplate.create("factory", destinationPath + viewPath + "/mvc/factory" + "/" + ConvertDomainName.convertToLower(domain_name) + "-factory.js", replaceEngine);

                //INSERT TO INDEX
                if (useDetail) {
                    AddTemplateToFile.add("import-ctrl-detail", destinationPath + viewPath + "/index.html", replaceEngine, "<!-- end controller -->", "<!-- " + domain_name + " ctrl -->");
                } else {
                    AddTemplateToFile.add("import-ctrl", destinationPath + viewPath + "/index.html", replaceEngine, "<!-- end controller -->", "<!-- " + domain_name + " ctrl -->");
                }
                AddTemplateToFile.add("import-factory", destinationPath + viewPath + "/index.html", replaceEngine, "<!-- end factory -->", "<!-- " + domain_name + " factory -->");

                //ADD STATE
                if (useDetail) {
                    AddTemplateToFile.add("state-detail", destinationPath + viewPath + "/mvc/common.js", replaceEngine, "END OF STATE", "// " + domain_name + " state");
                } else {
                    AddTemplateToFile.add("state", destinationPath + viewPath + "/mvc/common.js", replaceEngine, "END OF STATE", "// " + domain_name + " state");
                }
            }
            return null;
        } else {
            Logger.log("destination path is not exist");
            return "destination path is not exist";
        }
    }
    
    public static void generate(String domain_name, String destinationPath, String backendPath, String viewPath, boolean useFile, boolean useDetail, boolean serverOnly, boolean viewOnly) {
        String domainPackagePath = "/com/viettel/backend/domain";
        String dtoPackagePath = "/com/viettel/backend/dto";
        String repositoryPackagePath = "/com/viettel/backend/repository";
        String servicePackagePath = "/com/viettel/backend/service";
        String serviceImplPackagePath = "/com/viettel/backend/service/impl";
        String controllerPackagePath = "/com/viettel/backend/restful";
        
        ReplaceEngine replaceEngine = new ReplaceEngine();
        replaceEngine.put("@domain-name@", domain_name);
        replaceEngine.put("@domain-name-lower@", ConvertDomainName.convertToLower(domain_name));
        replaceEngine.put("@domain-name-lower-only-letter@", ConvertDomainName.convertToLowerOnlyLetter(domain_name));
        replaceEngine.put("@domain-name-only-letter@", ConvertDomainName.convertToOnlyLetter(domain_name));
        
        if (CheckPathExist.check(destinationPath + backendPath) && CheckPathExist.check(destinationPath + viewPath))
        {
            if (!viewOnly) {
                //DOMAIN
                if (!CheckPathExist.check(destinationPath + backendPath + domainPackagePath)) {
                    (new File(destinationPath + backendPath + domainPackagePath)).mkdirs();
                }
                if (useFile) {
                    CreateFileFromTemplate.create("domain-file", destinationPath + backendPath + domainPackagePath + "/" + domain_name + ".java", replaceEngine);
                } else {
                    CreateFileFromTemplate.create("domain", destinationPath + backendPath + domainPackagePath + "/" + domain_name + ".java", replaceEngine);
                }

                //DTO
                if (!CheckPathExist.check(destinationPath + backendPath + dtoPackagePath)) {
                    (new File(destinationPath + backendPath + dtoPackagePath)).mkdirs();
                }
                if (useFile) {
                    CreateFileFromTemplate.create("dto-file", destinationPath + backendPath + dtoPackagePath + "/" + domain_name + "Info.java", replaceEngine);
                } else {
                    CreateFileFromTemplate.create("dto", destinationPath + backendPath + dtoPackagePath + "/" + domain_name + "Info.java", replaceEngine);
                }

                //REPOSITORY
                if (!CheckPathExist.check(destinationPath + backendPath + repositoryPackagePath)) {
                    (new File(destinationPath + backendPath + repositoryPackagePath)).mkdirs();
                }
                CreateFileFromTemplate.create("repository", destinationPath + backendPath + repositoryPackagePath + "/" + domain_name + "Repository.java", replaceEngine);

                //SERVICE
                if (!CheckPathExist.check(destinationPath + backendPath + servicePackagePath)) {
                    (new File(destinationPath + backendPath + servicePackagePath)).mkdirs();
                }
                CreateFileFromTemplate.create("service", destinationPath + backendPath + servicePackagePath + "/" + domain_name + "Service.java", replaceEngine);

                //SERVICE IMPL
                if (!CheckPathExist.check(destinationPath + backendPath + serviceImplPackagePath)) {
                    (new File(destinationPath + backendPath + serviceImplPackagePath)).mkdirs();
                }
                if (useFile) {
                    CreateFileFromTemplate.create("service-impl-file", destinationPath + backendPath + serviceImplPackagePath + "/" + domain_name + "ServiceImpl.java", replaceEngine);
                } else {
                    CreateFileFromTemplate.create("service-impl", destinationPath + backendPath + serviceImplPackagePath + "/" + domain_name + "ServiceImpl.java", replaceEngine);
                }

                //CONTROLLER
                if (!CheckPathExist.check(destinationPath + backendPath + controllerPackagePath)) {
                    (new File(destinationPath + backendPath + controllerPackagePath)).mkdirs();
                }
                if (useFile) {
                    CreateFileFromTemplate.create("controller-file", destinationPath + backendPath + controllerPackagePath + "/" + domain_name + "Controller.java", replaceEngine);
                } else {
                    CreateFileFromTemplate.create("controller", destinationPath + backendPath + controllerPackagePath + "/" + domain_name + "Controller.java", replaceEngine);
                }
            }
            //VIEW
            if (!serverOnly) {
                //CTRL
                if (!CheckPathExist.check(destinationPath + viewPath + "/mvc/controller")) {
                    (new File(destinationPath + viewPath + "/mvc/controller")).mkdirs();
                }
                if (useDetail) {
                    CreateFileFromTemplate.create("ctrl-detail", destinationPath + viewPath + "/mvc/controller" + "/" + ConvertDomainName.convertToLower(domain_name) + "-ctrl.js", replaceEngine);
                    //DETAIL CTRL
                    CreateFileFromTemplate.create("detail-ctrl", destinationPath + viewPath + "/mvc/controller" + "/" + ConvertDomainName.convertToLower(domain_name) + "-detail-ctrl.js", replaceEngine);

                } else {
                    CreateFileFromTemplate.create("ctrl", destinationPath + viewPath + "/mvc/controller" + "/" + ConvertDomainName.convertToLower(domain_name) + "-ctrl.js", replaceEngine);
                    //POPUP CTRL
                    CreateFileFromTemplate.create("popup-ctrl", destinationPath + viewPath + "/mvc/controller" + "/" + ConvertDomainName.convertToLower(domain_name) + "-detail-popup-ctrl.js", replaceEngine);        
                }

                //HTML
                if (!CheckPathExist.check(destinationPath + viewPath + "/mvc/view")) {
                    (new File(destinationPath + viewPath + "/mvc/view")).mkdirs();
                }
                if (useDetail) {
                    CreateFileFromTemplate.create("html-detail", destinationPath + viewPath + "/mvc/view" + "/" + ConvertDomainName.convertToLower(domain_name) + ".html", replaceEngine);
                    //POPUP HTML
                    CreateFileFromTemplate.create("detail-html", destinationPath + viewPath + "/mvc/view" + "/" + ConvertDomainName.convertToLower(domain_name) + "-detail.html", replaceEngine);
                } else {
                    CreateFileFromTemplate.create("html", destinationPath + viewPath + "/mvc/view" + "/" + ConvertDomainName.convertToLower(domain_name) + ".html", replaceEngine);
                    //POPUP HTML
                    CreateFileFromTemplate.create("popup-html", destinationPath + viewPath + "/mvc/view" + "/" + ConvertDomainName.convertToLower(domain_name) + "-detail-popup.html", replaceEngine);
                }

                //FACTORY
                if (!CheckPathExist.check(destinationPath + viewPath + "/mvc/factory")) {
                    (new File(destinationPath + viewPath + "/mvc/factory")).mkdirs();
                }
                CreateFileFromTemplate.create("factory", destinationPath + viewPath + "/mvc/factory" + "/" + ConvertDomainName.convertToLower(domain_name) + "-factory.js", replaceEngine);

                //INSERT TO INDEX
                if (useDetail) {
                    AddTemplateToFile.add("import-ctrl-detail", destinationPath + viewPath + "/index.html", replaceEngine, "<!-- end controller -->", "<!-- " + domain_name + " ctrl -->");
                } else {
                    AddTemplateToFile.add("import-ctrl", destinationPath + viewPath + "/index.html", replaceEngine, "<!-- end controller -->", "<!-- " + domain_name + " ctrl -->");
                }
                AddTemplateToFile.add("import-factory", destinationPath + viewPath + "/index.html", replaceEngine, "<!-- end factory -->", "<!-- " + domain_name + " factory -->");

                //ADD STATE
                if (useDetail) {
                    AddTemplateToFile.add("state-detail", destinationPath + viewPath + "/mvc/common.js", replaceEngine, "END OF STATE", "// " + domain_name + " state");
                } else {
                    AddTemplateToFile.add("state", destinationPath + viewPath + "/mvc/common.js", replaceEngine, "END OF STATE", "// " + domain_name + " state");
                }
            }
        } else {
            Logger.log("destination path is not exist");
        }
    }
    
}
