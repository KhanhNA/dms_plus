/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import log.Logger;

/**
 *
 * @author trungkh
 */
public class AddTemplateToFile {

    private static final String TEMPLATE_PATH = "resource/";

    public static final boolean add(String templateName, String targetPath, ReplaceEngine replaceEngine, String placeToAdd, String breakRegex) {
        if (CheckPathExist.check(TEMPLATE_PATH + templateName) && CheckPathExist.check(targetPath)) {

            try (BufferedReader br = new BufferedReader(new FileReader(TEMPLATE_PATH + templateName))) {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(replaceEngine.process(line));
                    sb.append(System.lineSeparator());
                    line = br.readLine();
                }
                String insertCode = sb.toString();

                boolean stop = false;
                try (BufferedReader br2 = new BufferedReader(new FileReader(targetPath))) {
                    StringBuilder sb2 = new StringBuilder();
                    String line2 = br2.readLine();

                    while (line2 != null) {
                        if (breakRegex != null && line2.contains(breakRegex)) {
                            stop = true;
                            break;
                        }
                        
                        if (line2.contains(placeToAdd)) {
                            sb2.append(insertCode);
                        }
                        
                        sb2.append(line2);
                        sb2.append(System.lineSeparator());
                        line2 = br2.readLine();
                    }
                    if (!stop) {
                        String everything = sb2.toString();

                        try (PrintWriter writer = new PrintWriter(targetPath, "UTF-8")) {
                            writer.print(everything);
                        }
                    }
                }
                
                return true;
            } catch (FileNotFoundException ex) {
                Logger.log("Read File Error", ex);
            } catch (IOException ex) {
                Logger.log("Read File Error", ex);
            }

        }

        return false;
    }

}
