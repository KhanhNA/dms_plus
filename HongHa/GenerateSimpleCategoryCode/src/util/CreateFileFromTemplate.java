/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import log.Logger;

/**
 *
 * @author trungkh
 */
public class CreateFileFromTemplate {
    
    private static final String TEMPLATE_PATH = "resource/";
    
    public static final boolean create(String templateName, String targetPath, ReplaceEngine replaceEngine) {
        try(BufferedReader br = new BufferedReader(new FileReader(TEMPLATE_PATH + templateName))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(replaceEngine.process(line));
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            String everything = sb.toString();
            
            try (PrintWriter writer = new PrintWriter(targetPath, "UTF-8")) {
                writer.print(everything);
            }
            
            return true;
        } catch (FileNotFoundException ex) {
            Logger.log("Read File Error", ex);
        } catch (IOException ex) {
            Logger.log("Read File Error", ex);
        }
        
        return false;
    }
    
}
