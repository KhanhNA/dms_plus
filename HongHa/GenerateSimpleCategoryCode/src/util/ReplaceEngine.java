/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author trungkh
 */
public class ReplaceEngine {

    private HashMap<String, String> map;

    public ReplaceEngine() {
        this.map = new HashMap<>();
    }

    public void put(String variable, String value) {
        this.map.put(variable, value);
    }

    public String process(Object o) {
        String text = o.toString();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            text = text.replace(entry.getKey(), entry.getValue());
        }

        return text;
    }

}
