conn = new Mongo();
db = conn.getDB("dmsoishi");

db.getCollectionNames().forEach(function(collection) {
   indexes = db[collection].getIndexes();
   if (collection == 'Area'
       || collection == 'CalendarConfig'
       || collection == 'Config'
       || collection == 'Customer'
       || collection == 'CustomerType'
       || collection == 'DeliveryMan'
       || collection == 'Distributor'
       || collection == 'Product'
       || collection == 'ProductCategory'
       || collection == 'Promotion'
       || collection == 'Route'
       || collection == 'Survey'
       || collection == 'Target'
       || collection == 'UOM'
       || collection == 'User'
       || collection == 'VisitAndOrder') {
       		print("Add clientId index to " + collection);
       		db[collection].createIndex( { "clientId" : 1 } );
   }

   if (collection == 'Area'
       || collection == 'Customer'
       || collection == 'DeliveryMan'
       || collection == 'Route'
       || collection == 'VisitAndOrder') {
       		print("Add distributor._id index to " + collection);
       		db[collection].createIndex( { "distributor._id" : 1 } );
   }

   if (collection == 'VisitAndOrder') {
       		print("Add startTime.value index to " + collection);
       		db[collection].createIndex( { "startTime.value" : -1 } );
   }

   if (collection == 'VisitAndOrder') {
          print("Add approveTime.value index to " + collection);
          db[collection].createIndex( { "approveTime.value" : -1 } );
   }

});
