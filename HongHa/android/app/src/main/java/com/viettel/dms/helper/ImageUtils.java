package com.viettel.dms.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by PHAMHUNG on 3/22/2016.
 */
public class ImageUtils {

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        int inSampleSize = 1;
        int height = options.outHeight;
        int width = options.outWidth;
        if ((reqWidth >= reqHeight) != (options.outWidth >= options.outHeight)) {
            height = options.outWidth;
            width = options.outHeight;
        }

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, false);
    }

    @SuppressLint("SimpleDateFormat")
    public static File createImageFile() throws IOException {
        File image = null;
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        try {
            if (!storageDir.exists()) {
                storageDir.mkdir();
            }
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException ex) {
            Log.e("FileUtils", ex.getMessage(), ex);
        } catch (Exception e){
            Log.e("FileUtils", e.getMessage(), e);
        }
        return image;
    }

    public static Bitmap optimizeAndCompressImage(String photoPath, int realWidth, int realHeight) {
        try {
            // Reduce bytes per pixel
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            bmOptions.inPreferredConfig = Bitmap.Config.RGB_565;
            BitmapFactory.decodeFile(photoPath, bmOptions);

            // Resize image
            bmOptions.inSampleSize = calculateInSampleSize(bmOptions, realWidth, realHeight);
            bmOptions.inJustDecodeBounds = false;
            Bitmap scaleBm = BitmapFactory.decodeFile(photoPath, bmOptions);

            // Rotate if need
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    scaleBm = rotateImage(scaleBm, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    scaleBm = rotateImage(scaleBm, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    scaleBm = rotateImage(scaleBm, 270);
                    break;
            }
            // Compress
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            scaleBm.compress(Bitmap.CompressFormat.JPEG, HardCodeUtil.IMAGE_QUALITY, byteArrayOutputStream);
            File f = new File(photoPath);
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(byteArrayOutputStream.toByteArray());
                fo.close();
            } catch (IOException e) {
                Log.e("IOException",e.getMessage(),e);
            }catch (Exception e){
                Log.e("Exception",e.getMessage(),e);
            }
            return scaleBm;
        } catch (Exception ex) {
            Log.e("Exception",ex.getMessage(),ex);
        }
        return null;
    }

    public static void makeResizedCopyImage(String originalPath, int realWidth, int realHeight, String destPath) {
        try {
            // Reduce bytes per pixel
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            bmOptions.inPreferredConfig = Bitmap.Config.RGB_565;
            BitmapFactory.decodeFile(originalPath, bmOptions);

            // Resize image
            bmOptions.inSampleSize = calculateInSampleSize(bmOptions, realWidth, realHeight);
            bmOptions.inJustDecodeBounds = false;
            Bitmap scaleBm = BitmapFactory.decodeFile(originalPath, bmOptions);

            // Rotate if need
            ExifInterface ei = new ExifInterface(originalPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    scaleBm = rotateImage(scaleBm, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    scaleBm = rotateImage(scaleBm, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    scaleBm = rotateImage(scaleBm, 270);
                    break;
            }
            // Compress
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            scaleBm.compress(Bitmap.CompressFormat.JPEG, HardCodeUtil.IMAGE_QUALITY, byteArrayOutputStream);
            File f = new File(destPath);
            try {
                f.exists();
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(byteArrayOutputStream.toByteArray());
                fo.close();
            } catch (IOException e) {
                Log.e("Exception",e.getMessage(),e);
            } finally {
                scaleBm.recycle();
            }

        } catch (Exception ex) {
            Log.e("Exception",ex.getMessage(),ex);
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (Exception ex) {
            Log.e("Exception",ex.getMessage(),ex);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public static void registerThisURI(Context context, Uri contentUri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    public static boolean isImageDamaged(String photoPath) {
        if (photoPath == null || !(new File(photoPath).exists())) return true;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        return bmOptions.outHeight == -1 || bmOptions.outWidth == -1;
    }
}
