package com.viettel.dms.helper.dialog;

/**
 * @author PHAMHUNG
 * @since 4/8/2015
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.viettel.dms.R;
import com.viettel.dms.helper.ImageUtils;

import java.io.File;

import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageDialog implements View.OnClickListener {

    protected Context context;
    private boolean isLocal = false;
    String path;
    private Dialog dialog;
    private PhotoViewAttacher mAttacher;
    private ImageView imgView;
    private ImageButton btnClose;


    @SuppressLint("InflateParams")
    public ImageDialog(Context context, String photoPath, boolean isLocal) {
        this.context = context;
        this.path = photoPath;
        this.isLocal = isLocal;
        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_image);
        this.dialog.setCancelable(false);
        this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        initViews();

    }

    private void initViews() {
        btnClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        imgView = (ImageView) dialog.findViewById(R.id.img_Photo);
        mAttacher = new PhotoViewAttacher(imgView);
        btnClose.setOnClickListener(this);
        imgView.post(new Runnable() {
            @Override
            public void run() {
                if (isLocal) {
                    if (ImageUtils.isImageDamaged(path)) {
                        Picasso.with(context).load(R.drawable.no_media).into(imgView);
                    } else {
                        Picasso.with(context).load(new File(path)).error(R.drawable.no_media).into(imgView, new Callback() {
                            @Override
                            public void onSuccess() {
                                mAttacher.update();
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                } else {
                    Picasso.with(context).load(path).error(R.drawable.no_media).into(imgView, new Callback() {
                        @Override
                        public void onSuccess() {
                            mAttacher.update();
                        }

                        @Override
                        public void onError() {

                        }
                    });
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == btnClose.getId()) {
            this.dismiss();
            this.imgView = null;
        }
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

}
