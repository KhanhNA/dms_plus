package com.viettel.dms.helper.drawer;

import com.viettel.dms.ui.fragment.NavigationDrawerFragment;
import com.viettel.dmsplus.sdk.models.UserInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHAMHUNG on 4/7/2016.
 */
public abstract class AbstractNavigationBuildler implements NavigationModuleBuilder {
    UserInfo userInfo;
    List<NavigationDrawerFragment.DrawerItem> items;

    public AbstractNavigationBuildler(UserInfo u) {
        userInfo = u;
        items = new ArrayList<>();
    }

    public List<NavigationDrawerFragment.DrawerItem> getItems() {
        return items;
    }

    public void build() {
        addHeader();
        // MAIN SECTION
        addModuleBase();
        if (userInfo.isUseExchangeReturn()) {
            addModuleExchangeReturn();
        }
        if (userInfo.isUseCheckIn()) {
            addModuleCheckIn();
        }
        // SUPPORT SECTION
        addSubHeaderSupport();
        addModuleSupport();
        if (userInfo.isUsePromotion()) {
            addModulePromotion();
        }
        // SYSTEM SECTION
        addSubHeaderSystem();
        addModuleSystem();
    }
}
