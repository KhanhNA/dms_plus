package com.viettel.dms.helper.drawer;

/**
 * Created by PHAMHUNG on 4/7/2016.
 */
public interface NavigationModuleBuilder {

    public void addHeader();

    public void addModuleBase();

    public void addModuleExchangeReturn();

    public void addModuleCheckIn();

    public void addModulePromotion();

    public void addSubHeaderSupport();

    public void addSubHeaderSystem();

    public void addModuleSystem();

    public void addModuleSupport();
}
