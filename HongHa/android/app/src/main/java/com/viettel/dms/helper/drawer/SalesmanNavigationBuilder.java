package com.viettel.dms.helper.drawer;

import com.viettel.dms.R;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.ui.fragment.NavigationDrawerFragment;
import com.viettel.dmsplus.sdk.models.UserInfo;

/**
 * Created by PHAMHUNG on 4/7/2016.
 */
public class SalesmanNavigationBuilder extends AbstractNavigationBuildler {
    public SalesmanNavigationBuilder(UserInfo u) {
        super(u);
        userInfo = u;
    }

    @Override
    public void addHeader() {
        items.add(new NavigationDrawerFragment.DrawerItem("", 0, HardCodeUtil.NavigationDrawer.DRAWER_HEADER, 0));
    }

    @Override
    public void addModuleBase() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-dashboard 24dp}", R.string.navigation_drawer_item_result_sale, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_RESULT_SALE));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-group 24dp}", R.string.navigation_drawer_item_visit, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_VISIT));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-assignment 24dp}", R.string.navigation_drawer_item_take_order, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_TAKE_ORDER));
    }

    @Override
    public void addModuleExchangeReturn() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-find-replace 24dp}", R.string.navigation_drawer_item_exchange, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_EXCHANGE));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-local-shipping 24dp}", R.string.navigation_drawer_item_return, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_RETURN));
    }

    @Override
    public void addModuleCheckIn() {

    }

    @Override
    public void addModulePromotion() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-local-offer 24dp }", R.string.navigation_drawer_item_promotion_program, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_PROMOTION_PROGRAM));

    }

    @Override
    public void addSubHeaderSupport() {
        items.add(new NavigationDrawerFragment.DrawerItem("", R.string.navigation_drawer_header_support_sale, HardCodeUtil.NavigationDrawer.DRAWER_SUBHEADER, 0));
    }

    @Override
    public void addSubHeaderSystem() {
        items.add(new NavigationDrawerFragment.DrawerItem("", R.string.navigation_drawer_header_system, HardCodeUtil.NavigationDrawer.DRAWER_SUBHEADER, 0));
    }

    @Override
    public void addModuleSystem() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-lock-outline 24dp}", R.string.navigation_drawer_item_change_pass, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_CHANGE_PASS));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-settings 24dp}", R.string.navigation_drawer_item_settings, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_THEME));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-exit-to-app 24dp}", R.string.navigation_drawer_item_exit, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_EXIT));
    }

    @Override
    public void addModuleSupport() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-widgets 24dp}", R.string.navigation_drawer_item_list_product, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_LIST_PRODUCT));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-person-add 24dp}", R.string.navigation_drawer_item_new_customer, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_REGISTER_CUSTOMER));
    }

}
