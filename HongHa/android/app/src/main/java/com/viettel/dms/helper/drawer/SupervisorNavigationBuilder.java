package com.viettel.dms.helper.drawer;

import com.viettel.dms.R;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.ui.fragment.NavigationDrawerFragment;
import com.viettel.dmsplus.sdk.models.UserInfo;

/**
 * Created by PHAMHUNG on 4/7/2016.
 */
public class SupervisorNavigationBuilder extends AbstractNavigationBuildler {

    public SupervisorNavigationBuilder(UserInfo u) {
        super(u);
        userInfo = u;
    }

    @Override
    public void addHeader() {
        items.add(new NavigationDrawerFragment.DrawerItem("", 0, HardCodeUtil.NavigationDrawer.DRAWER_HEADER, 0));
    }

    @Override
    public void addModuleCheckIn() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-pin-drop 24dp}", R.string.navigation_drawer_item_check_in, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_CHECK_IN));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-dns 24dp}", R.string.navigation_drawer_item_check_in_history, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_CHECK_IN_HISTORY));
    }
    @Override
    public void addSubHeaderSystem() {
        items.add(new NavigationDrawerFragment.DrawerItem("", R.string.navigation_drawer_header_system, HardCodeUtil.NavigationDrawer.DRAWER_SUBHEADER, 0));
    }

    @Override
    public void addModuleSystem() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-lock-outline 24dp}", R.string.navigation_drawer_item_change_pass, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_CHANGE_PASS));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-settings 24dp}", R.string.navigation_drawer_item_settings, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_THEME));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-exit-to-app 24dp}", R.string.navigation_drawer_item_exit, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_EXIT));
    }


    @Override
    public void addModuleBase() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-widgets 24dp}", R.string.navigation_drawer_item_general_statistics, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_GENERAL_STATISTICS));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-widgets 24dp}", R.string.navigation_drawer_item_general_statistics_month, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_GENERAL_STATISTICS_MONTH));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-widgets 24dp}", R.string.text_checkin_of_day_result, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_CHECKIN_OF_DAY_RESULT));

    }

    @Override
    public void addModuleExchangeReturn() {

    }

    @Override
    public void addModulePromotion() {

    }

    @Override
    public void addSubHeaderSupport() {

        //duongnv18 - add menu category for suppervisor
        items.add(new NavigationDrawerFragment.DrawerItem("", R.string.navigation_drawer_header_category, HardCodeUtil.NavigationDrawer.DRAWER_SUBHEADER, 0));

    }

    @Override
    public void addModuleSupport() {
        items.add(new NavigationDrawerFragment.DrawerItem("{md-widgets 24dp}", R.string.navigation_drawer_item_list_product, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_LIST_PRODUCT));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-person-add 24dp}", R.string.navigation_drawer_item_customer_list, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_REGISTER_CUSTOMER));
        items.add(new NavigationDrawerFragment.DrawerItem("{md-person-add 24dp}", R.string.navigation_drawer_item_promotion, HardCodeUtil.NavigationDrawer.DRAWER_ITEM, HardCodeUtil.NavigationDrawer.DRAWER_ITEM_PROMOTION_PROGRAM));

    }

}
