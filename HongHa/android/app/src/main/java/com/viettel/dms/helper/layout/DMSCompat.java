package com.viettel.dms.helper.layout;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.StyleRes;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @author thanh
 * @since 10/12/15
 */
public class DMSCompat {

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void setBackgroundDrawable(View view, Drawable drawable) {
        final int sdk = Build.VERSION.SDK_INT;
        if (sdk < Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(drawable);
        } else {
            view.setBackground(drawable);
        }
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.M)
    public static void setTextAppearance(Context context, EditText editText, @StyleRes int resId) {
        final int version = Build.VERSION.SDK_INT;
        if (editText == null) return;
        if (version >= 23) {
            editText.setTextAppearance(resId);
        } else if (version >= 11) {
            editText.setTextAppearance(context, resId);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public static void setTextAppearance(Context context, TextView textView, @StyleRes int resId) {
        final int version = Build.VERSION.SDK_INT;
        if (textView == null) return;
        if (version >= 23) {
            textView.setTextAppearance(resId);
        } else if (version >= 11) {
            textView.setTextAppearance(context, resId);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }
}
