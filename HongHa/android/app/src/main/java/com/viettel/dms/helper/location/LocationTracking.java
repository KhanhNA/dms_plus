package com.viettel.dms.helper.location;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by PHAMHUNG on 3/25/2016.
 */
public class LocationTracking implements LocationListener {
    private Context mContext;

    private float mMaximunDistance;
    private float mMaximumAccuracy;
    private long mTimeout;
    private Location lastKnowLocation;
    private String mLocationProvider;
    private LocationTrackingListener listener;

    private Thread timeOutThread;
    private Boolean finished;

    public LocationTracking(Context context, float maxAccurancy, LocationTrackingListener listener, long timeout) {
        this.mContext = context;
        this.listener = listener;
        this.mTimeout = timeout;
        this.mMaximumAccuracy = maxAccurancy;

        boolean notGpsOrNetwork = true;
        if (!LocationHelper.isHaveRequiredPermission(context)) {
            onMatchFail();
            return;
        }
        if (LocationHelper.isGpsProviderEnabled(mContext)) {
            notGpsOrNetwork = false;
            mLocationProvider = LocationManager.GPS_PROVIDER;
            LocationHelper.addLocationUpdatesListener(mContext, LocationManager.GPS_PROVIDER, this);
        }
        if (LocationHelper.isNetworkProviderEnabled(mContext)) {
            notGpsOrNetwork = false;
            mLocationProvider = LocationManager.NETWORK_PROVIDER;
            LocationHelper.addLocationUpdatesListener(mContext, LocationManager.NETWORK_PROVIDER, this);
        }
        if (notGpsOrNetwork) {
            onMatchFail();
            return;
        }
        this.finished = Boolean.FALSE;

        if (timeout > 0) {
            timeOutThread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(mTimeout);
                    } catch (InterruptedException e) {
                        ;
                    }
                    synchronized (this) {
                        if (finished) {
                            return;
                        }
                        finished = Boolean.TRUE;
                    }
                    stopListening();
                    if (isAccuracyEnough(lastKnowLocation)) {
                        onMatchSuccess(lastKnowLocation);
                    } else {
                        onMatchFail();
                    }
                }
            };
            timeOutThread.start();
        }
    }

    private void onMatchFail() {
        Handler mainHandler = new Handler(mContext.getMainLooper());
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                listener.onLocateFail();
            }
        });
    }

    private void onMatchSuccess(final Location location) {
        Handler mainHandler = new Handler(mContext.getMainLooper());
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                listener.onLocateSuccess(location);
            }
        });
    }

    public void stop() {
        synchronized (this) {
            if (finished) {
                return;
            }
            finished = Boolean.TRUE;
            if (timeOutThread != null && timeOutThread.isAlive()) {
                timeOutThread.interrupt();
            }
            stopListening();
        }
    }

    private void stopListening() {
        LocationHelper.removeLocationUpdatesListener(mContext, this);
    }

    private boolean isAccuracyEnough(Location location) {
        return location != null && location.getAccuracy() <= mMaximumAccuracy;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (isAccuracyEnough(location)) {
            stop();
            onMatchSuccess(location);
            return;
        }
        if (lastKnowLocation == null
                || LocationHelper.isBetterLocation(location, lastKnowLocation)) {
            lastKnowLocation = location;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
        if (provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER)) {
            if (listener != null) {
                stop();
                listener.onTurnOffGPS();
            }
        }
    }
}
