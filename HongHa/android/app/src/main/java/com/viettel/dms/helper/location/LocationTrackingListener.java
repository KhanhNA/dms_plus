package com.viettel.dms.helper.location;

import android.location.Location;

/**
 * Created by PHAMHUNG on 3/25/2016.
 */
public interface LocationTrackingListener {

    void onLocateSuccess(Location location);

    void onLocateFail();

    void onTurnOffGPS();
}
