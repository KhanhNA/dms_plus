package com.viettel.dms.presenter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.AlertDialogWrapper;
import com.viettel.dms.BuildConfig;
import com.viettel.dms.R;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.ImageUtils;
import com.viettel.dms.helper.location.LocationHelper;
import com.viettel.dms.helper.location.LocationTrackingListener;
import com.viettel.dms.ui.iview.ICheckInView;
import com.viettel.dmsplus.sdk.MainEndpoint;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.auth.OAuthSession;
import com.viettel.dmsplus.sdk.models.CheckInCreateDto;
import com.viettel.dmsplus.sdk.models.IdDto;
import com.viettel.dmsplus.sdk.models.UserInfo;
import com.viettel.dmsplus.sdk.network.RequestCompleteCallback;
import com.viettel.dmsplus.sdk.network.SdkAsyncTask;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PHAMHUNG on 3/25/2016.
 */
public class CheckInPresenter {
    ICheckInView iView;
    Context context;
    private Location mLocation = null;
    private SdkAsyncTask<?> networkTask;

    private List<String> lstPhotoPath = new ArrayList<>();
    private List<String> lstUploadedPhotoId = new ArrayList<>();
    private String note;
    public String currentPhotoId;

    public CheckInPresenter(ICheckInView i, Context context) {
        this.iView = i;
        this.context = context;
        lstPhotoPath.add("dummy"); // Add dummy
    }

    public void doCheckIn(final LocationCallback callback) {
        boolean okGPS = iView.checkLegalLocationProviderAndEnableGPS();
        if (!okGPS) return;
        iView.showProgressLocating();

        LocationTrackingListener listener = new LocationTrackingListener() {
            @Override
            public void onLocateSuccess(Location location) {
                mLocation = location;
                callback.onLocateSuccess();
            }

            @Override
            public void onLocateFail() {
                iView.dismissProgress();
                Dialog.OnClickListener clickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            callback.onContinueAnyway();
                        } else if (which == DialogInterface.BUTTON_NEUTRAL) {
                            doCheckIn(callback);
                        } else if (which == DialogInterface.BUTTON_POSITIVE) {
                            mLocation = null;
                        }
                    }
                };
                AlertDialogWrapper.Builder builder = new AlertDialogWrapper.Builder(context);
                builder.setTitle(R.string.warning);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.confirm_cancel, clickListener);
                builder.setNegativeButton(R.string.confirm_continue, clickListener);
                builder.setNeutralButton(R.string.confirm_retry, clickListener);
                builder.setMessage(R.string.customer_visit_message_cannot_locate);
                Dialog alertDialog = builder.create();
                alertDialog.show();
            }

            @Override
            public void onTurnOffGPS() {
                iView.dismissProgress();
                iView.showWarningTurnOffGPSWhenLocating();
            }
        };

        UserInfo userInfo = OAuthSession.getDefaultSession().getUserInfo();
        if (BuildConfig.DEBUG) {
            float maxAllowedDistance = userInfo.getVisitDistanceKPI() != 0 ? (float) userInfo.getVisitDistanceKPI() * 1000 : HardCodeUtil.CustomerVisit.MAX_ALLOWED_DISTANCE_DEBUG;
            LocationHelper.trackingLocation(context, maxAllowedDistance, HardCodeUtil.CustomerVisit.MAX_ALLOWED_TIME_DEBUG, listener);
        } else {
            float maxAllowedDistance = userInfo.getVisitDistanceKPI() != 0 ? (float) userInfo.getVisitDistanceKPI() * 1000 : HardCodeUtil.CustomerVisit.MAX_ALLOWED_DISTANCE_DEBUG;
            LocationHelper.trackingLocation(context, maxAllowedDistance, HardCodeUtil.CustomerVisit.MAX_ALLOWED_TIME, listener);
        }

    }

    public void doUploadPhoto(final int pos) {
        if (lstPhotoPath == null || lstPhotoPath.size() <= 1 || pos == lstPhotoPath.size() - 1) {
            doPostData();
        } else {
            iView.updateProgress(context.getString(R.string.check_in_sending_photo), pos + 1, lstPhotoPath.size() - 1);
            String filePath = lstPhotoPath.get(pos);
            File tempFile = null;
            try {
                tempFile = ImageUtils.createImageFile();
                ImageUtils.makeResizedCopyImage(filePath, 300, 300, tempFile.getAbsolutePath());
            } catch (IOException e) {
                Log.e("Exception",e.getMessage(),e);
            }
            final File finalTempFile = tempFile;
            if (tempFile != null && !ImageUtils.isImageDamaged(tempFile.getAbsolutePath())) {
                networkTask = MainEndpoint.get().requestUploadImage(tempFile.getAbsolutePath()).executeAsync(
                        new RequestCompleteCallback<IdDto>() {
                            @Override
                            public void onSuccess(IdDto data) {
                                currentPhotoId = data.getId();
                                lstUploadedPhotoId.add(currentPhotoId);
                            }

                            @Override
                            public void onError(SdkException info) {
                                Toast.makeText(context,R.string.fail_send_file_error,Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFinish(boolean canceled) {
                                networkTask = null;
                                if (finalTempFile.exists()) finalTempFile.delete();
                                doUploadPhoto(pos + 1);

                            }
                        }
                );
            } else {
                doUploadPhoto(pos + 1);
            }
        }
    }

    // TODO: In case of sending data fail, we need store uploaded photo Id to avoid duplication
    public void doPostData() {
        CheckInCreateDto dto = new CheckInCreateDto();
        dto.setNote(note);
        dto.setLocation(mLocation == null ? null : new com.viettel.dmsplus.sdk.models.Location(mLocation.getLatitude(), mLocation.getLongitude()));
        int size = lstUploadedPhotoId.size();
        if (size > 0) {
            String[] ids = new String[size];
            for (int i = 0; i < size; i++) {
                ids[i] = lstUploadedPhotoId.get(i);
            }
            dto.setPhotos(ids);
        }
        networkTask = MainEndpoint.get().requestCreateCheckIn(dto).executeAsync(new RequestCompleteCallback<IdDto>() {
            @Override
            public void onSuccess(IdDto response) {
                iView.dismissProgress();
                lstPhotoPath.clear();
                lstPhotoPath.add("dummy");
                iView.createCheckInSuccess();
            }

            @Override
            public void onError(SdkException ex) {
                iView.dismissProgress();
                iView.createCheckInFail();
            }

            @Override
            public void onFinish(boolean canceled) {
                lstUploadedPhotoId.clear();
            }
        });

    }

    public void doClickSend(String note) {
        setNote(note);
        doCheckIn(checkInCallBack);
    }

    LocationCallback checkInCallBack = new LocationCallback() {
        @Override
        public void onLocateSuccess() {
            doUploadPhoto(0);
        }

        @Override
        public void onContinueAnyway() {
            doUploadPhoto(0);
        }
    };


    public interface LocationCallback {
        void onLocateSuccess();

        void onContinueAnyway();
    }

    public List<String> getLstPhotoPath() {
        return lstPhotoPath;
    }

    public void setLstPhotoPath(List<String> lstPhotoPath) {
        this.lstPhotoPath = lstPhotoPath;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
