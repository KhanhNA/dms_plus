package com.viettel.dms.presenter;

import com.viettel.dms.ui.iview.ICheckinOfResultSpinner;
import com.viettel.dmsplus.sdk.MainEndpoint;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CategorySimpleResult;
import com.viettel.dmsplus.sdk.models.SaleStatisticToday;
import com.viettel.dmsplus.sdk.models.UserSimpleResult;
import com.viettel.dmsplus.sdk.network.RequestCompleteCallback;
import com.viettel.dmsplus.sdk.network.SdkAsyncTask;

import java.util.Arrays;

/**
 * Created by duongnv18 on 18/02/2017.
 */

public class CheckinOfResultPresenter extends BasePresenter {
    ICheckinOfResultSpinner iView;
    private SdkAsyncTask<?> getDataTask;

    public CheckinOfResultPresenter(ICheckinOfResultSpinner i) {
        iView = i;
    }

    @Override
    public void onStop() {
        if (getDataTask != null) {
            getDataTask.cancel(true);
            getDataTask = null;
        }
    }


    public void processGetAllDistributors() {
        RequestCompleteCallback<CategorySimpleResult> mCallback = new RequestCompleteCallback<CategorySimpleResult>() {
            @Override
            public void onSuccess(CategorySimpleResult data) {
                iView.getDistributorListSuccess(Arrays.asList(data.getList()));
            }

            @Override
            public void onError(SdkException info) {
                iView.getDistributorListError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };
        getDataTask = MainEndpoint.get().requestDistributorListAll().executeAsync(mCallback);



    }



    public void processGetSalemanOfSup(String supId) {
        RequestCompleteCallback<UserSimpleResult> mCallback = new RequestCompleteCallback<UserSimpleResult>() {
            @Override
            public void onSuccess(UserSimpleResult data) {

                iView.getSalemanListSuccess(Arrays.asList(data.getList()));
            }

            @Override
            public void onError(SdkException info) {
                iView.getSalemanListError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };

        getDataTask = MainEndpoint.get().requestListSalemasOfSup(supId).executeAsync(mCallback);


    }



    public  void getCheckinResultOfToday(String supId, String salemanId){
        RequestCompleteCallback<SaleStatisticToday> mCallback = new RequestCompleteCallback<SaleStatisticToday>() {
            @Override
            public void onSuccess(SaleStatisticToday data) {

                iView.getCheckinResultOfToday(data);
            }

            @Override
            public void onError(SdkException info) {
                iView.getCheckinResultOfTodayError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };

        getDataTask = MainEndpoint.get().requestGetCheckinResultOfToday(supId,salemanId).executeAsync(mCallback);


    }


}
