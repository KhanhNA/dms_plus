package com.viettel.dms.presenter;

import com.viettel.dms.ui.iview.ICustomerCheckinOfToday;
import com.viettel.dmsplus.sdk.MainEndpoint;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfToday;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfTodayResult;
import com.viettel.dmsplus.sdk.network.RequestCompleteCallback;
import com.viettel.dmsplus.sdk.network.SdkAsyncTask;

/**
 * Created by duongnv18 on 22/02/2017.
 */

public class CustomerCheckinOfTodayPresenter extends BasePresenter {

    ICustomerCheckinOfToday iView;
    private SdkAsyncTask<?> getDataTask;

    public CustomerCheckinOfTodayPresenter(ICustomerCheckinOfToday  i) {
        iView = i;
    }

    @Override
    public void onStop() {
        if (getDataTask != null) {
            getDataTask.cancel(true);
            getDataTask = null;
        }
    }

    public void getCustomerCheckinOfToday(String distributorId, String salesmanId) {
        RequestCompleteCallback<CustomerCheckinOfTodayResult> mCallback = new RequestCompleteCallback<CustomerCheckinOfTodayResult>() {
            @Override
            public void onSuccess(CustomerCheckinOfTodayResult data) {
                iView.getListCustomerSuccess(data);
            }

            @Override
            public void onError(SdkException info) {
                iView.getListCustomerError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };
        getDataTask = MainEndpoint.get().requestgetListCustomerCheckinOfToday(distributorId, salesmanId).executeAsync(mCallback);
    }


    public void getCustomerCheckinOfToday(String customerId) {
        RequestCompleteCallback<CustomerCheckinOfToday> mCallback = new RequestCompleteCallback<CustomerCheckinOfToday>() {
            @Override
            public void onSuccess(CustomerCheckinOfToday data) {
                iView.getCustomerCheckinDetailOfToday(data);
            }

            @Override
            public void onError(SdkException info) {
                iView.getListCustomerError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };
        getDataTask = MainEndpoint.get().requestgetCustomerCheckinOfTodayDetail(customerId).executeAsync(mCallback);
    }
}
