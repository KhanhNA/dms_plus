package com.viettel.dms.presenter;

import com.viettel.dms.ui.iview.IDistributorListview;
import com.viettel.dmsplus.sdk.MainEndpoint;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CategorySimple;
import com.viettel.dmsplus.sdk.models.CategorySimpleResult;
import com.viettel.dmsplus.sdk.models.CustomerForVisit;
import com.viettel.dmsplus.sdk.models.CustomerListResult;
import com.viettel.dmsplus.sdk.models.UserSimple;
import com.viettel.dmsplus.sdk.models.UserSimpleResult;
import com.viettel.dmsplus.sdk.network.RequestCompleteCallback;
import com.viettel.dmsplus.sdk.network.SdkAsyncTask;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author PHAMHUNG
 * @since 9/9/2015
 */
public class DistributorListPresenter extends BasePresenter {
    IDistributorListview iView;
    private SdkAsyncTask<?> getDataTask;

    CategorySimpleResult spDistributorTypeData;

    UserSimpleResult spSalemanTypeData;

    CustomerListResult spCustomerTypeData;


    private List<CategorySimple> mDataNotFilter = null;

    private List<UserSimple> mUserDataNotFilter = null;

    private List<CustomerForVisit> mCustomerDataNotFilter = null;

    public DistributorListPresenter(IDistributorListview i) {
        iView = i;
    }

    @Override
    public void onStop() {
        if (getDataTask != null) {
            getDataTask.cancel(true);
            getDataTask = null;
        }
    }

    public void processGetAllDistributors() {
        RequestCompleteCallback<CategorySimpleResult> mCallback = new RequestCompleteCallback<CategorySimpleResult>() {
            @Override
            public void onSuccess(CategorySimpleResult data) {
                spDistributorTypeData = data;
                if (mDataNotFilter == null) {
                    mDataNotFilter = new LinkedList<>();
                } else {
                    mDataNotFilter.clear();
                }
                mDataNotFilter.addAll(Arrays.asList(data.getList()));
                iView.getDistributorListSuccess();
            }

            @Override
            public void onError(SdkException info) {
                iView.getDistributorListError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };
        getDataTask = MainEndpoint.get().requestDistributorListAll().executeAsync(mCallback);



    }

    public void processGetSalemanOfSup(String supId) {
        RequestCompleteCallback<UserSimpleResult> mCallback = new RequestCompleteCallback<UserSimpleResult>() {
            @Override
            public void onSuccess(UserSimpleResult data) {
                spSalemanTypeData =  data;
                if (mUserDataNotFilter  == null) {
                    mUserDataNotFilter  = new LinkedList<>();
                } else {
                    mUserDataNotFilter .clear();
                }
                mUserDataNotFilter.addAll(Arrays.asList(data.getList()));
                iView.getSalemanListSuccess();
            }

            @Override
            public void onError(SdkException info) {
                iView.getSalemanListError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };

        getDataTask = MainEndpoint.get().requestListSalemasOfSup(supId).executeAsync(mCallback);



    }



    public void processGetListCustomer(String distributorId,String salemanId) {
        RequestCompleteCallback<CustomerListResult> mCallback = new RequestCompleteCallback<CustomerListResult>() {
            @Override
            public void onSuccess(CustomerListResult data) {
                spCustomerTypeData =  data;
                if (mCustomerDataNotFilter== null) {
                    mCustomerDataNotFilter= new LinkedList<>();
                } else {
                    mCustomerDataNotFilter.clear();
                }
                mCustomerDataNotFilter.addAll(Arrays.asList(data.getList()));
                iView.getListCustomerSuccess();
            }

            @Override
            public void onError(SdkException info) {
                iView.getListCustomerError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };

        getDataTask = MainEndpoint.get().requesCustomertListByDistributorAndSaleman(distributorId,salemanId).executeAsync(mCallback);



    }








    public CategorySimpleResult getSpDistributorTypeData() {
        return spDistributorTypeData;
    }

    public void setSpDistributorTypeData(CategorySimpleResult spDistributorTypeData) {
        this.spDistributorTypeData = spDistributorTypeData;
    }



    public List<CategorySimple> getmDataNotFilter() {
        return mDataNotFilter;
    }


    public UserSimpleResult getSpSalemanTypeData() {
        return spSalemanTypeData;
    }

    public void setSpSalemanTypeData(UserSimpleResult spSalemanTypeData) {
        this.spSalemanTypeData = spSalemanTypeData;
    }

    public CustomerListResult getSpCustomerTypeData() {
        return spCustomerTypeData;
    }

    public void setSpCustomerTypeData(CustomerListResult spCustomerTypeData) {
        this.spCustomerTypeData = spCustomerTypeData;
    }


    public List<CustomerForVisit> getmCustomerDataNotFilter() {
        return mCustomerDataNotFilter;
    }

    public void setmCustomerDataNotFilter(List<CustomerForVisit> mCustomerDataNotFilter) {
        this.mCustomerDataNotFilter = mCustomerDataNotFilter;
    }
}
