package com.viettel.dms.presenter;

import com.viettel.dms.ui.iview.IGeneralSaleStatisticListView;
import com.viettel.dms.ui.iview.IOrderCustomerListView;
import com.viettel.dmsplus.sdk.MainEndpoint;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CustomerForVisit;
import com.viettel.dmsplus.sdk.models.CustomerListResult;
import com.viettel.dmsplus.sdk.models.GeneralSaleStatisticTodayListResult;
import com.viettel.dmsplus.sdk.network.RequestCompleteCallback;
import com.viettel.dmsplus.sdk.network.SdkAsyncTask;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * @author duongnv18
 * @since 07/02/2017
 */
public class GeneralSaleStatisticListPresenter extends BasePresenter {
    IGeneralSaleStatisticListView iView;
    private SdkAsyncTask<?> getDataTask;

    public GeneralSaleStatisticListPresenter(IGeneralSaleStatisticListView i) {
        iView = i;
    }

    @Override
    public void onStop() {
        if (getDataTask != null) {
            getDataTask.cancel(true);
            getDataTask = null;
        }
    }

    public void processGeneralSaleStatistic() {
        RequestCompleteCallback<GeneralSaleStatisticTodayListResult> mCallback = new RequestCompleteCallback<GeneralSaleStatisticTodayListResult>() {
            @Override
            public void onSuccess(GeneralSaleStatisticTodayListResult data) {

                iView.getGeneralSaleStatisticListSuccess(data);
            }

            @Override
            public void onError(SdkException info) {
                iView.getGeneralSaleStatisticError(info);
            }

            @Override
            public void onFinish(boolean canceled) {
                super.onFinish(canceled);
                getDataTask = null;
                iView.finishTask();
            }
        };
        getDataTask = MainEndpoint.get().requestGeneralSaleStatisticToday().executeAsync(mCallback);
    }


}
