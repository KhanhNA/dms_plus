package com.viettel.dms.ui.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.rey.material.widget.SnackBar;
import com.viettel.dms.R;
import com.viettel.dms.helper.DialogUtils;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.ImageUtils;
import com.viettel.dms.helper.network.NetworkErrorDialog;
import com.viettel.dmsplus.sdk.MainEndpoint;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CustomerForVisit;
import com.viettel.dmsplus.sdk.models.IdDto;
import com.viettel.dmsplus.sdk.models.Location;
import com.viettel.dmsplus.sdk.network.RequestCompleteCallback;
import com.viettel.dmsplus.sdk.network.SdkAsyncTask;

import java.io.File;
import java.io.IOException;

import uk.co.senab.photoview.PhotoViewAttacher;

public class CloseVisitActivity extends BaseActivity {

    public static final String PARAM_CUSTOMER_INFO = "PARAM_CUSTOMER_INFO";
    public static final String PARAM_LOCATION = "PARAM_LOCATION";
    public static final String PARAM_PHOTO_ID = "PARAM_PHOTO_ID";
    public static final int REQUEST_IMAGE_CAPTURE = 10001;

    private CustomerForVisit customerInfo;
    private Location location;

    private String mCurrentPhotoPath;

    private Toolbar toolbar;
    private ImageView imageView;
    private PhotoViewAttacher mAttacher;

    private Dialog progressDialog;
    private SnackBar mSnackBar;

    private String photoId;
    private Boolean isTakingPicture = false;

    private SdkAsyncTask<?> mTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_visit);

        Bundle args = getIntent().getExtras();
        customerInfo = args.getParcelable(PARAM_CUSTOMER_INFO);
        location = args.getParcelable(PARAM_LOCATION);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        imageView = (ImageView) findViewById(R.id.imageView);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CloseVisitActivity.this.finish();
            }
        });
        setTitle(R.string.close_visit_title);
        mAttacher = new PhotoViewAttacher(imageView);
        synchronized (this) {
            if (!isTakingPicture) {
                if (checkPermissionStorage()) {
                    dispatchTakePictureIntent();
                    isTakingPicture = true;
                } else {
                    requestPermissionStorage();
                }
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mb_menu_close_visit, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_close_visit) {
            if (mCurrentPhotoPath == null) {
                return true;
            }
            progressDialog = DialogUtils.showProgressDialog(this, R.string.notify, R.string.close_visit_uploading_image, true);
            postImage();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mTask != null) {
            mTask.cancel(true);
            mTask = null;
        }
    }

    private boolean checkPermissionStorage() {
        return !(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionStorage() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, HardCodeUtil.Permission.STORAGE);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == HardCodeUtil.Permission.STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
                isTakingPicture = true;
            } else {
                if (mSnackBar != null && mSnackBar.getState() == SnackBar.STATE_SHOWING) {
                    mSnackBar.dismiss();
                    mSnackBar = null;
                }
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    mSnackBar = SnackBar.make(this)
                            .text(R.string.permission_storage_explain)
                            .actionText(R.string.permission_storage_explain_action)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    requestPermissionStorage();
                                }
                            });
                    mSnackBar.applyStyle(R.style.SnackBarSingleLine);
                    mSnackBar.show(this
                    );
                } else {
                    mSnackBar = SnackBar.make(this)
                            .text(R.string.permission_storage_force)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            })
                            .actionText(R.string.permission_storage_force_action);
                    mSnackBar.applyStyle(R.style.SnackBarMultiLine);
                    mSnackBar.show(this);
                }
            }
        }
    }

    private void postImage() {
        mTask = MainEndpoint.get().requestUploadImage(mCurrentPhotoPath).executeAsync(
                new RequestCompleteCallback<IdDto>() {

                    @Override
                    public void onSuccess(IdDto data) {
                        photoId = data.getId();
                        closeVisit();
                    }

                    @Override
                    public void onError(SdkException info) {
                        progressDialog.dismiss();
                        progressDialog = null;
                        NetworkErrorDialog.processError(CloseVisitActivity.this, info);
                    }

                    @Override
                    public void onFinish(boolean canceled) {
                        mTask = null;
                    }
                }
        );
    }

    private void closeVisit() {
        mTask = MainEndpoint.get().requestCloseVisit(customerInfo.getId(), photoId, location).executeAsync(
                new RequestCompleteCallback<IdDto>() {
                    @Override
                    public void onSuccess(IdDto data) {
                        progressDialog.dismiss();
                        progressDialog = null;
                        new File(mCurrentPhotoPath).delete();
                        Intent i = new Intent();
                        i.putExtra(PARAM_PHOTO_ID, photoId);
                        CloseVisitActivity.this.setResult(RESULT_OK, i);
                        CloseVisitActivity.this.finish();
                    }

                    @Override
                    public void onError(SdkException info) {
                        progressDialog.dismiss();
                        progressDialog = null;
                        NetworkErrorDialog.processError(CloseVisitActivity.this, info);
                    }

                    @Override
                    public void onFinish(boolean canceled) {
                        mTask = null;
                    }
                }
        );
    }

    @Override
    public void onBackPressed() {

    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile;
            try {
                photoFile = ImageUtils.createImageFile();
                mCurrentPhotoPath = photoFile.getAbsolutePath();
            } catch (Exception ex) {
                // Error occurred while creating the File
                DialogUtils.showMessageDialog(this,
                        R.string.error, R.string.error_unknown,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                Log.e("Exception",ex.getMessage(),ex);
                return;
            }
            // Continue only if the File was successfully created
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

        } else {
            DialogUtils.showMessageDialog(this,
                    R.string.error,
                    R.string.error_not_have_cammera_app,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            isTakingPicture = false;
            imageView.post(new Runnable() {
                @Override
                public void run() {
                    Bitmap bm = ImageUtils.optimizeAndCompressImage(mCurrentPhotoPath, imageView.getWidth(), imageView.getHeight());
                    imageView.setImageBitmap(bm);
                    mAttacher.update();
                }
            });
        } else {
            finish();
        }
    }
}
