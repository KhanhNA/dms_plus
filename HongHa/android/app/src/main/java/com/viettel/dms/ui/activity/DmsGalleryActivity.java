package com.viettel.dms.ui.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.rey.material.widget.SnackBar;
import com.squareup.picasso.Picasso;
import com.viettel.dms.R;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.ImageUtils;
import com.viettel.dms.helper.layout.SquareImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DmsGalleryActivity extends BaseActivity {

    @Bind(R.id.grid_photos)
    GridView gridPhotos;
    @Bind(R.id.app_bar)
    Toolbar toolbar;

    int numberPhoto;
    GridAdapter adapter;
    List<MyGalleryItem> lstAllImagePaths = new ArrayList<>();
    SnackBar mSnackBar;
    LayoutInflater inflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        numberPhoto = getIntent().getIntExtra("NUMBER_PHOTO", HardCodeUtil.MAXIMUM_NUMBER_OF_PHOTOS);
        setContentView(R.layout.activity_dms_gallery);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DmsGalleryActivity.this.finish();
            }
        });
        setTitle(R.string.check_in_choose_from_gallery);

        inflater = LayoutInflater.from(this);
        adapter = new GridAdapter();
        gridPhotos.setAdapter(adapter);
        gridPhotos.setOnItemClickListener(onItemClickListener);

        if (checkPermissionStorage()) {
            lstAllImagePaths = getAllImageUriFromPhone(this);
            adapter.notifyDataSetChanged();
        } else {
            requestPermissionStorage();
        }
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            boolean isSelected = lstAllImagePaths.get(position).isSelected;
            lstAllImagePaths.get(position).isSelected = !isSelected;
            adapter.notifyDataSetChanged();
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mb_menu_action_done, menu);
        MenuItem itemDeselectAll = menu.findItem(R.id.action_done);
        itemDeselectAll.setOnMenuItemClickListener(onMenuItemClickListener);
        return super.onCreateOptionsMenu(menu);
    }

    private boolean checkPermissionStorage() {
        return !(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionStorage() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, HardCodeUtil.Permission.STORAGE);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == HardCodeUtil.Permission.STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                lstAllImagePaths = getAllImageUriFromPhone(this);
                adapter.notifyDataSetChanged();
                ;
            } else {
                if (mSnackBar != null && mSnackBar.getState() == SnackBar.STATE_SHOWING) {
                    mSnackBar.dismiss();
                    mSnackBar = null;
                }
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    mSnackBar = SnackBar.make(this)
                            .text(R.string.permission_storage_explain)
                            .actionText(R.string.permission_storage_explain_action)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    requestPermissionStorage();
                                }
                            });
                    mSnackBar.applyStyle(R.style.SnackBarSingleLine);
                    mSnackBar.show(this
                    );
                } else {
                    mSnackBar = SnackBar.make(this)
                            .text(R.string.permission_storage_force)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            })
                            .actionText(R.string.permission_storage_force_action);
                    mSnackBar.applyStyle(R.style.SnackBarMultiLine);
                    mSnackBar.show(this);
                }
            }
        }
    }

    private MenuItem.OnMenuItemClickListener onMenuItemClickListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (item.getItemId() == R.id.action_done) {
                ArrayList<String> selectedPhotoPaths = new ArrayList<>();
                for (MyGalleryItem i : lstAllImagePaths) {
                    if (i.isSelected) {
                        selectedPhotoPaths.add(i.photoPath);
                    }
                }

                if (selectedPhotoPaths.size() > numberPhoto) {
                    Toast.makeText(getApplicationContext(), R.string.check_in_maximum_of_photo_taken, Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent();
                    intent.putStringArrayListExtra("DATA", selectedPhotoPaths);
                    setResult(RESULT_OK, intent);
                    DmsGalleryActivity.this.finish();
                }
            }
            return true;
        }
    };


    private List<MyGalleryItem> getAllImageUriFromPhone(DmsGalleryActivity dmsGalleryActivity) {
        List<MyGalleryItem> images = new ArrayList<>();
        Cursor imageCursor = null;
        try {
            final String[] columns = {MediaStore.Images.Media.DATA};
            final String orderBy = MediaStore.Images.Media.DATE_ADDED + " DESC";

            imageCursor = dmsGalleryActivity.getApplicationContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
            while (imageCursor != null && imageCursor.moveToNext()) {
                Uri uri = Uri.parse(imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.Media.DATA)));
                MyGalleryItem item = new MyGalleryItem();
                item.isSelected = false;
                item.photoPath = uri.toString();
                images.add(item);
            }
        } catch (Exception e) {
            Log.e("Exception",e.getMessage(),e);
        } finally {
            if (imageCursor != null && !imageCursor.isClosed()) {
                imageCursor.close();
            }
        }
        return images;
    }

    class GridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return lstAllImagePaths == null ? 0 : lstAllImagePaths.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = inflater.inflate(R.layout.adapter_gallery_item, parent, false);
                GridViewHolder holder = new GridViewHolder();
                holder.imageView = (SquareImageView) row.findViewById(R.id.img_Check_In);
                holder.checkBox = (ImageView) row.findViewById(R.id.img_Check_Box);
                row.setTag(holder);
            }
            final GridViewHolder holder = (GridViewHolder) row.getTag();
            MyGalleryItem item = lstAllImagePaths.get(position);
            if (ImageUtils.isImageDamaged(item.photoPath)) {
                Picasso.with(getApplicationContext()).load(R.drawable.no_media).noFade().centerCrop().resize(200, 200).into(holder.imageView);
            } else {
                Picasso.with(getApplicationContext()).load(new File(item.photoPath)).resize(200, 200).centerCrop().into(holder.imageView);
            }
            holder.checkBox.setSelected(item.isSelected);
            return row;
        }
    }

    class GridViewHolder {
        SquareImageView imageView;
        ImageView checkBox;
    }

    class MyGalleryItem {
        String photoPath;
        boolean isSelected;
    }
}

