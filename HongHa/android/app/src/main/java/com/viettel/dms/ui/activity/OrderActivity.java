package com.viettel.dms.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.viettel.dms.R;
import com.viettel.dms.helper.layout.LayoutUtils;
import com.viettel.dms.ui.fragment.BaseFragment;
import com.viettel.dms.ui.fragment.OrderCustomerListFragment;
import com.viettel.dms.ui.fragment.PlaceOrderProductListMBFragment;
import com.viettel.dms.ui.fragment.PlaceOrderProductListTBFragment;
import com.viettel.dmsplus.sdk.models.CustomerForVisit;
import com.viettel.dmsplus.sdk.models.CustomerSimple;
import com.viettel.dmsplus.sdk.models.OrderDetailResult;
import com.viettel.dmsplus.sdk.models.OrderHolder;
import com.viettel.dmsplus.sdk.models.PlaceOrderProduct;

public class OrderActivity extends BaseActivity implements FragmentManager.OnBackStackChangedListener {
    private Toolbar mToolbar;
    int defaultToolbarPadding;

    // In case recreate rejected order
    CustomerSimple customer;
    OrderDetailResult.OrderDetailItem[] products;
    boolean isCaseRecreateRejectedOrder = false;

    // In case go from list all customers
    CustomerForVisit customerInfo;
    boolean isVansales;
    boolean isGoFromListCustomerAll = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        isCaseRecreateRejectedOrder = intent.getBooleanExtra("recreate", false);
        customer = intent.getParcelableExtra("customer");
        customerInfo = intent.getParcelableExtra("customerFromAll");
        isVansales = intent.getBooleanExtra("isVanSales", false);
        isGoFromListCustomerAll = intent.getBooleanExtra("isGoFromListCustomerAll", false);
        Parcelable[] temp = intent.getParcelableArrayExtra("products");
        if (temp !=null && temp.length > 0) {
            int length = temp.length;
            products = new OrderDetailResult.OrderDetailItem[length];
            for (int i = 0; i < length; i++) {
                if (temp[i] instanceof OrderDetailResult.OrderDetailItem) {
                    products[i] = (OrderDetailResult.OrderDetailItem) temp[i];
                }
            }
        }


        setContentView(R.layout.activity_order);
        defaultToolbarPadding = getResources().getDimensionPixelSize(R.dimen.second_keyline);
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if (customer != null && products != null) {
            // Go from recreate rejected order
            CustomerForVisit customerInfo = new CustomerForVisit();
            customerInfo.setId(customer.getId());
            customerInfo.setName(customer.getName());
            customerInfo.setAddress(customer.getAddress());

            OrderHolder orderHolder = new OrderHolder();
            orderHolder.productsSelected = new PlaceOrderProduct[products.length];
            for (int i = 0; i < products.length; i++) {
                PlaceOrderProduct item = new PlaceOrderProduct();
                item.setId(products[i].getProduct().getId());
                item.setName(products[i].getProduct().getName());
                item.setQuantity(products[i].getQuantity().intValue());
                orderHolder.productsSelected[i] = item;
            }

            PlaceOrderProductListMBFragment fragment = PlaceOrderProductListMBFragment.newInstance(customerInfo, null, orderHolder, false, isCaseRecreateRejectedOrder,false);
            replaceCurrentFragment(fragment, true, false);

        } else if (customerInfo != null) {
            // Go from list all customer
            PlaceOrderProductListMBFragment fragment = PlaceOrderProductListMBFragment.newInstance(customerInfo, null, null, isVansales, false,true);
            replaceCurrentFragment(fragment, true, false);

        } else {
            OrderCustomerListFragment fragment = OrderCustomerListFragment.newInstance();
            replaceCurrentFragment(fragment, true, false);
        }
    }

    @Override
    public void adjustWhenFragmentChanged(BaseFragment fragment) {
        super.adjustWhenFragmentChanged(fragment);

        if (fragment.getPaddingLeft() != null) {
            // Convert dip to px
            int paddingInPx = LayoutUtils.dipToPx(this, fragment.getPaddingLeft());
            mToolbar.setContentInsetsRelative(paddingInPx, mToolbar.getContentInsetEnd());
        } else {
            mToolbar.setContentInsetsRelative(defaultToolbarPadding, mToolbar.getContentInsetEnd());
        }
    }

    @Override
    public void onBackStackChanged() {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 2) {
            mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        } else {
            mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        }
    }
}
