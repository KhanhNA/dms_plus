package com.viettel.dms.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import com.viettel.dms.R;
import com.viettel.dms.ui.fragment.OrderCustomerListFragment;
import com.viettel.dms.ui.fragment.PlaceOrderProductListMBFragment;
import com.viettel.dms.ui.fragment.PlaceOrderProductListTBFragment;
import com.viettel.dmsplus.sdk.models.CustomerForVisit;
import com.viettel.dmsplus.sdk.models.CustomerSimple;
import com.viettel.dmsplus.sdk.models.OrderDetailResult;
import com.viettel.dmsplus.sdk.models.OrderHolder;
import com.viettel.dmsplus.sdk.models.PlaceOrderProduct;

public class OrderTBActivity extends BaseActivity {

    // In case recreate rejected order
    CustomerSimple customer;
    OrderDetailResult.OrderDetailItem[] products;
    boolean isRecreateRejectedOrder = false;

    // In case go from list all customers
    CustomerForVisit customerInfo;
    boolean isVansales;
    boolean isGoFromListCustomerAll = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_tb);

        Intent intent = getIntent();
        isRecreateRejectedOrder = intent.getBooleanExtra("recreate", false);
        customer = intent.getParcelableExtra("customer");
        customerInfo = intent.getParcelableExtra("customerFromAll");
        isVansales = intent.getBooleanExtra("isVanSales", false);
        isGoFromListCustomerAll = intent.getBooleanExtra("isGoFromListCustomerAll", false);

        Parcelable[] temp = intent.getParcelableArrayExtra("products");
        if (temp != null && temp.length > 0) {
            int length = temp.length;
            products = new OrderDetailResult.OrderDetailItem[length];
            for (int i = 0; i < length; i++) {
                if (temp[i] instanceof OrderDetailResult.OrderDetailItem) {
                    products[i] = (OrderDetailResult.OrderDetailItem) temp[i];
                }
            }
        }

        if (isRecreateRejectedOrder && customer != null && products != null) {
            // Go from recreate rejected order
            CustomerForVisit customerInfo = new CustomerForVisit();
            customerInfo.setId(customer.getId());
            customerInfo.setName(customer.getName());
            customerInfo.setAddress(customer.getAddress());

            OrderHolder orderHolder = new OrderHolder();
            orderHolder.productsSelected = new PlaceOrderProduct[products.length];
            for (int i = 0; i < products.length; i++) {
                PlaceOrderProduct item = new PlaceOrderProduct();
                item.setId(products[i].getProduct().getId());
                item.setName(products[i].getProduct().getName());
                item.setQuantity(products[i].getQuantity().intValue());
                orderHolder.productsSelected[i] = item;
            }

            PlaceOrderProductListTBFragment fragment = PlaceOrderProductListTBFragment.newInstance(customerInfo, null, orderHolder, false, isRecreateRejectedOrder, false);
            replaceCurrentFragment(fragment, true, false);

        } else if (customerInfo != null) {
            // Go from list all customer
            PlaceOrderProductListTBFragment fragment = PlaceOrderProductListTBFragment.newInstance(customerInfo, null, null, isVansales, false, true);
            replaceCurrentFragment(fragment, true, false);
        } else {
            OrderCustomerListFragment fragment = OrderCustomerListFragment.newInstance();
            replaceCurrentFragment(fragment, true, false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
