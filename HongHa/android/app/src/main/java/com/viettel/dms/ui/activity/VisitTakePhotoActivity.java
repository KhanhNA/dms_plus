package com.viettel.dms.ui.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.rey.material.widget.SnackBar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.viettel.dms.R;
import com.viettel.dms.helper.DialogUtils;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.ImageUtils;
import com.viettel.dms.helper.StringUtils;
import com.viettel.dmsplus.sdk.MainEndpoint;

import java.io.File;
import java.io.IOException;

import uk.co.senab.photoview.PhotoViewAttacher;

public class VisitTakePhotoActivity extends BaseActivity {
    public static final int REQUEST_IMAGE_CAPTURE = 10002;
    public static final String PARAM_URL = "PARAM_URL";
    public static final String PARAM_IS_VISITED = "PARAM_IS_VISITED";
    public static final String PARAM_VISITED_PHOTO_ID = "PARAM_VISITED_PHOTO_ID";

    private String mOriginalPhotoPath;
    private String mCurrentPhotoPath;
    private String mPreviousPhotoPath;
    private boolean isVisited = false;
    private String visitedPhotoId;

    private Toolbar toolbar;
    private ImageView imageView;
    private PhotoViewAttacher mAttacher;
    private SnackBar mSnackBar;
    private Boolean isTakingPicture = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_take_picture);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        imageView = (ImageView) findViewById(R.id.imageView);
        isVisited = getIntent().getBooleanExtra(PARAM_IS_VISITED, false);
        visitedPhotoId = getIntent().getStringExtra(PARAM_VISITED_PHOTO_ID);
        mOriginalPhotoPath = getIntent().getStringExtra(PARAM_URL);
        mCurrentPhotoPath = mOriginalPhotoPath;

        setSupportActionBar(toolbar);
        setTitle(isVisited ? R.string.visit_view_photo_title : R.string.visit_take_photo_title);
        mAttacher = new PhotoViewAttacher(imageView);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!StringUtils.isNullOrEmpty(mCurrentPhotoPath) && !mCurrentPhotoPath.equalsIgnoreCase(mOriginalPhotoPath)) {
                    new File(mCurrentPhotoPath).delete();
                }
                VisitTakePhotoActivity.this.setResult(RESULT_CANCELED);
                VisitTakePhotoActivity.this.finish();
            }
        });
        if (isVisited) {
            Picasso.with(this).load(MainEndpoint.get().getImageURL(visitedPhotoId)).into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    mAttacher.update();
                }

                @Override
                public void onError() {

                }
            });

            return;
        }
        if (StringUtils.isNullOrEmpty(mCurrentPhotoPath) || !(new File(mCurrentPhotoPath).exists())) {
            synchronized (this) {
                if (!isTakingPicture) {
                    if (checkPermissionStorage()) {
                        mPreviousPhotoPath = mCurrentPhotoPath;
                        dispatchTakePictureIntent();
                        isTakingPicture = true;
                    } else {
                        requestPermissionStorage();
                    }
                }
            }
        } else {
            imageView.post(new Runnable() {
                @Override
                public void run() {
                    Bitmap image = BitmapFactory.decodeFile(mCurrentPhotoPath);
                    imageView.setImageBitmap(image);
                    mAttacher.update();
                }
            });
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isVisited) {
            getMenuInflater().inflate(R.menu.menu_visit_take_picture, menu);
        }
        return true;
    }

    private boolean checkPermissionStorage() {
        return !(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionStorage() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, HardCodeUtil.Permission.STORAGE);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == HardCodeUtil.Permission.STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mPreviousPhotoPath = mCurrentPhotoPath;
                dispatchTakePictureIntent();
                isTakingPicture = true;
            } else {
                if (mSnackBar != null && mSnackBar.getState() == SnackBar.STATE_SHOWING) {
                    mSnackBar.dismiss();
                    mSnackBar = null;
                }
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    mSnackBar = SnackBar.make(this)
                            .text(R.string.permission_storage_explain)
                            .actionText(R.string.permission_storage_explain_action)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    requestPermissionStorage();
                                }
                            });
                    mSnackBar.applyStyle(R.style.SnackBarSingleLine);
                    mSnackBar.show(this
                    );
                } else {
                    mSnackBar = SnackBar.make(this)
                            .text(R.string.permission_storage_force)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            })
                            .actionText(R.string.permission_storage_force_action);
                    mSnackBar.applyStyle(R.style.SnackBarMultiLine);
                    mSnackBar.show(this);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_take_picture) {
            mPreviousPhotoPath = mCurrentPhotoPath;
            mCurrentPhotoPath = null;
            synchronized (this) {
                if (!isTakingPicture) {
                    dispatchTakePictureIntent();
                    isTakingPicture = true;
                }
            }
        }
        if (item.getItemId() == R.id.action_done) {
            if (!StringUtils.isNullOrEmpty(mOriginalPhotoPath) && !mCurrentPhotoPath.equalsIgnoreCase(mOriginalPhotoPath)) {
                new File(mOriginalPhotoPath).delete();
            }
            Intent intent = new Intent();
            intent.putExtra(PARAM_URL, mCurrentPhotoPath);
            VisitTakePhotoActivity.this.setResult(RESULT_OK, intent);
            VisitTakePhotoActivity.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            isTakingPicture = false;
            // Delete previous image except original image
            if (!StringUtils.isNullOrEmpty(mPreviousPhotoPath) && !mPreviousPhotoPath.equalsIgnoreCase(mOriginalPhotoPath) && new File(mPreviousPhotoPath).exists()) {
                new File(mPreviousPhotoPath).delete();
            }
            mPreviousPhotoPath = null;
            imageView.post(new Runnable() {
                @Override
                public void run() {
                    Bitmap bm = ImageUtils.optimizeAndCompressImage(mCurrentPhotoPath, imageView.getWidth(), imageView.getHeight());
                    imageView.setImageBitmap(bm);
                    mAttacher.update();
                }
            });
        } else {
            finish();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = ImageUtils.createImageFile();
                mCurrentPhotoPath = photoFile.getAbsolutePath();
            } catch (Exception ex) {
                // Error occurred while creating the File
                DialogUtils.showMessageDialog(this,
                        R.string.error, R.string.error_unknown,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                Log.e("Exception", ex.getMessage(),ex);
                return;
            }
            // Continue only if the File was successfully created
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

        } else {
            DialogUtils.showMessageDialog(this,
                    R.string.error,
                    R.string.error_not_have_cammera_app,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
        }
    }
}
