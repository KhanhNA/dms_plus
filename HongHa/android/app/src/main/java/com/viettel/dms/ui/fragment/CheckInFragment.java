package com.viettel.dms.ui.fragment;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.widget.SnackBar;
import com.squareup.picasso.Picasso;
import com.viettel.dms.R;
import com.viettel.dms.helper.DialogUtils;
import com.viettel.dms.helper.GooglePlayUtils;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.ImageUtils;
import com.viettel.dms.helper.StringUtils;
import com.viettel.dms.helper.dialog.ImageDialog;
import com.viettel.dms.helper.layout.ExpandableHeightGridView;
import com.viettel.dms.helper.layout.SquareImageView;
import com.viettel.dms.helper.location.LocationHelper;
import com.viettel.dms.presenter.CheckInPresenter;
import com.viettel.dms.ui.activity.DmsGalleryActivity;
import com.viettel.dms.ui.iview.ICheckInView;
import com.viettel.dmsplus.sdk.auth.OAuthSession;
import com.viettel.dmsplus.sdk.models.UserInfo;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CheckInFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener, ICheckInView {
    public static final int REQUEST_IMAGE_CAPTURE = 10002;
    public static final int SELECT_PHOTO = 10003;
    @Bind(R.id.google_map)
    MapView mMapView;
    @Bind(R.id.edt_content)
    MaterialEditText metContent;
    @Bind(R.id.grid_photos)
    ExpandableHeightGridView gridView;
    ImageAdapter imageAdapter;
    String currentAddedPhotoPath = null;
    private int currentClickedPhoto = -1;

    double lastLatitude = 0d, lastLongitude = 0d;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mGoogleMap;
    private Marker locationMaker;
    Dialog mProgressDialog;
    SnackBar mSnackBar;

    BitmapDescriptor markerIcon;
    CheckInPresenter presenter;

    public CheckInFragment() {
        // Required empty public constructor
    }

    public static CheckInFragment newInstance() {
        return new CheckInFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createGoogleApiClient();
        createLocationRequest();
        setHasOptionsMenu(true);
        presenter = new CheckInPresenter(this, context);
        imageAdapter = new ImageAdapter();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_in, container, false);
        ButterKnife.bind(this, view);
        setTitleResource(R.string.navigation_drawer_item_check_in);
        mMapView.onCreate(savedInstanceState);
        gridView.setAdapter(imageAdapter);
        gridView.setExpanded(true);
        gridView.setOnItemClickListener(onItemClickListener);

        return view;
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (position == presenter.getLstPhotoPath().size() - 1) {
                if (presenter.getLstPhotoPath().size() >= HardCodeUtil.MAXIMUM_NUMBER_OF_PHOTOS + 1) {
                    Toast.makeText(context, R.string.check_in_maximum_of_photo_taken, Toast.LENGTH_SHORT).show();
                } else {
                    // Add new Image
                    DialogUtils.showChangeSettingDialog(context, R.string.check_in_add_photo, R.array.check_in_ways_to_add_photo, popUpCallbackAddPhoto);
                }
            } else {
                // View or delete this image
                currentClickedPhoto = position;
                DialogUtils.showChangeSettingDialog(context, R.string.check_in_view_or_delete_photo, R.array.check_in_choose_action_for_photo, popUpCallbackViewOrDelete);
            }
        }
    };
    private MaterialDialog.ListCallback popUpCallbackAddPhoto = new MaterialDialog.ListCallback() {

        @Override
        public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
            switch (which) {
                case 0: // Take photo
                    if (checkPermissionStorage()) {
                        dispatchTakePictureIntent();
                    } else {
                        requestPermissionStorage();
                    }
                    break;
                case 1: // Choose from gallery
                    if (checkPermissionStorage()) {
                        Intent intent = new Intent(context, DmsGalleryActivity.class);
                        intent.putExtra("NUMBER_PHOTO", HardCodeUtil.MAXIMUM_NUMBER_OF_PHOTOS - presenter.getLstPhotoPath().size() + 1);
                        startActivityForResult(intent, SELECT_PHOTO);
                    } else {
                        requestPermissionStorage();
                    }
                    break;
                default:
                    break;
            }
        }
    };
    private MaterialDialog.ListCallback popUpCallbackViewOrDelete = new MaterialDialog.ListCallback() {

        @Override
        public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
            switch (which) {
                case 0: // View
                    ImageDialog imgDialog = new ImageDialog(context, presenter.getLstPhotoPath().get(currentClickedPhoto), true);
                    imgDialog.show();
                    break;
                case 1: // Delete
                    presenter.getLstPhotoPath().remove(currentClickedPhoto);
                    currentClickedPhoto = -1;
                    imageAdapter.notifyDataSetChanged();
                    break;
                default:
                    break;
            }
        }
    };

    private boolean checkPermissionStorage() {
        return !(ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissionStorage() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, HardCodeUtil.Permission.STORAGE);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == HardCodeUtil.Permission.STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                if (mSnackBar != null && mSnackBar.getState() == SnackBar.STATE_SHOWING) {
                    mSnackBar.dismiss();
                    mSnackBar = null;
                }
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    mSnackBar = SnackBar.make(context)
                            .text(R.string.permission_storage_explain)
                            .actionText(R.string.permission_storage_explain_action)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    requestPermissionStorage();
                                }
                            });
                    mSnackBar.applyStyle(R.style.SnackBarSingleLine);
                    mSnackBar.show(getActivity()
                    );
                } else {
                    mSnackBar = SnackBar.make(context
                    )
                            .text(R.string.permission_storage_force)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            })
                            .actionText(R.string.permission_storage_force_action);
                    mSnackBar.applyStyle(R.style.SnackBarMultiLine);
                    mSnackBar.show(getActivity());
                }
            }
        }
        if (requestCode == HardCodeUtil.Permission.LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                return;
            } else {
                if (mSnackBar != null && mSnackBar.getState() == SnackBar.STATE_SHOWING) {
                    mSnackBar.dismiss();
                    mSnackBar = null;
                }
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    mSnackBar = SnackBar.make(context)
                            .text(R.string.permission_location_explain)
                            .actionText(R.string.permission_location_explain_action)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    requestPermissionLocation();
                                }
                            });
                    mSnackBar.applyStyle(R.style.SnackBarSingleLine);
                    mSnackBar.show(getActivity());
                } else {
                    mSnackBar = SnackBar.make(context)
                            .text(R.string.permission_location_force)
                            .actionClickListener(new SnackBar.OnActionClickListener() {
                                @Override
                                public void onActionClick(SnackBar sb, int actionId) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            })
                            .actionText(R.string.permission_location_force_action);
                    mSnackBar.applyStyle(R.style.SnackBarMultiLine);
                    mSnackBar.show(getActivity());
                }
            }
        }
    }

    private void requestPermissionLocation() {
        requestPermissions(new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        }, HardCodeUtil.Permission.LOCATION);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile;
            try {
                photoFile = ImageUtils.createImageFile();
                currentAddedPhotoPath = photoFile.getAbsolutePath();
            } catch (Exception ex) {
                // Error occurred while creating the File
                DialogUtils.showMessageDialog(context,
                        R.string.error, R.string.error_unknown);
                Log.e("Exception", ex.getMessage(),ex);
                return;
            }
            // Continue only if the File was successfully created
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

        } else {
            DialogUtils.showMessageDialog(context,
                    R.string.error,
                    R.string.error_not_have_cammera_app);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            int currentSize = presenter.getLstPhotoPath().size();
            switch (requestCode) {
                case REQUEST_IMAGE_CAPTURE:
                    presenter.getLstPhotoPath().add(currentSize - 1, currentAddedPhotoPath);
                    ImageUtils.registerThisURI(context, Uri.fromFile(new File(currentAddedPhotoPath)));
                    break;
                case SELECT_PHOTO:
                    List<String> selectedPath = data.getStringArrayListExtra("DATA");
                    presenter.getLstPhotoPath().addAll(currentSize - 1, selectedPath);
                    break;
                default:
                    return;
            }
            currentAddedPhotoPath = null;
            imageAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        if (GooglePlayUtils.checkPlayService(context, getActivity())) {
            MapsInitializer.initialize(this.getActivity());
            setUpMapIfNeeded();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    private void setUpMapIfNeeded() {
        if (mGoogleMap == null) {
            UserInfo userInfo = OAuthSession.getDefaultSession().getUserInfo();
            double defaultLatitude = userInfo.getLocation().getLatitude() != 0 ? userInfo.getLocation().getLatitude() : HardCodeUtil.Location.defaultLast;
            double defaultLongitude = userInfo.getLocation().getLongitude() != 0 ? userInfo.getLocation().getLongitude() : HardCodeUtil.Location.defaultLong;
            mGoogleMap = mMapView.getMap();
            markerIcon = BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.marker_icon));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(defaultLatitude, defaultLongitude), 14.0f));
        }
        try {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissionLocation();
                return;
            } else {
                mGoogleMap.setMyLocationEnabled(false);
            }
            if(LocationHelper.isGpsProviderEnabled(context) == false){
                LocationHelper.showAlertNoLocationService(context);
            }
        } catch (Exception ex) {
            Log.e("Exception", ex.getMessage(),ex);
        }
    }

    private void zoomToMyLocation(Location location) {
        try {
            mGoogleMap.clear();
            if (location == null) return;
            lastLatitude = location.getLatitude();
            lastLongitude = location.getLongitude();
            LatLng newLocation = new LatLng(lastLatitude, lastLongitude);
            locationMaker = mGoogleMap.addMarker(new MarkerOptions().position(newLocation).icon(markerIcon).draggable(false));
            locationMaker.showInfoWindow();
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 18));
        } catch (Exception ex) {
            Log.e("Exception", ex.getMessage(),ex);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissionLocation();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            zoomToMyLocation(location);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.mb_menu_action_send, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        if (item.getItemId() == R.id.action_send) {
            if (validateData()) {
                String note = metContent.getText().toString();
                presenter.doClickSend(note);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validateData() {
        String note = metContent.getText().toString();
        if (StringUtils.isNullOrEmpty(note) || note.length() > 500) {
            metContent.setError(context.getString(R.string.check_in_validate_note));
            return false;
        }
        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public boolean checkLegalLocationProviderAndEnableGPS() {
        if (LocationHelper.isMockSettingsOn(context)) {
            LocationHelper.showAlertTurnOffMockLocation(context);
            return false;
        }
        if (!LocationHelper.isGpsProviderEnabled(context)) {
            LocationHelper.showAlertNoLocationService(context);
            return false;
        }
        if (!LocationHelper.isHaveRequiredPermission(context)) {
            LocationHelper.requestPermission(getActivity());
            return false;
        }

        return true;
    }

    @Override
    public void showProgressLocating() {
        mProgressDialog = DialogUtils.showProgressDialog(context, null, StringUtils.getString(context, R.string.customer_visit_message_locating), true, false);
    }

    @Override
    public void dismissProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    public void showWarningTurnOffGPSWhenLocating() {
        DialogUtils.showMessageDialog(context, R.string.warning, R.string.customer_visit_location_turn_off_manually);
    }

    @Override
    public void createCheckInSuccess() {
        DialogUtils.showMessageDialog(context, R.string.notify, R.string.check_in_success, R.string.confirm_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                metContent.setText("");
                imageAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void createCheckInFail() {
        DialogUtils.showMessageDialog(context, R.string.notify, R.string.check_in_fail);
    }

    @Override
    public void updateProgress(final String progress, final int current, final int total) {
        if (mProgressDialog != null) {
            if (mProgressDialog instanceof MaterialDialog) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MaterialDialog) mProgressDialog).setContent(String.format("%s (%s/%s)", progress, current, total));
                    }
                });
            }
        } else {
            mProgressDialog = DialogUtils.showProgressDialog(context, null, StringUtils.getString(context, R.string.check_in_sending_photo), true);
        }
    }

    public class ImageAdapter extends BaseAdapter {
        LayoutInflater inflater;

        ImageAdapter() {
            inflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return presenter.getLstPhotoPath().size();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            String path;
            if (row == null) {
                row = inflater.inflate(R.layout.adapter_grid_image, parent, false);
                GridViewHolder holder = new GridViewHolder();
                holder.imageView = (SquareImageView) row.findViewById(R.id.img_Check_In);
                row.setTag(holder);
            }
            final GridViewHolder holder = (GridViewHolder) row.getTag();

            if (position == getCount() - 1) {
                Picasso.with(context).load(R.drawable.btn_add).into(holder.imageView);
                return row;
            } else {
                try {
                    path = presenter.getLstPhotoPath().get(position);
                    Picasso.with(context).load(new File(path)).noFade().centerCrop().resize(200, 200).error(R.drawable.no_media).into(holder.imageView);
                } catch (Exception e) {
                    Log.e("Exception", e.getMessage(),e);
                }
            }
            return row;
        }
    }

    class GridViewHolder {
        SquareImageView imageView;
    }
}
