package com.viettel.dms.ui.fragment;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.viettel.dms.R;
import com.viettel.dms.helper.GooglePlayUtils;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.dialog.ImageDialog;
import com.viettel.dms.helper.layout.ExpandableHeightGridView;
import com.viettel.dms.helper.layout.GeneralSwipeRefreshLayout;
import com.viettel.dms.helper.layout.SquareImageView;
import com.viettel.dms.helper.network.NetworkErrorDialog;
import com.viettel.dms.ui.activity.BaseActivity;
import com.viettel.dmsplus.sdk.MainEndpoint;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.auth.OAuthSession;
import com.viettel.dmsplus.sdk.models.CheckInDto;
import com.viettel.dmsplus.sdk.models.UserInfo;
import com.viettel.dmsplus.sdk.network.RequestCompleteCallback;
import com.viettel.dmsplus.sdk.network.SdkAsyncTask;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CheckInHistoryDetailFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    CheckInDto data;
    String checkInID;
    String title;
    @Bind(R.id.swipe_refresh)
    GeneralSwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.ll_View)
    LinearLayout llView;
    @Bind(R.id.google_map)
    MapView mMapView;
    @Bind(R.id.tv_Note)
    TextView tvNote;
    @Bind(R.id.grid_photos)
    ExpandableHeightGridView gridView;
    ImageAdapter imageAdapter;
    GoogleMap mGoogleMap;
    BitmapDescriptor markerIcon;
    private Marker locationMaker;
    private boolean loading = false;
    private SdkAsyncTask<?> refreshTask;

    public CheckInHistoryDetailFragment() {
        // Required empty public constructor
    }

    public static CheckInHistoryDetailFragment newInstance(String id, String dateStr) {
        CheckInHistoryDetailFragment fragment = new CheckInHistoryDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, id);
        args.putString(ARG_PARAM2, dateStr);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            checkInID = getArguments().getString(ARG_PARAM1);
            title = getArguments().getString(ARG_PARAM2);
        }
        imageAdapter = new ImageAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_in_history_detail, container, false);
        ButterKnife.bind(this, view);
        setTitle(title);
        mMapView.onCreate(savedInstanceState);
        gridView.setExpanded(true);
        gridView.setAdapter(imageAdapter);
        gridView.setOnItemClickListener(onItemClickListener);

        swipeRefreshLayout.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                ((BaseActivity) getActivity()).closeSoftKey();
                return ViewCompat.canScrollVertically(llView, -1);
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(loading);
                swipeRefreshLayout.setOnRefreshListener(CheckInHistoryDetailFragment.this);
                checkIfHaveData();
            }
        });
        return view;
    }

    private void checkIfHaveData() {
        if (!loading) {
            if (data == null) {
                loading = true;
                swipeRefreshLayout.setRefreshing(true);
                onRefresh();
            } else {
                rebindData();
            }
        }
    }

    private void rebindData() {
        if (data == null) return;
        tvNote.setText(data.getNote());
        imageAdapter.notifyDataSetChanged();
        if (data.getLocation() != null) {
            zoomToMyLocation(data.getLocation().getLatitude(), data.getLocation().getLongitude());
        }
    }

    @Override
    public void onRefresh() {
        refreshTask = MainEndpoint
                .get()
                .requestCheckInDetail(checkInID)
                .executeAsync(mCallback);
    }

    private RequestCompleteCallback<CheckInDto> mCallback = new RequestCompleteCallback<CheckInDto>() {
        @Override
        public void onSuccess(CheckInDto response) {
            data = response;
            rebindData();
        }

        @Override
        public void onError(SdkException info) {
            NetworkErrorDialog.processError(context, info);
        }

        @Override
        public void onFinish(boolean canceled) {
            refreshTask = null;
            loading = false;
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                    swipeRefreshLayout.setEnabled(false);
                }
            });
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        if (GooglePlayUtils.checkPlayService(context, getActivity())) {
            MapsInitializer.initialize(this.getActivity());
            setUpMapIfNeeded();
        }
    }

    private void setUpMapIfNeeded() {
        if (mGoogleMap == null) {
            UserInfo userInfo = OAuthSession.getDefaultSession().getUserInfo();
            double defaultLatitude = userInfo.getLocation().getLatitude() != 0 ? userInfo.getLocation().getLatitude() : HardCodeUtil.Location.defaultLast;
            double defaultLongitude = userInfo.getLocation().getLongitude() != 0 ? userInfo.getLocation().getLongitude() : HardCodeUtil.Location.defaultLong;
            mGoogleMap = mMapView.getMap();
            markerIcon = BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.marker_icon));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(defaultLatitude, defaultLongitude), 14.0f));
        }
        try {
            mGoogleMap.setMyLocationEnabled(false);
            if (data.getLocation() != null)
                zoomToMyLocation(data.getLocation().getLatitude(), data.getLocation().getLongitude());
        } catch (Exception ex) {
            Log.e("Exception", ex.getMessage(),ex);
        }
    }

    private void zoomToMyLocation(double latitude, double longitude) {
        try {
            mGoogleMap.clear();
            LatLng newLocation = new LatLng(latitude, longitude);
            locationMaker = mGoogleMap.addMarker(new MarkerOptions().position(newLocation).icon(markerIcon).draggable(false));
            locationMaker.showInfoWindow();
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 18));
        } catch (Exception ex) {
            Log.e("Exception", ex.getMessage(),ex);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String photoId = data.getPhotos()[position];
            ImageDialog imgDialog = new ImageDialog(context, MainEndpoint.get().getImageURL(photoId), false);
            imgDialog.show();
        }
    };


    public class ImageAdapter extends BaseAdapter {
        LayoutInflater inflater;

        ImageAdapter() {
            inflater = LayoutInflater.from(context);
        }

        public int getCount() {
            if (data == null || data.getPhotos() == null) {
                return 0;
            }
            return data.getPhotos().length;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = inflater.inflate(R.layout.adapter_grid_image, parent, false);
                GridViewHolder holder = new GridViewHolder();
                holder.imageView = (SquareImageView) row.findViewById(R.id.img_Check_In);
                row.setTag(holder);
            }
            final GridViewHolder holder = (GridViewHolder) row.getTag();
            final String path = data.getPhotos()[position];
            holder.imageView.post(new Runnable() {
                @Override
                public void run() {
                    Picasso.with(context).load(MainEndpoint.get().getImageURL(path)).centerCrop().resize(200, 200).noFade().error(R.drawable.no_media).into(holder.imageView);
                }
            });
            return row;
        }
    }

    class GridViewHolder {
        SquareImageView imageView;
    }
}
