package com.viettel.dms.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.viettel.dms.R;
import com.viettel.dms.helper.DateTimeUtils;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.layout.EndlessListAdapterWrapper;
import com.viettel.dms.helper.layout.ViewEmptyStateLayout;
import com.viettel.dms.helper.network.NetworkErrorDialog;
import com.viettel.dmsplus.sdk.MainEndpoint;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CheckInDto;
import com.viettel.dmsplus.sdk.models.CheckInListResult;
import com.viettel.dmsplus.sdk.network.RequestCompleteCallback;
import com.viettel.dmsplus.sdk.network.SdkAsyncTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CheckInHistoryFragment extends BaseFragment {

    @Bind(R.id.lst_Check_In_History)
    ListView lstCheckInHistory;
    @Bind(R.id.view_State)
    ViewEmptyStateLayout viewEmptyStateLayout;
    private int viewState = ViewEmptyStateLayout.VIEW_STATE_NORMAL;
    LayoutInflater layoutInflater;

    private List<CheckInDto> loadData = new ArrayList<>();
    CheckInEndlessAdapter mCheckInEndlessAdapter;
    private SdkAsyncTask<?> mLoadDataTask;

    public CheckInHistoryFragment() {
        // Required empty public constructor
    }

    public static CheckInHistoryFragment newInstance() {
        return new CheckInHistoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutInflater = LayoutInflater.from(context);
        mCheckInEndlessAdapter = new CheckInEndlessAdapter(context, loadData, 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_in_history, container, false);
        setTitleResource(R.string.check_in_history);
        ButterKnife.bind(this, view);
        lstCheckInHistory.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        lstCheckInHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                CheckInDto item = mCheckInEndlessAdapter.getInnerAdapter().visibleData.get(position);
                CheckInHistoryDetailFragment fragment = CheckInHistoryDetailFragment.newInstance(item.getId(), DateTimeUtils.formatDateAndTime(item.getCreatedTime()));
                replaceCurrentFragment(fragment);
            }
        });
        lstCheckInHistory.setAdapter(mCheckInEndlessAdapter);
        viewEmptyStateLayout.updateViewState(viewState);
        return view;
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    public void processToGetProductList(final int page) {
        mLoadDataTask = MainEndpoint
                .get()
                .requestCheckInHistory(page, HardCodeUtil.LOAD_ITEMS_PER_PAGE)
                .executeAsync(refreshCallback);
    }

    private RequestCompleteCallback<CheckInListResult> refreshCallback = new RequestCompleteCallback<CheckInListResult>() {
        @Override
        public void onSuccess(CheckInListResult data) {
            if (data.getItems() != null) {
                viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_NORMAL);
                loadData.clear();
                loadData = new ArrayList<>(Arrays.asList(data.getItems()));
                mCheckInEndlessAdapter.appendData(loadData);
            }
            if (data.getItems() == null || data.getItems().length == 0) {
                if (mCheckInEndlessAdapter.page == 1) {
                    viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_EMPTY_CHECK_IN);
                }
                mCheckInEndlessAdapter.stopAppending();
            }
        }

        @Override
        public void onError(SdkException ex) {
            loadData.clear();
            mCheckInEndlessAdapter.showRetryView();
            NetworkErrorDialog.processError(context, ex);
        }

        @Override
        public void onFinish(boolean canceled) {
            mLoadDataTask = null;
        }
    };

    class CheckInEndlessAdapter extends EndlessListAdapterWrapper {

        private int page = 1;

        public CheckInEndlessAdapter(Context ct, List<CheckInDto> list, int page) {
            super(ct, new InnerCheckInListAdapter(context, R.layout.view_list_pending, list),
                    R.layout.view_list_pending, R.layout.view_list_retry, R.id.btnRetry);
            this.page = page;
            this.setRunInBackground(false);
        }

        @Override
        protected boolean cacheInBackground() throws Exception {
            processToGetProductList(page);
            return true;
        }

        @Override
        protected void appendCachedData() {

        }

        // If you in search context, your requested page number increases if and only if you got not null result
        protected void appendData(List<CheckInDto> appendData) {
            InnerCheckInListAdapter wrappedAdapter = (InnerCheckInListAdapter) getWrappedAdapter();
            wrappedAdapter.append(appendData);
            if (appendData.size() != 0)
                page++;
            onDataReady();
        }

        public InnerCheckInListAdapter getInnerAdapter() {
            return (InnerCheckInListAdapter) getWrappedAdapter();
        }
    }

    class InnerCheckInListAdapter extends ArrayAdapter<CheckInDto> {

        List<CheckInDto> visibleData = new ArrayList<>();

        public InnerCheckInListAdapter(Context context, int resource, List<CheckInDto> objects) {
            super(context, resource, objects);
            visibleData.addAll(objects);
        }

        public void append(List<CheckInDto> appendData) {
            visibleData.addAll(appendData);
            notifyDataSetChanged();
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = layoutInflater.inflate(R.layout.adapter_check_in_history, parent, false);
                final EndlessListViewHolder viewHolder = new EndlessListViewHolder();
                viewHolder.tvTime = (TextView) row.findViewById(R.id.tv_Time);
                viewHolder.tvNote = (TextView) row.findViewById(R.id.tv_Note);
                row.setTag(viewHolder);
            }

            EndlessListViewHolder vh = (EndlessListViewHolder) row.getTag();
            CheckInDto item = visibleData.get(position);
            vh.tvNote.setText(item.getNote());
            vh.tvTime.setText(DateTimeUtils.formatDateAndTime(item.getCreatedTime()));
            return row;
        }

        @Override
        public int getCount() {
            return visibleData.size();
        }

        class EndlessListViewHolder {
            TextView tvTime;
            TextView tvNote;
        }
    }
}
