package com.viettel.dms.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.viettel.dms.R;
import com.viettel.dms.presenter.CheckinOfResultPresenter;
import com.viettel.dms.presenter.DistributorListPresenter;
import com.viettel.dms.ui.activity.BaseActivity;
import com.viettel.dms.ui.iview.ICheckinOfResultSpinner;
import com.viettel.dms.ui.iview.IDistributorListview;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CategorySimple;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfToday;
import com.viettel.dmsplus.sdk.models.SaleStatisticToday;
import com.viettel.dmsplus.sdk.models.UserSimple;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckinOfDayResultFragment extends BaseFragment implements ICheckinOfResultSpinner {

    //Constant
    public static final String KEY_DISTRIBUTORID= "KEY_DISTRIBUTORID";
    public static final String KEY_SALEMANID= "KEY_SALEMANID";


    //find and init control
    @Bind(R.id.spinDistributor)
    Spinner spinDistributor;
    @Bind(R.id.spinSale)
    Spinner spinSale;

    @Bind(R.id.row_checkin)
    View rowCheckin;
    TextView txtCheckinTitle;
    TextView txtCheckinResult;


    @Bind(R.id.row_checkin_wrong_position)
    View rowWrongPosition;
    TextView txtWrongPositionTitle;
    TextView txtWrongPositionResult;

    @Bind(R.id.row_time_not_enough)
    View rowTimeNotEnough;
    TextView txtTimeNotEnoughTitle;
    TextView txtTimeNotEnoughResult;


    @Bind(R.id.row_view_detail)
    View rowViewDetail;
    TextView txtViewDetailTitle;


    private String supId;
    private String saleId;


    private LayoutInflater layoutInflater;
    private CheckinOfResultPresenter checkinOfResultPresenter;

    ImageView imgCheckinICon, imgWrongPostionIcon, imgTimeNotEnoughIcon, imgViewDetailIcon;

    private final static int spinnerResouceDropdownId = android.R.layout.simple_spinner_dropdown_item;
    MySpinnerAdapter distributorAdapter;
    MyUserSpinnerAdapter salesmanAdapter;

    List<CategorySimple> lstDistributor;
    List<UserSimple> lstSaleman;

    public CheckinOfDayResultFragment() {
        // Required empty public constructor
    }


    public static CheckinOfDayResultFragment newInstance() {
        CheckinOfDayResultFragment fragment = new CheckinOfDayResultFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.layoutInflater = getLayoutInflater(savedInstanceState);
        setHasOptionsMenu(true);

        checkinOfResultPresenter = new CheckinOfResultPresenter(this);
        checkinOfResultPresenter.processGetAllDistributors();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_checkin_of_day_result, container, false);
        ButterKnife.bind(this, view);
        setTitleResource(R.string.text_title_Report_progress_day);

        spinDistributor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                checkinOfResultPresenter.processGetSalemanOfSup(lstDistributor.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinSale.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 supId =  lstDistributor.get(spinDistributor.getSelectedItemPosition()).getId();
                 saleId = lstSaleman.get(position).getId();
                checkinOfResultPresenter.getCheckinResultOfToday(supId,saleId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //set icon for row
        setIconAndTitleForRow();

        //find text result
        txtCheckinResult = (TextView) rowCheckin.findViewById(R.id.txtNumber);
        txtWrongPositionResult= (TextView) rowWrongPosition.findViewById(R.id.txtNumber);
        txtTimeNotEnoughResult = (TextView) rowTimeNotEnough.findViewById(R.id.txtNumber);

        rowViewDetail.setClickable(true);
        rowViewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity baseActivity = (BaseActivity) getActivity();
                CustomerListCheckinResultTodayFragment fragment = CustomerListCheckinResultTodayFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString(KEY_DISTRIBUTORID, supId);
                bundle.putString(KEY_SALEMANID, saleId);
                fragment.setArguments(bundle);
                baseActivity.replaceCurrentFragment(fragment);


            }
        });


        return view;


    }

    //set icon for row
    private void setIconAndTitleForRow() {


        //find image
        imgCheckinICon = (ImageView) rowCheckin.findViewById(R.id.imgIcon);
        imgWrongPostionIcon = (ImageView) rowWrongPosition.findViewById(R.id.imgIcon);
        imgTimeNotEnoughIcon = (ImageView) rowTimeNotEnough.findViewById(R.id.imgIcon);
        imgViewDetailIcon = (ImageView) rowViewDetail.findViewById(R.id.imgIcon);

        //find title

        txtCheckinTitle = (TextView) rowCheckin.findViewById(R.id.txtLabel);
        txtWrongPositionTitle = (TextView) rowWrongPosition.findViewById(R.id.txtLabel);
        txtTimeNotEnoughTitle = (TextView) rowTimeNotEnough.findViewById(R.id.txtLabel);
        txtViewDetailTitle = (TextView) rowViewDetail.findViewById(R.id.txtLabel);

        TextView txtHiden = (TextView) rowViewDetail.findViewById(R.id.txtNumber);
        txtHiden.setVisibility(View.GONE);

        //set image
        imgViewDetailIcon.setImageResource(R.drawable.informationicon);
        imgTimeNotEnoughIcon.setImageResource(R.drawable.clockicon);
        imgWrongPostionIcon.setImageResource(R.drawable.locationicon);
        imgCheckinICon.setImageResource(R.drawable.personicon);

        //set textview
        txtCheckinTitle.setText(R.string.navigation_drawer_item_visit);
        txtTimeNotEnoughTitle.setText(R.string.text_Visit_error_Duration);
        txtWrongPositionTitle.setText(R.string.text_Visit_error_Position);
        txtViewDetailTitle.setText(R.string.home_page_view_detail);

    }


    private  void setDataForRow(SaleStatisticToday saleStatisticToday){
        txtCheckinResult.setText(saleStatisticToday.getVisit().getActual() +"/" + saleStatisticToday.getVisit().getPlan());
        txtWrongPositionResult.setText(String.valueOf(saleStatisticToday.getVisitErrorPosition().getActual()));
        txtTimeNotEnoughResult.setText(String.valueOf(saleStatisticToday.getVisitErrorDuration().getActual()));

    }

    @Override
    public void getDistributorListSuccess(List<CategorySimple> data) {
        this.lstDistributor = data;
        distributorAdapter = new MySpinnerAdapter(getActivity(),data,spinnerResouceDropdownId);
        distributorAdapter.setDropDownViewResource(spinnerResouceDropdownId);
        spinDistributor.setAdapter(distributorAdapter);


    }

    @Override
    public void getDistributorListError(SdkException info) {

    }

    @Override
    public void getSalemanListSuccess(List<UserSimple> data) {

        this.lstSaleman = data;
        salesmanAdapter = new MyUserSpinnerAdapter(getActivity(),data,spinnerResouceDropdownId);
        salesmanAdapter.setDropDownViewResource(spinnerResouceDropdownId);
        spinSale.setAdapter(salesmanAdapter);
    }


    @Override
    public void getSalemanListError(SdkException info) {

    }

    @Override
    public void getCheckinResultOfToday(SaleStatisticToday saleStatisticToday) {

        //SaleStatisticToday saleStatisticTodayTmp = saleStatisticToday;
        this.setDataForRow(saleStatisticToday);


    }

    @Override
    public void getCheckinResultOfTodayError(SdkException info) {

    }

    @Override
    public void getListCustomerSuccess() {

    }

    @Override
    public void getListCustomerError(SdkException info) {

    }

    @Override
    public void finishTask() {

    }


    //spinner adapter
    public class MySpinnerAdapter extends ArrayAdapter<CategorySimple> {
        private List<CategorySimple> list;
        private Context context;
        int resourceId;
        LayoutInflater inflater;

        public MySpinnerAdapter(Context context, List<CategorySimple> list, int resourceId) {
            super(context, resourceId, list);

            this.context = context;
            this.list = list;
            this.resourceId = resourceId;
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        @Override
        public int getCount() {
            //return super.getCount();
            if (list == null) return 0;
            else return list.size();
        }

        @Nullable
        @Override
        public CategorySimple getItem(int position) {
            return list.get(position);
        }
        //
        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //return super.getView(position, convertView, parent);
            ViewHolder viewHolder;
            viewHolder = null;
            convertView = inflater.inflate(resourceId, null);

            if(viewHolder == null){
                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) convertView.findViewById(android.R.id.text1);
                convertView.setTag(viewHolder);
            }
            else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            //set data
            viewHolder.textView.setText(list.get(position).getName());

            return  convertView;


        }


        private class ViewHolder {
            public android.widget.TextView textView;
        }

    }


    public class MyUserSpinnerAdapter extends ArrayAdapter<UserSimple> {
        private List<UserSimple> list;
        private Context context;
        int resourceId;
        LayoutInflater inflater;

        public MyUserSpinnerAdapter(Context context, List<UserSimple> list, int resourceId) {
            super(context, resourceId, list);

            this.context = context;
            this.list = list;
            this.resourceId = resourceId;
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        @Override
        public int getCount() {
            //return super.getCount();
            if (list == null) return 0;
            else return list.size();
        }

        @Nullable
        @Override
        public UserSimple getItem(int position) {
            return list.get(position);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //return super.getView(position, convertView, parent);
            ViewHolder viewHolder;
            viewHolder = null;
            convertView = inflater.inflate(resourceId, null);

            if(viewHolder == null){
                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) convertView.findViewById(android.R.id.text1);
                convertView.setTag(viewHolder);
            }
            else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            //set data
            viewHolder.textView.setText(list.get(position).getFullname());

            return  convertView;


        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

          ViewHolder viewHolder;
            viewHolder = null;
            //convertView = inflater.inflate(resourceId, null);
            convertView =  super.getDropDownView(position, convertView, parent);

            if(viewHolder == null){
                viewHolder = new ViewHolder();
                viewHolder.textView = (TextView) convertView.findViewById(android.R.id.text1);
                viewHolder.textView.setMinimumHeight(35);
                convertView.setTag(viewHolder);
            }
            else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            //set data
            viewHolder.textView.setText(list.get(position).getFullname());
            return  convertView;

            //return super.getDropDownView(position, convertView, parent);
        }

        private class ViewHolder {
            public android.widget.TextView textView;
        }

    }

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();

    }

    @Override
    public void onResume() {
        checkinOfResultPresenter.processGetAllDistributors();
        super.onResume();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.mnu_fragment_checkin_today,menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.mnu_refresh){
            checkinOfResultPresenter.processGetAllDistributors();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
