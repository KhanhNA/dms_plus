package com.viettel.dms.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.viettel.dms.R;
import com.viettel.dms.helper.DateTimeUtils;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.NumberFormatUtils;
import com.viettel.dms.presenter.CustomerCheckinOfTodayPresenter;
import com.viettel.dms.ui.activity.BaseActivity;
import com.viettel.dms.ui.iview.ICustomerCheckinOfToday;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfToday;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfTodayResult;
import com.viettel.dmsplus.sdk.utils.MethodUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.datatype.Duration;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckinTodayDetailFragment extends BaseFragment implements ICustomerCheckinOfToday {

    @Bind(R.id.txtAddress)
    TextView txtAddress;
    @Bind(R.id.txtCode)
    TextView txtCode;

    //find and bind view
    @Bind(R.id.firstrow)
    View firstRow;
    @Bind(R.id.secondrow)
    View secondRow;

    @Bind(R.id.row_saleman)
    View rowSaleman;
    @Bind(R.id.row_time)
    View rowTime;
    @Bind(R.id.row_distance)
    View rowDistance;
    @Bind(R.id.row_order)
    View rowOrder;
    @Bind(R.id.row_order_status)
    View rowOrderStatus;

    //icon for row
    ImageView imgFistRow;
    ImageView imgSecondRow;

    ImageView imgSaleman;
    ImageView imgTime;
    ImageView imgDistance;
    ImageView imgOder;
    ImageView imgOderStatus;

    //label for row
    TextView txtFistRowLable;
    TextView txtSecondRowLable;

    TextView txtSalemanLable;
    TextView txtTimeLable;
    TextView txtDistanceLable;
    TextView txtOderLable;
    TextView txtOderStatusLable;

    //right textview
    TextView txtRightFistRowContent;
    TextView txtRightSecondRowContent;

    TextView txtRightSalemanContent;
    TextView txtRightTimeContent;
    TextView txtRightDistanceContent;
    TextView txtRightOderContent;
    TextView txtRightOderStatusContent;

    private CustomerCheckinOfToday customerCheckinOfToday = null;

    public CheckinTodayDetailFragment() {
        // Required empty public constructor
    }

    CustomerCheckinOfTodayPresenter presenter;
    private String customerId = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            customerId = getArguments().getString(CustomerListCheckinResultTodayFragment.KEY_CUSTOMERID);
            Toast.makeText(context, "customerId  is " + customerId, Toast.LENGTH_LONG).show();
        }

        setHasOptionsMenu(true);
        presenter = new CustomerCheckinOfTodayPresenter(this);
        presenter.getCustomerCheckinOfToday(customerId);

    }


    public static CheckinTodayDetailFragment newInstance() {
        CheckinTodayDetailFragment fragment = new CheckinTodayDetailFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_checkin_today_detail, container, false);
        ButterKnife.bind(this, view);
        setTitleResource(R.string.navigation_drawer_item_visit);

        //find imageview
        imgFistRow = (ImageView) firstRow.findViewById(R.id.imgIcon);
        imgSecondRow = (ImageView) secondRow.findViewById(R.id.imgIcon);

        imgSaleman = (ImageView) rowSaleman.findViewById(R.id.imgIcon);
        imgTime = (ImageView) rowTime.findViewById(R.id.imgIcon);
        imgDistance = (ImageView) rowDistance.findViewById(R.id.imgIcon);
        imgOder = (ImageView) rowOrder.findViewById(R.id.imgIcon);
        imgOderStatus = (ImageView) rowOrderStatus.findViewById(R.id.imgIcon);

        setImageIconForRow();
        //find textview
        txtFistRowLable = (TextView) firstRow.findViewById(R.id.txtLabel);
        txtSecondRowLable = (TextView) secondRow.findViewById(R.id.txtLabel);

        txtSalemanLable = (TextView) rowSaleman.findViewById(R.id.txtLabel);
        txtTimeLable = (TextView) rowTime.findViewById(R.id.txtLabel);
        txtDistanceLable = (TextView) rowDistance.findViewById(R.id.txtLabel);
        txtOderLable = (TextView) rowOrder.findViewById(R.id.txtLabel);
        txtOderStatusLable = (TextView) rowOrderStatus.findViewById(R.id.txtLabel);
        setTextLabelForRow();

        //right content
        txtRightFistRowContent = (TextView) firstRow.findViewById(R.id.txtRightContent);
        txtRightSecondRowContent = (TextView) secondRow.findViewById(R.id.txtRightContent);

        txtRightSalemanContent = (TextView) rowSaleman.findViewById(R.id.txtRightContent);


        txtRightTimeContent = (TextView) rowTime.findViewById(R.id.txtRightContent);
        txtRightDistanceContent = (TextView) rowDistance.findViewById(R.id.txtRightContent);
        txtRightOderContent = (TextView) rowOrder.findViewById(R.id.txtRightContent);
        txtRightOderStatusContent = (TextView) rowOrderStatus.findViewById(R.id.txtRightContent);


        rowOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (customerCheckinOfToday == null || TextUtils.isEmpty(customerCheckinOfToday.getCode())) {
                    Toast.makeText(context, R.string.txt_no_has_order, Toast.LENGTH_LONG).show();
                } else {
                    //move fragment and put data
                    BaseActivity baseActivity = (BaseActivity) getActivity();
                    ReviewPurchaseOrderFragment fragment = ReviewPurchaseOrderFragment.newInstance(customerCheckinOfToday.getId(), customerCheckinOfToday.getCustomer().getName(), customerCheckinOfToday.getApproveStatus() == HardCodeUtil.OrderStatus.REJECTED);
                    Bundle bundle = new Bundle();
                    bundle.putString(ReviewPurchaseOrderFragment.PARAM_ORDER_ID, customerCheckinOfToday.getId());
                    bundle.putString(ReviewPurchaseOrderFragment.PARAM_ORDER_CUSTOMER_NAME, customerCheckinOfToday.getCustomer().getName());
                    bundle.putBoolean(ReviewPurchaseOrderFragment.PARAM_ORDER_IS_REJECTED, customerCheckinOfToday.getApproveStatus() == HardCodeUtil.OrderStatus.REJECTED);
                    fragment.setArguments(bundle);
                    baseActivity.replaceCurrentFragment(fragment);
                }


            }
        });

        return view;
    }

    private void setTextLabelForRow() {
        txtFistRowLable.setText(R.string.txt_customer_type);
        txtSecondRowLable.setText(R.string.txt_erea);

        txtSalemanLable.setText(R.string.txt_sale_man_name);
        txtTimeLable.setText(R.string.txt_time);
        txtDistanceLable.setText(R.string.txt_distance);
        txtOderLable.setText(R.string.txt_order);
        txtOderStatusLable.setText(R.string.txt_order_status);

    }

    private void setImageIconForRow() {
        imgFistRow.setImageResource(R.drawable.customertyepicon);
        imgSecondRow.setImageResource(R.drawable.areaicon);

        imgSaleman.setImageResource(R.drawable.salemanicon);
        imgTime.setImageResource(R.drawable.timeicon);
        imgDistance.setImageResource(R.drawable.distanceicon);
        imgOder.setImageResource(R.drawable.ordericon);
        imgOderStatus.setVisibility(View.GONE);

    }


    @Override
    public void getListCustomerSuccess(CustomerCheckinOfTodayResult data) {

    }

    @Override
    public void getCustomerCheckinDetailOfToday(CustomerCheckinOfToday data) {
        customerCheckinOfToday = data;
        this.setData(data);

    }

    private void setData(CustomerCheckinOfToday data) {
        txtAddress.setText(data.getCustomer().getName());
        txtCode.setText(data.getCustomer().getCode());

        txtRightFistRowContent.setText(data.getCustomer().getCustomerType().getName());
        txtRightSecondRowContent.setText(data.getCustomer().getArea().getName());

        txtRightSalemanContent.setText(data.getSalesman().getFullname());
        String timeStart = DateTimeUtils.formatDate(data.getStartTime(), "HH:mm:ss");
        String timeEnd = DateTimeUtils.formatDate(data.getEndTime(), "HH:mm:ss");

        //long diff = DateTimeUtils.formatDate(data.getStartTime(), "HH:mm:ss") - DateTimeUtils.formatDate(data.getEndTime(), "HH:mm:ss");
        String minus = "";
        try {
            minus = MethodUtils.minusDate(MethodUtils.getDatewithformat(timeStart), MethodUtils.getDatewithformat(timeEnd));
        } catch (ParseException e) {
            e.printStackTrace();
            minus = "";
        }
        txtRightTimeContent.setText(timeStart + " - " + timeEnd + minus);

        if (data.getDistance() == null || data.getDistance() < 0) {
            txtRightDistanceContent.setText(R.string.txt_no_infi);
        } else {
            txtRightDistanceContent.setText(String.valueOf(NumberFormatUtils.formatNumber(new BigDecimal(MethodUtils.round(data.getDistance() * 1000, 0)))) + " m");
        }



        if (!data.isHasOrder()) {
            txtRightOderContent.setText(R.string.txt_no_have);
        } else {
            txtRightOderContent.setText(NumberFormatUtils.formatNumber(data.getSubTotal()));
        }

        if (data.isHasOrder()) {
            if (data.getApproveStatus() == HardCodeUtil.OrderStatus.ACCEPTED) {
                txtRightOderStatusContent.setText(R.string.txt_acepted);
            } else if (data.getApproveStatus() == HardCodeUtil.OrderStatus.WAITING) {
                txtRightOderStatusContent.setText(R.string.txt_wait_approve);
            } else {
                txtRightOderStatusContent.setText(R.string.txt_cancel);
            }
        } else {
            txtRightOderStatusContent.setText("");
        }


    }

    @Override
    public void getListCustomerError(SdkException info) {

    }

    @Override
    public void finishTask() {

    }


    @Override
    public void onResume() {
        presenter.getCustomerCheckinOfToday(customerId);
        super.onResume();

    }


}
