package com.viettel.dms.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.viettel.dms.R;
import com.viettel.dms.helper.DialogUtils;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.StringUtils;
import com.viettel.dms.helper.layout.DividerItemDecoration;
import com.viettel.dms.helper.layout.GeneralSwipeRefreshLayout;
import com.viettel.dms.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.viettel.dms.helper.layout.SetTextUtils;
import com.viettel.dms.helper.layout.ViewEmptyStateLayout;
import com.viettel.dms.helper.network.NetworkErrorDialog;
import com.viettel.dms.presenter.DistributorListPresenter;
import com.viettel.dms.ui.activity.BaseActivity;
import com.viettel.dms.ui.activity.OrderActivity;
import com.viettel.dms.ui.activity.OrderTBActivity;
import com.viettel.dms.ui.iview.IDistributorListview;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.auth.OAuthSession;
import com.viettel.dmsplus.sdk.models.CategorySimple;
import com.viettel.dmsplus.sdk.models.CategorySimpleResult;
import com.viettel.dmsplus.sdk.models.CustomerForVisit;
import com.viettel.dmsplus.sdk.models.CustomerListResult;
import com.viettel.dmsplus.sdk.models.UserInfo;
import com.viettel.dmsplus.sdk.models.UserSimple;
import com.viettel.dmsplus.sdk.models.UserSimpleResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class CustomerListAllForSupervisorFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener, IDistributorListview {

    @Bind(R.id.swipe_refresh_customer_list)
    GeneralSwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.view_State)
    ViewEmptyStateLayout viewEmptyStateLayout;
    @Nullable
    @Bind(R.id.sub_bar)
    Toolbar mToolbar;


    @Bind(R.id.spinner_distributor)
    Spinner spinnerDistributor;

    @Bind(R.id.spinner_saleman)
    Spinner spinnerSaleman;


    private LayoutInflater layoutInflater;
    private RecyclerView.LayoutManager layoutManager;
    private boolean loading = false;

    private SearchView searchView;
    private MenuItem searchItem;

    private String filterString = "";
    private int viewState = ViewEmptyStateLayout.VIEW_STATE_NORMAL;

    private DistributorListPresenter presenter;
    DistributorSpinnerAdaper distributorAdapter;

    SalesmanSpinnerAdaper salesmanAdapter;

    private CategorySimple distributorSelected = null;
    private CustomerListAllAdapter customerAdapter;


    public CustomerListAllForSupervisorFragment() {
        // Required empty public constructor
    }


    public static CustomerListAllForSupervisorFragment newInstance() {
        CustomerListAllForSupervisorFragment fragment = new CustomerListAllForSupervisorFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.layoutInflater = getLayoutInflater(savedInstanceState);
        setHasOptionsMenu(true);

        customerAdapter = new CustomerListAllAdapter();

        this.presenter = new DistributorListPresenter(this);
        this.presenter.processGetAllDistributors();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_customer_list_all_supervisor, container, false);
        ButterKnife.bind(this, view);

        spinnerDistributor.setMinimumHeight(60);
        spinnerDistributor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(((DistributorSpinnerAdaper) parent.getAdapter()).visibleData.size() > 0 && ((DistributorSpinnerAdaper) parent.getAdapter()).visibleData.size() > position){
                    distributorSelected = ((DistributorSpinnerAdaper) parent.getAdapter()).visibleData.get(position);
                    presenter.processGetSalemanOfSup(presenter.getSpDistributorTypeData().getList()[position].getId());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerSaleman.setMinimumHeight(60);
        /*spinnerSaleman.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (((SalesmanSpinnerAdaper) parent.getAdapter()).visibleData.size() > 0 && ((SalesmanSpinnerAdaper) parent.getAdapter()).visibleData.size() > position) {
                    UserSimple userSimple = ((SalesmanSpinnerAdaper) parent.getAdapter()).visibleData.get(position);
                    Toast.makeText(getActivity(), "saleman id :" + userSimple.getId() + "", Toast.LENGTH_LONG).show();
                    presenter.processGetListCustomer(distributorSelected.getId(), userSimple.getId());
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.w("lsdlflds","onNothingSelected");
            }
        });*/

        if (getResources().getBoolean(R.bool.is_tablet) && (mToolbar != null)) {
            ((BaseActivity) getActivity()).setSupportActionBar(mToolbar);
            mToolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });
        }
        setTitleResource(R.string.navigation_drawer_item_customer_list);

        setPaddingLeft(getResources().getBoolean(R.bool.is_tablet) ? 104f : 72f);

        layoutManager = new LinearLayoutManager(context);


        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(customerAdapter);
        recyclerView.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false, getResources().getBoolean(R.bool.is_tablet) ? R.drawable.divider_list_104 :
                        R.drawable.divider_list_72));

        swipeRefreshLayout.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                ((BaseActivity) getActivity()).closeSoftKey();
                return ViewCompat.canScrollVertically(recyclerView, -1);
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(loading);
                swipeRefreshLayout.setOnRefreshListener(CustomerListAllForSupervisorFragment.this);
                checkIfHaveData();
            }
        });
        viewEmptyStateLayout.updateViewState(viewState);
        return view;


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //ButterKnife.unbind(this);
    }

    @Override
    public boolean onBackPressed() {
        getActivity().finish();
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_action_search, menu);
        searchItem = menu.findItem(R.id.action_search);

        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
            if (!StringUtils.isNullOrEmpty(filterString)) {
                searchView.setQuery(filterString, false);
                searchView.setIconified(false);
                searchView.post(new Runnable() {
                    @Override
                    public void run() {
                        ((BaseActivity) getActivity()).closeSoftKey();
                    }
                });
            }
            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    filterString = "";
                    customerAdapter.refreshData();
                    return false;
                }
            });

            MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    ((BaseActivity) getActivity()).closeSoftKey();
                    filterString = "";
                    customerAdapter.refreshData();
                    return true;
                }
            });
            MenuItemCompat.setActionView(searchItem, searchView);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }

    private void checkIfHaveData() {
        if (!loading) {
            if (presenter.getmDataNotFilter() == null) {
                loading = true;
                swipeRefreshLayout.setRefreshing(true);
                onRefresh();
            }
        }
    }

    @Override
    public void onRefresh() {
        //    presenter.processGetAllDistributors();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        filterString = query;
        //customerAdapter.setFilter(filterString);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    @Override
    public void getDistributorListSuccess() {

        if (distributorAdapter == null) {
            distributorAdapter = new DistributorSpinnerAdaper(context, presenter.getSpDistributorTypeData(), true, false);
            distributorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerDistributor.setAdapter(distributorAdapter);
        } else {
           distributorAdapter.refreshData(presenter.getSpDistributorTypeData().getList());
        }

    }

    @Override
    public void getSalemanListSuccess() {

       /* if (presenter.getSpSalemanTypeData() == null || presenter.getSpSalemanTypeData().getList().length == 0) {
            customerAdapter.visibleData.clear();
            customerAdapter.notifyDataSetChanged();
        }
*/
        salesmanAdapter = new SalesmanSpinnerAdaper(context, presenter.getSpSalemanTypeData(), true, false);
        salesmanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSaleman.setAdapter(salesmanAdapter);

      /*  if (salesmanAdapter == null) {
            salesmanAdapter = new SalesmanSpinnerAdaper(context, presenter.getSpSalemanTypeData(), true, false);
            salesmanAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSaleman.setAdapter(salesmanAdapter);
        } else {
            *//*salesmanAdapter.refreshData(presenter.getSpSalemanTypeData().getList());
            spinnerSaleman.post(new Runnable() {
                @Override
                public void run() {
                    //  spinnerSaleman.setSelection(0, true);
                }
            });*//*
        }
        *//*if (presenter.getSpSalemanTypeData() == null || presenter.getSpSalemanTypeData().getList().length == 0) {
            customerAdapter.visibleData.clear();
            customerAdapter.notifyDataSetChanged();
        }*/
    }


    private void processException(SdkException info) {
        NetworkErrorDialog.processError(context, info);
        viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_NETWORK_ERROR);
    }

    @Override
    public void getSalemanListError(SdkException info) {
        this.processException(info);
    }

    @Override
    public void getListCustomerSuccess() {
        CustomerListResult customerListResult = presenter.getSpCustomerTypeData();
        if (customerListResult.getList() == null || customerListResult.getList().length == 0) {
            customerAdapter.visibleData.clear();
            customerAdapter.notifyDataSetChanged();
        } else {
            customerAdapter.refreshData(Arrays.asList(customerListResult.getList()));
        }
    }

    @Override
    public void getListCustomerError(SdkException info) {
        this.processException(info);
    }

    @Override
    public void getDistributorListError(SdkException info) {
        this.processException(info);
    }

    @Override
    public void finishTask() {
        loading = false;
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    public CustomerForVisit getClickCustomer(int position) {
        if (customerAdapter == null || customerAdapter.visibleData == null) return null;
        else
            return customerAdapter.visibleData.get(position);

    }

    public void onListItemClick(int pos) {
        final CustomerForVisit customerInfo = getClickCustomer(pos);
        final UserInfo userInfo = OAuthSession.getDefaultSession().getUserInfo();
        if (userInfo != null && userInfo.isVanSales()) {
            DialogUtils.showSingleChoiceDialog(context, R.string.place_order_orders_choose_sale_type, R.array.sale_type, R.string.confirm_yes, R.string.confirm_cancel, userInfo.isVanSales() ? 1 : 0, new MaterialDialog.ListCallbackSingleChoice() {
                @Override
                public boolean onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                    Intent intent = new Intent(getActivity(), getResources().getBoolean(R.bool.is_tablet) ? OrderTBActivity.class : OrderActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("customerFromAll", customerInfo);
                    bundle.putBoolean("isVanSales", (i == 1));
                    bundle.putBoolean("isRecreateRejectedOrder", false);
                    bundle.putBoolean("goFromListCustomerAll", true);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    return false;
                }
            });
        } else {
            Intent intent = new Intent(getActivity(), getResources().getBoolean(R.bool.is_tablet) ? OrderTBActivity.class : OrderActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("customerFromAll", customerInfo);
            bundle.putBoolean("isVanSales", false);
            bundle.putBoolean("recreate", false);
            bundle.putBoolean("goFromListCustomerAll", true);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    class SalesmanSpinnerAdaper extends ArrayAdapter<CategorySimple> {
        Context context;
        public List<UserSimple> visibleData = new ArrayList<>();

        public SalesmanSpinnerAdaper(Context context, UserSimpleResult data, Boolean isSorted, Boolean isAddAll) {
            super(context, android.R.layout.simple_spinner_dropdown_item);
            this.context = context;

            if (data != null && data.getList() != null && data.getList().length > 0) {
                for (UserSimple cat : data.getList()) {
                    visibleData.add(cat);
                }
                if (isSorted) {
                    Collections.sort(visibleData, new Comparator<UserSimple>() {
                        @Override
                        public int compare(UserSimple t1, UserSimple t2) {
                            String en1 = StringUtils.getEngStringFromUnicodeString(t1.getFullname());
                            String en2 = StringUtils.getEngStringFromUnicodeString(t2.getFullname());
                            return en1.compareTo(en2);
                        }
                    });
                }

                if (isAddAll) {
                    UserSimple userSimple = new UserSimple();
                    userSimple.setFullname("--All--");
                    userSimple.setId("-1");
                    visibleData.add(0, userSimple);
                }


            }
        }

        private View getCustomView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null || !(row.getTag() instanceof CustomerListAllForSupervisorFragment.SpinnerViewHolder)) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                CustomerListAllForSupervisorFragment.SpinnerViewHolder holder = new CustomerListAllForSupervisorFragment.SpinnerViewHolder();
                holder.txt = (android.widget.TextView) row.findViewById(android.R.id.text1);
                row.setTag(holder);
            }
            if (row.getTag() instanceof CustomerListAllForSupervisorFragment.SpinnerViewHolder) {
                CustomerListAllForSupervisorFragment.SpinnerViewHolder holder = (CustomerListAllForSupervisorFragment.SpinnerViewHolder) row.getTag();
                if (visibleData.size() > 0 && visibleData.size() > position) {
                    UserSimple item = visibleData.get(position);
                    holder.txt.setText(item.getFullname());
                } else {
                    holder.txt.setText("");
                }

            }
            return row;
        }

        public void refreshData(UserSimple[] data) {
            visibleData.clear();
            notifyDataSetChanged();
            for (int i = 0; i < data.length; i++) {
                visibleData.add(data[i]);
            }
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public int getCount() {
            if (visibleData == null) return 0;
            else return visibleData.size();
        }

        public String getId(int pos) {
            if (visibleData.size() > 0 && visibleData.size() > pos)
                return visibleData.get(pos).getId();

            return "";
        }
    }

    class DistributorSpinnerAdaper extends ArrayAdapter<CategorySimple> {
        Context context;
        public List<CategorySimple> visibleData = new ArrayList<>();

        public DistributorSpinnerAdaper(Context context, CategorySimpleResult data, Boolean isSorted, Boolean isAddAll) {
            super(context, android.R.layout.simple_spinner_dropdown_item);
            this.context = context;

            if (data != null && data.getList() != null && data.getList().length > 0) {
                for (CategorySimple cat : data.getList()) {
                    visibleData.add(cat);
                }
                if (isSorted) {
                    Collections.sort(visibleData, new Comparator<CategorySimple>() {
                        @Override
                        public int compare(CategorySimple t1, CategorySimple t2) {
                            String en1 = StringUtils.getEngStringFromUnicodeString(t1.getName());
                            String en2 = StringUtils.getEngStringFromUnicodeString(t2.getName());
                            return en1.compareTo(en2);
                        }
                    });
                }

                if (isAddAll) {
                    CategorySimple categorySimple = new CategorySimple();
                    categorySimple.setName("--All--");
                    categorySimple.setId("-1");
                    visibleData.add(0, categorySimple);


                }


            }
        }

        private View getCustomView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null || !(row.getTag() instanceof CustomerListAllForSupervisorFragment.SpinnerViewHolder)) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
                CustomerListAllForSupervisorFragment.SpinnerViewHolder holder = new CustomerListAllForSupervisorFragment.SpinnerViewHolder();
                holder.txt = (android.widget.TextView) row.findViewById(android.R.id.text1);
                row.setTag(holder);
            }
            if (row.getTag() instanceof CustomerListAllForSupervisorFragment.SpinnerViewHolder) {
                CustomerListAllForSupervisorFragment.SpinnerViewHolder holder = (CustomerListAllForSupervisorFragment.SpinnerViewHolder) row.getTag();

                if (visibleData.size() > 0 && visibleData.size() > position) {
                    CategorySimple item = visibleData.get(position);
                    holder.txt.setText(item.getName());
                } else {
                    holder.txt.setText("");
                }

            }
            return row;
        }

        public void refreshData(CategorySimple[] data) {
            visibleData.clear();
            notifyDataSetChanged();
            for (int i = 0; i < data.length; i++) {
                visibleData.add(data[i]);
            }
            notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position, convertView, parent);
        }

        @Override
        public int getCount() {
            if (visibleData == null) return 0;
            else return visibleData.size();
        }

        @Nullable
        @Override
        public CategorySimple getItem(int position) {

            return super.getItem(position);
        }

        public String getId(int pos) {
            return visibleData.get(pos).getId();
        }
    }

    private static class SpinnerViewHolder {
        public android.widget.TextView txt;
    }


    /*customerAdapter for recycleviewwer*/
    private class CustomerListAllAdapter extends RecyclerView.Adapter {
        public List<CustomerForVisit> visibleData = new LinkedList<>();

        private void refreshData() {
            this.visibleData.clear();
            notifyDataSetChanged();
            if (presenter == null || presenter.getmCustomerDataNotFilter().isEmpty()) {
                viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_EMPTY_CUSTOMER);
            } else {
                viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_NORMAL);
                this.visibleData.addAll(presenter.getmCustomerDataNotFilter());
                Collections.sort(visibleData);
                //getRecycledViewPool().clear();
                notifyDataSetChanged();
            }
        }


        //duongnv18


        private void refreshData(List<CustomerForVisit> customerForVisitList) {
            this.visibleData.clear();
            notifyDataSetChanged();
            /*if (presenter == null || presenter.getmCustomerDataNotFilter().isEmpty()) {
                viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_EMPTY_CUSTOMER);
            } else {
                viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_NORMAL);
                this.visibleData.addAll(customerForVisitList);
                Collections.sort(visibleData);
                notifyDataSetChanged();
            }*/

            if (presenter == null || (customerForVisitList == null || customerForVisitList.size() <= 0)) {
                viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_EMPTY_CUSTOMER);
            } else {
                viewEmptyStateLayout.updateViewState(viewState = ViewEmptyStateLayout.VIEW_STATE_NORMAL);
                this.visibleData.addAll(customerForVisitList);
                Collections.sort(visibleData);
                notifyDataSetChanged();
            }


        }
        //end


        private void setFilter(String query) {
            try {
                visibleData.clear();
                notifyDataSetChanged();
                if (query == null || TextUtils.isEmpty(query)) {
                    refreshData();
                } else {
                    query = StringUtils.getEngStringFromUnicodeString(query).toLowerCase();
                    for (CustomerForVisit item : presenter.getmCustomerDataNotFilter()) {
                        String name = item.getName() != null ? StringUtils.getEngStringFromUnicodeString(item.getName()).toLowerCase() : null;
                        String address = item.getAddress() != null ? StringUtils.getEngStringFromUnicodeString(item.getAddress().toLowerCase()) : null;
                        if (name != null && name.contains(query)) {
                            visibleData.add(item);
                        } else if (address != null && address.contains(query)) {
                            visibleData.add(item);
                        }
                    }
                }
                Collections.sort(visibleData);
                viewEmptyStateLayout.updateViewState(viewState = visibleData.size() > 0 ? ViewEmptyStateLayout.VIEW_STATE_NORMAL : ViewEmptyStateLayout.VIEW_STATE_SEARCH_NOT_FOUND);
                notifyDataSetChanged();
            } catch (Exception e) {
                Log.e("Exception", e.getMessage(), e);
            }
        }

        View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemPosition = (int) view.getTag();
                if (itemPosition < 0 || itemPosition >= visibleData.size()) {
                    return;
                }
                //onListItemClick(itemPosition);
            }
        };

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.adapter_order_customer_list, parent, false);
            view.setOnClickListener(mOnClickListener);
            return new OrderCustomerListFragment.ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            OrderCustomerListFragment.ItemViewHolder vh = (OrderCustomerListFragment.ItemViewHolder) holder;
            vh.itemView.setTag(position);
            CustomerForVisit item = visibleData.get(position);
            char letter = java.lang.Character.toUpperCase(item.getName().charAt(0));
            vh.imgCircle.setImageResource(HardCodeUtil.getResourceIdColor(letter));
            vh.tvFirstLetter.setText(String.valueOf(letter));
            vh.tvCustomerName.setText(item.getName());
            SetTextUtils.setText(vh.tvAddress, item.getAddress());
            SetTextUtils.setText(vh.tvPhone, item.getMobile());
        }

        @Override
        public int getItemCount() {
            if (visibleData != null) {
                return visibleData.size();
            }
            return 0;
        }


    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.img_Circle)
        CircleImageView imgCircle;
        @Bind(R.id.tv_First_Letter)
        TextView tvFirstLetter;
        @Bind(R.id.tv_Customer_Name)
        TextView tvCustomerName;
        @Bind(R.id.tv_Address)
        TextView tvAddress;
        @Nullable
        @Bind(R.id.tv_Phone)
        TextView tvPhone;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
