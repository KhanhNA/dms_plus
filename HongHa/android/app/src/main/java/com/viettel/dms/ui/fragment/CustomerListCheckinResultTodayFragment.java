package com.viettel.dms.ui.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.viettel.dms.R;
import com.viettel.dms.helper.DateTimeUtils;
import com.viettel.dms.helper.HardCodeUtil;
import com.viettel.dms.helper.NumberFormatUtils;
import com.viettel.dms.helper.StringUtils;
import com.viettel.dms.presenter.CustomerCheckinOfTodayPresenter;
import com.viettel.dms.ui.activity.BaseActivity;
import com.viettel.dms.ui.iview.ICustomerCheckinOfToday;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfToday;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfTodayResult;
import com.viettel.dmsplus.sdk.utils.MethodUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerListCheckinResultTodayFragment extends BaseFragment implements ICustomerCheckinOfToday, SearchView.OnQueryTextListener {

    public static final String KEY_CUSTOMERID = "KEYCUSTOMERID";
    private String distributorId, salesmanId;
    @Bind(R.id.recycler_view)
    RecyclerView recycler_view;
    private MyRecycleViewAdapter adapter;
    private MyRecycleViewTabletAdapter tabletAdapter;
    CustomerCheckinOfTodayPresenter presenter;

    private SearchView searchView;

    public CustomerListCheckinResultTodayFragment() {
        // Required empty public constructor
    }

    public static CustomerListCheckinResultTodayFragment newInstance() {
        CustomerListCheckinResultTodayFragment fragment = new CustomerListCheckinResultTodayFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            distributorId = getArguments().getString(CheckinOfDayResultFragment.KEY_DISTRIBUTORID);
            salesmanId = getArguments().getString(CheckinOfDayResultFragment.KEY_SALEMANID);
        }

        setHasOptionsMenu(true);
        presenter = new CustomerCheckinOfTodayPresenter(this);
        presenter.getCustomerCheckinOfToday(distributorId, salesmanId);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_customer_list_checkin_result_today, container, false);
        ButterKnife.bind(this, view);
        setTitleResource(R.string.navigation_drawer_item_visit);

        return view;
    }

    @Override
    public void getListCustomerSuccess(CustomerCheckinOfTodayResult data) {
        CustomerCheckinOfTodayResult customerCheckinOfTodayResult = data;
        List<CustomerCheckinOfToday> lstCustomerCheckinOfToday = new ArrayList<>();
        lstCustomerCheckinOfToday.addAll(Arrays.asList(data.getList()));

        if (context.getResources().getBoolean(R.bool.is_tablet)) {
            tabletAdapter = new MyRecycleViewTabletAdapter(lstCustomerCheckinOfToday, context);
            recycler_view.setBackgroundColor(getResources().getColor(R.color.white));
            recycler_view.setAdapter(tabletAdapter);
        } else {
            adapter = new MyRecycleViewAdapter(lstCustomerCheckinOfToday, context);
            recycler_view.setAdapter(adapter);
        }

        recycler_view.setHasFixedSize(true);
        // Attach the adapter to the recyclerview to populate items
        recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));


    }

    @Override
    public void getCustomerCheckinDetailOfToday(CustomerCheckinOfToday data) {

    }

    @Override
    public void getListCustomerError(SdkException info) {

    }

    @Override
    public void finishTask() {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //return false;
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if (context.getResources().getBoolean(R.bool.is_tablet)) {
            if (tabletAdapter == null || tabletAdapter.getItemCount() == 0)
                return false;

            if (TextUtils.isEmpty(newText)) {
                tabletAdapter.getFilter().filter("");
            } else {
                tabletAdapter.getFilter().filter(newText.toString());
            }

        } else {
            if (adapter == null || adapter.getItemCount() == 0)
                return false;

            if (TextUtils.isEmpty(newText)) {
                adapter.getFilter().filter("");
            } else {
                adapter.getFilter().filter(newText.toString());
            }


        }

        // return false;
        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.mnu_customer_list_checkin_today, menu);
        MenuItem itemSearch = menu.findItem(R.id.search_view);
        searchView = (SearchView) itemSearch.getActionView();
        //set OnQueryTextListener for search view then search by text
        searchView.setOnQueryTextListener(this);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    // adapter for cycleview
    public class MyRecycleViewAdapter extends RecyclerView.Adapter<MyRecycleViewAdapter.ViewHolder> {

        private List<CustomerCheckinOfToday> lstCustomerCheckinOfToday;
        public List<CustomerCheckinOfToday> orig;

        // Store the context for easy access
        private Context mContext;

        public MyRecycleViewAdapter(List<CustomerCheckinOfToday> lstCustomerCheckinOfToday, Context mContext) {
            this.lstCustomerCheckinOfToday = lstCustomerCheckinOfToday;
            this.mContext = mContext;
        }

        @Override
        public MyRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);
            // Inflate the custom layout
            View view = inflater.inflate(R.layout.row_customer_list_checkin_today, parent, false);

            // Return a new holder instance
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;

            //context.getResources().getBoolean(R.bool.is_tablet)
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final CustomerCheckinOfToday item = lstCustomerCheckinOfToday.get(position);
            TextView txtCustomerName = holder.txtCustomerName;
            TextView txtNumber = holder.txtNumber;
            String customerName = item.getCustomer().getName().toString();
            txtCustomerName.setText(customerName);
            char letter = java.lang.Character.toUpperCase(customerName.charAt(0));
            holder.imgCircle.setImageResource(HardCodeUtil.getResourceIdColor(letter));
            txtNumber.setText(String.valueOf(letter));

            //check right icon
            if (item.isVanSales()) {
                holder.imgRightIcon.setImageResource(R.drawable.truckrighticon);
            }
            if (item.isClosed()) {
                holder.imgRightIcon.setImageResource(R.drawable.homeicon);
            }
            if (!StringUtils.isNullOrEmpty(item.getPhoto())) {
                holder.imgRightIcon.setImageResource(R.drawable.photocameraiocon);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(getActivity(), "Customer id : " + item.getCustomer().getId(), Toast.LENGTH_LONG).show();
                    BaseActivity baseActivity = (BaseActivity) getActivity();
                    CheckinTodayDetailFragment fragment = CheckinTodayDetailFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString(CustomerListCheckinResultTodayFragment.KEY_CUSTOMERID, item.getId());
                    fragment.setArguments(bundle);
                    baseActivity.replaceCurrentFragment(fragment);
                }
            });

        }


        // filter for search view
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected Filter.FilterResults performFiltering(CharSequence constraint) {
                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<CustomerCheckinOfToday> results = new ArrayList<CustomerCheckinOfToday>();
                    if (orig == null)
                        orig = lstCustomerCheckinOfToday;
                    if (constraint != null) {
                        if (orig != null && orig.size() > 0) {
                            for (final CustomerCheckinOfToday g : orig) {
                                if (g.getCustomer().getName().toLowerCase().contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    lstCustomerCheckinOfToday = (ArrayList<CustomerCheckinOfToday>) results.values;
                    notifyDataSetChanged();
                }
            };
        }


        @Override
        public int getItemCount() {
            if (lstCustomerCheckinOfToday == null) return 0;
            return lstCustomerCheckinOfToday.size();
        }

        public Context getmContext() {
            return this.mContext;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtNumber;
            public TextView txtCustomerName;
            public ImageView imgRightIcon;
            public CircleImageView imgCircle;
            public View itemView;

            public ViewHolder(View itemView) {
                super(itemView);
                txtNumber = (TextView) itemView.findViewById(R.id.txtNumber);
                txtCustomerName = (TextView) itemView.findViewById(R.id.txtCustomerName);
                imgRightIcon = (ImageView) itemView.findViewById(R.id.imgRightIcon);
                imgCircle = (CircleImageView) itemView.findViewById(R.id.imgCircle);
                this.itemView = itemView;
            }

        }
    }

    @Override
    public void onResume() {
        presenter.getCustomerCheckinOfToday(distributorId, salesmanId);
        super.onResume();
    }


    //adapeter for tablet


    // adapter for cycleview
    public class MyRecycleViewTabletAdapter extends RecyclerView.Adapter<MyRecycleViewTabletAdapter.ViewHolder> {

        private List<CustomerCheckinOfToday> lstCustomerCheckinOfToday;
        public List<CustomerCheckinOfToday> orig;

        // Store the context for easy access
        private Context mContext;

        public MyRecycleViewTabletAdapter(List<CustomerCheckinOfToday> lstCustomerCheckinOfToday, Context mContext) {
            this.lstCustomerCheckinOfToday = lstCustomerCheckinOfToday;
            this.mContext = mContext;
        }

        @Override
        public MyRecycleViewTabletAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);
            // Inflate the custom layout
            View view = inflater.inflate(R.layout.row_customer_list_checkin_today_tablet, parent, false);

            // Return a new holder instance
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;

            //context.getResources().getBoolean(R.bool.is_tablet)
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final CustomerCheckinOfToday item = lstCustomerCheckinOfToday.get(position);
            TextView txtCustomerName = holder.txtCustomerName;
            TextView txtDateTime = holder.txtDateTime;
            TextView txtDistance = holder.txtDistance;
            TextView txtARow = holder.txtARow;


            String customerName = item.getCustomer().getName().toString();
            txtCustomerName.setText(customerName);
            String timeStart = DateTimeUtils.formatDate(item.getStartTime(), "HH:mm:ss");
            String timeEnd = DateTimeUtils.formatDate(item.getEndTime(), "HH:mm:ss");

            String minus = "";
            try {
                minus = MethodUtils.minusDate(MethodUtils.getDatewithformat(timeStart), MethodUtils.getDatewithformat(timeEnd));
            } catch (ParseException e) {
                e.printStackTrace();
                minus = "";
            }
            txtDateTime.setText(minus);

            if (item.getDistance() == null || item.getDistance() < 0) {
                txtDistance.setText(R.string.txt_no_infi);
            } else {
                txtDistance.setText(String.valueOf(NumberFormatUtils.formatNumber(new BigDecimal(MethodUtils.round(item.getDistance() * 1000, 0)))) + " m");
            }

            int oddColor = R.color.recylerow;
            int evenColor = R.color.gray300;
            if (position % 2 == 0) {
                txtCustomerName.setBackgroundColor(getResources().getColor(evenColor));
                txtDateTime.setBackgroundColor(getResources().getColor(evenColor));
                txtDistance.setBackgroundColor(getResources().getColor(evenColor));
                txtARow.setBackgroundColor(getResources().getColor(evenColor));
            }
            else {
                txtCustomerName.setBackgroundColor(getResources().getColor(oddColor));
                txtDateTime.setBackgroundColor(getResources().getColor(oddColor));
                txtDistance.setBackgroundColor(getResources().getColor(oddColor));
                txtARow.setBackgroundColor(getResources().getColor(oddColor));
            }


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseActivity baseActivity = (BaseActivity) getActivity();
                    CheckinTodayDetailFragment fragment = CheckinTodayDetailFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString(CustomerListCheckinResultTodayFragment.KEY_CUSTOMERID, item.getId());
                    fragment.setArguments(bundle);
                    baseActivity.replaceCurrentFragment(fragment);
                }
            });


        }


        // filter for search view
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected Filter.FilterResults performFiltering(CharSequence constraint) {
                    final FilterResults oReturn = new FilterResults();
                    final ArrayList<CustomerCheckinOfToday> results = new ArrayList<CustomerCheckinOfToday>();
                    if (orig == null)
                        orig = lstCustomerCheckinOfToday;
                    if (constraint != null) {
                        if (orig != null && orig.size() > 0) {
                            for (final CustomerCheckinOfToday g : orig) {
                                if (g.getCustomer().getName().toLowerCase().contains(constraint.toString()))
                                    results.add(g);
                            }
                        }
                        oReturn.values = results;
                    }
                    return oReturn;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    lstCustomerCheckinOfToday = (ArrayList<CustomerCheckinOfToday>) results.values;
                    notifyDataSetChanged();
                }
            };
        }


        @Override
        public int getItemCount() {
            if (lstCustomerCheckinOfToday == null) return 0;
            return lstCustomerCheckinOfToday.size();
        }

        public Context getmContext() {
            return this.mContext;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtCustomerName;
            public TextView txtDateTime;
            public TextView txtDistance;
            public TextView txtARow;
            public View itemView;

            public ViewHolder(View itemView) {
                super(itemView);
                txtCustomerName = (TextView) itemView.findViewById(R.id.txtCustomerName);
                txtDateTime = (TextView) itemView.findViewById(R.id.txtDateTime);
                txtDistance = (TextView) itemView.findViewById(R.id.txtDistance);
                txtARow = (TextView) itemView.findViewById(R.id.txtARow);
                this.itemView = itemView;

            }

        }
    }


}
