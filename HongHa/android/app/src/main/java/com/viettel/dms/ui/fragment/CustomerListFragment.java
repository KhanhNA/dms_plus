package com.viettel.dms.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.viettel.dms.R;
import com.viettel.dms.helper.layout.SlidingTabLayout;
import com.viettel.dms.ui.activity.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CustomerListFragment extends BaseFragment {

    @Bind(R.id.view_pager_customers)
    ViewPager mViewPager;
    @Bind(R.id.sliding_tabs_customers)
    SlidingTabLayout mSlidingTabLayout;

    public CustomerListFragment() {
        // Required empty public constructor
    }


    public static CustomerListFragment newInstance() {
        CustomerListFragment fragment = new CustomerListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_customer_list, container, false);
        ButterKnife.bind(this, view);
        mViewPager.setAdapter(new MyDummyPagerAdapter(getChildFragmentManager()));
        mSlidingTabLayout.setDistributeEvenly(!getResources().getBoolean(R.bool.is_tablet));
        mSlidingTabLayout.setViewPager(mViewPager, (BaseActivity) getActivity());
        return view;
    }

    class MyDummyPagerAdapter extends FragmentPagerAdapter {
        String[] tabnames;

        public MyDummyPagerAdapter(FragmentManager fm) {
            super(fm);
            tabnames = getResources().getStringArray(R.array.customer_list_tabs_title);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return CustomerListPlannedFragment.newInstance();
                case 1:
                    return CustomerListAllFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return tabnames.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabnames[position];
        }
    }
}
