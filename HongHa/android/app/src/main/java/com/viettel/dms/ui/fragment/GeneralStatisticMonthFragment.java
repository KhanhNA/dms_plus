package com.viettel.dms.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viettel.dms.R;
import com.viettel.dms.helper.NumberFormatUtils;
import com.viettel.dms.presenter.GeneralSaleStatisticListPresenter;
import com.viettel.dms.ui.iview.IGeneralSaleStatisticListView;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.GeneralSaleStatisticTodayListResult;
import com.viettel.dmsplus.sdk.models.Location;
import com.viettel.dmsplus.sdk.models.SaleStatisticToday;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.viettel.dms.R.id.txtTitle;

/**
 * A simple {@link Fragment} subclass.
 */
public class GeneralStatisticMonthFragment extends BaseFragment implements IGeneralSaleStatisticListView {


    public GeneralStatisticMonthFragment() {
        // Required empty public constructor
    }

    @Bind(R.id.viewRevenue)
    View viewRevenue;
    @Bind(R.id.viewRevenueWithoutDiscount)
    View viewRevenueWithoutDiscount;

    @Bind(R.id.ViewOrderDay)
    View ViewOrderDay;
    @Bind(R.id.ViewRevenueOrder)
    View ViewRevenueOrder;
    @Bind(R.id.viewVisitDay)
    View ViewVisitDay;


    TextView txtRevenueResult;
    TextView txtRevenueWithoutDiscountResult;

    TextView txtThisOrderDayResult;
    TextView txtLastMonthOderDayResult;

    TextView txtThisRevenueOrderResult;
    TextView txtLastMonthRevenueOrderResult;

    TextView txtThisVisitDayResult;
    TextView txtLastMonthVisitDayResult;


    RelativeLayout relative_row_first;
    RelativeLayout relative_row_second;
    RelativeLayout relative_row_third;


    TextView txtTitleFirstRow;
    TextView txtTitleSecondRow;
    TextView txtTitleThirdRow;

    TextView txtLabelHeaderRevenue;
    TextView txtLabelHeaderDiscount;




    private SaleStatisticToday thisMonthResult;
    private SaleStatisticToday lastMonthResult;

    GeneralSaleStatisticListPresenter presenter;

    public static GeneralStatisticMonthFragment newInstance() {
        GeneralStatisticMonthFragment fragment = new GeneralStatisticMonthFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        setHasOptionsMenu(true);
        presenter = new GeneralSaleStatisticListPresenter(this);
        presenter.processGeneralSaleStatistic();


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_general_statistic_month, container, false);
        ButterKnife.bind(this, view);
        setTitleResource(R.string.text_title_Report_progress_month);

        //find control
        txtRevenueResult = (TextView) viewRevenue.findViewById(R.id.txtNumber);
        txtRevenueWithoutDiscountResult = (TextView) viewRevenueWithoutDiscount.findViewById(R.id.txtNumber);


        txtThisOrderDayResult = (TextView) ViewOrderDay.findViewById(R.id.txtThisMonthResult);
        txtLastMonthOderDayResult = (TextView) ViewOrderDay.findViewById(R.id.txtLastMonthResult);

        txtThisRevenueOrderResult = (TextView) ViewRevenueOrder.findViewById(R.id.txtThisMonthResult);
        txtLastMonthRevenueOrderResult = (TextView) ViewRevenueOrder.findViewById(R.id.txtLastMonthResult);

        txtThisVisitDayResult = (TextView) ViewVisitDay.findViewById(R.id.txtThisMonthResult);
        txtLastMonthVisitDayResult = (TextView) ViewVisitDay.findViewById(R.id.txtLastMonthResult);


        //set background color for row
        relative_row_first = (RelativeLayout) ViewOrderDay.findViewById(R.id.relative_row_month);
        relative_row_first.setBackgroundColor(getResources().getColor(R.color.colorSecondary3));

        relative_row_second = (RelativeLayout) ViewRevenueOrder.findViewById(R.id.relative_row_month);
        relative_row_second.setBackgroundColor(getResources().getColor(R.color.colorSecondaryDark2));

        relative_row_third = (RelativeLayout) ViewVisitDay.findViewById(R.id.relative_row_month);
        relative_row_third.setBackgroundColor(getResources().getColor(R.color.indicator_remain_visit));

        //set title for row
        txtTitleFirstRow = (TextView) ViewOrderDay.findViewById(R.id.txtTitle);
        txtTitleFirstRow.setText(R.string.text_order_by_day);

        txtTitleSecondRow = (TextView) ViewRevenueOrder.findViewById(R.id.txtTitle);
        txtTitleSecondRow.setText(R.string.text_revenue_by_order);

        txtTitleThirdRow = (TextView) ViewVisitDay.findViewById(R.id.txtTitle);
        txtTitleThirdRow.setText(R.string.text_checkin_by_day);

        txtLabelHeaderRevenue = (TextView) viewRevenue.findViewById(R.id.txtLabel);
        txtLabelHeaderRevenue.setText(R.string.text_revenue);

        txtLabelHeaderDiscount = (TextView) viewRevenueWithoutDiscount.findViewById(R.id.txtLabel);
        txtLabelHeaderDiscount.setText(R.string.text_Revenue_Without_Discount);

        return view;

    }

    @Override
    public void getGeneralSaleStatisticListSuccess(GeneralSaleStatisticTodayListResult data) {
        this.thisMonthResult = data.getThisMonthResult();
        this.lastMonthResult = data.getLastMonthResult();

        /*txtRevenueResult.setText(String.valueOf(thisMonthResult.getRevenue().getActual()));
        txtRevenueWithoutDiscountResult.setText(String.valueOf(lastMonthResult.getRevenue().getActual()));
*/

        txtRevenueResult.setText(NumberFormatUtils.formatNumber(thisMonthResult.getRevenue().getActual(),true,true));
        txtRevenueWithoutDiscountResult.setText(NumberFormatUtils.formatNumber(lastMonthResult.getRevenue().getActual(),true,true));



        txtThisOrderDayResult.setText(String.valueOf((float)thisMonthResult.getOrderByDay()));
        txtLastMonthOderDayResult.setText(String.valueOf((float)lastMonthResult.getOrderByDay()));

        /*txtThisRevenueOrderResult.setText(this.getTextWithLength(String.valueOf(thisMonthResult.getRevenueByOrder()),3) + " K");
        txtLastMonthRevenueOrderResult.setText(this.getTextWithLength(String.valueOf(lastMonthResult.getRevenueByOrder()),3) + " K");*/

        txtThisRevenueOrderResult.setText(NumberFormatUtils.formatNumber(BigDecimal.valueOf(thisMonthResult.getRevenueByOrder()),true,true));
        txtLastMonthRevenueOrderResult.setText(NumberFormatUtils.formatNumber(BigDecimal.valueOf(lastMonthResult.getRevenueByOrder()),true,true));

        txtThisVisitDayResult.setText(String.valueOf((float)thisMonthResult.getVisitByDay()));
        txtLastMonthVisitDayResult.setText(String.valueOf((float)lastMonthResult.getVisitByDay()));




    }


    @Override
    public void getGeneralSaleStatisticError(SdkException info) {

    }

    @Override
    public void finishTask() {

    }
}
