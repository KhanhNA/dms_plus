package com.viettel.dms.ui.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.Theme;
import com.viettel.dms.R;
import com.viettel.dms.helper.NumberFormatUtils;
import com.viettel.dms.helper.layout.ViewEmptyStateLayout;
import com.viettel.dms.presenter.GeneralSaleStatisticListPresenter;
import com.viettel.dms.ui.iview.IGeneralSaleStatisticListView;
import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.GeneralSaleStatisticTodayListResult;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by duongnv18 on 08/02/2017.
 */

public class GeneralStatisticsFragment extends BaseFragment implements IGeneralSaleStatisticListView{

    ViewEmptyStateLayout viewEmptyStateLayout;
    private int viewState = ViewEmptyStateLayout.VIEW_STATE_NORMAL;

    TextView txtDateTimeLable;
    TextView txtDateTime;

    @Bind(R.id.view_DateTime)
    View viewDateTime;


    @Bind(R.id.view_sales_result)
    View viewSalesResult;

    @Bind(R.id.view_checkin_result)
    View viewCheckinResult;

    @Bind(R.id.view_order_out_routing)
    View viewOrderOutRouting;

    @Bind(R.id.view_checkin_has_order)
    View viewCheckinHasOrder;

    @Bind(R.id.view_checkin_time_not_enough)
    View viewCheckinTimeNotEnough;

    @Bind(R.id.view_checkin_wrong_position)
    View viewCheckinWrongPosition;

    TextView txtSaleResultLabel;
    TextView txtNumberSaleResult;

    TextView txtCheckinResultLable;
    TextView txtNumberCheckinResult;

    TextView txtOrderOutRouting;
    TextView txtOrderOutRoutingTitle;
    TextView txtCheckinHasOrder;
    TextView txtCheckinHasOrderTitle;
    TextView txtCheckinTimeNotEnough;
    TextView txtCheckinTimeNotEnoughTitle;
    TextView txtCheckinWrongPosition;
    TextView txtCheckinWrongPositionTitle;

    //icon for row
    /*ImageView imgIconSale;
    ImageView imgIconCheckin;*/

    ImageView imgIconOrderWithOutRoute;
    ImageView imgIconCheckinHasOrder;
    ImageView imgIconTimeNotEnough;
    ImageView imgIconWrongPosition;


    GeneralSaleStatisticListPresenter presenter;
    List<GeneralSaleStatisticTodayListResult> lstdataSaleToday;

    public static GeneralStatisticsFragment newInstance() {
        GeneralStatisticsFragment fragment = new GeneralStatisticsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public GeneralStatisticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

        setHasOptionsMenu(true);
        presenter = new GeneralSaleStatisticListPresenter(this);
        presenter.processGeneralSaleStatistic();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_general_statistics, container, false);
        ButterKnife.bind(this, view);
        setTitleResource(R.string.text_title_Report_progress_day);

        //view loading
        txtDateTimeLable = (TextView) viewDateTime.findViewById(R.id.txtLabel);
        txtDateTimeLable.setText(R.string.text_today);
        txtDateTime = (TextView) viewDateTime.findViewById(R.id.txtNumber);


        txtSaleResultLabel = (TextView) viewSalesResult.findViewById(R.id.txtLabel);
        txtSaleResultLabel.setText(getString(R.string.text_sale_result));
       /* imgIconSale  = (ImageView) viewSalesResult.findViewById(R.id.imgIcon);
        imgIconSale.setImageResource(R.drawable.moneyicon);*/

        txtCheckinResultLable= (TextView) viewCheckinResult.findViewById(R.id.txtLabel);
        txtCheckinResultLable.setText(getString(R.string.text_visit));

        /*imgIconCheckin = (ImageView) viewCheckinResult.findViewById(R.id.imgIcon);
        imgIconCheckin.setImageResource(R.drawable.personicon);*/



        txtNumberSaleResult = (TextView) viewSalesResult.findViewById(R.id.txtNumber);
        txtNumberCheckinResult = (TextView) viewCheckinResult.findViewById(R.id.txtNumber);


        txtOrderOutRouting = (TextView) viewOrderOutRouting.findViewById(R.id.txtNumberResult);
        txtCheckinHasOrder = (TextView) viewCheckinHasOrder.findViewById(R.id.txtNumberResult);

        /*imgIconOrderWithOutRoute = (ImageView) viewOrderOutRouting.findViewById(R.id.imgIcon);
        imgIconOrderWithOutRoute.setImageResource(R.drawable.handicon);*/


        txtCheckinTimeNotEnough  = (TextView) viewCheckinTimeNotEnough.findViewById(R.id.txtNumberResult);
        txtCheckinWrongPosition = (TextView) viewCheckinWrongPosition.findViewById(R.id.txtNumberResult);



        txtOrderOutRoutingTitle = (TextView) viewOrderOutRouting.findViewById(R.id.txtTitleResult);
        txtOrderOutRoutingTitle.setText(R.string.text_Order_without_Visit);

        txtCheckinHasOrderTitle = (TextView) viewCheckinHasOrder.findViewById(R.id.txtTitleResult);
        txtCheckinHasOrderTitle.setText(R.string.text_Visit_has_Order);

        /*imgIconCheckinHasOrder = (ImageView) viewCheckinHasOrder.findViewById(R.id.imgIcon);
        imgIconCheckinHasOrder.setImageResource(R.drawable.truckicon);*/


        txtCheckinTimeNotEnoughTitle = (TextView) viewCheckinTimeNotEnough.findViewById(R.id.txtTitleResult);
        txtCheckinTimeNotEnoughTitle.setText(R.string.text_Visit_error_Duration);

        /*imgIconTimeNotEnough = (ImageView) viewCheckinTimeNotEnough.findViewById(R.id.imgIcon);
        imgIconTimeNotEnough.setImageResource(R.drawable.clockicon);*/


        txtCheckinWrongPositionTitle= (TextView) viewCheckinWrongPosition.findViewById(R.id.txtTitleResult);
        txtCheckinWrongPositionTitle.setText(R.string.text_Visit_error_Position);

        /*imgIconWrongPosition = (ImageView) viewCheckinWrongPosition.findViewById(R.id.imgIcon);
        imgIconWrongPosition.setImageResource(R.drawable.locationicon);*/

        //set color
        txtOrderOutRouting.setTextColor(getResources().getColor(R.color.first_letter_B));
        txtCheckinHasOrder.setTextColor(getResources().getColor(R.color.first_letter_S));
        txtCheckinTimeNotEnough.setTextColor(getResources().getColor(R.color.first_letter_O));
        txtCheckinWrongPosition.setTextColor(getResources().getColor(R.color.first_letter_8));



        //  viewEmptyStateLayout.updateViewState(viewState);
        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //ButterKnife.unbind(this);
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void getGeneralSaleStatisticListSuccess(GeneralSaleStatisticTodayListResult data) {
        if(data != null){
            this.setText(data);
        }

    }

    @Override
    public void getGeneralSaleStatisticError(SdkException info) {

    }

    @Override
    public void finishTask() {

    }


    private void setText(GeneralSaleStatisticTodayListResult data){

        /*String moneyFormat = NumberFormatUtils.formatMoneyWithRepresent(data.getTodayResult().getRevenue().getActual(),getCurrentCountryCode()) ;
        txtNumberSaleResult.setText(moneyFormat);*/
       // NumberFormatUtils.formatMoneyWithRepresent()
        /*String locale = context.getResources().getConfiguration().locale.getCountry();
        String iso3 = context.getResources().getConfiguration().locale.getISO3Country();*/

        txtDateTime.setText(data.getToday());
        String moneyFormat = NumberFormatUtils.formatMoneyWithRepresent(data.getTodayResult().getRevenue().getActual(),"VND") ;
        txtNumberSaleResult.setText(moneyFormat);

        txtNumberCheckinResult.setText(data.getTodayResult().getVisit().getActual() + "/" + data.getTodayResult().getVisit().getPlan());
        txtOrderOutRouting.setText(String.valueOf(data.getTodayResult().getOrderNoVisit().getActual()));
        txtCheckinHasOrder.setText(String.valueOf(data.getTodayResult().getVisitHasOrder().getActual()));
        txtCheckinTimeNotEnough.setText(String.valueOf(data.getTodayResult().getVisitErrorDuration().getActual()));
        txtCheckinWrongPosition.setText(String.valueOf(data.getTodayResult().getVisitErrorPosition().getActual()));
    }



    private String getCurrentCountryCode(){
        TelephonyManager tm = (TelephonyManager)getContext().getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
        Toast.makeText(getActivity(), "code" + countryCodeValue, Toast.LENGTH_SHORT).show();
        if(countryCodeValue == null || countryCodeValue.equals("")){
            countryCodeValue = "VND";
            Toast.makeText(getActivity(), "Cannot get current country code", Toast.LENGTH_SHORT).show();
        }
        return  countryCodeValue;
    }

}
