package com.viettel.dms.ui.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.viettel.dms.R;
import com.viettel.dms.helper.DialogUtils;
import com.viettel.dms.helper.NumberFormatUtils;
import com.viettel.dms.helper.StringUtils;
import com.viettel.dms.helper.layout.GeneralSwipeRefreshLayout;
import com.viettel.dms.helper.layout.SetTextUtils;
import com.viettel.dms.helper.layout.DMSCompat;
import com.viettel.dms.presenter.HomePagePresenter;
import com.viettel.dms.ui.iview.HomePageView;
import com.viettel.dmsplus.sdk.models.DashboardMonthlyInfo;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Thanh
 * @since 9/8/15
 */
public class HomePageFragment extends BaseFragment implements HomePageView, SwipeRefreshLayout.OnRefreshListener {

    public static HomePageFragment newInstance() {
        return new HomePageFragment();
    }

    @Nullable
    @Bind(R.id.scroll_view)
    ScrollView scrollView;

    @Bind(R.id.swipe_refresh)
    GeneralSwipeRefreshLayout swipeRefreshLayout;

    //region  REVENUE
    @Nullable
    @Bind(R.id.tv_revenue_actual)
    TextView tvRevenueActual;
    @Nullable
    @Bind(R.id.tv_revenue_percent)
    TextView tvRevenuePercentage;
    @Nullable
    @Bind(R.id.tv_revenue_target)
    TextView tvRevenueTarget;
    //endregion

    //region  SUB REVENUE
    @Nullable
    @Bind(R.id.tv_sub_revenue_actual)
    TextView tvSubRevenueActual;
    @Nullable
    @Bind(R.id.tv_sub_revenue_percent)
    TextView tvSubRevenuePercentage;
    @Nullable
    @Bind(R.id.tv_sub_revenue_target)
    TextView tvSubRevenueTarget;
    //endregion

    //region  PRODUCTIVITY
    @Nullable
    @Bind(R.id.tv_productivity_actual)
    TextView tvProductivityActual;
    @Nullable
    @Bind(R.id.tv_productivity_percent)
    TextView tvProductivityPercentage;
    @Nullable
    @Bind(R.id.tv_productivity_target)
    TextView tvProductivityTarget;
    //endregion

    //region  VISIT
    @Nullable
    @Bind(R.id.tv_visit_actual)
    TextView tvVisitActual;
    @Nullable
    @Bind(R.id.tv_visit_percent)
    TextView tvVisitPercentage;
    @Nullable
    @Bind(R.id.tv_visit_target)
    TextView tvVisitTarget;
    @Nullable
    @Bind(R.id.tv_visit_remaining)
    TextView tvVisitRemaining;
    //endregion

    //region  SALES DAY
    @Bind(R.id.tv_sales_day_actual)
    TextView tvSalesDayActual;
    @Bind(R.id.tv_sales_day_percent)
    TextView tvSalesDayPercentage;
    @Bind(R.id.tv_sales_day_target)
    TextView tvSalesDayTarget;
    //endregion
    @Nullable
    @Bind(R.id.chart)
    PieChart pieChart;

    private static final int INDEX_COMPLETED_VISIT = 0;
    private static final int INDEX_REMAINING_VISIT = 1;

    private DashboardMonthlyInfo data;
    private boolean loading;

    private HomePagePresenter presenter;

    public HomePageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setTitleResource(R.string.home_page_title);
        setPaddingLeft(getResources().getBoolean(R.bool.is_tablet) ? 104f : 72f);
        final View view = inflater.inflate(R.layout.fragment_home_page, container, false);

        ButterKnife.bind(this, view);

        presenter = new HomePagePresenter(this);

        swipeRefreshLayout.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                return scrollView.getScrollY() != 0;
            }
        });
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(loading);
                    swipeRefreshLayout.setOnRefreshListener(HomePageFragment.this);
                    checkIfHaveData();
                }
            }
        });

        return view;
    }

    private void setupPieChart() {
        pieChart.setExtraOffsets(8, 8, 8, 8);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        pieChart.setCenterTextTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Regular.ttf"));
        pieChart.setCenterTextSize(24f);

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(74f);
        pieChart.setTransparentCircleRadius(77f);

        pieChart.setDrawCenterText(true);

        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.getLegend().setEnabled(false);
        pieChart.setDescription("");
        pieChart.setDrawSliceText(false);
    }

    /**
     * data about visit performance
     */
    private void setDataForPieChart() {
        if (data == null || data.getVisit() == null) return;
        setupPieChart();
        ArrayList<Entry> yVals1 = new ArrayList<>();
        float completed = (data.getVisit().getPercentage() != null) ? data.getVisit().getPercentage().floatValue() : 0f;
        float remaining = 100f - completed;
        yVals1.add(new Entry(completed, INDEX_COMPLETED_VISIT));
        yVals1.add(new Entry(remaining, INDEX_REMAINING_VISIT));

        ArrayList<String> xVals = new ArrayList<>();
        xVals.add("dummy 1");
        xVals.add("dummy 2");

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setDrawValues(false);

        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(ContextCompat.getColor(context, R.color.indicator_visit));
        colors.add(ContextCompat.getColor(context, R.color.indicator_remain_visit));

        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        pieChart.setData(data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void checkIfHaveData() {
        if (!loading) {
            if (data == null) {
                presenter.reloadData();
            } else {
                displayData(data);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void displayData(DashboardMonthlyInfo info) {
        this.data = info;
        if (data.getRevenue() != null) {
            SetTextUtils.setText(tvRevenueActual, NumberFormatUtils.formatNumber(data.getRevenue().getActual(), true, true));
            SetTextUtils.setText(tvRevenueTarget, NumberFormatUtils.formatNumber(data.getRevenue().getPlan(), true, true));
            SetTextUtils.setText(tvRevenuePercentage,
                    NumberFormatUtils.formatNumber(data.getRevenue().getPercentage() == null ? new BigDecimal(100L) : data.getRevenue().getPercentage()) + StringUtils.getPercentageSymbol(context)
            );
            DMSCompat.setTextAppearance(context, tvRevenuePercentage, data.getRevenue().getPercentage() != null && data.getRevenue().getPercentage().compareTo(new BigDecimal(100L)) < 0 ? R.style.TextAppearance_Regular_14_SecondaryColor : R.style.TextAppearance_Regular_14_PrimaryColor);
        } else {
            SetTextUtils.setText(tvRevenueActual, null);
            SetTextUtils.setText(tvRevenueTarget, null);
            SetTextUtils.setText(tvRevenuePercentage, null);
        }

        if (data.getSubRevenue() != null) {
            SetTextUtils.setText(tvSubRevenueActual, NumberFormatUtils.formatNumber(data.getSubRevenue().getActual(), true, true));
            SetTextUtils.setText(tvSubRevenueTarget, NumberFormatUtils.formatNumber(data.getSubRevenue().getPlan(), true, true));
            SetTextUtils.setText(tvSubRevenuePercentage,
                    NumberFormatUtils.formatNumber(data.getSubRevenue().getPercentage() == null ? new BigDecimal(100L) : data.getSubRevenue().getPercentage()) + StringUtils.getPercentageSymbol(context)
            );
            DMSCompat.setTextAppearance(context, tvSubRevenuePercentage, data.getSubRevenue().getPercentage() != null && data.getSubRevenue().getPercentage().compareTo(new BigDecimal(100L)) < 0 ? R.style.TextAppearance_Regular_14_SecondaryColor : R.style.TextAppearance_Regular_14_PrimaryColor);
        } else {
            SetTextUtils.setText(tvSubRevenueActual, null);
            SetTextUtils.setText(tvSubRevenueTarget, null);
            SetTextUtils.setText(tvSubRevenuePercentage, null);
        }

        if (data.getVisit() != null) {
            SetTextUtils.setText(tvVisitActual, NumberFormatUtils.formatNumber(data.getVisit().getActual(), true));
            SetTextUtils.setText(tvVisitTarget, NumberFormatUtils.formatNumber(data.getVisit().getPlan(), true));
            SetTextUtils.setText(tvVisitPercentage,
                    NumberFormatUtils.formatNumber(data.getVisit().getPercentage() == null ? new BigDecimal(100L) : data.getVisit().getPercentage()) + StringUtils.getPercentageSymbol(context)
            );
            DMSCompat.setTextAppearance(context, tvVisitPercentage, data.getVisit().getPercentage() != null && data.getVisit().getPercentage().compareTo(new BigDecimal(100L)) < 0 ? R.style.TextAppearance_Regular_14_SecondaryColor : R.style.TextAppearance_Regular_14_PrimaryColor);
            // For Tablet pie chart
            if (pieChart != null) {
                setDataForPieChart();
                pieChart.setCenterText(decorateCenterPercent(NumberFormatUtils.formatNumber(data.getVisit().getPercentage() == null ? new BigDecimal(100L) : data.getVisit().getPercentage()) + StringUtils.getPercentageSymbol(context)));
                pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
                SetTextUtils.setText(tvVisitRemaining, NumberFormatUtils.formatNumber(data.getVisit().getRemaining(), true));
            }
        } else {
            SetTextUtils.setText(tvVisitActual, null);
            SetTextUtils.setText(tvVisitTarget, null);
            SetTextUtils.setText(tvVisitPercentage, null);
        }

        if (data.getProductivity() != null) {
            SetTextUtils.setText(tvProductivityActual, NumberFormatUtils.formatNumber(data.getProductivity().getActual(), true, true));
            SetTextUtils.setText(tvProductivityTarget, NumberFormatUtils.formatNumber(data.getProductivity().getPlan(), true, true));
            SetTextUtils.setText(tvProductivityPercentage,
                    NumberFormatUtils.formatNumber(data.getProductivity().getPercentage() == null ? new BigDecimal(100L) : data.getProductivity().getPercentage()) + StringUtils.getPercentageSymbol(context)
            );
            DMSCompat.setTextAppearance(context, tvProductivityPercentage, data.getProductivity().getPercentage() != null && data.getProductivity().getPercentage().compareTo(new BigDecimal(100L)) < 0 ? R.style.TextAppearance_Regular_14_SecondaryColor : R.style.TextAppearance_Regular_14_PrimaryColor);
        } else {
            SetTextUtils.setText(tvProductivityActual, null);
            SetTextUtils.setText(tvProductivityTarget, null);
            SetTextUtils.setText(tvProductivityPercentage, null);
        }

        if (data.getSaleDays() != null) {
            SetTextUtils.setText(tvSalesDayActual, NumberFormatUtils.formatNumber(data.getSaleDays().getActual(), true));
            SetTextUtils.setText(tvSalesDayTarget, NumberFormatUtils.formatNumber(data.getSaleDays().getPlan(), true));
            SetTextUtils.setText(tvSalesDayPercentage,
                    NumberFormatUtils.formatNumber(data.getSaleDays().getPercentage() == null ? new BigDecimal(100L) : data.getSaleDays().getPercentage()) + StringUtils.getPercentageSymbol(context)
            );
            DMSCompat.setTextAppearance(context, tvSalesDayPercentage, data.getSaleDays().getPercentage() != null && data.getSaleDays().getPercentage().compareTo(new BigDecimal(100L)) < 0 ? R.style.TextAppearance_Regular_14_SecondaryColor : R.style.TextAppearance_Regular_14_PrimaryColor);
        } else {
            SetTextUtils.setText(tvSalesDayActual, null);
            SetTextUtils.setText(tvSalesDayTarget, null);
            SetTextUtils.setText(tvSalesDayPercentage, null);
        }
    }

    private SpannableString decorateCenterPercent(String s) {
        StringBuilder builder = new StringBuilder();
        builder.append(s);
        builder.append('\n');
        String strOfComplete = context.getString(R.string.home_page_cv_of_completed);
        builder.append(strOfComplete);

        SpannableString str = new SpannableString(builder.toString());
        str.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
        str.setSpan(new RelativeSizeSpan(1.3f), 0, s.length(), 0);

        str.setSpan(new StyleSpan(Typeface.NORMAL), s.length(), str.length(), 0);
        str.setSpan(new ForegroundColorSpan(Color.GRAY), s.length(), str.length(), 0);
        str.setSpan(new RelativeSizeSpan(.5f), s.length(), str.length(), 0);
        return str;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @OnClick(value = {R.id.btn_view_detail_revenue, R.id.btn_view_detail_productivity, R.id.btn_view_detail_salesday})
    public void onButtonClick(View v) {
        int type = v.getId();
        switch (type) {
            case R.id.btn_view_detail_revenue:
                replaceCurrentFragment(SalesDetailFragment.newInstance(SalesDetailFragment.TYPE_REVENUE));
                break;
            case R.id.btn_view_detail_productivity:
                replaceCurrentFragment(SalesDetailFragment.newInstance(SalesDetailFragment.TYPE_PRODUCTIVITY));
                break;
            case R.id.btn_view_detail_salesday:
                replaceCurrentFragment(getResources().getBoolean(R.bool.is_tablet) ? SalesMonthlySummaryTBFragment.newInstance() : SalesMonthlySummaryFragment.newInstance());
                break;
            default:
                break;
        }
    }

    @Override
    public void onRefresh() {
        presenter.reloadData();
    }

    @Override
    public void showLoading() {
        loading = true;
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(true);
            }
        });
    }

    @Override
    public void hideLoading() {
        loading = false;
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void handleNullMainPoint() {
        DialogUtils.showMessageDialog(context,
                R.string.error, R.string.error_unknown,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                });
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
