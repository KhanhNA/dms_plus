package com.viettel.dms.ui.iview;

/**
 * Created by PHAMHUNG on 3/25/2016.
 */
public interface ICheckInView {
    boolean checkLegalLocationProviderAndEnableGPS();

    void showProgressLocating();

    void dismissProgress();

    void showWarningTurnOffGPSWhenLocating();

    void createCheckInSuccess();

    void createCheckInFail();

    void updateProgress(String progress,int current, int total);
}
