package com.viettel.dms.ui.iview;

import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CategorySimple;
import com.viettel.dmsplus.sdk.models.GeneralSaleStatisticTodayListResult;
import com.viettel.dmsplus.sdk.models.SaleStatisticToday;
import com.viettel.dmsplus.sdk.models.UserSimple;

import java.util.List;

/**
 * Created by duongnv18 on 18/02/2017.
 */

public interface ICheckinOfResultSpinner {

    void getDistributorListSuccess(List<CategorySimple> data);
    void getDistributorListError(SdkException info);

    void getSalemanListSuccess(List<UserSimple> data);
    void getSalemanListError(SdkException info);
    void getCheckinResultOfToday(SaleStatisticToday saleStatisticToday);
    void getCheckinResultOfTodayError(SdkException info);


    void getListCustomerSuccess();
    void getListCustomerError(SdkException info);


    void finishTask();
}
