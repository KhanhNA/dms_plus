package com.viettel.dms.ui.iview;

import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfToday;
import com.viettel.dmsplus.sdk.models.CustomerCheckinOfTodayResult;

/**
 * Created by duongnv18 on 22/02/2017.
 */

public interface ICustomerCheckinOfToday {
    void getListCustomerSuccess(CustomerCheckinOfTodayResult data);
    void getCustomerCheckinDetailOfToday(CustomerCheckinOfToday data);

    void getListCustomerError(SdkException info);
    void finishTask();

}
