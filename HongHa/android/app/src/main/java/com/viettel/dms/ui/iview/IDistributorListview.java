package com.viettel.dms.ui.iview;

import com.viettel.dmsplus.sdk.SdkException;

/**
 * @author PHAMHUNG
 * @since 9/9/2015
 */
public interface IDistributorListview {

    void getDistributorListSuccess();
    void getDistributorListError(SdkException info);

    void getSalemanListSuccess();
    void getSalemanListError(SdkException info);


    void getListCustomerSuccess();
    void getListCustomerError(SdkException info);

    void finishTask();





}
