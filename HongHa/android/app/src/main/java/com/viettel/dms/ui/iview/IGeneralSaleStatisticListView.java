package com.viettel.dms.ui.iview;

import com.viettel.dmsplus.sdk.SdkException;
import com.viettel.dmsplus.sdk.models.GeneralSaleStatisticTodayListResult;

/**
 * @author PHAMHUNG
 * @since 9/9/2015
 */
public interface IGeneralSaleStatisticListView {

    void getGeneralSaleStatisticListSuccess(GeneralSaleStatisticTodayListResult data);

    void getGeneralSaleStatisticError(SdkException info);

    void finishTask();

}
