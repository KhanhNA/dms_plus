package com.viettel.dmsplus.sdk;

import com.viettel.dmsplus.sdk.auth.OAuthSession;

public class SdkConfig {

    /**
     * Flag for whether logging is enabled. This will log all requests and responses made by the SDK
     */
    public static final boolean IS_LOG_ENABLED = true;

    /**
     * Flag for whether a user should be allowed to continue when there is an SSL error in the webview. Disabled by default.
     */
    public static final boolean ALLOW_SSL_ERROR = false;

    /**
     * Because BuildConfig.DEBUG is always false when library projects publish their release variants we use ApplicationInfo
     * Flag for whether the app is currently run in debug mode. This is set by the {@link OAuthSession}
     * object and is determined from the {@link android.content.pm.ApplicationInfo.FLAG_DEBUGGABLE}
     */
    public static final boolean IS_DEBUG = false;

    /**
     * Client id used for the OAuth flow
     */
    public static final String CLIENT_ID = "dmsplus";

    /**
     * Client secret used for the OAuth flow
     */
    public static final String CLIENT_SECRET = "secret";

    /**
     * The redirect url used with OAuth flow
     */
    public static final String REDIRECT_URL = "oauth2://dmsplus.sdk";

    //SERVER TEST
    public static final String BASE_URL = "http://10.60.108.153:8080";

    //SERVER HUNGP1-PC
    //public static final String BASE_URL = "http://10.61.186.65:8080";




    public static final String getApiBaseUrl() {
        return BASE_URL + "/api";
    }

    public static final String getRequestTokenUrl() {
        return BASE_URL + "/oauth/token";
    }

    public static final String getRequestAuthorizeUrl() {
        return BASE_URL + "/oauth/authorize";
    }

    public static final String getUserInfoUrl() {
        return BASE_URL + "/oauth/userinfo";
    }

    public static final String getRevokeTokenUrl() {
        return BASE_URL + "/oauth/revoke/{token}";
    }

    public static final String getChangePasswordUrl() {
        return BASE_URL + "/oauth/password";
    }
}
