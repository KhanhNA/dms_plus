package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by PHAMHUNG on 3/28/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckInCreateDto implements Serializable, Parcelable{
    private String note;
    private String[] photos;
    private Location location;

    public CheckInCreateDto(){}

    public CheckInCreateDto(Parcel in) {
        note = in.readString();
        photos = in.createStringArray();
        location = in.readParcelable(Location.class.getClassLoader());
    }

    public static final Creator<CheckInCreateDto> CREATOR = new Creator<CheckInCreateDto>() {
        @Override
        public CheckInCreateDto createFromParcel(Parcel in) {
            return new CheckInCreateDto(in);
        }

        @Override
        public CheckInCreateDto[] newArray(int size) {
            return new CheckInCreateDto[size];
        }
    };

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String[] getPhotos() {
        return photos;
    }

    public void setPhotos(String[] photos) {
        this.photos = photos;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(note);
        dest.writeStringArray(photos);
        dest.writeParcelable(location, flags);
    }
}
