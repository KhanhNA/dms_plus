package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.viettel.dmsplus.sdk.network.IsoDateDeserializer;

import java.util.Date;

/**
 * Created by PHAMHUNG on 3/28/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckInDto extends IdDto {
    private UserSimple createdBy;
    @JsonDeserialize(using = IsoDateDeserializer.class)
    private Date createdTime;

    private String note;
    private String[] photos;
    private Location location;

    public CheckInDto() {
    }

    public CheckInDto(Parcel in) {
        createdBy = in.readParcelable(UserSimple.class.getClassLoader());
        long tmpCreatedTime = in.readLong();
        this.createdTime = tmpCreatedTime == -1 ? null : new Date(tmpCreatedTime);
        note = in.readString();
        in.readStringArray(photos);
        location = in.readParcelable(Location.class.getClassLoader());
    }

    public static final Creator<CheckInDto> CREATOR = new Creator<CheckInDto>() {
        public CheckInDto createFromParcel(Parcel in) {
            return new CheckInDto(in);
        }

        public CheckInDto[] newArray(int size) {
            return new CheckInDto[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(createdBy, flags);
        dest.writeLong(createdTime != null ? createdTime.getTime() : -1);
        dest.writeString(note);
        dest.writeStringArray(photos);
        dest.writeParcelable(location, flags);
    }

    public UserSimple getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserSimple createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String[] getPhotos() {
        return photos;
    }

    public void setPhotos(String[] photos) {
        this.photos = photos;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
