package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by PHAMHUNG on 3/28/2016.
 */
public class CheckInListResult implements Parcelable {
    @JsonProperty("list")
    private CheckInDto[] items;
    private int count;

    public CheckInListResult() {
    }

    public CheckInListResult(Parcel in) {

        items = in.createTypedArray(CheckInDto.CREATOR);
        count = in.readInt();
    }

    public static final Creator<CheckInListResult> CREATOR = new Creator<CheckInListResult>() {
        @Override
        public CheckInListResult createFromParcel(Parcel in) {
            return new CheckInListResult(in);
        }

        @Override
        public CheckInListResult[] newArray(int size) {
            return new CheckInListResult[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(items, flags);
        dest.writeInt(count);
    }

    public CheckInDto[] getItems() {
        return items;
    }

    public void setItems(CheckInDto[] items) {
        this.items = items;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
