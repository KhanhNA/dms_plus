package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.viettel.dmsplus.sdk.network.IsoDateDeserializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by duongnv18 on 22/02/2017.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerCheckinOfToday extends IdDto implements Comparable<CustomerCheckinOfToday>, Serializable {
    public static final Creator<CustomerCheckinOfToday> CREATOR = new Creator<CustomerCheckinOfToday>() {
        public CustomerCheckinOfToday createFromParcel(Parcel in) {
            return new CustomerCheckinOfToday(in);
        }

        public CustomerCheckinOfToday[] newArray(int size) {
            return new CustomerCheckinOfToday[size];
        }
    };


    public CategorySimple getDistributor() {
        return distributor;
    }

    public void setDistributor(CategorySimple distributor) {
        this.distributor = distributor;
    }

    //field
    private CategorySimple distributor;

    public CustomerSimple getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerSimple customer) {
        this.customer = customer;
    }

    public CategorySimple getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(CategorySimple createdBy) {
        this.createdBy = createdBy;
    }

    public boolean isVanSales() {
        return vanSales;
    }

    public void setVanSales(boolean vanSales) {
        this.vanSales = vanSales;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }

    public UserSimple getSalesman() {
        return salesman;
    }

    public void setSalesman(UserSimple salesman) {
        this.salesman = salesman;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    //private CategorySimple customer;
    private CustomerSimple customer;
    private CategorySimple createdBy;
    private boolean vanSales;
    private boolean closed;
    private String photo;
    private Double distance;
    @JsonDeserialize(using = IsoDateDeserializer.class)
    private Date startTime;
    @JsonDeserialize(using = IsoDateDeserializer.class)
    private Date endTime;
    private BigDecimal subTotal;
    private int approveStatus;
    private UserSimple salesman;

    public boolean isHasOrder() {
        return hasOrder;
    }

    public void setHasOrder(boolean hasOrder) {
        this.hasOrder = hasOrder;
    }

    private boolean hasOrder;


    private String code;


    //vh.tvTotalAmount.setText(NumberFormatUtils.formatNumber(orderDetail.getSubTotal()));


    public CustomerCheckinOfToday() {
        super();
    }


    public CustomerCheckinOfToday(Parcel in) {
        super(in);

        this.distributor = in.readParcelable(this.getClass().getClassLoader());

       /* this.vanSales = in.readByte() != 0;
        this.closed = in.readByte() != 0;
        this.customer = in.readParcelable(this.getClass().getClassLoader());
        this.createdBy = in.readParcelable(this.getClass().getClassLoader());
        this.photo = in.readString();

        long time = in.readLong();
        this.startTime = time != 0 ? new Date(time) : null;
        time = in.readLong();
        this.endTime = time != 0 ? new Date(time) : null;

        this.distance = in.readDouble();
        if (this.distance < 0) {
            this.distance = null;
        }

        this.subTotal = (BigDecimal) in.readSerializable();
        this.approveStatus = in.readInt();

        this.salesman = in.readParcelable(this.getClass().getClassLoader());*/
    }


    @Override
    public void writeToParcel(Parcel dest, int flag) {
        super.writeToParcel(dest, flag);
        dest.writeParcelable(distributor, flag);

        /*dest.writeByte((byte) (vanSales ? 1 : 0));
        dest.writeByte((byte) (closed ? 1 : 0));
        dest.writeParcelable(customer, flag);
        dest.writeParcelable(createdBy, flag);
        dest.writeString(photo);

        dest.writeLong(startTime != null ? startTime.getTime() : 0);
        dest.writeLong(endTime != null ? endTime.getTime() : 0);
        dest.writeDouble(distance != null ? distance : -1);
        dest.writeSerializable(this.subTotal);
        dest.writeInt(this.approveStatus);
        dest.writeParcelable(salesman, flag);*/
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public int compareTo(CustomerCheckinOfToday another) {
        return 0;
    }



}
