package com.viettel.dmsplus.sdk.models;

/**
 * Created by duongnv18 on 22/02/2017.
 */

public class CustomerCheckinOfTodayResult {

    private CustomerCheckinOfToday[] list;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public CustomerCheckinOfToday[] getList() {
        return list;
    }

    public void setList(CustomerCheckinOfToday[] list) {
        this.list = list;
    }

    private int count;

}
