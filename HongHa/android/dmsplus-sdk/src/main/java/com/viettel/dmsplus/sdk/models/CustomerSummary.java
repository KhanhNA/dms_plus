package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author PHAMHUNG
 * @since 3/3/2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerSummary extends CustomerSimple {

    public static final Creator<CustomerSummary> CREATOR = new Creator<CustomerSummary>() {
        public CustomerSummary createFromParcel(Parcel source) {
            return new CustomerSummary(source);
        }

        public CustomerSummary[] newArray(int size) {
            return new CustomerSummary[size];
        }
    };

    private String mobile;
    private String phone;
    private String email;
    private String contact;
    private Location location;

    private BigDecimal productivityLastMonth;
    private BigDecimal productivityThisMonth;
    private long nbOrderThisMonth;

    private List<CustomerInfoRevenue> revenueLastThreeMonth;
    private List<CustomerInfoRecentOrder> lastFiveOrders;

    public CustomerSummary() {

    }

    public CustomerSummary(Parcel in) {
        super(in);

        this.mobile = in.readString();
        this.phone = in.readString();
        this.email = in.readString();
        this.contact = in.readString();
        this.location = in.readParcelable(Location.class.getClassLoader());

        this.productivityLastMonth = (BigDecimal) in.readSerializable();
        this.productivityThisMonth = (BigDecimal) in.readSerializable();
        this.nbOrderThisMonth = in.readLong();

        this.revenueLastThreeMonth = in.createTypedArrayList(CustomerInfoRevenue.CREATOR);
        this.lastFiveOrders = in.createTypedArrayList(CustomerInfoRecentOrder.CREATOR);
    }


    @Override
    public String toString() {
        return getName() + " " + getCode();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeString(this.mobile);
        dest.writeString(this.phone);
        dest.writeString(this.email);
        dest.writeString(this.contact);
        dest.writeParcelable(this.location, 0);

        dest.writeSerializable(this.productivityLastMonth);
        dest.writeSerializable(this.productivityThisMonth);
        dest.writeLong(this.nbOrderThisMonth);

        dest.writeTypedList(revenueLastThreeMonth);
        dest.writeTypedList(lastFiveOrders);
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public BigDecimal getProductivityLastMonth() {
        return productivityLastMonth;
    }

    public void setProductivityLastMonth(BigDecimal productivityLastMonth) {
        this.productivityLastMonth = productivityLastMonth;
    }

    public BigDecimal getProductivityThisMonth() {
        return productivityThisMonth;
    }

    public void setProductivityThisMonth(BigDecimal productivityThisMonth) {
        this.productivityThisMonth = productivityThisMonth;
    }

    public long getNbOrderThisMonth() {
        return nbOrderThisMonth;
    }

    public void setNbOrderThisMonth(long nbOrderThisMonth) {
        this.nbOrderThisMonth = nbOrderThisMonth;
    }

    public List<CustomerInfoRevenue> getRevenueLastThreeMonth() {
        return revenueLastThreeMonth;
    }

    public void setRevenueLastThreeMonth(List<CustomerInfoRevenue> revenueLastThreeMonth) {
        this.revenueLastThreeMonth = revenueLastThreeMonth;
    }

    public List<CustomerInfoRecentOrder> getLastFiveOrders() {
        return lastFiveOrders;
    }

    public void setLastFiveOrders(List<CustomerInfoRecentOrder> lastFiveOrders) {
        this.lastFiveOrders = lastFiveOrders;
    }

}
