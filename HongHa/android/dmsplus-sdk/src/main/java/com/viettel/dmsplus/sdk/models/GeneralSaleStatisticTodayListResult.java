package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * @author duongnv18
 * @since 07/02/2017
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeneralSaleStatisticTodayListResult implements Parcelable, Serializable {

    public static final Creator<GeneralSaleStatisticTodayListResult> CREATOR = new Creator<GeneralSaleStatisticTodayListResult>() {
        public GeneralSaleStatisticTodayListResult createFromParcel(Parcel in) {
            return new GeneralSaleStatisticTodayListResult(in);
        }

        public GeneralSaleStatisticTodayListResult[] newArray(int size) {
            return new GeneralSaleStatisticTodayListResult[size];
        }
    };

    private String today;

    private SaleStatisticToday todayResult;
    private SaleStatisticToday thisMonthResult;
    private SaleStatisticToday lastMonthResult;




    public GeneralSaleStatisticTodayListResult() {

    }

    public GeneralSaleStatisticTodayListResult(Parcel in) {
        today = in.readString();
        todayResult = in.readParcelable(SaleStatisticToday.class.getClassLoader());

        thisMonthResult= in.readParcelable(SaleStatisticToday.class.getClassLoader());
        lastMonthResult = in.readParcelable(SaleStatisticToday.class.getClassLoader());

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(today);
        dest.writeParcelable(todayResult,flags);
        dest.writeParcelable(thisMonthResult,flags);
        dest.writeParcelable(lastMonthResult,flags);


    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public SaleStatisticToday getTodayResult() {
        return todayResult;
    }

    public void setTodayResult(SaleStatisticToday todayResult) {
        this.todayResult = todayResult;
    }
    public SaleStatisticToday getLastMonthResult() {
        return lastMonthResult;
    }

    public void setLastMonthResult(SaleStatisticToday lastMonthResult) {
        this.lastMonthResult = lastMonthResult;
    }

    public SaleStatisticToday getThisMonthResult() {
        return thisMonthResult;
    }

    public void setThisMonthResult(SaleStatisticToday thisMonthResult) {
        this.thisMonthResult = thisMonthResult;
    }

}
