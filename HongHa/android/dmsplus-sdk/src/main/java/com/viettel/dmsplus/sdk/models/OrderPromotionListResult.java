package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by PHAMHUNG on 4/26/2016.
 */
public class OrderPromotionListResult implements Parcelable {
    public static final Parcelable.Creator<OrderPromotionListResult> CREATOR = new Parcelable.Creator<OrderPromotionListResult>() {
        public OrderPromotionListResult createFromParcel(Parcel in) {
            return new OrderPromotionListResult(in);
        }

        public OrderPromotionListResult[] newArray(int size) {
            return new OrderPromotionListResult[size];
        }
    };

    @JsonProperty("list")
    private OrderPromotion[] items;
    private int count;

    public OrderPromotionListResult(){}
    public OrderPromotionListResult(Parcel in) {
        in.readTypedArray(items, OrderPromotion.CREATOR);
        count = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(count);
        dest.writeTypedArray(items, flags);
    }

    public OrderPromotion[] getItems() {
        return items;
    }

    public void setItems(OrderPromotion[] items) {
        this.items = items;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
