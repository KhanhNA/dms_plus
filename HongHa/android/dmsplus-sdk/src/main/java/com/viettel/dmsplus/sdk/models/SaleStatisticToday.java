package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SaleStatisticToday implements Parcelable {  //,Comparable<SaleStatisticToday>

    public static final Creator<SaleStatisticToday> CREATOR = new Creator<SaleStatisticToday>() {
        public SaleStatisticToday createFromParcel(Parcel in) {
            return new SaleStatisticToday(in);
        }

        public SaleStatisticToday[] newArray(int size) {
            return new SaleStatisticToday[size];
        }
    };

    //available

    private int nbDay;
    private String productivity;
    private double subRevenue;
    private int orderByDay;
    private int visitByDay;
    private double revenueByOrder;
    private DashboardInfoItem revenue;
    private DashboardInfoItem visit;
    private DashboardInfoItem visitHasOrder;
    private DashboardInfoItem visitErrorPosition;
    private DashboardInfoItem order;
    private DashboardInfoItem orderNoVisit;
    private DashboardInfoItem visitErrorDuration;



    public SaleStatisticToday() {
        super();
    }

    public SaleStatisticToday(Parcel in) {
        ClassLoader classLoader = getClass().getClassLoader();
        nbDay = in.readInt();
        productivity = in.readString();
        subRevenue = in.readDouble();
        orderByDay = in.readInt();
        visitByDay = in.readInt();
        revenueByOrder = in.readDouble();

        revenue  = in.readParcelable(classLoader);
        visit = in.readParcelable(classLoader);
        visitHasOrder = in.readParcelable(classLoader);
        visitErrorPosition = in.readParcelable(classLoader);
        order = in.readParcelable(classLoader);
        orderNoVisit = in.readParcelable(classLoader);
        visitErrorDuration = in.readParcelable(classLoader);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flag) {

        dest.writeInt(nbDay);
        dest.writeString(productivity);
        dest.writeDouble(subRevenue);
        dest.writeInt(orderByDay);
        dest.writeInt(visitByDay);
        dest.writeDouble(revenueByOrder);

        dest.writeParcelable(revenue, flag);
        dest.writeParcelable(visit , flag);
        dest.writeParcelable(visitHasOrder  , flag);
        dest.writeParcelable(visitErrorPosition   , flag);
        dest.writeParcelable(order, flag);
        dest.writeParcelable(orderNoVisit , flag);
        dest.writeParcelable(visitErrorDuration , flag);

    }

    /**
     * Compare base on isplanned
     * true: Noi tuyen
     * false : ngoai tuyen
     * Then compare base on status
     * 0: Dang tham 1:Chua tham 2: Da tham
     * 0 - 1 : ngoai tuyen
     */
//    @Override
//    public int compareTo(@NonNull SaleStatisticToday another) {
//        return 0;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        return super.equals(o);
//    }
//
//    @Override
//    public int hashCode() {
//        return super.hashCode();
//    }


    public int getNbDay() {
        return nbDay;
    }

    public void setNbDay(int nbDay) {
        this.nbDay = nbDay;
    }

    public String getProductivity() {
        return productivity;
    }

    public void setProductivity(String productivity) {
        this.productivity = productivity;
    }

    public double getSubRevenue() {
        return subRevenue;
    }

    public void setSubRevenue(double subRevenue) {
        this.subRevenue = subRevenue;
    }

    public int getOrderByDay() {
        return orderByDay;
    }

    public void setOrderByDay(int orderByDay) {
        this.orderByDay = orderByDay;
    }

    public int getVisitByDay() {
        return visitByDay;
    }

    public void setVisitByDay(int visitByDay) {
        this.visitByDay = visitByDay;
    }

    public double getRevenueByOrder() {
        return revenueByOrder;
    }

    public void setRevenueByOrder(double revenueByOrder) {
        this.revenueByOrder = revenueByOrder;
    }

    public DashboardInfoItem getRevenue() {
        return revenue;
    }

    public void setRevenue(DashboardInfoItem revenue) {
        this.revenue = revenue;
    }

    public DashboardInfoItem getVisit() {
        return visit;
    }

    public void setVisit(DashboardInfoItem visit) {
        this.visit = visit;
    }

    public DashboardInfoItem getVisitHasOrder() {
        return visitHasOrder;
    }

    public void setVisitHasOrder(DashboardInfoItem visitHasOrder) {
        this.visitHasOrder = visitHasOrder;
    }

    public DashboardInfoItem getVisitErrorPosition() {
        return visitErrorPosition;
    }

    public void setVisitErrorPosition(DashboardInfoItem visitErrorPosition) {
        this.visitErrorPosition = visitErrorPosition;
    }

    public DashboardInfoItem getOrder() {
        return order;
    }

    public void setOrder(DashboardInfoItem order) {
        this.order = order;
    }

    public DashboardInfoItem getOrderNoVisit() {
        return orderNoVisit;
    }

    public void setOrderNoVisit(DashboardInfoItem orderNoVisit) {
        this.orderNoVisit = orderNoVisit;
    }

    public DashboardInfoItem getVisitErrorDuration() {
        return visitErrorDuration;
    }

    public void setVisitErrorDuration(DashboardInfoItem visitErrorDuration) {
        this.visitErrorDuration = visitErrorDuration;
    }
}
