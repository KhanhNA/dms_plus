package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.viettel.dmsplus.sdk.Role;

import java.util.List;

/**
 * @author PHAMHUNG
 * @since 8/17/2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo implements Parcelable {

    /*Module Config*/
    public static final String CHECK_IN = "CHECK_IN";
    public static final String PROMOTION = "PROMOTION";
    public static final String SURVEY = "SURVEY";
    public static final String EXCHANGE_RETURN = "EXCHANGE_RETURN";
    public static final String INVENTORY = "INVENTORY";

    public static final Creator<UserInfo> CREATOR = new Creator<UserInfo>() {
        public UserInfo createFromParcel(Parcel in) {
            return new UserInfo(in);
        }

        public UserInfo[] newArray(int size) {
            return new UserInfo[size];
        }
    };

    /* User Credential*/
    private String id;
    private String username;
    private String fullname;
    private String clientName;
    @JsonProperty("roleCode")
    private Role role;

    /* Default Config*/
    private Location location;
    private String dateFormat;
    private boolean canEditCustomerLocation;
    private double visitDistanceKPI;
    private long visitDurationKPI;
    private boolean vanSales;


    private List<String> modules;

    public UserInfo() {
    }

    public UserInfo(Parcel in) {
        id = in.readString();
        username = in.readString();
        fullname = in.readString();
        role = in.readParcelable(Role.class.getClassLoader());
        clientName = in.readString();
        in.readStringList(modules);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(username);
        dest.writeString(fullname);
        dest.writeParcelable(role, flags);
        dest.writeString(clientName);
        dest.writeStringList(modules);
    }

    //region get/set
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public boolean isCanEditCustomerLocation() {
        return canEditCustomerLocation;
    }

    public void setCanEditCustomerLocation(boolean canEditCustomerLocation) {
        this.canEditCustomerLocation = canEditCustomerLocation;
    }

    public double getVisitDistanceKPI() {
        return visitDistanceKPI;
    }

    public void setVisitDistanceKPI(double visitDistanceKPI) {
        this.visitDistanceKPI = visitDistanceKPI;
    }

    public long getVisitDurationKPI() {
        return visitDurationKPI;
    }

    public void setVisitDurationKPI(long visitDurationKPI) {
        this.visitDurationKPI = visitDurationKPI;
    }

    public boolean isVanSales() {
        return vanSales;
    }

    public void setVanSales(boolean vanSales) {
        this.vanSales = vanSales;
    }

    public String getClientName() {
        return clientName;
    }


    public boolean isUseInventory() {
        return (modules !=null && modules.contains(INVENTORY));
    }


    public boolean isUseExchangeReturn() {
        return (modules !=null && modules.contains(EXCHANGE_RETURN));
    }

    public boolean isUseCheckIn() {
        return (modules !=null && modules.contains(CHECK_IN));
    }


    public boolean isUsePromotion() {
        return (modules !=null && modules.contains(PROMOTION));
    }


    public boolean isUseSurvey() {
        return (modules !=null && modules.contains(SURVEY));
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }
    //endregion
}
