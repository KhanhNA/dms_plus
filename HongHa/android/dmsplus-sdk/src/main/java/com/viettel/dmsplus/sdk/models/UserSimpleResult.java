package com.viettel.dmsplus.sdk.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.viettel.dmsplus.sdk.network.IsoShortDateDeserializer;

import java.util.Date;

/**
 * @author Thanh
 * @since 3/10/2015
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSimpleResult implements Parcelable {

    public static final Creator<UserSimpleResult> CREATOR = new Creator<UserSimpleResult>() {
        public UserSimpleResult createFromParcel(Parcel in) {
            return new UserSimpleResult(in);
        }

        public UserSimpleResult[] newArray(int size) {
            return new UserSimpleResult[size];
        }
    };

    private UserSimple[] list;
    private int count;

    public UserSimpleResult() {

    }

    public UserSimpleResult(Parcel in) {
        list = in.createTypedArray(UserSimple.CREATOR);
        count = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedArray(list, flags);
        dest.writeInt(count);
    }

    public UserSimple[] getList() {
        return list;
    }

    public void setList(UserSimple[] list) {
        this.list = list;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
