package com.viettel.dmsplus.sdk.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by duongnv18 on 03/03/2017.
 */

public class MethodUtils {
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    public static Date getDatewithformat(String strDate) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        java.util.Date d1 = format.parse(strDate);
        return d1;
    }

    public static String minusDate(java.util.Date d1, java.util.Date d2) {
        long diff = d2.getTime() - d1.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;

        return "(" + diffMinutes + "m " + diffSeconds + "s" + ")";

    }
}
