package com.viettel.backend.cache.redis;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.redis.cache.DefaultRedisCachePrefix;
import org.springframework.data.redis.cache.RedisCachePrefix;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

public class A_CacheManager implements Serializable, InitializingBean{

    private static final long serialVersionUID = 1L;
    
    private static final StringRedisSerializer STRING_REDIS_SERIALIZER = new StringRedisSerializer();
    
    @SuppressWarnings("rawtypes")
    private final ConcurrentMap<String, BoundKeyCache> cacheMap = new ConcurrentHashMap<String, BoundKeyCache>(16);
    private Set<String> cacheNames = new LinkedHashSet<String>(16);
    private RedisConnectionFactory connectionFactory;
    private RedisCachePrefix cachePrefix = new DefaultRedisCachePrefix();

    public A_CacheManager(RedisConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }
    
    @Override
    public void afterPropertiesSet() {
        this.cacheMap.clear();
        this.cacheNames.clear();
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected <K, V> BoundValueCache<K, V> getBoundValueCache(String cacheName, Class<K> keyType, Class<V> valueType) {
        BoundValueCache cache = (BoundValueCache) lookupCache(cacheName);
        if (cache != null) {
            return cache;
        } else {
            byte[] prefix = cachePrefix.prefix(cacheName);
            RedisTemplate<K, V> template = constructTemplate(connectionFactory, keyType, valueType, prefix);
            cache = new BoundValueCache<K, V>(cacheName, prefix, template);
            addCache(cache);
            return (BoundValueCache<K, V>) lookupCache(cacheName);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected <K, V> BoundListCache<K, V> getBoundListCache(String cacheName, Class<K> keyType, Class<V> valueType) {
        BoundListCache cache = (BoundListCache) lookupCache(cacheName);
        if (cache != null) {
            return cache;
        } else {
            byte[] prefix = cachePrefix.prefix(cacheName);
            RedisTemplate<K, V> template = constructTemplate(connectionFactory, keyType, valueType, prefix);
            cache = new BoundListCache<K, V>(cacheName, prefix, template);
            addCache(cache);
            return (BoundListCache<K, V>) lookupCache(cacheName);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected <K, V> BoundSetCache<K, V> getBoundSetCache(String cacheName, Class<K> keyType, Class<V> valueType) {
        BoundSetCache cache = (BoundSetCache) lookupCache(cacheName);
        if (cache != null) {
            return cache;
        } else {
            byte[] prefix = cachePrefix.prefix(cacheName);
            RedisTemplate<K, V> template = constructTemplate(connectionFactory, keyType, valueType, prefix);
            cache = new BoundSetCache<K, V>(cacheName, prefix, template);
            addCache(cache);
            return (BoundSetCache<K, V>) lookupCache(cacheName);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected <K, V> BoundZSetCache<K, V> getBoundZSetCache(String cacheName, Class<K> keyType, Class<V> valueType) {
        BoundZSetCache cache = (BoundZSetCache) lookupCache(cacheName);
        if (cache != null) {
            return cache;
        } else {
            byte[] prefix = cachePrefix.prefix(cacheName);
            RedisTemplate<K, V> template = constructTemplate(connectionFactory, keyType, valueType, prefix);
            cache = new BoundZSetCache<K, V>(cacheName, prefix, template);
            addCache(cache);
            return (BoundZSetCache<K, V>) lookupCache(cacheName);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected <H, HK, HV> BoundHashCache<H, HK, HV> getBoundHashCache(String cacheName, Class<H> keyType,
            Class<HK> hashKeyType, Class<HV> hashValueType) {
        BoundHashCache cache = (BoundHashCache) lookupCache(cacheName);
        if (cache != null) {
            return cache;
        } else {
            byte[] prefix = cachePrefix.prefix(cacheName);
            RedisTemplate<H, Object> template = constructTemplate(connectionFactory, keyType, null, prefix);
            // BoundHashCache need additional initialization
            template.setHashKeySerializer(getSerializerForType(hashKeyType));
            template.setHashValueSerializer(getSerializerForType(hashValueType));
            cache = new BoundHashCache<H, HK, HV>(cacheName, prefix, template);
            addCache(cache);
            return (BoundHashCache<H, HK, HV>) lookupCache(cacheName);
        }
    }

    @SuppressWarnings("rawtypes")
    protected final void addCache(BoundKeyCache cache) {
        this.cacheMap.put(cache.getName(), cache);
        this.cacheNames.add(cache.getName());
    }

    @SuppressWarnings("rawtypes")
    protected final BoundKeyCache lookupCache(String name) {
        return this.cacheMap.get(name);
    }

    private <K, V> RedisTemplate<K, V> constructTemplate(RedisConnectionFactory connectionFactory, Class<K> keyType,
            Class<V> valueType, byte[] prefix) {
        RedisTemplate<K, V> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer(getKeySerializerForType(keyType, prefix));
        if (valueType != null) {
            template.setValueSerializer(getSerializerForType(valueType));
        }
        template.afterPropertiesSet();
        return template;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private <T> RedisSerializer getKeySerializerForType(Class<T> type, byte[] prefix) {
        if (type.equals(String.class)) {
            return (RedisSerializer<T>) new PrefixSupportStringRedisSerializer(prefix);
        }
        if (isPrimitive(type)) {
            return new PrefixSupportGenericToStringSerializer<T>(prefix, type);
        }
        return new PrefixSupportJdkSerializationRedisSerializer(prefix);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private <T> RedisSerializer getSerializerForType(Class<T> type) {
        if (type.equals(String.class)) {
            return (RedisSerializer<T>) STRING_REDIS_SERIALIZER;
        }
        if (isPrimitive(type)) {
            return new GenericToStringSerializer<T>(type);
        }
        return new JdkSerializationRedisSerializer();
    }

    private static boolean isPrimitive(Class<?> type) {
        if (type.isPrimitive()) {
            return true;
        }
        if (Number.class.isAssignableFrom(type) || type == Boolean.class || type == Character.class) {
            return true;
        }
        return false;
    }
    
}
