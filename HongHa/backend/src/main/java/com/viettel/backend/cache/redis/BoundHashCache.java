package com.viettel.backend.cache.redis;

import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;

public class BoundHashCache<H, HK, HV> extends BoundKeyCache<H> {
    
    public BoundHashCache(String name, byte[] prefix, RedisTemplate<? extends Object, ? extends Object> template) {
        super(name, prefix, template);
    }

    @SuppressWarnings("unchecked")
    public <T> BoundHashOperations<H, HK, HV> get(H key) {
        return template.boundHashOps(key);
    }

}
