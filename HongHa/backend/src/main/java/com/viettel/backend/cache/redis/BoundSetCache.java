package com.viettel.backend.cache.redis;

import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.RedisTemplate;

public class BoundSetCache<K, V> extends BoundKeyCache<K> {
    
    public BoundSetCache(String name, byte[] prefix, RedisTemplate<? extends Object, ? extends Object> template) {
        super(name, prefix, template);
    }

    @SuppressWarnings("unchecked")
    public BoundSetOperations<K, V> get(K key) {
        return template.boundSetOps(key);
    }
    
}
