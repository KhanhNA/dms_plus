package com.viettel.backend.cache.redis;

import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;

public class BoundValueCache<K, V> extends BoundKeyCache<K> {
    
    public BoundValueCache(String name, byte[] prefix, RedisTemplate<? extends Object, ? extends Object> template) {
        super(name, prefix, template);
    }

    @SuppressWarnings("unchecked")
    public BoundValueOperations<K, V> get(K key) {
        return template.boundValueOps(key);
    }

}