package com.viettel.backend.cache.redis;

import org.bson.types.ObjectId;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;

/**
 * @author thanh Pre-defined some common caches
 */
public class CacheManager extends A_CacheManager {

    private static final long serialVersionUID = 1L;

    private static final String SYSTEM_INFO = "SYSTEM_INFO";

    private static final String CACHE_SALES_CONFIG = "CACHE_SALES_CONFIG";
    private static final String CACHE_CLIENT_CONFIG = "CACHE_CLIENT_CONFIG";
    private static final String CACHE_WORKING_DAYS = "CACHE_WORKING_DAYS";

    private static final String CACHE_DISTRIBUTOR_DATA = "CACHE_DISTRIBUTOR_DATA";
    public static final String DISTRIBUTOR_DATA_TYPE_REVENUE = "REVENUE";
    public static final String DISTRIBUTOR_DATA_TYPE_QUANTITY = "QUANTITY";
    public static final String DISTRIBUTOR_DATA_TYPE_NBORDER = "NBORDER";
    public static final String DISTRIBUTOR_DATA_TYPE_NBORDER_WITHOUT_VISIT = "NBORDER_WITHOUT_VISIT";
    public static final String DISTRIBUTOR_DATA_TYPE_NBVISIT = "NBVISIT";
    public static final String DISTRIBUTOR_DATA_TYPE_NBVISIT_WITH_PO = "NBVISIT_WITH_PO";
    public static final String DISTRIBUTOR_DATA_TYPE_NBVISIT_ERROR_DURATION = "NBVISIT_ERROR_DURATION";
    public static final String DISTRIBUTOR_DATA_TYPE_NBVISIT_ERROR_POSITION = "NBVISIT_ERROR_POSITION";

    private static final String CACHE_DISTRIBUTOR_SALESMAN_REVENUE = "CACHE_DISTRIBUTOR_SALESMAN_REVENUE";
    
    private static final String CACHE_DISTRIBUTOR_PRODUCT_QUANTITY = "CACHE_DISTRIBUTOR_PRODUCT_QUANTITY";
    private static final String CACHE_ADMIN_PRODUCT_QUANTITY = "CACHE_ADMIN_PRODUCT_QUANTITY";
    
    private static final String CACHE_DISTRIBUTOR_SALESMAN_VISISTED = "CACHE_DISTRIBUTOR_SALESMAN_VISISTED";
    private static final String CACHE_SCHEDULE_NUMBER_PLANNED = "CACHE_SCHEDULE_NUMBER_PLANNED";

    public CacheManager(RedisConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    public void clearAll() {
        getClientConfigCache().clear();
        getSalesConfigCache().clear();
        getWorkingDaysCache().clear();
        getDistributorDataCache().clear();
        getDistributorSalesmanRevenueCache().clear();
        getDistributorProductQuantityCache().clear();
        getAdminProductQuantityCache().clear();
        getDistributorSalesmanVisitedCache().clear();
        getScheduleNumberPlannedCache().clear();
    }

    public BoundValueCache<String, String> getSystemInfo() {
        return getBoundValueCache(SYSTEM_INFO, String.class, String.class);
    }

    public BoundValueCache<ObjectId, SalesConfig> getSalesConfigCache() {
        return getBoundValueCache(CACHE_SALES_CONFIG, ObjectId.class, SalesConfig.class);
    }

    public BoundValueCache<ObjectId, ClientConfig> getClientConfigCache() {
        return getBoundValueCache(CACHE_CLIENT_CONFIG, ObjectId.class, ClientConfig.class);
    }

    public BoundListCache<Period, SimpleDate> getWorkingDaysCache() {
        return getBoundListCache(CACHE_WORKING_DAYS, Period.class, SimpleDate.class);
    }

    public BoundHashCache<String, String, Double> getDistributorDataCache() {
        return getBoundHashCache(CACHE_DISTRIBUTOR_DATA, String.class, String.class, Double.class);
    }

    public BoundHashCache<String, ObjectId, Double> getDistributorSalesmanRevenueCache() {
        return getBoundHashCache(CACHE_DISTRIBUTOR_SALESMAN_REVENUE, String.class, ObjectId.class, Double.class);
    }
    
    public BoundHashCache<String, ObjectId, Integer> getDistributorProductQuantityCache() {
        return getBoundHashCache(CACHE_DISTRIBUTOR_PRODUCT_QUANTITY, String.class, ObjectId.class, Integer.class);
    }

    public BoundHashCache<String, ObjectId, Integer> getAdminProductQuantityCache() {
        return getBoundHashCache(CACHE_ADMIN_PRODUCT_QUANTITY, String.class, ObjectId.class, Integer.class);
    }

    public BoundHashCache<String, ObjectId, Integer> getDistributorSalesmanVisitedCache() {
        return getBoundHashCache(CACHE_DISTRIBUTOR_SALESMAN_VISISTED, String.class, ObjectId.class, Integer.class);
    }

    /**
     * key 1: routeId
     * key 2: day_week
     * value: number customer to planned to visit
     */
    public BoundHashCache<String, String, Integer> getScheduleNumberPlannedCache() {
        return getBoundHashCache(CACHE_SCHEDULE_NUMBER_PLANNED, String.class, String.class, Integer.class);
    }

}
