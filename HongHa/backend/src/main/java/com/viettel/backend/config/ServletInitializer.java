package com.viettel.backend.config;

import javax.servlet.FilterRegistration.Dynamic;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;
import org.springframework.web.context.AbstractContextLoaderInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import com.viettel.backend.config.restful.RestfulConfig;
import com.viettel.backend.config.root.ApplicationConfig;
import com.viettel.backend.config.web.WebMvcConfig;

/**
 * Servlet initiator with Spring Http Session enabled
 * Using {@link AbstractContextLoaderInitializer} for default session
 * @author thanh
 */
public class ServletInitializer extends AbstractHttpSessionApplicationInitializer {
	
    public ServletInitializer() {
        super(ApplicationConfig.class);
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        super.onStartup(servletContext);

        // Create api dispatcher servlet context
        AnnotationConfigWebApplicationContext apiServletContext = new AnnotationConfigWebApplicationContext();
        apiServletContext.register(RestfulConfig.class);
        apiServletContext.scan("com.viettel.backend.config.restful");

        ServletRegistration.Dynamic apiDispatcherServlet = servletContext.addServlet("apiDispatcherServlet",
                new DispatcherServlet(apiServletContext));
        apiDispatcherServlet.setLoadOnStartup(1);
        apiDispatcherServlet.setAsyncSupported(true);
        apiDispatcherServlet.addMapping("/api/*");

        // Create default dispatcher servlet context
        AnnotationConfigWebApplicationContext dispatcherServletContext = new AnnotationConfigWebApplicationContext();
        dispatcherServletContext.register(WebMvcConfig.class);
        dispatcherServletContext.scan("com.viettel.backend.config.web");

        ServletRegistration.Dynamic dispatcherServlet = servletContext.addServlet("dispatcher", 
        		new DispatcherServlet(dispatcherServletContext));
        dispatcherServlet.setLoadOnStartup(3);
        dispatcherServlet.setAsyncSupported(true);
        dispatcherServlet.addMapping("/");

        Dynamic encodingFilter = servletContext.addFilter("encoding-filter", CharacterEncodingFilter.class);
        encodingFilter.addMappingForUrlPatterns(null, false, "/*");
        encodingFilter.setInitParameter("encoding", "UTF-8");
        encodingFilter.setInitParameter("forceEncoding", "true");
        
        DelegatingFilterProxy corsFilter = new DelegatingFilterProxy("corsFilter");
        servletContext.addFilter("corsFilter", corsFilter).addMappingForUrlPatterns(null, false, "/*");
        
        DelegatingFilterProxy securityFilter = new DelegatingFilterProxy("springSecurityFilterChain");
        // filter.setContextAttribute("org.springframework.web.servlet.FrameworkServlet.CONTEXT.dispatcher");
        servletContext.addFilter("springSecurityFilterChain", securityFilter).addMappingForUrlPatterns(null, false, "/*");
    }

}
