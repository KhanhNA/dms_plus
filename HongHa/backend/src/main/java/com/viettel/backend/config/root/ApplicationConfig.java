package com.viettel.backend.config.root;

import org.bson.types.ObjectId;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;

import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.engine.promotion.PromotionEngine;

@Configuration
@PropertySource("classpath:config.properties")
@ComponentScan(basePackages = { 
        "com.viettel.backend.config.root",
        "com.viettel.backend.util",
        "com.viettel.backend.service",
		"com.viettel.backend.repository" })
public class ApplicationConfig {
	
	@Bean
    public MessageSource messageSource() {
        final ReloadableResourceBundleMessageSource ret = new ReloadableResourceBundleMessageSource();
        ret.setBasename("classpath:language");
        ret.setDefaultEncoding("UTF-8");
        return ret;
    }
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
    
    @SuppressWarnings("rawtypes")
    @Bean
    public PromotionEngine promotionEngine() {
    	return new PromotionEngine<ObjectId, ProductEmbed>();
    }
    
}
