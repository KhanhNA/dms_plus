package com.viettel.backend.config.root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.user.UserSessionRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.StompWebSocketEndpointRegistration;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;

import com.viettel.backend.websocket.core.OAuth2TokenHandshakeHandler;
import com.viettel.backend.websocket.core.OAuth2TokenWebSocketAuthenticationHandler;
import com.viettel.backend.websocket.core.RedisUserSessionRegistry;
import com.viettel.backend.websocket.core.StompLoginSubProtocolWebSocketHandlerDecoratorFactory;
import com.viettel.backend.websocket.core.WebSocketAuthenticationHandler;

@Configuration
@EnableWebSocketMessageBroker
@EnableScheduling
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Autowired
    private TokenStore tokenStore;
    
    @Autowired
    private RedisConnectionFactory redisConnectionFactory;
    
    @Value("${cors.allowed.origins}")
	private String allowedOrigins;
    
    @Value("${broker.stomp.host}")
    private String stompBrokerHost;
    
    @Value("${broker.stomp.port:0}")
    private int stompBrokerPort;
    
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        // If STOMP broker not configured, create an simple fallback
        if (!StringUtils.isEmpty(stompBrokerHost) || stompBrokerPort <= 0) {
            config.enableStompBrokerRelay("/topic", "/queue")
                    .setRelayHost(stompBrokerHost)
                    .setRelayPort(stompBrokerPort)
                    ;
        } else {
            config.enableSimpleBroker("/topic", "/queue");
        }
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
    	StompWebSocketEndpointRegistration endpoint = registry.addEndpoint("/websocket");
    	// If no allowed origin, mean only allow from same domain
    	if (!StringUtils.isEmpty(allowedOrigins)) {
        	String[] arrAllowedOrigins = allowedOrigins.split(",");
    		endpoint.setAllowedOrigins(arrAllowedOrigins);
    	}
        endpoint.setHandshakeHandler(new OAuth2TokenHandshakeHandler());
    	endpoint.withSockJS();
    }
    
    @Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
        registration.addDecoratorFactory(loginSubProtocolWebSocketHandlerDecoratorFactory());
    }
    
    @Bean
    public StompLoginSubProtocolWebSocketHandlerDecoratorFactory loginSubProtocolWebSocketHandlerDecoratorFactory() {
        return new StompLoginSubProtocolWebSocketHandlerDecoratorFactory(webSocketAuthenticationProvider());
    }
    
    @Bean
    public WebSocketAuthenticationHandler webSocketAuthenticationProvider() {
        return new OAuth2TokenWebSocketAuthenticationHandler(tokenStore);
    }
    
    @Bean
    public UserSessionRegistry userSessionRegistry() {
        return new RedisUserSessionRegistry(redisConnectionFactory);
    }
    
}
