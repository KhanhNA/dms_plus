package com.viettel.backend.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.entity.SimpleDate;

@Document(collection = "CalendarConfig")
public class CalendarConfig extends PO {

    private static final long serialVersionUID = 7763979551635207290L;

    private List<Integer> workingDays;
    private List<SimpleDate> everyYearHolidays;
    private List<SimpleDate> exceptionHolidays;
    private List<SimpleDate> exceptionWorkingDays;

    public List<Integer> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<Integer> workingDays) {
        this.workingDays = workingDays;
    }

    public List<SimpleDate> getEveryYearHolidays() {
        return everyYearHolidays;
    }

    public void setEveryYearHolidays(List<SimpleDate> everyYearHolidays) {
        this.everyYearHolidays = everyYearHolidays;
    }

    public List<SimpleDate> getExceptionHolidays() {
        return exceptionHolidays;
    }

    public void setExceptionHolidays(List<SimpleDate> exceptionHolidays) {
        this.exceptionHolidays = exceptionHolidays;
    }

    public List<SimpleDate> getExceptionWorkingDays() {
        return exceptionWorkingDays;
    }

    public void setExceptionWorkingDays(List<SimpleDate> exceptionWorkingDays) {
        this.exceptionWorkingDays = exceptionWorkingDays;
    }

}
