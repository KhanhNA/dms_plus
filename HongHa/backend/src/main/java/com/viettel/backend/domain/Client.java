package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.annotation.ClientRootFixed;
import com.viettel.backend.domain.annotation.ClientRootInclude;

/**
 * @author auto-gen
 */

@ClientRootFixed
@ClientRootInclude
@Document(collection = "Client")
public class Client extends NameCategory {

    private static final long serialVersionUID = 4538417826817640859L;

    public Client() {
        super();
    }

}
