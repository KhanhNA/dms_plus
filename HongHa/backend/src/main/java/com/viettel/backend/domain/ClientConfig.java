package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.entity.SimpleFile;

@Document(collection = "ClientConfig")
public class ClientConfig extends PO {

    private static final long serialVersionUID = -4995404568124100927L;

    // DEFAULT VALUE
    private String defaultProductPhoto;
    private String defaultCustomerPhoto;
    private String defaultDateFormat;

    // CALENDAR INDEX
    private int firstDayOfWeek;
    private int minimalDaysInFirstWeek;
    private int numberWeekOfFrequency;
    private boolean complexSchedule;
    
    // NUMBER DAY ORDER PENDING EXPIRE
    private int numberDayOrderPendingExpire;
    
    private SimpleFile importCustomerTemplate;
    private SimpleFile importProductTemplate;
    
    public String getDefaultProductPhoto() {
        return defaultProductPhoto;
    }

    public void setDefaultProductPhoto(String defaultProductPhoto) {
        this.defaultProductPhoto = defaultProductPhoto;
    }

    public String getDefaultCustomerPhoto() {
        return defaultCustomerPhoto;
    }

    public void setDefaultCustomerPhoto(String defaultCustomerPhoto) {
        this.defaultCustomerPhoto = defaultCustomerPhoto;
    }

    public String getDefaultDateFormat() {
        return defaultDateFormat;
    }

    public void setDefaultDateFormat(String defaultDateFormat) {
        this.defaultDateFormat = defaultDateFormat;
    }

    public int getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(int firstDayOfWeek) {
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public int getMinimalDaysInFirstWeek() {
        return minimalDaysInFirstWeek;
    }

    public void setMinimalDaysInFirstWeek(int minimalDaysInFirstWeek) {
        this.minimalDaysInFirstWeek = minimalDaysInFirstWeek;
    }

    public int getNumberWeekOfFrequency() {
        return numberWeekOfFrequency;
    }

    public void setNumberWeekOfFrequency(int numberWeekOfFrequency) {
        this.numberWeekOfFrequency = numberWeekOfFrequency;
    }
    
    public boolean isComplexSchedule() {
        return complexSchedule;
    }
    
    public void setComplexSchedule(boolean complexSchedule) {
        this.complexSchedule = complexSchedule;
    }
    
    public int getNumberDayOrderPendingExpire() {
        return numberDayOrderPendingExpire;
    }
    
    public void setNumberDayOrderPendingExpire(int numberDayOrderPendingExpire) {
        this.numberDayOrderPendingExpire = numberDayOrderPendingExpire;
    }
    
    public SimpleFile getImportCustomerTemplate() {
        return importCustomerTemplate;
    }
    
    public void setImportCustomerTemplate(SimpleFile importCustomerTemplate) {
        this.importCustomerTemplate = importCustomerTemplate;
    }
    
    public SimpleFile getImportProductTemplate() {
        return importProductTemplate;
    }
    
    public void setImportProductTemplate(SimpleFile importProductTemplate) {
        this.importProductTemplate = importProductTemplate;
    }

}
