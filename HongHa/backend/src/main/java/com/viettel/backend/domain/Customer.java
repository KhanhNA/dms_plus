package com.viettel.backend.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.entity.SimpleDate;

@Document(collection = "Customer")
public class Customer extends NameCategory implements I_NeedApprovePO {

    private static final long serialVersionUID = -1182418810917243398L;

    public static final String COLUMNNAME_CODE = "code";
    public static final String COLUMNNAME_MOBILE = "mobile";
    public static final String COLUMNNAME_PHONE = "phone";
    public static final String COLUMNNAME_LOCATION = "location";
    public static final String COLUMNNAME_CONTACT = "contact";
    public static final String COLUMNNAME_ADDRESS = "address";
    public static final String COLUMNNAME_CUSTOMER_TYPE_ID = "customerType.id";
    public static final String COLUMNNAME_CREATED_BY_FULLNAME = "createdBy.fullname";
    public static final String COLUMNNAME_CREATED_BY_ID = "createdBy.id";
    public static final String COLUMNNAME_CREATED_TIME_VALUE = "createdTime.value";
    public static final String COLUMNNAME_DISTRIBUTOR = "distributor";
    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor.id";
    public static final String COLUMNNAME_DISTRIBUTOR_NAME = "distributor.name";
    public static final String COLUMNNAME_DISTRIBUTOR_CODE = "distributor.code";
    public static final String COLUMNNAME_SCHEDULE = "schedule";

    private String code;
    private String mobile;
    private String phone;
    private String contact;
    private String idNumber;
    private String email;
    private String address;
    private List<String> photos;

    private double[] location; // (x,y) -> (Longtitude, Latitude)

    private CustomerType customerType;
    private District district;

    private String description;

    private UserEmbed createdBy;
    private SimpleDate createdTime;

    private DistributorEmbed distributor;
    private CustomerSchedule schedule;

    private int approveStatus;

    public Customer() {
        super();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }
    
    public District getDistrict() {
        return district;
    }
    
    public void setDistrict(District district) {
        this.district = district;
    }

    public CustomerType getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CustomerType customerType) {
        this.customerType = customerType;
    }

    public UserEmbed getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserEmbed createdBy) {
        this.createdBy = createdBy;
    }

    public SimpleDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        this.createdTime = createdTime;
    }

    public DistributorEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbed distributor) {
        this.distributor = distributor;
    }

    public CustomerSchedule getSchedule() {
        return schedule;
    }

    public void setSchedule(CustomerSchedule schedule) {
        this.schedule = schedule;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }

}
