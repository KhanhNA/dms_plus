package com.viettel.backend.domain;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.entity.SimpleDate;

@Document(collection = "CustomerCheck")
public class CustomerCheck extends PO {

    private static final long serialVersionUID = -1182418810917243398L;

    public static final String COLUMNNAME_CUSTOMER = "customer";
    public static final String COLUMNNAME_CUSTOMER_ID = "customer.id";
    public static final String COLUMNNAME_CUSTOMER_DISTRICT_NAME = "customer.district.name";
    public static final String COLUMNNAME_STORECHECKER = "storeChecker";
    public static final String COLUMNNAME_STORECHECKER_ID = "storeChecker.id";
    public static final String COLUMNNAME_SALESMAN = "salesman";
    public static final String COLUMNNAME_SALESMAN_ID = "salesman.id";
    public static final String COLUMNNAME_CREATED_TIME_VALUE = "createdTime.value";
    public static final String COLUMNNAME_PASS = "passed";

    private CustomerEmbed customer;
    private UserEmbed storeChecker;
    private UserEmbed salesman;
    private DistributorEmbed distributor;
    private SimpleDate createdTime;
    private boolean passed;
    private String note;
    private boolean hasOrder;
    private SimpleDate visitDate;

    public CustomerCheck() {
        super();
    }

    public CustomerCheck(ObjectId clientId, ObjectId id) {
        super(clientId, id);
    }

    public CustomerEmbed getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEmbed customer) {
        this.customer = customer;
    }

    public UserEmbed getStoreChecker() {
        return storeChecker;
    }

    public void setStoreChecker(UserEmbed storeChecker) {
        this.storeChecker = storeChecker;
    }

    public UserEmbed getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbed salesman) {
        this.salesman = salesman;
    }

    public SimpleDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        this.createdTime = createdTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public DistributorEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbed distributor) {
        this.distributor = distributor;
    }

    public boolean isHasOrder() {
        return hasOrder;
    }

    public void setHasOrder(boolean hasOrder) {
        this.hasOrder = hasOrder;
    }

    public SimpleDate getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(SimpleDate visitDate) {
        this.visitDate = visitDate;
    }

}
