package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "CustomerType")
public class CustomerType extends NameCategory {

    private static final long serialVersionUID = -5002224531838381367L;

    public CustomerType() {
        super();
    }

}
