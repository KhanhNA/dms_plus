package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.UserEmbed;

@Document(collection = "Distributor")
public class Distributor extends NameCategory {

    /**
     * 
     */
    private static final long serialVersionUID = 8782304292353101761L;

    public static final String COLUMNNAME_CODE = "code";
    public static final String COLUMNNAME_ADDRESS = "address";
    public static final String COLUMNNAME_CUSTOMER_IDS = "customerIds";
    public static final String COLUMNNAME_SUPERVISOR_ID = "supervisor.id";

    private String code;
    private String address;
    private UserEmbed supervisor;

    public Distributor() {
        super();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public UserEmbed getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(UserEmbed supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public String[] getSearchValues() {
        return new String[] { code, getName(), address };
    }
}
