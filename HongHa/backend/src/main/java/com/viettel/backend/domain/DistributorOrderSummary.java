package com.viettel.backend.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.domain.embed.UserEmbed;

public class DistributorOrderSummary implements Serializable {
    
    private static final long serialVersionUID = -1202147311063291443L;

    private DistributorEmbed distributor;

    private int nbOrder;
    private BigDecimal revenue;
    private Set<ObjectId> salesmanIds;
    private List<SalesmanOrderSummary> salesmanOrderSummaries;
    private List<ProductOrderSummary> productOrderSummaries;

    public DistributorOrderSummary(DistributorEmbed distributor, int nbOrder, BigDecimal revenue, Set<ObjectId> salesmanIds,
            List<SalesmanOrderSummary> salesmanOrderSummaries, List<ProductOrderSummary> productOrderSummaries) {
        if (distributor == null) {
            throw new IllegalArgumentException("distributor is null");
        }

        this.distributor = new DistributorEmbed(distributor.getId(), distributor.getCode(), distributor.getName());
        this.nbOrder = nbOrder;
        this.revenue = revenue;
        this.salesmanIds = salesmanIds;
        this.salesmanOrderSummaries = salesmanOrderSummaries;
        this.productOrderSummaries = productOrderSummaries;
    }

    public DistributorEmbed getDistributor() {
        return distributor;
    }

    public int getNbOrder() {
        return nbOrder;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }
    
    public Set<ObjectId> getSalesmanIds() {
        return salesmanIds;
    }

    public Collection<SalesmanOrderSummary> getSalesmanOrderSummaries() {
        return salesmanOrderSummaries;
    }

    public Collection<ProductOrderSummary> getProductOrderSummaries() {
        return productOrderSummaries;
    }

    public static class SalesmanOrderSummary implements Serializable {
        
        private static final long serialVersionUID = 1906226120241807112L;

        private UserEmbed salesman;

        private int nbOrder;
        private BigDecimal revenue;

        public SalesmanOrderSummary(UserEmbed salesman, int nbOrder, BigDecimal revenue) {
            if (salesman == null) {
                throw new IllegalArgumentException("salesman is null");
            }

            this.salesman = new UserEmbed(salesman.getId(), salesman.getUsername(), salesman.getFullname());
            this.nbOrder = nbOrder;
            this.revenue = revenue;
        }

        public UserEmbed getSalesman() {
            return salesman;
        }

        public int getNbOrder() {
            return nbOrder;
        }

        public BigDecimal getRevenue() {
            return revenue;
        }

    }

    public static class ProductOrderSummary implements Serializable {
        
        private static final long serialVersionUID = 7639697220797950167L;

        private ProductEmbed product;

        private int nbOrder;
        private BigDecimal revenue;
        private BigDecimal quantity;

        public ProductOrderSummary(ProductEmbed product, int nbOrder, BigDecimal revenue, BigDecimal quantity) {
            if (product == null) {
                throw new IllegalArgumentException("product is null");
            }

            this.product = new ProductEmbed(product);
            this.nbOrder = nbOrder;
            this.revenue = revenue;
            this.quantity = quantity;
        }

        public ProductEmbed getProduct() {
            return product;
        }

        public int getNbOrder() {
            return nbOrder;
        }

        public BigDecimal getRevenue() {
            return revenue;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

    }

}
