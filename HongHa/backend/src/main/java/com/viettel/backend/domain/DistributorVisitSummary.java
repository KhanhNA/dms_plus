package com.viettel.backend.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.embed.Result;

public class DistributorVisitSummary implements Serializable {

    private static final long serialVersionUID = 97323440167596792L;
    
    private DistributorEmbed distributor;
    private Result result;
    private List<SalesmanVisitSummary> salesmanVisitSummaries;

    public DistributorVisitSummary(DistributorEmbed distributor, Result result,
            List<SalesmanVisitSummary> salesmanVisitSummaries) {
        super();

        this.distributor = distributor;
        this.result = result;
        this.salesmanVisitSummaries = salesmanVisitSummaries;
    }

    public DistributorEmbed getDistributor() {
        return distributor;
    }

    public Result getResult() {
        return result;
    }

    public Collection<SalesmanVisitSummary> getSalesmanVisitSummaries() {
        return salesmanVisitSummaries;
    }

    public static class SalesmanVisitSummary implements Serializable {
        
        private static final long serialVersionUID = 4940781771589118295L;
        
        private UserEmbed salesman;
        private Result result;

        public SalesmanVisitSummary(UserEmbed salesman, Result result) {
            super();

            this.salesman = salesman;
            this.result = result;
        }

        public UserEmbed getSalesman() {
            return salesman;
        }

        public Result getResult() {
            return result;
        }

    }

}
