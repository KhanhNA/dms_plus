package com.viettel.backend.domain;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.ExhibitionRatingItem;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.entity.SimpleDate;

/**
 * @author thanh
 */
@Document(collection = "ExhibitionRating")
public class ExhibitionRating extends PO {

    private static final long serialVersionUID = 7632969965465389576L;

    public static final String COLUMNNAME_CUSTOMER_ID = "customer.id";
    public static final String COLUMNNAME_EXHIBITION_ID = "exhibitionId";
    public static final String COLUMNNAME_STORECHECKER_ID = "storeChecker.id";
    public static final String COLUMNNAME_CREATED_TIME = "createdTime";

    private UserEmbed storeChecker;
    private CustomerEmbed customer;
    private ObjectId exhibitionId;
    private SimpleDate createdTime;
    
    private List<ExhibitionRatingItem> items;

    public ObjectId getExhibitionId() {
        return exhibitionId;
    }

    public void setExhibitionId(ObjectId exhibitionId) {
        this.exhibitionId = exhibitionId;
    }

    public SimpleDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        this.createdTime = createdTime;
    }

    public List<ExhibitionRatingItem> getItems() {
        return items;
    }

    public void setItems(List<ExhibitionRatingItem> items) {
        this.items = items;
    }

    public UserEmbed getStoreChecker() {
        return storeChecker;
    }

    public void setStoreChecker(UserEmbed storeChecker) {
        this.storeChecker = storeChecker;
    }

    public CustomerEmbed getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEmbed customer) {
        this.customer = customer;
    }

}
