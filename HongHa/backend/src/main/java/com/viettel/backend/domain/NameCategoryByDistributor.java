package com.viettel.backend.domain;

import com.viettel.backend.domain.embed.DistributorEmbed;

/**
 * @author trung
 */
public abstract class NameCategoryByDistributor extends NameCategory {

    private static final long serialVersionUID = 1L;

    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor.id";

    private DistributorEmbed distributor;

    public DistributorEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbed distributor) {
        this.distributor = distributor;
    }

}
