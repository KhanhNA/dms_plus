package com.viettel.backend.domain;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.OrderPromotion;
import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.engine.promotion.I_Order;
import com.viettel.backend.entity.SimpleDate;

@Document(collection = "VisitAndOrder")
public class Order extends PO implements I_NeedApprovePO, I_Order<ObjectId, ProductEmbed> {

    private static final long serialVersionUID = -1522058115974076896L;

    public static final int DELIVERY_TYPE_IMMEDIATE = 0;
    public static final int DELIVERY_TYPE_IN_DAY = 1;
    public static final int DELIVERY_TYPE_ANOTHER_DAY = 2;

    public static final String COLUMNNAME_CUSTOMER_ID = "customer.id";
    public static final String COLUMNNAME_CUSTOMER_CODE = "customer.code";
    public static final String COLUMNNAME_CUSTOMER_NAME = "customer.name";
    public static final String COLUMNNAME_CUSTOMER_ADDRESS = "customer.address";

    public static final String COLUMNNAME_SALESMAN_ID = "salesman.id";
    public static final String COLUMNNAME_SALESMAN_FULLNAME = "salesman.fullname";

    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor.id";
    public static final String COLUMNNAME_DISTRIBUTOR_CODE = "distributor.code";
    public static final String COLUMNNAME_DISTRIBUTOR_NAME = "distributor.name";

    public static final String COLUMNNAME_IS_VISIT = "isVisit";
    public static final String COLUMNNAME_IS_ORDER = "isOrder";

    public static final String COLUMNNAME_CODE = "code";

    public static final String COLUMNNAME_CREATED_TIME_VALUE = "endTime.value";
    public static final String COLUMNNAME_DELIVERY_TYPE = "deliveryType";
    public static final String COLUMNNAME_DELIVERY_DATE = "deliveryDate";
    
    public static final String COLUMNNAME_APPROVE_TIME_VALUE = "approveTime.value";
    public static final String COLUMNNAME_APPROVE_USER = "approveUser";

    public static final String COLUMNNAME_SUB_TOTAL = "subTotal";
    public static final String COLUMNNAME_PROMOTION_AMT = "promotionAmt";
    public static final String COLUMNNAME_DISCOUNT_AMT = "discountAmt";

    public static final String COLUMNNAME_GRAND_TOTAL = "grandTotal";

    public Order() {
        super();

        setVisit(false);
        setOrder(true);
    }

    private DistributorEmbed distributor;
    private CustomerEmbed customer;
    private UserEmbed salesman;

    private SimpleDate startTime;
    private SimpleDate endTime;

    private boolean isVisit;
    private boolean isOrder;
    private String code;
    private int approveStatus;
    
    private SimpleDate approveTime;
    private UserEmbed approveUser;

    private int deliveryType;
    private SimpleDate deliveryTime;

    // Thong tin khuyen mai khac
    private String comment;

    // Gia tri don hang truoc khuyen mai
    private BigDecimal subTotal;

    // Gia tri khuyen mai
    private BigDecimal promotionAmt;

    // Manual Discount Percentage (null if amount is fixed)
    private BigDecimal discountPercentage;
    // Manual Discount Amount
    // discountAmt = fixed or = (subTotal - promotionAmt) * discount
    private BigDecimal discountAmt;

    // Gia tri cuoi cung cua don hang
    // = subTotal - promotionAmt - discountAmt;
    private BigDecimal grandTotal;

    private List<OrderDetail> details;
    private List<OrderPromotion> promotionResults;
    
    public DistributorEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbed distributor) {
        this.distributor = distributor;
    }

    public CustomerEmbed getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEmbed customer) {
        this.customer = customer;
    }

    public UserEmbed getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbed salesman) {
        this.salesman = salesman;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }
    
    public SimpleDate getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(SimpleDate approveTime) {
        this.approveTime = approveTime;
    }

    public UserEmbed getApproveUser() {
        return approveUser;
    }

    public void setApproveUser(UserEmbed approveUser) {
        this.approveUser = approveUser;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }

    public SimpleDate getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(SimpleDate deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getPromotionAmt() {
        return promotionAmt;
    }

    public void setPromotionAmt(BigDecimal promotionAmt) {
        this.promotionAmt = promotionAmt;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    @SuppressWarnings("unchecked")
    public List<OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetail> details) {
        this.details = details;
    }

    public void addDetails(OrderDetail detail) {
        if (this.details == null) {
            this.details = new LinkedList<OrderDetail>();
        }
        this.details.add(detail);
    }

    @SuppressWarnings("unchecked")
    public List<OrderPromotion> getPromotionResults() {
        return promotionResults;
    }

    public void setPromotionResults(List<OrderPromotion> promotionResults) {
        this.promotionResults = promotionResults;
    }

    public void addPromotionResult(OrderPromotion promotion) {
        if (this.promotionResults == null) {
            this.promotionResults = new LinkedList<OrderPromotion>();
        }
        this.promotionResults.add(promotion);
    }

    public SimpleDate getCreatedTime() {
        return endTime;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        startTime = new SimpleDate(createdTime);
        endTime = new SimpleDate(createdTime);
    }

    // PROTECTED
    public boolean isVisit() {
        return isVisit;
    }

    protected void setVisit(boolean isVisit) {
        this.isVisit = isVisit;
    }

    public boolean isOrder() {
        return isOrder;
    }

    protected void setOrder(boolean isOrder) {
        this.isOrder = isOrder;
    }

    protected SimpleDate getStartTime() {
        return startTime;
    }

    protected void setStartTime(SimpleDate startTime) {
        this.startTime = startTime;
    }

    protected SimpleDate getEndTime() {
        return endTime;
    }

    protected void setEndTime(SimpleDate endTime) {
        this.endTime = endTime;
    }
    
}
