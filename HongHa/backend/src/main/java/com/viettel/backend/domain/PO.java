package com.viettel.backend.domain;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import com.viettel.backend.domain.annotation.ClientRootFixed;

/**
 * Persistent Object
 * 
 * @author ptht
 */
public abstract class PO implements Serializable {

    private static final long serialVersionUID = 3498632065144170387L;

    public static final String COLUMNNAME_CLIENT_ID = "clientId";
    public static final String COLUMNNAME_ACTIVE = "active";
    public static final String COLUMNNAME_ID = "id";

    public static final String COLUMNNAME_DRAFT = "draft";

    public static final ObjectId CLIENT_ROOT_ID = ObjectId.createFromLegacyFormat(0, 0, 0);
    public static final ObjectId ORG_ROOT_ID = ObjectId.createFromLegacyFormat(0, 0, 0);
    public static final ObjectId ROLE_ROOT_ID = ObjectId.createFromLegacyFormat(0, 0, 0);

    @Id
    private ObjectId id;

    private ObjectId clientId;

    private boolean active;

    private boolean draft;

    protected PO() {
        this(null, null);
    }

    public PO(ObjectId clientId, ObjectId id) {
        if (PO.isClientRootFixed(this.getClass())) {
            this.clientId = PO.CLIENT_ROOT_ID;
        } else {
            this.clientId = clientId;
        }

        this.id = id;
        this.active = true;
        this.draft = false;
    }

    public ObjectId getClientId() {
        return clientId;
    }

    public void setClientId(ObjectId clientId) {
        if (!PO.isClientRootFixed(this.getClass())) {
            this.clientId = clientId;
        }
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    /***************** OBJECT'S METHODS *************************/
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass().equals(this.getClass())) {
            PO po = (PO) obj;
            if (po.getId() != null && this.getId() != null) {
                return po.getId().equals(this.getId());
            }

            return super.equals(obj);
        }

        return false;
    }
    
    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }

    @Override
    public String toString() {
        return new StringBuilder(this.getClass().getSimpleName()).append("=").append(getId()).toString();
    }

    /******************* STATIC METHODS *****************************/
    public static <D extends PO> boolean isClientRootFixed(Class<D> clazz) {
        return clazz.isAnnotationPresent(ClientRootFixed.class);
    }
    
    public static void copy(PO source, PO destination) {
        if (source == null || destination == null) {
            throw new IllegalArgumentException("param is null");
        }
        
        destination.setId(source.getId());
        destination.setClientId(source.getClientId());
        destination.setActive(source.isActive());
        destination.setDraft(source.isDraft());
    }
}
