package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.UserEmbed;

@Document(collection = "Route")
public class Route extends NameCategoryByDistributor {

    private static final long serialVersionUID = 1L;

    public static final String COLUMNNAME_SALESMAN_ID = "salesman.id";

    private UserEmbed salesman;

    public UserEmbed getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbed salesman) {
        this.salesman = salesman;
    }

}
