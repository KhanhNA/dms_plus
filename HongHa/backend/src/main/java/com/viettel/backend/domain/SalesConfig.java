package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "SalesConfig")
public class SalesConfig extends PO {

    private static final long serialVersionUID = -4995404568124100927L;

    private long visitDurationKPI;
    private double visitDistanceKPI;
    private double meetReqRate;
    private boolean canEditCustomerLocation;

    public long getVisitDurationKPI() {
        return visitDurationKPI;
    }

    public void setVisitDurationKPI(long visitDurationKPI) {
        this.visitDurationKPI = visitDurationKPI;
    }

    public double getVisitDistanceKPI() {
        return visitDistanceKPI;
    }

    public void setVisitDistanceKPI(double visitDistanceKPI) {
        this.visitDistanceKPI = visitDistanceKPI;
    }

    public double getMeetReqRate() {
        return meetReqRate;
    }

    public void setMeetReqRate(double meetReqRate) {
        this.meetReqRate = meetReqRate;
    }
    
    public boolean isCanEditCustomerLocation() {
        return canEditCustomerLocation;
    }
    
    public void setCanEditCustomerLocation(boolean canEditCustomerLocation) {
        this.canEditCustomerLocation = canEditCustomerLocation;
    }

}
