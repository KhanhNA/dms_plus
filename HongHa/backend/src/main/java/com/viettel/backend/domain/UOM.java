package com.viettel.backend.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "UOM")
public class UOM extends NameCategory {

    private static final long serialVersionUID = 863511075009914692L;

    public static final String COLUMNNAME_CODE = "code";

    private String code;

    public UOM() {
        super();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String[] getSearchValues() {
        return new String[] { getCode(), getName() };
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        UOM other = (UOM) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        return true;
    }
}
