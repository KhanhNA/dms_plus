package com.viettel.backend.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;

@Document(collection = "User")
public class User extends POSearchable {

    private static final long serialVersionUID = -2980948476333281024L;

    public static final String COLUMNNAME_USERNAME = "username";
    public static final String COLUMNNAME_FULLNAME = "fullname";
    public static final String COLUMNNAME_ROLES = "roles";
    public static final String COLUMNNAME_DISTRIBUTOR = "distributor";
    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor.id";
    public static final String COLUMNNAME_DISTRIBUTOR_NAME = "distributor.name";
    public static final String COLUMNNAME_DISTRIBUTOR_CODE = "distributor.code";
    public static final String COLUMNNAME_STORE_CHECKER = "storeChecker";
    public static final String COLUMNNAME_STORE_CHECKER_ID = "storeChecker.id";

    private String username;
    private String fullname;
    private String password;

    private List<String> roles;

    // FILED FOR SALE SYSTEM
    private DistributorEmbed distributor;

    private UserEmbed storeChecker;

    public User() {
        super();

        this.username = null;
        this.fullname = null;
        this.password = null;
        this.roles = null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public DistributorEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbed distributor) {
        this.distributor = distributor;
    }

    public UserEmbed getStoreChecker() {
        return storeChecker;
    }

    public void setStoreChecker(UserEmbed storeChecker) {
        this.storeChecker = storeChecker;
    }

    @Override
    public String[] getSearchValues() {
        return new String[] { getUsername(), getFullname() };
    }

}
