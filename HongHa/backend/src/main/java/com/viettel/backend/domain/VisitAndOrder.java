package com.viettel.backend.domain;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.OrderPromotion;
import com.viettel.backend.domain.embed.SurveyAnswer;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.entity.SimpleDate;

/**
 * User only for init cache
 */
@Document(collection = "VisitAndOrder")
public class VisitAndOrder extends PO {

    private static final long serialVersionUID = -4796838272235690717L;
    
    private DistributorEmbed distributor;
    private CustomerEmbed customer;
    private UserEmbed salesman;

    private SimpleDate startTime;
    private SimpleDate endTime;

    private boolean isVisit;
    private boolean isOrder;
    private String code;
    private int approveStatus;

    private SimpleDate approveTime;
    private UserEmbed approveUser;

    private int deliveryType;
    private SimpleDate deliveryTime;

    private String comment;

    private BigDecimal subTotal;

    private BigDecimal promotionAmt;

    private BigDecimal discountPercentage;
    private BigDecimal discountAmt;

    private BigDecimal grandTotal;

    private int visitStatus;
    private boolean closed;
    private String closingPhoto;

    private double[] location;
    private double[] customerLocation;
    private int locationStatus;
    private Double distance;

    private long duration;
    private boolean errorDuration;

    // FEEDBACK
    private boolean feedbacksReaded;
    private List<String> feedbacks;

    // SURVEY ANSWER
    private List<SurveyAnswer> surveyAnswers;

    private List<OrderDetail> details;
    private List<OrderPromotion> promotionResults;

    public VisitAndOrder() {
        super();
    }
    
    public DistributorEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbed distributor) {
        this.distributor = distributor;
    }

    public CustomerEmbed getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEmbed customer) {
        this.customer = customer;
    }

    public UserEmbed getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbed salesman) {
        this.salesman = salesman;
    }

    public SimpleDate getStartTime() {
        return startTime;
    }

    public void setStartTime(SimpleDate startTime) {
        this.startTime = startTime;
    }

    public SimpleDate getEndTime() {
        return endTime;
    }

    public void setEndTime(SimpleDate endTime) {
        this.endTime = endTime;
    }

    public boolean isVisit() {
        return isVisit;
    }

    public void setVisit(boolean isVisit) {
        this.isVisit = isVisit;
    }

    public boolean isOrder() {
        return isOrder;
    }

    public void setOrder(boolean isOrder) {
        this.isOrder = isOrder;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }

    public SimpleDate getApproveTime() {
        return approveTime;
    }

    public void setApproveTime(SimpleDate approveTime) {
        this.approveTime = approveTime;
    }

    public UserEmbed getApproveUser() {
        return approveUser;
    }

    public void setApproveUser(UserEmbed approveUser) {
        this.approveUser = approveUser;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }

    public SimpleDate getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(SimpleDate deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getPromotionAmt() {
        return promotionAmt;
    }

    public void setPromotionAmt(BigDecimal promotionAmt) {
        this.promotionAmt = promotionAmt;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(int visitStatus) {
        this.visitStatus = visitStatus;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getClosingPhoto() {
        return closingPhoto;
    }

    public void setClosingPhoto(String closingPhoto) {
        this.closingPhoto = closingPhoto;
    }

    public double[] getLocation() {
        return location;
    }

    public void setLocation(double[] location) {
        this.location = location;
    }

    public double[] getCustomerLocation() {
        return customerLocation;
    }

    public void setCustomerLocation(double[] customerLocation) {
        this.customerLocation = customerLocation;
    }

    public int getLocationStatus() {
        return locationStatus;
    }

    public void setLocationStatus(int locationStatus) {
        this.locationStatus = locationStatus;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isErrorDuration() {
        return errorDuration;
    }

    public void setErrorDuration(boolean errorDuration) {
        this.errorDuration = errorDuration;
    }

    public boolean isFeedbacksReaded() {
        return feedbacksReaded;
    }

    public void setFeedbacksReaded(boolean feedbacksReaded) {
        this.feedbacksReaded = feedbacksReaded;
    }

    public List<String> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<String> feedbacks) {
        this.feedbacks = feedbacks;
    }

    public List<SurveyAnswer> getSurveyAnswers() {
        return surveyAnswers;
    }

    public void setSurveyAnswers(List<SurveyAnswer> surveyAnswers) {
        this.surveyAnswers = surveyAnswers;
    }

    public List<OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetail> details) {
        this.details = details;
    }

    public List<OrderPromotion> getPromotionResults() {
        return promotionResults;
    }

    public void setPromotionResults(List<OrderPromotion> promotionResults) {
        this.promotionResults = promotionResults;
    }

}
