package com.viettel.backend.domain.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface ClientRootInclude {

}
