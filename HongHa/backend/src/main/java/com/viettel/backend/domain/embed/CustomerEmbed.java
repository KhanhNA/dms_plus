package com.viettel.backend.domain.embed;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.District;
import com.viettel.backend.domain.NameCategory;

public class CustomerEmbed extends NameCategory {

    private static final long serialVersionUID = -3158993502257758137L;

    private String code;
    private String address;
    private District district;

    public CustomerEmbed() {
        super();
    }

    public CustomerEmbed(Customer customer) {
        super();

        setId(customer.getId());
        setName(customer.getName());

        this.code = customer.getCode();
        this.address = customer.getAddress();
        this.district = customer.getDistrict();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

}
