package com.viettel.backend.domain.embed;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;

public class CustomerSchedule implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String COLUMNNAME_ROUTE_ID = "routeId";
    public static final String COLUMNNAME_ITEM = "item";
    public static final String COLUMNNAME_ITEMS = "items";

    private ObjectId routeId;
    private CustomerScheduleItem item;
    private List<CustomerScheduleItem> items;

    public ObjectId getRouteId() {
        return routeId;
    }
    
    public void setRouteId(ObjectId routeId) {
        this.routeId = routeId;
    }

    public CustomerScheduleItem getItem() {
        return item;
    }
    
    public void setItem(CustomerScheduleItem item) {
        this.item = item;
    }
    
    public List<CustomerScheduleItem> getItems() {
        return items;
    }

    public void setItems(List<CustomerScheduleItem> items) {
        this.items = items;
    }

}
