package com.viettel.backend.domain.embed;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.NameCategory;

public class DistributorEmbed extends NameCategory {

    private static final long serialVersionUID = -3137655980640920852L;

    private String code;

    public DistributorEmbed() {
        this(null, null, null);
    }

    public DistributorEmbed(Distributor distributor) {
        this(distributor.getId(), distributor.getCode(), distributor.getName());
    }

    public DistributorEmbed(ObjectId id, String code, String name) {
        super();

        setId(id);
        setName(name);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
