package com.viettel.backend.domain.embed;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.NameCategory;
import com.viettel.backend.engine.promotion.I_PromotionDetailResult;
import com.viettel.backend.engine.promotion.PromotionRewardResult;

public class OrderPromotionDetail extends NameCategory implements I_PromotionDetailResult<ObjectId, ProductEmbed> {

    private static final long serialVersionUID = 6620807032420284594L;

    private PromotionRewardResult<ObjectId, ProductEmbed> rewardResult;

    public PromotionRewardResult<ObjectId, ProductEmbed> getRewardResult() {
        return rewardResult;
    }

    public void setRewardResult(PromotionRewardResult<ObjectId, ProductEmbed> rewardResult) {
        this.rewardResult = rewardResult;
    }

}
