package com.viettel.backend.dto;

public class CheckCustomerDto {

    private String visitId;
    private String salesmanId;
    private boolean passed;
    private String note;

    public CheckCustomerDto() {
    }

    public CheckCustomerDto(String visitId, String salesmanId, boolean passed, String note) {
        super();
        this.visitId = visitId;
        this.salesmanId = salesmanId;
        this.passed = passed;
        this.note = note;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
