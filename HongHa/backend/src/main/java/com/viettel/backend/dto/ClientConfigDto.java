package com.viettel.backend.dto;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.entity.SimpleFile;

public class ClientConfigDto extends DTO {

    private static final long serialVersionUID = 4618363876249230856L;

    // DEFAULT VALUE
    private String defaultProductPhoto;
    private String defaultCustomerPhoto;
    private String defaultDateFormat;

    // CALENDAR INDEX
    private int firstDayOfWeek;
    private int minimalDaysInFirstWeek;
    private int numberWeekOfFrequency;
    private boolean complexSchedule;
    
    private int numberDayOrderPendingExpire;
    
    private SimpleFile importCustomerTemplate;
    private SimpleFile importProductTemplate;

    public ClientConfigDto() {
        super();
    }

    public ClientConfigDto(ClientConfig clientConfig) {
        super(clientConfig);
        
        this.setDefaultProductPhoto(clientConfig.getDefaultProductPhoto());
        this.setDefaultCustomerPhoto(clientConfig.getDefaultCustomerPhoto());
        this.setDefaultDateFormat(clientConfig.getDefaultDateFormat());
        
        this.setFirstDayOfWeek(clientConfig.getFirstDayOfWeek());
        this.setMinimalDaysInFirstWeek(clientConfig.getMinimalDaysInFirstWeek());
        this.setNumberWeekOfFrequency(clientConfig.getNumberWeekOfFrequency());
        
        this.setNumberDayOrderPendingExpire(clientConfig.getNumberDayOrderPendingExpire());
        
        this.setImportCustomerTemplate(clientConfig.getImportCustomerTemplate());
        this.setImportProductTemplate(clientConfig.getImportProductTemplate());
        this.setComplexSchedule(clientConfig.isComplexSchedule());
    }

    public String getDefaultProductPhoto() {
        return defaultProductPhoto;
    }

    public void setDefaultProductPhoto(String defaultProductPhoto) {
        this.defaultProductPhoto = defaultProductPhoto;
    }

    public String getDefaultCustomerPhoto() {
        return defaultCustomerPhoto;
    }

    public void setDefaultCustomerPhoto(String defaultCustomerPhoto) {
        this.defaultCustomerPhoto = defaultCustomerPhoto;
    }

    public String getDefaultDateFormat() {
        return defaultDateFormat;
    }

    public void setDefaultDateFormat(String defaultDateFormat) {
        this.defaultDateFormat = defaultDateFormat;
    }

    public int getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(int firstDayOfWeek) {
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public int getMinimalDaysInFirstWeek() {
        return minimalDaysInFirstWeek;
    }

    public void setMinimalDaysInFirstWeek(int minimalDaysInFirstWeek) {
        this.minimalDaysInFirstWeek = minimalDaysInFirstWeek;
    }

    public int getNumberWeekOfFrequency() {
        return numberWeekOfFrequency;
    }

    public void setNumberWeekOfFrequency(int numberWeekOfFrequency) {
        this.numberWeekOfFrequency = numberWeekOfFrequency;
    }
    
    public int getNumberDayOrderPendingExpire() {
        return numberDayOrderPendingExpire;
    }
    
    public void setNumberDayOrderPendingExpire(int numberDayOrderPendingExpire) {
        this.numberDayOrderPendingExpire = numberDayOrderPendingExpire;
    }
    
    public SimpleFile getImportCustomerTemplate() {
        return importCustomerTemplate;
    }
    
    public void setImportCustomerTemplate(SimpleFile importCustomerTemplate) {
        this.importCustomerTemplate = importCustomerTemplate;
    }
    
    public SimpleFile getImportProductTemplate() {
        return importProductTemplate;
    }
    
    public void setImportProductTemplate(SimpleFile importProductTemplate) {
        this.importProductTemplate = importProductTemplate;
    }

    public boolean isComplexSchedule() {
        return complexSchedule;
    }

    public void setComplexSchedule(boolean complexSchedule) {
        this.complexSchedule = complexSchedule;
    }

}
