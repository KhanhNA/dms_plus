package com.viettel.backend.dto;

import java.util.List;

import com.viettel.backend.domain.Customer;

public class CustomerDto extends CustomerSimpleDto {

    private static final long serialVersionUID = 6617041574138019886L;

    private String idNumber;
    private String email;
    private String description;
    private List<String> photos;
    
    public CustomerDto() {
        
    }

    public CustomerDto(Customer customer) {
        super(customer);

        this.idNumber = customer.getIdNumber();
        this.email = customer.getEmail();
        this.description = customer.getDescription();
        this.photos = customer.getPhotos();
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

}
