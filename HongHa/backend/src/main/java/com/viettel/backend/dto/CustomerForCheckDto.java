package com.viettel.backend.dto;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Visit;

public class CustomerForCheckDto extends CustomerDto {

    private static final long serialVersionUID = -6146358781236339238L;

    private boolean checked;
    private boolean closed;
    private boolean hasOrder;
    private String visitId;
    private boolean passed;
    private String note;

    public CustomerForCheckDto(Customer customer, Visit visit, boolean checked, boolean passed, String note) {
        super(customer);
        this.setSalesman(new UserSimpleDto(visit.getSalesman()));
        this.checked = checked;
        this.closed = visit.isClosed();
        this.hasOrder = visit.isOrder();
        this.visitId = visit.getId().toString();
        this.passed = passed;
        this.note = note;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public boolean isHasOrder() {
        return hasOrder;
    }

    public void setHasOrder(boolean hasOrder) {
        this.hasOrder = hasOrder;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
