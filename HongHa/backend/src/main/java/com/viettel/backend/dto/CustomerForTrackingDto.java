package com.viettel.backend.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Visit;

public class CustomerForTrackingDto extends CustomerForVisitDto {

    private static final long serialVersionUID = -8481404278128653260L;

    public static class VisitInfo {
        private String id;
        private boolean hasOrder;
        private boolean closed;
        private String closingPhoto;
        private String startTime;
        private String endTime;
        private long duration;
        private boolean errorDuration;
        private int locationStatus;
        private Double distance;
        private BigDecimal grandTotal;
        private int approveStatus;
        
        public VisitInfo(Visit visit) {
            this.id = visit.getId().toString();
            this.hasOrder = visit.isOrder();
            this.closed = visit.isClosed();
            this.closingPhoto = visit.getClosingPhoto();
            this.locationStatus = visit.getLocationStatus();
            this.distance = visit.getDistance();
            this.startTime = visit.getStartTime() != null ? visit.getStartTime().getIsoTime() : null;
            this.endTime = visit.getEndTime() != null ? visit.getEndTime().getIsoTime() : null;
            this.duration = visit.getDuration();
            this.errorDuration = visit.isErrorDuration();
            this.grandTotal = visit.getGrandTotal();
            this.approveStatus = visit.getApproveStatus();
        }

        public boolean isHasOrder() {
            return hasOrder;
        }

        public void setHasOrder(boolean hasOrder) {
            this.hasOrder = hasOrder;
        }

        public boolean isClosed() {
            return closed;
        }

        public void setClosed(boolean closed) {
            this.closed = closed;
        }

        public String getClosingPhoto() {
            return closingPhoto;
        }

        public void setClosingPhoto(String closingPhoto) {
            this.closingPhoto = closingPhoto;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public long getDuration() {
            return duration;
        }

        public void setDuration(long duration) {
            this.duration = duration;
        }

        public boolean isErrorDuration() {
            return errorDuration;
        }

        public void setErrorDuration(boolean errorDuration) {
            this.errorDuration = errorDuration;
        }

        public int getLocationStatus() {
            return locationStatus;
        }

        public void setLocationStatus(int locationStatus) {
            this.locationStatus = locationStatus;
        }

        public Double getDistance() {
            return distance;
        }

        public void setDistance(Double distance) {
            this.distance = distance;
        }

        public BigDecimal getGrandTotal() {
            return grandTotal;
        }

        public void setGrandTotal(BigDecimal grandTotal) {
            this.grandTotal = grandTotal;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getApproveStatus() {
            return approveStatus;
        }

        public void setApproveStatus(int approveStatus) {
            this.approveStatus = approveStatus;
        }

    }

    @JsonInclude(Include.NON_NULL)
    private VisitInfo visitInfo;

    public CustomerForTrackingDto(Customer customer, boolean planned, 
            int visitStatus, int seqNo, Visit visit) {
        super(customer, planned, visitStatus, seqNo);
        if (visit != null && visit.getVisitStatus() == Visit.VISIT_STATUS_VISITED) {
            visitInfo = new VisitInfo(visit);
        }
    }

    public VisitInfo getVisitInfo() {
        return visitInfo;
    }

    public void setVisitInfo(VisitInfo visitInfo) {
        this.visitInfo = visitInfo;
    }

}
