package com.viettel.backend.dto;

import com.viettel.backend.domain.Customer;

public class CustomerForVisitDto extends CustomerDto {

    private static final long serialVersionUID = -6146358781236339238L;

    public static final int STATUS_VISITING = 0;
    public static final int STATUS_UNVISITED = 1;
    public static final int STATUS_VISITED = 2;

    public static final String FIELDNAME_PLANNED = "planned";
    public static final String FIELDNAME_STATUS = "status";
    public static final String FIELDNAME_SEQ_NO = "seqNo";
    
    public CustomerForVisitDto(Customer customer, boolean planned, int visitStatus, int seqNo) {
        super(customer);
        
        this.planned = planned;
        this.visitStatus = visitStatus;
        this.seqNo = seqNo;
    }

    private boolean planned;
    private int visitStatus;
    private int seqNo;

    public boolean isPlanned() {
        return planned;
    }

    public void setPlanned(boolean planned) {
        this.planned = planned;
    }

    public int getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(int visitStatus) {
        this.visitStatus = visitStatus;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

}
