package com.viettel.backend.dto;

import java.math.BigDecimal;

import com.viettel.backend.domain.Customer;

public class CustomerSaleResultDto extends CustomerSimpleDto {

    private static final long serialVersionUID = -646684675802012874L;

    private BigDecimal productivity;
    private BigDecimal revenue;

    public CustomerSaleResultDto(Customer customer, BigDecimal productivity, BigDecimal revenue) {
        super(customer);

        this.productivity = productivity;
        this.revenue = revenue;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

}
