package com.viettel.backend.dto;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.dto.embed.DistributorEmbedDto;

public class CustomerSimpleDto extends NameCategoryDto {

    private static final long serialVersionUID = -8198585862402781027L;

    private String code;
    private String mobile;
    private String phone;
    private String contact;
    private String address;

    private double longitude;
    private double latitude;

    private int status;
    private UserSimpleDto createdBy;
    private String createdTime;

    private NameCategoryDto customerType;
    private NameCategoryDto district;

    private DistributorEmbedDto distributor;

    private UserSimpleDto salesman;

    public CustomerSimpleDto() {

    }

    public CustomerSimpleDto(Customer customer) {
        super(customer);

        this.code = customer.getCode();
        this.mobile = customer.getMobile();
        this.phone = customer.getPhone();
        this.contact = customer.getContact();
        this.address = customer.getAddress();

        if (customer.getLocation() != null && customer.getLocation().length == 2) {
            this.longitude = customer.getLocation()[0];
            this.latitude = customer.getLocation()[1];
        }

        this.status = customer.getApproveStatus();
        if (customer.getDistributor() != null) {
            this.distributor = new DistributorEmbedDto(customer.getDistributor());
        }
        if (customer.getCreatedBy() != null) {
            this.createdBy = new UserSimpleDto(customer.getCreatedBy());
        }
        if (customer.getCustomerType() != null) {
            this.customerType = new NameCategoryDto(customer.getCustomerType());
        }
        if (customer.getDistrict() != null) {
            this.district = new NameCategoryDto(customer.getDistrict());
        }
        if (customer.getCreatedTime() != null) {
            this.createdTime = customer.getCreatedTime().getIsoTime();
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    public NameCategoryDto getDistrict() {
        return district;
    }
    
    public void setDistrict(NameCategoryDto district) {
        this.district = district;
    }

    public NameCategoryDto getCustomerType() {
        return customerType;
    }

    public void setCustomerType(NameCategoryDto customerType) {
        this.customerType = customerType;
    }

    public UserSimpleDto getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserSimpleDto createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public DistributorEmbedDto getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbedDto distributor) {
        this.distributor = distributor;
    }

    public UserSimpleDto getSalesman() {
        return salesman;
    }

    public void setSalesman(UserSimpleDto salesman) {
        this.salesman = salesman;
    }

}
