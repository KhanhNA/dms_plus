package com.viettel.backend.dto;

import java.io.Serializable;
import java.util.List;

public class DistributorCreateDto implements Serializable {

    private static final long serialVersionUID = -4570514747523311583L;

    private String name;
    private String code;
    private String address;
    private String supervisorId;
    
    private List<String> salesmanIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }
    
    public List<String> getSalesmanIds() {
        return salesmanIds;
    }
    
    public void setSalesmanIds(List<String> salesmanIds) {
        this.salesmanIds = salesmanIds;
    }

}
