package com.viettel.backend.dto;

import java.io.Serializable;

public class DistributorNotificationDto implements Serializable {

    private static final long serialVersionUID = -6508740234889263658L;

    private long nbOrderToApprove;

    public DistributorNotificationDto(long nbOrderToApprove) {
        super();
        
        this.nbOrderToApprove = nbOrderToApprove;
    }

    public long getNbOrderToApprove() {
        return nbOrderToApprove;
    }

    public void setNbOrderToApprove(long nbOrderToApprove) {
        this.nbOrderToApprove = nbOrderToApprove;
    }

}
