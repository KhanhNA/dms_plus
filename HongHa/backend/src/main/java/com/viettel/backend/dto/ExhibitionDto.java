package com.viettel.backend.dto;

import java.util.LinkedList;
import java.util.List;

import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.domain.embed.ExhibitionCategory;
import com.viettel.backend.domain.embed.ExhibitionItem;

/**
 * @author thanh
 */
public class ExhibitionDto extends ExhibitionSimpleDto {

    private static final long serialVersionUID = 4294936505380930621L;

    private List<ExhibitionCategoryDto> categories;

    public ExhibitionDto() {
        super();
    }

    public ExhibitionDto(Exhibition exhibition) {
        super(exhibition);

        if (exhibition.getCategories() != null) {
            for (ExhibitionCategory exhibitionCategory : exhibition.getCategories()) {
                addCategory(new ExhibitionCategoryDto(exhibitionCategory));
            }
        }
    }

    public List<ExhibitionCategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(List<ExhibitionCategoryDto> categories) {
        this.categories = categories;
    }

    public void addCategory(ExhibitionCategoryDto category) {
        if (this.categories == null) {
            this.categories = new LinkedList<ExhibitionCategoryDto>();
        }

        this.categories.add(category);
    }

    /**
     * @author thanh
     */
    public static class ExhibitionCategoryDto extends NameCategoryDto {

        private static final long serialVersionUID = -2523574162736376978L;

        private List<ExhibitionItemDto> items;

        public ExhibitionCategoryDto() {
            super();
        }

        public ExhibitionCategoryDto(ExhibitionCategory exhibitionCategory) {
            super(exhibitionCategory);

            if (exhibitionCategory.getItems() != null) {
                for (ExhibitionItem item : exhibitionCategory.getItems()) {
                    addItem(new ExhibitionItemDto(item));
                }
            }
        }

        public List<ExhibitionItemDto> getItems() {
            return items;
        }

        public void setItems(List<ExhibitionItemDto> items) {
            this.items = items;
        }

        public void addItem(ExhibitionItemDto item) {
            if (this.items == null) {
                this.items = new LinkedList<ExhibitionItemDto>();
            }

            this.items.add(item);
        }

    }

    /**
     * @author thanh
     */
    public static class ExhibitionItemDto extends NameCategoryDto {

        private static final long serialVersionUID = -8692725141573698924L;

        public ExhibitionItemDto() {
            super();
        }

        public ExhibitionItemDto(ExhibitionItem exhibitionItem) {
            super(exhibitionItem);
        }

    }
}
