package com.viettel.backend.dto;

import com.viettel.backend.domain.Exhibition;

/**
 * @author thanh
 */
public class ExhibitionReportDto extends ExhibitionSimpleDto {

    private static final long serialVersionUID = 7584629448715597869L;

    private float participationRates;
    private float meetRequirementRates;

    public ExhibitionReportDto(Exhibition exhibition, float participationRates, float meetRequirementRates) {
        super(exhibition);
        this.participationRates = participationRates;
        this.meetRequirementRates = meetRequirementRates;
    }

    public float getParticipationRates() {
        return participationRates;
    }

    public void setParticipationRates(float participationRates) {
        this.participationRates = participationRates;
    }

    public float getMeetRequirementRates() {
        return meetRequirementRates;
    }

    public void setMeetRequirementRates(float meetRequirementRates) {
        this.meetRequirementRates = meetRequirementRates;
    }

}
