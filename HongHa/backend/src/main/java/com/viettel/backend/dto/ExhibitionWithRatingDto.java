package com.viettel.backend.dto;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.domain.ExhibitionRating;
import com.viettel.backend.domain.embed.ExhibitionCategory;
import com.viettel.backend.domain.embed.ExhibitionItem;
import com.viettel.backend.domain.embed.ExhibitionRatingItem;

/**
 * @author thanh
 */
public class ExhibitionWithRatingDto extends ExhibitionSimpleDto {

    private static final long serialVersionUID = 4294936505380930621L;

    private List<ExhibitionCategoryWithRatingDto> categories;
    
    private boolean rated;
    
    public ExhibitionWithRatingDto() {
        
    }
    
    public ExhibitionWithRatingDto(Exhibition exhibition, ExhibitionRating exhibitionRating) {
        super(exhibition);

        if (exhibition.getCategories() != null) {
            Map<ObjectId, ExhibitionRatingItem> exhibitionRatingItems = null;
            if (exhibitionRating != null && exhibitionRating.getItems() != null) {
                this.rated = true;
                exhibitionRatingItems = new HashMap<>(exhibitionRating.getItems().size());
                for (ExhibitionRatingItem exhibitionRatingItem : exhibitionRating.getItems()) {
                    exhibitionRatingItems.put(exhibitionRatingItem.getExhibitionItemId(), exhibitionRatingItem);
                }
            }
            
            for (ExhibitionCategory exhibitionCategory : exhibition.getCategories()) {
                addCategory(new ExhibitionCategoryWithRatingDto(exhibitionCategory, exhibitionRatingItems));
            }
        }
    }
    
    public List<ExhibitionCategoryWithRatingDto> getCategories() {
        return categories;
    }

    public void setCategories(List<ExhibitionCategoryWithRatingDto> categories) {
        this.categories = categories;
    }

    public void addCategory(ExhibitionCategoryWithRatingDto category) {
        if (this.categories == null) {
            this.categories = new LinkedList<ExhibitionCategoryWithRatingDto>();
        }

        this.categories.add(category);
    }

    public boolean isRated() {
        return rated;
    }

    public void setRated(boolean rated) {
        this.rated = rated;
    }

    /**
     * @author thanh
     */
    public static class ExhibitionCategoryWithRatingDto extends NameCategoryDto {

        private static final long serialVersionUID = -6767684452051571876L;
        
        private List<ExhibitionItemWithRatingDto> items;
        
        public ExhibitionCategoryWithRatingDto() {
            super();
        }

        public ExhibitionCategoryWithRatingDto(ExhibitionCategory exhibitionCategory, 
                Map<ObjectId, ExhibitionRatingItem> exhibitionRatingItems) {
            super(exhibitionCategory);

            if (exhibitionCategory.getItems() != null) {
                for (ExhibitionItem item : exhibitionCategory.getItems()) {
                    addItem(new ExhibitionItemWithRatingDto(item, 
                            exhibitionRatingItems != null ? exhibitionRatingItems.get(item.getId()) : null));
                }
            }
        }

        public List<ExhibitionItemWithRatingDto> getItems() {
            return items;
        }

        public void setItems(List<ExhibitionItemWithRatingDto> items) {
            this.items = items;
        }
        
        public void addItem(ExhibitionItemWithRatingDto item) {
            if (this.items == null) {
                this.items = new LinkedList<ExhibitionItemWithRatingDto>();
            }

            this.items.add(item);
        }
    }

    /**
     * @author thanh
     */
    public static class ExhibitionItemWithRatingDto extends NameCategoryDto {
        
        private static final long serialVersionUID = -9126015608207155328L;

        @JsonInclude(Include.NON_NULL)
        private Float rate;

        public ExhibitionItemWithRatingDto() {
            super();
        }

        public ExhibitionItemWithRatingDto(ExhibitionItem exhibitionItem, ExhibitionRatingItem exhibitionRatingItem) {
            super(exhibitionItem);
            if (exhibitionRatingItem != null) {
                this.rate = exhibitionRatingItem.getRate();
            }
        }

        public Float getRate() {
            return rate;
        }

        public void setRate(Float rate) {
            this.rate = rate;
        }

    }
    
}
