package com.viettel.backend.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class HomepageDto implements Serializable {

    private static final long serialVersionUID = -2425918035587920200L;

    private String today;

    // MONTHLY
    private BigDecimal orderByDayThisMonth;
    private BigDecimal orderByDayLastMonth;
    private BigDecimal revenueByOrderThisMonth;
    private BigDecimal revenueByOrderLastMonth;
    private BigDecimal nbVisitByDayThisMonth;
    private BigDecimal nbVisitByDayLastMonth;
    private BigDecimal revenueThisMonth;
    private BigDecimal revenueTargetThisMonth;

    // DAILY
    private BigDecimal revenueToday;
    private int nbVisit;
    private int nbVisitPlanned;
    private int nbVisitHasOrder;
    private int nbOrderNoVisit;
    private int nbVisitErrorTime;
    private int nbVisitErrorPosition;

    private List<ProductSalesSummaryDto> top5products;
    private List<SalesmanWithScoreDto> worst5salesmen;

    public HomepageDto() {
        this.orderByDayThisMonth = BigDecimal.ZERO;
        this.orderByDayLastMonth = BigDecimal.ZERO;
        this.revenueByOrderThisMonth = BigDecimal.ZERO;
        this.revenueByOrderLastMonth = BigDecimal.ZERO;
        this.nbVisitByDayThisMonth = BigDecimal.ZERO;
        this.nbVisitByDayLastMonth = BigDecimal.ZERO;
        this.revenueThisMonth = BigDecimal.ZERO;
        this.revenueTargetThisMonth = BigDecimal.ZERO;

        this.revenueToday = BigDecimal.ZERO;
        this.nbVisit = 0;
        this.nbVisitPlanned = 0;
        this.nbVisitHasOrder = 0;
        this.nbOrderNoVisit = 0;
        this.nbVisitErrorTime = 0;
        this.nbVisitErrorPosition = 0;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public BigDecimal getOrderByDayThisMonth() {
        return orderByDayThisMonth;
    }

    public void setOrderByDayThisMonth(BigDecimal orderByDayThisMonth) {
        this.orderByDayThisMonth = orderByDayThisMonth;
    }

    public BigDecimal getOrderByDayLastMonth() {
        return orderByDayLastMonth;
    }

    public void setOrderByDayLastMonth(BigDecimal orderByDayLastMonth) {
        this.orderByDayLastMonth = orderByDayLastMonth;
    }

    public BigDecimal getRevenueByOrderThisMonth() {
        return revenueByOrderThisMonth;
    }

    public void setRevenueByOrderThisMonth(BigDecimal revenueByOrderThisMonth) {
        this.revenueByOrderThisMonth = revenueByOrderThisMonth;
    }

    public BigDecimal getRevenueByOrderLastMonth() {
        return revenueByOrderLastMonth;
    }

    public void setRevenueByOrderLastMonth(BigDecimal revenueByOrderLastMonth) {
        this.revenueByOrderLastMonth = revenueByOrderLastMonth;
    }

    public BigDecimal getNbVisitByDayThisMonth() {
        return nbVisitByDayThisMonth;
    }

    public void setNbVisitByDayThisMonth(BigDecimal nbVisitByDayThisMonth) {
        this.nbVisitByDayThisMonth = nbVisitByDayThisMonth;
    }

    public BigDecimal getNbVisitByDayLastMonth() {
        return nbVisitByDayLastMonth;
    }

    public void setNbVisitByDayLastMonth(BigDecimal nbVisitByDayLastMonth) {
        this.nbVisitByDayLastMonth = nbVisitByDayLastMonth;
    }

    public BigDecimal getRevenueThisMonth() {
        return revenueThisMonth;
    }

    public void setRevenueThisMonth(BigDecimal revenueThisMonth) {
        this.revenueThisMonth = revenueThisMonth;
    }

    public BigDecimal getRevenueTargetThisMonth() {
        return revenueTargetThisMonth;
    }

    public void setRevenueTargetThisMonth(BigDecimal revenueTargetThisMonth) {
        this.revenueTargetThisMonth = revenueTargetThisMonth;
    }

    public BigDecimal getRevenueToday() {
        return revenueToday;
    }

    public void setRevenueToday(BigDecimal revenueToday) {
        this.revenueToday = revenueToday;
    }

    public int getNbVisit() {
        return nbVisit;
    }

    public void setNbVisit(int nbVisit) {
        this.nbVisit = nbVisit;
    }

    public int getNbVisitPlanned() {
        return nbVisitPlanned;
    }

    public void setNbVisitPlanned(int nbVisitPlanned) {
        this.nbVisitPlanned = nbVisitPlanned;
    }

    public int getNbVisitHasOrder() {
        return nbVisitHasOrder;
    }

    public void setNbVisitHasOrder(int nbVisitHasOrder) {
        this.nbVisitHasOrder = nbVisitHasOrder;
    }

    public int getNbOrderNoVisit() {
        return nbOrderNoVisit;
    }

    public void setNbOrderNoVisit(int nbOrderNoVisit) {
        this.nbOrderNoVisit = nbOrderNoVisit;
    }

    public int getNbVisitErrorTime() {
        return nbVisitErrorTime;
    }

    public void setNbVisitErrorTime(int nbVisitErrorTime) {
        this.nbVisitErrorTime = nbVisitErrorTime;
    }

    public List<ProductSalesSummaryDto> getTop5products() {
        return top5products;
    }

    public int getNbVisitErrorPosition() {
        return nbVisitErrorPosition;
    }
    
    public void setNbVisitErrorPosition(int nbVisitErrorPosition) {
        this.nbVisitErrorPosition = nbVisitErrorPosition;
    }

    public void setTop5products(List<ProductSalesSummaryDto> top5products) {
        this.top5products = top5products;
    }

    public List<SalesmanWithScoreDto> getWorst5salesmen() {
        return worst5salesmen;
    }

    public void setWorst5salesmen(List<SalesmanWithScoreDto> worst5salesmen) {
        this.worst5salesmen = worst5salesmen;
    }

}
