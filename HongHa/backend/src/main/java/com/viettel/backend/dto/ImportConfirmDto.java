package com.viettel.backend.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ImportConfirmDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private boolean errorTemplate;
    private int total;
    private List<RowData> rowDatas;

    public ImportConfirmDto(boolean errorTemplate, int total, List<RowData> rowDatas) {
        super();
        this.errorTemplate = errorTemplate;
        this.total = total;
        this.rowDatas = rowDatas;
    }

    public boolean isErrorTemplate() {
        return errorTemplate;
    }

    public void setErrorTemplate(boolean errorTemplate) {
        this.errorTemplate = errorTemplate;
    }

    public int getTotal() {
        return total;
    }
    
    public void setTotal(int total) {
        this.total = total;
    }

    public List<RowData> getRowDatas() {
        return rowDatas;
    }
    
    public void setRowDatas(List<RowData> rowDatas) {
        this.rowDatas = rowDatas;
    }

    public static class RowData {
        
        private int rowNumber;
        private List<String> dataTexts;
        private List<Boolean> errors;
        
        @JsonIgnore
        private List<Object> datas;
        
        public RowData(int rowNumber, List<String> dataTexts, List<Boolean> errors, List<Object> datas) {
            super();
            this.rowNumber = rowNumber;
            this.dataTexts = dataTexts;
            this.errors = errors;
            
            this.datas = datas;
        }

        public int getRowNumber() {
            return rowNumber;
        }

        public void setRowNumber(int rowNumber) {
            this.rowNumber = rowNumber;
        }

        public List<String> getDataTexts() {
            return dataTexts;
        }

        public void setDataTexts(List<String> dataTexts) {
            this.dataTexts = dataTexts;
        }

        public List<Boolean> getErrors() {
            return errors;
        }

        public void setErrors(List<Boolean> errors) {
            this.errors = errors;
        }
        
        public List<Object> getDatas() {
            return datas;
        }
        
        public void setDatas(List<Object> datas) {
            this.datas = datas;
        }

    }

}
