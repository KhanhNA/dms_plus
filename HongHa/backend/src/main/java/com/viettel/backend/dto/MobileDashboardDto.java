package com.viettel.backend.dto;

import java.io.Serializable;

import com.viettel.backend.dto.embed.Result;

public class MobileDashboardDto implements Serializable {

    private static final long serialVersionUID = -6931337321980872133L;

    private Result revenue;
    private Result nbVisit;
    private Result nbSaleDays;

    public Result getRevenue() {
        return revenue;
    }

    public void setRevenue(Result revenue) {
        this.revenue = revenue;
    }

    public Result getNbVisit() {
        return nbVisit;
    }
    
    public void setNbVisit(Result nbVisit) {
        this.nbVisit = nbVisit;
    }

    public Result getNbSaleDays() {
        return nbSaleDays;
    }

    public void setNbSaleDays(Result nbSaleDays) {
        this.nbSaleDays = nbSaleDays;
    }

}
