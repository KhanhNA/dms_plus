package com.viettel.backend.dto;

public class NameCategoryByDistributorCreateDto extends NameCategoryDto {

    private static final long serialVersionUID = 1041537230068995353L;

    private String distributorId;

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

}
