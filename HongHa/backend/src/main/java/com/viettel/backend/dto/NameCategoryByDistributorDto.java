package com.viettel.backend.dto;

import com.viettel.backend.domain.NameCategoryByDistributor;
import com.viettel.backend.dto.embed.DistributorEmbedDto;

public class NameCategoryByDistributorDto extends NameCategoryDto {

    private static final long serialVersionUID = 1041537230068995353L;

    private DistributorEmbedDto distributor;

    public NameCategoryByDistributorDto(NameCategoryByDistributor category) {
        super(category);
        
        this.distributor = new DistributorEmbedDto(category.getDistributor());
    }
    
    public DistributorEmbedDto getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbedDto distributor) {
        this.distributor = distributor;
    }

}
