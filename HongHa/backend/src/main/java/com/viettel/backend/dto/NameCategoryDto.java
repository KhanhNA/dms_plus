package com.viettel.backend.dto;

import javax.validation.constraints.NotNull;

import com.viettel.backend.domain.NameCategory;

/**
 * @author thanh
 */
public class NameCategoryDto extends DTO {

    private static final long serialVersionUID = 1041537230068995353L;

    @NotNull
    private String name;

    public NameCategoryDto() {
        super();
    }

    public NameCategoryDto(NameCategory nameCategory) {
        super(nameCategory);
        
        if (nameCategory != null) {
            setId(nameCategory.getId() == null ? null : nameCategory.getId().toString());
            setDraft(nameCategory.isDraft());
            this.name = nameCategory.getName();
        }
    }

    public NameCategoryDto(String id, String name) {
        this();

        setId(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
