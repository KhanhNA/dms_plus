package com.viettel.backend.dto;

import java.math.BigDecimal;
import java.util.List;

import com.viettel.backend.dto.embed.OrderDetailCreatedDto;

public class OrderCreateDto extends DTO {

    private static final long serialVersionUID = 4372092863401688032L;

    private String distributorId;
    private String customerId;
    private String salesmanId;

    private String code;

    private int deliveryType;
    private String deliveryTime;

    private String comment;

    private BigDecimal discountPercentage;
    private BigDecimal discountAmt;

    private List<OrderDetailCreatedDto> details;

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public List<OrderDetailCreatedDto> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetailCreatedDto> details) {
        this.details = details;
    }

}
