package com.viettel.backend.dto;

import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.dto.embed.ProductEmbedDto;

public class ProductSalesSummaryDto extends ProductEmbedDto implements Comparable<ProductSalesSummaryDto> {

    private static final long serialVersionUID = -3397585930168578667L;
    
    private int quantity;

    public ProductSalesSummaryDto(ProductEmbed product, int quantity) {
        super(product, null);
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int compareTo(ProductSalesSummaryDto o) {
        if (o == null) {
            return -1;
        }
        return o.getQuantity() - this.getQuantity();
    }

}
