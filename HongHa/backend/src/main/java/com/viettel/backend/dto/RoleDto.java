package com.viettel.backend.dto;

import java.io.Serializable;

public class RoleDto implements Serializable {

    private static final long serialVersionUID = 5316468090244338990L;

    private String code;
    private String name;

    public RoleDto(String code, String name) {
        super();
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
