package com.viettel.backend.dto;

public class RouteCreateDto extends NameCategoryDto {

    private static final long serialVersionUID = 1L;

    private String distributorId;
    private String salesmanId;

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

}
