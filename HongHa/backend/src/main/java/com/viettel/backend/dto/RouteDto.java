package com.viettel.backend.dto;

import com.viettel.backend.domain.Route;
import com.viettel.backend.dto.embed.DistributorEmbedDto;
import com.viettel.backend.dto.embed.UserEmbedDto;

public class RouteDto extends NameCategoryDto {

    private static final long serialVersionUID = 1L;
    
    private DistributorEmbedDto distributor;
    private UserEmbedDto salesman;

    public RouteDto(Route route) {
        super(route);
        
        this.distributor = new DistributorEmbedDto(route.getDistributor());
        this.salesman = route.getSalesman() == null ? null : new UserEmbedDto(route.getSalesman());
    }
    
    public DistributorEmbedDto getDistributor() {
        return distributor;
    }

    public void setDistributor(DistributorEmbedDto distributor) {
        this.distributor = distributor;
    }

    public UserEmbedDto getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbedDto salesman) {
        this.salesman = salesman;
    }

}
