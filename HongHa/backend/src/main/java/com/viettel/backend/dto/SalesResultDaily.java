package com.viettel.backend.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SalesResultDaily implements Serializable {

    private static final long serialVersionUID = 5244255433430240893L;

    private String date;
    private BigDecimal productivity;
    private BigDecimal revenue;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

}
