package com.viettel.backend.dto;

import java.math.BigDecimal;

import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.UserEmbed;

public class SalesmanSalesReportDto extends UserSimpleDto {

    private static final long serialVersionUID = 3991523494712638803L;

    private BigDecimal revenue;
    private BigDecimal productivity;
    private long nbOrder;
    private long nbCustomer;
    
    public SalesmanSalesReportDto(User user, BigDecimal revenue, BigDecimal productivity, long nbOrder,
            long nbCustomer) {
        super(user);
        
        this.revenue = revenue;
        this.productivity = productivity;
        this.nbOrder = nbOrder;
        this.nbCustomer = nbCustomer;
    }

    public SalesmanSalesReportDto(UserEmbed user, BigDecimal revenue, BigDecimal productivity, long nbOrder,
            long nbCustomer) {
        super(user);
        
        this.revenue = revenue;
        this.productivity = productivity;
        this.nbOrder = nbOrder;
        this.nbCustomer = nbCustomer;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public long getNbOrder() {
        return nbOrder;
    }

    public void setNbOrder(long nbOrder) {
        this.nbOrder = nbOrder;
    }

    public long getNbCustomer() {
        return nbCustomer;
    }

    public void setNbCustomer(long nbCustomer) {
        this.nbCustomer = nbCustomer;
    }

}
