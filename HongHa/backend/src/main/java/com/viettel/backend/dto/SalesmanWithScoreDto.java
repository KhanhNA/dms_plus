package com.viettel.backend.dto;

import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.embed.Result;
import com.viettel.backend.dto.embed.UserEmbedDto;

public class SalesmanWithScoreDto extends UserEmbedDto implements Comparable<SalesmanWithScoreDto> {

    private static final long serialVersionUID = -3397585930168578667L;

    private Result result;

    public SalesmanWithScoreDto(UserEmbed user, Result result) {
        super(user);
        this.result = result;
    }

    @Override
    public int compareTo(SalesmanWithScoreDto o) {
        if (o == null) {
            return -1;
        }
        return (this.result.getPercentage() - o.result.getPercentage());
    }

    public int getActual() {
        return this.result.getActual().intValue();
    }

    public int getPlan() {
        return this.result.getPlan().intValue();
    }

    public int getPercentage() {
        return this.result.getPercentage();
    }

}
