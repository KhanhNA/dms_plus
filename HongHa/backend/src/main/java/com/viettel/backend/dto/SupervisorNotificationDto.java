package com.viettel.backend.dto;

import java.io.Serializable;

public class SupervisorNotificationDto implements Serializable {

    private static final long serialVersionUID = -6508740234889263658L;

    private long nbFeedbackToRead;
    private long nbCustomerToApprove;

    public SupervisorNotificationDto(long nbFeedbackToRead, long nbCustomerToApprove) {
        super();
        this.nbFeedbackToRead = nbFeedbackToRead;
        this.nbCustomerToApprove = nbCustomerToApprove;
    }

    public long getNbFeedbackToRead() {
        return nbFeedbackToRead;
    }

    public void setNbFeedbackToRead(long nbFeedbackToRead) {
        this.nbFeedbackToRead = nbFeedbackToRead;
    }

    public long getNbCustomerToApprove() {
        return nbCustomerToApprove;
    }

    public void setNbCustomerToApprove(long nbCustomerToApprove) {
        this.nbCustomerToApprove = nbCustomerToApprove;
    }

}
