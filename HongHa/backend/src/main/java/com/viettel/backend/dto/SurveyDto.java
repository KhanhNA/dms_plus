package com.viettel.backend.dto;

import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.domain.Survey;
import com.viettel.backend.domain.embed.SurveyQuestion;
import com.viettel.backend.dto.embed.SurveyQuestionDto;

public class SurveyDto extends SurveySimpleDto {

    private static final long serialVersionUID = 4294936505380930621L;

    private List<SurveyQuestionDto> questions;

    public SurveyDto() {
        super();
    }

    public SurveyDto(Survey survey) {
        super(survey);

        if (survey.getQuestions() != null) {
            for (SurveyQuestion surveyQuestion : survey.getQuestions()) {
                addQuestion(new SurveyQuestionDto(surveyQuestion));
            }
        }
    }

    public List<SurveyQuestionDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<SurveyQuestionDto> questions) {
        this.questions = questions;
    }

    public void addQuestion(SurveyQuestionDto question) {
        if (this.questions == null) {
            this.questions = new ArrayList<SurveyQuestionDto>();
        }

        this.questions.add(question);
    }

}
