package com.viettel.backend.dto;

import java.math.BigDecimal;

public class TargetCreateDto extends DTO {

    private static final long serialVersionUID = -3377268163150327565L;

    private String salesmanId;
    private int month;
    private int year;
    private BigDecimal revenue;
    
    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(String salesmanId) {
        this.salesmanId = salesmanId;
    }

}
