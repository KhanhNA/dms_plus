package com.viettel.backend.dto;

import com.viettel.backend.domain.UOM;

public class UOMDto extends NameCategoryDto {

    private static final long serialVersionUID = 5316468090244338990L;

    private String code;

    public UOMDto() {
        super();
    }

    public UOMDto(UOM uom) {
        super(uom);

        this.code = uom.getCode();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
