package com.viettel.backend.dto;

import java.util.List;

import com.viettel.backend.domain.User;

/**
 * @author thanh
 */
public class UserDto extends UserSimpleDto {

    private static final long serialVersionUID = 1L;

    private String role;
    private String distributorId;

    private List<String> salesmanIds;

    public UserDto() {
        super();
    }

    public UserDto(User user) {
        super(user);

        if (user.getRoles() != null && !user.getRoles().isEmpty()) {
            this.role = user.getRoles().get(0);
        }

        if (user.getDistributor() != null) {
            this.distributorId = user.getDistributor().getId().toString();
        }
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public List<String> getSalesmanIds() {
        return salesmanIds;
    }

    public void setSalesmanIds(List<String> salesmanIds) {
        this.salesmanIds = salesmanIds;
    }

}
