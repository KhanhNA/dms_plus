package com.viettel.backend.dto;

import java.util.List;

import com.viettel.backend.domain.User;


public class UserLoginDto extends DTO {

    /**
     * 
     */
    private static final long serialVersionUID = -8039769069456882676L;

    private String clientName;
    
    private String username;
    private String fullname;
    private List<String> roleCodes;
    
    public UserLoginDto(User user) {
        super(user);
        
        this.username = user.getUsername();
        this.fullname = user.getFullname();
        this.roleCodes = user.getRoles();
    }

    public String getClientName() {
        return clientName;
    }
    
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    
    public String getFullname() {
        return fullname;
    }

    public List<String> getRoleCodes() {
        return roleCodes;
    }

    public void setRoleCodes(List<String> roleCodes) {
        this.roleCodes = roleCodes;
    }
    
}
