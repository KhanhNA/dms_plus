package com.viettel.backend.dto;

import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.UserEmbed;

public class UserSimpleDto extends DTO {

    private static final long serialVersionUID = -3703101604369169590L;

    private String username;
    private String fullname;

    public UserSimpleDto() {
        super();
    }

    public UserSimpleDto(User user) {
        super(user);

        this.username = user.getUsername();
        this.fullname = user.getFullname();
    }

    public UserSimpleDto(UserEmbed user) {
        super();

        this.username = user.getUsername();
        this.fullname = user.getFullname();
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
