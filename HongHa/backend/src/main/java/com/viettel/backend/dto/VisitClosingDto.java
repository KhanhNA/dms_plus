package com.viettel.backend.dto;

import java.io.Serializable;

public class VisitClosingDto implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -522893756957841955L;
    
    private String closingPhoto;
    private LocationDto location;

    public String getClosingPhoto() {
        return closingPhoto;
    }

    public void setClosingPhoto(String closingPhoto) {
        this.closingPhoto = closingPhoto;
    }
    
    public LocationDto getLocation() {
        return location;
    }
    
    public void setLocation(LocationDto location) {
        this.location = location;
    }

}
