package com.viettel.backend.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.dto.embed.Result;
import com.viettel.backend.oauth2.core.UserLogin;

public class VisitReportDailyDto implements Serializable {

    private static final long serialVersionUID = -8775721328789400806L;

    private Result numberVisits;
    private int numberVisitErrorDuration;
    private int numberVisitErrorPosition;

    private List<VisitInfoDto> visits;

    public VisitReportDailyDto(UserLogin userLogin, Collection<Visit> _visits, String defaultProductPhoto,
            int numberCustomerForVisitToday) {
        if (_visits == null) {
            _visits = Collections.<Visit> emptyList();
        }

        numberVisitErrorDuration = 0;
        numberVisitErrorPosition = 0;

        Collection<ObjectId> visitIds = new ArrayList<ObjectId>(_visits.size());
        visits = new ArrayList<VisitInfoDto>(_visits.size());
        for (Visit visit : _visits) {
            visitIds.add(visit.getId());

            VisitInfoDto visitInfoDto = new VisitInfoDto(visit, defaultProductPhoto);
            if (visitInfoDto.isErrorDuration()) {
                numberVisitErrorDuration++;
            }
            if (visit.getLocationStatus() != Visit.LOCATION_STATUS_LOCATED) {
                numberVisitErrorPosition++;
            }

            visits.add(visitInfoDto);
        }

        this.numberVisits = new Result(numberCustomerForVisitToday, visits.size());
    }

    public Result getNumberVisits() {
        return numberVisits;
    }

    public void setNumberVisits(Result numberVisits) {
        this.numberVisits = numberVisits;
    }

    public int getNumberVisitErrorDuration() {
        return numberVisitErrorDuration;
    }

    public void setNumberVisitErrorDuration(int numberVisitErrorDuration) {
        this.numberVisitErrorDuration = numberVisitErrorDuration;
    }

    public int getNumberVisitErrorPosition() {
        return numberVisitErrorPosition;
    }

    public void setNumberVisitErrorPosition(int numberVisitErrorPosition) {
        this.numberVisitErrorPosition = numberVisitErrorPosition;
    }

    public List<VisitInfoDto> getVisits() {
        return visits;
    }

    public void setVisits(List<VisitInfoDto> visits) {
        this.visits = visits;
    }

}
