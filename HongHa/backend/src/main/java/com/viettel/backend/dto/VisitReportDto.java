package com.viettel.backend.dto;

import java.io.Serializable;

public class VisitReportDto implements Serializable {

    private static final long serialVersionUID = 8420780748738793113L;
    
    private String customer;
    private String customerId;
    private int numberVisitPlan;
    private int numberVisited;
    private int numberVisitClosed;
    private int numberVisitErrorTime;
    private int numberVisitNoErrorTime;
    private int numberVisitInRange;
    private int numberVisitOutRange;
    private int numberVisitUnlocated;

    public VisitReportDto() {
        this.numberVisitPlan = 0;
        this.numberVisited = 0;
        this.numberVisitClosed = 0;
        this.numberVisitErrorTime = 0;
        this.numberVisitNoErrorTime = 0;
        this.numberVisitInRange = 0;
        this.numberVisitOutRange = 0;
        this.numberVisitUnlocated = 0;
    }
    
    public String getCustomer() {
        return customer;
    }
    
    public void setCustomer(String customer) {
        this.customer = customer;
    }
    
    public String getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    
    public int getNumberVisitPlan() {
        return numberVisitPlan;
    }

    public void setNumberVisitPlan(int numberVisitPlan) {
        this.numberVisitPlan = numberVisitPlan;
    }

    public int getNumberVisited() {
        return numberVisited;
    }

    public void setNumberVisited(int numberVisited) {
        this.numberVisited = numberVisited;
    }

    public int getNumberVisitClosed() {
        return numberVisitClosed;
    }

    public void setNumberVisitClosed(int numberVisitClosed) {
        this.numberVisitClosed = numberVisitClosed;
    }

    public int getNumberVisitErrorTime() {
        return numberVisitErrorTime;
    }

    public void setNumberVisitErrorTime(int numberVisitErrorTime) {
        this.numberVisitErrorTime = numberVisitErrorTime;
    }

    public int getNumberVisitNoErrorTime() {
        return numberVisitNoErrorTime;
    }

    public void setNumberVisitNoErrorTime(int numberVisitNoErrorTime) {
        this.numberVisitNoErrorTime = numberVisitNoErrorTime;
    }

    public int getNumberVisitInRange() {
        return numberVisitInRange;
    }

    public void setNumberVisitInRange(int numberVisitInRange) {
        this.numberVisitInRange = numberVisitInRange;
    }

    public int getNumberVisitOutRange() {
        return numberVisitOutRange;
    }

    public void setNumberVisitOutRange(int numberVisitOutRange) {
        this.numberVisitOutRange = numberVisitOutRange;
    }

    public int getNumberVisitUnlocated() {
        return numberVisitUnlocated;
    }

    public void setNumberVisitUnlocated(int numberVisitUnlocated) {
        this.numberVisitUnlocated = numberVisitUnlocated;
    }

}
