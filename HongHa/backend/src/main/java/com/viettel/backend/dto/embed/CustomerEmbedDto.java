package com.viettel.backend.dto.embed;

import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.dto.NameCategoryDto;

public class CustomerEmbedDto extends NameCategoryDto {

    private static final long serialVersionUID = 1652073192406165389L;
    
    private String code;
    private String address;
    private NameCategoryDto district;
    
    public CustomerEmbedDto() {
        super();
    }
    
    public CustomerEmbedDto(CustomerEmbed customer) {
        super(customer);
        
        this.code = customer.getCode();
        this.address = customer.getAddress();
        
        if (customer.getDistrict() != null) {
            this.district = new NameCategoryDto(customer.getDistrict());
        }
    }
    
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public NameCategoryDto getDistrict() {
        return district;
    }
	
	public void setDistrict(NameCategoryDto district) {
        this.district = district;
    }
	
}
