package com.viettel.backend.dto.embed;

import java.io.Serializable;
import java.math.BigDecimal;

public class OrderDetailCreatedDto implements Serializable {

    private static final long serialVersionUID = 6236136783748119701L;

    private String productId;
    private BigDecimal quantity;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }
    
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

}
