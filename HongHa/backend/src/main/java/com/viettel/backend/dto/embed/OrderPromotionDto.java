package com.viettel.backend.dto.embed;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.viettel.backend.domain.embed.OrderPromotion;
import com.viettel.backend.domain.embed.OrderPromotionDetail;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.engine.promotion.I_PromotionResult;

public class OrderPromotionDto extends NameCategoryDto implements I_PromotionResult<String, ProductEmbedDto> {

    private static final long serialVersionUID = 4560950228432621757L;

    private List<OrderPromotionDetailDto> details;

    public OrderPromotionDto() {
        super();
    }
    
    public OrderPromotionDto(OrderPromotion orderPromotion,
            String defaultProductPhoto) {
        super(orderPromotion);

        if (orderPromotion.getDetails() != null) {
            this.details = new ArrayList<OrderPromotionDetailDto>(orderPromotion.getDetails().size());
            for (OrderPromotionDetail detail : orderPromotion.getDetails()) {
                this.details.add(new OrderPromotionDetailDto(detail, defaultProductPhoto));
            }
        }
    }

    @SuppressWarnings("unchecked")
    public List<OrderPromotionDetailDto> getDetails() {
        return details;
    }

    public void setDetails(List<OrderPromotionDetailDto> details) {
        this.details = details;
    }
    
    public void addDetail(OrderPromotionDetailDto detail) { 
        if (this.details == null) {
            this.details = new LinkedList<OrderPromotionDetailDto>();
        }
        
        this.details.add(detail);
    }

}
