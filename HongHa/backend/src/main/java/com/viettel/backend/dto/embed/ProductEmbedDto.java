package com.viettel.backend.dto.embed;

import java.math.BigDecimal;

import org.springframework.util.StringUtils;

import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.dto.UOMDto;
import com.viettel.backend.engine.promotion.I_Product;

public class ProductEmbedDto extends NameCategoryDto implements I_Product<String> {

    private static final long serialVersionUID = 7240727794465791875L;

    private String code;
    private String photo;
    private UOMDto uom;
    private BigDecimal price;
    private BigDecimal output;

    public ProductEmbedDto(ProductEmbed product, String defaultProductPhoto) {
        super(product);

        this.code = product.getCode();
        this.price = product.getPrice();
        this.output = product.getOutput();

        if (product.getUom() != null) {
            this.uom = new UOMDto(product.getUom());
        }

        if (product.getPhoto() != null) {
            this.photo = product.getPhoto();
        } else {
            this.photo = defaultProductPhoto;
        }
    }
    
    public ProductEmbedDto(String productName) {
        super();
        
        setId(StringUtils.trimAllWhitespace(productName));
        setName(productName);
        
        UOMDto uom = new UOMDto();
        uom.setId("nouom");
        uom.setDraft(false);
        uom.setCode("");
        uom.setName("");
        setUom(uom);
        
        this.code = "";
        this.price = BigDecimal.ZERO;
        this.output = BigDecimal.ZERO;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public UOMDto getUom() {
        return uom;
    }

    public void setUom(UOMDto uom) {
        this.uom = uom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOutput() {
        return output;
    }

    public void setOutput(BigDecimal output) {
        this.output = output;
    }

}
