package com.viettel.backend.dto.embed;

import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.domain.embed.SurveyOption;
import com.viettel.backend.domain.embed.SurveyQuestion;
import com.viettel.backend.dto.NameCategoryDto;

public class SurveyQuestionDto extends NameCategoryDto {

    /**
     * 
     */
    private static final long serialVersionUID = 5496584668724710642L;

    private boolean multipleChoice;
    private boolean required;

    private List<SurveyOptionDto> options;

    public SurveyQuestionDto() {
        super();
    }

    public SurveyQuestionDto(SurveyQuestion surveyQuestion) {
        super(surveyQuestion);

        this.multipleChoice = surveyQuestion.isMultipleChoice();
        this.required = surveyQuestion.isRequired();

        if (surveyQuestion.getOptions() != null) {
            for (SurveyOption surveyOption : surveyQuestion.getOptions()) {
                addOption(new SurveyOptionDto(surveyOption));
            }
        }
    }

    public boolean isMultipleChoice() {
        return multipleChoice;
    }

    public void setMultipleChoice(boolean multipleChoice) {
        this.multipleChoice = multipleChoice;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<SurveyOptionDto> getOptions() {
        return options;
    }

    public void setOptions(List<SurveyOptionDto> options) {
        this.options = options;
    }

    public void addOption(SurveyOptionDto option) {
        if (this.options == null) {
            this.options = new ArrayList<SurveyOptionDto>();
        }

        this.options.add(option);
    }
}
