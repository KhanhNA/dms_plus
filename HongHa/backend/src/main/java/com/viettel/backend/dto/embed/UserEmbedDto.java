package com.viettel.backend.dto.embed;

import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.DTO;

public class UserEmbedDto extends DTO {
	/**
	 * 
	 */
    private static final long serialVersionUID = 5689092223088750722L;
    
	private String fullname;
	
	public UserEmbedDto() {
	    super();
	}
	
	public UserEmbedDto(UserEmbed user) {
        super(user);
        
        this.fullname = user.getFullname();
    }
	
	public String getFullname() {
		return fullname;
	}
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	
}
