package com.viettel.backend.dto.report;

import java.math.BigDecimal;

import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.dto.embed.DistributorEmbedDto;

public class DistributorSalesReportDto extends DistributorEmbedDto {

    private static final long serialVersionUID = -4613095523957567352L;

    private BigDecimal revenue;
    private BigDecimal productivity;
    private long nbOrder;
    private long nbCustomer;
    private long nbSalesman;

    public DistributorSalesReportDto(DistributorEmbed distributor, BigDecimal revenue, BigDecimal productivity,
            long nbOrder, long nbCustomer, long nbSalesman) {
        super(distributor);

        this.revenue = revenue;
        this.productivity = productivity;
        this.nbOrder = nbOrder;
        this.nbCustomer = nbCustomer;
        this.nbSalesman = nbSalesman;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public long getNbOrder() {
        return nbOrder;
    }

    public void setNbOrder(long nbOrder) {
        this.nbOrder = nbOrder;
    }

    public long getNbCustomer() {
        return nbCustomer;
    }

    public void setNbCustomer(long nbCustomer) {
        this.nbCustomer = nbCustomer;
    }

    public long getNbSalesman() {
        return nbSalesman;
    }

    public void setNbSalesman(long nbSalesman) {
        this.nbSalesman = nbSalesman;
    }

}
