package com.viettel.backend.dto.report;

import java.math.BigDecimal;

import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.dto.embed.ProductEmbedDto;

public class ProductSalesReportDto extends ProductEmbedDto {

    private static final long serialVersionUID = -4613095523957567352L;

    private BigDecimal revenue;
    private BigDecimal productivity;
    private long nbOrder;

    public ProductSalesReportDto(ProductEmbed product, String defaultProductPhoto, BigDecimal revenue,
            BigDecimal productivity, long nbOrder) {
        super(product, defaultProductPhoto);

        this.revenue = revenue;
        this.productivity = productivity;
        this.nbOrder = nbOrder;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public long getNbOrder() {
        return nbOrder;
    }

    public void setNbOrder(long nbOrder) {
        this.nbOrder = nbOrder;
    }

}
