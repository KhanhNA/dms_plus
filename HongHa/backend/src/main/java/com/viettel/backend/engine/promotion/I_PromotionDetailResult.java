package com.viettel.backend.engine.promotion;

import java.io.Serializable;

public interface I_PromotionDetailResult<ID extends Serializable, P extends I_Product<ID>> extends Serializable {

    public ID getId();
    
    public PromotionRewardResult<ID, P> getRewardResult();
    
}
