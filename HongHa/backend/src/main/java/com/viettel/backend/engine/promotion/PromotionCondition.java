package com.viettel.backend.engine.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromotionCondition<ID extends Serializable, P extends I_Product<ID>> implements Serializable {

    private static final long serialVersionUID = 3092840229319211359L;

    private P product;
    private BigDecimal quantity;

    public PromotionCondition() {
        super();
    }

    public PromotionCondition(P product, BigDecimal quantity) {
        super();
        this.product = product;
        this.quantity = quantity;
    }

    public P getProduct() {
        return product;
    }

    public void setProduct(P product) {
        this.product = product;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

}
