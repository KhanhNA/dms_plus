package com.viettel.backend.engine.promotion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.MultiKeyMap;
import org.springframework.util.Assert;

import com.viettel.backend.engine.promotion.I_PromotionDetail.PromotionDetailType;

public class PromotionEngine<ID extends Serializable, P extends I_Product<ID>> {

    public <PROMOTION extends I_Promotion<ID, P>> List<PromotionResult<ID, P>> calculate(List<PROMOTION> promotions,
            I_Order<ID, P> order) {

        Assert.notNull(order);
        Assert.notNull(order.getDetails());

        if (promotions == null || promotions.isEmpty()) {
            return null;
        }

        // TAO MAP ORDER DETAIL THEO PRODUCT ID
        Map<ID, I_OrderDetail<ID, P>> mapOrderDetails = new HashMap<ID, I_OrderDetail<ID, P>>(order.getDetails().size());
        for (I_OrderDetail<ID, P> orderDetail : order.getDetails()) {
            mapOrderDetails.put(orderDetail.getProduct().getId(), orderDetail);
        }

        // CHAY QUA TUNG PROMOTION
        List<PromotionResult<ID, P>> promotionResults = new LinkedList<PromotionResult<ID, P>>();
        for (I_Promotion<ID, P> promotion : promotions) {

            List<I_PromotionDetail<ID, P>> promotionDetails = promotion.getDetails();
            if (promotionDetails == null || promotionDetails.isEmpty()) {
                continue;
            }

            List<PromotionDetailResult<ID, P>> detailResults = new LinkedList<PromotionDetailResult<ID, P>>();

            // CHAY QUA TUNG PROMOTION DETAIL
            for (I_PromotionDetail<ID, P> promotionDetail : promotionDetails) {
                ID productID = promotionDetail.getCondition().getProduct().getId();
                if (mapOrderDetails.containsKey(productID)) {
                    I_OrderDetail<ID, P> orderDetail = mapOrderDetails.get(productID);
                    PromotionDetailResult<ID, P> promotionDetailResult = calculateDetail(orderDetail, promotionDetail);
                    if (promotionDetailResult != null) {
                        detailResults.add(promotionDetailResult);

                        // XXX verify 1 san pham hien tai chay duoc nhieu khuyen
                        // mai
                        // mapOrderDetails.remove(productID);

                        if (mapOrderDetails.isEmpty()) {
                            // TAT CA CAC SAN PHAM DEU DEU DA CO PROMOTION
                            break;
                        }
                    }
                }

            }

            if (!detailResults.isEmpty()) {
                PromotionResult<ID, P> promotionResult = new PromotionResult<ID, P>(promotion.getId(), detailResults);
                promotionResults.add(promotionResult);
            }

            if (mapOrderDetails.isEmpty()) {
                // TAT CA CAC SAN PHAM DEU DEU DA CO PROMOTION
                break;
            }
        }

        if (!promotionResults.isEmpty()) {
            return promotionResults;
        }

        return null;
    }

    private PromotionDetailResult<ID, P> calculateDetail(I_OrderDetail<ID, P> orderDetail,
            I_PromotionDetail<ID, P> promotionDetail) {

        if (!orderDetail.getProduct().getId().equals(promotionDetail.getCondition().getProduct().getId())) {
            return null;
        }

        PromotionRewardResult<ID, P> rewardResult = null;

        // Mua lon hon mot so luong san pham duoc giam tien
        if (promotionDetail.getType() == PromotionDetailType.C_PRODUCT_QTY_R_PERCENTAGE_AMT) {
            if (orderDetail.getQuantity().compareTo(promotionDetail.getCondition().getQuantity()) < 0) {
                return null;
            }
            rewardResult = new PromotionRewardResult<ID, P>();

            BigDecimal promotionAmount = orderDetail.getQuantity().multiply(orderDetail.getProduct().getPrice())
                    .multiply(promotionDetail.getReward().getPercentage())
                    .divide(new BigDecimal(100), 0, BigDecimal.ROUND_HALF_UP);

            rewardResult.setAmount(promotionAmount);
        }
        // Mua X san pham tang Y san pham
        else if (promotionDetail.getType() == PromotionDetailType.C_PRODUCT_QTY_R_PRODUCT_QTY) {
            if (orderDetail.getQuantity().compareTo(promotionDetail.getCondition().getQuantity()) < 0) {
                return null;
            }

            rewardResult = new PromotionRewardResult<ID, P>();
            rewardResult.setProduct(promotionDetail.getReward().getProduct());
            rewardResult.setProductText(promotionDetail.getReward().getProductText());
            rewardResult.setQuantity(orderDetail.getQuantity()
                    .divide(promotionDetail.getCondition().getQuantity(), 0, BigDecimal.ROUND_DOWN)
                    .multiply(promotionDetail.getReward().getQuantity()));
        }

        if (rewardResult != null) {
            PromotionDetailResult<ID, P> result = new PromotionDetailResult<ID, P>();
            result.setId(promotionDetail.getId());
            result.setRewardResult(rewardResult);
            return result;
        }

        return null;
    }

    public <PROMOTION extends I_Promotion<ID, P>> boolean checkOrder(List<PROMOTION> promotions, I_Order<ID, P> order) {
        List<PromotionResult<ID, P>> promotionResults = calculate(promotions, order);
        if (promotionResults == null) {
            promotionResults = Collections.emptyList();
        }

        List<I_PromotionResult<ID, P>> needCheckPromotionResults = order.getPromotionResults();
        if (needCheckPromotionResults == null) {
            needCheckPromotionResults = Collections.emptyList();
        }

        if (promotionResults.size() != needCheckPromotionResults.size()) {
            return false;
        }

        MultiKeyMap<ID, PromotionRewardResult<ID, P>> mapRewardResults = new MultiKeyMap<ID, PromotionRewardResult<ID, P>>();
        for (I_PromotionResult<ID, P> promotionResult : needCheckPromotionResults) {
            if (promotionResult == null || promotionResult.getId() == null || promotionResult.getDetails() == null) {
                return false;
            }

            for (I_PromotionDetailResult<ID, P> detailResult : promotionResult.getDetails()) {
                if (detailResult == null || detailResult.getId() == null || detailResult.getRewardResult() == null) {
                    return false;
                }

                mapRewardResults.put(promotionResult.getId(), detailResult.getId(), detailResult.getRewardResult());
            }
        }

        for (PromotionResult<ID, P> promotionResult : promotionResults) {
            for (PromotionDetailResult<ID, P> detailResult : promotionResult.getDetails()) {
                PromotionRewardResult<ID, P> needCheckPromotionRewardResult = mapRewardResults.get(
                        promotionResult.getId(), detailResult.getId());

                if (needCheckPromotionRewardResult == null) {
                    return false;
                }

                if (!needCheckPromotionRewardResult.getProduct().getId()
                        .equals(detailResult.getRewardResult().getProduct().getId())) {
                    return false;
                }

                if (!needCheckPromotionRewardResult.equals(detailResult.getRewardResult())) {
                    return false;
                }
            }
        }

        return true;
    }
}
