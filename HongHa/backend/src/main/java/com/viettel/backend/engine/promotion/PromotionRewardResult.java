package com.viettel.backend.engine.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromotionRewardResult<ID extends Serializable, P extends I_Product<ID>> implements Serializable {

    private static final long serialVersionUID = 7495282081559264914L;

    private BigDecimal amount;
    private BigDecimal quantity;
    private P product;
    private String productText;

    public PromotionRewardResult() {
        super();
    }

    public PromotionRewardResult(BigDecimal amount, BigDecimal quantity, P product, String productText) {
        super();
        this.setAmount(amount);
        this.quantity = quantity;
        this.product = product;
        this.productText = productText;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public P getProduct() {
        return product;
    }

    public void setProduct(P product) {
        this.product = product;
    }
    
    public String getProductText() {
        return productText;
    }
    
    public void setProductText(String productText) {
        this.productText = productText;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        PromotionRewardResult<ID, P> reward = (PromotionRewardResult<ID, P>) obj;

        if (!compareObject(getAmount(), reward.getAmount())) {
            return false;
        }

        if (!compareObject(getQuantity(), reward.getQuantity())) {
            return false;
        }

        if (!compareProduct(getProduct(), reward.getProduct())) {
            return false;
        }
        
        if (!compareObject(getProductText(), reward.getProductText())) {
            return false;
        }

        return true;
    }

    private boolean compareObject(Object o1, Object o2) {
        if (o1 == null || o2 == null) {
            return true;
        }

        if (o1 != null && o2 != null && o1.equals(o2)) {
            return true;
        }

        return false;
    }

    private boolean compareProduct(I_Product<ID> o1, I_Product<ID> o2) {
        if (o1 == null || o2 == null) {
            return true;
        }

        if (o1 != null && o2 != null && o1.getId() != null && o2.getId() != null && o1.getId().equals(o2.getId())) {
            return true;
        }

        return false;
    }

}
