package com.viettel.backend.exeption;

/**
 * The Class BussinessException.
 */
public class BusinessException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6127762932224120122L;

	/**
	 * Instantiates a new bussiness exception.
	 */
	public BusinessException() {
	}

	/**
	 * Instantiates a new bussiness exception.
	 * 
	 * @param message
	 *            the message
	 */
	public BusinessException(String message) {
		super(message);
	}
	
	public BusinessException(String messageToFormat, Object... argsForFormat) {
	    this(String.format(messageToFormat, argsForFormat));
	}
	
	/**
	 * Instantiates a new bussiness exception.
	 * 
	 * @param cause
	 *            the cause
	 */
	public BusinessException(Throwable cause) {
		super(cause);
	}

	/**
	 * Instantiates a new bussiness exception.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

}
