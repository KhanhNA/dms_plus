package com.viettel.backend.oauth2.core;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * @author thanh
 */
public class SecurityContextHelper {

    public static UserLogin getCurrentUser() {
        OAuth2Authentication oauth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        return extractUserLogin(oauth);
    }

    public static String getCurrentUserId() {
        return SecurityContextHelper.getCurrentUser().getUserId().toString();
    }

    public static boolean isUserInRole(String role) {
        UserLogin userLogin = getCurrentUser();
        if (userLogin == null) {
            return false;
        }
        return userLogin.hasRole(role);
    }
    
    public static UserLogin extractUserLogin(OAuth2Authentication oauth) {
        if (oauth == null) {
            return null;
        }
        UserAuthenticationToken auth = (UserAuthenticationToken) oauth.getUserAuthentication();
        return auth.getUserLogin();
    }
}
