package com.viettel.backend.oauth2.core;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;

import com.viettel.backend.domain.User;
import com.viettel.backend.service.AuthenticationService;

/**
 * Provider for users of system
 * 
 * @author thanh
 */
public class UserAuthenticationProvider implements AuthenticationProvider {

    public static final String ROLE_PREFIX = "ROLE_";

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String username = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();
        if (StringUtils.isEmpty(username)) {
            throw new BadCredentialsException("Bad User Credentials.");
        }
        username = username.toLowerCase();
        
        User user = authenticationService.getUserByUsername(username);

        if (user != null && passwordEncoder.matches(password, user.getPassword())) {
            List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
            // Always have role USER
            grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));

            if (user.getRoles() == null || user.getRoles().isEmpty()) {
                throw new BadCredentialsException("User has no role");
            }

            for (String roleCode : user.getRoles()) {
                if (!roleCode.startsWith(ROLE_PREFIX)) {
                    roleCode = ROLE_PREFIX + roleCode;
                }
                grantedAuthorities.add(new SimpleGrantedAuthority(roleCode));
            }

            UserAuthenticationToken auth = new UserAuthenticationToken(username,
                    authentication.getCredentials(), grantedAuthorities);

            UserLogin userLogin = new UserLogin(user.getClientId(), user.getId(), user.getUsername(), user.getRoles());

            auth.setUserLogin(userLogin);

            return auth;
        } else {
            throw new BadCredentialsException("Bad User Credentials.");
        }
    }

    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }
}