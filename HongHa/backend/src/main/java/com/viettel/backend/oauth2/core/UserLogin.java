package com.viettel.backend.oauth2.core;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.util.StringUtils;

public class UserLogin implements Serializable {

    private static final long serialVersionUID = -7942635748301089503L;

    private ObjectId clientId;
    private ObjectId userId;
    private String username;
    private List<String> roles;
    
    public UserLogin() {
    	
    }

    public UserLogin(ObjectId clientId, ObjectId userId, String username, List<String> roles) {
        this.clientId = clientId;
        this.userId = userId;
        this.username = username;
        this.roles = roles;
    }

    public ObjectId getClientId() {
        return clientId;
    }

    public void setClientId(ObjectId clientId) {
        this.clientId = clientId;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public boolean hasRole(String role) {
        if (this.roles == null || StringUtils.isEmpty(role)) {
            return false;
        }
        
        role = role.toUpperCase();
        return this.roles.contains(role);
    }
}
