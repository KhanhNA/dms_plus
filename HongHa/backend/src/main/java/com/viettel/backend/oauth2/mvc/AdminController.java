package com.viettel.backend.oauth2.mvc;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.viettel.backend.dto.UserLoginDto;
import com.viettel.backend.oauth2.core.SecurityContextHelper;
import com.viettel.backend.oauth2.core.SimpleUserApprovalHandler;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.AuthenticationService;

/**
 * Controller for resetting the token store for testing purposes.
 * 
 * @author Dave Syer
 */
@Controller
public class AdminController {

	@Autowired
	private ConsumerTokenServices tokenServices;

	@Autowired
	private TokenStore tokenStore;

	@Autowired
	private SimpleUserApprovalHandler userApprovalHandler;
	
	@Autowired
	private AuthenticationService userService;

//	@RequestMapping("/oauth/cache_approvals")
//	@ResponseStatus(HttpStatus.NO_CONTENT)
//	public void startCaching() throws Exception {
//		userApprovalHandler.setUseApprovalStore(true);
//	}
//
//	@RequestMapping("/oauth/uncache_approvals")
//	@ResponseStatus(HttpStatus.NO_CONTENT)
//	public void stopCaching() throws Exception {
//		userApprovalHandler.setUseApprovalStore(false);
//	}
//
//	@RequestMapping("/oauth/clients/{client}/users/{user}/tokens")
//	@ResponseBody
//	public Collection<OAuth2AccessToken> listTokensForUser(@PathVariable String client, @PathVariable String user,
//			Principal principal) throws Exception {
//		checkResourceOwner(user, principal);
//		return enhance(tokenStore.findTokensByClientIdAndUserName(client, user));
//	}

	@RequestMapping(value = "/oauth/users/{user}/tokens/{token}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> revokeToken(@PathVariable String user, @PathVariable String token, Principal principal)
			throws Exception {
		checkResourceOwner(user, principal);
		if (tokenServices.revokeToken(token)) {
			return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
		}
		else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}

//	@RequestMapping("/oauth/clients/{client}/tokens")
//	@ResponseBody
//	public Collection<OAuth2AccessToken> listTokensForClient(@PathVariable String client) throws Exception {
//		return enhance(tokenStore.findTokensByClientId(client));
//	}
	
	@RequestMapping("/oauth/userinfo")
	public ResponseEntity<?> getUserInfo() throws Exception {
		UserLogin userLogin = SecurityContextHelper.getCurrentUser();
		UserLoginDto userLoginDto = userService.getUserLoginDto(userLogin);
		return new ResponseEntity<UserLoginDto>(userLoginDto, HttpStatus.OK);
	}

//	private Collection<OAuth2AccessToken> enhance(Collection<OAuth2AccessToken> tokens) {
//		Collection<OAuth2AccessToken> result = new ArrayList<OAuth2AccessToken>();
//		for (OAuth2AccessToken prototype : tokens) {
//			DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(prototype);
//			OAuth2Authentication authentication = tokenStore.readAuthentication(token);
//			if (authentication == null) {
//				continue;
//			}
//			String clientId = authentication.getOAuth2Request().getClientId();
//			if (clientId != null) {
//				Map<String, Object> map = new HashMap<String, Object>(token.getAdditionalInformation());
//				map.put("client_id", clientId);
//				token.setAdditionalInformation(map);
//				result.add(token);
//			}
//		}
//		return result;
//	}

	private void checkResourceOwner(String user, Principal principal) {
		if (principal instanceof OAuth2Authentication) {
			OAuth2Authentication authentication = (OAuth2Authentication) principal;
			if (!authentication.isClientOnly() && !user.equals(principal.getName())) {
				throw new AccessDeniedException(String.format("User '%s' cannot obtain tokens for user '%s'",
						principal.getName(), user));
			}
		}
	}

	/**
	 * @param userApprovalHandler the userApprovalHandler to set
	 */
	public void setUserApprovalHandler(SimpleUserApprovalHandler userApprovalHandler) {
		this.userApprovalHandler = userApprovalHandler;
	}

	/**
	 * @param tokenServices the consumerTokenServices to set
	 */
	public void setTokenServices(ConsumerTokenServices tokenServices) {
		this.tokenServices = tokenServices;
	}

	/**
	 * @param tokenStore the tokenStore to set
	 */
	public void setTokenStore(TokenStore tokenStore) {
		this.tokenStore = tokenStore;
	}

}
