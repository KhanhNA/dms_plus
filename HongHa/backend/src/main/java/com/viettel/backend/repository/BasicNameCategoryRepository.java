package com.viettel.backend.repository;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.NameCategory;

public interface BasicNameCategoryRepository<D extends NameCategory> extends BasicCategoryRepository<D> {

    public boolean existsByName(ObjectId clientId, ObjectId id, String name);
    
}
