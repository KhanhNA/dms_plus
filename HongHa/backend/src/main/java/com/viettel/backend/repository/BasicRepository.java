package com.viettel.backend.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.data.repository.NoRepositoryBean;

import com.viettel.backend.domain.PO;

@NoRepositoryBean
public interface BasicRepository<D extends PO> extends Serializable {

    //** ACTIVE = TRUE **
    public D getById(ObjectId clientId, ObjectId id);

    public List<D> getListByIds(ObjectId clientId, Collection<ObjectId> ids);

    public boolean exists(ObjectId clientId, ObjectId id);

    public boolean exists(ObjectId clientId, Collection<ObjectId> ids);

    public D save(ObjectId clientId, D domain);

    public boolean delete(ObjectId clientId, ObjectId id);
    
    public Map<ObjectId, D> getMapByIds(ObjectId clientId, Collection<ObjectId> ids);
    
    //** ACTIVE = BOTH **
    public D getByIdWithDeactivated(ObjectId clientId, ObjectId id);

    public List<D> getListByIdsWithDeactivated(ObjectId clientId, Collection<ObjectId> ids);

    public Map<ObjectId, D> getMapByIdsWithDeactivated(ObjectId clientId, Collection<ObjectId> ids);
    
    public boolean setActive(ObjectId clientId, ObjectId id, boolean active);

}
