package com.viettel.backend.repository;

import java.io.Serializable;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.ClientConfig;

public interface ClientConfigRepository extends Serializable {

    public ClientConfig getClientConfig(ObjectId clientId);

    public ClientConfig save(ObjectId clientId, ClientConfig domain);

}
