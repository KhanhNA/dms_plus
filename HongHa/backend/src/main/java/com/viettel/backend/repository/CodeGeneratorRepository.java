package com.viettel.backend.repository;

import java.io.Serializable;
import java.util.List;

public interface CodeGeneratorRepository extends Serializable {

    public String getDistributorCode(String clientId);

    public String getOrderCode(String clientId);

    public String getCustomerCode(String clientId);
    
    public List<String> getBatchCustomerCode(String clientId, int size);
}
