package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.CustomerCheck;
import com.viettel.backend.entity.SimpleDate.Period;

/**
 * Repository for customer check
 * 
 */
public interface CustomerCheckRepository extends BasicRepository<CustomerCheck> {

    public List<CustomerCheck> getBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            Sort sort);

    public List<CustomerCheck> getByStoreCheckers(ObjectId clientId, Collection<ObjectId> storeCheckerIds,
            Period period, Sort sort);

    public boolean existsByCustomerAndPeriod(ObjectId clientId, ObjectId customerId, Period period);

}
