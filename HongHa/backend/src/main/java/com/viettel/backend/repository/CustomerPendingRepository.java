package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Customer;

public interface CustomerPendingRepository extends BasicRepository<Customer> {

    public List<Customer> getCustomersByCreatedUsers(ObjectId clientId, Collection<ObjectId> userIds, Integer status,
            String search, Pageable pageable, Sort sort);

    public long countCustomersByCreatedUsers(ObjectId clientId, Collection<ObjectId> userIds, Integer status,
            String search);
    
    public List<Customer> getCustomersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Integer status,
            String search, Pageable pageable, Sort sort);

    public long countCustomersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Integer status,
            String search);

}
