package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.entity.SimpleDate;

/**
 * Repository for customer approved
 * 
 * @author trungkh
 *
 */
public interface CustomerRepository extends BasicNameCategoryRepository<Customer> {

    public List<Customer> getCustomers(ObjectId clientId, String search, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIds(ObjectId clientId, String search);

    public long count(ObjectId clientId, String search);
    
    public void insertCustomers(ObjectId clientId, Collection<Customer> customers);

    // ** BY DISTRIBUTOR **
    public List<Customer> getCustomersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, boolean withAvailable,
            String search, Integer dayOfWeek, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, boolean withAvailable,
            String search, Integer dayOfWeek);

    public long countCustomersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, boolean withAvailable,
            String search, Integer dayOfWeek);

    // ** UNSCHEDULED **
    public List<Customer> getCustomersByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId,
            String search, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId, String search);

    public long countCustomersByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId, String search);

    // ** EDIT SCHEDULE **
    public void clearCustomerSchedule(ObjectId clientId, Collection<ObjectId> customerIds);

    public void updateCustomerSchedule(ObjectId clientId, ObjectId customerId, CustomerSchedule customerSchedule);

    // ** BY ROUTE **
    public List<Customer> getCustomersByRoute(ObjectId clientId, Collection<ObjectId> routeIds, String search,
            Integer dayOfWeek, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsByRoute(ObjectId clientId, Collection<ObjectId> routeIds, String search);

    public long countCustomersByRoute(ObjectId clientId, Collection<ObjectId> routeIds, String search,
            Integer dayOfWeek);

    public List<Customer> getCustomersByRouteDate(ObjectId clientId, Collection<ObjectId> routeIds,
            String search, SimpleDate date, Pageable pageable, Sort sort);

    public Set<ObjectId> getCustomerIdsByRouteDate(ObjectId clientId, Collection<ObjectId> routeIds,
            String search, SimpleDate date);

    public long countCustomersByRouteDate(ObjectId clientId, Collection<ObjectId> routeIds, String search,
            SimpleDate date);

    public boolean checkCustomerTypeUsed(ObjectId clientId, ObjectId customerTypeId);

    public boolean checkDistributorUsed(ObjectId clientId, ObjectId distributorId);
    
    public boolean checkRouteUsed(ObjectId clientId, ObjectId routeId);

}
