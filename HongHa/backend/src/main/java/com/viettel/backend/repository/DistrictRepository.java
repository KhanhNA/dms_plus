package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.District;

public interface DistrictRepository extends BasicNameCategoryRepository<District> {
    
    public List<District> getRoutesByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds);
    
    public Set<ObjectId> getRouteIdsByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds);
    
    public boolean existsByNameByDistributor(ObjectId clientId, ObjectId distributorId, ObjectId id, String name);

}
