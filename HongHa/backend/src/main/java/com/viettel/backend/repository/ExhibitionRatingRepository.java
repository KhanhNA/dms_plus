package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.ExhibitionRating;

public interface ExhibitionRatingRepository extends BasicRepository<ExhibitionRating> {

    public List<ExhibitionRating> getExhibitionRatingsByCustomers(ObjectId clientId, 
            Collection<ObjectId> exhibitionId, Collection<ObjectId> customerIds);
    
    public List<ExhibitionRating> getExhibitionRatings(ObjectId clientId, Collection<ObjectId> exhibitionId);
    
}
