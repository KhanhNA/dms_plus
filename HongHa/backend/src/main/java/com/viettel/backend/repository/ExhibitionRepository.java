package com.viettel.backend.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Exhibition;

/**
 * @author thanh
 */
public interface ExhibitionRepository extends BasicCategoryRepository<Exhibition> {

    public List<Exhibition> getExhibitions(ObjectId clientId, String search, Pageable pageable, Sort sort);

    public long count(ObjectId clientId, String search);

    public List<Exhibition> getExhibitionsByCustomer(ObjectId clientId, ObjectId customerId, String search,
            Pageable pageable, Sort sort);

}
