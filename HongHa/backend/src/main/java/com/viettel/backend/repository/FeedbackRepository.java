package com.viettel.backend.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Visit;

public interface FeedbackRepository extends Serializable {

    public Visit getById(ObjectId clientId, ObjectId visitId);

    public List<Visit> getFeedbackByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Pageable pageable,
            Sort sort);

    public long countFeedbackByCustomers(ObjectId clientId, Collection<ObjectId> customerIds);

    public long countFeedbackByCustomersUnread(ObjectId clientId, Collection<ObjectId> customerIds);

    public void markAsRead(ObjectId clientId, ObjectId visitId);

}
