package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Order;

public interface OrderPendingRepository extends BasicRepository<Order> {

    public List<Order> getPendingOrdersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds,
            Pageable pageable, Sort sort);

    public long countPendingOrdersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds);

    public boolean checkCustomerHasPendingOrder(ObjectId clientId, ObjectId customerId);

    public List<Order> getAllOrdersByCustomersToday(ObjectId clientId, Collection<ObjectId> customerIds, Sort sort);

}
