package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.Route;

public interface RouteRepository extends BasicNameCategoryRepository<Route> {
    
    public List<Route> getRoutesByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds);
    
    public Set<ObjectId> getRouteIdsByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds);
    
    public Set<ObjectId> getRouteIdsBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds);

    public void clearSalesmanRoute(ObjectId clientId, Collection<ObjectId> salesmanIds);
    
    public boolean existsByNameByDistributor(ObjectId clientId, ObjectId distributorId, ObjectId id, String name);

}
