package com.viettel.backend.repository;

import java.io.Serializable;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.SalesConfig;

public interface SalesConfigRepository extends Serializable {

    public SalesConfig getSalesConfig(ObjectId clientId);

    public SalesConfig save(ObjectId clientId, SalesConfig domain);

}
