package com.viettel.backend.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.embed.SurveyAnswer;

public interface SurveyAnswerRepository extends Serializable {
    
    public Set<ObjectId> getSurveyIdsDoneByCustomer(ObjectId clientId, ObjectId customerId, Collection<ObjectId> surveyIds);

    public List<SurveyAnswer> getSurveyAnswersByCustomers(ObjectId clientId, ObjectId surveyId,
            Collection<ObjectId> customerIds);
    
    public List<SurveyAnswer> getSurveyAnswers(ObjectId clientId, ObjectId surveyId);
    
}
