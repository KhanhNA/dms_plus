package com.viettel.backend.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Survey;

public interface SurveyRepository extends BasicCategoryRepository<Survey> {

    public List<Survey> getSurveys(ObjectId clientId, String search, Pageable pageable, Sort sort);

    public long count(ObjectId clientId, String search);

    public List<Survey> getSurveyAvailableByCustomer(ObjectId clientId, ObjectId customerId);

}
