package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.viettel.backend.domain.Target;

public interface TargetRepository extends BasicRepository<Target> {

    public List<Target> getTargetsBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, int month, int year,
            Pageable pageable, Sort sort);

    public long countTargetsBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, int month, int year);

    public boolean existsTargetBySalesman(ObjectId clientId, ObjectId salesmanId, int month, int year);

    public Target getTargetBySalesman(ObjectId clientId, ObjectId salesmanId, int month, int year);

}
