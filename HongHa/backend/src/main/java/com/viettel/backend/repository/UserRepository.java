package com.viettel.backend.repository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.User;

/**
 * @author trungkh
 */
public interface UserRepository extends BasicCategoryRepository<User> {

    public List<User> getUsersByRole(ObjectId clientId, String roleCode);

    public Set<ObjectId> getUserIdsByRole(ObjectId clientId, String roleCode);
    
    public List<User> getDistributorUsers(ObjectId clientId, Collection<ObjectId> distributorIds);

    public Set<ObjectId> getDistributorUserIds(ObjectId clientId, Collection<ObjectId> distributorIds);

    // ** SPECIAL **
    public List<User> getSalesmenByStoreCheckers(ObjectId clientId, Collection<ObjectId> storeChekerIds);

    public Set<ObjectId> getSalesmanIdsByStoreCheckers(ObjectId clientId, Collection<ObjectId> storeChekerIds);

    public List<User> getSalesmenBySupervisors(ObjectId clientId, Collection<ObjectId> supervisorIds);

    public Set<ObjectId> getSalesmanIdsBySupervisors(ObjectId clientId, Collection<ObjectId> supervisorIds);

    public List<User> getSalesmenByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds);

    public Set<ObjectId> getSalesmanIdsByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds);

    public boolean checkDistributorUsed(ObjectId clientId, ObjectId distributorId);

    // ** DISTRIBUTOR **
    public void updateSalesmenOfDistributor(ObjectId clientId, ObjectId distributorId, Collection<ObjectId> salesmanIds);

    public void updateSalesmenOfStoreChecker(ObjectId clientId, ObjectId storeCheckerId,
            Collection<ObjectId> salesmanIds);

}
