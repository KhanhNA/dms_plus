package com.viettel.backend.repository;

import java.util.List;

import org.bson.types.ObjectId;

import com.viettel.backend.domain.VisitAndOrder;
import com.viettel.backend.entity.SimpleDate.Period;

/**
 * User only for init cache
 */
public interface VisitAndOrderRepository {

    /**
     * User only for init cache
     */
    public List<VisitAndOrder> getDataByCreatedTime(ObjectId clientId, Period period);
    
}
