package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;

import com.viettel.backend.domain.NameCategoryByDistributor;
import com.viettel.backend.domain.POSearchable;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.util.CriteriaUtils;

public abstract class BasicCategoryRepositoryImpl<D extends POSearchable> extends BasicRepositoryImpl<D> implements
        BasicCategoryRepository<D> {

    private static final long serialVersionUID = -8958421384263600937L;
    
    @Override
    protected Boolean getDefaultDraftFilter() {
        return false;
    }
    
    protected Criteria getDistributorCriteria(Collection<ObjectId> distributorIds) {
        if (distributorIds != null) {
            return Criteria.where(NameCategoryByDistributor.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }
        
        return null;
    }

    @Override
    public List<D> getAll(ObjectId clientId, Collection<ObjectId> distributorIds) {
        return _getListWithDraft(clientId, true, false, getDistributorCriteria(distributorIds), null, null);
    }

    @Override
    public List<D> getListWithDraft(ObjectId clientId, Collection<ObjectId> distributorIds, String search, Boolean active, Boolean draft, Pageable pageable, Sort sort) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria criteria = CriteriaUtils.andOperator(searchCriteria, getDistributorCriteria(distributorIds));
        return _getListWithDraft(clientId, active, draft, criteria, pageable, sort);
    }

    @Override
    public long countWithDraft(ObjectId clientId, Collection<ObjectId> distributorIds, String search, Boolean active, Boolean draft) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria criteria = CriteriaUtils.andOperator(searchCriteria, getDistributorCriteria(distributorIds));
        return _countWithDraft(clientId, active, draft, criteria);
    }
    
    @Override
    public D getByIdWithDraft(ObjectId clientId, ObjectId id) {
        return _getByIdWithDraft(clientId, true, id);
    }

    @Override
    public boolean enable(ObjectId clientId, ObjectId id) {
        return _enableDraft(clientId, id);
    }

}
