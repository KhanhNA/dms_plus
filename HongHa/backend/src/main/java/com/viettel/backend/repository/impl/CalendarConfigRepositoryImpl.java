package com.viettel.backend.repository.impl;

import java.util.LinkedList;
import java.util.List;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viettel.backend.cache.redis.CacheManager;
import com.viettel.backend.domain.CalendarConfig;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class CalendarConfigRepositoryImpl extends BasicRepositoryImpl<CalendarConfig> implements
        CalendarConfigRepository {

    private static final long serialVersionUID = -8401381924226354121L;
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private CacheManager cacheManager;

    @Override
    public CalendarConfig getCalendarConfig(ObjectId clientId) {
        return super._getFirst(clientId, true, null, null);
    }

    @Override
    public CalendarConfig save(ObjectId clientId, CalendarConfig domain) {
        try {
            cacheManager.getWorkingDaysCache().clear();
        } catch (Exception e) {
            logger.error("cache error", e);
        }
        
        return super._save(clientId, domain);
    }

    @Override
    public List<SimpleDate> getWorkingDays(ObjectId clientId, Period period) {
        List<SimpleDate> dates = null;
        try {
            dates = cacheManager.getWorkingDaysCache().get(period).range(0, -1);
            if (dates != null && dates.size() > 0) {
                return dates;
            }
        } catch (Exception e) {
            logger.error("cache error", e);
        }
        
        CalendarConfig calendarConfig = getCalendarConfig(clientId);
        if (calendarConfig == null) {
            throw new UnsupportedOperationException("calendar config not found");
        }

        dates = new LinkedList<SimpleDate>();
        SimpleDate temp = period.getFromDate();
        while (temp.compareTo(period.getToDate()) < 0) {
            if (calendarConfig != null) {
                if (isWorkingDay(calendarConfig, temp)) {
                    dates.add(temp);
                }
            } else {
                dates.add(temp);
            }

            temp = DateTimeUtils.addDays(temp, 1);
        }

        try {
            SimpleDate[] arrDates = new SimpleDate[dates.size()];
            arrDates = dates.toArray(arrDates);
            cacheManager.getWorkingDaysCache().get(period).rightPushAll(arrDates);
        } catch (Exception e) {
            logger.error("cache error", e);
        }
        
        return dates;
    }

    @Override
    public SimpleDate getPreviousWorkingDay(ObjectId clientId, SimpleDate date) {
        CalendarConfig calendarConfig = getCalendarConfig(clientId);
        if (calendarConfig == null) {
            throw new UnsupportedOperationException("calendar config not found");
        }
        
        int i = 0;
        SimpleDate temp = date;
        while(true) {
            if (i > 365) {
                throw new IllegalArgumentException("not found working day from one year");
            }
            
            temp = DateTimeUtils.addDays(temp, -1);
            if (isWorkingDay(calendarConfig, temp)) {
                return temp;
            }
            i++;
        } 
    }
    
    private boolean isWorkingDay(CalendarConfig calendarConfig, SimpleDate _date) {
        if (calendarConfig == null || _date == null) {
            throw new IllegalArgumentException("calendar config or date null");
        }
        
        int day = _date.getDayOfWeek();
        int date = _date.getDate();
        int month = _date.getMonth();
        int year = _date.getYear();

        // NGAY LAM BU
        if (calendarConfig.getExceptionWorkingDays() != null) {
            for (SimpleDate simpleDate : calendarConfig.getExceptionWorkingDays()) {
                if (simpleDate.getDate() == date && simpleDate.getMonth() == month
                        && simpleDate.getYear() == year) {
                    return true;
                }
            }
        }

        // NGAY NGHI DAC BIET
        if (calendarConfig.getExceptionHolidays() != null) {
            for (SimpleDate simpleDate : calendarConfig.getExceptionHolidays()) {
                if (simpleDate.getDate() == date && simpleDate.getMonth() == month
                        && simpleDate.getYear() == year) {
                    return false;
                }
            }
        }

        // NGAY NGHI HANG NAM
        if (calendarConfig.getEveryYearHolidays() != null) {
            for (SimpleDate simpleDate : calendarConfig.getEveryYearHolidays()) {
                if (simpleDate.getDate() == date && simpleDate.getMonth() == month) {
                    return false;
                }
            }
        }

        // BO WEEKEND
        if (calendarConfig.getWorkingDays() != null && calendarConfig.getWorkingDays().contains(day)) {
            return true;
        }
        
        return false;
    }
    
}