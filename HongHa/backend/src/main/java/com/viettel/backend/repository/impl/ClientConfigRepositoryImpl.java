package com.viettel.backend.repository.impl;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viettel.backend.cache.redis.CacheManager;
import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.repository.ClientConfigRepository;

@Repository
public class ClientConfigRepositoryImpl extends AbstractRepository<ClientConfig> implements ClientConfigRepository {

    private static final long serialVersionUID = -1702535078849812226L;
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private CacheManager cacheManager;

    @Override
    public ClientConfig getClientConfig(ObjectId clientId) {
        ClientConfig clientConfig = null;
        
        try {
            clientConfig = cacheManager.getClientConfigCache().get(clientId).get();
        } catch (Exception e) {
            logger.error("cache error", e);
        }
        
        if (clientConfig == null) {
            clientConfig = _getFirst(clientId, true, null, null);
            
            try {
                cacheManager.getClientConfigCache().get(clientId).set(clientConfig);
            } catch (Exception e) {
                logger.error("cache error", e);
            }
        }
        return clientConfig;
    }

    @Override
    public ClientConfig save(ObjectId clientId, ClientConfig domain) {
        ClientConfig clientConfig = _save(clientId, domain);
        
        try {
            cacheManager.getClientConfigCache().get(clientId).set(clientConfig);
        } catch (Exception e) {
            logger.error("cache error", e);
        }
        
        return clientConfig;
    }

}
