package com.viettel.backend.repository.impl;

import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Client;
import com.viettel.backend.repository.ClientRepository;

@Repository
public class ClientRepositoryImpl extends BasicCategoryRepositoryImpl<Client> implements
        ClientRepository {

    private static final long serialVersionUID = 7117071473313734921L;

}
