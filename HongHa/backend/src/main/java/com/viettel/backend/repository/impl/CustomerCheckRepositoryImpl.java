package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.CustomerCheck;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.CustomerCheckRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class CustomerCheckRepositoryImpl extends BasicRepositoryImpl<CustomerCheck> implements CustomerCheckRepository {

    private static final long serialVersionUID = -5940564748339881209L;

    @Override
    public List<CustomerCheck> getBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            Sort sort) {
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return Collections.emptyList();
        }
        
        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(CustomerCheck.COLUMNNAME_CREATED_TIME_VALUE, period);
        Criteria salesmanCriteria = Criteria.where(CustomerCheck.COLUMNNAME_SALESMAN_ID).in(salesmanIds);
        
        return super._getList(clientId, true, CriteriaUtils.andOperator(dateCriteria, salesmanCriteria), null, sort);
    }
    
    @Override
    public List<CustomerCheck> getByStoreCheckers(ObjectId clientId, Collection<ObjectId> storeCheckerIds,
            Period period, Sort sort) {
        if (storeCheckerIds == null || storeCheckerIds.isEmpty()) {
            return Collections.emptyList();
        }
        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(CustomerCheck.COLUMNNAME_CREATED_TIME_VALUE, period);
        Criteria storeCheckerCriteria = Criteria.where(CustomerCheck.COLUMNNAME_STORECHECKER_ID).in(storeCheckerIds);
        
        return super._getList(clientId, true, CriteriaUtils.andOperator(dateCriteria, storeCheckerCriteria), null, sort);
    }

    @Override
    public boolean existsByCustomerAndPeriod(ObjectId clientId, ObjectId customerId, Period period) {
        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(CustomerCheck.COLUMNNAME_CREATED_TIME_VALUE, period);
        Criteria customerCriteria = Criteria.where(CustomerCheck.COLUMNNAME_CUSTOMER_ID).is(customerId);
        
        return super._exists(clientId, true, CriteriaUtils.andOperator(dateCriteria, customerCriteria));
    }

}
