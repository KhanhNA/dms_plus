package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.POSearchable;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class CustomerRepositoryImpl extends BasicNameCategoryRepositoryImpl<Customer> implements CustomerRepository {

    private static final long serialVersionUID = 2100546745554205377L;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria approvedCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS).is(
                Customer.APPROVE_STATUS_APPROVED);
        return approvedCriteria;
    }

    @Override
    public List<Customer> getCustomers(ObjectId clientId, String search, Pageable pageable, Sort sort) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Customer.COLUMNNAME_SEARCH, search);
        return super._getList(clientId, true, searchCriteria, pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIds(ObjectId clientId, String search) {
        List<Customer> customers = getCustomers(clientId, search, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long count(ObjectId clientId, String search) {
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);

        return super._count(clientId, true, searchCriteria);
    }

    @Override
    public void insertCustomers(ObjectId clientId, Collection<Customer> customers) {
        super._insertBatch(clientId, customers);
    }

    // ** BY DISTRIBUTOR **
    @Override
    public List<Customer> getCustomersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds,
            boolean withAvailable, String search, Integer dayOfWeek, Pageable pageable, Sort sort) {
        if (distributorIds == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        if (distributorIds.isEmpty()) {
            Collections.emptyList();
        }

        if (withAvailable) {
            distributorIds = new HashSet<ObjectId>(distributorIds);
            distributorIds.add(null);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        Criteria dayCriteria = getDayCriteria(clientId, dayOfWeek);

        return super._getList(clientId, true,
                CriteriaUtils.andOperator(distributorCriteria, searchCriteria, dayCriteria), pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, boolean withAvailable,
            String search, Integer dayOfWeek) {
        List<Customer> customers = getCustomersByDistributors(clientId, distributorIds, withAvailable, search, dayOfWeek,
                null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, boolean withAvailable,
            String search, Integer dayOfWeek) {
        if (distributorIds == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        if (distributorIds.isEmpty()) {
            Collections.emptyList();
        }

        if (withAvailable) {
            distributorIds = new HashSet<ObjectId>(distributorIds);
            distributorIds.add(null);
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        Criteria dayCriteria = getDayCriteria(clientId, dayOfWeek);

        return super
                ._count(clientId, true, CriteriaUtils.andOperator(distributorCriteria, searchCriteria, dayCriteria));
    }

    // ** UNSCHEDULED **
    @Override
    public List<Customer> getCustomersByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId,
            String search, Pageable pageable, Sort sort) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);
        Criteria scheduleCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE).is(null);

        return super._getList(clientId, true,
                CriteriaUtils.andOperator(distributorCriteria, scheduleCriteria, searchCriteria), pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId, String search) {
        List<Customer> customers = getCustomersByDistributorUnscheduled(clientId, distributorId, search, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomersByDistributorUnscheduled(ObjectId clientId, ObjectId distributorId, String search) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);
        Criteria scheduleCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE).is(null);

        return super._count(clientId, true,
                CriteriaUtils.andOperator(distributorCriteria, scheduleCriteria, searchCriteria));
    }

    // ** EDIT SCHEDULE *
    @Override
    public void clearCustomerSchedule(ObjectId clientId, Collection<ObjectId> customerIds) {
        if (customerIds != null && !customerIds.isEmpty()) {
            Update update = new Update();
            update.set(Customer.COLUMNNAME_SCHEDULE, null);

            Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(customerIds);
            super._updateMulti(clientId, true, criteria, update);
        }
    }

    @Override
    public void updateCustomerSchedule(ObjectId clientId, ObjectId customerId, CustomerSchedule customerSchedule) {
        if (customerId == null) {
            throw new IllegalArgumentException("customerId is null");
        }

        Update update = new Update();
        update.set(Customer.COLUMNNAME_SCHEDULE, customerSchedule);

        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(customerId);
        super._updateMulti(clientId, true, criteria, update);
    }

    // ** BY SCHEDULE **
    @Override
    public List<Customer> getCustomersByRoute(ObjectId clientId, Collection<ObjectId> routeIds, String search,
            Integer dayOfWeek, Pageable pageable, Sort sort) {
        if (routeIds == null || routeIds.isEmpty()) {
            return Collections.<Customer> emptyList();
        }

        Criteria scheduleCriteria = Criteria.where(
                Customer.COLUMNNAME_SCHEDULE + "." + CustomerSchedule.COLUMNNAME_ROUTE_ID).in(routeIds);
        Criteria dayCriteria = getDayCriteria(clientId, dayOfWeek);
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);

        return _getList(clientId, true, CriteriaUtils.andOperator(scheduleCriteria, dayCriteria, searchCriteria),
                pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsByRoute(ObjectId clientId, Collection<ObjectId> routeIds, String search) {
        List<Customer> customers = getCustomersByRoute(clientId, routeIds, search, null, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomersByRoute(ObjectId clientId, Collection<ObjectId> routeIds, String search,
            Integer dayOfWeek) {
        if (routeIds == null || routeIds.isEmpty()) {
            return 0l;
        }

        Criteria scheduleCriteria = Criteria.where(
                Customer.COLUMNNAME_SCHEDULE + "." + CustomerSchedule.COLUMNNAME_ROUTE_ID).in(routeIds);
        Criteria dayCriteria = getDayCriteria(clientId, dayOfWeek);
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);

        return _count(clientId, true, CriteriaUtils.andOperator(scheduleCriteria, dayCriteria, searchCriteria));
    }

    @Override
    public List<Customer> getCustomersByRouteDate(ObjectId clientId, Collection<ObjectId> routeIds,
            String search, SimpleDate date, Pageable pageable, Sort sort) {
        if (routeIds == null) {
            throw new IllegalArgumentException("routeIds is null");
        }

        if (routeIds.isEmpty()) {
            return Collections.<Customer> emptyList();
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria scheduleCriteria = getScheduleCriteria(clientId, routeIds, date);

        return _getList(clientId, true, CriteriaUtils.andOperator(scheduleCriteria, searchCriteria), pageable, sort);
    }

    @Override
    public Set<ObjectId> getCustomerIdsByRouteDate(ObjectId clientId, Collection<ObjectId> routeIds,
            String search, SimpleDate date) {
        List<Customer> customers = getCustomersByRouteDate(clientId, routeIds, search, date, null, null);
        return _getIdSet(customers);
    }

    @Override
    public long countCustomersByRouteDate(ObjectId clientId, Collection<ObjectId> routeIds, String search,
            SimpleDate date) {
        if (routeIds == null) {
            throw new IllegalArgumentException("routeIds is null");
        }

        if (routeIds.isEmpty()) {
            return 0l;
        }

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(POSearchable.COLUMNNAME_SEARCH, search);
        Criteria scheduleCriteria = getScheduleCriteria(clientId, routeIds, date);

        return _count(clientId, true, CriteriaUtils.andOperator(scheduleCriteria, searchCriteria));
    }

    @Override
    public boolean checkCustomerTypeUsed(ObjectId clientId, ObjectId customerTypeId) {
        if (customerTypeId == null) {
            throw new IllegalArgumentException("customerTypeId is null");
        }

        Criteria criteria = Criteria.where(Customer.COLUMNNAME_CUSTOMER_TYPE_ID).is(customerTypeId);

        return super._exists(clientId, true, criteria);
    }

    @Override
    public boolean checkDistributorUsed(ObjectId clientId, ObjectId distributorId) {
        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        Criteria criteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);

        return super._exists(clientId, true, criteria);
    }
    
    @Override
    public boolean checkRouteUsed(ObjectId clientId, ObjectId routeId) {
        if (routeId == null) {
            throw new IllegalArgumentException("routeId is null");
        }

        Criteria criteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE + 
                "." + CustomerSchedule.COLUMNNAME_ROUTE_ID).is(routeId);

        return super._exists(clientId, true, criteria);
    }

    // ** PRIVATE **
    private Criteria getScheduleCriteria(ObjectId clientId, Collection<ObjectId> routeIds, SimpleDate date) {
        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("client config id null");
        }

        Criteria scheduleCriteria = Criteria.where(
                Customer.COLUMNNAME_SCHEDULE + "." + CustomerSchedule.COLUMNNAME_ROUTE_ID).in(routeIds);

        int dayOfToday = date.getDayOfWeek();
        Criteria dayWeekCriteria = null;
        if (clientConfig.isComplexSchedule()) {
            Criteria weekCriteria = null;
            if (clientConfig.getNumberWeekOfFrequency() > 1) {
                weekCriteria = Criteria.where(CustomerScheduleItem.COLUMNNAME_WEEKS).all(
                        getWeekIndex(date, clientConfig));
            }

            dayWeekCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE + "." + CustomerSchedule.COLUMNNAME_ITEMS)
                    .elemMatch(
                            CriteriaUtils.andOperator(Criteria.where(CustomerScheduleItem.getDayColumnname(dayOfToday))
                                    .is(true), weekCriteria));
        } else {
            Criteria weekCriteria = null;
            if (clientConfig.getNumberWeekOfFrequency() > 1) {
                weekCriteria = Criteria.where(
                        Customer.COLUMNNAME_SCHEDULE + "." + CustomerSchedule.COLUMNNAME_ITEM + "."
                                + CustomerScheduleItem.COLUMNNAME_WEEKS).all(getWeekIndex(date, clientConfig));
            }

            dayWeekCriteria = CriteriaUtils.andOperator(
                    Criteria.where(
                            Customer.COLUMNNAME_SCHEDULE + "." + CustomerSchedule.COLUMNNAME_ITEM + "."
                                    + CustomerScheduleItem.getDayColumnname(dayOfToday)).is(true), weekCriteria);
        }

        return CriteriaUtils.andOperator(scheduleCriteria, dayWeekCriteria);
    }

    private int getWeekIndex(SimpleDate date, ClientConfig clientConfig) {
        int firstDayOfWeek = clientConfig.getFirstDayOfWeek();
        int minimalDaysInFirstWeek = clientConfig.getMinimalDaysInFirstWeek();
        // Tuan lam viec cua ngay hom nay tinh tu dau nam
        int weekOfToday = DateTimeUtils.getWeekOfYear(date, firstDayOfWeek, minimalDaysInFirstWeek);
        int weeekOfFrequency = clientConfig.getNumberWeekOfFrequency();
        // Thu tu cua tuan theo week duration
        int weekIndex = (weekOfToday % weeekOfFrequency) + 1;
        return weekIndex;
    }

    private Criteria getDayCriteria(ObjectId clientId, Integer dayOfWeek) {
        if (dayOfWeek == null) {
            return null;
        }

        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("client config id null");
        }

        if (clientConfig.isComplexSchedule()) {
            return Criteria.where(Customer.COLUMNNAME_SCHEDULE + "." + CustomerSchedule.COLUMNNAME_ITEMS).elemMatch(
                    Criteria.where(CustomerScheduleItem.getDayColumnname(dayOfWeek)).is(true));
        } else {
            return Criteria.where(
                    Customer.COLUMNNAME_SCHEDULE + "." + CustomerSchedule.COLUMNNAME_ITEM + "."
                            + CustomerScheduleItem.getDayColumnname(dayOfWeek)).is(true);
        }
    }

}
