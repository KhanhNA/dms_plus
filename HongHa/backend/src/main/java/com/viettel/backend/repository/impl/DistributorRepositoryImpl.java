package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.repository.DistributorRepository;

@Repository
public class DistributorRepositoryImpl extends BasicCategoryRepositoryImpl<Distributor> implements
        DistributorRepository {

    private static final long serialVersionUID = -7134721170250815795L;

    public DistributorRepositoryImpl() {
        super();
    }

    @Override
    public List<Distributor> getDistributorsBySupervisors(ObjectId clientId, Collection<ObjectId> supervisorIds) {
        if (supervisorIds == null || supervisorIds.isEmpty()) {
            return Collections.<Distributor> emptyList();
        }

        Criteria supervisorCriteria = Criteria.where(Distributor.COLUMNNAME_SUPERVISOR_ID).in(supervisorIds);

        return super._getList(clientId, true, supervisorCriteria, null, null);
    }

    @Override
    public Set<ObjectId> getDistributorIdsBySupervisors(ObjectId clientId, Collection<ObjectId> supervisorIds) {
        List<Distributor> distributors = getDistributorsBySupervisors(clientId, supervisorIds);
        return _getIdSet(distributors);
    }
    
    @Override
    public boolean checkSupervisorUsed(ObjectId clientId, ObjectId superviosrId) {
        if (superviosrId == null) {
            throw new IllegalArgumentException("superviosrId is null");
        }

        Criteria criteria = Criteria.where(Distributor.COLUMNNAME_SUPERVISOR_ID).is(superviosrId);

        return super._exists(clientId, true, criteria);
    }

}
