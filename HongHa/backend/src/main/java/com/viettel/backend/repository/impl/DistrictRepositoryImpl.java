package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.District;
import com.viettel.backend.domain.NameCategory;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Route;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class DistrictRepositoryImpl extends BasicNameCategoryRepositoryImpl<District> implements
        DistrictRepository {

    private static final long serialVersionUID = 7117071473313734921L;
    
    @Override
    public List<District> getRoutesByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds) {
        if (distributorIds == null || distributorIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria criteria = Criteria.where(District.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        return super._getList(clientId, true, criteria, null, null);
    }

    @Override
    public Set<ObjectId> getRouteIdsByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds) {
        List<District> routes = getRoutesByDistributors(clientId, distributorIds);
        return _getIdSet(routes);
    }

    @Override
    public boolean existsByNameByDistributor(ObjectId clientId, ObjectId distributorId, ObjectId id, String name) {
        Criteria otherCriteria = null;
        if (id != null) {
            otherCriteria = Criteria.where(PO.COLUMNNAME_ID).ne(id);
        }

        Criteria nameCriteria = CriteriaUtils.getSearchInsensitiveCriteria(NameCategory.COLUMNNAME_NAME, name);
        Criteria distributorCriteria = Criteria.where(Route.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);
        return super._countWithDraft(clientId, true, null,
                CriteriaUtils.andOperator(otherCriteria, nameCriteria, distributorCriteria)) > 0;
    }

}
