package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.ExhibitionRating;
import com.viettel.backend.repository.ExhibitionRatingRepository;
import com.viettel.backend.util.CriteriaUtils;

/**
 * @author thanh
 */
@Repository
public class ExhibitionRatingRepositoryImpl extends BasicRepositoryImpl<ExhibitionRating> implements ExhibitionRatingRepository {

    private static final long serialVersionUID = -621341622021413043L;

    @Override
    public List<ExhibitionRating> getExhibitionRatingsByCustomers(ObjectId clientId, Collection<ObjectId> exhibitionIds,
            Collection<ObjectId> customerIds) {
        Criteria exhibitionCriteria = Criteria.where(ExhibitionRating.COLUMNNAME_EXHIBITION_ID).in(exhibitionIds);
        Criteria customerCriteria = Criteria.where(ExhibitionRating.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        List<ExhibitionRating> exhibitionRatings = _getList(clientId, true, CriteriaUtils.andOperator(exhibitionCriteria, customerCriteria),
                null, null);

        if (exhibitionRatings == null || exhibitionRatings.isEmpty()) {
            return Collections.<ExhibitionRating> emptyList();
        }
        
        return exhibitionRatings;
    }

    @Override
    public List<ExhibitionRating> getExhibitionRatings(ObjectId clientId, Collection<ObjectId> exhibitionIds) {
        Criteria exhibitionCriteria = Criteria.where(ExhibitionRating.COLUMNNAME_EXHIBITION_ID).in(exhibitionIds);

        List<ExhibitionRating> exhibitionRatings = _getList(clientId, true, exhibitionCriteria, null, null);

        if (exhibitionRatings == null || exhibitionRatings.isEmpty()) {
            return Collections.<ExhibitionRating> emptyList();
        }
        
        return exhibitionRatings;
    }

}
