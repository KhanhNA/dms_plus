package com.viettel.backend.repository.impl;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.repository.ExhibitionRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.DateTimeUtils;

/**
 * @author thanh
 */
@Repository
public class ExhibitionRepositoryImpl extends BasicCategoryRepositoryImpl<Exhibition> implements ExhibitionRepository {

    private static final long serialVersionUID = -9182004570746524625L;

    @Override
    public List<Exhibition> getExhibitions(ObjectId clientId, String search, Pageable pageable, Sort sort) {
        return getListWithDraft(clientId, null, search, true, false, pageable, sort);
    }

    @Override
    public long count(ObjectId clientId, String search) {
        return countWithDraft(clientId, null, search, true, false);
    }
    
    @Override
    public List<Exhibition> getExhibitionsByCustomer(ObjectId clientId, ObjectId customerId, String search,
            Pageable pageable, Sort sort) {
       // Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Exhibition.COLUMNNAME_SEARCH, search);
        
        SimpleDate today = DateTimeUtils.getToday();

        Criteria criteria = CriteriaUtils.andOperator(
                Criteria.where(Exhibition.COLUMNNAME_DRAFT).is(false), 
                Criteria.where(Exhibition.COLUMNNAME_START_DATE_VALUE).lte(today.getValue()),
                Criteria.where(Exhibition.COLUMNNAME_END_DATE_VALUE).gte(today.getValue()));
        

        return super._getList(clientId, true, criteria, pageable, sort);
    }

}
