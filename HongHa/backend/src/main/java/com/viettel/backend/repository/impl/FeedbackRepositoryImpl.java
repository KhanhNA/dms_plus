package com.viettel.backend.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.repository.FeedbackRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class FeedbackRepositoryImpl extends AbstractRepository<Visit> implements FeedbackRepository {

    private static final long serialVersionUID = 5485843631290762184L;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isVisitCriteria = Criteria.where(Visit.COLUMNNAME_IS_VISIT).is(true);
        Criteria visitStatusCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITED);
        Criteria hasFeedbackCriteria = Criteria.where(Visit.COLUMNNAME_FEEDBACKS).ne(null);

        return CriteriaUtils.andOperator(isVisitCriteria, visitStatusCriteria, hasFeedbackCriteria);
    }

    @Override
    public Visit getById(ObjectId clientId, ObjectId visitId) {
        return _getById(clientId, true, visitId);
    }
    
    @Override
    public List<Visit> getFeedbackByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Pageable pageable,
            Sort sort) {
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        return super._getList(clientId, true, customerCriteria, pageable, sort);
    }

    @Override
    public long countFeedbackByCustomers(ObjectId clientId, Collection<ObjectId> customerIds) {
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        return _count(clientId, true, customerCriteria);
    }

    @Override
    public long countFeedbackByCustomersUnread(ObjectId clientId, Collection<ObjectId> customerIds) {
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);
        Criteria unreadCriteria = Criteria.where(Visit.COLUMNNAME_IS_FEEDBACKS_READED).is(false);

        return _count(clientId, true, CriteriaUtils.andOperator(customerCriteria, unreadCriteria));
    }

    @Override
    public void markAsRead(ObjectId clientId, ObjectId visitId) {
        List<ObjectId> ids = new ArrayList<ObjectId>();
        ids.add(visitId);

        Update update = new Update();
        update.set(Visit.COLUMNNAME_IS_FEEDBACKS_READED, true);

        super._updateMulti(clientId, true, ids, update);
    }

}
