package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class OrderPendingRepositoryImpl extends BasicRepositoryImpl<Order> implements OrderPendingRepository {

    private static final long serialVersionUID = -2035785564064751389L;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isOrderCriteria = Criteria.where(Order.COLUMNNAME_IS_ORDER).is(true);
        return isOrderCriteria;
    }

    private Criteria getPendingPeriodCriteria() {
        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("clientConfig is null");
        }

        SimpleDate today = DateTimeUtils.getToday();

        long from = DateTimeUtils.addDays(today, -clientConfig.getNumberDayOrderPendingExpire()).getValue();
        long to = DateTimeUtils.getTomorrow().getValue();

        return Criteria.where(Visit.COLUMNNAME_START_TIME_VALUE).gte(from).lt(to);
    }

    @Override
    public List<Order> getPendingOrdersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds,
            Pageable pageable, Sort sort) {
        Criteria pendingPeriodCriteria = getPendingPeriodCriteria();

        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }

        Criteria pendingStatusCriteria = Criteria.where(Order.COLUMNNAME_APPROVE_STATUS).is(
                Order.APPROVE_STATUS_PENDING);

        Criteria criteria = CriteriaUtils
                .andOperator(pendingPeriodCriteria, distributorCriteria, pendingStatusCriteria);

        return _getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countPendingOrdersByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds) {
        Criteria pendingPeriodCriteria = getPendingPeriodCriteria();

        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }

        Criteria pendingStatusCriteria = Criteria.where(Order.COLUMNNAME_APPROVE_STATUS).is(
                Order.APPROVE_STATUS_PENDING);

        Criteria criteria = CriteriaUtils
                .andOperator(pendingPeriodCriteria, distributorCriteria, pendingStatusCriteria);

        return _count(clientId, true, criteria);
    }

    @Override
    public boolean checkCustomerHasPendingOrder(ObjectId clientId, ObjectId customerId) {
        Assert.notNull(customerId);

        Criteria pendingPeriodCriteria = getPendingPeriodCriteria();

        Criteria customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(customerId);

        Criteria pendingStatusCriteria = Criteria.where(Order.COLUMNNAME_APPROVE_STATUS).is(
                Order.APPROVE_STATUS_PENDING);

        Criteria criteria = CriteriaUtils.andOperator(pendingPeriodCriteria, customerCriteria, pendingStatusCriteria);

        return _exists(clientId, true, criteria);
    }

    @Override
    public List<Order> getAllOrdersByCustomersToday(ObjectId clientId, Collection<ObjectId> customerIds, Sort sort) {
        Assert.notNull(customerIds);

        Criteria pendingPeriodCriteria = getPendingPeriodCriteria();

        Criteria customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        Criteria todayPeriod = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE,
                DateTimeUtils.getPeriodToday());

        Criteria criteria = CriteriaUtils.andOperator(pendingPeriodCriteria, customerCriteria, todayPeriod);
        
        return _getList(clientId, true, criteria, null, sort);
    }

}
