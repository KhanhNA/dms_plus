package com.viettel.backend.repository.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class OrderRepositoryImpl extends BasicRepositoryImpl<Order> implements OrderRepository {

    private static final long serialVersionUID = -8401381924226354121L;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isOrderCriteria = Criteria.where(Order.COLUMNNAME_IS_ORDER).is(true);
        Criteria approveStatusCriteria = Criteria.where(Order.COLUMNNAME_APPROVE_STATUS).is(
                Order.APPROVE_STATUS_APPROVED);
        return CriteriaUtils.andOperator(isOrderCriteria, approveStatusCriteria);
    }

    private Criteria getIndexTimeCriteria(Period period) {
        if (period == null) {
            throw new IllegalArgumentException("period is null");
        }

        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("clientConfig is null");
        }

        long from = DateTimeUtils.addDays(DateTimeUtils.truncate(period.getFromDate(), Calendar.DATE),
                -clientConfig.getNumberDayOrderPendingExpire()).getValue();
        long to = DateTimeUtils.addDays(DateTimeUtils.truncate(period.getToDate(), Calendar.DATE), 1).getValue();

        return Criteria.where(Visit.COLUMNNAME_START_TIME_VALUE).gte(from).lt(to);
    }

    @Override
    public List<Order> getOrders(ObjectId clientId, Period period, String searchCode, Pageable pageable, Sort sort) {
        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils.andOperator(indexTimeCriteria, dateCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public List<Order> getOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            String searchCode, Pageable pageable, Sort sort) {
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).in(salesmanIds);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils
                .andOperator(indexTimeCriteria, dateCriteria, salesmanCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds, Period period,
            String searchCode) {
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return 0l;
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).in(salesmanIds);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils
                .andOperator(indexTimeCriteria, dateCriteria, salesmanCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getOrderBySalesmenAndCustomers(ObjectId clientId, Collection<ObjectId> salesmanIds,
            Collection<ObjectId> customerIds, Period period, String searchCode, Pageable pageable, Sort sort) {
        Assert.notNull(salesmanIds);

        if (customerIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).in(salesmanIds);

        Criteria customerCriteria = null;
        if (customerIds != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);
        }

        Criteria searchCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);
        }

        Criteria criteria = CriteriaUtils.andOperator(indexTimeCriteria, dateCriteria, salesmanCriteria,
                customerCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderBySalesmenAndCustomers(ObjectId clientId, Collection<ObjectId> salesmanIds,
            Collection<ObjectId> customerIds, Period period, String searchCode) {
        Assert.notNull(salesmanIds);

        if (customerIds.isEmpty()) {
            return 0l;
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria salesmanCriteria = Criteria.where(Order.COLUMNNAME_SALESMAN_ID).in(salesmanIds);

        Criteria customerCriteria = null;
        if (customerIds != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);
        }

        Criteria searchCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);
        }

        Criteria criteria = CriteriaUtils.andOperator(indexTimeCriteria, dateCriteria, salesmanCriteria,
                customerCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Period period,
            String searchCode, Pageable pageable, Sort sort) {
        Assert.notNull(distributorIds);
        if (distributorIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }

        Criteria searchCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);
        }

        Criteria criteria = CriteriaUtils.andOperator(indexTimeCriteria, dateCriteria, distributorCriteria,
                searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds, Period period,
            String searchCode) {
        Assert.notNull(distributorIds);
        if (distributorIds.isEmpty()) {
            return 0l;
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        }

        Criteria searchCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);
        }

        Criteria criteria = CriteriaUtils.andOperator(indexTimeCriteria, dateCriteria, distributorCriteria,
                searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Period period,
            String searchCode, Pageable pageable, Sort sort) {
        if (customerIds == null || customerIds.isEmpty()) {
            return Collections.<Order> emptyList();
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils
                .andOperator(indexTimeCriteria, dateCriteria, customerCriteria, searchCriteria);

        return super._getList(clientId, true, criteria, pageable, sort);
    }

    @Override
    public long countOrderByCustomers(ObjectId clientId, Collection<ObjectId> customerIds, Period period,
            String searchCode) {
        if (customerIds == null || customerIds.isEmpty()) {
            return 0l;
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Order.COLUMNNAME_CODE, searchCode);

        Criteria criteria = CriteriaUtils
                .andOperator(indexTimeCriteria, dateCriteria, customerCriteria, searchCriteria);

        return super._count(clientId, true, criteria);
    }

    @Override
    public List<Order> getLastOrderByCustomer(ObjectId clientId, ObjectId customerId, int size, Period period) {
        if (customerId == null) {
            return Collections.<Order> emptyList();
        }

        Assert.notNull(period);

        Criteria indexTimeCriteria = getIndexTimeCriteria(period);

        Criteria dateCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_APPROVE_TIME_VALUE, period);

        Criteria customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(customerId);

        Criteria criteria = CriteriaUtils.andOperator(indexTimeCriteria, dateCriteria, customerCriteria);

        PageRequest pageable = new PageRequest(0, size, Direction.DESC, Order.COLUMNNAME_APPROVE_TIME_VALUE);

        return super._getList(clientId, true, criteria, pageable, null);
    }

}
