package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Product;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class ProductRepositoryImpl extends BasicNameCategoryRepositoryImpl<Product> implements ProductRepository {

    private static final long serialVersionUID = 2100546745554205377L;

    @Override
    public void insertProducts(ObjectId clientId, Collection<Product> products) {
        super._insertBatch(clientId, products);
    }
    
    @Override
    public List<Product> getProductsByCategory(ObjectId clientId, ObjectId productCategoryId) {
        if (productCategoryId == null) {
            throw new IllegalArgumentException("productCategoryId is null");
        }

        Criteria criteria = Criteria.where(Product.COLUMNNAME_CATEGORY_ID).is(productCategoryId);
        return super._getList(clientId, true, criteria, null, null);
    }

    @Override
    public List<Product> getProducts(ObjectId clientId, String search, Pageable pageable, Sort sort) {
        return getListWithDraft(clientId, null, search, true, false, pageable, sort);
    }

    @Override
    public long count(ObjectId clientId, String search) {
        return countWithDraft(clientId, null, search, true, false);
    }

    @Override
    public List<Product> getProductsNotInIds(ObjectId clientId, Collection<ObjectId> ids, Pageable pageable, Sort sort) {
        Criteria notInCriteria = Criteria.where(Product.COLUMNNAME_ID).nin(ids);
        return super._getList(clientId, true, notInCriteria, pageable, sort);
    }

    @Override
    public long countProductNotInIds(ObjectId clientId, Collection<ObjectId> ids) {
        Criteria notInCriteria = Criteria.where(Product.COLUMNNAME_ID).nin(ids);
        return super._count(clientId, true, notInCriteria);
    }

    @Override
    public boolean existsByCode(ObjectId clientId, ObjectId id, String code) {
        Criteria otherCriteria = null;
        if (id != null) {
            otherCriteria = Criteria.where(PO.COLUMNNAME_ID).ne(id);
        }

        Criteria codeCriteria = CriteriaUtils.getSearchInsensitiveCriteria(Product.COLUMNNAME_CODE, code);
        
        return super._countWithDraft(clientId, true, null, CriteriaUtils.andOperator(otherCriteria, codeCriteria)) > 0;
    }

    @Override
    public boolean checkUOMUsed(ObjectId clientId, ObjectId uomId) {
        if (uomId == null) {
            throw new IllegalArgumentException("uomId is null");
        }

        Criteria criteria = Criteria.where(Product.COLUMNNAME_UOM_ID).is(uomId);

        return super._exists(clientId, true, criteria);
    }

    @Override
    public boolean checkProductCategoryUsed(ObjectId clientId, ObjectId productCategoryId) {
        if (productCategoryId == null) {
            throw new IllegalArgumentException("productCategoryId is null");
        }

        Criteria criteria = Criteria.where(Product.COLUMNNAME_CATEGORY_ID).is(productCategoryId);

        return super._exists(clientId, true, criteria);
    }

}
