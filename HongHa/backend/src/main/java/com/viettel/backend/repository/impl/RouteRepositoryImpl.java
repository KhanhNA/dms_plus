package com.viettel.backend.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.NameCategory;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Route;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class RouteRepositoryImpl extends BasicNameCategoryRepositoryImpl<Route> implements RouteRepository {

    private static final long serialVersionUID = 1L;

    @Override
    public List<Route> getRoutesByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds) {
        if (distributorIds == null || distributorIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria criteria = Criteria.where(Route.COLUMNNAME_DISTRIBUTOR_ID).in(distributorIds);
        return super._getList(clientId, true, criteria, null, null);
    }

    @Override
    public Set<ObjectId> getRouteIdsByDistributors(ObjectId clientId, Collection<ObjectId> distributorIds) {
        List<Route> routes = getRoutesByDistributors(clientId, distributorIds);
        return _getIdSet(routes);
    }

    @Override
    public Set<ObjectId> getRouteIdsBySalesmen(ObjectId clientId, Collection<ObjectId> salesmanIds) {
        if (salesmanIds != null && !salesmanIds.isEmpty()) {
            Criteria criteria = Criteria.where(Route.COLUMNNAME_SALESMAN_ID).in(salesmanIds);
            List<Route> routes = super._getList(clientId, true, criteria, null, null);
            return _getIdSet(routes);
        }

        return Collections.emptySet();
    }

    @Override
    public void clearSalesmanRoute(ObjectId clientId, Collection<ObjectId> salesmanIds) {
        if (salesmanIds != null && !salesmanIds.isEmpty()) {
            Update update = new Update();
            update.set(Route.COLUMNNAME_SALESMAN_ID, null);

            Criteria criteria = Criteria.where(Route.COLUMNNAME_SALESMAN_ID).in(salesmanIds);
            super._updateMulti(clientId, true, criteria, update);
        }
    }

    @Override
    public boolean existsByNameByDistributor(ObjectId clientId, ObjectId distributorId, ObjectId id, String name) {
        Criteria otherCriteria = null;
        if (id != null) {
            otherCriteria = Criteria.where(PO.COLUMNNAME_ID).ne(id);
        }

        Criteria nameCriteria = CriteriaUtils.getSearchInsensitiveCriteria(NameCategory.COLUMNNAME_NAME, name);
        Criteria distributorCriteria = Criteria.where(Route.COLUMNNAME_DISTRIBUTOR_ID).is(distributorId);
        return super._countWithDraft(clientId, true, null,
                CriteriaUtils.andOperator(otherCriteria, nameCriteria, distributorCriteria)) > 0;
    }

}
