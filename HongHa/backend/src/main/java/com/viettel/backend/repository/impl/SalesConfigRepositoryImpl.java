package com.viettel.backend.repository.impl;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viettel.backend.cache.redis.CacheManager;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.repository.SalesConfigRepository;

@Repository
public class SalesConfigRepositoryImpl extends AbstractRepository<SalesConfig> implements SalesConfigRepository {

    private static final long serialVersionUID = -1702535078849812226L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private CacheManager cacheManager;
    
    @Override
    public SalesConfig getSalesConfig(ObjectId clientId) {
        SalesConfig salesConfig = null;
        
        try {
            salesConfig = cacheManager.getSalesConfigCache().get(clientId).get();
        } catch (Exception e) {
            logger.error("cache error", e);
        }
        
        
        if (salesConfig == null) {
            salesConfig = _getFirst(clientId, true, null, null);
            
            try {
                cacheManager.getSalesConfigCache().get(clientId).set(salesConfig);
            } catch (Exception e) {
                logger.error("cache error", e);
            }
        }
        return salesConfig;
    }

    @Override
    public SalesConfig save(ObjectId clientId, SalesConfig domain) {
        SalesConfig salesConfig = _save(clientId, domain);
        
        try {
            cacheManager.getSalesConfigCache().get(clientId).set(salesConfig);
        } catch (Exception e) {
            logger.error("cache error", e);
        }
        
        return salesConfig;
    }

}
