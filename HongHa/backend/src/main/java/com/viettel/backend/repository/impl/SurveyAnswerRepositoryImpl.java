package com.viettel.backend.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.SurveyAnswer;
import com.viettel.backend.repository.SurveyAnswerRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class SurveyAnswerRepositoryImpl extends AbstractRepository<Visit> implements SurveyAnswerRepository {

    private static final long serialVersionUID = -3712400671054628300L;

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isVisitCriteria = Criteria.where(Visit.COLUMNNAME_IS_VISIT).is(true);
        Criteria visitStatusCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITED);

        return CriteriaUtils.andOperator(isVisitCriteria, visitStatusCriteria);
    }

    @Override
    public Set<ObjectId> getSurveyIdsDoneByCustomer(ObjectId clientId, ObjectId customerId,
            Collection<ObjectId> surveyIds) {
        Criteria surveyCriteria = Criteria.where(Visit.COLUMNNAME_SURVEY_ANSWERS).elemMatch(
                Criteria.where(SurveyAnswer.COLUMNNAME_SURVEY_ID).in(surveyIds));
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).is(customerId);

        List<Visit> visits = _getList(clientId, true, CriteriaUtils.andOperator(surveyCriteria, customerCriteria),
                null, null);

        if (visits == null || visits.isEmpty()) {
            return Collections.<ObjectId> emptySet();
        }

        HashSet<ObjectId> ids = new HashSet<ObjectId>();
        for (Visit visit : visits) {
            if (visit.getSurveyAnswers() != null && !visit.getSurveyAnswers().isEmpty()) {
                for (SurveyAnswer surveyAnswer : visit.getSurveyAnswers()) {
                    ids.add(surveyAnswer.getSurveyId());
                }
            }
        }

        return ids;
    }

    @Override
    public List<SurveyAnswer> getSurveyAnswersByCustomers(ObjectId clientId, ObjectId surveyId,
            Collection<ObjectId> customerIds) {
        Criteria surveyCriteria = Criteria.where(Visit.COLUMNNAME_SURVEY_ANSWERS).elemMatch(
                Criteria.where(SurveyAnswer.COLUMNNAME_SURVEY_ID).is(surveyId));
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(customerIds);

        List<Visit> visits = _getList(clientId, true, CriteriaUtils.andOperator(surveyCriteria, customerCriteria),
                null, null);

        if (visits == null || visits.isEmpty()) {
            return Collections.<SurveyAnswer> emptyList();
        }

        List<SurveyAnswer> surveyAnswers = new ArrayList<SurveyAnswer>();
        for (Visit visit : visits) {
            if (visit.getSurveyAnswers() != null && !visit.getSurveyAnswers().isEmpty()) {
                for (SurveyAnswer surveyAnswer : visit.getSurveyAnswers()) {
                    surveyAnswer.setVisit(visit);
                    surveyAnswers.add(surveyAnswer);
                }
            }
        }

        return surveyAnswers;
    }
    
    @Override
    public List<SurveyAnswer> getSurveyAnswers(ObjectId clientId, ObjectId surveyId) {
        Criteria surveyCriteria = Criteria.where(Visit.COLUMNNAME_SURVEY_ANSWERS).elemMatch(
                Criteria.where(SurveyAnswer.COLUMNNAME_SURVEY_ID).is(surveyId));

        List<Visit> visits = _getList(clientId, true, surveyCriteria,
                null, null);

        if (visits == null || visits.isEmpty()) {
            return Collections.<SurveyAnswer> emptyList();
        }

        List<SurveyAnswer> surveyAnswers = new ArrayList<SurveyAnswer>();
        for (Visit visit : visits) {
            if (visit.getSurveyAnswers() != null && !visit.getSurveyAnswers().isEmpty()) {
                for (SurveyAnswer surveyAnswer : visit.getSurveyAnswers()) {
                    surveyAnswer.setVisit(visit);
                    surveyAnswers.add(surveyAnswer);
                }
            }
        }

        return surveyAnswers;
    }

}
