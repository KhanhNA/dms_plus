package com.viettel.backend.repository.impl;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.UOM;
import com.viettel.backend.repository.UOMRepository;
import com.viettel.backend.util.CriteriaUtils;

@Repository
public class UOMRepositoryImpl extends BasicNameCategoryRepositoryImpl<UOM> implements UOMRepository {

    private static final long serialVersionUID = 7117071473313734921L;

    @Override
    public boolean existsByCode(ObjectId clientId, ObjectId id, String code) {
        Criteria otherCriteria = null;
        if (id != null) {
            otherCriteria = Criteria.where(PO.COLUMNNAME_ID).ne(id);
        }

        Criteria codeCriteria = CriteriaUtils.getSearchInsensitiveCriteria(UOM.COLUMNNAME_CODE, code);
        return super._countWithDraft(clientId, true, null, CriteriaUtils.andOperator(otherCriteria, codeCriteria)) > 0;
    }

}
