package com.viettel.backend.repository.impl;

import java.util.Calendar;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.VisitAndOrder;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.VisitAndOrderRepository;
import com.viettel.backend.util.CriteriaUtils;
import com.viettel.backend.util.DateTimeUtils;

@Repository
public class VisitAndOrderRepositoryImpl extends AbstractRepository<VisitAndOrder> implements VisitAndOrderRepository {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    public List<VisitAndOrder> getDataByCreatedTime(ObjectId clientId, Period period) {
        Assert.notNull(period);
        
        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("clientConfig is null");
        }

        period.setFromDate(DateTimeUtils.addDays(DateTimeUtils.truncate(period.getFromDate(), Calendar.DATE),
                -clientConfig.getNumberDayOrderPendingExpire()));
        
        Criteria periodCriteria  = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_START_TIME_VALUE, period);
        
        Sort sort = new Sort(Visit.COLUMNNAME_START_TIME_VALUE);
        
        return _getList(clientId, true, periodCriteria, null, sort);
    }

}
