package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ClientConfigDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.impl.ClientConfigService;

@RestController(value = "adminClientConfigController")
@RequestMapping(value = "/admin/clientconfig")
public class ClientConfigController extends AbstractController {

    private static final long serialVersionUID = -2083390468606881411L;

    @Autowired
    private ClientConfigService clientConfigService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanList(@RequestParam(required = false) String distributorId) {
        ClientConfigDto dto = clientConfigService.getClientConfig(getUserLogin());
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
