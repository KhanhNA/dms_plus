package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerDto;
import com.viettel.backend.dto.CustomerForTrackingDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.AdminCustomerService;
import com.viettel.backend.service.BasicCategoryService;

@RestController(value="adminCustomerController")
@RequestMapping(value = "/admin/customer")
public class CustomerController extends
        BasicCategoryController<Customer, CustomerSimpleDto, CustomerDto, CustomerCreateDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminCustomerService adminCustomerService;

    @Override
    protected BasicCategoryService<Customer, CustomerSimpleDto, CustomerDto, CustomerCreateDto> getService() {
        return adminCustomerService;
    }
    
    /**
     * Lấy danh sách khách hàng nằm trong tuyến ngày hôm nay của một NVBH thuộc
     * NVGS hiện tại - có kèm theo trạng thái ghé thăm
     */
    @RequestMapping(value = "/today", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomersTodayBySalesman(@RequestParam(required = true) String salesmanId) {
        ListJson<CustomerForTrackingDto> results = adminCustomerService.getCustomersTodayBySalesman(getUserLogin(),
                salesmanId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
