package com.viettel.backend.restful.admin;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ImportConfirmDto;
import com.viettel.backend.dto.ImportResultDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.CustomerScheduleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.CustomerScheduleService;
import com.viettel.backend.service.ImportCustomerScheduleService;

@RestController(value = "adminCustomerScheduleController")
@RequestMapping(value = "/admin/schedule")
public class CustomerScheduleController extends AbstractController {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private CustomerScheduleService customerScheduleService;
    
    @Autowired
    private ImportCustomerScheduleService importCustomerScheduleService;

    /** Lấy danh sách lịch trình tuyến */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomerSchedulesByDistributor(
            @RequestParam(value = "distributorId", required = true) String distributorId,
            @RequestParam(value = "routeId", required = false) String routeId,
            @RequestParam(value = "all", defaultValue = "false") boolean all,
            @RequestParam(value = "q", required = false, defaultValue = "") String search,
            @RequestParam(value = "day", required = false) Integer day,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<CustomerScheduleDto> list = customerScheduleService.getCustomerSchedules(getUserLogin(), search,
                distributorId, routeId, all, day, getPageRequest(page, size));
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    /** Cập nhật danh sách lịch trình tuyến theo NPP */
    @RequestMapping(value = "/bydistributor", method = RequestMethod.PUT)
    public ResponseEntity<?> saveByDistributor(
            @RequestParam(value = "distributorId", required = true) String distributorId,
            @RequestBody @Valid List<CustomerScheduleDto> dtos) {
        customerScheduleService.saveCustomerScheduleByDistributor(getUserLogin(), distributorId, dtos);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    /** Cập nhật lịch trình tuyến cho một khách hàng */
    @RequestMapping(value = "/bycustomer", method = RequestMethod.PUT)
    public ResponseEntity<?> saveByCustomer(@RequestParam(required = true) String customerId,
            @RequestBody @Valid CustomerScheduleDto dto) {
        customerScheduleService.saveCustomerScheduleByCustomer(getUserLogin(), customerId, dto);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/import/template", method = RequestMethod.GET)
    public ResponseEntity<?> exportSurveyResult(@RequestParam(required = true) String distributorId) throws IOException {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"CustomerSchedule.xlsx\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(importCustomerScheduleService.getTemplate(
                getUserLogin(), distributorId), header, HttpStatus.OK);
        return result;
    }

    @RequestMapping(value = "/import/verify", method = RequestMethod.GET)
    public ResponseEntity<?> verify(@RequestParam(required = true) String distributorId,
            @RequestParam(required = true) String fileId) {
        ImportConfirmDto dto = importCustomerScheduleService.verify(getUserLogin(), distributorId, fileId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public ResponseEntity<?> importCustomer(@RequestParam(required = true) String distributorId,
            @RequestParam(required = true) String fileId) {
        ImportResultDto dto = importCustomerScheduleService.doImport(getUserLogin(), distributorId, fileId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
