package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.AdminCustomerTypeService;
import com.viettel.backend.service.BasicCategoryService;

@RestController(value="adminCustomerTypeController")
@RequestMapping(value = "/admin/customertype")
public class CustomerTypeController extends
        BasicCategoryController<CustomerType, NameCategoryDto, NameCategoryDto, NameCategoryDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminCustomerTypeService adminCustomerTypeService;

    @Override
    protected BasicCategoryService<CustomerType, NameCategoryDto, NameCategoryDto, NameCategoryDto> getService() {
        return adminCustomerTypeService;
    }

}
