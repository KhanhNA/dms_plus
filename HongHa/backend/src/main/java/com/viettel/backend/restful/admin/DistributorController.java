package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.dto.DistributorCreateDto;
import com.viettel.backend.dto.DistributorDto;
import com.viettel.backend.dto.DistributorSimpleDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.AdminDistributorService;
import com.viettel.backend.service.BasicCategoryService;

@RestController(value="adminDistributorController")
@RequestMapping(value = "/admin/distributor")
public class DistributorController extends
        BasicCategoryController<Distributor, DistributorSimpleDto, DistributorDto, DistributorCreateDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminDistributorService adminDistributorService;

    @Override
    protected BasicCategoryService<Distributor, DistributorSimpleDto, DistributorDto, DistributorCreateDto> getService() {
        return adminDistributorService;
    }

}
