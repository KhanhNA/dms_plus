package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.dto.ExhibitionDto;
import com.viettel.backend.dto.ExhibitionSimpleDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.AdminExhibitionService;
import com.viettel.backend.service.BasicCategoryService;

@RestController(value="adminExhibitionController")
@RequestMapping(value = "/admin/exhibition")
public class ExhibitionController extends
        BasicCategoryController<Exhibition, ExhibitionSimpleDto, ExhibitionDto, ExhibitionDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminExhibitionService adminExhibitionService;

    @Override
    protected BasicCategoryService<Exhibition, ExhibitionSimpleDto, ExhibitionDto, ExhibitionDto> getService() {
        return adminExhibitionService;
    }

}
