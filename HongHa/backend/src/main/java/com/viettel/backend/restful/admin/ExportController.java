package com.viettel.backend.restful.admin;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.service.ExportService;

@RestController(value = "adminExportController")
@RequestMapping(value = "/admin/export")
public class ExportController extends AbstractController {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    private ExportService exportService;

    @RequestMapping(value = "/visit", method = RequestMethod.GET)
    public ResponseEntity<?> exportSurveyResult(@RequestParam String date) throws IOException {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"Visit.xlsx\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>((exportService.exportVisitsByDay(getUserLogin(),
                date)), header, HttpStatus.OK);
        return result;
    }

    @RequestMapping(value = "/order/summary", method = RequestMethod.GET)
    public ResponseEntity<?> exportOrderSummaryReport(
            @RequestParam String distributorId,
            @RequestParam String fromDate, 
            @RequestParam String toDate
            ) throws IOException {
        
        InputStream reportResult = exportService.exportOrderSummary(
                getUserLogin(), distributorId, fromDate, toDate);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        String filename = "PurchaseOrderSummaryReport_" + fromDate + "_" + toDate + ".xlsx";
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");
        InputStreamResource response = new InputStreamResource(reportResult);
        
        ResponseEntity<InputStreamResource> result = new ResponseEntity<InputStreamResource>(
                response, header, HttpStatus.OK);
        return result;
    }
    
    @RequestMapping(value = "/order/detail", method = RequestMethod.GET)
    public ResponseEntity<?> exportOrderDetailReport(
            @RequestParam String distributorId,
            @RequestParam String fromDate, 
            @RequestParam String toDate
            ) throws IOException {
        
        InputStream reportResult = exportService.exportOrderDetail(
                getUserLogin(), distributorId, fromDate, toDate);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        String filename = "PurchaseOrderDetailsReport_" + fromDate + "_" + toDate + ".xlsx";
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");
        InputStreamResource response = new InputStreamResource(reportResult);
        
        ResponseEntity<InputStreamResource> result = new ResponseEntity<InputStreamResource>(
                response, header, HttpStatus.OK);
        return result;
    }

}
