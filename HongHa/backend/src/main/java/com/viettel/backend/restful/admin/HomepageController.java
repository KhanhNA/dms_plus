package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.HomepageDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.HomepageService;

@RestController(value="adminHomepageController")
@RequestMapping(value = "/admin/homepage")
public class HomepageController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private HomepageService homepageService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getHomepage() {
        HomepageDto dto = homepageService.getHomepage(getUserLogin());
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
