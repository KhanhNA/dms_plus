package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.IdDto;
import com.viettel.backend.dto.ImportConfirmDto;
import com.viettel.backend.dto.ImportResultDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.ImportCustomerService;

@RestController(value = "ImportCustomerController")
@RequestMapping(value = "/admin/import/customer")
public class ImportCustomerController extends AbstractController {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ImportCustomerService importCustomerService;

    @RequestMapping(value = "/template", method = RequestMethod.GET)
    public ResponseEntity<?> getImportCustomerTemplate() {
        String templateId = importCustomerService.getImportCustomerTemplate(getUserLogin());
        return new Envelope(new IdDto(templateId)).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/verify", method = RequestMethod.GET)
    public ResponseEntity<?> verify(@RequestParam(required = true) String distributorId,
            @RequestParam(required = true) String fileId) {
        ImportConfirmDto dto = importCustomerService.verify(getUserLogin(), distributorId, fileId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public ResponseEntity<?> importCustomer(@RequestParam(required = true) String distributorId,
            @RequestParam(required = true) String fileId) {
        ImportResultDto dto = importCustomerService.importCustomer(getUserLogin(), distributorId, fileId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
