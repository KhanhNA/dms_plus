package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.CommonReportService;

@RestController(value="adminOrderController")
@RequestMapping(value = "/admin/order")
public class OrderController extends AbstractController {

    private static final long serialVersionUID = -5643500619636563651L;

    @Autowired
    private CommonReportService commonReportService;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getOrders(
            @RequestParam(required = false) String distributorId,
            @RequestParam(required = true) String fromDate,
            @RequestParam(required = true) String toDate,
            @RequestParam(required = false) Integer page, 
            @RequestParam(required = false) Integer size) {
        ListJson<OrderSimpleDto> results = commonReportService.getOrders(getUserLogin(), distributorId, fromDate,
                toDate, getPageRequest(page, size));
        
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }
    
    /**
     * Lấy chi tiết đơn hàng đã được duyệt theo ID
     */
    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET)
    public ResponseEntity<?> getOrderDetail(@PathVariable String orderId) {
        OrderDto result = commonReportService.getOrderById(getUserLogin(), orderId);
        return new Envelope(result).toResponseEntity(HttpStatus.OK);
    }
    
}
