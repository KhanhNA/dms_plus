package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.Promotion;
import com.viettel.backend.dto.PromotionCreateDto;
import com.viettel.backend.dto.PromotionDto;
import com.viettel.backend.dto.PromotionSimpleDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.service.AdminPromotionService;
import com.viettel.backend.service.BasicCategoryService;

@RestController("adminPromotionController")
@RequestMapping(value = "/admin/promotion")
public class PromotionController extends
        BasicCategoryController<Promotion, PromotionSimpleDto, PromotionDto, PromotionCreateDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminPromotionService adminPromotionService;

    @Override
    protected BasicCategoryService<Promotion, PromotionSimpleDto, PromotionDto, PromotionCreateDto> getService() {
        return adminPromotionService;
    }

}
