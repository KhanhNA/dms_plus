package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.Route;
import com.viettel.backend.dto.RouteCreateDto;
import com.viettel.backend.dto.RouteDto;
import com.viettel.backend.restful.BasicNameCategoryByDistributorController;
import com.viettel.backend.service.BasicNameCategoryByDistributorService;
import com.viettel.backend.service.RouteService;

@RestController(value = "adminRouteController")
@RequestMapping(value = "/admin/route")
public class RouteController extends
        BasicNameCategoryByDistributorController<Route, RouteDto, RouteDto, RouteCreateDto> {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    private RouteService routeService;

    @Override
    protected BasicNameCategoryByDistributorService<Route, RouteDto, RouteDto, RouteCreateDto> getService() {
        return routeService;
    }

}
