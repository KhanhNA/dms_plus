package com.viettel.backend.restful.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.IdDto;
import com.viettel.backend.dto.SalesConfigDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.AdminSalesConfigService;

@RestController
@RequestMapping(value = "/admin/salesconfig")
public class SalesConfigController extends AbstractController {

	private static final long serialVersionUID = 2394833065851281843L;
	
	@Autowired
	private AdminSalesConfigService adminSalesConfigService;
	
    // SAVE
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody @Valid SalesConfigDto dto) {
        String id = adminSalesConfigService.save(getUserLogin(), dto);
        return new Envelope(new IdDto(id)).toResponseEntity(HttpStatus.OK); 
    }

    // DETAIL
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> detail() {
        SalesConfigDto dto = adminSalesConfigService.getSalesConfigDto(getUserLogin());
    	return new Envelope(dto).toResponseEntity(HttpStatus.OK); 
    }
    
}
