package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.AdminUserService;

@RestController(value = "adminSalesmanController")
@RequestMapping(value = "/admin/salesman")
public class SalesmanController extends AbstractController {

    private static final long serialVersionUID = -2083390468606881411L;

    @Autowired
    private AdminUserService adminUserService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanList(@RequestParam(required = false) String distributorId) {
        ListJson<UserSimpleDto> results = adminUserService.getSalesmen(getUserLogin(), distributorId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
