package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserDto;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.restful.BasicCategoryController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.AdminUserService;
import com.viettel.backend.service.BasicCategoryService;

@RestController
@RequestMapping(value = "/admin/user")
public class UserController extends BasicCategoryController<User, UserSimpleDto, UserDto, UserDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private AdminUserService adminUserService;

    @Override
    protected BasicCategoryService<User, UserSimpleDto, UserDto, UserDto> getService() {
        return adminUserService;
    }

    @RequestMapping(value = "/{id}/resetpassword", method = RequestMethod.PUT)
    public final ResponseEntity<?>  resetPassword(@PathVariable String id) {
        adminUserService.resetPassword(getUserLogin(), id);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/supervisor", method = RequestMethod.GET)
    public final ResponseEntity<?> getSupervisors() {
        ListJson<UserSimpleDto> dtos = adminUserService.getSupervisors(getUserLogin());
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/salesman/bydistributor", method = RequestMethod.GET)
    public final ResponseEntity<?> getSalesmanByDistributorWithAvailable(
            @RequestParam(required = true) String distributorId) {
        ListJson<UserSimpleDto> dtos = adminUserService.getSalesmanByDistributorWithAvailable(getUserLogin(),
                distributorId);
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/salesman/bystorechecker", method = RequestMethod.GET)
    public final ResponseEntity<?> getSalesmanByStoreCheckerWithAvailable(
            @RequestParam(required = true) String storeCheckerId) {
        ListJson<UserSimpleDto> dtos = adminUserService.getSalesmanByStoreCheckerWithAvailable(getUserLogin(),
                storeCheckerId);
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

}
