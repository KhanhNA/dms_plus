package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.VisitInfoDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.AdminVisitService;

@RestController(value="adminVisitController")
@RequestMapping(value = "/admin/visit")
public class VisitController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private AdminVisitService adminVisitService;

    /** Lấy chi tiết một chuyến ghé thăm theo ID */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDistributor(@PathVariable String id) {
        VisitInfoDto dto = adminVisitService.getVisit(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
