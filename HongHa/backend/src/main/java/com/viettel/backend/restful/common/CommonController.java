package com.viettel.backend.restful.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ChangePasswordDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.AuthenticationService;

@RestController
@RequestMapping(value = "/common/system")
public class CommonController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private AuthenticationService authenticationService;
    
    @RequestMapping(value = "/changepassword", method = RequestMethod.PUT)
    public ResponseEntity<?> approve(@RequestBody ChangePasswordDto dto) {
        authenticationService.changePassword(getUserLogin(), dto);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

}
