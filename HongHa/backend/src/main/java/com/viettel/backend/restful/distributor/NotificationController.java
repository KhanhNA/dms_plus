package com.viettel.backend.restful.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.DistributorNotificationDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.DistributorNotificationService;

@RestController(value="distributorNotificationController")
@RequestMapping(value = "/distributor/notification")
public class NotificationController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private DistributorNotificationService distributorNotificationService;

    /** Lây các thông tin notification của GSBH */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getNotification() {
        DistributorNotificationDto dto = distributorNotificationService.getNotification(getUserLogin());
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
