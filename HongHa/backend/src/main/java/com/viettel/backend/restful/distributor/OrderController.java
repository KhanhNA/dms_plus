package com.viettel.backend.restful.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.CommonReportService;
import com.viettel.backend.service.DistributorOrderService;

@RestController(value="distributorOrderController")
@RequestMapping(value = "/distributor/order")
public class OrderController extends AbstractController {

    private static final long serialVersionUID = -5643500619636563651L;

    @Autowired
    private DistributorOrderService distributorOrderService;
    
    @Autowired
    private CommonReportService commonReportService;
    
    /**
     * Lấy danh sách đơn hàng đã được duyệt - có phân trang của NVGS hiện tại
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getOrders(
            @RequestParam(required = true) String fromDate,
            @RequestParam(required = true) String toDate,
            @RequestParam(required = false) Integer page, 
            @RequestParam(required = false) Integer size) {
        ListJson<OrderSimpleDto> results = commonReportService.getOrders(
                getUserLogin(), getUserLogin().getUserId().toString(), fromDate,
                toDate, getPageRequest(page, size));
        
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }
    
    /**
     * Lấy chi tiết đơn hàng đã được duyệt theo ID
     */
    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET)
    public ResponseEntity<?> getOrderDetail(@PathVariable String orderId) {
        
        OrderDto result = commonReportService.getOrderById(getUserLogin(), orderId);
        return new Envelope(result).toResponseEntity(HttpStatus.OK);
    }
    
    /**
     * Lấy danh sách đơn hàng đang chờ duyệt - có phân trang của NVGS hiện tại
     */
    @RequestMapping(value = "/pending", method = RequestMethod.GET)
    public ResponseEntity<?> getPendingOrders(
            @RequestParam(required = false) Integer page, 
            @RequestParam(required = false) Integer size) {
        
        ListJson<OrderSimpleDto> results = distributorOrderService.getPendingOrders(
                getUserLogin(), getPageRequest(page, size));
        
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }
    
    /**
     * Lấy thông tin chi tiết một đơn hàng đang chờ duyệt của NVGS hiện tại theo ID
     */
    @RequestMapping(value = "/pending/{orderId}", method = RequestMethod.GET)
    public ResponseEntity<?> getPendingOrderDetail(@PathVariable String orderId) {
        
        OrderDto result = distributorOrderService.getPendingOrderById(getUserLogin(), orderId);
        return new Envelope(result).toResponseEntity(HttpStatus.OK);
    }
    
    /**
     * Duyệt một đơn hàng
     */
    @RequestMapping(value = "/pending/{orderId}/approve", method = RequestMethod.PUT)
    public ResponseEntity<?> approve(@PathVariable String orderId) {
        
        distributorOrderService.approvePendingOrder(getUserLogin(), orderId);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }
    
    /**
     * Từ chối duyệt một đơn hàng
     */
    @RequestMapping(value = "/pending/{orderId}/reject", method = RequestMethod.PUT)
    public ResponseEntity<?> reject(@PathVariable String orderId) {
        
        distributorOrderService.rejectPendingOrder(getUserLogin(), orderId);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

}
