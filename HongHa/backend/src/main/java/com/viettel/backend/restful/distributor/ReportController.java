package com.viettel.backend.restful.distributor;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.service.DistributorCustomerService;

@RestController(value = "distributorReportController")
@RequestMapping(value = "/distributor/report")
public class ReportController extends AbstractController {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private DistributorCustomerService distributorCustomerService;

    @RequestMapping(value = "/customer/check/export", method = RequestMethod.GET)
    public ResponseEntity<?> exportExhibitionReport(
            @RequestParam(required = false) String salesmanId,
            @RequestParam String fromDate,
            @RequestParam String toDate
            ) throws IOException {
        String fileName = "DISPLAY_CHECK_" + fromDate + "_" + toDate + ".xlsx";
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(
                distributorCustomerService.exportCustomerCheckReport(
                        getUserLogin(), fromDate, toDate, salesmanId), 
                header, HttpStatus.OK);
        return result;
    }

}
