package com.viettel.backend.restful.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.DistributorUserService;

@RestController(value = "distributorSalesmanController")
@RequestMapping(value = "/distributor/salesman")
public class SalesmanController extends AbstractController {

    private static final long serialVersionUID = -2083390468606881411L;

    @Autowired
    private DistributorUserService distributorUserService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanList() {

        ListJson<UserSimpleDto> results = distributorUserService.getSalesmen(getUserLogin());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
