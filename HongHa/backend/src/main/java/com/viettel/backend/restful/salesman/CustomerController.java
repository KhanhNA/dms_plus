package com.viettel.backend.restful.salesman;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerForVisitDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.CustomerSummaryDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.SalesmanCustomerService;

@RestController(value="salesmanCustomerController")
@RequestMapping(value = "/salesman/customer")
public class CustomerController extends AbstractController {

    private static final long serialVersionUID = -2083390468606881411L;

    @Autowired
    private SalesmanCustomerService salesmanCustomerService;

    // CUSTOMER SUMMARY
    @RequestMapping(value = "/{id}/summary", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomerSummary(@PathVariable String id) {
        CustomerSummaryDto dto = salesmanCustomerService.getCustomerSummary(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    // LAY DS TUYEN HOM NAY CUA NHAN VIEN BAN HANG
    @RequestMapping(value = "/forvisit/today", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomersForVisitToday() {
        ListJson<CustomerForVisitDto> results = salesmanCustomerService.getCustomersForVisitToday(getUserLogin());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    // LAY DS KHACH HANG CUA NHAN VIEN BAN HANG
    @RequestMapping(value = "/forvisit/all", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomersForVisitAll() {
        ListJson<CustomerForVisitDto> results = salesmanCustomerService.getCustomersForVisitAll(getUserLogin());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    // REGISTER
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody @Valid CustomerCreateDto dto) {
        String domainId = salesmanCustomerService.register(getUserLogin(), dto);
        return new Envelope(domainId).toResponseEntity(HttpStatus.CREATED);
    }

    // LIST CUSTOMER REGISTER BY SALES MAN
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ResponseEntity<?> listCustomerRegisteredBySalesMan(@RequestParam(required = false) String q,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<CustomerSimpleDto> results = salesmanCustomerService.getCustomersRegistered(getUserLogin(), q,
                getPageRequest(page, size));
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    // UPDATE PHONE
    @RequestMapping(value = "/{id}/phone", method = RequestMethod.PUT)
    public ResponseEntity<?> updatePhone(@PathVariable String id, @RequestBody @Valid String phone) {
        salesmanCustomerService.updatePhone(getUserLogin(), id, phone);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    // UPDATE MOBILE
    @RequestMapping(value = "/{id}/mobile", method = RequestMethod.PUT)
    public ResponseEntity<?> updateMobile(@PathVariable String id, @RequestBody @Valid String mobile) {
        salesmanCustomerService.updateMobile(getUserLogin(), id, mobile);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    // UPDATE LOCATION
    @RequestMapping(value = "/{id}/location", method = RequestMethod.PUT)
    public ResponseEntity<?> updateLocation(@PathVariable String id, @RequestBody @Valid LocationDto dto) {
        salesmanCustomerService.updateLocation(getUserLogin(), id, dto);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

}
