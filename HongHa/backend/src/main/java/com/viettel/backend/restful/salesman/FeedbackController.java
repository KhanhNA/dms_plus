package com.viettel.backend.restful.salesman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.FeedbackDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SalesmanFeedbackService;

@RestController(value="salesmanFeedbackController")
@RequestMapping(value = "/salesman/feedback")
public class FeedbackController extends AbstractController {
	
    private static final long serialVersionUID = -4558727885745419731L;
    
    @Autowired
    private SalesmanFeedbackService salesmanFeedbackService;
	
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getLastCustomerFeedbacks(
            @RequestParam String customerId) {
        ListJson<FeedbackDto> feedbacks = salesmanFeedbackService.getLastCustomerFeedbacks(getUserLogin(), customerId);
        return new Envelope(feedbacks).toResponseEntity(HttpStatus.OK);
    }
    
}
