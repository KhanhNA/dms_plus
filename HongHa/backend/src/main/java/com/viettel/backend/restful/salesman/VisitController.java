package com.viettel.backend.restful.salesman;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.LocationDto.LocationDtoWrapper;
import com.viettel.backend.dto.VisitClosingDto;
import com.viettel.backend.dto.VisitEndDto;
import com.viettel.backend.dto.VisitInfoDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SalesmanVisitService;

@RestController(value="salesmanVisitController")
@RequestMapping(value = "salesman/visit")
public class VisitController extends AbstractController {

    private static final long serialVersionUID = -2707535959788556815L;

    @Autowired
    private SalesmanVisitService salesmanVisitService;

    // START VISIT - GHE THAM
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public ResponseEntity<?> startVisit(@RequestParam(value = "customerId", required = true) String customerId,
            @RequestBody LocationDtoWrapper dto) {
        String visitId = salesmanVisitService.startVisit(getUserLogin(), customerId, dto.getLocation());
        return new Envelope(visitId).toResponseEntity(HttpStatus.OK);
    }

    // KET THUC GHE THAM
    @RequestMapping(value = "/end/{id}", method = RequestMethod.POST)
    public ResponseEntity<?> endVisit(@PathVariable String id, @RequestBody @Valid VisitEndDto dto) {
        VisitInfoDto visitInfoDto = salesmanVisitService.endVisit(getUserLogin(), id, dto);
        return new Envelope(visitInfoDto).toResponseEntity(HttpStatus.OK);
    }

    // DONG CUA
    @RequestMapping(value = "/close", method = RequestMethod.POST)
    public ResponseEntity<?> markAsClosed(@RequestParam(value = "customerId", required = true) String customerId,
            @RequestBody @Valid VisitClosingDto dto) {
        String visitId = salesmanVisitService.markAsClosed(getUserLogin(), customerId, dto);
        return new Envelope(visitId).toResponseEntity(HttpStatus.OK);
    }

    // GET VISIT INFO
    @RequestMapping(value = "/today", method = RequestMethod.GET)
    public ResponseEntity<?> getVisitedTodayInfo(@RequestParam(value = "customerId", required = true) String customerId) {
        VisitInfoDto visitInfoDto = salesmanVisitService.getVisitedTodayInfo(getUserLogin(), customerId);
        return new Envelope(visitInfoDto).toResponseEntity(HttpStatus.OK);
    }

}
