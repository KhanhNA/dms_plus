package com.viettel.backend.restful.storechecker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.CheckCustomerDto;
import com.viettel.backend.dto.CustomerForCheckDto;
import com.viettel.backend.dto.CustomerSummaryDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.StoreCheckerCustomerService;

@RestController(value = "storecheckerCustomerController")
@RequestMapping(value = "/storechecker/customer")
public class CustomerController extends AbstractController {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    private StoreCheckerCustomerService storeCheckerCustomerService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSalesmanDashboard() {
        ListJson<CustomerForCheckDto> dtos = storeCheckerCustomerService.getCustomersForCheckToday(getUserLogin());
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/{id}/check", method = RequestMethod.POST)
    public ResponseEntity<?> checkForCustomer(
            @PathVariable String id,
            @RequestBody CheckCustomerDto dto) {
        storeCheckerCustomerService.checkForCustomer(getUserLogin(), id, dto);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }
    
    // CUSTOMER SUMMARY
    @RequestMapping(value = "/{id}/summary", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomerSummary(@PathVariable String id) {
        CustomerSummaryDto dto = storeCheckerCustomerService.getCustomerSummary(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
