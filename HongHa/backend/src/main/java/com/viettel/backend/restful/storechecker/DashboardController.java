package com.viettel.backend.restful.storechecker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.CustomerSaleResultDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.MobileDashboardDto;
import com.viettel.backend.dto.OrderSummaryDto;
import com.viettel.backend.dto.SalesResultDaily;
import com.viettel.backend.oauth2.core.SecurityContextHelper;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.MobileDashboardService;

@RestController(value = "storecheckerDashboardController")
@RequestMapping(value = "/storechecker/dashboard")
public class DashboardController {

    @Autowired
    private MobileDashboardService mobileDashboardService;

    // GET  DASHBOARD
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getDashboard() {
        MobileDashboardDto dto = mobileDashboardService.getDashboard(SecurityContextHelper.getCurrentUser());
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/bycustomer", method = RequestMethod.GET)
    public ResponseEntity<?> getDashboardByCustomer() {
        ListJson<CustomerSaleResultDto> results = mobileDashboardService.getDashboardByCustomer(SecurityContextHelper
                .getCurrentUser());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/bycustomer/detail", method = RequestMethod.GET)
    public ResponseEntity<?> getDashboardByCustomerDetail(
            @RequestParam(value = "customerId", required = true) String customerId) {
        ListJson<OrderSummaryDto> results = mobileDashboardService.getDashboardByCustomerDetail(
                SecurityContextHelper.getCurrentUser(), customerId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/byday", method = RequestMethod.GET)
    public ResponseEntity<?> getDashboardByDay() {
        ListJson<SalesResultDaily> results = mobileDashboardService.getDashboardByDay(SecurityContextHelper
                .getCurrentUser());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/byday/detail", method = RequestMethod.GET)
    public ResponseEntity<?> getDashboardByDayDetail(@RequestParam(value = "date", required = true) String date) {
        ListJson<CustomerSaleResultDto> results = mobileDashboardService.getDashboardByDayDetail(
                SecurityContextHelper.getCurrentUser(), date);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
