package com.viettel.backend.restful.storechecker;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ExhibitionRatingDto.ExhibitionRatingItemDto;
import com.viettel.backend.dto.ExhibitionWithRatingDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.StoreCheckerExhibitionService;

/**
 * @author thanh
 */
@RestController(value="storecheckerExhibitionController")
@RequestMapping(value = "/storechecker/exhibition")
public class ExhibitionController extends AbstractController {

    private static final long serialVersionUID = 8047048300443030912L;

    @Autowired
    private StoreCheckerExhibitionService storeCheckerExhibitionService;

    // LIST AVAILABLE FOR CUSTOMER
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSurveyForCustomer(@RequestParam String customerId) {
        ListJson<ExhibitionWithRatingDto> results = storeCheckerExhibitionService
                .getExhibitionsByCustomer(getUserLogin(), customerId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/{exhibitionId}", method = RequestMethod.POST)
    public ResponseEntity<?> submitExhibitionForCustomer(
            @PathVariable String exhibitionId,
            @RequestParam String customerId,
            @RequestBody List<ExhibitionRatingItemDto> exhibitionRatingItemDtos) {
        storeCheckerExhibitionService.createExhibitionRatingByCustomer(
                getUserLogin(), exhibitionId, customerId, exhibitionRatingItemDtos);
        return new Envelope(Meta.OK).toResponseEntity();
    }

}
