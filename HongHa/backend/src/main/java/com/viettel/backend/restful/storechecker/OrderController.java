package com.viettel.backend.restful.storechecker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.StoreCheckerOrderService;

@RestController(value="storecheckerOrderController")
@RequestMapping(value = "/storechecker/order")
public class OrderController extends AbstractController {

    private static final long serialVersionUID = -5643500619636563651L;

    @Autowired
    private StoreCheckerOrderService storeCheckerOrderService;

    // ORDER BY CURRENT SALESMAN
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> viewOrderDetail(@PathVariable String id) {
        OrderDto result = storeCheckerOrderService.getOrderById(getUserLogin(), id);
        return new Envelope(result).toResponseEntity(HttpStatus.OK);
    }

}
