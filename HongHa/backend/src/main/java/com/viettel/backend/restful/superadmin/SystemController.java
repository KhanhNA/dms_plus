package com.viettel.backend.restful.superadmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.ApplicationInitService;

@RestController
@RequestMapping(value = "/superadmin/system")
public class SystemController extends AbstractController {

	private static final long serialVersionUID = 2394833065851281843L;
	
	@Autowired
	private ApplicationInitService service;
	
    @RequestMapping(value = "/cache/init", method = RequestMethod.PUT)
    public ResponseEntity<?> resetCache() {
        service.initCache();
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK); 
    }

}
