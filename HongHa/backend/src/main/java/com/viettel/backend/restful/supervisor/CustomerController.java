package com.viettel.backend.restful.supervisor;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerDto;
import com.viettel.backend.dto.CustomerForTrackingDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.IdDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.SupervisorCustomerService;

@RestController(value="supervisorCustomerController")
@RequestMapping(value = "/supervisor/customer")
public class CustomerController extends AbstractController {

    private static final long serialVersionUID = -2083390468606881411L;

    @Autowired
    private SupervisorCustomerService supervisorCustomerService;

    /** Lấy danh sách khách hàng đã được duyệt - có phân trang của NVGS hiện tại */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomers(@RequestParam(required = false) String salesmanId,
            @RequestParam(required = false) String distributorId,
            @RequestParam(value = "q", required = false, defaultValue = "") String search,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<CustomerSimpleDto> results = supervisorCustomerService.getCustomers(getUserLogin(), search,
                distributorId, salesmanId, getPageRequest(page, size));
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    /**
     * Lấy danh sách khách hàng đã được duyệt - không phân trang của NVGS hiện
     * tại
     */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomers(@RequestParam(required = false) String salesmanId,
            @RequestParam(required = false) String distributorId,
            @RequestParam(value = "q", required = false, defaultValue = "") String search) {
        ListJson<CustomerSimpleDto> results = supervisorCustomerService.getCustomers(getUserLogin(), search,
                distributorId, salesmanId, null);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    /** Lấy thông tin chi tiết về một khách hàng của NVGS hiện tại theo ID */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomer(@PathVariable String id) {
        CustomerDto dto = supervisorCustomerService.getCustomerById(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    /**
     * Lấy danh sách khách hàng đã được duyệt - không phân trang của NVGS hiện
     * tại
     */
    @RequestMapping(value = "/pending", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomersPending(@RequestParam(required = false) String salesmanId,
            @RequestParam(value = "q", required = false, defaultValue = "") String search,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<CustomerSimpleDto> results = supervisorCustomerService.getCustomersPending(getUserLogin(), search,
                salesmanId, getPageRequest(page, size));
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

    /**
     * Lấy thông tin chi tiết về một khách hàng cần duyệt của NVGS hiện tại theo
     * ID
     */
    @RequestMapping(value = "/pending/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomerPending(@PathVariable String id) {
        CustomerDto dto = supervisorCustomerService.getCustomerPendingById(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    /** Duyệt một khách hàng */
    @RequestMapping(value = "/pending/{id}/approve", method = RequestMethod.PUT)
    public ResponseEntity<?> approveCustomerPending(@PathVariable String id) {
        supervisorCustomerService.approveCustomer(getUserLogin(), id);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    /** Từ chối một khách hàng */
    @RequestMapping(value = "/pending/{id}/reject", method = RequestMethod.PUT)
    public ResponseEntity<?> rejectCustomerPending(@PathVariable String id) {
        supervisorCustomerService.rejectCustomer(getUserLogin(), id);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    /**
     * Lấy danh sách khách hàng nằm trong tuyến ngày hôm nay của một NVBH thuộc
     * NVGS hiện tại - có kèm theo trạng thái ghé thăm
     */
    @RequestMapping(value = "/today", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomersTodayBySalesman(@RequestParam(required = true) String salesmanId) {
        ListJson<CustomerForTrackingDto> results = supervisorCustomerService.getCustomersTodayBySalesman(getUserLogin(),
                salesmanId);
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> createCustomer(@RequestBody @Valid CustomerCreateDto dto) {
        String id = supervisorCustomerService.createCustomer(getUserLogin(), dto);
        return new Envelope(new IdDto(id)).toResponseEntity(HttpStatus.CREATED);
    }

}
