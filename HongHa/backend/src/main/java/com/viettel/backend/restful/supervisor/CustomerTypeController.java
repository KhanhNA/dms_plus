package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.CommonCategoryService;

@RestController(value="supervisorCustomerTypeController")
@RequestMapping(value = "/supervisor/customertype")
public class CustomerTypeController extends AbstractController {

    private static final long serialVersionUID = 2800307633195967878L;

    @Autowired
    private CommonCategoryService commonCategoryService;

    // LIST ALL
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> all() {
        ListJson<NameCategoryDto> dtos = commonCategoryService.getCustomerTypes(getUserLogin());
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

}
