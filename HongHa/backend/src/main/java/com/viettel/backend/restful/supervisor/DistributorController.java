package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.DistributorSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SupervisorDistributorService;

@RestController(value="supervisorDistributorController")
@RequestMapping(value = "/supervisor/distributor")
public class DistributorController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private SupervisorDistributorService supervisorDistributorService;

    /** Lấy danh sách NPP của NVGS hiện tại - không phân trang */
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> getDistributor() {
        ListJson<DistributorSimpleDto> results = supervisorDistributorService.getDistbutors(getUserLogin());
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
