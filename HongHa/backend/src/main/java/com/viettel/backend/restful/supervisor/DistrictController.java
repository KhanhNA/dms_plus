package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.domain.District;
import com.viettel.backend.dto.NameCategoryByDistributorCreateDto;
import com.viettel.backend.dto.NameCategoryByDistributorDto;
import com.viettel.backend.restful.BasicNameCategoryByDistributorController;
import com.viettel.backend.service.BasicNameCategoryByDistributorService;
import com.viettel.backend.service.DistrictService;

@RestController(value = "supervisorDistrictController")
@RequestMapping(value = "/supervisor/district")
public class DistrictController
        extends
        BasicNameCategoryByDistributorController<District, NameCategoryByDistributorDto, NameCategoryByDistributorDto, NameCategoryByDistributorCreateDto> {

    private static final long serialVersionUID = -2085177250191517175L;

    @Autowired
    private DistrictService districtService;

    @Override
    protected BasicNameCategoryByDistributorService<District, NameCategoryByDistributorDto, NameCategoryByDistributorDto, NameCategoryByDistributorCreateDto> getService() {
        return districtService;
    }
}
