package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SurveySimpleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.SupervisorSurveyService;

@RestController(value="supervisorSurveyController")
@RequestMapping(value = "/supervisor/survey")
public class SurveyController extends AbstractController {

    private static final long serialVersionUID = -3492046947585355471L;

    @Autowired
    private SupervisorSurveyService supervisorSurveyService;

    /** Lấy danh sách các đợt khảo sát */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSurveys(@RequestParam(value = "q", required = false) String search,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListJson<SurveySimpleDto> results = supervisorSurveyService.getSurveys(getUserLogin(), search,
                getPageRequest(page, size));
        return new Envelope(results).toResponseEntity(HttpStatus.OK);
    }

}
