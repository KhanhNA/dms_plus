package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.CalendarConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AdminCalendarConfigService extends Serializable {

    public CalendarConfigDto getCalendarConfig(UserLogin userLogin);
    
    public void saveCalendarConfig(UserLogin userLogin, CalendarConfigDto dto);
    
}
