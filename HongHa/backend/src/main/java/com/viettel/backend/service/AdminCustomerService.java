package com.viettel.backend.service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerDto;
import com.viettel.backend.dto.CustomerForTrackingDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AdminCustomerService extends
        BasicCategoryService<Customer, CustomerSimpleDto, CustomerDto, CustomerCreateDto> {

    public ListJson<CustomerForTrackingDto> getCustomersTodayBySalesman(UserLogin userLogin, String salesmanId);
    
}
