package com.viettel.backend.service;

import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.dto.NameCategoryDto;

public interface AdminCustomerTypeService extends
        BasicCategoryService<CustomerType, NameCategoryDto, NameCategoryDto, NameCategoryDto> {

}
