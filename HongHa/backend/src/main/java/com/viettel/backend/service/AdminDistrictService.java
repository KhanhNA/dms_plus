package com.viettel.backend.service;

import com.viettel.backend.domain.District;
import com.viettel.backend.dto.NameCategoryDto;

public interface AdminDistrictService extends
        BasicCategoryService<District, NameCategoryDto, NameCategoryDto, NameCategoryDto> {

}
