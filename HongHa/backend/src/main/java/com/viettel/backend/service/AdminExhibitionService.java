package com.viettel.backend.service;

import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.dto.ExhibitionDto;
import com.viettel.backend.dto.ExhibitionSimpleDto;

public interface AdminExhibitionService extends
        BasicCategoryService<Exhibition, ExhibitionSimpleDto, ExhibitionDto, ExhibitionDto> {

}
