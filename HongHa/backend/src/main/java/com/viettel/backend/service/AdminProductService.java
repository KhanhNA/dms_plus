package com.viettel.backend.service;

import com.viettel.backend.domain.Product;
import com.viettel.backend.dto.ProductCreateDto;
import com.viettel.backend.dto.ProductDto;

public interface AdminProductService extends BasicCategoryService<Product, ProductDto, ProductDto, ProductCreateDto> {

}
