package com.viettel.backend.service;

import com.viettel.backend.domain.Promotion;
import com.viettel.backend.dto.PromotionCreateDto;
import com.viettel.backend.dto.PromotionDto;
import com.viettel.backend.dto.PromotionSimpleDto;

public interface AdminPromotionService extends
        BasicCategoryService<Promotion, PromotionSimpleDto, PromotionDto, PromotionCreateDto> {

}
