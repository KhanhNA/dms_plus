package com.viettel.backend.service;

import com.viettel.backend.domain.Survey;
import com.viettel.backend.dto.SurveyDto;
import com.viettel.backend.dto.SurveySimpleDto;

public interface AdminSurveyService extends BasicCategoryService<Survey, SurveySimpleDto, SurveyDto, SurveyDto> {

}
