package com.viettel.backend.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.cache.redis.CacheManager;
import com.viettel.backend.domain.Client;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.VisitAndOrder;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.repository.ClientRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitAndOrderRepository;
import com.viettel.backend.service.sub.CacheSubService;
import com.viettel.backend.util.DateTimeUtils;

@Service
public class ApplicationInitService implements Serializable {

    private static final long serialVersionUID = 6552753342101938808L;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private VisitAndOrderRepository visitAndOrderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private CacheSubService cacheSubService;

    public void initCache() {
        cacheManager.clearAll();

        List<Client> clients = clientRepository.getAll(PO.CLIENT_ROOT_ID, null);

        MultiKeyMap<String, Double> distributorDataMap = new MultiKeyMap<String, Double>();

        MultiKeyMap<Object, Double> mapDistributorSalesmanRevenue = new MultiKeyMap<>();

        MultiKeyMap<Object, Integer> mapDistributorQuantity = new MultiKeyMap<>();
        MultiKeyMap<Object, Integer> mapAdminProductQuantity = new MultiKeyMap<>();

        MultiKeyMap<Object, Integer> mapSalesmanVisited = new MultiKeyMap<>();

        for (Client client : clients) {
            Period period = new Period(DateTimeUtils.getFirstOfLastMonth(), DateTimeUtils.getTomorrow());
            SimpleDate firstOfThisMonth = DateTimeUtils.getFirstOfThisMonth();

            List<VisitAndOrder> visitAndOrders = visitAndOrderRepository.getDataByCreatedTime(client.getId(),
                    period.getCopy());

            for (VisitAndOrder visitAndOrder : visitAndOrders) {
                // Visit
                if (visitAndOrder.isVisit() && visitAndOrder.getVisitStatus() == Visit.VISIT_STATUS_VISITED
                        && visitAndOrder.getStartTime() != null
                        && visitAndOrder.getStartTime().compareTo(period.getFromDate()) >= 0
                        && visitAndOrder.getEndTime() != null
                        && visitAndOrder.getEndTime().compareTo(period.getToDate()) < 0) {
                    String key_distributor_monthly = visitAndOrder.getDistributor().getId().toString() + "-"
                            + visitAndOrder.getStartTime().getIsoMonth();
                    String key_distributor_daily = visitAndOrder.getDistributor().getId().toString() + "-"
                            + visitAndOrder.getStartTime().getIsoDate();

                    incrementMapValue(distributorDataMap, key_distributor_monthly,
                            CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT, 1);
                    incrementMapValue(distributorDataMap, key_distributor_daily,
                            CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT, 1);

                    if (visitAndOrder.isErrorDuration() && !visitAndOrder.isClosed()) {
                        incrementMapValue(distributorDataMap, key_distributor_daily,
                                CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_ERROR_DURATION, 1);
                    }

                    if (visitAndOrder.getLocationStatus() != Visit.LOCATION_STATUS_LOCATED) {
                        incrementMapValue(distributorDataMap, key_distributor_daily,
                                CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_ERROR_POSITION, 1);
                    }

                    if (visitAndOrder.isOrder()) {
                        incrementMapValue(distributorDataMap, key_distributor_daily,
                                CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_WITH_PO, 1);
                    }

                    if (visitAndOrder.getEndTime().compareTo(firstOfThisMonth) >= 0) {
                        incrementMapValue(mapSalesmanVisited, key_distributor_monthly, visitAndOrder.getSalesman()
                                .getId(), 1);
                        incrementMapValue(mapSalesmanVisited, key_distributor_daily, visitAndOrder.getSalesman()
                                .getId(), 1);
                    }
                }

                // Order
                if (visitAndOrder.isOrder() && visitAndOrder.getApproveStatus() == Order.APPROVE_STATUS_APPROVED
                        && visitAndOrder.getApproveTime() != null
                        && visitAndOrder.getApproveTime().compareTo(period.getFromDate()) >= 0
                        && visitAndOrder.getApproveTime().compareTo(period.getToDate()) < 0) {
                    String key_distributor_monthly = visitAndOrder.getDistributor().getId().toString() + "-"
                            + visitAndOrder.getStartTime().getIsoMonth();
                    String key_distributor_daily = visitAndOrder.getDistributor().getId().toString() + "-"
                            + visitAndOrder.getStartTime().getIsoDate();

                    incrementMapValue(distributorDataMap, key_distributor_monthly,
                            CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER, 1);
                    incrementMapValue(distributorDataMap, key_distributor_daily,
                            CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER, 1);

                    incrementMapValue(distributorDataMap, key_distributor_monthly,
                            CacheManager.DISTRIBUTOR_DATA_TYPE_REVENUE, visitAndOrder.getGrandTotal().doubleValue());
                    incrementMapValue(distributorDataMap, key_distributor_daily,
                            CacheManager.DISTRIBUTOR_DATA_TYPE_REVENUE, visitAndOrder.getGrandTotal().doubleValue());

                    incrementMapValue(mapDistributorSalesmanRevenue, key_distributor_monthly, visitAndOrder
                            .getSalesman().getId(), visitAndOrder.getGrandTotal().doubleValue());
                    incrementMapValue(mapDistributorSalesmanRevenue, key_distributor_daily, visitAndOrder.getSalesman()
                            .getId(), visitAndOrder.getGrandTotal().doubleValue());

                    for (OrderDetail detail : visitAndOrder.getDetails()) {

                        incrementMapValue(distributorDataMap, key_distributor_monthly,
                                CacheManager.DISTRIBUTOR_DATA_TYPE_QUANTITY, detail.getQuantity().doubleValue());
                        incrementMapValue(distributorDataMap, key_distributor_daily,
                                CacheManager.DISTRIBUTOR_DATA_TYPE_QUANTITY, detail.getQuantity().doubleValue());

                        // Init product sales report cache
                        if (visitAndOrder.getApproveTime().compareTo(firstOfThisMonth) >= 0) {
                            String key_monthly = visitAndOrder.getEndTime().getIsoMonth();
                            String key_daily = visitAndOrder.getEndTime().getIsoDate();

                            incrementMapValue(mapDistributorQuantity, key_monthly, detail.getProduct().getId(), detail
                                    .getQuantity().intValue());
                            incrementMapValue(mapDistributorQuantity, key_daily, detail.getProduct().getId(), detail
                                    .getQuantity().intValue());

                            incrementMapValue(mapAdminProductQuantity, key_monthly, detail.getProduct().getId(), detail
                                    .getQuantity().intValue());
                            incrementMapValue(mapAdminProductQuantity, key_daily, detail.getProduct().getId(), detail
                                    .getQuantity().intValue());
                        }
                    }

                    if (!visitAndOrder.isVisit()) {
                        incrementMapValue(distributorDataMap, key_distributor_daily,
                                CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER_WITHOUT_VISIT, 1);
                    }
                }
            }

            for (Entry<MultiKey<? extends String>, Double> entry : distributorDataMap.entrySet()) {
                cacheManager.getDistributorDataCache().get(entry.getKey().getKey(0))
                        .put(entry.getKey().getKey(1), entry.getValue());

                cacheManager.getDistributorDataCache().get(entry.getKey().getKey(0)).expire(62, TimeUnit.DAYS);
            }

            for (Entry<MultiKey<? extends Object>, Double> entry : mapDistributorSalesmanRevenue.entrySet()) {
                cacheManager.getDistributorSalesmanRevenueCache().get((String) entry.getKey().getKey(0))
                        .put((ObjectId) entry.getKey().getKey(1), entry.getValue());

                cacheManager.getDistributorSalesmanRevenueCache().get((String) entry.getKey().getKey(0))
                        .expire(62, TimeUnit.DAYS);
            }

            for (Entry<MultiKey<? extends Object>, Integer> entry : mapDistributorQuantity.entrySet()) {
                cacheManager.getDistributorProductQuantityCache().get((String) entry.getKey().getKey(0))
                        .put((ObjectId) entry.getKey().getKey(1), entry.getValue());

                cacheManager.getDistributorProductQuantityCache().get((String) entry.getKey().getKey(0))
                        .expire(62, TimeUnit.DAYS);
            }

            for (Entry<MultiKey<? extends Object>, Integer> entry : mapAdminProductQuantity.entrySet()) {
                cacheManager.getAdminProductQuantityCache().get((String) entry.getKey().getKey(0))
                        .put((ObjectId) entry.getKey().getKey(1), entry.getValue());

                cacheManager.getAdminProductQuantityCache().get((String) entry.getKey().getKey(0))
                        .expire(62, TimeUnit.DAYS);
            }

            for (Entry<MultiKey<? extends Object>, Integer> entry : mapSalesmanVisited.entrySet()) {
                cacheManager.getDistributorSalesmanVisitedCache().get((String) entry.getKey().getKey(0))
                        .put((ObjectId) entry.getKey().getKey(1), entry.getValue());

                cacheManager.getDistributorSalesmanVisitedCache().get((String) entry.getKey().getKey(0))
                        .expire(62, TimeUnit.DAYS);
            }

            // VISIT SCHEDULE
            List<Customer> customers = customerRepository.getAll(client.getId(), null);
            cacheSubService.initScheduleNumberPlannedCache(customers);
        }
    }

    private void incrementMapValue(MultiKeyMap<String, Double> map, String key1, String key2, double value) {
        Double temp = map.get(key1, key2);
        temp = temp == null ? 0 : temp;
        map.put(key1, key2, temp + value);
    }

    private void incrementMapValue(MultiKeyMap<Object, Integer> map, String key1, ObjectId key2, int value) {
        Integer temp = map.get(key1, key2);
        temp = temp == null ? 0 : temp;
        map.put(key1, key2, temp + value);
    }

    private void incrementMapValue(MultiKeyMap<Object, Double> map, String key1, ObjectId key2, double value) {
        Double temp = map.get(key1, key2);
        temp = temp == null ? 0 : temp;
        map.put(key1, key2, temp + value);
    }

}