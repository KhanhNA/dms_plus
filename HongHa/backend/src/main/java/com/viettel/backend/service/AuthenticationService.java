package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ChangePasswordDto;
import com.viettel.backend.dto.UserLoginDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AuthenticationService extends Serializable {

    public User getUserByUsername(String username);

    public UserLoginDto getUserLoginDto(UserLogin userLogin);

    public void changePassword(UserLogin userLogin, ChangePasswordDto changePasswordDto);

}
