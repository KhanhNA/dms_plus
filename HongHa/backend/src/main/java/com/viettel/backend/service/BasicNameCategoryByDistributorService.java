package com.viettel.backend.service;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.domain.NameCategoryByDistributor;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface BasicNameCategoryByDistributorService<DOMAIN extends NameCategoryByDistributor, SIMPLEDTO extends NameCategoryDto, DETAILDTO extends SIMPLEDTO, CREATEDTO extends NameCategoryDto>
        extends BasicCategoryService<DOMAIN, SIMPLEDTO, DETAILDTO, CREATEDTO> {

    /**
     * @deprecated use getList(UserLogin userLogin, String distributorId, String
     *             search, Boolean active, Boolean draft, Pageable pageable)
     */
    @Deprecated
    public ListJson<SIMPLEDTO> getList(UserLogin userLogin, String search, Boolean active, Boolean draft,
            Pageable pageable);

    /** Get list PO */
    public ListJson<SIMPLEDTO> getList(UserLogin userLogin, String distributorId, String search, Boolean active,
            Boolean draft, Pageable pageable);

}