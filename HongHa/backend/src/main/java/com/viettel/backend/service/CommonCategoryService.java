package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.dto.ProductDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface CommonCategoryService extends Serializable {
    
    public ListJson<ProductDto> getProducts(UserLogin userLogin);

    public ListJson<NameCategoryDto> getProductCategories(UserLogin userLogin);
    
    public ListJson<NameCategoryDto> getCustomerTypes(UserLogin userLogin);
    
    public ListJson<NameCategoryDto> getDistricts(UserLogin userLogin);
    
}
