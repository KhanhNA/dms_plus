package com.viettel.backend.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.CustomerScheduleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface CustomerScheduleService extends Serializable {

    public ListJson<CustomerScheduleDto> getCustomerSchedules(UserLogin userLogin, String customerSearch,
            String distributorId, String salesmanId, boolean all, Integer dayOfWeek, Pageable pageable);

    public void saveCustomerScheduleByDistributor(UserLogin userLogin, String _distributorId, List<CustomerScheduleDto> dtos);

    public void saveCustomerScheduleByCustomer(UserLogin userLogin, String _customerId, CustomerScheduleDto dto);
    
}
