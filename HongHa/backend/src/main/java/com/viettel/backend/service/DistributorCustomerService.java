package com.viettel.backend.service;

import com.viettel.backend.oauth2.core.UserLogin;

public interface DistributorCustomerService {

    public byte[] exportCustomerCheckReport(UserLogin userLogin, String _fromDate, 
            String _toDate, String _salesmanId);

}
