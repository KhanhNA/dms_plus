package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.DistributorNotificationDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface DistributorNotificationService extends Serializable {

    public DistributorNotificationDto getNotification(UserLogin userLogin);

}
