package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface DistributorUserService extends Serializable {

    public ListJson<UserSimpleDto> getSalesmen(UserLogin userLogin);

}
