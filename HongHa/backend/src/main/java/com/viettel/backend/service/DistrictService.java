package com.viettel.backend.service;

import com.viettel.backend.domain.District;
import com.viettel.backend.dto.NameCategoryByDistributorCreateDto;
import com.viettel.backend.dto.NameCategoryByDistributorDto;

public interface DistrictService
        extends
        BasicNameCategoryByDistributorService<District, NameCategoryByDistributorDto, NameCategoryByDistributorDto, NameCategoryByDistributorCreateDto> {

}
