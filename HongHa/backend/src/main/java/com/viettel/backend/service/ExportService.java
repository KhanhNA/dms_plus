package com.viettel.backend.service;

import java.io.InputStream;

import com.viettel.backend.oauth2.core.UserLogin;

public interface ExportService {

    public byte[] exportVisitsByDay(UserLogin userLogin, String isoDate);
    
    public InputStream exportOrderSummary(UserLogin userLogin, String distributorId, String fromDate, String toDate);
    
    public InputStream exportOrderDetail(UserLogin userLogin, String distributorId, String fromDate, String toDate);
    
}
