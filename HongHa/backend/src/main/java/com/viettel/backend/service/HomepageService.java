package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.HomepageDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface HomepageService extends Serializable {

    public HomepageDto getHomepage(UserLogin userLogin);
    
}
