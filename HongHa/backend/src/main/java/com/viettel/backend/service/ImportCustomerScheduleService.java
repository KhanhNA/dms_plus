package com.viettel.backend.service;

import com.viettel.backend.dto.ImportConfirmDto;
import com.viettel.backend.dto.ImportResultDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface ImportCustomerScheduleService {
    
    public byte[] getTemplate(UserLogin userLogin, String _distributorId);
    
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId);
    
    public ImportResultDto doImport(UserLogin userLogin, String _distributorId, String fileId);
    
}
