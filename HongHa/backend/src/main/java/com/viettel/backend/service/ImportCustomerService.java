package com.viettel.backend.service;

import com.viettel.backend.dto.ImportConfirmDto;
import com.viettel.backend.dto.ImportResultDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface ImportCustomerService {
    
    public String getImportCustomerTemplate(UserLogin userLogin);
    
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId);
    
    public ImportResultDto importCustomer(UserLogin userLogin, String _distributorId, String fileId);
    
}
