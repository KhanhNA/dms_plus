package com.viettel.backend.service;

import com.viettel.backend.dto.ImportConfirmDto;
import com.viettel.backend.dto.ImportProductPhotoDto;
import com.viettel.backend.dto.ImportResultDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface ImportProductService {

    public String getImportProductTemplate(UserLogin userLogin);

    public ImportConfirmDto verify(UserLogin userLogin, String fileId);
    
    public ImportConfirmDto confirm(UserLogin userLogin, String fileId);

    public ImportResultDto importProduct(UserLogin userLogin, String fileId, ImportProductPhotoDto photoDto);

}
