package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.CustomerSaleResultDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderSummaryDto;
import com.viettel.backend.dto.SalesResultDaily;
import com.viettel.backend.dto.MobileDashboardDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface MobileDashboardService extends Serializable {

    public MobileDashboardDto getDashboard(UserLogin userLogin);

    /**
     * Get total order in current month group by all customer {id,
     * code, name, contact, address, productivity, revenue}
     **/
    public ListJson<CustomerSaleResultDto> getDashboardByCustomer(UserLogin userLogin);

    /**
     * Get total order in current month group by one customer {id,
     * code, created, productivity, grandTotal}
     **/
    public ListJson<OrderSummaryDto> getDashboardByCustomerDetail(UserLogin userLogin, String customerId);

    /**
     * Get total order in current month group by all working day {date,
     * productivity, revenue}
     **/
    public ListJson<SalesResultDaily> getDashboardByDay(UserLogin userLogin);

    /**
     * GGet total order in current month group by one day {id, code,
     * name, contact, address, productivity, revenue}
     **/
    public ListJson<CustomerSaleResultDto> getDashboardByDayDetail(UserLogin userLogin, String isoDate);

}
