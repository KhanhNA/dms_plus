package com.viettel.backend.service;

import com.viettel.backend.domain.Route;
import com.viettel.backend.dto.RouteCreateDto;
import com.viettel.backend.dto.RouteDto;

public interface RouteService extends BasicNameCategoryByDistributorService<Route, RouteDto, RouteDto, RouteCreateDto> {

}
