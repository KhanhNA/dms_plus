package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderCreateDto;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.dto.embed.OrderPromotionDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanOrderService extends Serializable {

    public ListJson<OrderPromotionDto> calculatePromotion(UserLogin userLogin, OrderCreateDto dto);

    public String createUnplannedOrder(UserLogin userLogin, OrderCreateDto dto);

    /** ALL Orders (draft + approved + rejected) */
    public ListJson<OrderSimpleDto> getOrdersToday(UserLogin userLogin);

    /** ALL Orders (draft + approved + rejected) */
    public ListJson<OrderSimpleDto> getOrdersByCustomerToday(UserLogin userLogin, String customerId);

    /** ALL Orders (draft + approved + rejected) */
    public OrderDto getOrderById(UserLogin userLogin, String id);

}
