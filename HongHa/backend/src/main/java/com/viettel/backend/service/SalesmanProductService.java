package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.ProductForOrderDto;
import com.viettel.backend.dto.ProductDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanProductService extends Serializable {

    // ****************
    // *** SALESMAN ***
    // ****************
    public ListJson<ProductDto> getProducts(UserLogin userLogin, String search, Pageable pageable);
    
    public ListJson<ProductForOrderDto> getProductsForOrder(UserLogin userLogin, String customerId);
    
}
