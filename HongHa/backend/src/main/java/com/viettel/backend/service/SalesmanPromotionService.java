package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.PromotionSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanPromotionService extends Serializable {

    // ****************
    // *** SALESMAN ***
    // ****************
    public ListJson<PromotionSimpleDto> getPromtionsAvailableToday(UserLogin userLogin);
    
}
