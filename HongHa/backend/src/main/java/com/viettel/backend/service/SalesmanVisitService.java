package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.dto.VisitClosingDto;
import com.viettel.backend.dto.VisitEndDto;
import com.viettel.backend.dto.VisitInfoDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesmanVisitService extends Serializable {

    /** Salesman start visit a customer at a location @return visit id */
    public String startVisit(UserLogin userLogin, String customerId, LocationDto locationDto);

    /** Salesman end a visit @return visit info */
    public VisitInfoDto endVisit(UserLogin userLogin, String visitId, VisitEndDto dto);

    /** Salesman mark as a customer is closed @return visit id */
    public String markAsClosed(UserLogin userLogin, String customerId, VisitClosingDto dto);

    /** get today visit info of salesman's customer */
    public VisitInfoDto getVisitedTodayInfo(UserLogin userLogin, String customerId);

}
