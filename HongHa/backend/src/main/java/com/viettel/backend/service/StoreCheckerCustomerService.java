package com.viettel.backend.service;

import com.viettel.backend.dto.CheckCustomerDto;
import com.viettel.backend.dto.CustomerForCheckDto;
import com.viettel.backend.dto.CustomerSummaryDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;

public interface StoreCheckerCustomerService {

    public CustomerSummaryDto getCustomerSummary(UserLogin userLogin, String id);
    
    public ListJson<CustomerForCheckDto> getCustomersForCheckToday(UserLogin userLogin);
    
    public void checkForCustomer(UserLogin userLogin, String customerId, CheckCustomerDto dto);

}
