package com.viettel.backend.service;

import java.io.Serializable;
import java.util.List;

import com.viettel.backend.dto.ExhibitionRatingDto.ExhibitionRatingItemDto;
import com.viettel.backend.dto.ExhibitionWithRatingDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;

public interface StoreCheckerExhibitionService extends Serializable {

    public ListJson<ExhibitionWithRatingDto> getExhibitionsByCustomer(UserLogin userLogin, String customerId);
    
    public void createExhibitionRatingByCustomer(UserLogin userLogin, String exhibitionId, 
            String customerId, List<ExhibitionRatingItemDto> exhibitionRatingItemDtos);

}
