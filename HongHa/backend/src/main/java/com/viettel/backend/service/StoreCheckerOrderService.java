package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface StoreCheckerOrderService extends Serializable {

    public OrderDto getOrderById(UserLogin userLogin, String id);

}
