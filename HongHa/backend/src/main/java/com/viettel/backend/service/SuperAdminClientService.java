package com.viettel.backend.service;

import com.viettel.backend.domain.Client;
import com.viettel.backend.dto.NameCategoryDto;

public interface SuperAdminClientService extends
        BasicCategoryService<Client, NameCategoryDto, NameCategoryDto, NameCategoryDto> {

}
