package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerDto;
import com.viettel.backend.dto.CustomerForTrackingDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorCustomerService extends Serializable {

    public ListJson<CustomerSimpleDto> getCustomers(UserLogin userLogin, String search, String distributorId,
            String salesmanId, Pageable pageable);

    public CustomerDto getCustomerById(UserLogin userLogin, String id);

    public ListJson<CustomerSimpleDto> getCustomersPending(UserLogin userLogin, String search, String salesmanId,
            Pageable pageable);

    public CustomerDto getCustomerPendingById(UserLogin userLogin, String id);

    public void approveCustomer(UserLogin userLogin, String id);

    public void rejectCustomer(UserLogin userLogin, String id);

    public ListJson<CustomerForTrackingDto> getCustomersTodayBySalesman(UserLogin userLogin, String salesmanId);
    
    public String createCustomer(UserLogin userLogin, CustomerCreateDto dto);

}
