package com.viettel.backend.service;

import java.io.Serializable;

import com.viettel.backend.dto.DistributorSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorDistributorService extends Serializable {

    public ListJson<DistributorSimpleDto> getDistbutors(UserLogin userLogin);

}
