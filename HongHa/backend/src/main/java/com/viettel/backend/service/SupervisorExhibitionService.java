package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ExhibitionSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorExhibitionService extends Serializable {

    public ListJson<ExhibitionSimpleDto> getExhibitions(UserLogin userLogin, String search, Pageable pageable);

}
