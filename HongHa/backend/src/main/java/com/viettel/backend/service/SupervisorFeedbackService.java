package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.VisitFeedbackDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorFeedbackService extends Serializable {

    public ListJson<VisitFeedbackDto> getFeedbacks(UserLogin userLogin, Pageable pageable);

    public VisitFeedbackDto readFeedback(UserLogin userLogin, String feedbackId);

}
