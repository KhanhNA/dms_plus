package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorOrderService extends Serializable {

    public OrderDto getOrderById(UserLogin userLogin, String orderId);

    public ListJson<OrderSimpleDto> getPendingOrders(UserLogin userLogin, String distributorId, Pageable pageable);

    public OrderDto getPendingOrderById(UserLogin userLogin, String orderId);

    public void approvePendingOrder(UserLogin userLogin, String orderId);

    public void rejectPendingOrder(UserLogin userLogin, String orderID);

}
