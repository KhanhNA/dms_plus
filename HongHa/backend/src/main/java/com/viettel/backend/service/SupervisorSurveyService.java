package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SurveySimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorSurveyService extends Serializable {

    public ListJson<SurveySimpleDto> getSurveys(UserLogin userLogin, String search, Pageable pageable);

}
