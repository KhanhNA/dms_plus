package com.viettel.backend.service;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.TargetCreateDto;
import com.viettel.backend.dto.TargetDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SupervisorTargetService extends Serializable {
    
    public ListJson<TargetDto> getTargets(UserLogin userLogin, int month, int year, Pageable pageable);

    public TargetDto getById(UserLogin userLogin, String targetId);
    
    public TargetDto create(UserLogin userLogin, TargetCreateDto dto);
    
    public TargetDto update(UserLogin userLogin, String targetId, TargetCreateDto dto);
    
    public void delete(UserLogin userLogin, String targetId);
    
}
