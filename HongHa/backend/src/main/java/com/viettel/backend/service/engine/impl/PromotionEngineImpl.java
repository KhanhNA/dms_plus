package com.viettel.backend.service.engine.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.Promotion;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.OrderPromotion;
import com.viettel.backend.domain.embed.OrderPromotionDetail;
import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.domain.embed.PromotionDetail;
import com.viettel.backend.dto.OrderCreateDto;
import com.viettel.backend.dto.embed.OrderDetailCreatedDto;
import com.viettel.backend.dto.embed.OrderPromotionDetailDto;
import com.viettel.backend.dto.embed.OrderPromotionDto;
import com.viettel.backend.dto.embed.ProductEmbedDto;
import com.viettel.backend.engine.promotion.PromotionCondition;
import com.viettel.backend.engine.promotion.PromotionDetailResult;
import com.viettel.backend.engine.promotion.PromotionEngine;
import com.viettel.backend.engine.promotion.PromotionResult;
import com.viettel.backend.engine.promotion.PromotionRewardResult;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.PromotionRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.util.ObjectIdUtils;

@Component
public class PromotionEngineImpl implements com.viettel.backend.service.engine.PromotionEngine {

    private static final long serialVersionUID = -4196457639452792913L;
    PromotionEngine<ObjectId, ProductEmbed> promotionEngine = new PromotionEngine<ObjectId, ProductEmbed>();

    @Autowired
    private PromotionRepository promotionRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Override
    public void calculatePromotion(UserLogin userLogin, OrderCreateDto dto, Order order) {
        if (dto == null || dto.getDetails() == null) {
            throw new IllegalArgumentException("order invalid");
        }

        order.setDiscountPercentage(dto.getDiscountPercentage());
        order.setDiscountAmt(dto.getDiscountAmt());

        BigDecimal subTotal = BigDecimal.ZERO;
        BigDecimal discountAmt = BigDecimal.ZERO;
        BigDecimal promotionAmt = BigDecimal.ZERO;
        BigDecimal grandTotal = BigDecimal.ZERO;

        for (OrderDetailCreatedDto detailDto : dto.getDetails()) {
            ObjectId productId = ObjectIdUtils.getObjectId(detailDto.getProductId(), null);
            if (productId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            Product product = productRepository.getById(userLogin.getClientId(), productId);
            if (product == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            OrderDetail detail = new OrderDetail();
            detail.setProduct(new ProductEmbed(product));
            detail.setQuantity(detailDto.getQuantity());

            subTotal = subTotal.add(detail.getAmount());

            order.addDetails(detail);
        }

        List<OrderPromotion> orderPromotions = null;
        List<Promotion> promotions = promotionRepository.getPromotionsAvailableToday(userLogin.getClientId());
        if (promotions != null) {
            // CALCULATE PROMOTION
            List<PromotionResult<ObjectId, ProductEmbed>> promotionResults = promotionEngine.calculate(promotions,
                    order);
            if (promotionResults != null) {
                // SETUP MAP PROMOTION
                HashMap<ObjectId, Promotion> promotionById = new HashMap<ObjectId, Promotion>();
                HashMap<ObjectId, PromotionDetail> promotionDetailById = new HashMap<ObjectId, PromotionDetail>();
                for (Promotion promotion : promotions) {
                    promotionById.put(promotion.getId(), promotion);
                    if (promotion.getDetails() != null) {
                        for (PromotionDetail promotionDetail : promotion.getDetails()) {
                            promotionDetailById.put(promotionDetail.getId(), promotionDetail);
                        }
                    }
                }

                // PREPARE DTO
                orderPromotions = new ArrayList<OrderPromotion>(promotionResults.size());
                for (PromotionResult<ObjectId, ProductEmbed> promotionResult : promotionResults) {
                    if (promotionResult.getDetails() != null) {
                        Promotion promotion = promotionById.get(promotionResult.getId());
                        if (promotion == null) {
                            throw new UnsupportedOperationException("promotion not found");
                        }

                        OrderPromotion orderPromotion = new OrderPromotion();
                        orderPromotion.setId(promotion.getId());
                        orderPromotion.setName(promotion.getName());

                        for (PromotionDetailResult<ObjectId, ProductEmbed> detailResult : promotionResult.getDetails()) {
                            PromotionDetail promotionDetail = promotionDetailById.get(detailResult.getId());
                            if (promotionDetail == null) {
                                throw new UnsupportedOperationException("promotion detail not found");
                            }

                            OrderPromotionDetail orderPromotionDetail = new OrderPromotionDetail();
                            orderPromotionDetail.setId(promotionDetail.getId());
                            orderPromotionDetail.setName(promotionDetail.getName());
                            if (detailResult.getRewardResult() == null) {
                                throw new UnsupportedOperationException("reward result is null");
                            }
                            orderPromotionDetail.setRewardResult(detailResult.getRewardResult());
                            if (detailResult.getRewardResult().getAmount() != null) {
                                promotionAmt = promotionAmt.add(detailResult.getRewardResult().getAmount());
                            }

                            orderPromotion.addDetail(orderPromotionDetail);
                        }

                        orderPromotions.add(orderPromotion);
                    }
                }
            }
        }
        order.setPromotionResults(orderPromotions);

        if (subTotal.subtract(promotionAmt).signum() == -1) {
            throw new UnsupportedOperationException("error when calculate promotion");
        }

        if (dto.getDiscountPercentage() != null) {
            order.setDiscountPercentage(dto.getDiscountPercentage());
            discountAmt = subTotal.subtract(promotionAmt).multiply(dto.getDiscountPercentage())
                    .divide(new BigDecimal(100), 0, BigDecimal.ROUND_HALF_UP);
        } else if (dto.getDiscountAmt() != null) {
            discountAmt = dto.getDiscountAmt();
        }

        grandTotal = subTotal.subtract(promotionAmt).subtract(discountAmt);

        order.setSubTotal(subTotal);
        order.setPromotionAmt(promotionAmt);
        order.setDiscountAmt(discountAmt);
        order.setGrandTotal(grandTotal);
    }

    @Override
    public List<OrderPromotionDto> getPromotions(UserLogin userLogin, OrderCreateDto dto) {
        if (dto == null || dto.getDetails() == null) {
            throw new IllegalArgumentException("order invalid");
        }

        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("client config not found");
        }

        // PREPARE ORDER
        Order order = new Order();
        for (OrderDetailCreatedDto detailDto : dto.getDetails()) {
            ObjectId productId = ObjectIdUtils.getObjectId(detailDto.getProductId(), null);
            if (productId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            Product product = productRepository.getById(userLogin.getClientId(), productId);
            if (product == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            OrderDetail detail = new OrderDetail();
            detail.setProduct(new ProductEmbed(product));
            detail.setQuantity(detailDto.getQuantity());

            order.addDetails(detail);
        }

        List<OrderPromotionDto> orderPromotionDtos = null;
        List<Promotion> promotions = promotionRepository.getPromotionsAvailableToday(userLogin.getClientId());
        if (promotions != null) {
            // CALCULATE PROMOTION
            List<PromotionResult<ObjectId, ProductEmbed>> promotionResults = promotionEngine.calculate(promotions,
                    order);
            if (promotionResults != null) {
                // SETUP MAP PROMOTION
                HashMap<ObjectId, Promotion> promotionById = new HashMap<ObjectId, Promotion>();
                HashMap<ObjectId, PromotionDetail> promotionDetailById = new HashMap<ObjectId, PromotionDetail>();
                for (Promotion promotion : promotions) {
                    promotionById.put(promotion.getId(), promotion);
                    if (promotion.getDetails() != null) {
                        for (PromotionDetail promotionDetail : promotion.getDetails()) {
                            promotionDetailById.put(promotionDetail.getId(), promotionDetail);
                        }
                    }
                }

                // PREPARE DTO
                orderPromotionDtos = new ArrayList<OrderPromotionDto>(promotionResults.size());
                for (PromotionResult<ObjectId, ProductEmbed> promotionResult : promotionResults) {
                    if (promotionResult.getDetails() != null) {
                        Promotion promotion = promotionById.get(promotionResult.getId());
                        if (promotion == null) {
                            throw new UnsupportedOperationException("promotion not found");
                        }

                        OrderPromotionDto orderPromotionDto = new OrderPromotionDto();
                        orderPromotionDto.setId(promotion.getId().toString());
                        orderPromotionDto.setName(promotion.getName());

                        for (PromotionDetailResult<ObjectId, ProductEmbed> detailResult : promotionResult.getDetails()) {
                            PromotionDetail promotionDetail = promotionDetailById.get(detailResult.getId());
                            if (promotionDetail == null) {
                                throw new UnsupportedOperationException("promotion detail not found");
                            }

                            OrderPromotionDetailDto orderPromotionDetailDto = new OrderPromotionDetailDto();
                            orderPromotionDetailDto.setId(promotionDetail.getId().toString());
                            orderPromotionDetailDto.setName(promotionDetail.getName());
                            if (detailResult.getRewardResult() == null) {
                                throw new UnsupportedOperationException("reward result is null");
                            }
                            PromotionRewardResult<String, ProductEmbedDto> rewardResultDto = new PromotionRewardResult<String, ProductEmbedDto>();
                            rewardResultDto.setAmount(detailResult.getRewardResult().getAmount());
                            if (detailResult.getRewardResult().getProduct() != null) {
                                rewardResultDto.setProduct(new ProductEmbedDto(detailResult.getRewardResult()
                                        .getProduct(), clientConfig.getDefaultProductPhoto()));
                                rewardResultDto.setQuantity(detailResult.getRewardResult().getQuantity());
                            } else if (detailResult.getRewardResult().getProductText() != null) {
                                rewardResultDto.setProduct(new ProductEmbedDto(detailResult.getRewardResult()
                                        .getProductText()));
                                rewardResultDto.setQuantity(detailResult.getRewardResult().getQuantity());
                            }

                            PromotionCondition<String, ProductEmbedDto> condition = new PromotionCondition<String, ProductEmbedDto>();
                            condition
                                    .setProduct(promotionDetail.getCondition().getProduct() != null ? new ProductEmbedDto(
                                            promotionDetail.getCondition().getProduct(), clientConfig
                                                    .getDefaultProductPhoto()) : null);
                            condition.setQuantity(promotionDetail.getCondition().getQuantity());
                            orderPromotionDetailDto.setCondition(condition);

                            orderPromotionDetailDto.setRewardResult(rewardResultDto);

                            orderPromotionDto.addDetail(orderPromotionDetailDto);
                        }

                        orderPromotionDtos.add(orderPromotionDto);
                    }
                }
            }
        }

        return orderPromotionDtos;
    }

}
