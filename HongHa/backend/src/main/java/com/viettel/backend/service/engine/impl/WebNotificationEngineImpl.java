package com.viettel.backend.service.engine.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.FeedbackRepository;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.websocket.core.WebsocketUtils;
import com.viettel.backend.websocket.queue.DelayObject;
import com.viettel.backend.websocket.queue.DelayQueueConsumer;

@Component
public class WebNotificationEngineImpl implements WebNotificationEngine, DisposableBean {

    private static final long serialVersionUID = -4843458026392503128L;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String QUEUE_NEW_ORDER = "/queue/new_order";
    private static final String QUEUE_NEW_CUSTOMER = "/queue/new_customer";
    private static final String QUEUE_NEW_FEEDBACK = "/queue/new_feedback";

    private static final String QUEUE_CHANGED_ORDER = "/queue/changed_order";
    private static final String QUEUE_CHANGED_CUSTOMER = "/queue/changed_customer";
    private static final String QUEUE_CHANGED_FEEDBACK = "/queue/changed_feedback";

    // Creates an instance of blocking queue using the DelayQueue.
    private BlockingQueue<DelayObject> queue;
    private DelayQueueConsumer consumer;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private UserRepository userRepository;

    public WebNotificationEngineImpl() {
        this.queue = new DelayQueue<DelayObject>();
        this.consumer = new DelayQueueConsumer(queue);
        this.consumer.start();
    }

    @Override
    public void notifyNewOrderForDistributor(final UserLogin userLogin, final ObjectId distributorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                long value = orderPendingRepository.countPendingOrdersByDistributors(userLogin.getClientId(),
                        Collections.singletonList(distributorId));

                Set<ObjectId> userIds = userRepository.getDistributorUserIds(userLogin.getClientId(),
                        Arrays.asList(distributorId));

                if (userIds != null) {
                    for (ObjectId userId : userIds) {
                        WebsocketUtils.sendToUser(messagingTemplate, userId.toString(), QUEUE_NEW_ORDER, userLogin
                                .getUserId().toString(), value);
                    }
                }
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyChangedOrderForDistributor(final UserLogin userLogin, final ObjectId distributorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                long value = orderPendingRepository.countPendingOrdersByDistributors(userLogin.getClientId(),
                        Collections.singletonList(distributorId));

                Set<ObjectId> userIds = userRepository.getDistributorUserIds(userLogin.getClientId(),
                        Arrays.asList(distributorId));

                if (userIds != null) {
                    for (ObjectId userId : userIds) {
                        WebsocketUtils.sendToUser(messagingTemplate, userId.toString(), QUEUE_CHANGED_ORDER, userLogin
                                .getUserId().toString(), value);
                    }
                }
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyNewFeedbackForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Collections.singleton(supervisorId));
                Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                        distributorIds, false, null, null);
                long value = feedbackRepository.countFeedbackByCustomersUnread(userLogin.getClientId(), customerIds);

                WebsocketUtils.sendToUser(messagingTemplate, supervisorId.toString(), QUEUE_NEW_FEEDBACK, userLogin
                        .getUserId().toString(), value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyChangedFeedbackForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Collections.singleton(supervisorId));
                Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                        distributorIds, false, null, null);
                long value = feedbackRepository.countFeedbackByCustomersUnread(userLogin.getClientId(), customerIds);

                WebsocketUtils.sendToUser(messagingTemplate, supervisorId.toString(), QUEUE_CHANGED_FEEDBACK, userLogin
                        .getUserId().toString(), value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyNewCustomerForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Arrays.asList(supervisorId));
                long value = customerPendingRepository.countCustomersByDistributors(userLogin.getClientId(),
                        distributorIds, Customer.APPROVE_STATUS_PENDING, null);

                WebsocketUtils.sendToUser(messagingTemplate, supervisorId.toString(), QUEUE_NEW_CUSTOMER, userLogin
                        .getUserId().toString(), value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void notifyChangedCustomerForSupervisor(final UserLogin userLogin, final ObjectId supervisorId) {
        DelayObject delayObject = new DelayObject(DelayObject.DEFAULT_DELAY_IN_MILLIS) {
            @Override
            public void excute() {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Arrays.asList(supervisorId));
                long value = customerPendingRepository.countCustomersByDistributors(userLogin.getClientId(),
                        distributorIds, Customer.APPROVE_STATUS_PENDING, null);

                WebsocketUtils.sendToUser(messagingTemplate, supervisorId.toString(), QUEUE_CHANGED_CUSTOMER, userLogin
                        .getUserId().toString(), value);
            }
        };

        try {
            this.queue.put(delayObject);
        } catch (InterruptedException e) {
            logger.error("queue error", e);
        }
    }

    @Override
    public void destroy() throws Exception {
        consumer.destroy();
    }

}
