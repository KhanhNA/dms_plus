package com.viettel.backend.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.domain.User;
import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicRepository;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.SalesConfigRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.util.LocationUtils;
import com.viettel.backend.util.ObjectIdUtils;
import com.viettel.backend.util.StringUtils;

public abstract class AbstractService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ClientConfigRepository clientConfigRepository;
    
    @Autowired
    private SalesConfigRepository salesConfigRepository;

    protected final <D extends PO> void initPOWhenCreate(Class<D> clazz, UserLogin userLogin, D domain) {
        if (PO.isClientRootFixed(clazz)) {
            domain.setClientId(PO.CLIENT_ROOT_ID);
        } else {
            domain.setClientId(userLogin.getClientId());
        }

        domain.setActive(true);
    }

    protected User getCurrentUser(UserLogin userLogin) {
        User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
        if (user == null) {
            throw new UnsupportedOperationException("current user not found");
        }
        
        return user;
    }
    
    protected final ClientConfig getClientCondig(UserLogin userLogin) {
        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("client config not found");
        }
        
        return clientConfig;
    }
    
    protected final SalesConfig getSalesConfig(UserLogin userLogin) {
        SalesConfig salesConfig = salesConfigRepository.getSalesConfig(userLogin.getClientId());
        if (salesConfig == null) {
            throw new UnsupportedOperationException("sales config not found");
        }
        
        return salesConfig;
    }
    
    protected <D extends PO> Set<ObjectId> getIdSet(Collection<D> pos) {
        if (pos == null || pos.isEmpty()) {
            return Collections.<ObjectId> emptySet();
        }

        Set<ObjectId> poIds = new HashSet<ObjectId>(pos.size());
        for (PO po : pos) {
            poIds.add(po.getId());
        }

        return poIds;
    }

    protected <D extends PO> D getMadatoryPO(UserLogin userLogin, String _id, BasicRepository<D> repository) {
        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        D domain = repository.getById(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return domain;
    }
    
    protected <D extends PO> D getMadatoryPOWithDeactivated(UserLogin userLogin, String _id, BasicRepository<D> repository) {
        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        D domain = repository.getByIdWithDeactivated(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return domain;
    }
    
    protected <D extends PO> ObjectId getMadatoryPOId(UserLogin userLogin, String _id, BasicRepository<D> repository) {
        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        if (!repository.exists(userLogin.getClientId(), id)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        
        return id;
    }
    
    protected <D extends PO> List<ObjectId> getMadatoryPOIds(UserLogin userLogin, List<String> _ids, BasicRepository<D> repository) {
        List<ObjectId> ids = ObjectIdUtils.getObjectIds(_ids);
        if (ids == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        for (ObjectId id : ids) {
            if (!repository.exists(userLogin.getClientId(), id)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
        }
        
        return ids;
    }

    protected void checkMandatoryParams(Object... params) {
        if (params != null) {
            for (Object param : params) {
                if (param == null) {
                    throw new BusinessException(ExceptionCode.INVALID_PARAM);
                }

                if (param instanceof String) {
                    if (StringUtils.isNullOrEmpty((String) param, true)) {
                        throw new BusinessException(ExceptionCode.INVALID_PARAM);
                    }
                } else if (param instanceof Collection<?>) {
                    Collection<?> items = (Collection<?>) param;

                    if (items.isEmpty()) {
                        throw new BusinessException(ExceptionCode.INVALID_PARAM);
                    }

                    for (Object item : items) {
                        checkMandatoryParams(item);
                    }
                } else if (param instanceof LocationDto) {
                    LocationDto location = (LocationDto) param;

                    if (!LocationUtils.checkLocationValid(location)) {
                        throw new BusinessException(ExceptionCode.INVALID_PARAM);
                    }
                }
            }
        }
    }
    
    protected final <D extends PO> HashMap<ObjectId, D> buildPOByIdMap(Class<D> clazz, List<D> domains) {
        HashMap<ObjectId, D> map = new HashMap<ObjectId, D>();
        if (domains != null && !domains.isEmpty()) {
            for (D domain : domains) {
                map.put(domain.getId(), domain);
            }
        }
        
        return map;
    }

}
