package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.viettel.backend.domain.User;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.util.HardCodeUtils;

public abstract class AbstractStoreCheckerService extends AbstractService {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    public void checkIsStoreChecker(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_STORE_CHECKER)) {
            throw new BusinessException(ExceptionCode.STORECHECKER_ONLY);
        }
    }

    public User getCurrentStoreChecker(UserLogin userLogin) {
        User supervisor = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
        if (supervisor == null) {
            throw new UnsupportedOperationException("current user not found");
        }

        return supervisor;
    }

}
