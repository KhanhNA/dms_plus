package com.viettel.backend.service.impl;

import java.util.Arrays;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.User;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

public abstract class AbstractSupervisorService extends AbstractService {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    public void checkIsSupervisor(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            throw new BusinessException(ExceptionCode.SUPERVISOR_ONLY);
        }
    }

    public User getCurrentSupervisor(UserLogin userLogin) {
        User supervisor = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
        if (supervisor == null) {
            throw new UnsupportedOperationException("current user not found");
        }

        return supervisor;
    }

    public User checkSupervisorSalesman(UserLogin userLogin, String _salesmanId, ObjectId distributorId) {
        ObjectId salesmanId = ObjectIdUtils.getObjectId(_salesmanId, null);
        if (salesmanId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (distributorId == null) {
            throw new IllegalArgumentException("distributorId is null");
        }

        User salesman = userRepository.getById(userLogin.getClientId(), salesmanId);
        if (salesman == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (salesman.getDistributor() == null || salesman.getDistributor().getId() == null
                || !salesman.getDistributor().getId().equals(distributorId)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return salesman;
    }

    public User checkSupervisorSalesman(UserLogin userLogin, String _salesmanId) {
        ObjectId salesmanId = ObjectIdUtils.getObjectId(_salesmanId, null);

        return checkSupervisorSalesman(userLogin, salesmanId);
    }

    public User checkSupervisorSalesman(UserLogin userLogin, ObjectId salesmanId) {
        if (salesmanId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()));

        User salesman = userRepository.getById(userLogin.getClientId(), salesmanId);
        if (salesman == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (salesman.getDistributor() == null || salesman.getDistributor().getId() == null || distributorIds == null
                || !distributorIds.contains(salesman.getDistributor().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return salesman;
    }

    public Customer checkSupervisorCustomer(UserLogin userLogin, String _customerId) {
        ObjectId customerId = ObjectIdUtils.getObjectId(_customerId, null);
        if (customerId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()));

        Customer customer = customerRepository.getById(userLogin.getClientId(), customerId);
        if (customer == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (customer.getDistributor() == null || customer.getDistributor().getId() == null || distributorIds == null
                || !distributorIds.contains(customer.getDistributor().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return customer;
    }

    public Customer checkSupervisorCustomerPending(UserLogin userLogin, String _customerId) {
        ObjectId customerId = ObjectIdUtils.getObjectId(_customerId, null);
        if (customerId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()));

        Customer customer = customerPendingRepository.getById(userLogin.getClientId(), customerId);
        if (customer == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (customer.getApproveStatus() != Customer.APPROVE_STATUS_PENDING) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (customer.getDistributor() == null || customer.getDistributor().getId() == null || distributorIds == null
                || !distributorIds.contains(customer.getDistributor().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return customer;
    }

    public Distributor checkSupervisorDistributor(UserLogin userLogin, String _distributorId) {
        ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
        if (distributorId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);
        if (distributor == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        if (distributor.getSupervisor() == null || distributor.getSupervisor().getId() == null
                || !distributor.getSupervisor().getId().equals(userLogin.getUserId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return distributor;
    }

}
