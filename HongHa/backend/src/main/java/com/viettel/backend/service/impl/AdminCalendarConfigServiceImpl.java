package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.CalendarConfig;
import com.viettel.backend.dto.CalendarConfigDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminCalendarConfigService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class AdminCalendarConfigServiceImpl extends AbstractService implements AdminCalendarConfigService {

    /**
     * 
     */
    private static final long serialVersionUID = -3178351861927142648L;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    @Override
    public CalendarConfigDto getCalendarConfig(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        CalendarConfig calendarConfig = calendarConfigRepository.getCalendarConfig(userLogin.getClientId());
        if (calendarConfig == null) {
            return null;
        }

        CalendarConfigDto dto = new CalendarConfigDto();
        dto.setId(calendarConfig.getId().toString());
        dto.setDraft(calendarConfig.isDraft());
        dto.setWorkingDays(calendarConfig.getWorkingDays());
        dto.setEveryYearHolidays(calendarConfig.getEveryYearHolidays());
        dto.setExceptionHolidays(calendarConfig.getExceptionHolidays());
        dto.setExceptionWorkingDays(calendarConfig.getExceptionWorkingDays());

        return dto;
    }

    @Override
    public void saveCalendarConfig(UserLogin userLogin, CalendarConfigDto dto) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        checkMandatoryParams(dto, dto.getWorkingDays());

        CalendarConfig calendarConfig = calendarConfigRepository.getCalendarConfig(userLogin.getClientId());
        if (calendarConfig == null) {
            calendarConfig = new CalendarConfig();
        }

        initPOWhenCreate(CalendarConfig.class, userLogin, calendarConfig);

        calendarConfig.setWorkingDays(dto.getWorkingDays());
        calendarConfig.setEveryYearHolidays(dto.getEveryYearHolidays());
        calendarConfig.setExceptionHolidays(dto.getExceptionHolidays());
        calendarConfig.setExceptionWorkingDays(dto.getExceptionWorkingDays());

        calendarConfigRepository.save(userLogin.getClientId(), calendarConfig);
    }

}
