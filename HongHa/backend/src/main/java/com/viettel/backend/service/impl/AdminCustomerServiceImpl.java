package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.District;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerDto;
import com.viettel.backend.dto.CustomerForTrackingDto;
import com.viettel.backend.dto.CustomerForVisitDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminCustomerService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.service.sub.CacheSubService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.LocationUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class AdminCustomerServiceImpl extends
        BasicCategoryServiceImpl<Customer, CustomerSimpleDto, CustomerDto, CustomerCreateDto> implements
        AdminCustomerService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private FileEngine fileEngine;

    @Autowired
    private CacheSubService cacheSubService;

    @Override
    public BasicCategoryRepository<Customer> getRepository() {
        return customerRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }

    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }

    @Override
    public Customer createDomain(UserLogin userLogin, CustomerCreateDto createdto) {
        checkMandatoryParams(createdto, createdto.getName(), createdto.getMobile(), createdto.getLatitude(),
                createdto.getLongitude(), createdto.getCustomerTypeId(), createdto.getDistrictId());

        if (!LocationUtils.checkLocationValid(createdto.getLongitude(), createdto.getLatitude())) {
            throw new BusinessException(ExceptionCode.INVALID_LOCATION_PARAM);
        }

        User admin = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());

        Customer customer = new Customer();
        initPOWhenCreate(Customer.class, userLogin, customer);
        customer.setDraft(true);

        customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
        customer.setCreatedTime(DateTimeUtils.getCurrentTime());
        customer.setCreatedBy(new UserEmbed(admin));
        customer.setCode(codeGeneratorRepository.getCustomerCode(userLogin.getClientId().toString()));

        customer.setName(createdto.getName());
        customer.setMobile(createdto.getMobile());
        customer.setPhone(createdto.getPhone());
        customer.setContact(createdto.getContact());
        customer.setIdNumber(createdto.getIdNumber());
        customer.setEmail(createdto.getEmail());
        // customer.setAddress(createdto.getAddress());
        customer.setDescription(createdto.getDescription());

        if (createdto.getDistributorId() != null) {
            ObjectId distributorId = ObjectIdUtils.getObjectId(createdto.getDistributorId(), null);
            if (distributorId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);
            if (distributor == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            customer.setDistributor(new DistributorEmbed(distributor));
        } else {
            customer.setDistributor(null);
        }

        CustomerType customerType = getMadatoryPO(userLogin, createdto.getCustomerTypeId(), customerTypeRepository);
        customer.setCustomerType(customerType);

        District district = getMadatoryPO(userLogin, createdto.getDistrictId(), districtRepository);
        customer.setDistrict(district);

        if (createdto.getPhotos() != null && createdto.getPhotos().size() > 0) {
            if (!fileEngine.exists(userLogin, createdto.getPhotos())) {
                throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
            }
            customer.setPhotos(createdto.getPhotos());
        }

        customer.setLocation(new double[] { createdto.getLongitude(), createdto.getLatitude() });

        return customer;
    }

    @Override
    public void updateDomain(UserLogin userLogin, Customer customer, CustomerCreateDto createdto) {
        if (!LocationUtils.checkLocationValid(createdto.getLongitude(), createdto.getLatitude())) {
            throw new BusinessException(ExceptionCode.INVALID_LOCATION_PARAM);
        }

        if (customer.isDraft()) {
            checkMandatoryParams(createdto, createdto.getName(), createdto.getMobile(), createdto.getLatitude(),
                    createdto.getLongitude(), createdto.getCustomerTypeId(), createdto.getDistrictId());

            customer.setName(createdto.getName());
            // customer.setAddress(createdto.getAddress());

            CustomerType customerType = getMadatoryPO(userLogin, createdto.getCustomerTypeId(), customerTypeRepository);
            customer.setCustomerType(customerType);

            District district = getMadatoryPO(userLogin, createdto.getDistrictId(), districtRepository);
            customer.setDistrict(district);
        } else {
            checkMandatoryParams(createdto, createdto.getMobile(), createdto.getLatitude(), createdto.getLongitude());
        }

        customer.setMobile(createdto.getMobile());
        customer.setPhone(createdto.getPhone());
        customer.setContact(createdto.getContact());
        customer.setIdNumber(createdto.getIdNumber());
        customer.setEmail(createdto.getEmail());
        customer.setDescription(createdto.getDescription());

        boolean changeDistribuor = false;
        if (createdto.getDistributorId() != null) {
            ObjectId distributorId = ObjectIdUtils.getObjectId(createdto.getDistributorId(), null);
            if (distributorId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);
            if (distributor == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            if (customer.getDistributor() == null || !customer.getDistributor().getId().equals(distributorId)) {
                changeDistribuor = true;
            }

            customer.setDistributor(new DistributorEmbed(distributor));
        } else {
            if (customer.getDistributor() != null) {
                changeDistribuor = true;
            }
            customer.setDistributor(null);
        }

        if (changeDistribuor) {
            if (orderPendingRepository.checkCustomerHasPendingOrder(userLogin.getClientId(), customer.getId())) {
                throw new BusinessException(ExceptionCode.CUSTOMER_HAS_PENDING_ORDER);
            }

            customer.setSchedule(null);
        }

        if (createdto.getPhotos() != null && createdto.getPhotos().size() > 0) {
            if (!fileEngine.exists(userLogin, createdto.getPhotos())) {
                throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
            }
            customer.setPhotos(createdto.getPhotos());
        }

        customer.setLocation(new double[] { createdto.getLongitude(), createdto.getLatitude() });
    }

    @Override
    public ObjectId update(UserLogin userLogin, String _id, CustomerCreateDto createdto) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Customer domain = getRepository().getByIdWithDraft(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        ObjectId oldDistributorId = domain.getDistributor() == null ? null : domain.getDistributor().getId();

        updateDomain(userLogin, domain, createdto);

        domain = getRepository().save(userLogin.getClientId(), domain);

        ObjectId newDistributorId = domain.getDistributor() == null ? null : domain.getDistributor().getId();

        Set<ObjectId> distributorIds = new HashSet<ObjectId>();
        if (oldDistributorId != null) {
            distributorIds.add(oldDistributorId);
        }
        if (newDistributorId != null) {
            distributorIds.add(newDistributorId);
        }
        if (distributorIds.size() == 1 || (distributorIds.size() == 2 && !oldDistributorId.equals(newDistributorId))) {
            Set<ObjectId> routeIds = routeRepository.getRouteIdsByDistributors(userLogin.getClientId(), distributorIds);
            cacheSubService.reclaculateScheduleNumberPlannedCache(userLogin, routeIds);
        }

        return domain.getId();
    }

    @Override
    public CustomerSimpleDto createSimpleDto(UserLogin userLogin, Customer domain) {
        return new CustomerSimpleDto(domain);
    }

    @Override
    public CustomerDto createDetailDto(UserLogin userLogin, Customer domain) {
        return new CustomerDto(domain);
    }

    @Override
    public ListJson<CustomerForTrackingDto> getCustomersTodayBySalesman(UserLogin userLogin, String salesmanId) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        User salesman = getMadatoryPO(userLogin, salesmanId, userRepository);

        ObjectId clientId = userLogin.getClientId();

        Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(clientId,
                Collections.singleton(salesman.getId()));

        Collection<Customer> todayCustomers = customerRepository.getCustomersByRouteDate(clientId, routeIds,
                null, DateTimeUtils.getToday(), null, null);
        if (todayCustomers == null || todayCustomers.isEmpty()) {
            return ListJson.emptyList();
        }

        List<ObjectId> todayCustomerIds = new ArrayList<ObjectId>(todayCustomers.size());
        for (Customer customer : todayCustomers) {
            todayCustomerIds.add(customer.getId());
        }

        Map<ObjectId, Visit> visitTodayByCustomerId = visitRepository.getMapVisitByCustomerIdsToday(clientId,
                todayCustomerIds);

        List<CustomerForTrackingDto> customerDtos = new ArrayList<>(todayCustomers.size());
        for (Customer customer : todayCustomers) {
            // STATUS
            int status = CustomerForVisitDto.STATUS_UNVISITED;
            Visit visit = visitTodayByCustomerId.get(customer.getId());
            if (visit != null) {
                if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITING) {
                    status = CustomerForVisitDto.STATUS_VISITING;
                } else {
                    status = CustomerForVisitDto.STATUS_VISITED;
                }
            }

            CustomerForTrackingDto customerDto = new CustomerForTrackingDto(customer, true, status, 0, visit);
            customerDtos.add(customerDto);
        }

        return new ListJson<>(customerDtos, Long.valueOf(customerDtos.size()));
    }

}
