package com.viettel.backend.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminCustomerTypeService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class AdminCustomerTypeServiceImpl extends
        BasicCategoryServiceImpl<CustomerType, NameCategoryDto, NameCategoryDto, NameCategoryDto> implements
        AdminCustomerTypeService {

    private static final long serialVersionUID = -5771173067596328856L;
    
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerTypeRepository repository;

    @Override
    public BasicCategoryRepository<CustomerType> getRepository() {
        return repository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }
    
    @Override
    protected void checkBeforeDelete(UserLogin userLogin, ObjectId id) {
        if (customerRepository.checkCustomerTypeUsed(userLogin.getClientId(), id)) {
            throw new BusinessException(ExceptionCode.CUSTOMER_TYPE_TO_CUSTOMER);
        }
    }
    
    @Override
    public CustomerType createDomain(UserLogin userLogin, NameCategoryDto createdto) {
        checkMandatoryParams(createdto, createdto.getName());

        CustomerType domain = new CustomerType();
        domain.setDraft(true);

        initPOWhenCreate(CustomerType.class, userLogin, domain);

        updateDomain(userLogin, domain, createdto);

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, CustomerType domain, NameCategoryDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }
        
        if (repository.existsByName(userLogin.getClientId(), domain.getId(), createdto.getName())) {
            throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
        }
        
        checkMandatoryParams(createdto, createdto.getName());

        domain.setName(createdto.getName());
    }

    @Override
    public NameCategoryDto createSimpleDto(UserLogin userLogin, CustomerType domain) {
        return new NameCategoryDto(domain);
    }

    @Override
    public NameCategoryDto createDetailDto(UserLogin userLogin, CustomerType domain) {
        return createSimpleDto(userLogin, domain);
    }

}
