package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.DistributorCreateDto;
import com.viettel.backend.dto.DistributorDto;
import com.viettel.backend.dto.DistributorSimpleDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminDistributorService;
import com.viettel.backend.service.sub.CacheSubService;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class AdminDistributorServiceImpl extends
        BasicCategoryServiceImpl<Distributor, DistributorSimpleDto, DistributorDto, DistributorCreateDto> implements
        AdminDistributorService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private CacheSubService cacheSubService;

    @Override
    public BasicCategoryRepository<Distributor> getRepository() {
        return distributorRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }

    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }

    @Override
    protected void checkBeforeDelete(UserLogin userLogin, ObjectId id) {
        // No need to check anything, only draft distributor can be delete
    }

    @Override
    protected void checkBeforeSetActive(UserLogin userLogin, ObjectId id, boolean active) {
        if (userRepository.checkDistributorUsed(userLogin.getClientId(), id)) {
            throw new BusinessException(ExceptionCode.DISTRIBUTOR_LINK_TO_SALESMAN);
        }

        if (customerRepository.checkCustomerTypeUsed(userLogin.getClientId(), id)) {
            throw new BusinessException(ExceptionCode.DISTRIBUTOR_LINK_TO_CUSTOMER);
        }
    }

    @Override
    public Distributor createDomain(UserLogin userLogin, DistributorCreateDto createdto) {
        checkMandatoryParams(createdto, createdto.getName(), createdto.getSupervisorId());

        Distributor distributor = new Distributor();
        initPOWhenCreate(Distributor.class, userLogin, distributor);
        distributor.setDraft(true);

        distributor.setName(createdto.getName());
        distributor.setCode(codeGeneratorRepository.getDistributorCode(userLogin.getClientId().toString()));
        distributor.setAddress(createdto.getAddress());

        ObjectId supervisorId = ObjectIdUtils.getObjectId(createdto.getSupervisorId(), null);
        if (supervisorId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        User supervisor = userRepository.getById(userLogin.getClientId(), supervisorId);
        if (supervisor == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        distributor.setSupervisor(new UserEmbed(supervisor));

        return distributor;
    }

    @Override
    public void updateDomain(UserLogin userLogin, Distributor distributor, DistributorCreateDto createdto) {
        if (!distributor.isDraft()) {
            checkMandatoryParams(createdto, createdto.getSupervisorId());
        } else {
            checkMandatoryParams(createdto, createdto.getName(), createdto.getSupervisorId());

            distributor.setName(createdto.getName());
        }

        distributor.setAddress(createdto.getAddress());

        ObjectId supervisorId = ObjectIdUtils.getObjectId(createdto.getSupervisorId(), null);
        if (supervisorId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        User supervisor = userRepository.getById(userLogin.getClientId(), supervisorId);
        if (supervisor == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        distributor.setSupervisor(new UserEmbed(supervisor));
    }

    @Override
    public DistributorSimpleDto createSimpleDto(UserLogin userLogin, Distributor domain) {
        return new DistributorSimpleDto(domain);
    }

    @Override
    public DistributorDto createDetailDto(UserLogin userLogin, Distributor domain) {
        if (domain.getId() != null) {
            Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByDistributors(userLogin.getClientId(),
                    Arrays.asList(domain.getId()));

            if (salesmanIds != null && !salesmanIds.isEmpty()) {
                List<String> _salesmanIds = new ArrayList<String>(salesmanIds.size());
                for (ObjectId salesmanId : salesmanIds) {
                    _salesmanIds.add(salesmanId.toString());
                }
                return new DistributorDto(domain, _salesmanIds);
            }
        }

        return new DistributorDto(domain, null);
    }

    @Override
    public ObjectId create(UserLogin userLogin, DistributorCreateDto dto) {
        ObjectId distributorId = super.create(userLogin, dto);

        // // SET DISTRIBUTOR FOR SALESMAN
        // List<ObjectId> salesmanIds =
        // ObjectIdUtils.getObjectIdsIgnoreNull(dto.getSalesmanIds());
        // userRepository.updateSalesmenOfDistributor(userLogin.getClientId(),
        // distributorId, salesmanIds);

        // XXX need roleback here ?

        return distributorId;
    }

    @Override
    public ObjectId update(UserLogin userLogin, String _id, DistributorCreateDto createdto) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Distributor domain = getRepository().getByIdWithDraft(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        updateDomain(userLogin, domain, createdto);

        domain = getRepository().save(userLogin.getClientId(), domain);

        if (!domain.isDraft()) {
            // SET DISTRIBUTOR FOR SALESMAN
            List<ObjectId> newSalesmanIds = ObjectIdUtils.getObjectIdsIgnoreNull(createdto.getSalesmanIds());
            Set<ObjectId> oldSalesmanIds = userRepository.getSalesmanIdsByDistributors(userLogin.getClientId(),
                    Arrays.asList(domain.getId()));

            if (oldSalesmanIds != null && !oldSalesmanIds.isEmpty()) {
                Collection<ObjectId> intersection = null;
                if (newSalesmanIds != null) {
                    intersection = CollectionUtils.intersection(oldSalesmanIds, newSalesmanIds);
                }

                Collection<ObjectId> salemanIdsChangedDistributor = oldSalesmanIds;
                if (intersection != null && !intersection.isEmpty()) {
                    salemanIdsChangedDistributor = CollectionUtils.subtract(oldSalesmanIds, intersection);
                }

                if (salemanIdsChangedDistributor != null && !salemanIdsChangedDistributor.isEmpty()) {
                    Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(userLogin.getClientId(),
                            salemanIdsChangedDistributor);

                    routeRepository.clearSalesmanRoute(userLogin.getClientId(), salemanIdsChangedDistributor);
                    cacheSubService.reclaculateScheduleNumberPlannedCache(userLogin, routeIds);
                }
            }

            // XXX need roleback here ?

            userRepository.updateSalesmenOfDistributor(userLogin.getClientId(), domain.getId(), newSalesmanIds);

        }

        return domain.getId();
    }

}
