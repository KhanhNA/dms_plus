package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.District;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminDistrictService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class AdminDistrictServiceImpl extends
        BasicCategoryServiceImpl<District, NameCategoryDto, NameCategoryDto, NameCategoryDto> implements
        AdminDistrictService {

    private static final long serialVersionUID = -5771173067596328856L;
    
    @Autowired
    private DistrictRepository repository;

    @Override
    public BasicCategoryRepository<District> getRepository() {
        return repository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }
    
    @Override
    public District createDomain(UserLogin userLogin, NameCategoryDto createdto) {
        checkMandatoryParams(createdto, createdto.getName());

        District domain = new District();
        domain.setDraft(true);

        initPOWhenCreate(District.class, userLogin, domain);

        updateDomain(userLogin, domain, createdto);

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, District domain, NameCategoryDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }
        
        if (repository.existsByName(userLogin.getClientId(), domain.getId(), createdto.getName())) {
            throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
        }
        
        checkMandatoryParams(createdto, createdto.getName());

        domain.setName(createdto.getName());
    }

    @Override
    public NameCategoryDto createSimpleDto(UserLogin userLogin, District domain) {
        return new NameCategoryDto(domain);
    }

    @Override
    public NameCategoryDto createDetailDto(UserLogin userLogin, District domain) {
        return createSimpleDto(userLogin, domain);
    }

}
