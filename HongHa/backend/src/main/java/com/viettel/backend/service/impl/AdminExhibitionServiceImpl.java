package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.domain.embed.ExhibitionCategory;
import com.viettel.backend.domain.embed.ExhibitionItem;
import com.viettel.backend.dto.ExhibitionDto;
import com.viettel.backend.dto.ExhibitionDto.ExhibitionCategoryDto;
import com.viettel.backend.dto.ExhibitionDto.ExhibitionItemDto;
import com.viettel.backend.dto.ExhibitionSimpleDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.ExhibitionRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminExhibitionService;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class AdminExhibitionServiceImpl extends
        BasicCategoryServiceImpl<Exhibition, ExhibitionSimpleDto, ExhibitionDto, ExhibitionDto> implements
        AdminExhibitionService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private ExhibitionRepository exhibitionRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public BasicCategoryRepository<Exhibition> getRepository() {
        return exhibitionRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }

    @Override
    public Exhibition createDomain(UserLogin userLogin, ExhibitionDto createdto) {
        Exhibition domain = new Exhibition();
        initPOWhenCreate(Exhibition.class, userLogin, domain);
        domain.setDraft(true);

        updateDomain(userLogin, domain, createdto);

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, Exhibition domain, ExhibitionDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }

        checkMandatoryParams(createdto, createdto.getName(), createdto.getStartDate(), createdto.getEndDate(),
                createdto.getCategories());

        SimpleDate startDate = SimpleDate.createByIsoDate(createdto.getStartDate(), null);
        SimpleDate endDate = SimpleDate.createByIsoDate(createdto.getEndDate(), null);
        if (startDate == null || endDate == null || startDate.compareTo(endDate) > 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }
        
        domain.setName(createdto.getName());
        domain.setDescription(createdto.getDescription());
        domain.setStartDate(SimpleDate.createByIsoDate(createdto.getStartDate(), null));
        domain.setEndDate(SimpleDate.createByIsoDate(createdto.getEndDate(), null));

        List<ExhibitionCategory> categories = null;
        if (createdto.getCategories() != null) {
            categories = new ArrayList<ExhibitionCategory>(createdto.getCategories().size());
            for (ExhibitionCategoryDto exhibitionCategoryDto : createdto.getCategories()) {
                ExhibitionCategory category = new ExhibitionCategory();
                category.setId(ObjectIdUtils.getObjectId(exhibitionCategoryDto.getId(), new ObjectId()));
                category.setName(exhibitionCategoryDto.getName());

                List<ExhibitionItem> items = null;
                if (exhibitionCategoryDto.getItems() != null) {
                    items = new ArrayList<ExhibitionItem>(exhibitionCategoryDto.getItems().size());
                    for (ExhibitionItemDto exhibitionItemDto : exhibitionCategoryDto.getItems()) {
                        ExhibitionItem item = new ExhibitionItem();
                        item.setId(ObjectIdUtils.getObjectId(exhibitionItemDto.getId(), new ObjectId()));
                        item.setName(exhibitionItemDto.getName());
                        items.add(item);
                    }
                }
                category.setItems(items);

                categories.add(category);
            }
        }
        domain.setCategories(categories);
    }

    @Override
    public ExhibitionSimpleDto createSimpleDto(UserLogin userLogin, Exhibition domain) {
        return new ExhibitionSimpleDto(domain);
    }

    @Override
    public ExhibitionDto createDetailDto(UserLogin userLogin, Exhibition domain) {
        return new ExhibitionDto(domain);
    }

}
