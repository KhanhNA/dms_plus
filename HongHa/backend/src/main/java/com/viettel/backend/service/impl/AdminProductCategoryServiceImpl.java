package com.viettel.backend.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ProductCategory;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.ProductCategoryRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminProductCategoryService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class AdminProductCategoryServiceImpl extends
        BasicCategoryServiceImpl<ProductCategory, NameCategoryDto, NameCategoryDto, NameCategoryDto> implements
        AdminProductCategoryService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private ProductCategoryRepository repository;
    
    @Autowired
    private ProductRepository productRepository;

    @Override
    public BasicCategoryRepository<ProductCategory> getRepository() {
        return repository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }
    
    @Override
    protected void checkBeforeDelete(UserLogin userLogin, ObjectId id) {
        if (productRepository.checkProductCategoryUsed(userLogin.getClientId(), id)) {
            throw new BusinessException(ExceptionCode.PRODUCT_CATEGORY_LINK_TO_PRODUCT);
        }
    }

    @Override
    public ProductCategory createDomain(UserLogin userLogin, NameCategoryDto createdto) {
        checkMandatoryParams(createdto, createdto.getName());

        if (repository.existsByName(userLogin.getClientId(), null, createdto.getName())) {
            throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
        }
        
        ProductCategory domain = new ProductCategory();
        domain.setDraft(true);

        initPOWhenCreate(ProductCategory.class, userLogin, domain);

        domain.setName(createdto.getName());

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, ProductCategory domain, NameCategoryDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }
        
        if (repository.existsByName(userLogin.getClientId(), domain.getId(), createdto.getName())) {
            throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
        }
        
        checkMandatoryParams(createdto, createdto.getName());

        domain.setName(createdto.getName());
    }

    @Override
    public NameCategoryDto createSimpleDto(UserLogin userLogin, ProductCategory domain) {
        return new NameCategoryDto(domain);
    }

    @Override
    public NameCategoryDto createDetailDto(UserLogin userLogin, ProductCategory domain) {
        return createSimpleDto(userLogin, domain);
    }

}
