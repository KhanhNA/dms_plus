package com.viettel.backend.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.ProductCategory;
import com.viettel.backend.domain.UOM;
import com.viettel.backend.dto.ProductCreateDto;
import com.viettel.backend.dto.ProductDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.ProductCategoryRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.UOMRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminProductService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class AdminProductServiceImpl extends
        BasicCategoryServiceImpl<Product, ProductDto, ProductDto, ProductCreateDto> implements AdminProductService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UOMRepository uomRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private FileEngine fileEngine;

    @Override
    public BasicCategoryRepository<Product> getRepository() {
        return productRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }

    @Override
    public Product createDomain(UserLogin userLogin, ProductCreateDto createdto) {
        checkMandatoryParams(createdto, createdto.getCode(), createdto.getName(), createdto.getPhoto(),
                createdto.getUomId(), createdto.getPrice(), createdto.getOutput(), createdto.getProductCategoryId());

        if (productRepository.existsByName(userLogin.getClientId(), null, createdto.getName())) {
            throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
        }
        
        if (productRepository.existsByCode(userLogin.getClientId(), null, createdto.getCode())) {
            throw new BusinessException(ExceptionCode.SAME_CODE_EXIST);
        }
        
        Product product = new Product();
        product.setDraft(true);

        initPOWhenCreate(Product.class, userLogin, product);

        product.setCode(createdto.getCode());
        product.setName(createdto.getName());
        product.setPrice(createdto.getPrice());
        product.setOutput(createdto.getOutput());
        product.setDescription(createdto.getDescription());

        if (!fileEngine.exists(userLogin, createdto.getPhoto())) {
            throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
        }
        product.setPhoto(createdto.getPhoto());

        ObjectId uomId = ObjectIdUtils.getObjectId(createdto.getUomId(), null);
        if (uomId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        UOM uom = uomRepository.getById(userLogin.getClientId(), uomId);
        if (uom == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        product.setUom(uom);

        ObjectId productCategoryId = ObjectIdUtils.getObjectId(createdto.getProductCategoryId(), null);
        if (productCategoryId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        ProductCategory productCategory = productCategoryRepository.getById(userLogin.getClientId(), productCategoryId);
        if (productCategory == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        product.setProductCategory(productCategory);

        return product;
    }

    @Override
    public void updateDomain(UserLogin userLogin, Product product, ProductCreateDto createdto) {
        if (!product.isDraft()) {
            checkMandatoryParams(createdto, createdto.getPhoto(), createdto.getPrice(), createdto.getOutput());
        } else {
            checkMandatoryParams(createdto, createdto.getCode(), createdto.getName(), createdto.getPhoto(),
                    createdto.getUomId(), createdto.getPrice(), createdto.getOutput(), createdto.getProductCategoryId());

            if (productRepository.existsByName(userLogin.getClientId(), product.getId(), createdto.getName())) {
                throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
            }
            
            if (productRepository.existsByCode(userLogin.getClientId(), product.getId(), createdto.getCode())) {
                throw new BusinessException(ExceptionCode.SAME_CODE_EXIST);
            }
            
            product.setCode(createdto.getCode());
            product.setName(createdto.getName());

            ObjectId uomId = ObjectIdUtils.getObjectId(createdto.getUomId(), null);
            if (uomId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            UOM uom = uomRepository.getById(userLogin.getClientId(), uomId);
            if (uom == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            
            ObjectId productCategoryId = ObjectIdUtils.getObjectId(createdto.getProductCategoryId(), null);
            if (productCategoryId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            ProductCategory productCategory = productCategoryRepository.getById(userLogin.getClientId(), productCategoryId);
            if (productCategory == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            product.setProductCategory(productCategory);
            
            product.setUom(uom);
        }

        product.setPrice(createdto.getPrice());
        product.setOutput(createdto.getOutput());
        product.setDescription(createdto.getDescription());

        if (!fileEngine.exists(userLogin, createdto.getPhoto())) {
            throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
        }
        product.setPhoto(createdto.getPhoto());

    }

    @Override
    public ProductDto createSimpleDto(UserLogin userLogin, Product domain) {
        return new ProductDto(domain, getClientCondig(userLogin).getDefaultProductPhoto());
    }

    @Override
    public ProductDto createDetailDto(UserLogin userLogin, Product domain) {
        return createSimpleDto(userLogin, domain);
    }

}
