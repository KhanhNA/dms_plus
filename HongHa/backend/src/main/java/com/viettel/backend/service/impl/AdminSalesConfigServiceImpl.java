package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.dto.SalesConfigDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.SalesConfigRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminSalesConfigService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class AdminSalesConfigServiceImpl extends AbstractService implements AdminSalesConfigService {

    private static final long serialVersionUID = -2527783300704110407L;

    @Autowired
    private SalesConfigRepository salesConfigRepository;

    @Override
    public SalesConfigDto getSalesConfigDto(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        SalesConfig SalesConfig = salesConfigRepository.getSalesConfig(userLogin.getClientId());
        if (SalesConfig == null) {
            return null;
        }

        SalesConfigDto dto = new SalesConfigDto(userLogin, SalesConfig);
        return dto;
    }

    @Override
    public String save(UserLogin userLogin, SalesConfigDto dto) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        checkMandatoryParams(dto, dto.getVisitDurationKPI(), dto.getVisitDistanceKPI(), dto.getMeetReqRate(),
                dto.isCanEditCustomerLocation());

        SalesConfig salesConfig = salesConfigRepository.getSalesConfig(userLogin.getClientId());
        if (salesConfig == null) {
            salesConfig = new SalesConfig();
            initPOWhenCreate(SalesConfig.class, userLogin, salesConfig);
        }

        salesConfig.setVisitDurationKPI(dto.getVisitDurationKPI());
        salesConfig.setVisitDistanceKPI(dto.getVisitDistanceKPI());
        salesConfig.setMeetReqRate(dto.getMeetReqRate());
        salesConfig.setCanEditCustomerLocation(dto.isCanEditCustomerLocation());

        salesConfig = salesConfigRepository.save(userLogin.getClientId(), salesConfig);

        return salesConfig.getId().toString();
    }

}
