package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserDto;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminUserService;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;
import com.viettel.backend.util.PasswordUtils;

@Service
public class AdminUserServiceImpl extends BasicCategoryServiceImpl<User, UserSimpleDto, UserDto, UserDto> implements
        AdminUserService {

    private static final long serialVersionUID = -5771173067596328856L;
    
    private static final Sort DEFAULT_SORT = new Sort(
            new Order(Direction.DESC, PO.COLUMNNAME_DRAFT),
            new Order(Direction.ASC, User.COLUMNNAME_USERNAME)
            );

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    protected Sort getSort() {
        return DEFAULT_SORT;
    }
    
    @Override
    public BasicCategoryRepository<User> getRepository() {
        return userRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN);
    }

    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.ADMIN_ONLY;
    }

    @Override
    protected void checkBeforeDelete(UserLogin userLogin, ObjectId id) {
        // No need to check anything, only draft users can be delete
        // But they cannot login to the system, so they cannot make change
    }
    
    @Override
    protected void checkBeforeSetActive(UserLogin userLogin, ObjectId id, boolean active) {
        User user = userRepository.getByIdWithDeactivated(userLogin.getClientId(), id);
        if (user == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        if (user.getId().equals(userLogin.getUserId())) {
            throw new BusinessException(ExceptionCode.CANNOT_CHANGE_CURRENT_USER);
        }

        if (user.getRoles().contains(HardCodeUtils.ROLE_SUPERVISOR)) {
            if (distributorRepository.checkSupervisorUsed(userLogin.getClientId(), id)) {
                throw new BusinessException(ExceptionCode.USER_LINK_TO_DISTRIBUTOR);
            }
        } else if (user.getRoles().contains(HardCodeUtils.ROLE_SALESMAN)) {
            if (user.getDistributor() != null) {
                throw new BusinessException(ExceptionCode.USER_LINK_TO_DISTRIBUTOR);
            }
        }
    }
    
    @Override
    protected void afterSetActive(UserLogin userLogin, ObjectId id, boolean active) {
        // Revoke all access token
        // Invalidate all session
    }

    @Override
    public User createDomain(UserLogin userLogin, UserDto createdto) {
        checkMandatoryParams(createdto, createdto.getUsername(), createdto.getFullname(), createdto.getRole());

        User user = new User();
        user.setDraft(true);
        user.setPassword(PasswordUtils.getDefaultPassword());

        initPOWhenCreate(User.class, userLogin, user);

        user.setUsername(createdto.getUsername());
        user.setFullname(createdto.getFullname());
        user.setRoles(Arrays.asList(createdto.getRole()));

        return user;
    }

    @Override
    public void updateDomain(UserLogin userLogin, User domain, UserDto createdto) {
        if (!domain.isDraft()) {
            if (domain.getRoles().contains(HardCodeUtils.ROLE_DISTRIBUTOR)) {
                checkMandatoryParams(createdto);

                Distributor distributor = getMadatoryPO(userLogin, createdto.getDistributorId(), distributorRepository);

                domain.setDistributor(new DistributorEmbed(distributor));
            }

        } else {
            checkMandatoryParams(createdto, createdto.getUsername(), createdto.getFullname(), createdto.getRole());

            domain.setUsername(createdto.getUsername());
            domain.setFullname(createdto.getFullname());
            domain.setRoles(Arrays.asList(createdto.getRole()));
        }
    }
    
    @Override
    public void afterUpdate(UserLogin userLogin, User domain, UserDto createdto) {
        if (!domain.isDraft()) {
            if (domain.getRoles().contains(HardCodeUtils.ROLE_STORE_CHECKER)) {
                List<ObjectId> salesmanIds = Collections.emptyList();
                if (createdto != null && createdto.getSalesmanIds() != null && !createdto.getSalesmanIds().isEmpty()) {
                    salesmanIds = getMadatoryPOIds(userLogin, createdto.getSalesmanIds(), userRepository);
                }
                userRepository.updateSalesmenOfStoreChecker(userLogin.getClientId(), domain.getId(), salesmanIds);
            }
        }
        
    }

    @Override
    public UserSimpleDto createSimpleDto(UserLogin userLogin, User domain) {
        return new UserSimpleDto(domain);
    }

    @Override
    public UserDto createDetailDto(UserLogin userLogin, User domain) {
        UserDto userDto = new UserDto(domain);
        
        if (domain.getId() != null) {
            Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByStoreCheckers(userLogin.getClientId(),
                    Arrays.asList(domain.getId()));

            if (salesmanIds != null && !salesmanIds.isEmpty()) {
                List<String> _salesmanIds = new ArrayList<String>(salesmanIds.size());
                for (ObjectId salesmanId : salesmanIds) {
                    _salesmanIds.add(salesmanId.toString());
                }
                userDto.setSalesmanIds(_salesmanIds);
            }
        }

        return userDto;
        
    }

    @Override
    public void resetPassword(UserLogin userLogin, String id) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        ObjectId userId = ObjectIdUtils.getObjectId(id, null);
        if (userId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        User user = userRepository.getById(userLogin.getClientId(), userId);
        if (user == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        user.setPassword(PasswordUtils.getDefaultPassword());

        userRepository.save(userLogin.getClientId(), user);
    }

    @Override
    public ListJson<UserSimpleDto> getSalesmen(UserLogin userLogin, String _distributorId) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        List<User> users = null;
        if (_distributorId == null) {
            users = userRepository.getUsersByRole(userLogin.getClientId(), HardCodeUtils.ROLE_SALESMAN);
            if (users == null || users.isEmpty()) {
                return ListJson.<UserSimpleDto> emptyList();
            }
        } else {
            ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
            if (distributorId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            if (!distributorRepository.exists(userLogin.getClientId(), distributorId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            users = userRepository.getSalesmenByDistributors(userLogin.getClientId(), Arrays.asList(distributorId));
            if (users == null || users.isEmpty()) {
                return ListJson.<UserSimpleDto> emptyList();
            }
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(users.size());
        for (User user : users) {
            dtos.add(new UserSimpleDto(user));
        }

        return new ListJson<UserSimpleDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<UserSimpleDto> getSupervisors(UserLogin userLogin) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        List<User> users = userRepository.getUsersByRole(userLogin.getClientId(), HardCodeUtils.ROLE_SUPERVISOR);
        if (users == null || users.isEmpty()) {
            return ListJson.<UserSimpleDto> emptyList();
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(users.size());
        for (User user : users) {
            dtos.add(new UserSimpleDto(user));
        }

        return new ListJson<UserSimpleDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<UserSimpleDto> getSalesmanByDistributorWithAvailable(UserLogin userLogin, String _distributorId) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
        if (distributorId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        if (!distributorRepository.exists(userLogin.getClientId(), distributorId)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        List<User> users = userRepository.getSalesmenByDistributors(userLogin.getClientId(),
                Arrays.asList(distributorId, null));
        if (users == null || users.isEmpty()) {
            return ListJson.<UserSimpleDto> emptyList();
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(users.size());
        for (User user : users) {
            dtos.add(new UserSimpleDto(user));
        }

        return new ListJson<UserSimpleDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<UserSimpleDto> getSalesmanByStoreCheckerWithAvailable(UserLogin userLogin, String _storeCheckerId) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        ObjectId storeCheckerId = getMadatoryPOId(userLogin, _storeCheckerId, userRepository);

        List<User> users = userRepository.getSalesmenByStoreCheckers(userLogin.getClientId(),
                Arrays.asList(storeCheckerId, null));
        if (users == null || users.isEmpty()) {
            return ListJson.<UserSimpleDto> emptyList();
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(users.size());
        for (User user : users) {
            dtos.add(new UserSimpleDto(user));
        }

        return new ListJson<UserSimpleDto>(dtos, (long) dtos.size());
    }

}
