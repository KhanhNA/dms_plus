package com.viettel.backend.service.impl;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.dto.VisitInfoDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AdminVisitService;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class AdminVisitServiceImpl extends AbstractSupervisorService implements AdminVisitService {

    private static final long serialVersionUID = 4822917139294672054L;

    @Autowired
    private VisitRepository visitRepository;

    @Override
    public VisitInfoDto getVisit(UserLogin userLogin, String id) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            throw new BusinessException(ExceptionCode.ADMIN_ONLY);
        }

        ObjectId visitId = ObjectIdUtils.getObjectId(id, null);
        if (visitId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Visit visit = visitRepository.getById(userLogin.getClientId(), visitId);

        if (visit == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        VisitInfoDto visitInfoDto = new VisitInfoDto(visit, getClientCondig(userLogin).getDefaultProductPhoto());

        return visitInfoDto;
    }

}
