package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ChangePasswordDto;
import com.viettel.backend.dto.UserLoginDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.AuthenticateRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.AuthenticationService;
import com.viettel.backend.util.PasswordUtils;

@Service
public class AuthenticateServiceImpl extends AbstractService implements AuthenticationService {

    private static final long serialVersionUID = -7521814129330891905L;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private AuthenticateRepository authenticateRepository;

    @Override
    public User getUserByUsername(String username) {
        return authenticateRepository.findByUserName(username);
    }

    @Override
    public UserLoginDto getUserLoginDto(UserLogin userLogin) {
        User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());

        if (user == null) {
            throw new UnsupportedOperationException("current user not found");
        }

        return new UserLoginDto(user);
    }
    
    @Override
    public void changePassword(UserLogin userLogin, ChangePasswordDto dto) {
        checkMandatoryParams(dto, dto.getOldPassword(), dto.getNewPassword());
        
        User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
        
        if (user == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        } 
        
        if (PasswordUtils.matches(dto.getOldPassword(), user.getPassword())) {
            user.setPassword(PasswordUtils.encode(dto.getNewPassword()));
            userRepository.save(userLogin.getClientId(), user);
        } else {
            throw new BusinessException(ExceptionCode.INVALID_OLD_PASSWORD); 
        }
    }

}
