package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;

import com.viettel.backend.domain.NameCategoryByDistributor;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.BasicNameCategoryByDistributorService;
import com.viettel.backend.util.ObjectIdUtils;

public abstract class BasicNameCategoryByDistributorServiceImpl<DOMAIN extends NameCategoryByDistributor, 
                                                                SIMPLEDTO extends NameCategoryDto, 
                                                                DETAILDTO extends SIMPLEDTO, 
                                                                CREATEDTO extends NameCategoryDto>
        extends BasicCategoryServiceImpl<DOMAIN, SIMPLEDTO, DETAILDTO, CREATEDTO> 
        implements BasicNameCategoryByDistributorService<DOMAIN, SIMPLEDTO, DETAILDTO, CREATEDTO> {

    private static final long serialVersionUID = 1L;

    protected abstract Collection<ObjectId> getDistributorIdsForFilter(UserLogin userLogin);

    private void checkDomainDistributor(UserLogin userLogin, DOMAIN domain) {
        if (domain != null) {
            Collection<ObjectId> distributorIdsForFilter = getDistributorIdsForFilter(userLogin);
            if (distributorIdsForFilter != null) {
                if (domain.getDistributor() == null) {
                    throw new BusinessException(ExceptionCode.NOT_FOUND);
                }
                
                if (!distributorIdsForFilter.contains(domain.getDistributor().getId())) {
                    throw new BusinessException(ExceptionCode.NOT_FOUND);
                }
            }
        }
    }
    
    protected void checkDistributor(UserLogin userLogin, ObjectId distributorId) {
        Collection<ObjectId> distributorIdsForFilter = getDistributorIdsForFilter(userLogin);
        if (distributorIdsForFilter != null) {
            if (!distributorIdsForFilter.contains(distributorId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
        }
    }
    
    /** Get all active PO */
    @Override
    public ListJson<SIMPLEDTO> getAll(UserLogin userLogin) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        List<DOMAIN> domains = getRepository().getAll(userLogin.getClientId(), getDistributorIdsForFilter(userLogin));
        if (domains == null || domains.isEmpty()) {
            return ListJson.<SIMPLEDTO> emptyList();
        }

        List<SIMPLEDTO> dtos = new ArrayList<SIMPLEDTO>();
        for (DOMAIN domain : domains) {
            dtos.add(createSimpleDto(userLogin, domain));
        }

        return new ListJson<SIMPLEDTO>(dtos, Long.valueOf(dtos.size()));
    }

    @Override
    public ListJson<SIMPLEDTO> getList(UserLogin userLogin, String search, Boolean active, Boolean draft,
            Pageable pageable) {
        throw new UnsupportedOperationException("wrong method");
    }
    
    @Override
    public ListJson<SIMPLEDTO> getList(UserLogin userLogin, String _distributorId, String search, Boolean active,
            Boolean draft, Pageable pageable) {
        Collection<ObjectId> distributorIds = getDistributorIdsForFilter(userLogin);
        if (_distributorId != null) {
            ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
            if (distributorId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            
            if (distributorIds != null && !distributorIds.contains(distributorId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            
            distributorIds = Collections.singleton(distributorId);
        }
        
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        List<DOMAIN> domains = getRepository().getListWithDraft(userLogin.getClientId(), distributorIds, search, active, draft,
                pageable, getSort());
        if (domains == null || domains.isEmpty()) {
            return ListJson.<SIMPLEDTO> emptyList();
        }

        List<SIMPLEDTO> dtos = new ArrayList<SIMPLEDTO>();
        for (DOMAIN domain : domains) {
            dtos.add(createSimpleDto(userLogin, domain));
        }

        long size = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || size == pageable.getPageSize()) {
                size = getRepository().countWithDraft(userLogin.getClientId(), distributorIds, search, active, draft);
            }
        }

        return new ListJson<SIMPLEDTO>(dtos, size);
    }

    @Override
    public DETAILDTO getById(UserLogin userLogin, String _id) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        DOMAIN domain = getRepository().getByIdWithDraft(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }
        
        checkDomainDistributor(userLogin, domain);

        return createDetailDto(userLogin, domain);
    }

    @Override
    public ObjectId create(UserLogin userLogin, CREATEDTO dto) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        DOMAIN domain = createDomain(userLogin, dto);
        
        checkDomainDistributor(userLogin, domain);

        domain = getRepository().save(userLogin.getClientId(), domain);

        return domain.getId();
    }

    @Override
    public ObjectId update(UserLogin userLogin, String _id, CREATEDTO createdto) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        DOMAIN domain = getRepository().getByIdWithDraft(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }
        
        checkDomainDistributor(userLogin, domain);

        updateDomain(userLogin, domain, createdto);
        
        checkDomainDistributor(userLogin, domain);

        domain = getRepository().save(userLogin.getClientId(), domain);

        afterUpdate(userLogin, domain, createdto);

        return domain.getId();
    }

    @Override
    public boolean enable(UserLogin userLogin, String _id) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        
        DOMAIN domain = getRepository().getByIdWithDraft(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }
        
        checkDomainDistributor(userLogin, domain);

        getRepository().enable(userLogin.getClientId(), id);

        return true;
    }

    @Override
    public boolean delete(UserLogin userLogin, String _id) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        ObjectId id = ObjectIdUtils.getObjectId(_id, null);
        if (id == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        
        DOMAIN domain = getRepository().getByIdWithDraft(userLogin.getClientId(), id);
        if (domain == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }
        
        checkDomainDistributor(userLogin, domain);

        checkBeforeDelete(userLogin, id);

        return this.getRepository().delete(userLogin.getClientId(), id);
    }

    @Override
    public void setActive(UserLogin userLogin, String _id, boolean active) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(noPermissionExceptionCode());
        }

        DOMAIN domain = getMadatoryPOWithDeactivated(userLogin, _id, getRepository());
        
        checkDomainDistributor(userLogin, domain);
        
        checkBeforeSetActive(userLogin, domain.getId(), active);

        if (this.getRepository().setActive(userLogin.getClientId(), domain.getId(), active)) {
            afterSetActive(userLogin, domain.getId(), active);
        }
    }
    
}
