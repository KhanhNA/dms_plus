package com.viettel.backend.service.impl;

import java.io.Serializable;

import com.viettel.backend.dto.ClientConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface ClientConfigService extends Serializable {

    public ClientConfigDto getClientConfig(UserLogin userLogin);
    
}
