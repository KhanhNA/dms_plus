package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.PO;
import com.viettel.backend.dto.ClientConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;

@Service
public class ClientConfigServiceImpl implements ClientConfigService {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    private ClientConfigRepository clientConfigRepository;
    
    @Override
    public ClientConfigDto getClientConfig(UserLogin userLogin) {
        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        return new ClientConfigDto(clientConfig);
    }
    
}
