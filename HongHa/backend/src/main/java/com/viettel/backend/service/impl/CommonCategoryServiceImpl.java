package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.domain.District;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.ProductCategory;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.dto.ProductDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.repository.ProductCategoryRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.service.CommonCategoryService;

@Service
public class CommonCategoryServiceImpl extends AbstractService implements CommonCategoryService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private ProductCategoryRepository productCategoryRepository;
    
    @Autowired
    private CustomerTypeRepository customerTypeRepository;
    
    @Autowired
    private DistrictRepository districtRepository;
    
    @Override
    public ListJson<ProductDto> getProducts(UserLogin userLogin) {
        List<Product> products = productRepository.getAll(userLogin.getClientId(), null);
        
        if (products == null || products.isEmpty()) {
            return ListJson.emptyList();
        }
        
        ClientConfig clientConfig = getClientCondig(userLogin);
        
        List<ProductDto> dtos = new ArrayList<ProductDto>(products.size());
        for (Product product : products) {
            dtos.add(new ProductDto(product, clientConfig.getDefaultProductPhoto()));
        }
        
        return new ListJson<ProductDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<NameCategoryDto> getProductCategories(UserLogin userLogin) {
        List<ProductCategory> productCategories = productCategoryRepository.getAll(userLogin.getClientId(), null);
        
        if (productCategories == null || productCategories.isEmpty()) {
            return ListJson.emptyList();
        }
        
        List<NameCategoryDto> dtos = new ArrayList<NameCategoryDto>(productCategories.size());
        for (ProductCategory productCategory : productCategories) {
            dtos.add(new NameCategoryDto(productCategory));
        }
        
        return new ListJson<NameCategoryDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<NameCategoryDto> getCustomerTypes(UserLogin userLogin) {
        List<CustomerType> customerTypes = customerTypeRepository.getAll(userLogin.getClientId(), null);
        
        if (customerTypes == null || customerTypes.isEmpty()) {
            return ListJson.emptyList();
        }
        
        List<NameCategoryDto> dtos = new ArrayList<NameCategoryDto>(customerTypes.size());
        for (CustomerType customerType : customerTypes) {
            dtos.add(new NameCategoryDto(customerType));
        }
        
        return new ListJson<NameCategoryDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<NameCategoryDto> getDistricts(UserLogin userLogin) {
        List<District> districts = districtRepository.getAll(userLogin.getClientId(), null);
        
        if (districts == null || districts.isEmpty()) {
            return ListJson.emptyList();
        }
        
        List<NameCategoryDto> dtos = new ArrayList<NameCategoryDto>(districts.size());
        for (District district : districts) {
            dtos.add(new NameCategoryDto(district));
        }
        
        return new ListJson<NameCategoryDto>(dtos, (long) dtos.size());
    }
    
}
