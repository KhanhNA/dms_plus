package com.viettel.backend.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.map.MultiKeyMap;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.domain.ExhibitionRating;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.domain.Survey;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.domain.embed.ExhibitionCategory;
import com.viettel.backend.domain.embed.ExhibitionItem;
import com.viettel.backend.domain.embed.ExhibitionRatingItem;
import com.viettel.backend.domain.embed.SurveyAnswer;
import com.viettel.backend.domain.embed.SurveyOption;
import com.viettel.backend.domain.embed.SurveyQuestion;
import com.viettel.backend.dto.ExhibitionReportDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.dto.SurveyResultDto;
import com.viettel.backend.dto.VisitReportDailyDto;
import com.viettel.backend.dto.VisitReportDto;
import com.viettel.backend.dto.embed.SurveyOptionResultDto;
import com.viettel.backend.dto.embed.SurveyQuestionResultDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.ExhibitionRatingRepository;
import com.viettel.backend.repository.ExhibitionRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.SurveyAnswerRepository;
import com.viettel.backend.repository.SurveyRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.CommonReportService;
import com.viettel.backend.service.sub.VisitSubService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class CommonReportSerivceImpl extends AbstractSupervisorService implements CommonReportService {

    private static final long serialVersionUID = 5686134592376322356L;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private SurveyAnswerRepository surveyAnswerRepository;

    @Autowired
    private ExhibitionRepository exhibitionRepository;

    @Autowired
    private ExhibitionRatingRepository exhibitionRatingRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private VisitSubService visitSubService;

    private boolean checkRolesAllowed(UserLogin userLogin) {
        if (userLogin.getRoles() == null || userLogin.getRoles().isEmpty()) {
            return false;
        }

        return Arrays.asList(HardCodeUtils.ROLE_ADMIN, HardCodeUtils.ROLE_SUPERVISOR, HardCodeUtils.ROLE_DISTRIBUTOR)
                .contains(userLogin.getRoles().get(0));
    }

    private Collection<ObjectId> getSalesmanIds(UserLogin userLogin, String salesmanId) {
        if (salesmanId == null) {
            if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
                Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                        userLogin.getClientId(), Arrays.asList(userLogin.getUserId()));

                return userRepository.getSalesmanIdsByDistributors(userLogin.getClientId(), distributorIds);
            } else {
                return userRepository.getUserIdsByRole(userLogin.getClientId(), HardCodeUtils.ROLE_SALESMAN);
            }
        } else {
            if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
                User salesman = checkSupervisorSalesman(userLogin, salesmanId);
                return Arrays.asList(salesman.getId());
            } else {
                User salesman = getMadatoryPO(userLogin, salesmanId, userRepository);
                return Arrays.asList(salesman.getId());
            }
        }
    }

    @Override
    public VisitReportDailyDto getVisitReportDaily(UserLogin userLogin, String salesmanId) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Collection<ObjectId> salesmanIds = getSalesmanIds(userLogin, salesmanId);

        Period todayPeriod = DateTimeUtils.getPeriodToday();

        Sort sort = new Sort(Direction.DESC, Visit.COLUMNNAME_END_TIME_VALUE);

        Collection<Visit> visits = visitRepository.getVisitedsBySalesmen(userLogin.getClientId(), salesmanIds,
                todayPeriod, sort);
        long numberCustomerForVisitToday = visitSubService.getNumberVisitPlanned(userLogin, todayPeriod, salesmanIds);

        VisitReportDailyDto dto = new VisitReportDailyDto(userLogin, visits, getClientCondig(userLogin)
                .getDefaultProductPhoto(), (int) numberCustomerForVisitToday);

        return dto;
    }

    @Override
    public ListJson<VisitReportDto> getSalesmanVisitReport(UserLogin userLogin, String salesmanId,
            String fromDateIsoDate, String toDateIsoDate) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ObjectId clientId = userLogin.getClientId();

        SimpleDate fromDate = SimpleDate.createByIsoDate(fromDateIsoDate, null);
        SimpleDate toDate = SimpleDate.createByIsoDate(toDateIsoDate, null);
        toDate = DateTimeUtils.addDays(toDate, 1);
        if (fromDate == null || toDate == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        if (fromDate.compareTo(toDate) >= 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }
        Period period = new Period(fromDate, toDate);

        ClientConfig clientConfig = getClientCondig(userLogin);

        User salesman = null;
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            salesman = checkSupervisorSalesman(userLogin, salesmanId);
        } else {
            salesman = getMadatoryPO(userLogin, salesmanId, userRepository);
        }

        Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(clientId,
                Collections.singleton(salesman.getId()));

        List<Customer> customers = customerRepository.getCustomersByRoute(clientId, routeIds, null, null, null,
                null);
        if (customers == null || customers.isEmpty()) {
            return ListJson.emptyList();
        }

        // DANH SACH TUYEN TRONG KHOANG THOI GIAN
        MultiKeyMap<Integer, Integer> numberDayByDayAndWeek = visitSubService.getNumberDayByDayAndWeek(clientConfig,
                period);

        List<ObjectId> customerIds = new ArrayList<ObjectId>(customers.size());
        HashMap<ObjectId, VisitReportDto> reportVisitByCustomer = new HashMap<ObjectId, VisitReportDto>();
        List<VisitReportDto> reportVisitMonthlys = new ArrayList<VisitReportDto>(customers.size());
        for (Customer customer : customers) {
            VisitReportDto dto = new VisitReportDto();
            dto.setCustomer(customer.getName());
            dto.setCustomerId(customer.getId().toString());

            int numberVisitPlan = 0;
            CustomerSchedule customerSchedule = customer.getSchedule();
            if (customerSchedule != null) {
                List<CustomerScheduleItem> items = customerSchedule.getItems();
                if (items != null && !items.isEmpty()) {
                    for (CustomerScheduleItem item : items) {
                        List<Integer> weeks = item.getWeeks();
                        List<Integer> days = item.getDays();

                        if (weeks != null && !weeks.isEmpty() && days != null && !days.isEmpty()) {

                            for (int week : weeks) {
                                for (int day : days) {
                                    Integer tmpNumberVisitPlan = numberDayByDayAndWeek.get(week, day);
                                    tmpNumberVisitPlan = tmpNumberVisitPlan == null ? 0 : tmpNumberVisitPlan;

                                    numberVisitPlan = numberVisitPlan + tmpNumberVisitPlan;
                                }
                            }

                        }
                    }
                }
            }

            dto.setNumberVisitPlan(numberVisitPlan);

            customerIds.add(customer.getId());
            reportVisitMonthlys.add(dto);
            reportVisitByCustomer.put(customer.getId(), dto);
        }

        Sort sort = new Sort(Direction.DESC, Visit.COLUMNNAME_END_TIME_VALUE);
        List<Visit> visits = visitRepository.getVisitedsBySalesmen(clientId, Arrays.asList(salesman.getId()), period,
                sort);
        if (visits != null && !visits.isEmpty()) {
            for (Visit visit : visits) {
                VisitReportDto dto = reportVisitByCustomer.get(visit.getCustomer().getId());
                if (dto == null) {
                    continue;
                }

                dto.setNumberVisited(dto.getNumberVisited() + 1);

                if (visit.isClosed()) {
                    dto.setNumberVisitClosed(dto.getNumberVisitClosed() + 1);
                } else {
                    if (visit.isErrorDuration()) {
                        dto.setNumberVisitErrorTime(dto.getNumberVisitErrorTime() + 1);
                    } else {
                        dto.setNumberVisitNoErrorTime(dto.getNumberVisitNoErrorTime() + 1);
                    }
                }

                if (visit.getDistance() == null) {
                    dto.setNumberVisitUnlocated(dto.getNumberVisitUnlocated() + 1);
                } else {
                    if (visit.getLocationStatus() == Visit.LOCATION_STATUS_LOCATED) {
                        dto.setNumberVisitInRange(dto.getNumberVisitInRange() + 1);
                    } else {
                        dto.setNumberVisitOutRange(dto.getNumberVisitOutRange() + 1);
                    }
                }
            }
        }

        return new ListJson<VisitReportDto>(reportVisitMonthlys, (long) reportVisitMonthlys.size());
    }

    @Override
    public SurveyResultDto getSurveyReport(UserLogin userLogin, String _surveyId) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ObjectId surveyId = ObjectIdUtils.getObjectId(_surveyId, null);
        if (surveyId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Survey survey = surveyRepository.getById(userLogin.getClientId(), surveyId);
        if (survey == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        List<SurveyAnswer> answers = null;
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
            Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                    distributorIds, false, null, null);

            answers = surveyAnswerRepository
                    .getSurveyAnswersByCustomers(userLogin.getClientId(), surveyId, customerIds);
        } else {
            answers = surveyAnswerRepository.getSurveyAnswers(userLogin.getClientId(), surveyId);
        }

        HashMap<ObjectId, Integer> resultByOptionId = new HashMap<ObjectId, Integer>();
        if (answers != null) {
            for (SurveyAnswer answer : answers) {
                if (answer.getOptions() != null) {
                    for (ObjectId optionId : answer.getOptions()) {
                        Integer result = resultByOptionId.get(optionId);
                        if (result == null) {
                            result = 0;
                        }
                        result = result + 1;
                        resultByOptionId.put(optionId, result);
                    }
                }
            }
        }

        SurveyResultDto surveyResultDto = new SurveyResultDto();
        surveyResultDto.setId(survey.getId().toString());
        surveyResultDto.setName(survey.getName());
        surveyResultDto.setStartDate(survey.getStartDate().getIsoDate());
        surveyResultDto.setEndDate(survey.getEndDate().getIsoDate());
        surveyResultDto.setRequired(survey.isRequired());

        if (survey.getQuestions() != null) {
            for (SurveyQuestion question : survey.getQuestions()) {
                SurveyQuestionResultDto questionResultDto = new SurveyQuestionResultDto();
                questionResultDto.setId(question.getId().toString());
                questionResultDto.setName(question.getName());
                questionResultDto.setMultipleChoice(question.isMultipleChoice());
                questionResultDto.setRequired(question.isRequired());

                if (question.getOptions() != null) {
                    for (SurveyOption option : question.getOptions()) {
                        SurveyOptionResultDto optionResultDto = new SurveyOptionResultDto();
                        optionResultDto.setId(option.getId().toString());
                        optionResultDto.setName(option.getName());
                        Integer result = resultByOptionId.get(option.getId());
                        if (result != null) {
                            optionResultDto.setResult(result);
                        }

                        questionResultDto.addOption(optionResultDto);
                    }
                }

                surveyResultDto.addQuestion(questionResultDto);
            }
        }

        return surveyResultDto;
    }

    @Override
    public byte[] exportSurveyReport(UserLogin userLogin, String _surveyId) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ObjectId surveyId = ObjectIdUtils.getObjectId(_surveyId, null);
        if (surveyId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Survey survey = surveyRepository.getById(userLogin.getClientId(), surveyId);
        if (survey == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        List<SurveyAnswer> answers = null;
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
            Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                    distributorIds, false, null, null);

            answers = surveyAnswerRepository
                    .getSurveyAnswersByCustomers(userLogin.getClientId(), surveyId, customerIds);
        } else {
            answers = surveyAnswerRepository.getSurveyAnswers(userLogin.getClientId(), surveyId);
        }

        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Survey");

        if (answers == null) {
            XSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("Empty");
        } else {
            XSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("Time");
            row.createCell(1).setCellValue("Customer");
            row.createCell(2).setCellValue("Salesman");
            if (survey.getQuestions() != null) {
                int index = 3;

                for (SurveyQuestion question : survey.getQuestions()) {
                    row.createCell(index).setCellValue(question.getName());
                    index++;
                }
            }

            int answerIndex = 1;
            for (SurveyAnswer answer : answers) {
                row = sheet.createRow(answerIndex);
                row.createCell(0).setCellValue(answer.getVisit().getEndTime().format("dd/MM/yyyy HH:mm"));
                row.createCell(1).setCellValue(answer.getVisit().getCustomer().getName());
                row.createCell(2).setCellValue(answer.getVisit().getSalesman().getFullname());

                if (survey.getQuestions() != null) {
                    int index = 3;

                    for (SurveyQuestion question : survey.getQuestions()) {
                        StringBuilder optionsDisplay = new StringBuilder();
                        boolean isFirst = true;
                        if (question.getOptions() != null) {
                            for (SurveyOption option : question.getOptions()) {
                                if (answer.getOptions() != null && answer.getOptions().contains(option.getId())) {
                                    if (!isFirst) {
                                        optionsDisplay.append(", ");
                                    }
                                    isFirst = false;

                                    optionsDisplay.append(option.getName());
                                }
                            }
                        }

                        row.createCell(index).setCellValue(optionsDisplay.toString());

                        index++;
                    }
                }

                answerIndex++;
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            workbook.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ExhibitionReportDto getExhibitionReport(UserLogin userLogin, String _exhibitionId) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ObjectId exhibitionId = ObjectIdUtils.getObjectId(_exhibitionId, null);
        if (exhibitionId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Exhibition exhibition = exhibitionRepository.getById(userLogin.getClientId(), exhibitionId);
        if (exhibition == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        int numberOfCustomers = 0;
        List<ExhibitionRating> ratings = null;
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
            Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                    distributorIds, false, null, null);

            if (customerIds == null || customerIds.isEmpty()) {
                return new ExhibitionReportDto(exhibition, 0, 0);
            }

            numberOfCustomers = customerIds.size();
            ratings = exhibitionRatingRepository.getExhibitionRatingsByCustomers(userLogin.getClientId(),
                    Arrays.asList(exhibitionId), customerIds);
            if (ratings == null) {
                ratings = Collections.emptyList();
            }
        } else {
            numberOfCustomers = (int) customerRepository.count(userLogin.getClientId(), null);
            if (numberOfCustomers == 0) {
                return new ExhibitionReportDto(exhibition, 0, 0);
            }

            ratings = exhibitionRatingRepository.getExhibitionRatings(userLogin.getClientId(),
                    Arrays.asList(exhibitionId));
            if (ratings == null) {
                ratings = Collections.emptyList();
            }
        }

        Map<ObjectId, List<Float>> rateByCustomer = new HashMap<ObjectId, List<Float>>();
        for (ExhibitionRating rating : ratings) {
            // If no item, skip it
            if (rating.getItems() == null || rating.getItems().isEmpty()) {
                continue;
            }

            List<Float> customerPoints = rateByCustomer.get(rating.getCustomer().getId());
            if (customerPoints == null) {
                customerPoints = new LinkedList<Float>();
                rateByCustomer.put(rating.getCustomer().getId(), customerPoints);
            }

            float rate = 0;
            for (ExhibitionRatingItem rateingItem : rating.getItems()) {
                rate += rateingItem.getRate();
            }
            rate = rate / rating.getItems().size();
            customerPoints.add(rate);
        }

        SalesConfig salesConfig = getSalesConfig(userLogin);

        long numberOfMeetReqCustomers = 0;
        for (Map.Entry<ObjectId, List<Float>> entry : rateByCustomer.entrySet()) {
            float totalPoint = 0;
            for (Float point : entry.getValue()) {
                totalPoint += point;
            }
            float avgPoint = totalPoint / entry.getValue().size();
            if (avgPoint >= salesConfig.getMeetReqRate()) {
                numberOfMeetReqCustomers += 1;
            }
        }

        // XXX verify!!!
        float meetReqRate = Math.round(numberOfMeetReqCustomers / numberOfCustomers * 100);
        float paticipatedRate = Math.round(rateByCustomer.size() / (float) numberOfCustomers * 100);

        return new ExhibitionReportDto(exhibition, paticipatedRate, meetReqRate);
    }

    @Override
    public byte[] exportExhibitionReport(UserLogin userLogin, String _exhibitionId) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ObjectId exhibitionId = ObjectIdUtils.getObjectId(_exhibitionId, null);
        if (exhibitionId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Exhibition exhibition = exhibitionRepository.getById(userLogin.getClientId(), exhibitionId);
        if (exhibition == null) {
            throw new BusinessException(ExceptionCode.NOT_FOUND);
        }

        List<ExhibitionRating> ratings = null;
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
            Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                    distributorIds, false, null, null);

            ratings = exhibitionRatingRepository.getExhibitionRatingsByCustomers(userLogin.getClientId(),
                    Arrays.asList(exhibitionId), customerIds);
            if (ratings == null) {
                ratings = Collections.emptyList();
            }
        } else {
            ratings = exhibitionRatingRepository.getExhibitionRatings(userLogin.getClientId(),
                    Arrays.asList(exhibitionId));
            if (ratings == null) {
                ratings = Collections.emptyList();
            }
        }

        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        // Create the sheet
        XSSFSheet sheet = workbook.createSheet("Exhibition");

        // Create header row
        XSSFRow row = sheet.createRow(0);
        row.createCell(0).setCellValue("Time");
        row.createCell(1).setCellValue("Customer");
        row.createCell(2).setCellValue("Store Checker");

        List<ObjectId> itemIds = new ArrayList<ObjectId>(10);
        if (exhibition.getCategories() != null) {
            int index = 3;

            for (ExhibitionCategory category : exhibition.getCategories()) {
                if (category.getItems() != null) {
                    for (ExhibitionItem item : category.getItems()) {
                        row.createCell(index).setCellValue(item.getName());
                        itemIds.add(item.getId());
                        index++;
                    }
                }
            }
        }

        int rowIndex = 1;
        int size = ratings.size();
        int itemsSize = itemIds.size();

        Map<ObjectId, Float> mapPoints = new HashMap<ObjectId, Float>(20);

        for (int i = 0; i < size; i++) {
            ExhibitionRating rating = ratings.get(i);
            row = sheet.createRow(rowIndex);
            row.createCell(0).setCellValue(rating.getCreatedTime().format("dd/MM/yyyy HH:mm"));
            row.createCell(1).setCellValue(rating.getCustomer().getName());
            row.createCell(2).setCellValue(rating.getStoreChecker().getFullname());

            mapPoints.clear();

            List<ExhibitionRatingItem> ratingItems = rating.getItems();
            if (ratingItems != null) {
                int index = 3;
                int ratedCount = ratingItems.size();

                for (int j = 0; j < ratedCount; j++) {
                    ExhibitionRatingItem ratingItem = ratingItems.get(j);
                    mapPoints.put(ratingItem.getExhibitionItemId(), ratingItem.getRate());
                }

                for (int j = 0; j < itemsSize; j++) {
                    ObjectId itemId = itemIds.get(j);
                    XSSFCell cell = row.createCell(index);
                    Float point = mapPoints.get(itemId);
                    if (point != null) {
                        cell.setCellValue(point);
                    }

                    index++;
                }
            }

            rowIndex++;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            workbook.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public ListJson<OrderSimpleDto> getOrders(UserLogin userLogin, String _distributorId, String _fromDate,
            String _toDate, Pageable pageable) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ObjectId clientId = userLogin.getClientId();

        SimpleDate fromDate = SimpleDate.createByIsoDate(_fromDate, null);
        SimpleDate toDate = SimpleDate.createByIsoDate(_toDate, null);
        if (fromDate == null || toDate == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        if (fromDate.compareTo(toDate) > 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }
        Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

        Set<ObjectId> distributorIds = null;
        if (_distributorId == null) {
            if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
                distributorIds = distributorRepository.getDistributorIdsBySupervisors(userLogin.getClientId(),
                        Arrays.asList(userLogin.getUserId()));
            } else if (userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
                User user = userRepository.getById(clientId, userLogin.getUserId());
                distributorIds = Collections.singleton(user.getDistributor().getId());
            }
            // ADMIN
            else {
                List<Distributor> distributors = distributorRepository.getAll(userLogin.getClientId(), null);
                distributorIds = new HashSet<ObjectId>();
                for (Distributor distributor : distributors) {
                    distributorIds.add(distributor.getId());
                }
            }
        } else {
            if (userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
                User user = userRepository.getById(clientId, userLogin.getUserId());
                distributorIds = Collections.singleton(user.getDistributor().getId());
            } else {
                Distributor distributor = null;
                if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
                    distributor = checkSupervisorDistributor(userLogin, _distributorId);
                }
                // ADMIN
                else {
                    distributor = getMadatoryPO(userLogin, _distributorId, distributorRepository);
                }
                distributorIds = new HashSet<ObjectId>();
                distributorIds.add(distributor.getId());
            }
        }

        Collection<Order> orders = orderRepository.getOrderByDistributors(clientId, distributorIds, period, null,
                pageable, new Sort(Direction.DESC, Order.COLUMNNAME_APPROVE_TIME_VALUE));

        if (orders == null || orders.isEmpty()) {
            ListJson.<OrderSimpleDto> emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<OrderSimpleDto>(orders.size());
        for (Order order : orders) {
            dtos.add(new OrderSimpleDto(order));
        }

        long size = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || pageable.getPageSize() == size) {
                size = orderRepository.countOrderByDistributors(clientId, distributorIds, period, null);
            }
        }

        return new ListJson<OrderSimpleDto>(dtos, size);
    }

    @Override
    public OrderDto getOrderById(UserLogin userLogin, String _orderId) {
        ObjectId clientId = userLogin.getClientId();

        Order order = getMadatoryPO(userLogin, _orderId, orderRepository);

        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Collection<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                    Arrays.asList(userLogin.getUserId()));
            if (distributorIds == null || distributorIds.isEmpty()) {
                throw new BusinessException(ExceptionCode.SUPERVISOR_NO_DISTRIBUTOR);
            }
            if (!distributorIds.contains(order.getDistributor().getId())) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
            User user = userRepository.getById(clientId, userLogin.getUserId());
            if (!order.getDistributor().getId().equals(user.getDistributor().getId())) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
        }

        return new OrderDto(order, getClientCondig(userLogin).getDefaultProductPhoto());
    }

}
