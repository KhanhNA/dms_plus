package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.CustomerScheduleDto;
import com.viettel.backend.dto.embed.CustomerScheduleItemDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.CustomerScheduleService;
import com.viettel.backend.service.sub.CacheSubService;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class CustomerScheduleServiceImpl extends AbstractService implements CustomerScheduleService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CacheSubService cacheSubService;

    private boolean checkRole(UserLogin userLogin) {
        if (userLogin.hasRole(HardCodeUtils.ROLE_ADMIN) || userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            return true;
        }

        return false;
    }

    private void checkDistributor(UserLogin userLogin, ObjectId distributorId) {
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorOfSupervisorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
            if (distributorOfSupervisorIds == null || !distributorOfSupervisorIds.contains(distributorId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
        }
    }

    private void checkCustomer(UserLogin userLogin, ObjectId customerId) {
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
            Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                    distributorIds, false, null, null);
            if (!customerIds.contains(customerId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
        }
    }

    public ListJson<CustomerScheduleDto> getCustomerSchedules(UserLogin userLogin, String customerSearch,
            String distributorId, String _routeId, boolean all, Integer dayOfWeek, Pageable pageable) {
        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Distributor distributor = getMadatoryPO(userLogin, distributorId, distributorRepository);
        checkDistributor(userLogin, distributor.getId());

        Collection<Customer> customers = null;
        long count;

        if (all) {
            // CUSTOMER SCHEDULED OF ALL SALESMAN
            customers = customerRepository.getCustomersByDistributors(userLogin.getClientId(),
                    Collections.singleton(distributor.getId()), false, customerSearch, dayOfWeek, pageable, null);
            count = customerRepository.countCustomersByDistributors(userLogin.getClientId(),
                    Collections.singleton(distributor.getId()), false, customerSearch, dayOfWeek);
        } else {
            if (_routeId == null) {
                // CUSTOMER UNSCHEDULED
                customers = customerRepository.getCustomersByDistributorUnscheduled(userLogin.getClientId(),
                        distributor.getId(), customerSearch, pageable, null);
                count = customerRepository.countCustomersByDistributorUnscheduled(userLogin.getClientId(),
                        distributor.getId(), customerSearch);
            }
            // CUSTOMER SCHEDULED OF ONE SALESMAN
            else {
                ObjectId routeId = ObjectIdUtils.getObjectId(_routeId, null);
                if (routeId == null) {
                    throw new BusinessException(ExceptionCode.INVALID_PARAM);
                }

                Set<ObjectId> routeOfDistributorIds = routeRepository.getRouteIdsByDistributors(
                        userLogin.getClientId(), Collections.singleton(distributor.getId()));
                if (!routeOfDistributorIds.contains(routeId)) {
                    throw new BusinessException(ExceptionCode.INVALID_PARAM);
                }

                customers = customerRepository.getCustomersByRoute(userLogin.getClientId(),
                        Collections.singleton(routeId), customerSearch, dayOfWeek, pageable, null);
                count = customerRepository.countCustomersByRoute(userLogin.getClientId(),
                        Collections.singleton(routeId), customerSearch, dayOfWeek);
            }
        }

        if (customers == null || customers.isEmpty()) {
            return ListJson.<CustomerScheduleDto> emptyList();
        }

        ClientConfig clientConfig = getClientCondig(userLogin);

        List<CustomerScheduleDto> customerScheduleDtos = new ArrayList<CustomerScheduleDto>(customers.size());
        for (Customer customer : customers) {
            CustomerScheduleDto customerScheduleDto = new CustomerScheduleDto();
            if (customer.getSchedule() != null) {
                customerScheduleDto.setCustomerId(customer.getId().toString());
                customerScheduleDto.setRouteId(customer.getSchedule().getRouteId().toString());

                if (clientConfig.isComplexSchedule()) {
                    if (customer.getSchedule().getItems() != null) {
                        for (CustomerScheduleItem item : customer.getSchedule().getItems()) {
                            CustomerScheduleItemDto itemDto = new CustomerScheduleItemDto();
                            itemDto.setDays(item.getDays());
                            itemDto.setWeeks(item.getWeeks());

                            customerScheduleDto.addItem(itemDto);
                        }
                    }
                } else {
                    if (customer.getSchedule().getItem() != null) {
                        CustomerScheduleItem item = customer.getSchedule().getItem();
                        CustomerScheduleItemDto itemDto = new CustomerScheduleItemDto();
                        itemDto.setDays(item.getDays());
                        itemDto.setWeeks(item.getWeeks());

                        customerScheduleDto.addItem(itemDto);
                    }
                }

            } else {
                customerScheduleDto.setCustomerId(customer.getId().toString());

                CustomerScheduleItemDto customerScheduleItemDto = new CustomerScheduleItemDto();
                customerScheduleItemDto.setDays(Collections.<Integer> emptyList());
                customerScheduleItemDto.setWeeks(getDefaultWeeks(userLogin, clientConfig));

                customerScheduleDto.addItem(customerScheduleItemDto);
            }

            customerScheduleDto.setName(customer.getName());
            customerScheduleDto.setCode(customer.getCode());
            customerScheduleDto.setAddress(customer.getAddress());

            customerScheduleDtos.add(customerScheduleDto);
        }

        return new ListJson<CustomerScheduleDto>(customerScheduleDtos, count);

    }

    @Override
    public void saveCustomerScheduleByDistributor(UserLogin userLogin, String distributorId,
            List<CustomerScheduleDto> dtos) {
        if (dtos == null || dtos.isEmpty()) {
            return;
        }

        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Distributor distributor = getMadatoryPO(userLogin, distributorId, distributorRepository);
        checkDistributor(userLogin, distributor.getId());

        Set<ObjectId> routeOfDistributorIds = routeRepository.getRouteIdsByDistributors(userLogin.getClientId(),
                Collections.singleton(distributor.getId()));

        Set<ObjectId> customerOfDistributorIds = customerRepository.getCustomerIdsByDistributors(
                userLogin.getClientId(), Collections.singleton(distributor.getId()), false, null, null);

        ClientConfig clientConfig = getClientCondig(userLogin);

        Map<ObjectId, CustomerSchedule> customerSchedules = new HashMap<ObjectId, CustomerSchedule>();
        List<ObjectId> customerHaveSchedule = new LinkedList<ObjectId>();
        List<ObjectId> customerNotHaveSchedule = new LinkedList<ObjectId>();

        for (CustomerScheduleDto dto : dtos) {
            ObjectId customerId = ObjectIdUtils.getObjectId(dto.getCustomerId(), null);
            if (customerId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            if (!customerOfDistributorIds.contains(customerId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            CustomerSchedule customerSchedule = new CustomerSchedule();
            ObjectId routeId = ObjectIdUtils.getObjectId(dto.getRouteId(), null);
            if (routeId != null) {
                if (!routeOfDistributorIds.contains(routeId)) {
                    throw new BusinessException(ExceptionCode.INVALID_PARAM);
                }

                customerSchedule.setRouteId(routeId);

                List<CustomerScheduleItemDto> itemDtos = dto.getItems();
                List<CustomerScheduleItem> items = new ArrayList<CustomerScheduleItem>(itemDtos.size());
                if (itemDtos != null && itemDtos.size() > 0) {
                    for (CustomerScheduleItemDto itemDto : itemDtos) {
                        if (itemDto.getDays() != null
                                && !itemDto.getDays().isEmpty()
                                && ((itemDto.getWeeks() != null && !itemDto.getWeeks().isEmpty()) || clientConfig
                                        .getNumberWeekOfFrequency() <= 1)) {
                            CustomerScheduleItem item = new CustomerScheduleItem();

                            item.setDays(itemDto.getDays());
                            item.setWeeks(itemDto.getWeeks());

                            items.add(item);
                        }
                    }
                }

                if (!items.isEmpty()) {
                    if (clientConfig.isComplexSchedule()) {
                        customerSchedule.setItems(items);
                    } else {
                        customerSchedule.setItem(items.get(0));
                    }
                    customerSchedules.put(customerId, customerSchedule);
                }

                customerHaveSchedule.add(customerId);
            } else {
                customerNotHaveSchedule.add(customerId);
            }
        }

        // XOA TUYEN CHO NHUNG KHACH HANG KO CON TUYEN
        customerRepository.clearCustomerSchedule(userLogin.getClientId(), customerNotHaveSchedule);

        // UPDATE LAI TUYEN CHO CAC KHACH HANG CO DOI TUYEN
        for (ObjectId customerId : customerHaveSchedule) {
            customerRepository.updateCustomerSchedule(userLogin.getClientId(), customerId,
                    customerSchedules.get(customerId));
        }

        cacheSubService.reclaculateScheduleNumberPlannedCache(userLogin, routeOfDistributorIds);
    }

    @Override
    public void saveCustomerScheduleByCustomer(UserLogin userLogin, String customerId, CustomerScheduleDto dto) {
        // Validate schudle
        if (dto == null || dto.getItems() == null || dto.getItems().size() == 0) {
            throw new BusinessException(ExceptionCode.INVALID_SCHEDULE_PARAM);
        }

        for (CustomerScheduleItemDto itemDto : dto.getItems()) {
            if (itemDto.getDays() == null || itemDto.getDays().isEmpty() || itemDto.getWeeks() == null
                    || itemDto.getWeeks().isEmpty()) {
                throw new BusinessException(ExceptionCode.INVALID_SCHEDULE_PARAM);
            }
        }

        // Skip customerId in DTO
        if (customerId == null || dto.getRouteId() == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        ClientConfig clientConfig = getClientCondig(userLogin);

        if (!checkRole(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Customer customer = getMadatoryPO(userLogin, customerId, customerRepository);
        checkCustomer(userLogin, customer.getId());

        // OLD SCHEDULE
        Set<ObjectId> routeChangedIds = new HashSet<ObjectId>();
        if (customer.getSchedule() != null && customer.getSchedule().getRouteId() != null) {
            routeChangedIds.add(customer.getSchedule().getRouteId());
        }

        Collection<ObjectId> routeOfDistributorIds = routeRepository.getRouteIdsByDistributors(
                userLogin.getClientId(), Collections.singleton(customer.getDistributor().getId()));

        ObjectId routeId = ObjectIdUtils.getObjectId(dto.getRouteId(), null);
        if (routeId == null || !routeOfDistributorIds.contains(routeId)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        routeChangedIds.add(routeId);

        // Create schedule
        CustomerSchedule customerSchedule = new CustomerSchedule();
        customerSchedule.setRouteId(routeId);

        List<CustomerScheduleItemDto> itemDtos = dto.getItems();
        List<CustomerScheduleItem> items = new ArrayList<CustomerScheduleItem>(itemDtos.size());
        if (itemDtos != null && itemDtos.size() > 0) {
            for (CustomerScheduleItemDto itemDto : itemDtos) {
                if (itemDto.getDays() != null && !itemDto.getDays().isEmpty() && itemDto.getWeeks() != null
                        && !itemDto.getWeeks().isEmpty()) {
                    CustomerScheduleItem item = new CustomerScheduleItem();

                    item.setDays(itemDto.getDays());
                    item.setWeeks(itemDto.getWeeks());

                    items.add(item);
                }
            }
        }

        if (!items.isEmpty()) {
            if (clientConfig.isComplexSchedule()) {
                customerSchedule.setItems(items);
            } else {
                customerSchedule.setItem(items.get(0));
            }
        }

        // Save to database
        customerRepository.updateCustomerSchedule(userLogin.getClientId(), customer.getId(), customerSchedule);

        cacheSubService.reclaculateScheduleNumberPlannedCache(userLogin, routeChangedIds);
    }

    private List<Integer> getDefaultWeeks(UserLogin userLogin, ClientConfig clientConfig) {
        if (clientConfig.getNumberWeekOfFrequency() > 1) {
            List<Integer> weeks = new ArrayList<Integer>(clientConfig.getNumberWeekOfFrequency());
            for (int i = 0; i < clientConfig.getNumberWeekOfFrequency(); i++) {
                weeks.add(i + 1);
            }

            return weeks;
        } else {
            return null;
        }
    }

}
