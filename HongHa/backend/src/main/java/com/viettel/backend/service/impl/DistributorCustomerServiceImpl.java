package com.viettel.backend.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.viettel.backend.domain.CustomerCheck;
import com.viettel.backend.domain.User;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerCheckRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.DistributorCustomerService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class DistributorCustomerServiceImpl extends AbstractService implements DistributorCustomerService {

    private static final long serialVersionUID = 1L;
    
    // Maximum day of period
    public static final int MAX_PERIOD_ALLOWED = 7;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerCheckRepository customerCheckRepository;

    @Override
    public byte[] exportCustomerCheckReport(UserLogin userLogin, String _fromDate, String _toDate, String _salesmanId) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
            throw new BusinessException(ExceptionCode.DISTRIBUTOR_ONLY);
        }
        
        ObjectId clientId = userLogin.getClientId();
        
        SimpleDate fromDate = SimpleDate.createByIsoDate(_fromDate, null);
        SimpleDate toDate = SimpleDate.createByIsoDate(_toDate, null);
        if (fromDate == null || toDate == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        if (fromDate.compareTo(toDate) > 0 || DateTimeUtils.addDays(fromDate, MAX_PERIOD_ALLOWED).compareTo(toDate) < 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }
        Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));
        
        User distributorUser = getCurrentUser(userLogin);
        if (distributorUser.getDistributor() == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Collection<ObjectId> distributorIds = Collections.singleton(distributorUser.getDistributor().getId());
        Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByDistributors(clientId, distributorIds);
        
        List<CustomerCheck> customerChecks = null;
        Sort sort = new Sort(
                CustomerCheck.COLUMNNAME_CREATED_TIME_VALUE,
                CustomerCheck.COLUMNNAME_CUSTOMER_DISTRICT_NAME);

        if (!StringUtils.isEmpty(_salesmanId)) {
            ObjectId salesmanId = ObjectIdUtils.getObjectId(_salesmanId, null);
            if (salesmanId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            
            if (!salesmanIds.contains(salesmanId)) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            
            customerChecks = customerCheckRepository.getBySalesmen(clientId, Collections.singletonList(salesmanId), period, sort);
        } else {
            if (salesmanIds.isEmpty()) {
                customerChecks = Collections.emptyList();
            } else {
                customerChecks = customerCheckRepository.getBySalesmen(clientId, salesmanIds, period, sort);
            }
        }
        
        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("RESULT");

        int rowIndex = 0;
        XSSFRow row = sheet.createRow(rowIndex++);
        row.createCell(0).setCellValue("DISTRIBUTOR");
        row.createCell(1).setCellValue(distributorUser.getFullname());
        
        rowIndex++;
        row = sheet.createRow(rowIndex++);
        row.createCell(0).setCellValue("CUSTOMER");
        row.createCell(1).setCellValue("VISIT DATE");
        row.createCell(2).setCellValue("CHECK DATE");
        row.createCell(3).setCellValue("DISTRICT");
        row.createCell(4).setCellValue("HAS ORDER");
        row.createCell(5).setCellValue("SALESMAN");
        row.createCell(6).setCellValue("PASSED");
        row.createCell(7).setCellValue("NOTE");
        if (!CollectionUtils.isEmpty(customerChecks)) {
            for (CustomerCheck customerCheck : customerChecks) {
                row = sheet.createRow(rowIndex++);
                row.createCell(0).setCellValue(customerCheck.getCustomer().getName());
                if (customerCheck.getVisitDate() != null) {
                    row.createCell(1).setCellValue(customerCheck.getVisitDate().format("dd/MM/yyyy HH:mm"));
                }
                row.createCell(2).setCellValue(customerCheck.getCreatedTime().format("dd/MM/yyyy HH:mm"));
                row.createCell(3).setCellValue(customerCheck.getCustomer().getDistrict().getName());
                row.createCell(4).setCellValue(customerCheck.isHasOrder() ? "x" : null);
                row.createCell(5).setCellValue(customerCheck.getSalesman().getFullname());
                row.createCell(6).setCellValue(customerCheck.isPassed() ? "x" : null);
                row.createCell(7).setCellValue(customerCheck.getNote());
            }
        }
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            workbook.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
