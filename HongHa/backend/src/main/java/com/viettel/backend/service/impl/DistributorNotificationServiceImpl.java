package com.viettel.backend.service.impl;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.DistributorNotificationDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.FeedbackRepository;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.DistributorNotificationService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class DistributorNotificationServiceImpl extends AbstractSupervisorService implements
        DistributorNotificationService {

    private static final long serialVersionUID = 4822917139294672054L;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    public DistributorNotificationDto getNotification(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        User user = getCurrentUser(userLogin);

        long nbOrderToApprove = 0;
        if (user.getDistributor() != null) {
            nbOrderToApprove = orderPendingRepository.countPendingOrdersByDistributors(userLogin.getClientId(),
                    Collections.singletonList(user.getDistributor().getId()));
        }

        return new DistributorNotificationDto(nbOrderToApprove);
    }

}
