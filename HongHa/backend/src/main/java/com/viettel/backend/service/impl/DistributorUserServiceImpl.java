package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.User;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.UserSimpleDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.DistributorUserService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class DistributorUserServiceImpl extends AbstractService implements DistributorUserService {

    private static final long serialVersionUID = -7521814129330891905L;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Override
    public ListJson<UserSimpleDto> getSalesmen(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        User distributorUser = getCurrentUser(userLogin);
        Collection<ObjectId> distributorIds = Collections.singleton(distributorUser.getDistributor().getId());
        
        List<User> salesmen = userRepository.getSalesmenByDistributors(userLogin.getClientId(), distributorIds);
        if (salesmen == null || salesmen.isEmpty()) {
            return ListJson.emptyList();
        }

        List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(salesmen.size());
        for (User salesman : salesmen) {
            dtos.add(new UserSimpleDto(salesman));
        }

        return new ListJson<UserSimpleDto>(dtos, Long.valueOf(dtos.size()));
    }

}
