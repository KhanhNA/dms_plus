package com.viettel.backend.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.District;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.dto.NameCategoryByDistributorCreateDto;
import com.viettel.backend.dto.NameCategoryByDistributorDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.DistrictService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class DistrictServiceImpl
        extends
        BasicNameCategoryByDistributorServiceImpl<District, NameCategoryByDistributorDto, NameCategoryByDistributorDto, NameCategoryByDistributorCreateDto>
        implements DistrictService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    protected Collection<ObjectId> getDistributorIdsForFilter(UserLogin userLogin) {
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorOfSupervisorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
            if (distributorOfSupervisorIds == null) {
                return Collections.emptySet();
            }
            return distributorOfSupervisorIds;
        }

        return null;
    }

    @Override
    protected void checkBeforeSetActive(UserLogin userLogin, ObjectId id, boolean active) {
        super.checkBeforeSetActive(userLogin, id, active);

        if (!active) {
            if (customerRepository.checkRouteUsed(userLogin.getClientId(), id)) {
                throw new BusinessException(ExceptionCode.ROUTE_IS_USING);
            }
        }
    }

    @Override
    public BasicCategoryRepository<District> getRepository() {
        return districtRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN) || userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR);
    }

    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.PERMISSION_DENIED;
    }

    @Override
    public District createDomain(UserLogin userLogin, NameCategoryByDistributorCreateDto createdto) {
        District domain = new District();
        domain.setDraft(true);

        initPOWhenCreate(District.class, userLogin, domain);

        updateDomain(userLogin, domain, createdto);

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, District domain, NameCategoryByDistributorCreateDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }
        
        checkMandatoryParams(createdto, createdto.getName(), createdto.getDistributorId());

        Distributor distributor = getMadatoryPO(userLogin, createdto.getDistributorId(), distributorRepository);
        domain.setDistributor(new DistributorEmbed(distributor));

        if (districtRepository.existsByNameByDistributor(userLogin.getClientId(), distributor.getId(), domain.getId(),
                createdto.getName())) {
            throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
        }
        domain.setName(createdto.getName());
    }

    @Override
    public NameCategoryByDistributorDto createSimpleDto(UserLogin userLogin, District domain) {
        return new NameCategoryByDistributorDto(domain);
    }

    @Override
    public NameCategoryByDistributorDto createDetailDto(UserLogin userLogin, District domain) {
        return createSimpleDto(userLogin, domain);
    }

}
