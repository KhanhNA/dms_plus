package com.viettel.backend.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.ExportService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class ExportServiceImpl extends AbstractService implements ExportService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    private boolean checkRolesAllowed(UserLogin userLogin) {
        if (userLogin.getRoles() == null || userLogin.getRoles().isEmpty()) {
            return false;
        }

        return Arrays.asList(HardCodeUtils.ROLE_ADMIN, HardCodeUtils.ROLE_SUPERVISOR).contains(
                userLogin.getRoles().get(0));
    }

    private Collection<ObjectId> getSalesmanIds(UserLogin userLogin) {
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Arrays.asList(userLogin.getUserId()));

            return userRepository.getSalesmanIdsByDistributors(userLogin.getClientId(), distributorIds);
        } else {
            return userRepository.getUserIdsByRole(userLogin.getClientId(), HardCodeUtils.ROLE_SALESMAN);
        }
    }

    @Override
    public byte[] exportVisitsByDay(UserLogin userLogin, String isoDate) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        checkMandatoryParams(isoDate);

        SimpleDate date = SimpleDate.createByIsoDate(isoDate, null);
        if (date == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        Period period = new Period(date, DateTimeUtils.addDays(date, 1));

        Sort sort = new Sort(Visit.COLUMNNAME_START_TIME_VALUE);

        List<Visit> visits = visitRepository.getVisitedsBySalesmen(userLogin.getClientId(), getSalesmanIds(userLogin),
                period, sort);

        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Visit");

        if (visits == null || visits.isEmpty()) {
            XSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("Empty");
        } else {
            XSSFRow row = sheet.createRow(0);
            row.createCell(0).setCellValue("Salesman");
            row.createCell(1).setCellValue("Customer");
            row.createCell(2).setCellValue("StartTime");
            row.createCell(2).setCellValue("EndTime");
            row.createCell(2).setCellValue("Duration");
            row.createCell(2).setCellValue("Distance");
            row.createCell(2).setCellValue("Status");
            row.createCell(2).setCellValue("PO");

            int index = 1;
            for (Visit visit : visits) {
                row = sheet.createRow(index);

                row.createCell(0).setCellValue(visit.getSalesman().getFullname());
                row.createCell(1).setCellValue(visit.getCustomer().getName());
                row.createCell(2).setCellValue(visit.getStartTime().format("hh:mm:ss"));
                row.createCell(3).setCellValue(visit.getEndTime().format("hh:mm:ss"));
                row.createCell(4).setCellValue(visit.getDuration() / 1000);

                if (visit.getDistance() == null) {
                    row.createCell(5).setCellValue("undefined");
                } else {
                    row.createCell(5).setCellValue(visit.getDistance());
                }

                row.createCell(6).setCellValue(visit.isClosed() ? "Closed" : "Normal");

                if (visit.isOrder()) {
                    if (visit.getApproveStatus() == Visit.APPROVE_STATUS_REJECTED) {
                        row.createCell(7).setCellValue("Rejected");
                    } else if (visit.getApproveStatus() == Visit.APPROVE_STATUS_PENDING) {
                        row.createCell(7).setCellValue("Pending");
                    } else if (visit.getApproveStatus() == Visit.APPROVE_STATUS_APPROVED) {
                        row.createCell(7).setCellValue(visit.getGrandTotal().longValue());
                    }
                } else {
                    row.createCell(7).setCellValue("No Order");
                }

                index++;
            }
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            workbook.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public InputStream exportOrderSummary(UserLogin userLogin, String _distributorId, String _fromDate, String _toDate) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        checkMandatoryParams(_distributorId, _fromDate, _toDate);

        ObjectId clientId = userLogin.getClientId();

        SimpleDate fromDate = SimpleDate.createByIsoDate(_fromDate, null);
        SimpleDate toDate = SimpleDate.createByIsoDate(_toDate, null);
        if (fromDate == null || toDate == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        // Constraint of duration between from - to date is no greater than 2
        // months
        if (fromDate.compareTo(toDate) > 0 || DateTimeUtils.addMonths(fromDate, 2).compareTo(toDate) < 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId);
        if (distributorId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Distributor distributor = distributorRepository.getById(clientId, distributorId);
        if (distributor == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Collection<ObjectId> distributorIds = Collections.singletonList(distributorId);

        Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

        Sort sort = new Sort(Direction.ASC, Order.COLUMNNAME_CREATED_TIME_VALUE);

        long count = orderRepository.countOrderByDistributors(clientId, distributorIds, period, null);
        int size = 2000;
        int maxPage = ((int) count / size) + ((count % (long) size) == 0 ? 0 : 1);

        SXSSFWorkbook workbook = null;
        OutputStream outputStream = null;
        try {
            workbook = new SXSSFWorkbook();
            // Create sheet
            SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("PO Report");

            CellStyle cs;
            Font font;
            Row row;
            Cell cell;

            // Create headers
            cs = workbook.createCellStyle();
            font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 11);
            cs.setFont(font);
            cs.setAlignment(CellStyle.ALIGN_CENTER);
            cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            row = sheet.createRow(1);
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 15));
            cell = row.createCell(0);
            cell.setCellValue("PURCHASE ORDER SUMMARY REPORT");
            cell.setCellStyle(cs);

            row = sheet.createRow(3);
            sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 1));
            row.createCell(0).setCellValue("From date: " + fromDate.format("dd/MM/yyyy"));
            row.createCell(2).setCellValue("To date: " + toDate.format("dd/MM/yyyy"));
            row = sheet.createRow(4);
            sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 1));
            row.createCell(0).setCellValue("Issued date: " + DateTimeUtils.getCurrentTime().format("dd/MM/yyyy"));

            // Create table headers
            cs = workbook.createCellStyle();
            font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 8);
            cs.setFont(font);
            cs.setAlignment(CellStyle.ALIGN_CENTER);
            cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            cs.setBorderBottom(CellStyle.BORDER_THIN);
            cs.setBorderTop(CellStyle.BORDER_THIN);
            cs.setBorderLeft(CellStyle.BORDER_THIN);
            cs.setBorderRight(CellStyle.BORDER_THIN);

            String[] headers = new String[] { "PO NUMBER", "DATE", "DELIVERY DATE", "PRIORITY", "CUSTOMER TYPE",
                    "CUSTOMER NO", "CUSTOMER NAME", "DISTRIBUTOR CODE", "DISTRIBUTOR NAME", "SALESMAN CODE",
                    "SALESMAN NAME", "DISTRICT", "TOTAL QUANTITY", "TOTAL AMOUNT", "PROMOTION", "DISCOUNT" };

            row = sheet.createRow(7);
            int cellnum = 0;
            for (String header : headers) {
                cell = row.createCell(cellnum++);
                cell.setCellStyle(cs);
                cell.setCellValue(header);
            }

            int rownum = 8;
            BigDecimal totalQuantity = BigDecimal.ZERO;
            BigDecimal totalAmount = BigDecimal.ZERO;
            BigDecimal totalPromotion = BigDecimal.ZERO;
            BigDecimal totalDiscount = BigDecimal.ZERO;

            // Load customer
            Map<ObjectId, Customer> mapCustomers = Collections.emptyMap();
            if (maxPage > 0) {
                // Change to get map with deactivated if needed
                List<Customer> customers = customerRepository.getCustomersByDistributors(clientId, distributorIds,
                        false, null, null, null, null);

                if (!CollectionUtils.isEmpty(customers)) {
                    mapCustomers = new HashMap<ObjectId, Customer>(customers.size());
                    for (int i = 0, length = mapCustomers.size(); i < length; i++) {
                        Customer customer = customers.get(i);
                        mapCustomers.put(customer.getId(), customer);
                    }
                } else {
                    mapCustomers = new HashMap<ObjectId, Customer>();
                }
            }

            for (int page = 1; page <= maxPage; page++) {
                Pageable pageable = new PageRequest(page - 1, size, null);

                List<Order> orders = orderRepository.getOrderByDistributors(clientId, distributorIds, period, null,
                        pageable, sort);
                for (int i = 0, length = orders.size(); i < length; i++) {
                    Order order = orders.get(i);

                    row = sheet.createRow(rownum++);
                    cell = row.createCell(0);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getCode());

                    cell = row.createCell(1);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getCreatedTime().format("dd/MM/yyyy"));

                    if (order.getDeliveryTime() != null
                            && !DateTimeUtils.isSameDate(order.getCreatedTime(), order.getDeliveryTime())) {
                        cell = row.createCell(2);
                        cell.setCellStyle(cs);
                        cell.setCellValue(order.getDeliveryTime().format("dd/MM/yyyy"));

                        cell = row.createCell(3);
                        cell.setCellStyle(cs);
                    } else {
                        cell = row.createCell(2);
                        cell.setCellStyle(cs);

                        cell = row.createCell(3);
                        cell.setCellStyle(cs);
                        cell.setCellValue("Immediately");
                    }

                    // Customer info
                    cell = row.createCell(4);
                    cell.setCellStyle(cs);
                    {
                        // If map not contains customer, this customer may be
                        // assigned to another distributor
                        // Dont worry, period for this report only max in 2
                        // months
                        if (!mapCustomers.containsKey(order.getCustomer().getId())) {
                            // Change to get with deactivated if needed
                            Customer customer = customerRepository.getById(clientId, order.getCustomer().getId());
                            // Allow null value to ignore load more than once
                            mapCustomers.put(order.getCustomer().getId(), customer);
                        }
                        Customer customer = mapCustomers.get(order.getCustomer().getId());
                        if (customer != null) {
                            cell.setCellValue(customer.getCustomerType().getName());
                        }
                    }

                    cell = row.createCell(5);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getCustomer().getCode());

                    cell = row.createCell(6);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getCustomer().getName());

                    // Distributor info
                    cell = row.createCell(7);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getDistributor().getCode());

                    cell = row.createCell(8);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getDistributor().getName());

                    // Salesman info
                    cell = row.createCell(9);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getSalesman().getUsername());

                    cell = row.createCell(10);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getSalesman().getFullname());

                    // District info
                    cell = row.createCell(11);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getCustomer().getDistrict().getName());

                    // Order info
                    BigDecimal quantity = BigDecimal.ZERO;
                    for (int j = 0, dl = order.getDetails().size(); j < dl; j++) {
                        OrderDetail detail = order.getDetails().get(j);
                        quantity = quantity.add(detail.getQuantity());
                    }
                    cell = row.createCell(12);
                    cell.setCellStyle(cs);
                    cell.setCellValue(quantity.doubleValue());
                    totalQuantity = totalQuantity.add(quantity);

                    cell = row.createCell(13);
                    cell.setCellStyle(cs);
                    cell.setCellValue(order.getGrandTotal().doubleValue());
                    totalAmount = totalAmount.add(order.getGrandTotal());

                    if (order.getPromotionAmt() != null) {
                        cell = row.createCell(14);
                        cell.setCellStyle(cs);
                        cell.setCellValue(order.getPromotionAmt().doubleValue());
                        totalPromotion = totalPromotion.add(order.getPromotionAmt());
                    } else {
                        cell = row.createCell(14);
                        cell.setCellStyle(cs);
                        cell.setCellValue(0.0);
                    }

                    if (order.getDiscountAmt() != null) {
                        cell = row.createCell(15);
                        cell.setCellStyle(cs);
                        cell.setCellValue(order.getDiscountAmt().doubleValue());
                        totalDiscount = totalDiscount.add(order.getDiscountAmt());
                    } else {
                        cell = row.createCell(15);
                        cell.setCellStyle(cs);
                        cell.setCellValue(0.0);
                    }
                }
            }

            // Create summary line
            int lastRownum = rownum++;
            row = sheet.createRow(lastRownum);
            sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, 8));
            sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 9, 11));
            row.createCell(0).setCellValue("Report: " + count + " record(s)");
            row.createCell(9).setCellValue("Total for the report:");
            row.createCell(12).setCellValue(totalQuantity.doubleValue());
            row.createCell(13).setCellValue(totalAmount.doubleValue());
            row.createCell(14).setCellValue(totalPromotion.doubleValue());
            row.createCell(15).setCellValue(totalDiscount.doubleValue());

            for (int i = 0, length = headers.length; i < length; i++) {
                sheet.autoSizeColumn(i);
            }

            File outTempFile = File.createTempFile(
                    "PurchaseOrderSummaryReport_" + _distributorId + "_" + System.currentTimeMillis(), "tmp");
            outputStream = new FileOutputStream(outTempFile);
            workbook.write(outputStream);
            workbook.dispose();
            outputStream.close();
            outputStream = null;

            return new FileInputStream(outTempFile);

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(workbook);
            IOUtils.closeQuietly(outputStream);
        }
    }

    @Override
    public InputStream exportOrderDetail(UserLogin userLogin, String _distributorId, String _fromDate, String _toDate) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        checkMandatoryParams(_distributorId, _fromDate, _toDate);

        ObjectId clientId = userLogin.getClientId();

        SimpleDate fromDate = SimpleDate.createByIsoDate(_fromDate, null);
        SimpleDate toDate = SimpleDate.createByIsoDate(_toDate, null);
        if (fromDate == null || toDate == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        // Constraint of duration between from - to date is no greater than 2
        // months
        if (fromDate.compareTo(toDate) > 0 || DateTimeUtils.addMonths(fromDate, 2).compareTo(toDate) < 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId);
        if (distributorId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Distributor distributor = distributorRepository.getById(clientId, distributorId);
        if (distributor == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Collection<ObjectId> distributorIds = Collections.singletonList(distributorId);

        Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

        Sort sort = new Sort(Direction.ASC, Order.COLUMNNAME_CREATED_TIME_VALUE);

        long count = orderRepository.countOrderByDistributors(clientId, distributorIds, period, null);
        int size = 2000;
        int maxPage = ((int) count / size) + ((count % (long) size) == 0 ? 0 : 1);

        SXSSFWorkbook workbook = null;
        OutputStream outputStream = null;
        try {
            workbook = new SXSSFWorkbook();
            // Create sheet
            SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("PO Details Report");

            CellStyle cs;
            Font font;
            Row row;
            Cell cell;

            // Create headers
            cs = workbook.createCellStyle();
            font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 11);
            cs.setFont(font);
            cs.setAlignment(CellStyle.ALIGN_CENTER);
            cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            row = sheet.createRow(1);
            sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 7));
            cell = row.createCell(0);
            cell.setCellValue("PURCHASE ORDER LINE ITEM REPORT");
            cell.setCellStyle(cs);

            row = sheet.createRow(3);
            sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 1));
            row.createCell(0).setCellValue("From date: " + fromDate.format("dd/MM/yyyy"));
            row.createCell(2).setCellValue("To date: " + toDate.format("dd/MM/yyyy"));
            row = sheet.createRow(4);
            sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 1));
            row.createCell(0).setCellValue("Issued date: " + DateTimeUtils.getCurrentTime().format("dd/MM/yyyy"));

            // Create table headers
            cs = workbook.createCellStyle();
            font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 8);
            cs.setFont(font);
            cs.setAlignment(CellStyle.ALIGN_CENTER);
            cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            cs.setBorderBottom(CellStyle.BORDER_THIN);
            cs.setBorderTop(CellStyle.BORDER_THIN);
            cs.setBorderLeft(CellStyle.BORDER_THIN);
            cs.setBorderRight(CellStyle.BORDER_THIN);

            String[] headers = new String[] { "PO NUMBER", "DATE", "DELIVERY DATE", "PRIORITY", "ITEM CODE", "UOM",
                    "QUANTITY", "AMOUNT" };

            row = sheet.createRow(7);
            int cellnum = 0;
            for (String header : headers) {
                cell = row.createCell(cellnum++);
                cell.setCellStyle(cs);
                cell.setCellValue(header);
            }

            int rownum = 8;

            for (int page = 1; page <= maxPage; page++) {
                Pageable pageable = new PageRequest(page - 1, size, null);

                List<Order> orders = orderRepository.getOrderByDistributors(clientId, distributorIds, period, null,
                        pageable, sort);
                for (int i = 0, length = orders.size(); i < length; i++) {
                    Order order = orders.get(i);

                    for (int j = 0, dl = order.getDetails().size(); j < dl; j++) {
                        OrderDetail detail = order.getDetails().get(j);

                        row = sheet.createRow(rownum++);
                        cell = row.createCell(0);
                        cell.setCellStyle(cs);
                        cell.setCellValue(order.getCode());

                        cell = row.createCell(1);
                        cell.setCellStyle(cs);
                        cell.setCellValue(order.getCreatedTime().format("dd/MM/yyyy"));

                        if (order.getDeliveryTime() != null
                                && !DateTimeUtils.isSameDate(order.getCreatedTime(), order.getDeliveryTime())) {
                            cell = row.createCell(2);
                            cell.setCellStyle(cs);
                            cell.setCellValue(order.getDeliveryTime().format("dd/MM/yyyy"));

                            cell = row.createCell(3);
                            cell.setCellStyle(cs);
                        } else {
                            cell = row.createCell(2);
                            cell.setCellStyle(cs);

                            cell = row.createCell(3);
                            cell.setCellStyle(cs);
                            cell.setCellValue("Immediately");
                        }

                        cell = row.createCell(4);
                        cell.setCellStyle(cs);
                        cell.setCellValue(detail.getProduct().getCode());

                        cell = row.createCell(5);
                        cell.setCellStyle(cs);
                        cell.setCellValue(detail.getProduct().getUom().getName());

                        cell = row.createCell(6);
                        cell.setCellStyle(cs);
                        cell.setCellValue(detail.getQuantity().doubleValue());

                        cell = row.createCell(7);
                        cell.setCellStyle(cs);
                        cell.setCellValue(detail.getAmount().doubleValue());
                    }
                }
            }

            for (int i = 0, length = headers.length; i < length; i++) {
                sheet.autoSizeColumn(i);
            }

            File outTempFile = File.createTempFile(
                    "PurchaseOrderDetailReport_" + _distributorId + "_" + System.currentTimeMillis(), "tmp");
            outputStream = new FileOutputStream(outTempFile);
            workbook.write(outputStream);
            workbook.dispose();
            outputStream.close();
            outputStream = null;

            return new FileInputStream(outTempFile);

        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(workbook);
            IOUtils.closeQuietly(outputStream);
        }
    }
}
