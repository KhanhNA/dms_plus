package com.viettel.backend.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.ProductCategory;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.domain.UOM;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.dto.OrderCreateDto;
import com.viettel.backend.dto.embed.OrderDetailCreatedDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.file.DbFileMeta;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.ProductCategoryRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UOMRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.GenerateDataService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.service.engine.PromotionEngine;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.LocationUtils;

@Service
public class GenerateDataSerivceImpl extends AbstractService implements GenerateDataService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PromotionEngine promotionEngine;

    @Autowired
    private UOMRepository uomRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private FileEngine fileEngine;

    @Override
    public void generateCustomerByDistributor(UserLogin userLogin, String distributorId, double longitude,
            double latitude, int numberCustomer) {
        if (!LocationUtils.checkLocationValid(longitude, latitude)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        long customerSize = customerRepository.count(userLogin.getClientId(), null);

        Distributor distributor = getMadatoryPO(userLogin, distributorId, distributorRepository);
        User currentUser = getCurrentUser(userLogin);
        List<CustomerType> customerTypes = customerTypeRepository.getAll(userLogin.getClientId(), null);

        for (int i = 0; i < numberCustomer; i++) {
            Customer customer = new Customer();
            initPOWhenCreate(Customer.class, userLogin, customer);
            customer.setDraft(false);
            customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
            customer.setCreatedTime(DateTimeUtils.getCurrentTime());
            customer.setCreatedBy(new UserEmbed(currentUser));
            customer.setCode(codeGeneratorRepository.getCustomerCode(userLogin.getClientId().toString()));
            customer.setName(generateCustomerName((int) (customerSize + i)));
            customer.setMobile("0988888888");
            customer.setAddress(distributor.getAddress());
            customer.setDistributor(new DistributorEmbed(distributor));
            customer.setCustomerType(customerTypes.get(i % customerTypes.size()));
            customer.setLocation(generateLocation(longitude, latitude, 0.0, 2.0));

            customerRepository.save(userLogin.getClientId(), customer);
        }
    }

    /**
     * 
     * @param longitude
     * @param latitude
     * @param minDistance
     *            (KM)
     * @param maxDistance
     *            (KM)
     * @return
     */
    private double[] generateLocation(double longitude, double latitude, double minDistance, double maxDistance) {
        Random random = new Random();

        double distance = -1;

        double foundLongitude = -1;
        double foundLatitude = -1;

        while (distance <= minDistance || distance >= maxDistance) {
            // Convert radius from meters to degrees
            double radiusInDegrees = (maxDistance * 1000) / 111000f;

            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west
            // distances
            double new_x = x / Math.cos(latitude);

            foundLongitude = new_x + longitude;
            foundLatitude = y + latitude;

            distance = LocationUtils.calculateDistance(latitude, longitude, foundLatitude, foundLongitude);
        }

        return new double[] { foundLongitude, foundLatitude };
    }

    private String generateCustomerName(int index) {
        String[] names = new String[] { "Lori Hernandez", "Jesse Gonzales", "Catherine Rivera", "Henry Mitchell",
                "Louise Walker", "Chris Harris", "Ronald Barnes", "Matthew Baker", "Willie Stewart", "Ann White",
                "Jennifer Turner", "Virginia Williams", "Tina Ward", "Kimberly Cook", "Barbara Jackson",
                "Frank Russell", "Jeffrey Griffin", "Phillip Watson", "Janice Garcia", "Scott Phillips", "Rose James",
                "Victor Brooks", "Deborah Martinez", "Bobby Price", "Rebecca Hall", "Walter Howard", "Joe Henderson",
                "Paul Peterson", "Amanda Allen", "Janet Long", "Fred Wright", "Judith Sanchez", "Gerald Jones",
                "Roger Powell", "Andrew Foster", "Anthony Collins", "Russell Bailey", "Edward Diaz", "Raymond Smith",
                "Douglas Carter", "William Parker", "Kelly Brown", "George Jenkins", "Alan Rogers", "Juan Cox",
                "Stephen Scott", "Phyllis Johnson", "Michael Bennett", "Betty Adams", "Martin Wood", "Helen Morris",
                "Ashley Butler", "Ryan Washington", "Paula Hill", "Ruth Patterson", "Jean Perry", "Craig Miller",
                "Elizabeth Evans", "Benjamin Alexander", "Eugene Clark", "Timothy Edwards", "Todd Thompson",
                "Patricia Bell", "Angela Richardson", "Diana Hughes", "Daniel Murphy", "Evelyn Moore", "David Nelson",
                "Thomas Sanders", "Eric King", "Ruby Robinson", "Terry Kelly", "Brian Ramirez", "Jose Flores",
                "Ralph Reed", "Mary Lopez", "Shawn Gray", "Joyce Taylor", "Arthur Green", "Cheryl Young",
                "Debra Roberts", "Rachel Campbell", "Lillian Morgan", "Brenda Anderson", "Joseph Wilson",
                "Sara Coleman", "Linda Ross", "Sarah Bryant", "Maria Cooper", "Gregory Thomas", "Melissa Rodriguez",
                "Bonnie Davis", "James Martin", "Christopher Lewis", "Jessica Torres", "Jimmy Simmons",
                "Lisa Gonzalez", "Wayne Perez", "Sandra Lee", "Carl", "Howard Bryant", "Raymond Carter",
                "Douglas King", "George Bennett", "Virginia Foster", "Carlos Diaz", "Victor Torres", "Andrea Ramirez",
                "Irene Murphy", "Sarah White", "Christine Baker", "Randy Cox", "Norma Perry", "Jonathan Coleman",
                "Amy Thompson", "Joshua Moore", "Patrick Roberts", "Teresa Thomas", "Anthony Bailey",
                "Martha Mitchell", "Catherine Brown", "Thomas Flores", "Julie Henderson", "Emily Rogers",
                "Philip Kelly", "Kathy Miller", "Carol Brooks", "Jessica Parker", "Joan Clark", "Theresa Smith",
                "Amanda Martinez", "Jeffrey Campbell", "Jerry Johnson", "Marilyn Collins", "Ann Nelson",
                "Kenneth Washingt", "Angela Edwards", "Kathleen Barnes", "David Watson", "Evelyn Hall", "Harold Hill",
                "Harry Phillips", "Linda Evans", "Tina Cook", "Mildred Scott", "Joyce Jones", "Wanda Sanders",
                "Anne Lewis", "Diana Ward", "Carolyn Rodrigue", "Cheryl Hernandez", "Jason Price", "Elizabeth James",
                "Lillian Gonzalez", "Justin Lopez", "John Harris", "Matthew Martin", "Shawn Davis", "Denise Stewart",
                "Brandon Turner", "Albert Wilson", "Beverly Alexande", "Kimberly Adams", "Terry Long",
                "Katherine Robinson", "Jean Rivera", "Wayne Ross", "Michelle Morgan", "Henry Williams", "Louise Wood",
                "Samuel Wright", "Gregory Perez", "Bruce Howard", "Keith Young", "Jane Patterson", "Jesse Griffin",
                "Doris Allen", "Todd Cooper", "Steve Simmons", "Adam Powell", "Bonnie Morris", "Nancy Peterson",
                "Juan Gonzales", "Jimmy Reed", "Paula Taylor", "Ruby Lee", "Kelly Richardson", "Roy Jenkins",
                "Melissa Gray", "Cynthia Hughes", "Christina Sanchez", "Patricia Jackson", "Billy Russell",
                "Andrew Garcia", "Janice Butler", "Marie Anderson", "Janet Green", "Sandra Walker", "Stephen Bell",
                "Jack", "Kimiko Sisson", "Karrie Giron", "Jenniffer Sotelo", "Jonna Goldsmith", "Jutta Ebert",
                "Weston Queen", "Emmy Quarles", "Evie Eaves", "Vida Lefebvre", "Tonita Woodson", "Demetra Bartley",
                "Sabina Lawless", "Belen Maples", "Tamika Haugen", "Jamaal Altman", "Gwyneth Hutchens", "Xiao Poulin",
                "Kandy Daly", "Stacie Whitlock", "Nedra Molina", "Hilario Bain", "Mitchel Beltran", "Crysta Martel",
                "Arleen Oaks", "Lanette Clifton", "Kasey Orellana", "Yoshie Dominguez", "Cortez Hood", "Kyla Graff",
                "Racheal Horne", "Oliva Chang", "Crystle Buckley", "Son Cornett", "Cathi Batts", "Kenya Greenfield",
                "Sharmaine Kopp", "Celine Christianson", "Angelia Anglin", "Lonny Bearden", "Fredricka Villanue",
                "Tatiana Evers", "Keneth Winfrey", "Dian Brogan", "Mark Smoot", "Cherri Matheny", "Larae Do",
                "Milagro Irizarry", "Lincoln Eastman", "Consuelo Cupp", "Hedwig Dyer", "Dominga Pollock",
                "Silas Saxton", "Loyce Grigsby", "Edwina Negrete", "Barney Lash", "Freddy Barba", "Garret Acosta",
                "Tula Ricks", "Usha Steffen", "Jetta Mcmullen", "Martina Brothers", "Yulanda Atwood", "Joselyn Smart",
                "Louvenia Ivy", "Dayna Heflin", "Marquetta Falls", "Charlena Mueller", "Yahaira Huggins",
                "Carey Salter", "Carri Stratton", "Effie Bingham", "Ursula Nevarez", "Kandace Maurer",
                "Wilhemina Duarte", "Willow Bergstrom", "Cristie John", "Jade Otis", "Margot Choi", "Bianca Abraham",
                "Aura Strong", "Daphine Halstead", "Laree Akers", "Adalberto Paine", "Paola Manns", "Eliza Glenn",
                "Lissa Spinks", "Ashli Lu", "Theo Weems", "Lizzie Mccauley", "Barbie Quick", "Jarod Farrar",
                "Stephane Rubin", "Yukiko Rau", "Alpha Clegg", "Kenton Creech", "Cherry Flynn", "Benton Hiatt",
                "Elouise Dietrich", "Sidney Mesa", "Temika Wharton", "Gerald Owens", "Loren Ingram", "Wilma Watkins",
                "Lewis Vasquez", "Jean Hammond", "Lynda Mcdaniel", "Carrie Mcgee", "Stanley Beck", "Gretchen Simon",
                "Bill Castillo", "Eduardo Houston", "Alton Rivera", "Bessie Daniels", "Estelle Garcia",
                "Kimberly Hunt", "Joseph Blake", "Emma Townsend", "Julius Fowler", "Jessie Simmons",
                "Lawrence Ballard", "Darin Nichols", "Yvette Reynolds", "Blanca Fletcher", "Bob Willis",
                "Natalie Mccormic", "Guillermo Wheeler", "Freddie Morrison", "Lucille Flowers", "Marion Wade",
                "Patricia Holt", "Fred Gregory", "Douglas Cross", "Terence Bailey", "Lamar Ramos", "Patrick Mcguire",
                "Sara Freeman", "Harriet Matthews", "Antonio Ferguson", "Guy Wells", "Julie Butler", "Timothy Harris",
                "Valerie Kim", "Homer Hines", "Myrtle Murray", "Jasmine Meyer", "Edith Webster", "Glenda Jenkins",
                "Tammy Garner", "Lindsay Wilkerso", "Jeffrey Riley", "Glen Barker", "Rosemary Manning",
                "Tricia Allison", "Rachael Owen", "Bertha Mccarthy", "Krystal Lopez", "Tom Fernandez",
                "Jeannette Wilson", "Boyd Stevens", "Sonia Jennings", "Orville Wise", "Velma Becker",
                "Trevor Martinez", "Troy Gross", "Eileen Gordon", "Lionel Brock", "Jennifer Patrick", "Fannie Carter",
                "Josephine Logan", "Garry Fitzgerald", "Sandy Mills", "Salvador Walsh", "Justin Ramirez", "Janie Love",
                "Pete Diaz", "Alberta Bass", "Tara Colon", "Barbara Knight", "Johnathan Munoz", "Owen Burke",
                "Essie Santiago", "Cecilia Wagner", "Alexandra Bennett", "Olivia Perez", "Eric Mason", "Sherry Harper",
                "Ruth Barrett", "Luis Webb", "Luke Soto", "George Underwood", "Sheri Clarke", "Herman Fisher",
                "Jeanne Banks", "Craig Morales", "Carole Andrews", "Brandi Bowen", "Dustin Chandler", "Hazel Perkins",
                "Jay Steele", "Brandy Schwartz" };

        return names[index % names.length];
    }

    @Override
    public void generateVisitsAndOrders(UserLogin userLogin, String _fromDate, String _toDate) {
        SimpleDate fromDate = SimpleDate.createByIsoDate(_fromDate, null);
        SimpleDate toDate = SimpleDate.createByIsoDate(_toDate, null);
        if (fromDate == null || toDate == null || fromDate.compareTo(toDate) >= 0) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        HashMap<ObjectId, Set<ObjectId>> routeIdsBySalesman = new HashMap<ObjectId, Set<ObjectId>>();

        List<SimpleDate> dates = calendarConfigRepository.getWorkingDays(userLogin.getClientId(), new Period(fromDate,
                toDate));

        List<User> supervisors = userRepository.getUsersByRole(userLogin.getClientId(), HardCodeUtils.ROLE_SUPERVISOR);
        HashMap<ObjectId, List<Distributor>> distributorBySupervisor = new HashMap<ObjectId, List<Distributor>>();
        HashMap<ObjectId, List<User>> salesmanByDistributor = new HashMap<ObjectId, List<User>>();
        HashMap<ObjectId, List<Customer>> customerBySalesman = new HashMap<ObjectId, List<Customer>>();
        for (User supervisor : supervisors) {
            List<Distributor> distributors = distributorRepository.getDistributorsBySupervisors(
                    userLogin.getClientId(), Arrays.asList(supervisor.getId()));
            distributorBySupervisor.put(supervisor.getId(), distributors);

            for (Distributor distributor : distributors) {
                List<User> salesmen = userRepository.getSalesmenByDistributors(userLogin.getClientId(),
                        Arrays.asList(distributor.getId()));
                salesmanByDistributor.put(distributor.getId(), salesmen);

                for (User salesman : salesmen) {
                    Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(userLogin.getClientId(),
                            Collections.singleton(salesman.getId()));
                    routeIdsBySalesman.put(salesman.getId(), routeIds);

                    customerBySalesman.put(salesman.getId(), customerRepository.getCustomersByRoute(
                            userLogin.getClientId(), routeIds, null, null, null, null));
                }
            }
        }

        List<Product> products = productRepository.getAll(userLogin.getClientId(), null);

        Random random = new Random();

        SalesConfig salesConfig = getSalesConfig(userLogin);

        for (SimpleDate currentDate : dates) {
            System.out.println(currentDate.getIsoDate());
            for (User supervisor : supervisors) {
                List<Distributor> distributors = distributorBySupervisor.get(supervisor.getId());
                for (Distributor distributor : distributors) {
                    List<User> salesmen = salesmanByDistributor.get(distributor.getId());
                    for (User salesman : salesmen) {
                        Set<ObjectId> routeIds = routeIdsBySalesman.get(salesman.getId());
                        List<Customer> customers = customerRepository.getCustomersByRouteDate(userLogin.getClientId(),
                                routeIds, null, currentDate, null, null);

                        SimpleDate time = new SimpleDate(currentDate);
                        time.setHour(8);

                        Set<ObjectId> customeIds = new HashSet<ObjectId>();
                        for (Customer customer : customers) {
                            customeIds.add(customer.getId());

                            // 90% visit
                            if (random.nextDouble() < 0.9) {
                                LocationDto locationDto = null;
                                // 90% xac dinh vi tri
                                if (random.nextDouble() < 0.9) {

                                    // 10% far
                                    if (random.nextDouble() < 0.1) {
                                        double[] l = generateLocation(customer.getLocation()[0],
                                                customer.getLocation()[1], salesConfig.getVisitDistanceKPI(),
                                                salesConfig.getVisitDistanceKPI() * 2);
                                        locationDto = new LocationDto(l[1], l[0]);
                                    } else {
                                        double[] l = generateLocation(customer.getLocation()[0],
                                                customer.getLocation()[1], 0.0, salesConfig.getVisitDistanceKPI());
                                        locationDto = new LocationDto(l[1], l[0]);
                                    }

                                }

                                Visit visit = createVisit(userLogin, salesman, distributor, customer, locationDto);

                                visit.setClosed(false);

                                visit.setStartTime(time);
                                // from 8 min -> 12 min
                                double duration = 8 + (7 * random.nextDouble());
                                time = DateTimeUtils.addMinutes(time, (int) duration);
                                visit.setEndTime(time);

                                visit.setDuration(SimpleDate.getDuration(visit.getStartTime(), visit.getEndTime()));
                                visit.setErrorDuration(visit.getDuration() < getSalesConfig(userLogin)
                                        .getVisitDurationKPI());

                                visit.setVisitStatus(Visit.VISIT_STATUS_VISITED);

                                // 80% co order
                                if (random.nextDouble() < 0.8) {
                                    OrderCreateDto order = getRandomOrder(products);
                                    visit.setCode(codeGeneratorRepository.getOrderCode(userLogin.getClientId()
                                            .toString()));
                                    visit.setDeliveryType(Order.DELIVERY_TYPE_IMMEDIATE);

                                    promotionEngine.calculatePromotion(userLogin, order, visit);

                                    visit.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
                                    visit.setApproveTime(time);
                                    visit.setApproveUser(new UserEmbed(supervisor));
                                }

                                visit = visitRepository.save(userLogin.getClientId(), visit);

                                // lan ghe tham tiep theo sau 10 phut
                                time = DateTimeUtils.addMinutes(time, 2);
                            }
                        }

                        int numberOrderUnplanned = (int) (3.0 * random.nextDouble());
                        for (int i = 0; i < numberOrderUnplanned; i++) {
                            List<Customer> allCustomers = customerBySalesman.get(salesman.getId());
                            Customer customer = null;
                            for (Customer c : allCustomers) {
                                if (!customeIds.contains(c.getId())) {
                                    customer = c;
                                    break;
                                }
                            }

                            Order order = new Order();
                            initPOWhenCreate(Order.class, userLogin, order);
                            order.setSalesman(new UserEmbed(salesman));
                            order.setCustomer(new CustomerEmbed(customer));
                            order.setDistributor(new DistributorEmbed(distributor));
                            order.setCreatedTime(time);
                            order.setCode(codeGeneratorRepository.getOrderCode(userLogin.getClientId().toString()));

                            order.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
                            order.setApproveTime(time);
                            order.setApproveUser(new UserEmbed(supervisor));

                            order.setDeliveryType(Order.DELIVERY_TYPE_IMMEDIATE);

                            promotionEngine.calculatePromotion(userLogin, getRandomOrder(products), order);

                            order = orderRepository.save(userLogin.getClientId(), order);

                            time = DateTimeUtils.addMinutes(time, 10);
                        }

                    }
                }
            }

        }

    }

    private Visit createVisit(UserLogin userLogin, User salesman, Distributor distributor, Customer customer,
            LocationDto locationDto) {
        Visit visit = new Visit();

        initPOWhenCreate(Visit.class, userLogin, visit);

        // LOCATION STATUS
        if (!LocationUtils.checkLocationValid(customer.getLocation())) {
            visit.setLocationStatus(Visit.LOCATION_STATUS_CUSTOMER_UNLOCATED);
            visit.setLocation(LocationUtils.convert(locationDto));
            visit.setCustomerLocation(null);
        } else {
            if (!LocationUtils.checkLocationValid(locationDto)) {
                visit.setLocationStatus(Visit.LOCATION_STATUS_UNLOCATED);
                visit.setLocation(null);
                visit.setCustomerLocation(customer.getLocation());
            } else {
                double distance = LocationUtils.calculateDistance(customer.getLocation()[1], customer.getLocation()[0],
                        locationDto.getLatitude(), locationDto.getLongitude());
                if (distance > getSalesConfig(userLogin).getVisitDistanceKPI()) {
                    visit.setLocationStatus(Visit.LOCATION_STATUS_TOO_FAR);
                } else {
                    visit.setLocationStatus(Visit.LOCATION_STATUS_LOCATED);
                }

                visit.setLocation(LocationUtils.convert(locationDto));
                visit.setCustomerLocation(customer.getLocation());
                visit.setDistance(distance);
            }
        }

        visit.setDistributor(new DistributorEmbed(distributor));
        visit.setSalesman(new UserEmbed(salesman));
        visit.setCustomer(new CustomerEmbed(customer));

        return visit;
    }

    private OrderCreateDto getRandomOrder(List<Product> products) {
        OrderCreateDto order = new OrderCreateDto();
        order.setDiscountAmt(BigDecimal.ZERO);
        List<OrderDetailCreatedDto> details = new LinkedList<OrderDetailCreatedDto>();
        Random random = new Random();

        double nbProduct = products.size();

        int nbProductOfOrder = 2 + ((int) (random.nextDouble() * 3.0));
        List<Integer> productIndexs = new ArrayList<Integer>(nbProductOfOrder);

        for (int i = 0; i < nbProductOfOrder; i++) {
            int index = (int) (random.nextDouble() * nbProduct);
            while (productIndexs.contains(index) || index < 0 || index >= products.size()) {
                index = (int) (random.nextDouble() * nbProduct);
            }
            productIndexs.add(index);
        }

        for (Integer productIndex : productIndexs) {
            int quantity = 3 + ((int) (random.nextDouble() * 4.0));
            details.add(createOrderDetail(products.get(productIndex).getId().toString(), quantity));
        }

        order.setDetails(details);

        return order;
    }

    private OrderDetailCreatedDto createOrderDetail(String productId, int quantity) {
        OrderDetailCreatedDto dto = new OrderDetailCreatedDto();
        dto.setProductId(productId);
        dto.setQuantity(new BigDecimal(quantity));

        return dto;
    }

    @Override
    public void generateOishiProduct(UserLogin userLogin) {
        Object[][] datas = { { "APSMC-ST_8g", "Snack Que Nhân Cafe Moka-8g", 16000, "STA" },
                { "APSMK-ST_8g", "Snack Que Nhân  Sữa-8g", 16000, "STA" },
                { "CBM-U_45g", "Snack Cua Xốt Chua Ngọt-45g", 40000, "UA/UB/UC" },
                { "COC-U_45g", "Snack Phô Mát Miếng-45g", 40000, "UA/UB/UC" },
                { "DCPL_350ml", "Vị Soda Chanh - 350ml", 27000, "RTD-350ml" },
                { "DCPO_350ml", "Vị Soda Cam Sành - 350ml", 27000, "RTD-350ml" },
                { "DCPP_350ml", "Vị Soda Chanh Dây - 350ml", 27000, "RTD-350ml" },
                { "FLTPS-U_40g", "Snack Khoai Tây Vị Muối-40g", 40000, "UA/UB/UC" },
                { "FLTSW-U_40g", "Snack Khoai Tây Vị Tảo Biển-40g", 40000, "UA/UB/UC" },
                { "KCCH-U_40g", "Snack Bắp Nón Vị Phômai-40g", 40000, "UA/UB/UC" },
                { "KCOR-U_40g", "Snack Bắp Nón Vị Tự Nhiên-40g", 40000, "UA/UB/UC" },
                { "MCPS-U_45g", "Snack Chay Vị Da Heo Quay-45g", 40000, "UA/UB/UC" },
                { "MGBQ-U_40g", "Snack Ngũ Cốc Vị Thịt Nướng-40g", 40000, "UA/UB/UC" },
                { "MGCH-U_40g", "Snack Ngũ Cốc Vị Phô mát-40g", 40000, "UA/UB/UC" },
                { "ONR-U_45g", "Snack Hành-45g", 40000, "UA/UB/UC" },
                { "PCBQ-U_45g", "Snack Bí Đỏ Vị Bò Nướng-45g", 40000, "UA/UB/UC" },
                { "PCCR-U_35g", "Khoai Tây Prims Vị Sườn Nướng Cà phê-35g", 40000, "UE" },
                { "PCSC-U_35g", "Khoai Tây Prims vị Kem Chua và Hành Tây-35g", 40000, "UE" },
                { "PFZPS-C_27g", "Khoai Tây Que Vị Muối -27g", 40000, "CA" },
                { "PFZSW-C_27g", "Khoai Tây Que Vị Tảo Biển -27g", 40000, "CA" },
                { "PLC-L_20g", "Snack Nhân Sôcôla-20g", 16000, "LA" },
                { "PLCM-L_20g", "Snack Nhân Vị Sữa Dừa-20g", 16000, "LA" },
                { "PNTCM-L_20g", "Snack Nhân Đậu Phộng Vị Nước Cốt Dừa-20g", 16000, "LP" },
                { "PNTSQ-L_20g", "Snack Nhân Đậu Phộng Vị Mực Cay-20g", 16000, "LP" },
                { "SCP-U_45g", "Snack Bắp Ngọt -45g", 40000, "UA/UB/UC" },
                { "SOPC-U_45g", "Snack Tôm Cay-45g", 40000, "UA/UB/UC" },
                { "SQCH-U_45g", "Bánh Phồng Mực Inđônêxia-45g", 40000, "UA/UB/UC" },
                { "SQR-U_40g", "Snack Mực Ống Vị  Sốt Cay Ngọt-40g", 40000, "UA/UB/UC" },
                { "SSOPC-U_45g", "Snack Tôm Cay Đặc Biệt-45g", 40000, "UA/UB/UC" },
                { "TMCH-U_45g", "Snack Bắp Vị Phô Mai-45g", 40000, "UA/UB/UC" },
                { "TMT-U_40g", "Snack Cà Chua-40g", 40000, "UA/UB/UC" } };

        HashMap<String, ProductCategory> categoryMap = new HashMap<String, ProductCategory>();
        for (Object[] data : datas) {
            ProductCategory productCategory = categoryMap.get(data[3]);
            if (productCategory == null) {
                productCategory = new ProductCategory();
                initPOWhenCreate(ProductCategory.class, userLogin, productCategory);
                productCategory.setDraft(false);
                productCategory.setName((String) data[3]);

                productCategory = productCategoryRepository.save(userLogin.getClientId(), productCategory);

                categoryMap.put((String) data[3], productCategory);
            }
        }

        UOM uom = uomRepository.getById(userLogin.getClientId(), new ObjectId("5577de7dd4c6724d3365eb4c"));

        for (Object[] data : datas) {
            Product product = new Product();
            product.setDraft(false);

            initPOWhenCreate(Product.class, userLogin, product);

            product.setCode((String) data[0]);
            product.setName((String) data[1]);
            product.setPrice(new BigDecimal((int) data[2]));
            product.setOutput(new BigDecimal(1));
            product.setDescription(data[0] + " - " + data[1] + " - " + data[3]);
            product.setUom(uom);
            product.setProductCategory(categoryMap.get(data[3]));

            String path = "/Users/kimhatrung/Documents/Work/Oishi/product_image/";
            String extension = ".jpg";
            File file = new File(path + (String) data[0] + extension);
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                String id = fileEngine
                        .storeImage(fileInputStream, file.getName(), "image/jpeg", new DbFileMeta(), null);
                product.setPhoto(id);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                throw new BusinessException("file not found");
            }

            productRepository.save(userLogin.getClientId(), product);

        }

    }

}
