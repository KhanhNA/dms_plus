package com.viettel.backend.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.cache.redis.CacheManager;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.Target;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.HomepageDto;
import com.viettel.backend.dto.ProductSalesSummaryDto;
import com.viettel.backend.dto.SalesmanWithScoreDto;
import com.viettel.backend.dto.embed.Result;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.HomepageService;
import com.viettel.backend.service.sub.VisitSubService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class HomepageServiceImpl extends AbstractService implements HomepageService {

    private static final int TOP_QUANTITY = 5;

    private static final long serialVersionUID = 2334147624997307241L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private VisitSubService visitSubService;

    private boolean checkRolesAllowed(UserLogin userLogin) {
        if (userLogin.getRoles() == null || userLogin.getRoles().isEmpty()) {
            return false;
        }

        return Arrays.asList(HardCodeUtils.ROLE_ADMIN, HardCodeUtils.ROLE_SUPERVISOR, HardCodeUtils.ROLE_DISTRIBUTOR)
                .contains(userLogin.getRoles().get(0));
    }

    private List<Distributor> getDistributors(UserLogin userLogin) {
        ObjectId clientId = userLogin.getClientId();

        if (userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            return distributorRepository.getAll(clientId, null);
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            return distributorRepository.getDistributorsBySupervisors(clientId, Arrays.asList(userLogin.getUserId()));
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
            User user = getCurrentUser(userLogin);
            if (user.getDistributor() != null) {
                return Arrays.asList(distributorRepository.getById(clientId, user.getDistributor().getId()));
            }
        }

        return Collections.emptyList();
    }

    @Override
    public HomepageDto getHomepage(UserLogin userLogin) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ObjectId clientId = userLogin.getClientId();

        // FIND ALL DISTRIBUTOR
        List<Distributor> distributors = getDistributors(userLogin);
        if (distributors == null || distributors.isEmpty()) {
            return new HomepageDto();
        }
        Set<ObjectId> distributorIds = new HashSet<ObjectId>();
        for (Distributor distributor : distributors) {
            distributorIds.add(distributor.getId());
        }

        // FIND ALL Salesman
        List<User> salesmen = userRepository.getSalesmenByDistributors(clientId, distributorIds);
        Set<ObjectId> salesmanIds = new HashSet<ObjectId>();
        for (User salesman : salesmen) {
            salesmanIds.add(salesman.getId());
        }

        SimpleDate today = DateTimeUtils.getToday();
        Period lastMonthPeriod = DateTimeUtils.getPeriodLastMonth();

        HomepageDto dto = new HomepageDto();

        // LAST MONTH
        List<SimpleDate> lastMonthWDs = calendarConfigRepository.getWorkingDays(clientId, lastMonthPeriod);
        if (lastMonthWDs != null && !lastMonthWDs.isEmpty()) {
            BigDecimal nbDayLastMonth = new BigDecimal(lastMonthWDs.size());

            Collection<Map<String, Double>> lastMonthDataMaps = getDataMapsByDistributors(distributorIds,
                    lastMonthPeriod.getFromDate(), true);

            BigDecimal lastMonthRevenue = sumDataMaps(lastMonthDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_REVENUE);
            BigDecimal lastMonthNbOrder = sumDataMaps(lastMonthDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER);
            BigDecimal lastMonthNbVisit = sumDataMaps(lastMonthDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT);

            dto.setOrderByDayLastMonth(lastMonthNbOrder.divide(nbDayLastMonth, 2, BigDecimal.ROUND_HALF_UP));
            dto.setNbVisitByDayLastMonth(lastMonthNbVisit.divide(nbDayLastMonth, 2, BigDecimal.ROUND_HALF_UP));

            if (lastMonthNbOrder.compareTo(BigDecimal.ZERO) > 0) {
                dto.setRevenueByOrderLastMonth(lastMonthRevenue.divide(lastMonthNbOrder, 2, BigDecimal.ROUND_HALF_UP));
            }
        }

        // TODAY
        Collection<Map<String, Double>> todayDataMaps = getDataMapsByDistributors(distributorIds, today, false);
        BigDecimal todayRevenue = sumDataMaps(todayDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_REVENUE);
        BigDecimal todayNbOrder = sumDataMaps(todayDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER);
        BigDecimal todayNbVisit = sumDataMaps(todayDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT);

        dto.setRevenueToday(todayRevenue);
        dto.setNbVisit(todayNbVisit.intValue());
        dto.setNbVisitPlanned(visitSubService.getNumberVisitPlanned(userLogin, DateTimeUtils.getPeriodToday(),
                salesmanIds));

        dto.setNbVisitHasOrder(sumDataMaps(todayDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_WITH_PO)
                .intValue());
        dto.setNbVisitErrorTime(sumDataMaps(todayDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_ERROR_DURATION)
                .intValue());
        dto.setNbVisitErrorPosition(sumDataMaps(todayDataMaps,
                CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_ERROR_POSITION).intValue());
        dto.setNbOrderNoVisit(sumDataMaps(todayDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER_WITHOUT_VISIT)
                .intValue());

        // THIS MONTH WITHOUT TODAY
        List<SimpleDate> thisMonthWithoutTodayWDs = calendarConfigRepository.getWorkingDays(clientId, new Period(
                DateTimeUtils.getFirstOfThisMonth(), today));
        if (thisMonthWithoutTodayWDs != null && !thisMonthWithoutTodayWDs.isEmpty()) {
            BigDecimal nbDayThisMonthWithoutToday = new BigDecimal(thisMonthWithoutTodayWDs.size());

            Collection<Map<String, Double>> thisMonthDataMaps = getDataMapsByDistributors(distributorIds, today, true);

            BigDecimal thisMonthRevenue = sumDataMaps(thisMonthDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_REVENUE)
                    .subtract(todayRevenue);
            BigDecimal thisMonthNbOrder = sumDataMaps(thisMonthDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER)
                    .subtract(todayNbOrder);
            BigDecimal thisMonthNbVisit = sumDataMaps(thisMonthDataMaps, CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT)
                    .subtract(todayNbVisit);

            dto.setOrderByDayThisMonth(thisMonthNbOrder.divide(nbDayThisMonthWithoutToday, 2, BigDecimal.ROUND_HALF_UP));
            dto.setNbVisitByDayThisMonth(thisMonthNbVisit.divide(nbDayThisMonthWithoutToday, 2,
                    BigDecimal.ROUND_HALF_UP));

            if (thisMonthNbOrder.compareTo(BigDecimal.ZERO) > 0) {
                dto.setRevenueByOrderThisMonth(thisMonthRevenue.divide(thisMonthNbOrder, 2, BigDecimal.ROUND_HALF_UP));
            }

            dto.setRevenueThisMonth(thisMonthRevenue);
        }

        // TARGET
        List<Target> targets = targetRepository.getTargetsBySalesmen(userLogin.getClientId(), salesmanIds,
                today.getMonth(), today.getYear(), null, null);
        BigDecimal revenueTargetThisMonth = BigDecimal.ZERO;
        if (targets != null) {
            for (Target target : targets) {
                revenueTargetThisMonth = revenueTargetThisMonth.add(target.getRevenue());
            }
        }
        dto.setRevenueTargetThisMonth(revenueTargetThisMonth);

        // TOP 5 PRODUCT WITHOUT TODAY
        if (userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            dto.setTop5products(sumProductForAdmin(userLogin));
        }
        // Other
        else {
            dto.setTop5products(sumProductByDistributor(userLogin, distributorIds));
        }

        // Top 5 worst salesman without today
        dto.setWorst5salesmen(getWorstSalesman(userLogin, salesmen));

        return dto;
    }

    private Collection<Map<String, Double>> getDataMapsByDistributors(Collection<ObjectId> distributorIds,
            SimpleDate date, boolean isMonthly) {
        if (date == null) {
            throw new IllegalArgumentException("date is null");
        }

        if (distributorIds == null || distributorIds.isEmpty()) {
            return Collections.emptySet();
        }

        StringBuilder dateKey = new StringBuilder();
        if (!isMonthly) {
            dateKey.append(date.getIsoDate());
        } else {
            dateKey.append(date.getIsoMonth());
        }

        HashSet<Map<String, Double>> dataMaps = new HashSet<Map<String, Double>>();

        try {
            for (ObjectId distributorId : distributorIds) {
                StringBuilder key = new StringBuilder(distributorId.toString()).append("-").append(dateKey.toString());
                Map<String, Double> dataMap = cacheManager.getDistributorDataCache().get(key.toString()).entries();
                if (dataMap != null) {
                    dataMaps.add(dataMap);
                }
            }
        } catch (Exception e) {
            logger.error("cache error", e);
            return Collections.emptySet();
        }

        return dataMaps;
    }

    private BigDecimal sumDataMaps(Collection<Map<String, Double>> dataMaps, String distributorDataType) {
        if (dataMaps == null || distributorDataType == null) {
            throw new IllegalArgumentException();
        }

        if (dataMaps.isEmpty()) {
            return BigDecimal.ZERO;
        }

        BigDecimal total = BigDecimal.ZERO;

        for (Map<String, Double> dataMap : dataMaps) {
            if (dataMap != null) {
                Double value = dataMap.get(distributorDataType);
                if (value != null) {
                    total = total.add(new BigDecimal(value));
                }
            }
        }

        return total;
    }

    private List<ProductSalesSummaryDto> sumProductByDistributor(UserLogin userLogin,
            Collection<ObjectId> distributorIds) {
        try {
            if (distributorIds == null || distributorIds.isEmpty()) {
                return Collections.emptyList();
            }

            // Get all of this month (include today)
            String thisMonth = DateTimeUtils.getFirstOfThisMonth().getIsoMonth();
            Map<ObjectId, Integer> monthlySummaries = new HashMap<>(50);

            for (ObjectId distributorId : distributorIds) {
                String keyDistributorThisMonth = distributorId.toString() + "-" + thisMonth;
                Map<ObjectId, Integer> productSummaries = cacheManager.getDistributorProductQuantityCache()
                        .get(keyDistributorThisMonth).entries();
                if (!CollectionUtils.isEmpty(productSummaries)) {
                    for (Map.Entry<ObjectId, Integer> entry : productSummaries.entrySet()) {
                        if (monthlySummaries.containsKey(entry.getKey())) {
                            monthlySummaries.put(entry.getKey(),
                                    monthlySummaries.get(entry.getKey()) + entry.getValue());
                        } else {
                            monthlySummaries.put(entry.getKey(), entry.getValue());
                        }
                    }
                }
            }

            // Get all of today
            String today = DateTimeUtils.getToday().getIsoDate();
            Map<ObjectId, Integer> todaySummaries = new HashMap<>(50);

            for (ObjectId distributorId : distributorIds) {
                String keyDistributorToday = distributorId.toString() + "-" + today;
                Map<ObjectId, Integer> productSummaries = cacheManager.getDistributorProductQuantityCache()
                        .get(keyDistributorToday).entries();
                if (!CollectionUtils.isEmpty(productSummaries)) {
                    for (Map.Entry<ObjectId, Integer> entry : productSummaries.entrySet()) {
                        if (todaySummaries.containsKey(entry.getKey())) {
                            todaySummaries.put(entry.getKey(), todaySummaries.get(entry.getKey()) + entry.getValue());
                        } else {
                            todaySummaries.put(entry.getKey(), entry.getValue());
                        }
                    }
                }
            }

            // Subtract today
            for (Map.Entry<ObjectId, Integer> entry : monthlySummaries.entrySet()) {
                if (todaySummaries.containsKey(entry.getKey())) {
                    monthlySummaries.put(entry.getKey(), entry.getValue() - todaySummaries.get(entry.getKey()));
                }
            }

            // Convert to list and Sort
            if (!CollectionUtils.isEmpty(monthlySummaries)) {
                List<ProductSalesSummaryDto> productSummaries = new ArrayList<ProductSalesSummaryDto>(
                        monthlySummaries.size());
                Map<ObjectId, Product> products = productRepository.getMapByIds(userLogin.getClientId(),
                        monthlySummaries.keySet());
                for (Map.Entry<ObjectId, Integer> entry : monthlySummaries.entrySet()) {
                    Product product = products.get(entry.getKey());
                    if (product == null) {
                        logger.error("Unnable to find product with id: " + entry.getKey());
                        continue;
                    }
                    ProductSalesSummaryDto summaryDto = new ProductSalesSummaryDto(new ProductEmbed(product),
                            entry.getValue());
                    productSummaries.add(summaryDto);
                }

                Collections.sort(productSummaries);
                return TOP_QUANTITY > productSummaries.size() ? productSummaries : productSummaries.subList(0,
                        TOP_QUANTITY);
            } else {
                return Collections.<ProductSalesSummaryDto> emptyList();
            }
        } catch (Exception e) {
            logger.error("cache error", e);
            return Collections.emptyList();
        }
    }

    private List<ProductSalesSummaryDto> sumProductForAdmin(UserLogin userLogin) {
        try {
            // Get this month (include today)
            String thisMonth = DateTimeUtils.getFirstOfThisMonth().getIsoMonth();
            Map<ObjectId, Integer> monthlySummaries = cacheManager.getAdminProductQuantityCache().get(thisMonth)
                    .entries();

            // Get today
            String today = DateTimeUtils.getToday().getIsoDate();
            Map<ObjectId, Integer> todaySummaries = cacheManager.getAdminProductQuantityCache().get(today).entries();
            ;

            // Subtract today
            if (!CollectionUtils.isEmpty(monthlySummaries) && !CollectionUtils.isEmpty(todaySummaries)) {
                for (Map.Entry<ObjectId, Integer> entry : monthlySummaries.entrySet()) {
                    if (todaySummaries.containsKey(entry.getKey())) {
                        monthlySummaries.put(entry.getKey(), entry.getValue() - todaySummaries.get(entry.getKey()));
                    }
                }
            }

            // Convert to list and Sort
            if (!CollectionUtils.isEmpty(monthlySummaries)) {
                List<ProductSalesSummaryDto> productSummaries = new ArrayList<ProductSalesSummaryDto>(
                        monthlySummaries.size());
                Map<ObjectId, Product> products = productRepository.getMapByIds(userLogin.getClientId(),
                        monthlySummaries.keySet());
                for (Map.Entry<ObjectId, Integer> entry : monthlySummaries.entrySet()) {
                    Product product = products.get(entry.getKey());
                    if (product == null) {
                        logger.error("Unnable to find product with id: " + entry.getKey());
                        continue;
                    }
                    ProductSalesSummaryDto summaryDto = new ProductSalesSummaryDto(new ProductEmbed(product),
                            entry.getValue());
                    productSummaries.add(summaryDto);
                }

                Collections.sort(productSummaries);
                return TOP_QUANTITY > productSummaries.size() ? productSummaries : productSummaries.subList(0,
                        TOP_QUANTITY);
            } else {
                return Collections.<ProductSalesSummaryDto> emptyList();
            }
        } catch (Exception e) {
            logger.error("cache error", e);
            return Collections.emptyList();
        }
    }

    private List<SalesmanWithScoreDto> getWorstSalesman(UserLogin userLogin, Collection<User> salesmen) {
        try {
            // Get all salesman with score of yesterday
            Map<User, Integer> salesmanVisitedMap = getSalesmanVisitedMap(userLogin, salesmen);
            if (CollectionUtils.isEmpty(salesmanVisitedMap)) {
                return Collections.emptyList();
            }

            SimpleDate today = DateTimeUtils.getToday();
            Period thisMonthPeriod = new Period(DateTimeUtils.getFirstOfThisMonth(), today);

            List<SalesmanWithScoreDto> dtos = new ArrayList<>(salesmanVisitedMap.size());
            for (Entry<User, Integer> entry : salesmanVisitedMap.entrySet()) {
                int nbVisitPlanned = visitSubService.getNumberVisitPlanned(userLogin, thisMonthPeriod,
                        Collections.singleton(entry.getKey().getId()));
                dtos.add(new SalesmanWithScoreDto(new UserEmbed(entry.getKey()), new Result(nbVisitPlanned, entry
                        .getValue())));
            }

            Collections.sort(dtos);
            dtos = TOP_QUANTITY > dtos.size() ? dtos : dtos.subList(0, TOP_QUANTITY);

            Collections.sort(dtos);

            return dtos;
        } catch (Exception e) {
            logger.error("cache error", e);
            return Collections.emptyList();
        }
    }

    /*
     * Get salesman with score of yesterday for admin
     */
    private Map<User, Integer> getSalesmanVisitedMap(UserLogin userLogin, Collection<User> salesmen) {
        SimpleDate today = DateTimeUtils.getToday();
        String isoMonth = today.getIsoMonth();
        String isoDate = today.getIsoDate();

        Map<User, Integer> salesmanVisitedMap = new HashMap<>(salesmen.size());
        for (User salesman : salesmen) {
            if (salesman.getDistributor() == null || salesman.getDistributor().getId() == null) {
                continue;
            }

            Integer nbVisitThisMonth = cacheManager.getDistributorSalesmanVisitedCache()
                    .get(salesman.getDistributor().getId().toString() + "-" + isoMonth).get(salesman.getId());
            nbVisitThisMonth = nbVisitThisMonth == null ? 0 : nbVisitThisMonth;

            Integer nbVisitToday = cacheManager.getDistributorSalesmanVisitedCache()
                    .get(salesman.getDistributor().getId().toString() + "-" + isoDate).get(salesman.getId());
            nbVisitToday = nbVisitToday == null ? 0 : nbVisitToday;

            salesmanVisitedMap.put(salesman, nbVisitThisMonth - nbVisitToday);
        }

        return salesmanVisitedMap;
    }

}
