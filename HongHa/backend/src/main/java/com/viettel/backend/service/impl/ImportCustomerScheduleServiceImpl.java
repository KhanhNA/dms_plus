package com.viettel.backend.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Route;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.dto.ImportConfirmDto;
import com.viettel.backend.dto.ImportConfirmDto.RowData;
import com.viettel.backend.dto.ImportResultDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.ImportCustomerScheduleService;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.StringUtils;

@Service
public class ImportCustomerScheduleServiceImpl extends AbstractImportService implements ImportCustomerScheduleService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RouteRepository routeRepository;

    private boolean checkRolesAllowed(UserLogin userLogin) {
        if (userLogin.getRoles() == null || userLogin.getRoles().isEmpty()) {
            return false;
        }

        return Arrays.asList(HardCodeUtils.ROLE_ADMIN, HardCodeUtils.ROLE_SUPERVISOR).contains(
                userLogin.getRoles().get(0));
    }

    @Override
    public byte[] getTemplate(UserLogin userLogin, String _distributorId) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Distributor distributor = getMadatoryPO(userLogin, _distributorId, distributorRepository);

        List<Customer> customers = customerRepository.getCustomersByDistributors(userLogin.getClientId(),
                Collections.singleton(distributor.getId()), false, null, null, null, null);

        List<Route> routes = routeRepository.getRoutesByDistributors(userLogin.getClientId(),
                Collections.singleton(distributor.getId()));

        HashMap<ObjectId, Route> routeMap = buildPOByIdMap(Route.class, routes);

        ClientConfig clientConfig = getClientCondig(userLogin);

        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet scheduleSheet = workbook.createSheet("Schedule");

        XSSFRow row = scheduleSheet.createRow(0);
        row.createCell(0).setCellValue("Code");
        row.createCell(1).setCellValue("Name");
        row.createCell(2).setCellValue("District");
        row.createCell(3).setCellValue("Route");
        row.createCell(4).setCellValue("Monday");
        row.createCell(5).setCellValue("Tuesday");
        row.createCell(6).setCellValue("Wednesday");
        row.createCell(7).setCellValue("Thursday");
        row.createCell(8).setCellValue("Friday");
        row.createCell(9).setCellValue("Saturday");
        row.createCell(10).setCellValue("Sunday");

        if (clientConfig.getNumberWeekOfFrequency() > 1) {
            for (int i = 1; i <= clientConfig.getNumberWeekOfFrequency(); i++) {
                row.createCell(10 + i).setCellValue("W" + i);
            }
        }

        int index = 1;
        for (Customer customer : customers) {
            row = scheduleSheet.createRow(index);

            CustomerSchedule customerSchedule = customer.getSchedule();
            if (customerSchedule != null) {
                Route route = routeMap.get(customerSchedule.getRouteId());

                if (clientConfig.isComplexSchedule()) {
                    if (customerSchedule.getItems() != null) {
                        for (CustomerScheduleItem item : customerSchedule.getItems()) {
                            fillDataToRow(row, clientConfig, customer, route, item);
                            index++;
                        }
                    }
                } else {
                    fillDataToRow(row, clientConfig, customer, route, customerSchedule.getItem());
                    index++;
                }
            } else {
                fillDataToRow(row, clientConfig, customer, null, null);
                index++;
            }
        }

        index = 0;
        XSSFSheet routeSheet = workbook.createSheet("Route");
        for (Route route : routes) {
            row = routeSheet.createRow(index);
            row.createCell(0).setCellValue(route.getName());
            index++;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            workbook.close();

            return baos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void fillDataToRow(XSSFRow row, ClientConfig clientConfig, Customer customer, Route route,
            CustomerScheduleItem item) {
        row.createCell(0).setCellValue(customer.getCode());
        row.createCell(1).setCellValue(customer.getName());
        row.createCell(2).setCellValue(customer.getDistrict().getName());

        if (route != null) {
            row.createCell(3).setCellValue(route.getName());
        }

        if (item != null) {
            row.createCell(4).setCellValue(!item.isMonday() ? null : "x");
            row.createCell(5).setCellValue(!item.isTuesday() ? null : "x");
            row.createCell(6).setCellValue(!item.isWednesday() ? null : "x");
            row.createCell(7).setCellValue(!item.isThursday() ? null : "x");
            row.createCell(8).setCellValue(!item.isFriday() ? null : "x");
            row.createCell(9).setCellValue(!item.isSaturday() ? null : "x");
            row.createCell(10).setCellValue(!item.isSunday() ? null : "x");

            if (clientConfig.getFirstDayOfWeek() > 1) {
                for (int i = 1; i <= clientConfig.getNumberWeekOfFrequency(); i++) {
                    if (item.getWeeks() != null && item.getWeeks().contains(i)) {
                        row.createCell(10 + i).setCellValue("x");
                    }
                }
            }
        }
    }

    private I_CellValidator[] getValidators(UserLogin userLogin, ObjectId distributorId) {
        ClientConfig clientConfig = getClientCondig(userLogin);

        List<Customer> customers = customerRepository.getCustomersByDistributors(userLogin.getClientId(),
                Collections.singleton(distributorId), false, null, null, null, null);
        Map<String, Customer> customerByCode = new HashMap<String, Customer>();
        for (Customer customer : customers) {
            customerByCode.put(customer.getCode().trim().toUpperCase(), customer);
        }

        List<Route> routes = routeRepository.getRoutesByDistributors(userLogin.getClientId(),
                Collections.singleton(distributorId));
        Map<String, Route> routeByName = new HashMap<String, Route>();
        for (Route route : routes) {
            routeByName.put(route.getName().trim().toUpperCase(), route);
        }

        LinkedList<I_CellValidator> validators = new LinkedList<AbstractImportService.I_CellValidator>();
        validators.add(new MultiCellValidator(new StringMandatoryCellValidator(), new ReferenceCellValidator<Customer>(
                customerByCode)));
        validators.add(new NoValidator());
        validators.add(new NoValidator());
        validators.add(new ReferenceCellValidator<Route>(routeByName));
        validators.add(null);
        validators.add(null);
        validators.add(null);
        validators.add(null);
        validators.add(null);
        validators.add(null);
        validators.add(null);

        if (clientConfig.getNumberWeekOfFrequency() > 1) {
            for (int i = 0; i < clientConfig.getNumberWeekOfFrequency(); i++) {
                validators.add(null);
            }
        }
        
        I_CellValidator[] array = new I_CellValidator[validators.size()];
        validators.toArray(array);
        return array;
    }

    @Override
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId) {
        Distributor distributor = getMadatoryPO(userLogin, _distributorId, distributorRepository);
        I_CellValidator[] validators = getValidators(userLogin, distributor.getId());
        return getErrorRows(userLogin, fileId, validators);
    }

    @Override
    public ImportResultDto doImport(UserLogin userLogin, String _distributorId, String fileId) {
        ClientConfig clientConfig = getClientCondig(userLogin);

        Distributor distributor = getMadatoryPO(userLogin, _distributorId, distributorRepository);
        I_CellValidator[] validators = getValidators(userLogin, distributor.getId());

        ImportConfirmDto dto = getValidRows(userLogin, fileId, validators);

        for (RowData row : dto.getRowDatas()) {
            Customer customer = (Customer) row.getDatas().get(0);
            Route route = (Route) row.getDatas().get(3);
            if (route != null) {
                CustomerScheduleItem item = new CustomerScheduleItem();
                item.setMonday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(4), true));
                item.setTuesday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(5), true));
                item.setWednesday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(6), true));
                item.setThursday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(7), true));
                item.setFriday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(8), true));
                item.setSaturday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(9), true));
                item.setSunday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(10), true));

                boolean weeksValid = true;
                if (clientConfig.getNumberWeekOfFrequency() > 1) {
                    weeksValid = false;
                    List<Integer> weeks = new LinkedList<Integer>();
                    for (int i = 1; i <= clientConfig.getNumberWeekOfFrequency(); i++) {
                        if (!StringUtils.isNullOrEmpty((String) row.getDatas().get(10 + i))) {
                            weeks.add(i);
                            weeksValid = true;
                        }
                    }
                    item.setWeeks(weeks);
                }

                if ((item.isMonday() || item.isTuesday() || item.isWednesday() || item.isThursday() || item.isFriday()
                        || item.isSaturday() || item.isSunday())
                        && weeksValid) {
                    CustomerSchedule customerSchedule = new CustomerSchedule();
                    customerSchedule.setRouteId(route.getId());

                    if (clientConfig.isComplexSchedule()) {
                        customerSchedule.setItems(Arrays.asList(item));
                    } else {
                        customerSchedule.setItem(item);
                    }

                    customer.setSchedule(customerSchedule);
                } else {
                    customer.setSchedule(null);
                }
            } else {
                customer.setSchedule(null);
            }

            customerRepository.save(userLogin.getClientId(), customer);
        }

        return new ImportResultDto(dto.getTotal(), dto.getRowDatas().size());
    }

}
