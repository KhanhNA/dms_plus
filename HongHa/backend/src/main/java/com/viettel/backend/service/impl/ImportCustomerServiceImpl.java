package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.District;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.ImportConfirmDto;
import com.viettel.backend.dto.ImportResultDto;
import com.viettel.backend.dto.ImportConfirmDto.RowData;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.ImportCustomerService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;
import com.viettel.backend.util.StringUtils;

@Service
public class ImportCustomerServiceImpl extends AbstractImportService implements ImportCustomerService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private FileEngine fileEngine;

    private boolean checkRolesAllowed(UserLogin userLogin) {
        if (userLogin.getRoles() == null || userLogin.getRoles().isEmpty()) {
            return false;
        }

        return Arrays.asList(HardCodeUtils.ROLE_ADMIN).contains(userLogin.getRoles().get(0));
    }

    // PUBLIC
    @Override
    public String getImportCustomerTemplate(UserLogin userLogin) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ClientConfig clientConfig = getClientCondig(userLogin);

        if (clientConfig.getImportCustomerTemplate() == null) {
            throw new UnsupportedOperationException("no import customer template");
        }

        return clientConfig.getImportCustomerTemplate().getFileId();
    }

    private I_CellValidator[] getValidators(UserLogin userLogin) {
        Map<String, CustomerType> customerTypeMap = getCustomerTypeMap(userLogin.getClientId());
        Map<String, District> districtMap = getDistrictMap(userLogin.getClientId());

        List<Customer> customers = customerRepository.getAll(userLogin.getClientId(), null);
        HashSet<String> customerNames = new HashSet<String>();
        for (Customer customer : customers) {
            if (!StringUtils.isNullOrEmpty(customer.getName(), true)) {
                customerNames.add(customer.getName().trim().toUpperCase());
            }
        }

        I_CellValidator[] validators = new I_CellValidator[] {
                new MultiCellValidator(new StringMandatoryCellValidator(),
                        new StringUniqueCellValidator(customerNames), new MaxLengthCellValidator(50)),
                new MultiCellValidator(new StringMandatoryCellValidator(), new ReferenceCellValidator<CustomerType>(
                        customerTypeMap)),
                new MultiCellValidator(new StringMandatoryCellValidator(), new ReferenceCellValidator<District>(
                        districtMap)),
                new MultiCellValidator(new StringMandatoryCellValidator(), new MaxLengthCellValidator(15)), null, null,
                null, new LatitudeCellValidator(), new LongitudeCellValidator() };
        return validators;
    }

    @Override
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId) {
        ObjectId distributorId = ObjectIdUtils.getObjectId(_distributorId, null);
        if (distributorId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        I_CellValidator[] validators = getValidators(userLogin);
        return getErrorRows(userLogin, fileId, validators);
    }

    @Override
    public ImportResultDto importCustomer(UserLogin userLogin, String _distributorId, String fileId) {
        Distributor distributor = getMadatoryPO(userLogin, _distributorId, distributorRepository);

        User currentUser = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());

        I_CellValidator[] validators = getValidators(userLogin);
        ImportConfirmDto dto = getValidRows(userLogin, fileId, validators);

        List<String> codes = codeGeneratorRepository.getBatchCustomerCode(userLogin.getClientId().toString(), dto
                .getRowDatas().size());

        List<Customer> customersToInsert = new ArrayList<Customer>(dto.getRowDatas().size());
        int index = 0;
        for (RowData row : dto.getRowDatas()) {
            Customer customer = new Customer();

            initPOWhenCreate(Customer.class, userLogin, customer);
            customer.setDraft(false);
            customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
            customer.setCreatedTime(DateTimeUtils.getCurrentTime());
            customer.setCreatedBy(new UserEmbed(currentUser));
            customer.setCode(codes.get(index));
            customer.setDistributor(new DistributorEmbed(distributor));

            customer.setName((String) row.getDatas().get(0));
            customer.setCustomerType((CustomerType) row.getDatas().get(1));
            customer.setDistrict((District) row.getDatas().get(2));
            customer.setMobile((String) row.getDatas().get(3));
            customer.setPhone(row.getDatas().get(4) == null ? null : (String) row.getDatas().get(4));
            customer.setContact(row.getDatas().get(5) == null ? null : (String) row.getDatas().get(5));
            customer.setEmail(row.getDatas().get(6) == null ? null : (String) row.getDatas().get(6));
            customer.setLocation(new double[] { (double) row.getDatas().get(7), (double) row.getDatas().get(8) });

            customersToInsert.add(customer);

            index++;
        }

        customerRepository.insertCustomers(userLogin.getClientId(), customersToInsert);

        return new ImportResultDto(dto.getTotal(), customersToInsert.size());
    }

    private Map<String, CustomerType> getCustomerTypeMap(ObjectId clientId) {
        List<CustomerType> customerTypes = customerTypeRepository.getAll(clientId, null);

        HashMap<String, CustomerType> map = new HashMap<String, CustomerType>();

        if (customerTypes != null) {
            for (CustomerType customerType : customerTypes) {
                map.put(customerType.getName().toUpperCase(), customerType);
            }
        }

        return map;
    }

    private Map<String, District> getDistrictMap(ObjectId clientId) {
        List<District> districts = districtRepository.getAll(clientId, null);

        HashMap<String, District> map = new HashMap<String, District>();

        if (districts != null) {
            for (District district : districts) {
                map.put(district.getName().toUpperCase(), district);
            }
        }

        return map;
    }

}
