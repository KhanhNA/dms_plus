package com.viettel.backend.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.ProductCategory;
import com.viettel.backend.domain.UOM;
import com.viettel.backend.dto.ImportConfirmDto;
import com.viettel.backend.dto.ImportProductPhotoDto;
import com.viettel.backend.dto.ImportResultDto;
import com.viettel.backend.dto.ImportConfirmDto.RowData;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ProductCategoryRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.UOMRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.ImportProductService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.StringUtils;

@Service
public class ImportProductServiceImpl extends AbstractImportService implements ImportProductService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private UOMRepository uomRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileEngine fileEngine;

    private boolean checkRolesAllowed(UserLogin userLogin) {
        if (userLogin.getRoles() == null || userLogin.getRoles().isEmpty()) {
            return false;
        }

        return Arrays.asList(HardCodeUtils.ROLE_ADMIN).contains(userLogin.getRoles().get(0));
    }

    // PUBLIC
    @Override
    public String getImportProductTemplate(UserLogin userLogin) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        ClientConfig clientConfig = getClientCondig(userLogin);

        if (clientConfig.getImportProductTemplate() == null) {
            throw new UnsupportedOperationException("no import customer template");
        }

        return clientConfig.getImportProductTemplate().getFileId();
    }

    private I_CellValidator[] getValidators(UserLogin userLogin) {
        Map<String, ProductCategory> productCategoryMap = getProductCategory(userLogin.getClientId());
        Map<String, UOM> uomMap = getUom(userLogin.getClientId());

        List<Product> products = productRepository.getAll(userLogin.getClientId(), null);
        HashSet<String> productNames = new HashSet<String>();
        HashSet<String> productCodes = new HashSet<String>();
        for (Product product : products) {
            if (!StringUtils.isNullOrEmpty(product.getName(), true)) {
                productNames.add(product.getName().trim().toUpperCase());
            }

            if (!StringUtils.isNullOrEmpty(product.getCode(), true)) {
                productCodes.add(product.getCode().trim().toUpperCase());
            }
        }

        return new I_CellValidator[] {
                new MultiCellValidator(new StringMandatoryCellValidator(), new StringUniqueCellValidator(productCodes),
                        new MaxLengthCellValidator(30), new RegexCellValidator("^\\S+$")),
                new MultiCellValidator(new StringMandatoryCellValidator(), new StringUniqueCellValidator(productNames),
                        new MaxLengthCellValidator(50)),
                new MultiCellValidator(new StringMandatoryCellValidator(), new ReferenceCellValidator<ProductCategory>(
                        productCategoryMap)),
                new MultiCellValidator(new StringMandatoryCellValidator(), new ReferenceCellValidator<UOM>(uomMap)),
                new MinMaxCellValidator(1d, 1000000000d, 0), null };
    }

    @Override
    public ImportConfirmDto verify(UserLogin userLogin, String fileId) {
        I_CellValidator[] validators = getValidators(userLogin);
        return getErrorRows(userLogin, fileId, validators);
    }

    @Override
    public ImportConfirmDto confirm(UserLogin userLogin, String fileId) {
        I_CellValidator[] validators = getValidators(userLogin);
        return getValidRows(userLogin, fileId, validators);
    }

    @Override
    public ImportResultDto importProduct(UserLogin userLogin, String fileId, ImportProductPhotoDto photoDto) {
        I_CellValidator[] validators = getValidators(userLogin);
        ImportConfirmDto dto = getValidRows(userLogin, fileId, validators);

        if (photoDto.getPhotos() == null || photoDto.getPhotos().length != dto.getRowDatas().size()) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        List<Product> productToInsert = new ArrayList<Product>(dto.getRowDatas().size());
        int index = 0;
        for (RowData row : dto.getRowDatas()) {
            String photo = photoDto.getPhotos()[index];

            Product product = new Product();

            initPOWhenCreate(Product.class, userLogin, product);
            product.setDraft(false);
            product.setOutput(BigDecimal.ONE);

            product.setCode((String) row.getDatas().get(0));
            product.setName((String) row.getDatas().get(1));
            product.setProductCategory((ProductCategory) row.getDatas().get(2));
            product.setUom((UOM) row.getDatas().get(3));
            product.setPrice(new BigDecimal((Double) row.getDatas().get(4)));
            product.setDescription((String) row.getDatas().get(5));

            product.setPhoto(photo);

            productToInsert.add(product);

            index++;
        }

        productRepository.insertProducts(userLogin.getClientId(), productToInsert);

        return new ImportResultDto(dto.getTotal(), productToInsert.size());
    }

    private Map<String, ProductCategory> getProductCategory(ObjectId clientId) {
        List<ProductCategory> productCategories = productCategoryRepository.getAll(clientId, null);

        HashMap<String, ProductCategory> map = new HashMap<String, ProductCategory>();

        if (productCategories != null) {
            for (ProductCategory productCategory : productCategories) {
                map.put(productCategory.getName().toUpperCase(), productCategory);
            }
        }

        return map;
    }

    private Map<String, UOM> getUom(ObjectId clientId) {
        List<UOM> uoms = uomRepository.getAll(clientId, null);

        HashMap<String, UOM> map = new HashMap<String, UOM>();

        if (uoms != null) {
            for (UOM uom : uoms) {
                map.put(uom.getName().toUpperCase(), uom);
            }
        }

        return map;
    }

}
