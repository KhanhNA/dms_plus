package com.viettel.backend.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.cache.redis.CacheManager;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Route;
import com.viettel.backend.domain.Target;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.dto.CustomerSaleResultDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.MobileDashboardDto;
import com.viettel.backend.dto.OrderSummaryDto;
import com.viettel.backend.dto.SalesResultDaily;
import com.viettel.backend.dto.embed.Result;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.MobileDashboardService;
import com.viettel.backend.service.sub.VisitSubService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class MobileDashboardServiceImpl extends AbstractSalesmanService implements MobileDashboardService {

    private static final long serialVersionUID = -9137978270275195070L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private RouteRepository routeRepository;
    
    @Autowired
    private VisitSubService visitSubService;

    @Autowired
    private CacheManager cacheManager;

    private Collection<ObjectId> getSalesmanIds(UserLogin userLogin) {
        Collection<ObjectId> salesmanIds = null;
        if (userLogin.hasRole(HardCodeUtils.ROLE_SALESMAN)) {
            salesmanIds = Arrays.asList(userLogin.getUserId());
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_STORE_CHECKER)) {
            salesmanIds = userRepository.getSalesmanIdsByStoreCheckers(userLogin.getClientId(),
                    Arrays.asList(userLogin.getUserId()));
        }

        return salesmanIds;
    }

    @Override
    public MobileDashboardDto getDashboard(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SALESMAN) && !userLogin.hasRole(HardCodeUtils.ROLE_STORE_CHECKER)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Collection<ObjectId> salesmanIds = getSalesmanIds(userLogin);

        // GET TARGET
        SimpleDate today = DateTimeUtils.getToday();
        List<Target> targets = targetRepository.getTargetsBySalesmen(userLogin.getClientId(), salesmanIds,
                today.getMonth(), today.getYear(), null, null);

        BigDecimal revenue = BigDecimal.ZERO;
        int nbVisit = 0;

        // CACHE
        try {
            List<User> salesmen = userRepository.getListByIds(userLogin.getClientId(), salesmanIds);
            for (User salesman : salesmen) {
                if (salesman.getDistributor() != null && salesman.getDistributor().getId() != null) {
                    String key = salesman.getDistributor().getId().toString() + "-" + today.getIsoMonth();
                    Double salesmanRevenue = cacheManager.getDistributorSalesmanRevenueCache().get(key)
                            .get(salesman.getId());

                    revenue = revenue.add(new BigDecimal(salesmanRevenue == null ? 0 : salesmanRevenue));

                    Integer nbVisitThisMonth = cacheManager.getDistributorSalesmanVisitedCache()
                            .get(salesman.getDistributor().getId().toString() + "-" + today.getIsoMonth())
                            .get(salesman.getId());
                    nbVisit += nbVisitThisMonth == null ? 0 : nbVisitThisMonth;
                }
            }
        } catch (Exception e) {
            logger.error("cache error", e);
        }

        MobileDashboardDto dashboardDto = new MobileDashboardDto();
        if (targets != null && !targets.isEmpty()) {
            BigDecimal revenueTarget = BigDecimal.ZERO;

            for (Target target : targets) {
                revenueTarget = revenueTarget.add(target.getRevenue());
            }

            dashboardDto.setRevenue(new Result(revenueTarget, revenue));
        } else {
            dashboardDto.setRevenue(new Result(null, revenue));
        }

        // VISIT
        int nbVisitPlanned = visitSubService.getNumberVisitPlanned(userLogin,
                DateTimeUtils.getPeriodByMonth(today.getMonth(), today.getYear()), salesmanIds);
        dashboardDto.setNbVisit(new Result(nbVisitPlanned, nbVisit));

        // GET WORKING DAY OF THIS MONTH
        List<SimpleDate> workingDays = calendarConfigRepository.getWorkingDays(userLogin.getClientId(),
                DateTimeUtils.getPeriodByMonth(today.getMonth(), today.getYear()));
        if (workingDays == null || workingDays.isEmpty()) {
            dashboardDto.setNbSaleDays(new Result(0, 0));
        } else {
            int todayIndexOfMonth = 0;
            for (SimpleDate date : workingDays) {
                if (date.compareTo(today) > 0) {
                    break;
                }
                todayIndexOfMonth++;
            }

            dashboardDto.setNbSaleDays(new Result(workingDays.size(), todayIndexOfMonth));
        }

        return dashboardDto;
    }

    @Override
    public ListJson<CustomerSaleResultDto> getDashboardByCustomer(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SALESMAN) && !userLogin.hasRole(HardCodeUtils.ROLE_STORE_CHECKER)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Collection<ObjectId> salesmanIds = getSalesmanIds(userLogin);

        Set<ObjectId> customerIds = new HashSet<ObjectId>();
        Map<ObjectId, BigDecimal[]> customerMaps = new HashMap<ObjectId, BigDecimal[]>();

        // Kiem tra tat ca cac don hang da phat sinh do salesman dat duoc trong
        // thang
        List<Order> orders = orderRepository.getOrderBySalesmen(userLogin.getClientId(), salesmanIds,
                DateTimeUtils.getPeriodThisMonthUntilToday(), null, null, null);
        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                if (order.getCustomer() == null || order.getCustomer().getId() == null) {
                    continue;
                }

                List<OrderDetail> details = order.getDetails();
                if (details == null || details.isEmpty()) {
                    continue;
                }

                ObjectId customerId = order.getCustomer().getId();
                BigDecimal[] values = customerMaps.get(customerId);
                if (values == null) {
                    customerIds.add(customerId);

                    // san luong / doanh thu
                    values = new BigDecimal[] { BigDecimal.ZERO, BigDecimal.ZERO };
                }

                for (OrderDetail detail : details) {
                    values[0] = values[0].add(detail.getOutput());
                }

                if (order.getGrandTotal() != null && order.getGrandTotal().signum() > 0) {
                    values[1] = values[1].add(order.getGrandTotal());
                }

                customerMaps.put(customerId, values);
            }
        }

        // Tat ca danh sach khach hang do salesman dang quan ly
        Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(userLogin.getClientId(), salesmanIds);
        Set<ObjectId> allCustomerIds = customerRepository.getCustomerIdsByRoute(userLogin.getClientId(),
                routeIds, null);
        if (allCustomerIds == null || allCustomerIds.isEmpty()) {
            if (customerIds.isEmpty()) {
                return ListJson.<CustomerSaleResultDto> emptyList();
            }
        } else {
            // Bo sung vao danh sach khach hang da phat sinh don hang
            for (ObjectId customerId : allCustomerIds) {
                customerIds.add(customerId);
            }
        }

        // Danh sach khach hang do salesman quan ly va khong quan ly nhung co
        // phat sinh don hang trong thang
        List<Customer> customers = customerRepository.getListByIdsWithDeactivated(userLogin.getClientId(), customerIds);
        if (customers == null || customers.isEmpty()) {
            return ListJson.<CustomerSaleResultDto> emptyList();
        }

        List<CustomerSaleResultDto> results = new ArrayList<CustomerSaleResultDto>();
        for (Customer customer : customers) {
            CustomerSaleResultDto dto = new CustomerSaleResultDto(customer, BigDecimal.ZERO, BigDecimal.ZERO);

            BigDecimal[] values = customerMaps.get(customer.getId());
            if (values != null) {
                dto.setProductivity(values[0]);
                dto.setRevenue(values[1]);
            }

            results.add(dto);
        }

        return new ListJson<CustomerSaleResultDto>(results, Long.valueOf(results.size()));
    }

    @Override
    public ListJson<OrderSummaryDto> getDashboardByCustomerDetail(UserLogin userLogin, String customerId) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SALESMAN) && !userLogin.hasRole(HardCodeUtils.ROLE_STORE_CHECKER)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Collection<ObjectId> salesmanIds = getSalesmanIds(userLogin);
        Customer customer = getMadatoryPOWithDeactivated(userLogin, customerId, customerRepository);
        if (salesmanIds == null || customer.getSchedule() == null || customer.getSchedule().getRouteId() == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Route schedule = routeRepository.getById(userLogin.getClientId(), customer.getSchedule().getRouteId());
        if (schedule.getSalesman() == null || !salesmanIds.contains(schedule.getSalesman().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        List<Order> orders = orderRepository.getOrderBySalesmenAndCustomers(userLogin.getClientId(), salesmanIds,
                Arrays.asList(customer.getId()), DateTimeUtils.getPeriodThisMonthUntilToday(), null, null, null);
        if (orders == null || orders.isEmpty()) {
            return ListJson.<OrderSummaryDto> emptyList();
        }

        List<OrderSummaryDto> results = new ArrayList<OrderSummaryDto>();
        for (Order order : orders) {
            List<OrderDetail> details = order.getDetails();
            if (details == null || details.isEmpty()) {
                continue;
            }

            BigDecimal productivity = BigDecimal.ZERO;
            for (OrderDetail detail : details) {
                productivity = productivity.add(detail.getOutput());
            }

            OrderSummaryDto dto = new OrderSummaryDto();
            dto.setId(order.getId().toString());
            dto.setCode(order.getCode());
            dto.setCreatedTime(order.getCreatedTime().getIsoTime());
            dto.setGrandTotal(order.getGrandTotal());
            dto.setProductivity(productivity);

            results.add(dto);
        }

        return new ListJson<OrderSummaryDto>(results, Long.valueOf(results.size()));
    }

    @Override
    public ListJson<SalesResultDaily> getDashboardByDay(UserLogin userLogin) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SALESMAN) && !userLogin.hasRole(HardCodeUtils.ROLE_STORE_CHECKER)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        List<SimpleDate> workingDays = calendarConfigRepository.getWorkingDays(userLogin.getClientId(),
                DateTimeUtils.getPeriodThisMonthUntilToday());
        if (workingDays == null || workingDays.isEmpty()) {
            return ListJson.<SalesResultDaily> emptyList();
        }

        Collection<ObjectId> salesmanIds = getSalesmanIds(userLogin);

        Map<Long, BigDecimal[]> orderDateMaps = new HashMap<Long, BigDecimal[]>();
        List<Order> orders = orderRepository.getOrderBySalesmen(userLogin.getClientId(), salesmanIds,
                DateTimeUtils.getPeriodThisMonthUntilToday(), null, null, null);
        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                List<OrderDetail> details = order.getDetails();
                if (details == null || details.isEmpty()) {
                    continue;
                }

                Long createdDateValue = SimpleDate.truncDate(order.getCreatedTime()).getValue();
                BigDecimal[] values = orderDateMaps.get(createdDateValue);
                if (values == null) {
                    values = new BigDecimal[] { BigDecimal.ZERO, BigDecimal.ZERO };
                }

                for (OrderDetail detail : details) {
                    values[0] = values[0].add(detail.getOutput());
                }

                values[1] = values[1].add(order.getGrandTotal());
                orderDateMaps.put(createdDateValue, values);
            }
        }

        long tomorrowValue = DateTimeUtils.getTomorrow().getValue();
        List<SalesResultDaily> results = new ArrayList<SalesResultDaily>();
        for (SimpleDate workingDay : workingDays) {
            BigDecimal[] values = orderDateMaps.get(workingDay.getValue());
            if (values != null || workingDay.getValue() < tomorrowValue) {
                SalesResultDaily dto = new SalesResultDaily();
                dto.setDate(workingDay.getIsoDate());
                dto.setProductivity(BigDecimal.ZERO);
                dto.setRevenue(BigDecimal.ZERO);

                if (values != null) {
                    dto.setProductivity(values[0]);
                    dto.setRevenue(values[1]);
                }

                results.add(dto);
            }
        }

        return new ListJson<SalesResultDaily>(results, Long.valueOf(results.size()));
    }

    @Override
    public ListJson<CustomerSaleResultDto> getDashboardByDayDetail(UserLogin userLogin, String isoDate) {
        if (!userLogin.hasRole(HardCodeUtils.ROLE_SALESMAN) && !userLogin.hasRole(HardCodeUtils.ROLE_STORE_CHECKER)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        SimpleDate date = SimpleDate.createByIsoDate(isoDate, null);
        if (date == null) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        Collection<ObjectId> salesmanIds = getSalesmanIds(userLogin);

        List<Order> orders = orderRepository.getOrderBySalesmen(userLogin.getClientId(), salesmanIds,
                DateTimeUtils.getPeriodOneDay(date), null, null, null);
        if (orders == null || orders.isEmpty()) {
            return ListJson.<CustomerSaleResultDto> emptyList();
        }

        Map<ObjectId, BigDecimal[]> customerMaps = new HashMap<ObjectId, BigDecimal[]>();
        for (Order order : orders) {
            if (order.getCustomer() == null || order.getCustomer().getId() == null) {
                continue;
            }

            List<OrderDetail> details = order.getDetails();
            if (details == null || details.isEmpty()) {
                continue;
            }

            ObjectId customerId = order.getCustomer().getId();
            BigDecimal[] values = customerMaps.get(customerId);
            if (values == null) {
                // san luong / doanh thu
                values = new BigDecimal[] { BigDecimal.ZERO, BigDecimal.ZERO };
            }

            for (OrderDetail detail : details) {
                values[0] = values[0].add(detail.getOutput());
            }

            if (order.getGrandTotal() != null && order.getGrandTotal().signum() > 0) {
                values[1] = values[1].add(order.getGrandTotal());
            }

            customerMaps.put(customerId, values);
        }

        if (customerMaps.isEmpty()) {
            return ListJson.<CustomerSaleResultDto> emptyList();
        }

        Collection<Customer> customers = customerRepository
                .getListByIdsWithDeactivated(userLogin.getClientId(), customerMaps.keySet());
        if (customers == null || customers.isEmpty() || customers.size() != customerMaps.size()) {
            return ListJson.<CustomerSaleResultDto> emptyList();
        }

        List<CustomerSaleResultDto> results = new ArrayList<CustomerSaleResultDto>();
        for (Customer customer : customers) {
            CustomerSaleResultDto dto = new CustomerSaleResultDto(customer, BigDecimal.ZERO, BigDecimal.ZERO);

            BigDecimal[] values = customerMaps.get(customer.getId());
            if (values != null) {
                dto.setProductivity(values[0]);
                dto.setRevenue(values[1]);
            }

            results.add(dto);
        }

        return new ListJson<CustomerSaleResultDto>(results, Long.valueOf(results.size()));
    }

}