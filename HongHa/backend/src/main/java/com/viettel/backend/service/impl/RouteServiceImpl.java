package com.viettel.backend.service.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Route;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.RouteCreateDto;
import com.viettel.backend.dto.RouteDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.RouteService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class RouteServiceImpl extends
        BasicNameCategoryByDistributorServiceImpl<Route, RouteDto, RouteDto, RouteCreateDto> implements RouteService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private RouteRepository routeRepository;
    
    @Autowired
    private DistributorRepository distributorRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @Override
    protected Collection<ObjectId> getDistributorIdsForFilter(UserLogin userLogin) {
        if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorOfSupervisorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
            if (distributorOfSupervisorIds == null) {
                return Collections.emptySet();
            }
            return distributorOfSupervisorIds;
        }

        return null;
    }
    
    @Override
    protected void checkBeforeSetActive(UserLogin userLogin, ObjectId id, boolean active) {
        super.checkBeforeSetActive(userLogin, id, active);
            
        if (!active) {
            if (customerRepository.checkRouteUsed(userLogin.getClientId(), id)) {
                throw new BusinessException(ExceptionCode.ROUTE_IS_USING);
            }
        }
    }

    @Override
    public BasicCategoryRepository<Route> getRepository() {
        return routeRepository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_ADMIN) || userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR);
    }

    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.PERMISSION_DENIED;
    }

    @Override
    public Route createDomain(UserLogin userLogin, RouteCreateDto createdto) {
        Route domain = new Route();
        domain.setDraft(true);

        initPOWhenCreate(Route.class, userLogin, domain);

        updateDomain(userLogin, domain, createdto);

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, Route domain, RouteCreateDto createdto) {
        if (domain.isDraft()) {
            checkMandatoryParams(createdto, createdto.getName(), createdto.getDistributorId());

            Distributor distributor = getMadatoryPO(userLogin, createdto.getDistributorId(), distributorRepository);
            domain.setDistributor(new DistributorEmbed(distributor));

            if (routeRepository.existsByNameByDistributor(userLogin.getClientId(), distributor.getId(),
                    domain.getId(), createdto.getName())) {
                throw new BusinessException(ExceptionCode.SAME_NAME_EXIST);
            }
            domain.setName(createdto.getName());
        } else {
            checkMandatoryParams(createdto);

            if (createdto.getSalesmanId() != null) {
                User salesman = getMadatoryPO(userLogin, createdto.getSalesmanId(), userRepository);
                domain.setSalesman(new UserEmbed(salesman));
            } else {
                domain.setSalesman(null);
            }
        }

    }

    @Override
    public RouteDto createSimpleDto(UserLogin userLogin, Route domain) {
        return new RouteDto(domain);
    }

    @Override
    public RouteDto createDetailDto(UserLogin userLogin, Route domain) {
        return createSimpleDto(userLogin, domain);
    }

}
