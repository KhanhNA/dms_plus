package com.viettel.backend.service.impl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.SalesmanSalesReportDto;
import com.viettel.backend.dto.report.DaySalesReportDto;
import com.viettel.backend.dto.report.DistributorSalesReportDto;
import com.viettel.backend.dto.report.ProductSalesReportDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.ProductCategoryRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SalesReportService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.HardCodeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SalesReportServiceImpl extends AbstractService implements SalesReportService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private UserRepository userRepository;

    private boolean checkRolesAllowed(UserLogin userLogin) {
        if (userLogin.getRoles() == null || userLogin.getRoles().isEmpty()) {
            return false;
        }

        return Arrays.asList(HardCodeUtils.ROLE_ADMIN, HardCodeUtils.ROLE_SUPERVISOR, HardCodeUtils.ROLE_DISTRIBUTOR)
                .contains(userLogin.getRoles().get(0));
    }

    private List<Order> getOrders(UserLogin userLogin, int month, int year) {
        ObjectId clientId = userLogin.getClientId();

        Period period = DateTimeUtils.getPeriodByMonth(month, year);

        if (userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            return orderRepository.getOrders(clientId, period, null, null, null);
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                    Arrays.asList(userLogin.getUserId()));

            return orderRepository.getOrderByDistributors(clientId, distributorIds, period, null, null, null);
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
            User user = getCurrentUser(userLogin);

            if (user.getDistributor() != null) {
                return orderRepository.getOrderByDistributors(clientId, Arrays.asList(user.getDistributor().getId()),
                        period, null, null, null);
            }
        }

        return Collections.emptyList();
    }

    private List<Distributor> getDistributors(UserLogin userLogin) {
        ObjectId clientId = userLogin.getClientId();

        if (userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            return distributorRepository.getAll(clientId, null);
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            return distributorRepository.getDistributorsBySupervisors(clientId, Arrays.asList(userLogin.getUserId()));
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
            User user = getCurrentUser(userLogin);
            if (user.getDistributor() != null) {
                return Arrays.asList(distributorRepository.getById(clientId, user.getDistributor().getId()));
            }
        }

        return Collections.emptyList();
    }

    private List<User> getSalesman(UserLogin userLogin) {
        ObjectId clientId = userLogin.getClientId();

        if (userLogin.hasRole(HardCodeUtils.ROLE_ADMIN)) {
            return userRepository.getUsersByRole(clientId, HardCodeUtils.ROLE_SALESMAN);
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_SUPERVISOR)) {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                    Arrays.asList(userLogin.getUserId()));

            return userRepository.getSalesmenByDistributors(clientId, distributorIds);
        } else if (userLogin.hasRole(HardCodeUtils.ROLE_DISTRIBUTOR)) {
            User user = getCurrentUser(userLogin);

            if (user.getDistributor() != null) {
                return userRepository.getSalesmenByDistributors(clientId, Arrays.asList(user.getDistributor().getId()));
            }
        }

        return Collections.emptyList();
    }

    @Override
    public ListJson<DaySalesReportDto> getDaySalesReport(UserLogin userLogin, int month, int year) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        Period period = DateTimeUtils.getPeriodByMonth(month, year);

        List<Order> orders = getOrders(userLogin, month, year);

        HashMap<String, DaySalesReportDto> reportByDate = new HashMap<String, DaySalesReportDto>();
        HashMap<String, HashSet<ObjectId>> distributorIdsByDate = new HashMap<String, HashSet<ObjectId>>();
        HashMap<String, HashSet<ObjectId>> customerIdsByDate = new HashMap<String, HashSet<ObjectId>>();
        HashMap<String, HashSet<ObjectId>> salesmanIdsByDate = new HashMap<String, HashSet<ObjectId>>();
        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                String isoDate = order.getApproveTime().getIsoDate();

                DaySalesReportDto report = reportByDate.get(isoDate);
                if (report == null) {
                    report = new DaySalesReportDto(order.getApproveTime().getIsoDate(), BigDecimal.ZERO,
                            BigDecimal.ZERO, 0l, 0l, 0l, 0l);
                    reportByDate.put(isoDate, report);
                }

                HashSet<ObjectId> distributorIds = distributorIdsByDate.get(isoDate);
                if (distributorIds == null) {
                    distributorIds = new HashSet<ObjectId>();
                    distributorIdsByDate.put(isoDate, distributorIds);
                }
                distributorIds.add(order.getDistributor().getId());

                HashSet<ObjectId> customerIds = customerIdsByDate.get(isoDate);
                if (customerIds == null) {
                    customerIds = new HashSet<ObjectId>();
                    customerIdsByDate.put(isoDate, customerIds);
                }
                customerIds.add(order.getCustomer().getId());

                HashSet<ObjectId> salesmanIds = salesmanIdsByDate.get(isoDate);
                if (salesmanIds == null) {
                    salesmanIds = new HashSet<ObjectId>();
                    salesmanIdsByDate.put(isoDate, salesmanIds);
                }
                salesmanIds.add(order.getSalesman().getId());

                report.setRevenue(report.getRevenue().add(order.getGrandTotal()));
                // TODO productivity ???
                report.setNbOrder(report.getNbOrder() + 1);
                report.setNbDistributor(distributorIds.size());
                report.setNbCustomer(customerIds.size());
                report.setNbSalesman(salesmanIds.size());
            }
        }

        List<DaySalesReportDto> dtos = new LinkedList<DaySalesReportDto>();
        SimpleDate tempDate = period.getFromDate();
        while (tempDate.compareTo(period.getToDate()) < 0) {
            String isoDate = tempDate.getIsoDate();
            DaySalesReportDto report = reportByDate.get(isoDate);
            if (report == null) {
                report = new DaySalesReportDto(isoDate, BigDecimal.ZERO, BigDecimal.ZERO, 0l, 0l, 0l, 0l);
            }
            dtos.add(report);

            tempDate = DateTimeUtils.addDays(tempDate, 1);
        }

        return new ListJson<DaySalesReportDto>(dtos, (long) dtos.size());
    }

    public ListJson<DistributorSalesReportDto> getDistributorSalesReport(UserLogin userLogin, int month, int year) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        List<Order> orders = getOrders(userLogin, month, year);

        HashMap<ObjectId, DistributorSalesReportDto> reportByDistributor = new HashMap<ObjectId, DistributorSalesReportDto>();
        HashMap<ObjectId, HashSet<ObjectId>> customerIdsByDistributor = new HashMap<ObjectId, HashSet<ObjectId>>();
        HashMap<ObjectId, HashSet<ObjectId>> salesmanIdsByDistributor = new HashMap<ObjectId, HashSet<ObjectId>>();

        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                DistributorSalesReportDto report = reportByDistributor.get(order.getDistributor().getId());
                if (report == null) {
                    report = new DistributorSalesReportDto(order.getDistributor(), BigDecimal.ZERO, BigDecimal.ZERO,
                            0l, 0l, 0l);
                    reportByDistributor.put(order.getDistributor().getId(), report);
                }

                HashSet<ObjectId> customerIds = customerIdsByDistributor.get(order.getDistributor().getId());
                if (customerIds == null) {
                    customerIds = new HashSet<ObjectId>();
                    customerIdsByDistributor.put(order.getDistributor().getId(), customerIds);
                }
                customerIds.add(order.getCustomer().getId());

                HashSet<ObjectId> salesmanIds = salesmanIdsByDistributor.get(order.getDistributor().getId());
                if (salesmanIds == null) {
                    salesmanIds = new HashSet<ObjectId>();
                    salesmanIdsByDistributor.put(order.getDistributor().getId(), salesmanIds);
                }
                salesmanIds.add(order.getSalesman().getId());

                report.setRevenue(report.getRevenue().add(order.getGrandTotal()));
                // TODO productivity ???
                report.setNbOrder(report.getNbOrder() + 1);
                report.setNbCustomer(customerIds.size());
                report.setNbSalesman(salesmanIds.size());
            }
        }

        List<Distributor> distributors = getDistributors(userLogin);
        List<DistributorSalesReportDto> dtos = new LinkedList<DistributorSalesReportDto>();
        for (Distributor distributor : distributors) {
            DistributorSalesReportDto report = reportByDistributor.get(distributor.getId());
            if (report == null) {
                report = new DistributorSalesReportDto(new DistributorEmbed(distributor), BigDecimal.ZERO,
                        BigDecimal.ZERO, 0l, 0l, 0l);
            }
            dtos.add(report);
        }

        return new ListJson<DistributorSalesReportDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<ProductSalesReportDto> getProductSalesReport(UserLogin userLogin, String _productCategoryId,
            int month, int year) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        List<Order> orders = getOrders(userLogin, month, year);

        ObjectId productCategoryId = ObjectIdUtils.getObjectId(_productCategoryId, null);
        if (productCategoryId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        if (!productCategoryRepository.exists(userLogin.getClientId(), productCategoryId)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        String defaulProductPhoto = getClientCondig(userLogin).getDefaultProductPhoto();

        List<Product> products = productRepository.getProductsByCategory(userLogin.getClientId(), productCategoryId);
        if (products == null || products.isEmpty()) {
            return ListJson.emptyList();
        }
        Set<ObjectId> productIds = new HashSet<ObjectId>();
        for (Product product : products) {
            productIds.add(product.getId());
        }

        HashMap<ObjectId, ProductSalesReportDto> reportByProduct = new HashMap<ObjectId, ProductSalesReportDto>();
        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                for (OrderDetail orderDetail : order.getDetails()) {
                    if (!productIds.contains(orderDetail.getProduct().getId())) {
                        continue;
                    }

                    ProductSalesReportDto report = reportByProduct.get(orderDetail.getProduct().getId());
                    if (report == null) {
                        report = new ProductSalesReportDto(orderDetail.getProduct(), defaulProductPhoto,
                                BigDecimal.ZERO, BigDecimal.ZERO, 0l);
                        reportByProduct.put(orderDetail.getProduct().getId(), report);
                    }

                    report.setRevenue(report.getRevenue().add(orderDetail.getAmount()));
                    report.setProductivity(report.getProductivity().add(orderDetail.getOutput()));
                    report.setNbOrder(report.getNbOrder() + 1);
                }
            }
        }

        List<ProductSalesReportDto> dtos = new LinkedList<ProductSalesReportDto>();
        for (Product product : products) {
            ProductSalesReportDto report = reportByProduct.get(product.getId());
            if (report == null) {
                report = new ProductSalesReportDto(new ProductEmbed(product), defaulProductPhoto, BigDecimal.ZERO,
                        BigDecimal.ZERO, 0l);
            }
            dtos.add(report);
        }

        return new ListJson<ProductSalesReportDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<SalesmanSalesReportDto> getSalesmanSalesReport(UserLogin userLogin, int month, int year) {
        if (!checkRolesAllowed(userLogin)) {
            throw new BusinessException(ExceptionCode.PERMISSION_DENIED);
        }

        List<Order> orders = getOrders(userLogin, month, year);

        HashMap<ObjectId, SalesmanSalesReportDto> reportBySalesman = new HashMap<ObjectId, SalesmanSalesReportDto>();
        if (orders != null && !orders.isEmpty()) {
            for (Order order : orders) {
                SalesmanSalesReportDto report = reportBySalesman.get(order.getSalesman().getId());
                if (report == null) {
                    report = new SalesmanSalesReportDto(order.getSalesman(), BigDecimal.ZERO, BigDecimal.ZERO, 0l, 0l);
                    reportBySalesman.put(order.getSalesman().getId(), report);
                }

                report.setRevenue(report.getRevenue().add(order.getGrandTotal()));
                // TODO productivity ???
                report.setNbOrder(report.getNbOrder() + 1);
            }
        }

        List<User> salesmen = getSalesman(userLogin);
        List<SalesmanSalesReportDto> dtos = new LinkedList<SalesmanSalesReportDto>();
        for (User salesman : salesmen) {
            SalesmanSalesReportDto report = reportBySalesman.get(salesman.getId());
            if (report == null) {
                report = new SalesmanSalesReportDto(salesman, BigDecimal.ZERO, BigDecimal.ZERO, 0l, 0l);
            }
            dtos.add(report);
            
            reportBySalesman.remove(salesman.getId());
        }
        
        // ADD NHUNG SALESMAN MA CO ORDER TRONG THANG TUY NHIEN KHONG CON THUOC DISTRIBUTOR NAY NUA
        for (SalesmanSalesReportDto report : reportBySalesman.values()) {
            dtos.add(report);
        }
        

        return new ListJson<SalesmanSalesReportDto>(dtos, (long) dtos.size());
    }

}
