package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.District;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerForVisitDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.CustomerSummaryDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SalesmanCustomerService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.service.sub.CustomerSubService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.LocationUtils;
import com.viettel.backend.util.StringUtils;

@Service
public class SalesmanCustomerServiceImpl extends AbstractSalesmanService implements SalesmanCustomerService {

    private static final long serialVersionUID = 2812222944238656195L;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private FileEngine fileEngine;

    @Autowired
    private WebNotificationEngine webNotificationEngine;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private CustomerSubService customerSubService;

    @Override
    public CustomerSummaryDto getCustomerSummary(UserLogin userLogin, String id) {
        checkIsSalesman(userLogin);
        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, id);
        return customerSubService.getCustomerSummary(userLogin, customer);
    }

    @Override
    public ListJson<CustomerForVisitDto> getCustomersForVisitToday(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(clientId,
                Collections.singleton(salesmanId));
        Collection<Customer> todayCustomers = customerRepository.getCustomersByRouteDate(clientId, routeIds,
                null, DateTimeUtils.getToday(), null, null);
        if (todayCustomers == null || todayCustomers.isEmpty()) {
            return ListJson.<CustomerForVisitDto> emptyList();
        }

        List<ObjectId> todayCustomerIds = new ArrayList<ObjectId>(todayCustomers.size());
        for (Customer customer : todayCustomers) {
            todayCustomerIds.add(customer.getId());
        }

        Map<ObjectId, Visit> visitTodayByCustomerId = visitRepository.getMapVisitByCustomerIdsToday(clientId,
                todayCustomerIds);

        List<ObjectId> customerIdsOrderByVisitLastWeek = getCustomerIdsOrderByVisitLastWeek(clientId, salesmanId);

        List<CustomerForVisitDto> customerDtos = new ArrayList<CustomerForVisitDto>(todayCustomers.size());
        for (Customer customer : todayCustomers) {
            // STATUS
            int status = CustomerForVisitDto.STATUS_UNVISITED;
            Visit visit = visitTodayByCustomerId.get(customer.getId());
            if (visit != null) {
                if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITING) {
                    status = CustomerForVisitDto.STATUS_VISITING;
                } else {
                    status = CustomerForVisitDto.STATUS_VISITED;
                }
            }

            // ORDER
            int seqNo = customerIdsOrderByVisitLastWeek.indexOf(customer.getId());
            if (seqNo == -1) {
                seqNo = customerIdsOrderByVisitLastWeek.size();
            }

            CustomerForVisitDto customerDto = new CustomerForVisitDto(customer, true, status, seqNo);
            customerDtos.add(customerDto);
        }

        return new ListJson<>(customerDtos, Long.valueOf(customerDtos.size()));
    }

    @Override
    public ListJson<CustomerForVisitDto> getCustomersForVisitAll(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(clientId,
                Collections.singleton(salesmanId));
        Collection<Customer> allCustomers = customerRepository.getCustomersByRoute(clientId, routeIds, null,
                null, null, null);
        if (allCustomers == null || allCustomers.isEmpty()) {
            return ListJson.<CustomerForVisitDto> emptyList();
        }
        Set<ObjectId> allCustomerIds = new HashSet<ObjectId>();
        for (Customer customer : allCustomers) {
            allCustomerIds.add(customer.getId());
        }

        Collection<Customer> todayCustomers = customerRepository.getCustomersByRouteDate(clientId, routeIds,
                null, DateTimeUtils.getToday(), null, null);
        Set<ObjectId> todayCustomerIds = new HashSet<ObjectId>();
        if (todayCustomers != null && !todayCustomers.isEmpty()) {
            for (Customer customer : todayCustomers) {
                todayCustomerIds.add(customer.getId());
            }
        }

        Map<ObjectId, Visit> visitTodayByCustomerId = visitRepository.getMapVisitByCustomerIdsToday(clientId,
                todayCustomerIds);

        List<ObjectId> customerIdsOrderByVisitLastWeek = getCustomerIdsOrderByVisitLastWeek(clientId, salesmanId);

        List<CustomerForVisitDto> customerDtos = new ArrayList<CustomerForVisitDto>(todayCustomers.size());
        for (Customer customer : allCustomers) {
            if (!todayCustomerIds.contains(customer.getId())) {
                CustomerForVisitDto customerDto = new CustomerForVisitDto(customer, false,
                        CustomerForVisitDto.STATUS_UNVISITED, 0);
                customerDtos.add(customerDto);
            } else {
                // TRUONG HOP TRONG TUYEN
                // STATUS
                int status = CustomerForVisitDto.STATUS_UNVISITED;
                Visit visit = visitTodayByCustomerId.get(customer.getId());
                if (visit != null) {
                    if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITING) {
                        status = CustomerForVisitDto.STATUS_VISITING;
                    } else {
                        status = CustomerForVisitDto.STATUS_VISITED;
                    }
                }

                // ORDER
                int seqNo = customerIdsOrderByVisitLastWeek.indexOf(customer.getId());
                if (seqNo == -1) {
                    seqNo = customerIdsOrderByVisitLastWeek.size();
                }

                CustomerForVisitDto customerDto = new CustomerForVisitDto(customer, true, status, seqNo);
                customerDtos.add(customerDto);
            }
        }

        return new ListJson<>(customerDtos, Long.valueOf(customerDtos.size()));
    }

    @Override
    public void updatePhone(UserLogin userLogin, String id, String phone) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, id);

        ObjectId clientId = userLogin.getClientId();

        if (StringUtils.isNullOrEmpty(phone)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        customer.setPhone(phone);

        customerRepository.save(clientId, customer);
    }

    @Override
    public void updateMobile(UserLogin userLogin, String id, String mobile) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, id);

        ObjectId clientId = userLogin.getClientId();

        if (StringUtils.isNullOrEmpty(mobile)) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        customer.setMobile(mobile);

        customerRepository.save(clientId, customer);
    }

    @Override
    public void updateLocation(UserLogin userLogin, String id, LocationDto locationDto) {
        checkIsSalesman(userLogin);

        if (getSalesConfig(userLogin).isCanEditCustomerLocation()) {
            User salesman = getCurrentSalesman(userLogin);
            Customer customer = checkSalesmanCustomer(userLogin, salesman, id);

            ObjectId clientId = userLogin.getClientId();

            if (locationDto == null) {
                throw new BusinessException(ExceptionCode.INVALID_LOCATION_PARAM);
            }

            double[] location = new double[] { locationDto.getLongitude(), locationDto.getLatitude() };

            customer.setLocation(location);

            customerRepository.save(clientId, customer);
        }
    }

    @Override
    public String register(UserLogin userLogin, CustomerCreateDto dto) {
        checkIsSalesman(userLogin);

        checkMandatoryParams(dto, dto.getName(), dto.getMobile(), dto.getLatitude(), dto.getLongitude(),
                dto.getCustomerTypeId(), dto.getDistrictId());

        if (!LocationUtils.checkLocationValid(dto.getLongitude(), dto.getLatitude())) {
            throw new BusinessException(ExceptionCode.INVALID_LOCATION_PARAM);
        }

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Customer customer = new Customer();

        initPOWhenCreate(Customer.class, userLogin, customer);

        customer.setName(dto.getName());
        customer.setCode(codeGeneratorRepository.getCustomerCode(userLogin.getClientId().toString()));
        customer.setMobile(dto.getMobile());
        customer.setPhone(dto.getPhone());
        customer.setContact(dto.getContact());
        customer.setIdNumber(dto.getIdNumber());
        customer.setEmail(dto.getEmail());
        // customer.setAddress(dto.getAddress());
        customer.setDescription(dto.getDescription());

        CustomerType customerType = getMadatoryPO(userLogin, dto.getCustomerTypeId(), customerTypeRepository);
        customer.setCustomerType(customerType);

        District district = getMadatoryPO(userLogin, dto.getDistrictId(), districtRepository);
        customer.setDistrict(district);

        // XXX check not link photo here
        if (dto.getPhotos() != null && dto.getPhotos().size() > 0) {
            if (!fileEngine.exists(userLogin, dto.getPhotos())) {
                throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
            }
            customer.setPhotos(dto.getPhotos());
        }

        customer.setLocation(new double[] { dto.getLongitude(), dto.getLatitude() });

        User salesman = userRepository.getById(clientId, salesmanId);
        if (salesman.getDistributor() == null) {
            throw new BusinessException(ExceptionCode.SALESMAN_NO_DISTRIBUTOR);
        }
        customer.setDistributor(salesman.getDistributor());

        customer.setApproveStatus(Customer.APPROVE_STATUS_PENDING);
        customer.setCreatedBy(new UserEmbed(salesman));
        customer.setCreatedTime(DateTimeUtils.getCurrentTime());

        customer = customerPendingRepository.save(userLogin.getClientId(), customer);

        Distributor distributor = distributorRepository.getById(clientId, salesman.getDistributor().getId());
        if (distributor.getSupervisor() != null && distributor.getSupervisor().getId() != null) {
            webNotificationEngine.notifyNewCustomerForSupervisor(userLogin, distributor.getSupervisor().getId());
        }

        return customer.getId().toString();
    }

    @Override
    public ListJson<CustomerSimpleDto> getCustomersRegistered(UserLogin userLogin, String search, Pageable pageable) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Sort sort = new Sort(Direction.DESC, Customer.COLUMNNAME_CREATED_TIME_VALUE);
        Collection<Customer> customers = customerPendingRepository.getCustomersByCreatedUsers(clientId,
                Arrays.asList(salesmanId), null, search, pageable, sort);
        if (customers == null || customers.isEmpty()) {
            return ListJson.<CustomerSimpleDto> emptyList();
        }

        List<CustomerSimpleDto> dtos = new ArrayList<CustomerSimpleDto>();
        for (Customer customer : customers) {
            CustomerSimpleDto dto = new CustomerSimpleDto(customer);
            dtos.add(dto);
        }

        long size = customers.size();
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || pageable.getPageSize() == size) {
                size = customerPendingRepository.countCustomersByCreatedUsers(clientId, Arrays.asList(salesmanId),
                        null, null);
            }
        }

        return new ListJson<CustomerSimpleDto>(dtos, size);
    }

    // ** PRIVATE **
    private List<ObjectId> getCustomerIdsOrderByVisitLastWeek(ObjectId clientId, ObjectId salesmanId) {
        // SMART ORDER
        Sort sort = new Sort(Visit.COLUMNNAME_START_TIME_VALUE);
        Period sameDayLastWeek = DateTimeUtils.getPeriodOneDay(DateTimeUtils.addWeeks(DateTimeUtils.getToday(), -1));
        List<Visit> visitsLastWeek = visitRepository.getVisitedsBySalesmen(clientId, Arrays.asList(salesmanId),
                sameDayLastWeek, sort);
        List<ObjectId> customerIdsLastWeek = new ArrayList<ObjectId>();
        if (visitsLastWeek != null && !visitsLastWeek.isEmpty()) {
            for (Visit visit : visitsLastWeek) {
                customerIdsLastWeek.add(visit.getCustomer().getId());
            }
        }

        return customerIdsLastWeek;
    }

}
