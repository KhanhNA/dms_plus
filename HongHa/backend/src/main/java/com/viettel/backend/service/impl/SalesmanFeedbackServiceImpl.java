package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.embed.FeedbackDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.service.SalesmanFeedbackService;
import com.viettel.backend.util.DateTimeUtils;

@Service
public class SalesmanFeedbackServiceImpl extends AbstractSalesmanService implements SalesmanFeedbackService {

    private static final long serialVersionUID = -7754544109849838021L;
    
    @Autowired
    private VisitRepository visitRepository;

    @Override
    public ListJson<FeedbackDto> getLastCustomerFeedbacks(UserLogin userLogin, String customerId) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, customerId);

        ObjectId clientId = userLogin.getClientId();
        
        //XXX FROM 3 MONTHS AGO
        SimpleDate today = DateTimeUtils.getToday();
        Period period = new Period(DateTimeUtils.addMonths(today, -3), DateTimeUtils.addDays(today, 1));

        List<Visit> visits = visitRepository.getLastVisitedsByCustomer(clientId, customer.getId(), 5, period);

        List<FeedbackDto> feedbackDtos = new ArrayList<FeedbackDto>();
        for (Visit visit : visits) {
            List<String> feedbacks = visit.getFeedbacks();
            if (feedbacks == null || feedbacks.isEmpty()) {
                continue;
            }

            String createdDate = visit.getEndTime().getIsoTime();
            for (String feedback : feedbacks) {
                FeedbackDto dto = new FeedbackDto();
                dto.setCreatedDate(createdDate);
                dto.setMessage(feedback);
                feedbackDtos.add(dto);
            }
        }

        return new ListJson<FeedbackDto>(feedbackDtos, (long) feedbackDtos.size());
    }

}
