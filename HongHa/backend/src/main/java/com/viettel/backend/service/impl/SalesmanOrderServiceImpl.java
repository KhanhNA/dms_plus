package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Product;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.domain.embed.ProductEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.OrderCreateDto;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.dto.OrderSimpleDto;
import com.viettel.backend.dto.embed.OrderDetailCreatedDto;
import com.viettel.backend.dto.embed.OrderPromotionDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.repository.ProductRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SalesmanOrderService;
import com.viettel.backend.service.engine.PromotionEngine;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SalesmanOrderServiceImpl extends AbstractSalesmanService implements SalesmanOrderService {

    private static final long serialVersionUID = 4840219204026219311L;

    public static final int NUMBER_PO_FOR_SMART_ORDER_PRODUCT = 1;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private WebNotificationEngine webNotificationEngine;

    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private PromotionEngine promotionEngine;

    @Override
    public ListJson<OrderPromotionDto> calculatePromotion(UserLogin userLogin, OrderCreateDto dto) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();

        // PREPARE ORDER
        Order order = new Order();
        for (OrderDetailCreatedDto detailDto : dto.getDetails()) {
            ObjectId productId = ObjectIdUtils.getObjectId(detailDto.getProductId(), null);
            if (productId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            Product product = productRepository.getById(clientId, productId);
            if (product == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            OrderDetail detail = new OrderDetail();
            detail.setProduct(new ProductEmbed(product));
            detail.setQuantity(detailDto.getQuantity());

            order.addDetails(detail);
        }

        List<OrderPromotionDto> orderPromotionDtos = promotionEngine.getPromotions(userLogin, dto);
        if (orderPromotionDtos == null) {
            return ListJson.<OrderPromotionDto> emptyList();
        } else {
            return new ListJson<OrderPromotionDto>(orderPromotionDtos, (long) orderPromotionDtos.size());
        }
    }

    @Override
    public String createUnplannedOrder(UserLogin userLogin, OrderCreateDto dto) {
        checkIsSalesman(userLogin);

        if (dto == null || dto.getDetails() == null || dto.getDetails().isEmpty()) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, dto.getCustomerId());
        Distributor distributor = checkSalesmanDistributor(userLogin, salesman);

        Order order = new Order();
        initPOWhenCreate(Order.class, userLogin, order);

        order.setSalesman(new UserEmbed(salesman));
        order.setCustomer(new CustomerEmbed(customer));
        order.setDistributor(new DistributorEmbed(distributor));

        order.setCreatedTime(DateTimeUtils.getCurrentTime());
        order.setCode(codeGeneratorRepository.getOrderCode(userLogin.getClientId().toString()));
        order.setApproveStatus(Order.APPROVE_STATUS_PENDING);

        order.setDeliveryType(dto.getDeliveryType());
        order.setDeliveryTime(SimpleDate.createByIsoTime(dto.getDeliveryTime(), null));

        promotionEngine.calculatePromotion(userLogin, dto, order);

        order = orderPendingRepository.save(userLogin.getClientId(), order);

        webNotificationEngine.notifyNewOrderForDistributor(userLogin, distributor.getId());

        return order.getId().toString();
    }

    @Override
    public ListJson<OrderSimpleDto> getOrdersToday(UserLogin userLogin) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(clientId,
                Collections.singleton(salesmanId));
        Set<ObjectId> customerIds = customerRepository.getCustomerIdsByRoute(clientId, routeIds, null);
        if (customerIds == null || customerIds.isEmpty()) {
            return ListJson.<OrderSimpleDto> emptyList();
        }

        Sort sort = new Sort(Direction.DESC, Order.COLUMNNAME_CREATED_TIME_VALUE);
        List<Order> orders = orderPendingRepository.getAllOrdersByCustomersToday(clientId, customerIds, sort);
        if (orders == null) {
            return ListJson.<OrderSimpleDto> emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<OrderSimpleDto>(orders.size());
        for (Order order : orders) {
            dtos.add(new OrderSimpleDto(order));
        }

        return new ListJson<OrderSimpleDto>(dtos, (long) dtos.size());
    }

    @Override
    public ListJson<OrderSimpleDto> getOrdersByCustomerToday(UserLogin userLogin, String _customerId) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, _customerId);

        ObjectId clientId = userLogin.getClientId();

        Sort sort = new Sort(Direction.DESC, Order.COLUMNNAME_CREATED_TIME_VALUE);
        List<Order> orders = orderPendingRepository.getAllOrdersByCustomersToday(clientId,
                Collections.singletonList(customer.getId()), sort);
        if (orders == null) {
            return ListJson.<OrderSimpleDto> emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<OrderSimpleDto>(orders.size());
        for (Order order : orders) {
            dtos.add(new OrderSimpleDto(order));
        }

        return new ListJson<OrderSimpleDto>(dtos, (long) dtos.size());
    }

    @Override
    public OrderDto getOrderById(UserLogin userLogin, String id) {
        checkIsSalesman(userLogin);

        String defaultProductPhoto = getClientCondig(userLogin).getDefaultProductPhoto();

        ObjectId clientId = userLogin.getClientId();
        ObjectId salesmanId = userLogin.getUserId();

        ObjectId orderId = ObjectIdUtils.getObjectId(id, null);
        if (orderId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Order order = orderPendingRepository.getById(clientId, orderId);
        if (order == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (!order.getSalesman().getId().equals(salesmanId)) {
            Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(clientId,
                    Collections.singleton(salesmanId));
            Set<ObjectId> customerIds = customerRepository.getCustomerIdsByRoute(clientId, routeIds, null);

            if (order.getCustomer() == null || !customerIds.contains(order.getCustomer().getId())) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
        }

        return new OrderDto(order, defaultProductPhoto);
    }

}
