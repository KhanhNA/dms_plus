package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.Survey;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.SurveyAnswer;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.LocationDto;
import com.viettel.backend.dto.SurveyDto;
import com.viettel.backend.dto.VisitClosingDto;
import com.viettel.backend.dto.VisitEndDto;
import com.viettel.backend.dto.VisitInfoDto;
import com.viettel.backend.dto.embed.SurveyAnswerDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.ExhibitionRepository;
import com.viettel.backend.repository.SalesConfigRepository;
import com.viettel.backend.repository.SurveyRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SalesmanVisitService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.service.engine.PromotionEngine;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.service.sub.CacheSubService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.LocationUtils;
import com.viettel.backend.util.ObjectIdUtils;
import com.viettel.backend.util.StringUtils;

@Service
public class SalesmanVisitServiceImpl extends AbstractSalesmanService implements SalesmanVisitService {

    /**
     * 
     */
    private static final long serialVersionUID = 1355253570401859621L;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private PromotionEngine promotionEngine;

    @Autowired
    private ClientConfigRepository clientConfigRepository;
    
    @Autowired
    private SalesConfigRepository salesConfigRepository;
    
    @Autowired
    private SurveyRepository surveyRepository;
    
    @Autowired
    private ExhibitionRepository exhibitionRepository;

    @Autowired
    private FileEngine fileEngine;

    @Autowired
    private WebNotificationEngine webNotificationEngine;
    
    @Autowired
    private CacheSubService cacheSubService;

    @Override
    public String startVisit(UserLogin userLogin, String customerId, LocationDto locationDto) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Distributor distributor = checkSalesmanDistributor(userLogin, salesman);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, customerId);

        Visit visit = visitRepository.getVisitByCustomerToday(userLogin.getClientId(), customer.getId());
        if (visit != null) {
            if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITED) {
                throw new BusinessException("this customer already visit");
            }

            return visit.getId().toString();
        }

        visit = createVisit(userLogin, salesman, distributor, customer, locationDto);

        visit.setStartTime(DateTimeUtils.getCurrentTime());
        visit.setEndTime(null);
        visit.setVisitStatus(Visit.VISIT_STATUS_VISITING);
        visit.setClosed(false);

        visit = visitRepository.save(userLogin.getClientId(), visit);

        return visit.getId().toString();
    }

    @Override
    public VisitInfoDto endVisit(UserLogin userLogin, String visitId, VisitEndDto dto) {
        checkIsSalesman(userLogin);

        ObjectId clientId = userLogin.getClientId();

        ObjectId currentVisitId = ObjectIdUtils.getObjectId(visitId, null);
        if (currentVisitId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Visit visit = visitRepository.getById(userLogin.getClientId(), currentVisitId);
        if (visit == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (visit.isClosed() || visit.getVisitStatus() == Visit.VISIT_STATUS_VISITED) {
            throw new BusinessException(ExceptionCode.VISIT_ENDED);
        }

        if (ObjectIdUtils.compare(userLogin.getUserId(), visit.getSalesman().getId()) != 0) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        String defaultProductPhoto = getClientCondig(userLogin).getDefaultProductPhoto();

        visit.setEndTime(DateTimeUtils.getCurrentTime());

        visit.setDuration(SimpleDate.getDuration(visit.getStartTime(), visit.getEndTime()));
        visit.setErrorDuration(visit.getDuration() < getSalesConfig(userLogin).getVisitDurationKPI());

        visit.setVisitStatus(Visit.VISIT_STATUS_VISITED);

        // FEEDBACK
        int feedbackCount = 0;
        if (dto.getFeedbacks() != null && !dto.getFeedbacks().isEmpty()) {
            for (String feedback : dto.getFeedbacks()) {
                if (!StringUtils.isNullOrEmpty(feedback)) {
                    visit.addFeedback(feedback.trim());
                    feedbackCount++;
                }
            }
        }
        visit.setFeedbacksReaded(feedbackCount == 0);

        // ORDER
        if (dto.getOrder() != null) {
            if (dto.getOrder().getDetails() == null || dto.getOrder().getDetails().isEmpty()) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            visit.setCode(codeGeneratorRepository.getOrderCode(userLogin.getClientId().toString()));
            visit.setApproveStatus(Order.APPROVE_STATUS_PENDING);

            visit.setDeliveryType(dto.getOrder().getDeliveryType());
            visit.setDeliveryTime(SimpleDate.createByIsoTime(dto.getOrder().getDeliveryTime(), null));

            promotionEngine.calculatePromotion(userLogin, dto.getOrder(), visit);
        }

        // SURVEY ANSWER
        if (dto.getSurveyAnswers() != null && !dto.getSurveyAnswers().isEmpty()) {
            List<SurveyAnswer> surveyAnswers = new ArrayList<SurveyAnswer>(dto.getSurveyAnswers().size());
            for (SurveyAnswerDto surveyAnswerDto : dto.getSurveyAnswers()) {
                SurveyAnswer surveyAnswer = new SurveyAnswer();

                surveyAnswer.setSurveyId(ObjectIdUtils.getObjectId(surveyAnswerDto.getSurveyId()));
                surveyAnswer.setOptions(ObjectIdUtils.getObjectIds(surveyAnswerDto.getOptions()));

                surveyAnswers.add(surveyAnswer);
            }
            visit.setSurveyAnswers(surveyAnswers);
        }

        visit = visitRepository.save(userLogin.getClientId(), visit);

        //ADD TO CACHE
        cacheSubService.addNewApprovedVisit(visit);
        
        // Notify
        if (feedbackCount > 0 || visit.isOrder()) {
	        Distributor distributor = distributorRepository.getById(clientId, visit.getDistributor().getId());
	        if (distributor != null 
	        		&& distributor.getSupervisor() != null
                    && distributor.getSupervisor().getId() != null) {
	        	// notify if supervisor to read feedback
		        if (feedbackCount > 0) {
		        	webNotificationEngine.notifyNewFeedbackForSupervisor(userLogin, distributor.getSupervisor().getId());
		        }
		        
		        // Notify supervisor about new orders
		        if (visit.isOrder()) {
		        	webNotificationEngine.notifyNewOrderForDistributor(userLogin, distributor.getId());
		        }
	        }
        }

        VisitInfoDto visitInfoDto = new VisitInfoDto(visit, defaultProductPhoto);
        
        if (visitInfoDto.getSurveyAnswers() != null) {
            Set<ObjectId> surveyIds = new HashSet<ObjectId>();
            for (SurveyAnswerDto surveyAnswerDto : visitInfoDto.getSurveyAnswers()) {
                surveyIds.add(ObjectIdUtils.getObjectId(surveyAnswerDto.getSurveyId()));
            }
            
            List<Survey> surveys = surveyRepository.getListByIds(clientId, surveyIds);
            if (surveys != null) {
                for (Survey survey : surveys) {
                    visitInfoDto.addSurvey(new SurveyDto(survey));
                }
            }
        }
        
        return visitInfoDto;
    }

    @Override
    public String markAsClosed(UserLogin userLogin, String customerId, VisitClosingDto dto) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);
        Distributor distributor = checkSalesmanDistributor(userLogin, salesman);
        Customer customer = checkSalesmanCustomer(userLogin, salesman, customerId);

        if (dto == null || dto.getClosingPhoto() == null) {
            throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
        }

        ObjectId fileId = ObjectIdUtils.getObjectId(dto.getClosingPhoto(), null);
        if (fileId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
        }

        if (!fileEngine.exists(userLogin, dto.getClosingPhoto())) {
            throw new BusinessException(ExceptionCode.INVALID_PHOTO_PARAM);
        }

        Visit visit = visitRepository.getVisitByCustomerToday(userLogin.getClientId(), customer.getId());
        if (visit != null) {
            throw new BusinessException(ExceptionCode.CUSTOMER_VISITED);
        }

        visit = createVisit(userLogin, salesman, distributor, customer, dto.getLocation());

        SimpleDate currentTime = DateTimeUtils.getCurrentTime();
        visit.setStartTime(currentTime);
        visit.setEndTime(currentTime);

        visit.setDuration(0);
        visit.setErrorDuration(false);

        visit.setClosingPhoto(dto.getClosingPhoto());

        visit.setVisitStatus(Visit.VISIT_STATUS_VISITED);
        visit.setClosed(true);

        visit = visitRepository.save(userLogin.getClientId(), visit);

        //ADD TO CACHE
        cacheSubService.addNewApprovedVisit(visit);
        
        // MARK FILE AS USED
        fileEngine.markAsUsed(userLogin, dto.getClosingPhoto());

        return visit.getId().toString();
    }

    @Override
    public VisitInfoDto getVisitedTodayInfo(UserLogin userLogin, String customerId) {
        checkIsSalesman(userLogin);

        User salesman = getCurrentSalesman(userLogin);

        Customer customer = checkSalesmanCustomer(userLogin, salesman, customerId);

        Visit visit = visitRepository.getVisitByCustomerToday(userLogin.getClientId(), customer.getId());
        if (visit == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITING) {
            throw new BusinessException(ExceptionCode.VISIT_NOT_ENDED);
        }

        String defaultProductPhoto = getClientCondig(userLogin).getDefaultProductPhoto();

        VisitInfoDto visitInfoDto = new VisitInfoDto(visit, defaultProductPhoto);
        
        if (visitInfoDto.getSurveyAnswers() != null) {
            Set<ObjectId> surveyIds = new HashSet<ObjectId>();
            for (SurveyAnswerDto surveyAnswerDto : visitInfoDto.getSurveyAnswers()) {
                surveyIds.add(ObjectIdUtils.getObjectId(surveyAnswerDto.getSurveyId()));
            }
            
            List<Survey> surveys = surveyRepository.getListByIds(userLogin.getClientId(), surveyIds);
            if (surveys != null) {
                for (Survey survey : surveys) {
                    visitInfoDto.addSurvey(new SurveyDto(survey));
                }
            }
        }
        
        return visitInfoDto;
    }

    // PRIVATE
    private Visit createVisit(UserLogin userLogin, User salesman, Distributor distributor, Customer customer,
            LocationDto locationDto) {
        Visit visit = new Visit();

        initPOWhenCreate(Visit.class, userLogin, visit);

        // LOCATION STATUS
        if (!LocationUtils.checkLocationValid(customer.getLocation())) {
            visit.setLocationStatus(Visit.LOCATION_STATUS_CUSTOMER_UNLOCATED);
            visit.setLocation(LocationUtils.convert(locationDto));
            visit.setCustomerLocation(null);
        } else {
            if (!LocationUtils.checkLocationValid(locationDto)) {
                visit.setLocationStatus(Visit.LOCATION_STATUS_UNLOCATED);
                visit.setLocation(null);
                visit.setCustomerLocation(customer.getLocation());
            } else {
                double distance = LocationUtils.calculateDistance(customer.getLocation()[1], customer.getLocation()[0],
                        locationDto.getLatitude(), locationDto.getLongitude());
                if (distance > getSalesConfig(userLogin).getVisitDistanceKPI()) {
                    visit.setLocationStatus(Visit.LOCATION_STATUS_TOO_FAR);
                } else {
                    visit.setLocationStatus(Visit.LOCATION_STATUS_LOCATED);
                }

                visit.setLocation(LocationUtils.convert(locationDto));
                visit.setCustomerLocation(customer.getLocation());
                visit.setDistance(distance);
            }
        }

        visit.setDistributor(new DistributorEmbed(distributor));
        visit.setSalesman(new UserEmbed(salesman));
        visit.setCustomer(new CustomerEmbed(customer));

        return visit;
    }

}