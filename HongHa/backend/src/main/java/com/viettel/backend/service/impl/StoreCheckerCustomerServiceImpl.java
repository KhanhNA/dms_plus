package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.CustomerCheck;
import com.viettel.backend.domain.Route;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.CheckCustomerDto;
import com.viettel.backend.dto.CustomerForCheckDto;
import com.viettel.backend.dto.CustomerSummaryDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.repository.CustomerCheckRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.StoreCheckerCustomerService;
import com.viettel.backend.service.sub.CustomerSubService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class StoreCheckerCustomerServiceImpl extends AbstractStoreCheckerService implements StoreCheckerCustomerService {

    private static final long serialVersionUID = 1L;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerSubService customerSubService;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    @Autowired
    private CustomerCheckRepository customerCheckRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public CustomerSummaryDto getCustomerSummary(UserLogin userLogin, String id) {
        checkIsStoreChecker(userLogin);

        Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByStoreCheckers(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()));

        Customer customer = getMadatoryPO(userLogin, id, customerRepository);
        if (salesmanIds == null || customer.getSchedule() == null || customer.getSchedule().getRouteId() == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Route route = routeRepository.getById(userLogin.getClientId(), customer.getSchedule().getRouteId());
        if (route.getSalesman() == null || !salesmanIds.contains(route.getSalesman().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return customerSubService.getCustomerSummary(userLogin, customer);
    }

    @Override
    public ListJson<CustomerForCheckDto> getCustomersForCheckToday(UserLogin userLogin) {
        checkIsStoreChecker(userLogin);

        Collection<ObjectId> storeCheckerIds = Collections.singletonList(userLogin.getUserId());

        Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByStoreCheckers(userLogin.getClientId(),
                storeCheckerIds);

        SimpleDate previousWorkingDay = calendarConfigRepository.getPreviousWorkingDay(userLogin.getClientId(),
                DateTimeUtils.getToday());
        Period period = new Period(previousWorkingDay, DateTimeUtils.addDays(previousWorkingDay, 1));

        List<Visit> visits = visitRepository.getVisitedsBySalesmen(userLogin.getClientId(), salesmanIds, period, null);
        if (visits == null || visits.isEmpty()) {
            return ListJson.emptyList();
        }

        Map<ObjectId, CustomerForCheckDto> dtos = new HashMap<>(visits.size());
        Set<ObjectId> customerIds = new HashSet<ObjectId>(visits.size());
        for (Visit visit : visits) {
            customerIds.add(visit.getCustomer().getId());
        }

        Map<ObjectId, Customer> mapCustomers = customerRepository.getMapByIds(userLogin.getClientId(), customerIds);

        for (Visit visit : visits) {
            CustomerForCheckDto dto = new CustomerForCheckDto(mapCustomers.get(visit.getCustomer().getId()), visit,
                    false, false, null);
            dtos.put(visit.getCustomer().getId(), dto);
        }

        List<CustomerCheck> customerChecks = customerCheckRepository.getByStoreCheckers(userLogin.getClientId(),
                storeCheckerIds, DateTimeUtils.getPeriodToday(), null);

        if (customerChecks != null) {
            for (CustomerCheck customerCheck : customerChecks) {
                CustomerForCheckDto dto = dtos.get(customerCheck.getCustomer().getId());
                if (dto == null) {
                    continue;
                }
                dto.setChecked(true);
                dto.setPassed(customerCheck.isPassed());
                dto.setNote(customerCheck.getNote());
            }
        }

        return new ListJson<CustomerForCheckDto>(new ArrayList<CustomerForCheckDto>(dtos.values()), (long) dtos.size());
    }

    @Override
    public void checkForCustomer(UserLogin userLogin, String _customerId, CheckCustomerDto dto) {
        checkIsStoreChecker(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId customerId = ObjectIdUtils.getObjectId(_customerId, null);
        if (customerId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        // Fetch all salesmans of current storechecker
        Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByStoreCheckers(clientId,
                Collections.singleton(userLogin.getUserId()));
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        // Fetch storechecker
        User storeChecker = userRepository.getById(clientId, userLogin.getUserId());

        CustomerCheck customerCheck = new CustomerCheck();
        initPOWhenCreate(CustomerCheck.class, userLogin, customerCheck);
        customerCheck.setActive(true);
        customerCheck.setDraft(false);
        customerCheck.setCreatedTime(DateTimeUtils.getCurrentTime());
        customerCheck.setNote(dto.getNote());
        customerCheck.setPassed(dto.isPassed());
        customerCheck.setStoreChecker(new UserEmbed(storeChecker));

        // If using visit
        if (!StringUtils.isEmpty(dto.getVisitId())) {

            // Fetch visit
            ObjectId visitId = ObjectIdUtils.getObjectId(dto.getVisitId(), null);
            if (visitId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            Visit visit = visitRepository.getById(clientId, visitId);
            if (visit == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            // Check if salesman of visit match salesmans of storechecker
            if (!salesmanIds.contains(visit.getSalesman().getId())) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            if (customerCheckRepository.existsByCustomerAndPeriod(clientId, customerId, DateTimeUtils.getPeriodToday())) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            customerCheck.setCustomer(visit.getCustomer());
            customerCheck.setSalesman(visit.getSalesman());
            customerCheck.setDistributor(visit.getDistributor());
            customerCheck.setHasOrder(visit.isOrder());
            customerCheck.setVisitDate(visit.getEndTime());
        }
        // If visit outside of system, and not have visit
        else {
            if (StringUtils.isEmpty(dto.getSalesmanId())) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            if (customerCheckRepository.existsByCustomerAndPeriod(clientId, customerId, DateTimeUtils.getPeriodToday())) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            ObjectId salesmanId = ObjectIdUtils.getObjectId(dto.getSalesmanId(), null);
            if (salesmanId == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            User salesman = userRepository.getById(clientId, salesmanId);
            if (salesman == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            if (salesman.getDistributor() == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            Customer customer = customerRepository.getById(clientId, customerId);
            if (customer == null) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }

            customerCheck.setCustomer(new CustomerEmbed(customer));
            customerCheck.setSalesman(new UserEmbed(salesman));
            customerCheck.setDistributor(salesman.getDistributor());
            customerCheck.setHasOrder(false);
            customerCheck.setVisitDate(null);
        }

        customerCheckRepository.save(clientId, customerCheck);
    }
}
