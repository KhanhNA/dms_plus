package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.domain.ExhibitionRating;
import com.viettel.backend.domain.Route;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.CustomerEmbed;
import com.viettel.backend.domain.embed.ExhibitionRatingItem;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.ExhibitionRatingDto.ExhibitionRatingItemDto;
import com.viettel.backend.dto.ExhibitionWithRatingDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.ExhibitionRatingRepository;
import com.viettel.backend.repository.ExhibitionRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.StoreCheckerExhibitionService;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.ObjectIdUtils;

/**
 * @author thanh
 */
@Service
public class StoreCheckerExhibitionServiceImpl extends AbstractStoreCheckerService implements
        StoreCheckerExhibitionService {

    private static final long serialVersionUID = -5402109549735065233L;

    @Autowired
    private ExhibitionRepository exhibitionRepository;

    @Autowired
    private ExhibitionRatingRepository exhibitionRatingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public ListJson<ExhibitionWithRatingDto> getExhibitionsByCustomer(UserLogin userLogin, String _customerId) {
        checkIsStoreChecker(userLogin);

        ObjectId clientId = userLogin.getClientId();

        Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByStoreCheckers(clientId,
                Arrays.asList(userLogin.getUserId()));

        Customer customer = getMadatoryPO(userLogin, _customerId, customerRepository);
        if (salesmanIds == null || customer.getSchedule() == null || customer.getSchedule().getRouteId() == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Route route = routeRepository.getById(userLogin.getClientId(), customer.getSchedule().getRouteId());
        if (route.getSalesman() == null || !salesmanIds.contains(route.getSalesman().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Sort sort = new Sort(Exhibition.COLUMNNAME_END_DATE_VALUE);
        List<Exhibition> exhibitions = exhibitionRepository.getExhibitionsByCustomer(clientId, customer.getId(), null,
                null, sort);
        if (exhibitions == null || exhibitions.isEmpty()) {
            return ListJson.emptyList();
        }

        Set<ObjectId> exhibitionIds = new HashSet<>(exhibitions.size());
        for (Exhibition exhibition : exhibitions) {
            exhibitionIds.add(exhibition.getId());
        }

        List<ExhibitionRating> exhibitionRatings = exhibitionRatingRepository.getExhibitionRatingsByCustomers(clientId,
                exhibitionIds, Arrays.asList(customer.getId()));

        Map<ObjectId, ExhibitionRating> mapExhibitionRatings = new HashMap<>(exhibitions.size());
        if (exhibitionRatings != null && !exhibitionRatings.isEmpty()) {
            for (ExhibitionRating exhibitionRating : exhibitionRatings) {
                mapExhibitionRatings.put(exhibitionRating.getExhibitionId(), exhibitionRating);
            }
        }

        List<ExhibitionWithRatingDto> dtos = new ArrayList<ExhibitionWithRatingDto>(exhibitions.size());
        for (Exhibition exhibition : exhibitions) {
            ExhibitionWithRatingDto dto = new ExhibitionWithRatingDto(exhibition, mapExhibitionRatings.get(exhibition
                    .getId()));
            dtos.add(dto);
        }

        return new ListJson<ExhibitionWithRatingDto>(dtos, Long.valueOf(dtos.size()));
    }

    @Override
    public void createExhibitionRatingByCustomer(UserLogin userLogin, String _exhibitionId, String _customerId,
            List<ExhibitionRatingItemDto> exhibitionRatingItemDtos) {
        checkIsStoreChecker(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId exhibitionId = ObjectIdUtils.getObjectId(_exhibitionId, null);

        if (exhibitionId == null || exhibitionRatingItemDtos == null || exhibitionRatingItemDtos.isEmpty()) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByStoreCheckers(clientId,
                Arrays.asList(userLogin.getUserId()));

        Customer customer = getMadatoryPO(userLogin, _customerId, customerRepository);
        if (salesmanIds == null || customer.getSchedule() == null || customer.getSchedule().getRouteId() == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Route route = routeRepository.getById(userLogin.getClientId(), customer.getSchedule().getRouteId());
        if (route.getSalesman() == null || !salesmanIds.contains(route.getSalesman().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        // Check if have exhibition
        List<Exhibition> exhibitions = exhibitionRepository.getExhibitionsByCustomer(clientId, customer.getId(), null,
                null, null);
        Exhibition exhibition = null;
        for (Exhibition ex : exhibitions) {
            if (ex.getId().equals(exhibitionId)) {
                exhibition = ex;
                break;
            }
        }

        if (exhibition == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        User storeChecker = getCurrentStoreChecker(userLogin);

        // Check if this exhibition already rated
        List<ExhibitionRating> exhibitionRatings = exhibitionRatingRepository.getExhibitionRatingsByCustomers(clientId,
                Arrays.asList(exhibitionId), Arrays.asList(customer.getId()));
        if (exhibitionRatings != null && !exhibitionRatings.isEmpty()) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        ExhibitionRating exhibitionRating = new ExhibitionRating();
        initPOWhenCreate(ExhibitionRating.class, userLogin, exhibitionRating);
        exhibitionRating.setCreatedTime(DateTimeUtils.getCurrentTime());
        exhibitionRating.setCustomer(new CustomerEmbed(customer));
        exhibitionRating.setExhibitionId(exhibitionId);
        exhibitionRating.setStoreChecker(new UserEmbed(storeChecker));
        exhibitionRating.setDraft(false);

        List<ExhibitionRatingItem> exhibitionRatingItems = new ArrayList<ExhibitionRatingItem>(
                exhibitionRatingItemDtos.size());

        // XXX: Check if item valid
        for (ExhibitionRatingItemDto exhibitionRatingItemDto : exhibitionRatingItemDtos) {
            ExhibitionRatingItem exhibitionRatingItem = new ExhibitionRatingItem();
            exhibitionRatingItem.setExhibitionItemId(ObjectIdUtils.getObjectId(exhibitionRatingItemDto
                    .getExhibitionItemId()));
            exhibitionRatingItem.setRate(exhibitionRatingItemDto.getRate());
            exhibitionRatingItems.add(exhibitionRatingItem);
        }

        exhibitionRating.setItems(exhibitionRatingItems);

        exhibitionRatingRepository.save(clientId, exhibitionRating);
    }

    // EXHIBITION RATING
    // if (dto.getExhibitionRatings() != null &&
    // !dto.getExhibitionRatings().isEmpty()) {
    // List<ExhibitionRating> exhibitionRatings = new
    // ArrayList<ExhibitionRating>(dto.getExhibitionRatings()
    // .size());
    // for (ExhibitionRatingDto exhibitionRatingDto :
    // dto.getExhibitionRatings()) {
    // ExhibitionRating exhibitionRating = new ExhibitionRating();
    // exhibitionRating.setExhibitionId(ObjectIdUtils.getObjectId(exhibitionRatingDto.getExhibitionId()));
    //
    // if (exhibitionRatingDto.getItems() != null) {
    // List<ExhibitionRatingItem> exhibitionRatingItems = new
    // ArrayList<ExhibitionRatingItem>(
    // exhibitionRatingDto.getItems().size());
    //
    // for (ExhibitionRatingItemDto exhibitionRatingItemDto :
    // exhibitionRatingDto.getItems()) {
    // ExhibitionRatingItem exhibitionRatingItem = new ExhibitionRatingItem();
    // exhibitionRatingItem.setExhibitionItemId(ObjectIdUtils.getObjectId(exhibitionRatingItemDto
    // .getExhibitionItemId()));
    // exhibitionRatingItem.setRate(exhibitionRatingItemDto.getRate());
    // exhibitionRatingItems.add(exhibitionRatingItem);
    // }
    //
    // exhibitionRating.setItems(exhibitionRatingItems);
    // }
    //
    // exhibitionRatings.add(exhibitionRating);
    // }
    // visit.setExhibitionRatings(exhibitionRatings);
    // }

    // if (visitInfoDto.getExhibitionRatings() != null) {
    // Set<ObjectId> exhibitionIds = new HashSet<ObjectId>();
    // for (ExhibitionRatingDto exhibitionRatingDto :
    // visitInfoDto.getExhibitionRatings()) {
    // exhibitionIds.add(ObjectIdUtils.getObjectId(exhibitionRatingDto.getExhibitionId()));
    // }
    //
    // List<Exhibition> exhibitions =
    // exhibitionRepository.getListByIds(clientId, exhibitionIds);
    // if (exhibitions != null) {
    // for (Exhibition exhibition : exhibitions) {
    // visitInfoDto.addExhibition(new ExhibitionDto(exhibition));
    // }
    // }
    // }

}
