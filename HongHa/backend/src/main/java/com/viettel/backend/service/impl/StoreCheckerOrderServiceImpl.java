package com.viettel.backend.service.impl;

import java.util.Arrays;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Order;
import com.viettel.backend.dto.OrderDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.StoreCheckerOrderService;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class StoreCheckerOrderServiceImpl extends AbstractStoreCheckerService implements StoreCheckerOrderService {

    private static final long serialVersionUID = 4840219204026219311L;

    public static final int NUMBER_PO_FOR_SMART_ORDER_PRODUCT = 1;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public OrderDto getOrderById(UserLogin userLogin, String id) {
        checkIsStoreChecker(userLogin);

        String defaultProductPhoto = getClientCondig(userLogin).getDefaultProductPhoto();

        ObjectId clientId = userLogin.getClientId();
        Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByStoreCheckers(clientId,
                Arrays.asList(userLogin.getUserId()));

        ObjectId orderId = ObjectIdUtils.getObjectId(id, null);
        if (orderId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Order order = orderRepository.getById(clientId, orderId);
        if (order == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (!salesmanIds.contains(order.getSalesman().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        return new OrderDto(order, defaultProductPhoto);
    }

}
