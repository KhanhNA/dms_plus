package com.viettel.backend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Client;
import com.viettel.backend.dto.NameCategoryDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.BasicCategoryRepository;
import com.viettel.backend.repository.ClientRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SuperAdminClientService;
import com.viettel.backend.util.HardCodeUtils;

@Service
public class SuperAdminClientServiceImpl extends
        BasicCategoryServiceImpl<Client, NameCategoryDto, NameCategoryDto, NameCategoryDto> implements
        SuperAdminClientService {

    private static final long serialVersionUID = -5771173067596328856L;

    @Autowired
    private ClientRepository repository;

    @Override
    public BasicCategoryRepository<Client> getRepository() {
        return repository;
    }

    @Override
    public boolean checkRole(UserLogin userLogin) {
        return userLogin.hasRole(HardCodeUtils.ROLE_SUPERADMIN);
    }
    
    @Override
    public String noPermissionExceptionCode() {
        return ExceptionCode.SUPERADMIN_ONLY;
    }

    @Override
    public Client createDomain(UserLogin userLogin, NameCategoryDto createdto) {
        checkMandatoryParams(createdto, createdto.getName());

        Client domain = new Client();
        domain.setDraft(true);

        initPOWhenCreate(Client.class, userLogin, domain);

        updateDomain(userLogin, domain, createdto);

        return domain;
    }

    @Override
    public void updateDomain(UserLogin userLogin, Client domain, NameCategoryDto createdto) {
        if (!domain.isDraft()) {
            throw new BusinessException(ExceptionCode.CANNOT_UPDATE_NO_DRAFT_DATA);
        }
        
        checkMandatoryParams(createdto, createdto.getName());

        domain.setName(createdto.getName());
    }

    @Override
    public NameCategoryDto createSimpleDto(UserLogin userLogin, Client domain) {
        return new NameCategoryDto(domain);
    }

    @Override
    public NameCategoryDto createDetailDto(UserLogin userLogin, Client domain) {
        return createSimpleDto(userLogin, domain);
    }

}
