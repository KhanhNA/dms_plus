package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.domain.Distributor;
import com.viettel.backend.domain.District;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.DistributorEmbed;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.CustomerCreateDto;
import com.viettel.backend.dto.CustomerDto;
import com.viettel.backend.dto.CustomerForTrackingDto;
import com.viettel.backend.dto.CustomerForVisitDto;
import com.viettel.backend.dto.CustomerSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CodeGeneratorRepository;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SupervisorCustomerService;
import com.viettel.backend.service.engine.FileEngine;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.util.DateTimeUtils;

@Service
public class SupervisorCustomerServiceImpl extends AbstractSupervisorService implements SupervisorCustomerService {

    private static final long serialVersionUID = 2812222944238656195L;

    public static final Sort SORT_BY_CREATE_TIME_DESC = new Sort(Direction.DESC, Customer.COLUMNNAME_CREATED_TIME_VALUE);

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private FileEngine fileEngine;

    @Autowired
    private WebNotificationEngine webNotificationEngine;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public ListJson<CustomerSimpleDto> getCustomers(UserLogin userLogin, String search, String distributorId,
            String salesmanId, Pageable pageable) {
        Distributor distributor = null;

        if (distributorId != null) {
            distributor = checkSupervisorDistributor(userLogin, distributorId);
        }

        User salesman = null;
        if (salesmanId != null) {
            if (distributor != null) {
                salesman = checkSupervisorSalesman(userLogin, salesmanId, distributor.getId());
            } else {
                salesman = checkSupervisorSalesman(userLogin, salesmanId);
            }
        }

        List<Customer> customers = null;
        long count = -1;
        if (salesman != null) {
            Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(userLogin.getClientId(),
                    Collections.singleton(salesman.getId()));

            customers = customerRepository.getCustomersByRoute(userLogin.getClientId(), routeIds, search, null,
                    pageable, null);
            if (pageable != null) {
                count = customerRepository.countCustomersByRoute(userLogin.getClientId(), routeIds, search, null);
            }
        } else {
            if (distributor != null) {
                customers = customerRepository.getCustomersByDistributors(userLogin.getClientId(),
                        Collections.singleton(distributor.getId()), false, search, null, pageable, null);
                if (pageable != null) {
                    count = customerRepository.countCustomersByDistributors(userLogin.getClientId(),
                            Collections.singleton(distributor.getId()), false, search, null);
                }
            } else {
                customers = customerRepository.getCustomers(userLogin.getClientId(), search, pageable, null);
                if (pageable != null) {
                    count = customerRepository.count(userLogin.getClientId(), search);
                }
            }
        }

        if (customers == null || customers.isEmpty()) {
            return ListJson.<CustomerSimpleDto> emptyList();
        }

        List<CustomerSimpleDto> dtos = new ArrayList<CustomerSimpleDto>(customers.size());
        for (Customer customer : customers) {
            CustomerSimpleDto dto = new CustomerSimpleDto(customer);
            dtos.add(dto);
        }

        if (pageable != null) {
            return new ListJson<CustomerSimpleDto>(dtos, count);
        } else {
            return new ListJson<CustomerSimpleDto>(dtos, (long) dtos.size());
        }
    }

    @Override
    public CustomerDto getCustomerById(UserLogin userLogin, String id) {
        checkIsSupervisor(userLogin);

        Customer customer = checkSupervisorCustomer(userLogin, id);

        CustomerDto dto = new CustomerDto(customer);

        return dto;
    }

    @Override
    public ListJson<CustomerSimpleDto> getCustomersPending(UserLogin userLogin, String search, String salesmanId,
            Pageable pageable) {
        List<Customer> customers = null;
        long count = -1;
        if (salesmanId != null) {
            User salesman = checkSupervisorSalesman(userLogin, salesmanId);
            customers = customerPendingRepository.getCustomersByCreatedUsers(userLogin.getClientId(),
                    Arrays.asList(salesman.getId()), Customer.APPROVE_STATUS_PENDING, search, pageable,
                    SORT_BY_CREATE_TIME_DESC);
            if (pageable != null) {
                count = customerPendingRepository.countCustomersByCreatedUsers(userLogin.getClientId(),
                        Arrays.asList(salesman.getId()), Customer.APPROVE_STATUS_PENDING, search);
            }
        } else {
            Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                    userLogin.getClientId(), Arrays.asList(userLogin.getUserId()));

            customers = customerPendingRepository.getCustomersByDistributors(userLogin.getClientId(), distributorIds,
                    Customer.APPROVE_STATUS_PENDING, search, pageable, SORT_BY_CREATE_TIME_DESC);
            if (pageable != null) {
                count = customerPendingRepository.countCustomersByDistributors(userLogin.getClientId(), distributorIds,
                        Customer.APPROVE_STATUS_PENDING, search);
            }
        }

        if (customers == null || customers.isEmpty()) {
            return ListJson.<CustomerSimpleDto> emptyList();
        }

        List<CustomerSimpleDto> dtos = new ArrayList<CustomerSimpleDto>(customers.size());
        for (Customer customer : customers) {
            CustomerSimpleDto dto = new CustomerSimpleDto(customer);
            dtos.add(dto);
        }

        if (pageable != null) {
            return new ListJson<CustomerSimpleDto>(dtos, count);
        } else {
            return new ListJson<CustomerSimpleDto>(dtos, (long) customers.size());
        }

    }

    @Override
    public CustomerDto getCustomerPendingById(UserLogin userLogin, String id) {
        checkIsSupervisor(userLogin);

        Customer customer = checkSupervisorCustomerPending(userLogin, id);

        CustomerDto dto = new CustomerDto(customer);

        return dto;
    }

    @Override
    public void approveCustomer(UserLogin userLogin, String id) {
        checkIsSupervisor(userLogin);

        Customer customer = checkSupervisorCustomerPending(userLogin, id);
        customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);

        customerPendingRepository.save(userLogin.getClientId(), customer);

        webNotificationEngine.notifyChangedCustomerForSupervisor(userLogin, userLogin.getUserId());
    }

    @Override
    public void rejectCustomer(UserLogin userLogin, String id) {
        checkIsSupervisor(userLogin);

        Customer customer = checkSupervisorCustomerPending(userLogin, id);
        customer.setApproveStatus(Customer.APPROVE_STATUS_REJECTED);

        customerPendingRepository.save(userLogin.getClientId(), customer);

        webNotificationEngine.notifyChangedCustomerForSupervisor(userLogin, userLogin.getUserId());
    }

    @Override
    public ListJson<CustomerForTrackingDto> getCustomersTodayBySalesman(UserLogin userLogin, String salesmanId) {
        checkIsSupervisor(userLogin);

        User salesman = checkSupervisorSalesman(userLogin, salesmanId);

        ObjectId clientId = userLogin.getClientId();

        Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(userLogin.getClientId(),
                Collections.singleton(salesman.getId()));
        Collection<Customer> todayCustomers = customerRepository.getCustomersByRouteDate(clientId, routeIds,
                null, DateTimeUtils.getToday(), null, null);
        if (todayCustomers == null || todayCustomers.isEmpty()) {
            return ListJson.emptyList();
        }

        List<ObjectId> todayCustomerIds = new ArrayList<ObjectId>(todayCustomers.size());
        for (Customer customer : todayCustomers) {
            todayCustomerIds.add(customer.getId());
        }

        Map<ObjectId, Visit> visitTodayByCustomerId = visitRepository.getMapVisitByCustomerIdsToday(clientId,
                todayCustomerIds);

        List<CustomerForTrackingDto> customerDtos = new ArrayList<>(todayCustomers.size());
        for (Customer customer : todayCustomers) {
            // STATUS
            int status = CustomerForVisitDto.STATUS_UNVISITED;
            Visit visit = visitTodayByCustomerId.get(customer.getId());
            if (visit != null) {
                if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITING) {
                    status = CustomerForVisitDto.STATUS_VISITING;
                } else {
                    status = CustomerForVisitDto.STATUS_VISITED;
                }
            }

            CustomerForTrackingDto customerDto = new CustomerForTrackingDto(customer, true, status, 0, visit);
            customerDtos.add(customerDto);
        }

        return new ListJson<>(customerDtos, Long.valueOf(customerDtos.size()));
    }

    public String createCustomer(UserLogin userLogin, CustomerCreateDto dto) {
        checkIsSupervisor(userLogin);

        if (dto == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        User supervisor = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
        Distributor distributor = checkSupervisorDistributor(userLogin, dto.getDistributorId());

        Customer customer = new Customer();
        initPOWhenCreate(Customer.class, userLogin, customer);
        customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
        customer.setCreatedTime(DateTimeUtils.getCurrentTime());
        customer.setCreatedBy(new UserEmbed(supervisor));

        customer.setName(dto.getName());
        customer.setCode(codeGeneratorRepository.getCustomerCode(userLogin.getClientId().toString()));
        customer.setMobile(dto.getMobile());
        customer.setPhone(dto.getPhone());
        customer.setContact(dto.getContact());
        customer.setIdNumber(dto.getIdNumber());
        customer.setEmail(dto.getEmail());
        // customer.setAddress(dto.getAddress());
        customer.setDescription(dto.getDescription());

        customer.setDistributor(new DistributorEmbed(distributor));

        CustomerType customerType = getMadatoryPO(userLogin, dto.getCustomerTypeId(), customerTypeRepository);
        customer.setCustomerType(customerType);

        District district = getMadatoryPO(userLogin, dto.getDistrictId(), districtRepository);
        customer.setDistrict(district);

        if (dto.getPhotos() != null && dto.getPhotos().size() > 0) {
            if (!fileEngine.exists(userLogin, dto.getPhotos())) {
                throw new BusinessException(ExceptionCode.INVALID_PARAM);
            }
            customer.setPhotos(dto.getPhotos());
        }

        if (dto.getLatitude() > 0 & dto.getLongitude() > 0) {
            customer.setLocation(new double[] { dto.getLongitude(), dto.getLatitude() });
        }

        customer = customerRepository.save(userLogin.getClientId(), customer);

        return customer.getId().toString();
    }

}
