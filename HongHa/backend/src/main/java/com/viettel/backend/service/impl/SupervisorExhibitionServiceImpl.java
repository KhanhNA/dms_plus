package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Exhibition;
import com.viettel.backend.domain.Survey;
import com.viettel.backend.dto.ExhibitionSimpleDto;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ExhibitionRepository;
import com.viettel.backend.service.SupervisorExhibitionService;

@Service
public class SupervisorExhibitionServiceImpl extends AbstractSupervisorService implements SupervisorExhibitionService {

    private static final long serialVersionUID = 4822917139294672054L;

    @Autowired
    private ExhibitionRepository exhibitionRepository;

    @Override
    public ListJson<ExhibitionSimpleDto> getExhibitions(UserLogin userLogin, String search, Pageable pageable) {
        checkIsSupervisor(userLogin);

        Sort sort = new Sort(Direction.DESC, Survey.COLUMNNAME_END_DATE_VALUE);
        List<Exhibition> exhibitions = exhibitionRepository.getExhibitions(userLogin.getClientId(), search, pageable,
                sort);

        if (exhibitions == null || exhibitions.isEmpty()) {
            return ListJson.emptyList();
        }

        List<ExhibitionSimpleDto> dtos = new ArrayList<ExhibitionSimpleDto>(exhibitions.size());
        for (Exhibition exhibition : exhibitions) {
            dtos.add(new ExhibitionSimpleDto(exhibition));
        }

        long size = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || size == pageable.getPageSize()) {
                size = exhibitionRepository.count(userLogin.getClientId(), search);
            }
        }

        return new ListJson<ExhibitionSimpleDto>(dtos, size);
    }

}
