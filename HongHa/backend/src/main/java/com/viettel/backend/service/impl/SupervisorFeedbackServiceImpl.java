package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.VisitFeedbackDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.FeedbackRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SupervisorFeedbackService;
import com.viettel.backend.service.engine.WebNotificationEngine;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SupervisorFeedbackServiceImpl extends AbstractSupervisorService implements SupervisorFeedbackService {

    private static final long serialVersionUID = 4822917139294672054L;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private WebNotificationEngine webNotificationEngine;

    @Override
    public ListJson<VisitFeedbackDto> getFeedbacks(UserLogin userLogin, Pageable pageable) {
        checkIsSupervisor(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId supervisorId = userLogin.getUserId();

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                userLogin.getClientId(), Collections.singleton(supervisorId));
        Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                distributorIds, false, null, null);

        if (customerIds == null || customerIds.isEmpty()) {
            return ListJson.<VisitFeedbackDto> emptyList();
        }

        Sort sort = new Sort(new Order(Direction.ASC, Visit.COLUMNNAME_IS_FEEDBACKS_READED), new Order(Direction.DESC,
                Visit.COLUMNNAME_END_TIME_VALUE));
        List<Visit> visits = feedbackRepository.getFeedbackByCustomers(clientId, customerIds, pageable, sort);
        if (visits == null || visits.isEmpty()) {
            return ListJson.<VisitFeedbackDto> emptyList();
        }

        List<VisitFeedbackDto> dtos = new ArrayList<VisitFeedbackDto>(visits.size());
        for (Visit visit : visits) {
            VisitFeedbackDto dto = new VisitFeedbackDto();
            dto.setId(visit.getId().toString());
            dto.setSalesman(visit.getSalesman().getFullname());
            dto.setCustomer(visit.getCustomer().getName());
            dto.setCreatedTime(visit.getEndTime().getIsoTime());
            dto.setFeedbacks(visit.getFeedbacks());
            dto.setFeedbacksReaded(visit.isFeedbacksReaded());

            dtos.add(dto);
        }
        
        long count = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || count == pageable.getPageSize()) {
                count = feedbackRepository.countFeedbackByCustomers(clientId, customerIds);
            }
        }

        return new ListJson<VisitFeedbackDto>(dtos, count);
    }

    @Override
    public VisitFeedbackDto readFeedback(UserLogin userLogin, String _feedbackId) {
        checkIsSupervisor(userLogin);

        ObjectId clientId = userLogin.getClientId();
        ObjectId supervisorId = userLogin.getUserId();

        ObjectId feedbackId = ObjectIdUtils.getObjectId(_feedbackId, null);
        if (feedbackId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
        Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                distributorIds, false, null, null);
        if (customerIds == null || customerIds.isEmpty()) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Visit visit = feedbackRepository.getById(clientId, feedbackId);
        if (visit == null || !customerIds.contains(visit.getCustomer().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        if (!visit.isFeedbacksReaded()) {
            feedbackRepository.markAsRead(clientId, feedbackId);
            webNotificationEngine.notifyChangedFeedbackForSupervisor(userLogin, supervisorId);
        }

        VisitFeedbackDto dto = new VisitFeedbackDto();
        dto.setId(visit.getId().toString());
        dto.setSalesman(visit.getSalesman().getFullname());
        dto.setCustomer(visit.getCustomer().getName());
        dto.setCreatedTime(visit.getEndTime().getIsoTime());
        dto.setFeedbacks(visit.getFeedbacks());
        dto.setFeedbacksReaded(false);

        return dto;
    }

}
