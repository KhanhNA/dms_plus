package com.viettel.backend.service.impl;

import java.util.Arrays;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.dto.SupervisorNotificationDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerPendingRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.FeedbackRepository;
import com.viettel.backend.repository.OrderPendingRepository;
import com.viettel.backend.service.SupervisorNotificationService;

@Service
public class SupervisorNotificationServiceImpl extends AbstractSupervisorService implements
        SupervisorNotificationService {

    private static final long serialVersionUID = 4822917139294672054L;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    public SupervisorNotificationDto getNotification(UserLogin userLogin) {
        checkIsSupervisor(userLogin);

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(userLogin.getClientId(),
                Arrays.asList(userLogin.getUserId()));

        long nbCustomerToApprove = 0;
        if (distributorIds != null && !distributorIds.isEmpty()) {
            nbCustomerToApprove = customerPendingRepository.countCustomersByDistributors(userLogin.getClientId(),
                    distributorIds, Customer.APPROVE_STATUS_PENDING, null);
        }

        Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                distributorIds, false, null, null);

        long nbFeedbackToRead = 0;
        if (customerIds != null && !customerIds.isEmpty()) {
            nbFeedbackToRead = feedbackRepository.countFeedbackByCustomersUnread(userLogin.getClientId(), customerIds);
        }

        return new SupervisorNotificationDto(nbFeedbackToRead, nbCustomerToApprove);
    }

}
