package com.viettel.backend.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Target;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.dto.ListJson;
import com.viettel.backend.dto.TargetCreateDto;
import com.viettel.backend.dto.TargetDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.TargetRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SupervisorTargetService;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SupervisorTargetServiceImpl extends AbstractSupervisorService implements SupervisorTargetService {

    private static final long serialVersionUID = 5936351463966015522L;

    public static final Sort SORT_BY_DATE_DESC = new Sort(Direction.DESC, Target.COLUMNNAME_YEAR,
            Target.COLUMNNAME_MONTH);

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    public ListJson<TargetDto> getTargets(UserLogin userLogin, int month, int year, Pageable pageable) {
        checkIsSupervisor(userLogin);
        ObjectId clientId = userLogin.getClientId();
        ObjectId supervisorId = userLogin.getUserId();

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(clientId,
                Arrays.asList(supervisorId));

        Set<ObjectId> salesmanIds = userRepository.getSalesmanIdsByDistributors(clientId, distributorIds);
        if (salesmanIds == null || salesmanIds.isEmpty()) {
            return ListJson.emptyList();
        }

        List<Target> targets = targetRepository.getTargetsBySalesmen(clientId, salesmanIds, month, year, pageable,
                SORT_BY_DATE_DESC);
        if (targets == null || targets.isEmpty()) {
            return ListJson.emptyList();
        }

        int targetsSize = targets.size();
        List<TargetDto> dtos = new ArrayList<TargetDto>(targetsSize);
        for (int i = 0; i < targetsSize; i++) {
            Target target = targets.get(i);
            dtos.add(new TargetDto(target));
        }

        long size = Long.valueOf(dtos.size());
        if (pageable != null) {
            if (pageable.getPageNumber() > 0 || pageable.getPageSize() == size) {
                size = targetRepository.countTargetsBySalesmen(clientId, salesmanIds, month, year);
            }
        }

        return new ListJson<TargetDto>(dtos, size);
    }

    @Override
    public TargetDto getById(UserLogin userLogin, String _targetId) {
        checkIsSupervisor(userLogin);
        ObjectId clientId = userLogin.getClientId();
        ObjectId targetId = ObjectIdUtils.getObjectId(_targetId, null);

        if (targetId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Target target = targetRepository.getById(clientId, targetId);
        if (target == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        checkSupervisorSalesman(userLogin, target.getSalesman().getId());
        return new TargetDto(target);
    }

    @Override
    public TargetDto create(UserLogin userLogin, TargetCreateDto dto) {
        checkIsSupervisor(userLogin);

        checkMandatoryParams(dto, dto.getYear(), dto.getMonth(), dto.getSalesmanId(), dto.getRevenue());

        if (dto.getMonth() < 1 || dto.getMonth() > 12) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        // Not sure how to check year, huh
        if (dto.getYear() < 1900) {
            throw new BusinessException(ExceptionCode.INVALID_DATE_PARAM);
        }

        ObjectId salesmanId = ObjectIdUtils.getObjectId(dto.getSalesmanId(), null);
        if (targetRepository.existsTargetBySalesman(userLogin.getClientId(), salesmanId, dto.getMonth(), dto.getYear())) {
            throw new BusinessException(ExceptionCode.SALESMAN_HAS_TARGET);
        }

        User salesman = checkSupervisorSalesman(userLogin, dto.getSalesmanId());

        Target target = new Target();
        initPOWhenCreate(Target.class, userLogin, target);
        target.setMonth(dto.getMonth());
        target.setYear(dto.getYear());
        target.setSalesman(new UserEmbed(salesman));

        target.setRevenue(dto.getRevenue());

        target = targetRepository.save(userLogin.getClientId(), target);
        return new TargetDto(target);
    }

    @Override
    public TargetDto update(UserLogin userLogin, String _targetId, TargetCreateDto dto) {
        checkIsSupervisor(userLogin);

        checkMandatoryParams(dto, dto.getRevenue());

        ObjectId clientId = userLogin.getClientId();
        ObjectId targetId = ObjectIdUtils.getObjectId(_targetId, null);

        if (targetId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Target target = targetRepository.getById(clientId, targetId);
        if (target == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        checkSupervisorSalesman(userLogin, target.getSalesman().getId());

        target.setRevenue(dto.getRevenue());

        target = targetRepository.save(clientId, target);
        return new TargetDto(target);
    }

    @Override
    public void delete(UserLogin userLogin, String _targetId) {
        checkIsSupervisor(userLogin);
        ObjectId clientId = userLogin.getClientId();
        ObjectId targetId = ObjectIdUtils.getObjectId(_targetId, null);

        if (targetId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
        Target target = targetRepository.getById(clientId, targetId);
        if (target == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        checkSupervisorSalesman(userLogin, target.getSalesman().getId());

        targetRepository.delete(clientId, targetId);
    }

}
