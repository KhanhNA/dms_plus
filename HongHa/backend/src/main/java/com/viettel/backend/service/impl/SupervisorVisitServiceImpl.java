package com.viettel.backend.service.impl;

import java.util.Collections;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Visit;
import com.viettel.backend.dto.VisitInfoDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.restful.ExceptionCode;
import com.viettel.backend.service.SupervisorVisitService;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class SupervisorVisitServiceImpl extends AbstractSupervisorService implements SupervisorVisitService {

    private static final long serialVersionUID = 4822917139294672054L;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private DistributorRepository distributorRepository;

    @Override
    public VisitInfoDto getVisit(UserLogin userLogin, String id) {
        checkIsSupervisor(userLogin);

        ObjectId visitId = ObjectIdUtils.getObjectId(id, null);
        if (visitId == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Visit visit = visitRepository.getById(userLogin.getClientId(), visitId);

        if (visit == null) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        Set<ObjectId> distributorIds = distributorRepository.getDistributorIdsBySupervisors(
                userLogin.getClientId(), Collections.singleton(userLogin.getUserId()));
        Set<ObjectId> customerIds = customerRepository.getCustomerIdsByDistributors(userLogin.getClientId(),
                distributorIds, false, null, null);

        if (customerIds == null || !customerIds.contains(visit.getCustomer().getId())) {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }

        VisitInfoDto visitInfoDto = new VisitInfoDto(visit, getClientCondig(userLogin).getDefaultProductPhoto());

        return visitInfoDto;
    }

}
