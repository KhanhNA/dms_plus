package com.viettel.backend.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.CustomerType;
import com.viettel.backend.domain.District;
import com.viettel.backend.domain.User;
import com.viettel.backend.domain.embed.CustomerSchedule;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.domain.embed.UserEmbed;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.CustomerTypeRepository;
import com.viettel.backend.repository.DistributorRepository;
import com.viettel.backend.repository.DistrictRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.util.DateTimeUtils;
import com.viettel.backend.util.ObjectIdUtils;

@Service
public class TestImportService extends AbstractSupervisorService {

    private static final long serialVersionUID = 1L;
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    public static final ObjectId CUSTOMER_TYPE_ID_STORE = ObjectIdUtils.getObjectId("5577f13cd4c6d82de99e95cd");
    public static final ObjectId CUSTOMER_TYPE_ID_SCHOOL = ObjectIdUtils.getObjectId("5577f146d4c6d82de99e95ce");
    
    @Autowired
    private CustomerTypeRepository customerTypeRepository;
    
    @Autowired
    private DistributorRepository distributorRepository;
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private DistrictRepository districtRepository;
    
    public void fillLocation() {
        String inputFile = "/Users/thanh/Desktop/IMPORT_OISHI.xls";
        String outFile = "/Users/thanh/Desktop/IMPORT_OISHI.OUT.%s.xls";
        
        String url = "https://maps.googleapis.com/maps/api/geocode/json?address={address},+Hồ+Chí+Minh,+Việt+Nam&key={apiKey}";

        String[] keys = new String[] {
                "AIzaSyBY0Uu29d7dxH_XzCX46fahaT1p0UQnw7s",
                "AIzaSyC0I8vN_1fvKIdcK6AloYsvtE3_98d1Kcs",
                "AIzaSyCcB41NEvzyRhP5xjGzjTtsMv7Orw2AA8E"
        };


        RestTemplate restTemplate = new RestTemplate();
        int keyIndex = 0;
        
        // Read file
        FileInputStream fileInput = null;
        FileOutputStream fileOutput = null;
        HSSFWorkbook workbook = null;
        // Map<Integer, String> mapAddress = new HashMap<Integer, String>(3500);
        try {
            fileInput = new FileInputStream(new File(inputFile));
            workbook = new HSSFWorkbook(fileInput);

            HSSFSheet sheet = workbook.getSheet("Customer");
            String code = null;
            int rowIndex = 0;
            boolean run = true;
            while (run) {
                rowIndex++;
                HSSFRow row = sheet.getRow(rowIndex);
                if (row == null) {
                    break;
                }
                code = row.getCell(8).getStringCellValue();
                if (code == null) {
                    break;
                }
                String address = row.getCell(5).getStringCellValue();
                if (StringUtils.isEmpty(address)) {
                    logger.error("Customer at row " + (rowIndex + 1) + " have an empty address");
                    continue;
                }
                // workbook.wr
                // mapAddress.put(rowIndex, address);
                int tryCount = 0;
                while (tryCount < 5) {
                    logger.error("Trying to get location for customer at row " + (rowIndex + 1) + " with attemp " + (tryCount + 1));
                    tryCount++;
                    try {
                        Thread.sleep(400);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    LocationResult result = null;
                    try {
                        result = restTemplate.getForObject(url, LocationResult.class, address, keys[keyIndex]);
                    } catch (Exception ex) {
                        logger.error("Exception when find location for customer at row " + (rowIndex + 1) + ", address = " + address, ex);
                    }
                    if (result != null 
                            && result.isSuccess()
                            && result.getResults().length > 0) {
                        HSSFCell cellLat = row.createCell(10);
                        cellLat.setCellValue(result.getResults()[0].getGeometry().getLocation().getLat());
                        
                        HSSFCell cellLng = row.createCell(11);
                        cellLng.setCellValue(result.getResults()[0].getGeometry().getLocation().getLng());
                        if (rowIndex % 200 == 0) {
                            logger.error("Flushing data at row " + (rowIndex + 1));

                            fileOutput = new FileOutputStream(new File(String.format(outFile, String.valueOf(rowIndex + 1))));
                            workbook.write(fileOutput);
                        }
                        break;
                    } else {
                        if (result == null) {
                            
                        } else if (result.isNoResult()) {
                            logger.error("Cannot find location for customer at row " + (rowIndex + 1));
                            break;
                        } else if (result.isOverQuota()) {
                            if (++keyIndex >= keys.length) {
                                logger.error("Over quota at row " + (rowIndex + 1));
                                run = false;
                                break;
                            }
                        }
                    }
                }
            }
            logger.error("Writing data to file");
            fileOutput = new FileOutputStream(new File(String.format(outFile, "final")));
            workbook.write(fileOutput);
            logger.error("Finished!!!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileInput != null) {
                try {
                    fileInput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    
    public void insertCustomer(UserLogin userLogin) {

        String inputFile = "/Users/thanh/Desktop/Import_Oishi_New.xls";
        
        ObjectId clientId = userLogin.getClientId();
        
        CustomerType CUSTOMER_TYPE_STORE = customerTypeRepository.getById(clientId, CUSTOMER_TYPE_ID_STORE);
        CustomerType CUSTOMER_TYPE_SCHOOL = customerTypeRepository.getById(clientId, CUSTOMER_TYPE_ID_SCHOOL);
        
        UserEmbed userEmbed = new UserEmbed(getCurrentSupervisor(userLogin));
        SimpleDate createTime = DateTimeUtils.getCurrentTime();
        
        // Load all salesman
        List<User> salesmans = userRepository.getAll(clientId, null);
        Map<String, User> mapCodeSalesman = new HashMap<>(salesmans.size());
        for (User user : salesmans) {
            mapCodeSalesman.put(user.getUsername().toUpperCase(), user);
        }
        
        // Read file
        FileInputStream fileInput = null;
        HSSFWorkbook workbook = null;
        Map<String, Customer> mapCodeCustomer = new HashMap<>(3500);
        try {
            fileInput = new FileInputStream(new File(inputFile));
            workbook = new HSSFWorkbook(fileInput);

            // Load customers
            HSSFSheet sheet = workbook.getSheet("Custumer");
            String code = null;
            int rowIndex = 0;
            
            Map<String, String> mapDistrict = new HashMap<String, String>(3500);
            while (true) {
                rowIndex++;
                HSSFRow row = sheet.getRow(rowIndex);
                if (row == null) {
                    break;
                }
                code = row.getCell(1).getStringCellValue();
                if (code == null) {
                    break;
                }
                
                String district = row.getCell(5).getStringCellValue();
                district = StringUtils.trimWhitespace(district);
                
                mapDistrict.put(district.toUpperCase(), district);
            }
            
            Map<String, District> mapDistrictDomain = new HashMap<String, District>();
            // Create District
            for (String districtName : mapDistrict.values()) {
                District district = new District();
                initPOWhenCreate(District.class, userLogin, district);
                district.setActive(true);
                district.setDraft(false);
                district.setName(districtName);
                district = districtRepository.save(clientId, district);
                mapDistrictDomain.put(districtName.toUpperCase(), district);
            }
            
            rowIndex = 0;
            while (true) {
                rowIndex++;
                HSSFRow row = sheet.getRow(rowIndex);
                if (row == null) {
                    break;
                }
                code = row.getCell(1).getStringCellValue();
                if (code == null) {
                    break;
                }
                String name = row.getCell(2).getStringCellValue();
                String type = row.getCell(3).getStringCellValue();
                String address = row.getCell(4).getStringCellValue();
                double lat = row.getCell(10).getNumericCellValue();
                double lng = row.getCell(11).getNumericCellValue();
                String district = row.getCell(5).getStringCellValue();
                district = StringUtils.trimWhitespace(district);
                
                Customer customer = new Customer();
                initPOWhenCreate(Customer.class, userLogin, customer);
                customer.setDraft(false);
                customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
                customer.setCode(code == null ? null : StringUtils.trimWhitespace(code));
                customer.setName(name == null ? null : StringUtils.trimWhitespace(name));
                customer.setCustomerType("STORE".equalsIgnoreCase(type) ? CUSTOMER_TYPE_STORE : CUSTOMER_TYPE_SCHOOL);
                customer.setAddress(address == null ? null : StringUtils.trimWhitespace(address));
                customer.setMobile("000000");
                customer.setDistrict(mapDistrictDomain.get(district.toUpperCase()));
                customer.setCreatedBy(userEmbed);
                customer.setCreatedTime(createTime);
                customer.setLocation(new double[] {lng, lat});
                
                mapCodeCustomer.put(code.toUpperCase(), customer);
            }
            logger.info("Read " + mapCodeCustomer.size() + " customers from file");
            
            
            // Load schedule
            sheet = workbook.getSheet("ROUTER");
            rowIndex = 1;
            while (true) {
                rowIndex++;
                HSSFRow row = sheet.getRow(rowIndex);
                if (row == null) {
                    break;
                }
                code = row.getCell(0).getStringCellValue();
                String salesmanCode = row.getCell(2).getStringCellValue();
                String customerCode = row.getCell(4).getStringCellValue();
                if (StringUtils.isEmpty(code) || StringUtils.isEmpty(salesmanCode) || StringUtils.isEmpty(customerCode)) {
                    logger.error("Invalid route info at row " + rowIndex);
                    return;
                }
                
                Customer customer = mapCodeCustomer.get(StringUtils.trimWhitespace(customerCode.toUpperCase()));
                if (customer == null) {
                    logger.error("Invalid customer code at " + rowIndex);
                    return;
                }
                
                User salesman = mapCodeSalesman.get(StringUtils.trimWhitespace(salesmanCode.toUpperCase()));
                if (salesman == null) {
                    logger.error("Invalid salesman code at " + rowIndex);
                    return;
                }
                
                CustomerSchedule schedule = new CustomerSchedule();
//                schedule.setSalesmanId(salesman.getId());
                schedule.setItems(Arrays.asList(getCustomerScheduleItem(row)));
                
                customer.setSchedule(schedule);
                customer.setDistributor(salesman.getDistributor());
            }
            logger.info("Read " + (rowIndex - 2) + " route from file");
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileInput != null) {
                try {
                    fileInput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Insert to DB
        int rowIndex = 1;
        Set<String> customerName = new HashSet<>(3500);
        for (Customer customer : mapCodeCustomer.values()) {
            //logger.info("Inserting customer at row " + rowIndex);
            if (customerName.contains(customer.getName().toUpperCase())) {
                logger.error(rowIndex + ": " + customer.getName());
                continue;
            }
            customerName.add(customer.getName().toUpperCase());
            customerRepository.save(clientId, customer);
            rowIndex++;
        }
        
        logger.info("Successfully! " + rowIndex + " inserted!");
    }
    
    private CustomerScheduleItem getCustomerScheduleItem(HSSFRow row) {
        CustomerScheduleItem item = new CustomerScheduleItem();
        // D = 9 -> 15
        List<Integer> days = new ArrayList<>(8);
        for (int i = 9; i < 16; i++) {
            HSSFCell cell = row.getCell(i);
            if (cell != null && !StringUtils.isEmpty(cell.getStringCellValue())) {
                if (i == 15) {
                    days.add(1);
                } else {
                    days.add(i - 7);
                }
            }
        }
        item.setDays(days);
        
        // W = 5 -> 8
        List<Integer> weeks = new ArrayList<>(4);
        for (int i = 5; i < 9; i++) {
            HSSFCell cell = row.getCell(i);
            if (cell != null && !StringUtils.isEmpty(cell.getStringCellValue())) {
                weeks.add(i - 4);
            }
        }
        item.setWeeks(weeks);
        
        return item;
    }
    
    public void insertCustomer2(UserLogin userLogin) {

        String inputFile = "/Users/thanh/Desktop/IMPORT_OISHI.OUT.final.xls";
        
        ObjectId clientId = userLogin.getClientId();
        
        CustomerType CUSTOMER_TYPE_STORE = customerTypeRepository.getById(clientId, CUSTOMER_TYPE_ID_STORE);
        CustomerType CUSTOMER_TYPE_SCHOOL = customerTypeRepository.getById(clientId, CUSTOMER_TYPE_ID_SCHOOL);
        
        UserEmbed userEmbed = new UserEmbed(getCurrentSupervisor(userLogin));
        SimpleDate createTime = DateTimeUtils.getCurrentTime();
        
        // Load all salesman
        List<User> salesmans = userRepository.getAll(clientId, null);
        Map<String, User> mapCodeSalesman = new HashMap<>(salesmans.size());
        for (User user : salesmans) {
            mapCodeSalesman.put(user.getUsername().toUpperCase(), user);
        }
        
        // Read file
        FileInputStream fileInput = null;
        HSSFWorkbook workbook = null;
        List<Customer> listCustomer = new ArrayList<>(3500);
        
        try {
            fileInput = new FileInputStream(new File(inputFile));
            workbook = new HSSFWorkbook(fileInput);

            // Load customers
            HSSFSheet sheet = workbook.getSheet("Customer");
            String stt = null;
            int rowIndex = 0;
            
            Map<String, String> mapDistrict = new HashMap<String, String>(3500);
            Map<String, Integer> mapDistrictSeq = new HashMap<String, Integer>(3500);
            Map<String, AtomicInteger> mapDistrictCustomerSeq = new HashMap<String, AtomicInteger>(3500);
            AtomicInteger districtSeq = new AtomicInteger();
            while (true) {
                rowIndex++;
                HSSFRow row = sheet.getRow(rowIndex);
                if (row == null) {
                    break;
                }
                stt = row.getCell(0) != null ? String.valueOf(row.getCell(0).getNumericCellValue()) : null;
                if (stt == null) {
                    break;
                }
                
                String district = row.getCell(7).getStringCellValue();
                district = StringUtils.trimWhitespace(district);
                
                if (!mapDistrict.containsKey(district.toUpperCase())) {
                	mapDistrict.put(district.toUpperCase(), district);
                	mapDistrictSeq.put(district.toUpperCase(), districtSeq.incrementAndGet());
                	mapDistrictCustomerSeq.put(district.toUpperCase(), new AtomicInteger());
                }
            }
            
            Map<String, District> mapDistrictDomain = new HashMap<String, District>();
            // Create District
            for (String districtName : mapDistrict.values()) {
                District district = new District();
                initPOWhenCreate(District.class, userLogin, district);
                district.setActive(true);
                district.setDraft(false);
                district.setName(districtName);
                district = districtRepository.save(clientId, district);
                mapDistrictDomain.put(districtName.toUpperCase(), district);
            }
            
            rowIndex = 0;
            while (true) {
                rowIndex++;
                HSSFRow row = sheet.getRow(rowIndex);
                if (row == null) {
                    break;
                }
                stt = row.getCell(0) != null ? String.valueOf(row.getCell(0).getNumericCellValue()) : null;
                if (stt == null) {
                    break;
                }
                String route = row.getCell(1).getStringCellValue();
                String name = row.getCell(5).getStringCellValue();
                String type = row.getCell(6).getStringCellValue();
                String salesmanCode = row.getCell(8).getStringCellValue();
                double lat = row.getCell(10).getNumericCellValue();
                double lng = row.getCell(11).getNumericCellValue();
                String district = row.getCell(7).getStringCellValue();
                district = StringUtils.trimWhitespace(district).toUpperCase();
                
                User salesman = mapCodeSalesman.get(StringUtils.trimWhitespace(salesmanCode.toUpperCase()));
                if (salesman == null) {
                    logger.error("Invalid salesman code at " + rowIndex);
                    return;
                }
                
                int districtNo = mapDistrictSeq.get(district);
                int customerNo = mapDistrictCustomerSeq.get(district).incrementAndGet();
                String code = String.format("HC%03d-%03d", districtNo, customerNo);
                
                Customer customer = new Customer();
                initPOWhenCreate(Customer.class, userLogin, customer);
                customer.setDraft(false);
                customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
                customer.setCode(code == null ? null : StringUtils.trimWhitespace(code));
                customer.setName(name == null ? null : StringUtils.trimWhitespace(name));
                customer.setCustomerType("STORE".equalsIgnoreCase(type) ? CUSTOMER_TYPE_STORE : CUSTOMER_TYPE_SCHOOL);
                customer.setAddress(null);
                customer.setMobile("000000");
                customer.setDistrict(mapDistrictDomain.get(district));
                customer.setCreatedBy(userEmbed);
                customer.setCreatedTime(createTime);
                customer.setLocation(new double[] {lng, lat});
                
                customer.setSchedule(getCustomerSchedule(salesman, route));
                customer.setDistributor(salesman.getDistributor());
                
                listCustomer.add(customer);
            }
            logger.info("Read " + listCustomer.size() + " customers from file");
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileInput != null) {
                try {
                    fileInput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Insert to DB
        int rowIndex = 1;
        Set<String> customerName = new HashSet<>(3500);
        for (Customer customer : listCustomer) {
            //logger.info("Inserting customer at row " + rowIndex);
            if (customerName.contains(customer.getName().toUpperCase())) {
                logger.error(rowIndex + ": " + customer.getName());
                continue;
            }
            customerName.add(customer.getName().toUpperCase());
            customerRepository.save(clientId, customer);
            rowIndex++;
        }
        
        logger.info("Successfully! " + rowIndex + " inserted!");
    }
    
    private CustomerSchedule getCustomerSchedule(User salesman, String visitDate) {
        
        CustomerScheduleItem item = new CustomerScheduleItem();
        // D = 9 -> 15
        List<Integer> days = Collections.singletonList(getVisitDate(visitDate));
        item.setDays(days);
        
        // W = 5 -> 8
        List<Integer> weeks = new ArrayList<>(4);
        for (int i = 5; i < 9; i++) {
        	weeks.add(i - 4);
        }
        item.setWeeks(weeks);
        
    	CustomerSchedule schedule = new CustomerSchedule();
//        schedule.setSalesmanId(salesman.getId());
        schedule.setItems(Collections.singletonList(item));
        
    	return schedule;
    }
    
    private int getVisitDate(String visitDate) {
    	visitDate = visitDate.toUpperCase();
    	switch (visitDate) {
			case "CN":
				return 1;
			case "T2":
				return 2;
			case "T3":
				return 3;
			case "T4":
				return 4;
			case "T5":
				return 5;
			case "T6":
				return 6;
			case "T7":
				return 7;
		}
    	return -1;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LocationResult {
        
        private String status;
        private LocationItem[] results;

        public LocationItem[] getResults() {
            return results;
        }

        public void setResults(LocationItem[] results) {
            this.results = results;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
        
        public boolean isSuccess() {
            return "OK".equalsIgnoreCase(status);
        }
        
        public boolean isNoResult() {
            return "ZERO_RESULTS".equalsIgnoreCase(status);
        }
        
        public boolean isOverQuota() {
            return "OVER_QUERY_LIMIT".equalsIgnoreCase(status);
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LocationItem {

        private Geometry geometry;

        public Geometry getGeometry() {
            return geometry;
        }

        public void setGeometry(Geometry geometry) {
            this.geometry = geometry;
        }

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Geometry {

            private Location location;

            public Location getLocation() {
                return location;
            }

            public void setLocation(Location location) {
                this.location = location;
            }

            public static class Location {
                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }
        }
    }
}
