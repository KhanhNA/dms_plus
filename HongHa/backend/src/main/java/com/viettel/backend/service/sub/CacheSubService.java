package com.viettel.backend.service.sub;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.cache.redis.CacheManager;
import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.PO;
import com.viettel.backend.domain.Visit;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.ClientConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.RouteRepository;

@Service
public class CacheSubService implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private RouteRepository routeRepository;
    
    @Autowired
    private ClientConfigRepository clientConfigRepository;

    @Autowired
    private CacheManager cacheManager;

    public void reclaculateScheduleNumberPlannedCache(UserLogin userLogin, Collection<ObjectId> routeIds) {
        try {
            HashSet<String> routeIdTxts = new HashSet<String>();
            for (ObjectId routeId : routeIds) {
                routeIdTxts.add(routeId.toString());
            }

            cacheManager.getScheduleNumberPlannedCache().delete(routeIdTxts);

            List<Customer> customers = customerRepository.getCustomersByRoute(userLogin.getClientId(), routeIds,
                    null, null, null, null);
            initScheduleNumberPlannedCache(customers);
        } catch (Exception e) {
            logger.error("cache error", e);
        }
    }

    public void initScheduleNumberPlannedCache(Collection<Customer> customers) {
        ClientConfig clientConfig = clientConfigRepository.getClientConfig(PO.CLIENT_ROOT_ID);
        if (clientConfig == null) {
            throw new UnsupportedOperationException("client config not found");
        }
        
        MultiKeyMap<String, Integer> visitRouteMap = new MultiKeyMap<String, Integer>();
        for (Customer customer : customers) {
            if (customer.getSchedule() == null || customer.getSchedule().getRouteId() == null) {
                continue;
            }
            
            Collection<CustomerScheduleItem> items = null;
            if (clientConfig.isComplexSchedule()) {
                if (customer.getSchedule().getItems() == null || customer.getSchedule().getItems().isEmpty()) {
                    continue;
                }
                items = customer.getSchedule().getItems();
            } else {
                if (customer.getSchedule().getItem() == null) {
                    continue;
                }
                items = Collections.singleton(customer.getSchedule().getItem());
            }

            for (CustomerScheduleItem item : items) {
                // MANY WEEK FREQUENCY
                if (clientConfig.getNumberWeekOfFrequency() > 1) {
                    if (item.getWeeks() == null) {
                        continue;
                    }
                    
                    for (int week : item.getWeeks()) {
                        for (int day : item.getDays()) {
                            String key2 = week + "-" + day;
                            incrementMapValue(visitRouteMap, customer.getSchedule().getRouteId().toString(), key2, 1);
                        }
                    }
                } 
                // ONLY ONE
                else {
                    for (int day : item.getDays()) {
                        String key2 = String.valueOf(day);
                        incrementMapValue(visitRouteMap, customer.getSchedule().getRouteId().toString(), key2, 1);
                    }
                }
            }
        }

        for (Entry<MultiKey<? extends String>, Integer> entry : visitRouteMap.entrySet()) {
            cacheManager.getScheduleNumberPlannedCache().get(entry.getKey().getKey(0))
                    .put(entry.getKey().getKey(1), entry.getValue());
        }
    }

    public void addNewApprovedVisit(Visit visit) {
        try {
            String key_distributor_monthly = visit.getDistributor().getId().toString() + "-"
                    + visit.getStartTime().getIsoMonth();
            String key_distributor_daily = visit.getDistributor().getId().toString() + "-"
                    + visit.getStartTime().getIsoDate();

            cacheManager.getDistributorDataCache().get(key_distributor_monthly)
                    .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT, 1);

            cacheManager.getDistributorDataCache().get(key_distributor_daily)
                    .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT, 1);

            if (visit.isErrorDuration() && !visit.isClosed()) {
                cacheManager.getDistributorDataCache().get(key_distributor_daily)
                        .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_ERROR_DURATION, 1);
            }

            if (visit.getLocationStatus() != Visit.LOCATION_STATUS_LOCATED) {
                cacheManager.getDistributorDataCache().get(key_distributor_daily)
                        .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_ERROR_POSITION, 1);
            }

            if (visit.isOrder()) {
                cacheManager.getDistributorDataCache().get(key_distributor_daily)
                        .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_NBVISIT_WITH_PO, 1);
            }

            cacheManager.getDistributorDataCache().get(key_distributor_monthly).expire(62, TimeUnit.DAYS);
            cacheManager.getDistributorDataCache().get(key_distributor_daily).expire(62, TimeUnit.DAYS);

            // Update salesman score cache
            cacheManager.getDistributorSalesmanVisitedCache().get(key_distributor_monthly)
                    .increment(visit.getSalesman().getId(), 1);
            cacheManager.getDistributorSalesmanVisitedCache().get(key_distributor_daily)
                    .increment(visit.getSalesman().getId(), 1);
        } catch (Exception e) {
            logger.error("cache error", e);
        }
    }

    public void addNewApprovedOrder(Order order) {
        try {
            String key_distributor_monthly = order.getDistributor().getId().toString() + "-"
                    + order.getApproveTime().getIsoMonth();
            String key_distributor_daily = order.getDistributor().getId().toString() + "-"
                    + order.getApproveTime().getIsoDate();

            String key_admin_monthly = order.getApproveTime().getIsoMonth();
            String key_admin_daily = order.getApproveTime().getIsoDate();

            cacheManager.getDistributorDataCache().get(key_distributor_monthly)
                    .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER, 1);
            cacheManager.getDistributorDataCache().get(key_distributor_daily)
                    .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER, 1);

            cacheManager.getDistributorDataCache().get(key_distributor_monthly)
                    .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_REVENUE, order.getGrandTotal().doubleValue());
            cacheManager.getDistributorDataCache().get(key_distributor_daily)
                    .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_REVENUE, order.getGrandTotal().doubleValue());

            cacheManager.getDistributorSalesmanRevenueCache().get(key_distributor_monthly)
                    .increment(order.getSalesman().getId(), order.getGrandTotal().doubleValue());
            cacheManager.getDistributorSalesmanRevenueCache().get(key_distributor_daily)
                    .increment(order.getSalesman().getId(), order.getGrandTotal().doubleValue());

            if (!order.isVisit()) {
                cacheManager.getDistributorDataCache().get(key_distributor_daily)
                        .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_NBORDER_WITHOUT_VISIT, 1);
            }

            for (OrderDetail detail : order.getDetails()) {
                cacheManager.getDistributorDataCache().get(key_distributor_monthly)
                        .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_QUANTITY, detail.getQuantity().doubleValue());
                cacheManager.getDistributorDataCache().get(key_distributor_daily)
                        .increment(CacheManager.DISTRIBUTOR_DATA_TYPE_QUANTITY, detail.getQuantity().doubleValue());

                cacheManager.getDistributorProductQuantityCache().get(key_distributor_monthly)
                        .increment(detail.getProduct().getId(), detail.getQuantity().intValue());
                cacheManager.getDistributorProductQuantityCache().get(key_distributor_daily)
                        .increment(detail.getProduct().getId(), detail.getQuantity().intValue());

                cacheManager.getAdminProductQuantityCache().get(key_admin_monthly)
                        .increment(detail.getProduct().getId(), detail.getQuantity().intValue());
                cacheManager.getAdminProductQuantityCache().get(key_admin_daily)
                        .increment(detail.getProduct().getId(), detail.getQuantity().intValue());
            }

            cacheManager.getDistributorDataCache().get(key_distributor_monthly).expire(62, TimeUnit.DAYS);
            cacheManager.getDistributorDataCache().get(key_distributor_daily).expire(62, TimeUnit.DAYS);

            cacheManager.getDistributorSalesmanRevenueCache().get(key_distributor_monthly).expire(62, TimeUnit.DAYS);
            cacheManager.getDistributorSalesmanRevenueCache().get(key_distributor_daily).expire(62, TimeUnit.DAYS);

            cacheManager.getDistributorProductQuantityCache().get(key_distributor_monthly).expire(62, TimeUnit.DAYS);
            cacheManager.getDistributorProductQuantityCache().get(key_distributor_daily).expire(62, TimeUnit.DAYS);
            cacheManager.getAdminProductQuantityCache().get(key_admin_monthly).expire(62, TimeUnit.DAYS);
            cacheManager.getAdminProductQuantityCache().get(key_admin_daily).expire(62, TimeUnit.DAYS);
        } catch (Exception e) {
            logger.error("cache error", e);
        }
    }

    // PRIVATE
    private void incrementMapValue(MultiKeyMap<String, Integer> map, String key1, String key2, int value) {
        Integer temp = map.get(key1, key2);
        temp = temp == null ? 0 : temp;
        map.put(key1, key2, temp + value);
    }

}
