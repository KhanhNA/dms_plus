package com.viettel.backend.service.sub;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.Order;
import com.viettel.backend.domain.SalesConfig;
import com.viettel.backend.domain.embed.OrderDetail;
import com.viettel.backend.dto.CustomerSummaryDto;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.OrderRepository;
import com.viettel.backend.repository.SalesConfigRepository;
import com.viettel.backend.util.DateTimeUtils;

@Service
public class CustomerSubService implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private SalesConfigRepository salesConfigRepository;

    public CustomerSummaryDto getCustomerSummary(UserLogin userLogin, Customer customer) {
        ObjectId clientId = userLogin.getClientId();
        
        CustomerSummaryDto dto = new CustomerSummaryDto(customer);

        // THIS MONTH
        List<Order> poOfThisMonths = orderRepository.getOrderByCustomers(clientId, Arrays.asList(customer.getId()),
                DateTimeUtils.getPeriodThisMonthUntilToday(), null, null, null);
        if (poOfThisMonths != null && !poOfThisMonths.isEmpty()) {
            BigDecimal output = BigDecimal.ZERO;
            for (Order order : poOfThisMonths) {
                if (order.getDetails() != null && !order.getDetails().isEmpty()) {
                    for (OrderDetail detail : order.getDetails()) {
                        output = output.add(detail.getOutput());
                    }
                }
            }

            dto.setOutputThisMonth(output);
            dto.setOrdersThisMonth(poOfThisMonths.size());
        } else {
            dto.setOutputThisMonth(BigDecimal.ZERO);
            dto.setOrdersThisMonth(0);
        }

        List<Map<String, Object>> revenueLastThreeMonth = new ArrayList<Map<String, Object>>(3);
        SimpleDate lastMonth = DateTimeUtils.getFirstOfLastMonth();
        // LAST MONTH
        BigDecimal revenue = BigDecimal.ZERO;
        List<Order> poOfLastMonths = orderRepository.getOrderByCustomers(clientId, Arrays.asList(customer.getId()),
                DateTimeUtils.getPeriodLastMonth(), null, null, null);
        if (poOfLastMonths != null && !poOfLastMonths.isEmpty()) {
            BigDecimal output = BigDecimal.ZERO;
            for (Order order : poOfLastMonths) {
                if (order.getDetails() != null && !order.getDetails().isEmpty()) {
                    for (OrderDetail detail : order.getDetails()) {
                        output = output.add(detail.getOutput());
                    }
                }
                if (order.getGrandTotal() != null) {
                    revenue = revenue.add(order.getGrandTotal());
                }
            }

            dto.setOutputLastMonth(output);
        } else {
            dto.setOutputLastMonth(BigDecimal.ZERO);
        }
        Map<String, Object> revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", lastMonth.format("MM/yyyy"));
        revenueDto.put("revenue", revenue);
        revenueLastThreeMonth.add(revenueDto);

        // 2 MONTH AGO
        revenue = BigDecimal.ZERO;
        List<Order> poOf2MonthAgo = orderRepository.getOrderByCustomers(clientId, Arrays.asList(customer.getId()),
                DateTimeUtils.getPeriodLastsMonth(2), null, null, null);
        if (poOf2MonthAgo != null && !poOf2MonthAgo.isEmpty()) {
            for (Order order : poOf2MonthAgo) {
                if (order.getGrandTotal() != null) {
                    revenue = revenue.add(order.getGrandTotal());
                }
            }

        }
        revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", DateTimeUtils.addMonths(lastMonth, -1).format("MM/yyyy"));
        revenueDto.put("revenue", revenue);
        revenueLastThreeMonth.add(revenueDto);

        // 3 MONTH AGO
        revenue = BigDecimal.ZERO;
        List<Order> poOf3MonthAgo = orderRepository.getOrderByCustomers(clientId, Arrays.asList(customer.getId()),
                DateTimeUtils.getPeriodLastsMonth(3), null, null, null);
        if (poOf3MonthAgo != null && !poOf3MonthAgo.isEmpty()) {
            for (Order order : poOf3MonthAgo) {
                if (order.getGrandTotal() != null) {
                    revenue = revenue.add(order.getGrandTotal());
                }
            }

        }
        revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", DateTimeUtils.addMonths(lastMonth, -2).format("MM/yyyy"));
        revenueDto.put("revenue", revenue);
        revenueLastThreeMonth.add(revenueDto);

        dto.setRevenueLastThreeMonth(revenueLastThreeMonth);

        //XXX FROM 3 MONTH AGO ONLY
        SimpleDate today = DateTimeUtils.getToday();
        Period period = new Period(DateTimeUtils.addMonths(today, -3), DateTimeUtils.addDays(today, 1));
        
        List<Order> lastFiveOrders = orderRepository.getLastOrderByCustomer(clientId, customer.getId(), 5, period);

        if (lastFiveOrders == null || lastFiveOrders.isEmpty()) {
            dto.setLastFiveOrders(Collections.<Map<String, Object>> emptyList());
        } else {
            List<Map<String, Object>> lastFiveOrderDtos = new ArrayList<Map<String, Object>>(5);
            for (Order order : lastFiveOrders) {
                Map<String, Object> orderDto = new HashMap<String, Object>();
                orderDto.put("date", order.getCreatedTime().getIsoTime());
                orderDto.put("skuNumber", order.getDetails() == null ? 0 : order.getDetails().size());
                orderDto.put("total", order.getGrandTotal());
                lastFiveOrderDtos.add(orderDto);
            }

            dto.setLastFiveOrders(lastFiveOrderDtos);
        }
        
        SalesConfig salesConfig = salesConfigRepository.getSalesConfig(clientId);
        dto.setCanEditLocation(salesConfig.isCanEditCustomerLocation());

        return dto;
    }
    
}
