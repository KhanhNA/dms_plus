package com.viettel.backend.service.sub;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.MultiKeyMap;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.cache.redis.CacheManager;
import com.viettel.backend.domain.ClientConfig;
import com.viettel.backend.domain.Customer;
import com.viettel.backend.domain.embed.CustomerScheduleItem;
import com.viettel.backend.entity.SimpleDate;
import com.viettel.backend.entity.SimpleDate.Period;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.repository.CalendarConfigRepository;
import com.viettel.backend.repository.CustomerRepository;
import com.viettel.backend.repository.RouteRepository;
import com.viettel.backend.repository.UserRepository;
import com.viettel.backend.repository.VisitRepository;
import com.viettel.backend.service.impl.AbstractService;
import com.viettel.backend.util.DateTimeUtils;

@Service
public class VisitSubService extends AbstractService implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    public MultiKeyMap<Integer, Integer> getNumberDayByDayAndWeek(ClientConfig clientConfig, Period period) {
        if (clientConfig == null || period == null) {
            throw new IllegalArgumentException("invalid params");
        }

        MultiKeyMap<Integer, Integer> numberVisitBySchedule = new MultiKeyMap<Integer, Integer>();
        SimpleDate tmp = period.getFromDate();
        while (tmp.compareTo(period.getToDate()) < 0) {
            int firstDayOfWeek = clientConfig.getFirstDayOfWeek();
            int minimalDaysInFirstWeek = clientConfig.getMinimalDaysInFirstWeek();

            // Tuan lam viec cua ngay hom nay tinh tu dau nam
            int weekOfToday = DateTimeUtils.getWeekOfYear(tmp, firstDayOfWeek, minimalDaysInFirstWeek);
            int weekDuration = clientConfig.getNumberWeekOfFrequency();
            // Thu tu cua tuan theo week duration
            int weekIndex = weekOfToday % weekDuration + 1;
            int dayOfTmp = tmp.getDayOfWeek();

            Integer numberVisit = numberVisitBySchedule.get(weekIndex, dayOfTmp);
            if (numberVisit == null) {
                numberVisit = 1;
            } else {
                numberVisit = numberVisit + 1;
            }

            numberVisitBySchedule.put(weekIndex, dayOfTmp, numberVisit);

            tmp = DateTimeUtils.addDays(tmp, 1);
        }
        return numberVisitBySchedule;
    }

    public int getNumberVisitPlanned(UserLogin userLogin, Period period, Collection<ObjectId> salesmanIds) {
        int total = 0;

        if (CollectionUtils.isEmpty(salesmanIds)) {
            return total;
        }

        ClientConfig clientConfig = getClientCondig(userLogin);
        MultiKeyMap<Integer, Integer> numberDayByDayAndWeek = getNumberDayByDayAndWeek(clientConfig, period);

        // FIRST TRY IN CACHE
        try {
            Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(userLogin.getClientId(), salesmanIds);
            if (CollectionUtils.isEmpty(routeIds)) {
                return total;
            }
            
            for (ObjectId routeId : routeIds) {
                Map<String, Integer> cache = cacheManager.getScheduleNumberPlannedCache().get(routeId.toString())
                        .entries();

                if (cache == null || cache.isEmpty()) {
                    continue;
                }

                for (Entry<MultiKey<? extends Integer>, Integer> entry : numberDayByDayAndWeek.entrySet()) {
                    int week = entry.getKey().getKey(0);
                    int day = entry.getKey().getKey(1);
                    int frequence = entry.getValue();

                    if (frequence <= 0) {
                        continue;
                    }

                    Integer numberCustomerToVisit = cache.get(week + "-" + day);

                    if (numberCustomerToVisit == null || numberCustomerToVisit <= 0) {
                        continue;
                    }

                    total = total + (frequence * numberCustomerToVisit);
                }
            }

            return total;
        } catch (Exception e) {
            logger.error("cache error", e);
        }

        // CACHE FAILED TRY WITH DATABASE
        Set<ObjectId> routeIds = routeRepository.getRouteIdsBySalesmen(userLogin.getClientId(), salesmanIds);
        List<Customer> customers = customerRepository.getCustomersByRoute(userLogin.getClientId(), routeIds,
                null, null, null, null);

        for (Customer customer : customers) {
            if (customer.getSchedule() == null) {
                continue;
            }
            List<CustomerScheduleItem> items = customer.getSchedule().getItems();
            if (CollectionUtils.isEmpty(items)) {
                continue;
            }
            for (CustomerScheduleItem item : items) {
                List<Integer> weeks = item.getWeeks();
                List<Integer> days = item.getDays();

                if (weeks == null || weeks.isEmpty() || days == null || days.isEmpty()) {
                    continue;
                }

                for (int week : weeks) {
                    for (int day : days) {
                        Integer tmpNumberVisitPlan = numberDayByDayAndWeek.get(week, day);
                        tmpNumberVisitPlan = tmpNumberVisitPlan == null ? 0 : tmpNumberVisitPlan;

                        total += tmpNumberVisitPlan;
                    }
                }
            }
        }

        return total;
    }
}
