package com.viettel.backend.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CollectionUtils {
	
	@SuppressWarnings("unchecked")
	public static <T> Set<T> createSetFromArrayIgnoreEmpty(T...values) {
		Set<T> retValue = new HashSet<T>();
		
		if (values != null && values.length > 0) {
			for (T value : values) {
				if (value == null) {
					continue;
				} else if (value instanceof String && "".equals(value)) {
					continue;
				}
				
				retValue.add(value);
			}
		}
		
		return retValue;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> createListFromArrayIgnoreEmpty(T...values) {
		List<T> retValue = new ArrayList<T>();
		
		if (values != null && values.length > 0) {
			for (T value : values) {
				if (value == null) {
					continue;
				} else if (value instanceof String && "".equals(value)) {
					continue;
				}
				
				retValue.add(value);
			}
		}
		
		return retValue;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> createListFromArray(T...values) {
		List<T> retValue = new ArrayList<T>();
		
		if (values != null && values.length > 0) {
			for (T value : values) {
				retValue.add(value);
			}
		}
		
		return retValue;
	}
}
