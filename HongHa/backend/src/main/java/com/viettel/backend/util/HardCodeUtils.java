package com.viettel.backend.util;

public class HardCodeUtils {

    public static final String ROLE_SUPERADMIN = "SUPER";
    public static final String ROLE_ADMIN = "AD";
    public static final String ROLE_SUPERVISOR = "SUP";
    public static final String ROLE_SALESMAN = "SM";
    public static final String ROLE_DISTRIBUTOR = "DIS";
    public static final String ROLE_STORE_CHECKER = "SC";
    
}
