package com.viettel.backend.util;

import java.math.BigDecimal;

public class NumberUtils {
	
	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	
	public static final BigDecimal TWELVE = new BigDecimal(12);
	
	public static final int PERCENTAGE_SCALE = 2;
	
	public static final int PERCENTAGE_ROUND_TYPE = BigDecimal.ROUND_HALF_UP;
	
	public static BigDecimal percentage(BigDecimal value) {
		if (value == null || value.signum() <= 0)
			return BigDecimal.ZERO;
		
		return value.divide(ONE_HUNDRED, PERCENTAGE_SCALE, PERCENTAGE_ROUND_TYPE);
	}
	
	public static final BigDecimal minValue(BigDecimal value, BigDecimal minValue) {
		if (value == null || value.compareTo(minValue) < 0) {
			return minValue;
		}
		
		return value;
	}
	
	public static final BigDecimal maxValue(BigDecimal value, BigDecimal maxValue) {
		if (value == null || value.compareTo(maxValue) > 0) {
			return maxValue;
		}
		
		return value;
	}
	
	public static final BigDecimal positiveValue(BigDecimal value) {
		return minValue(value, BigDecimal.ZERO);
	}
	
}
