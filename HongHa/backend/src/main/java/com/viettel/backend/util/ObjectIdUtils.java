package com.viettel.backend.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;

import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.restful.ExceptionCode;

public class ObjectIdUtils {

    public static ObjectId getObjectId(String id) {
        if (ObjectId.isValid(id)) {
            return new ObjectId(id);
        } else {
            throw new BusinessException(ExceptionCode.INVALID_PARAM);
        }
    }
    
    public static ObjectId getObjectId(String id, ObjectId defaultValue) {
        if (ObjectId.isValid(id)) {
            return new ObjectId(id);
        }
        
        return defaultValue;
    }
    
    public static ObjectId[] getObjectIds(String... idTxts) {
        if (idTxts == null) {
            return null;
        }

        ObjectId[] ids = new ObjectId[idTxts.length];
        for (int i = 0; i < idTxts.length; i++) {
            ids[i] = ObjectIdUtils.getObjectId(idTxts[i]);
        }
        
        return ids;
    }
    
    public static List<ObjectId> getObjectIds(Collection<String> idTxts) {
        if (idTxts == null) {
            return null;
        }

        List<ObjectId> ids = new ArrayList<ObjectId>(idTxts.size());
        for (String idTxt : idTxts) {
            ids.add(ObjectIdUtils.getObjectId(idTxt));
        }
        
        return ids;
    }

    public static List<ObjectId> getObjectIdsIgnoreNull(Collection<String> idTxts) {
        if (idTxts == null) {
            return null;
        }

        List<ObjectId> ids = new ArrayList<ObjectId>();
        for (String idTxt : idTxts) {
        	ObjectId id = ObjectIdUtils.getObjectId(idTxt, null);
        	if (id != null) {
        		ids.add(id);
        	}
        }
        
        return ids;
    }
    
    public static List<ObjectId> getObjectIdsIgnoreNull(String... idTxts) {
        if (idTxts == null || idTxts.length == 0) {
            return null;
        }

        List<ObjectId> ids = new ArrayList<ObjectId>();
        for (String idTxt : idTxts) {
        	ObjectId id = ObjectIdUtils.getObjectId(idTxt, null);
        	if (id != null) {
        		ids.add(id);
        	}
        }
        
        return ids;
    }
    
    public static boolean checkChange(String id, ObjectId objectId) {
        if (objectId == null && id == null) {
            return false;
        } else if (objectId == null || id == null) {
            return true;
        }

        return !objectId.toString().equals(id);
    }

    public static boolean checkNullAndChange(String id, ObjectId objectId) {
        if (objectId == null || id == null) {
            return true;
        }

        return !objectId.toString().equals(id);
    }

    public static int compare(ObjectId o1, ObjectId o2) {
    	if (o1 == null && o2 == null) {
    		return 0;
    	}
    	
    	if (o1 == null) {
    		return -1;
    	}
    	
    	if (o2 == null) {
    		return 1;
    	}
    	
    	return o1.compareTo(o2);
    }
}
