package com.viettel.backend.websocket.core;

import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.viettel.backend.dto.OutputMessage;

public class WebsocketUtils {
	
    public static void send(SimpMessagingTemplate template, String destination, String sender, Object data) {
        OutputMessage message = new OutputMessage();
        message.setSender(sender);
        message.setData(data);
       
        template.convertAndSend(destination, message);
    }
    
    public static void sendToUser(SimpMessagingTemplate template, String userId, String destination, String sender, Object data) {
        OutputMessage message = new OutputMessage();
        message.setSender(sender);
        message.setData(data);
       
        template.convertAndSendToUser(userId, destination, message);
    }
    
}
