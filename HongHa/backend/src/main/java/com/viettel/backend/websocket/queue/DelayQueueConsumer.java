package com.viettel.backend.websocket.queue;

import java.util.concurrent.BlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DelayQueueConsumer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private BlockingQueue<DelayObject> queue;

    public DelayQueueConsumer(BlockingQueue<DelayObject> queue) {
        super();
        this.queue = queue;
    }

    private Thread consumerThread = new Thread(new Runnable() {
        @Override
        public void run() {

            while (true) {
                try {
                    // Take elements out from the DelayQueue object.
                    DelayObject object = queue.take();
                    if (object != null) {
                        object.excute();
                    }

                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                } catch (Exception e) {
                    logger.error("Cannot execute task", e);
                }
            }
        }
    });

    public void start() {
        this.consumerThread.start();
    }

    public void destroy() throws Exception {
        consumerThread.interrupt();
    }
}
