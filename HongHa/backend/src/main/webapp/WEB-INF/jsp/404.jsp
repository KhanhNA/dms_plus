<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="header.jsp"></jsp:include>

<div class="page-err">
    <div class="err-container">
        <div class="text-center">
            <div class="err-status">
                 <h1>404</h1>
            </div>
            <div class="err-message">
                <h2><spring:message code="error.not.found" /></h2>
            </div>
            <div class="err-body">
                <a href="<c:url value="/" />" class="btn btn-lg btn-goback">
                    <span class="glyphicon glyphicon-home"></span>
                    <span class="space"></span>
                    <spring:message code="error.go.back.to.home" />
                </a>
            </div>
        </div>
    </div>
    <div class="footer text-center">
        <spring:message code="app.copyright" />
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>