<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!doctype html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><spring:message code="app.title" /></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        
        <!-- needs images, font... therefore can not be part of ui.css -->
        <link rel="stylesheet" href='<c:url value="/assets/font-icon/font-awesome/css/font-awesome.min.css" />'>
        <link rel="stylesheet" href='<c:url value="/assets/font-icon/weather-icons/css/weather-icons.min.css" />'>
        <!-- end needs images -->

        <link rel="stylesheet" href='<c:url value="/assets/styles/main.css" />'>
		<link rel="stylesheet" href='<c:url value="/assets/styles/login.css" />'>
    </head>
    <body id="app" >
        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="body-special">
			<div class="view-container">
				<section id="content" class="animate-fade-up">