<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="header.jsp"></jsp:include>

<div class="page-signin">
    <div class="main-body">
        <div class="form-container">
        	<div class="overlay shadow"></div>
            <form class="form-horizontal" action="<c:url value="/account/login"/>" method="post">
                <fieldset>
                    <div class="form-group text-center">
                        <img src="<c:url value='/assets/images/logo.png' />" alt="logo" class="logo" />
                    </div>
                    <c:if test="${not empty param.authentication_error}">
						<div class="form-login-error" style="display: block !important;">
							<h3><spring:message code="login.message.woops" text="Woops!" /></h3>
							<p><spring:message code="login.message.not.success" text="Your login attempt was not successful." /></p>
						</div>
					</c:if>
					<c:if test="${not empty param.authorization_error}">
						<div class="form-login-error" style="display: block !important;">
							<h3><spring:message code="login.message.woops" text="Woops!" /></h3>
							<p><spring:message code="login.message.not.permitted" text="You are not permitted to access that resource." /></p>
						</div>
					</c:if>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-user"></span>
                            </span>
                            <input type="text" class="form-control" placeholder="<spring:message code="login.hint.account" text="account" />" name="j_username" >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-lock"></span>
                            </span>
                            <input type="password" class="form-control" placeholder="<spring:message code="login.hint.password" text="password" />" name="j_password">
                        </div>
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
						<input type="submit" value="<spring:message code="login.button" text="Login" />" class="btn btn-lg btn-block btn-login" />
					</div>
                </fieldset>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
        </div>
        <c:set var="lang">${pageContext.response.locale.language}</c:set>
        <div class="language-container text-center">
        	<a href="?lang=vi">
        		<img src="<c:url value='/assets/images/language_vietnamese_inactive.png' />" 
        				alt="Tiếng Việt" class="flag <c:if test="${lang == 'vi'}">active</c:if>" />
        	</a>
            <a href="?lang=en">
            	<img src="<c:url value='/assets/images/language_english_inactive.png' />" 
            			alt="English" class="flag <c:if test="${lang == 'en'}">active</c:if>" />
            </a>
        </div>
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>