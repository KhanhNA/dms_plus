package com.viettel.backend.config.root;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;

import com.viettel.backend.config.security.TotpProperties;
import com.viettel.backend.oauth2.core.DefaultSignedRequestConverter;
import com.viettel.backend.oauth2.core.SignedRequestConverter;
import com.viettel.backend.service.core.sub.WebNotificationService;
import com.viettel.backend.service.core.sub.impl.WebNotificationServiceImpl;
import com.viettel.persistence.mongo.config.FTPProperties;
import com.viettel.persistence.mongo.config.SharedFolderProperties;

import reactor.Environment;
import reactor.bus.EventBus;
import reactor.spring.context.config.EnableReactor;

@Configuration
@ComponentScan(basePackages = { 
        "com.viettel.backend.config.root", 
        "com.viettel.backend.repository",
        "com.viettel.backend.service",
        "com.viettel.persistence.mongo.repository",
        })
@EnableAspectJAutoProxy(proxyTargetClass = true)
@EnableScheduling
@EnableReactor
@EnableConfigurationProperties({ FrontendProperties.class, AppProperties.class, FTPProperties.class,
        SharedFolderProperties.class, TotpProperties.class })
public class ApplicationConfig {
    
    @Value("${oauth2.signed-request-verifier-key}")
    private String signedRequestVerifierKey;
    
    @Bean
    public MessageSource messageSource() {
        final ReloadableResourceBundleMessageSource ret = new ReloadableResourceBundleMessageSource();
        ret.setBasename("classpath:language");
        ret.setDefaultEncoding("UTF-8");
        return ret;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder();
    }
    
    @Bean
    public SignedRequestConverter signedRequestConverter() {
        DefaultSignedRequestConverter signedRequestConverter = new DefaultSignedRequestConverter();
        if (!StringUtils.isEmpty(signedRequestVerifierKey)) {
            signedRequestConverter.setSigningKey(signedRequestVerifierKey);
        }
        return signedRequestConverter;
    }

//    @Bean
//    @SuppressWarnings("rawtypes")
//    public PromotionEngine promotionEngine() {
//        return new PromotionEngine<ObjectId, OrderProduct, Promotion, PromotionDetail, OrderPromotion, 
//                OrderPromotionDetail, OrderPromotionReward>();
//    }
    
    @Bean
    public EventBus createEventBus(Environment env) {
        return EventBus.create(env, Environment.THREAD_POOL);
    }
    
    @Bean
    @Lazy
    public WebNotificationService webNotificationEngine() {
        return new WebNotificationServiceImpl();
    }

}
