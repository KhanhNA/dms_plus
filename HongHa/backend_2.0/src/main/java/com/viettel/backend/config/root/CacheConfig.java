package com.viettel.backend.config.root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import com.viettel.repository.common.cache.RedisCache;

@Configuration
public class CacheConfig {

    @Autowired
	private RedisConnectionFactory redisConnectionFactory;

    @Bean
    public RedisCache redisCache() {
        return new RedisCache(redisConnectionFactory);
    }
    
//    @Bean
//    public CacheRepository cacheRepository() {
//        return new CacheRepository(cacheManager());
//    }
    
}
