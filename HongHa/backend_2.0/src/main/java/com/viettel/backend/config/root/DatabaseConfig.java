package com.viettel.backend.config.root;

import org.springframework.context.annotation.Configuration;

@Configuration
//@EnableConfigurationProperties(MongoClientProperties.class)
public class DatabaseConfig {
    
//    @Autowired
//    private MongoProperties mongoProperties;
//    
//    @Autowired
//    private MongoClientProperties mongoClientProperties;
//    
//    private String domainPackageName = ClassUtils.getPackageName(PO.class);
//    
//    @Bean
//    public MongoClientOptions mongoClientOptions() {
//        MongoClientOptions.Builder builder = MongoClientOptions.builder();
//        builder.connectionsPerHost(mongoClientProperties.getConnectionPerHost());
//        builder.threadsAllowedToBlockForConnectionMultiplier(mongoClientProperties.getThreadsAllowedToBlockMultiplier());
//        return builder.build();
//    }
//    
//    @Bean
//    public CustomConversions customConversions() {
//        return new CustomConversions(Collections.emptyList());
//    }
//    
//    @Bean
//    public MongoMappingContext mongoMappingContext(BeanFactory beanFactory)
//            throws ClassNotFoundException {
//        MongoMappingContext context = new MongoMappingContext();
//        context.setInitialEntitySet(getInitialEntitySet());
//        Class<?> strategyClass = this.mongoProperties.getFieldNamingStrategy();
//        if (strategyClass != null) {
//            context.setFieldNamingStrategy(
//                    (FieldNamingStrategy) BeanUtils.instantiate(strategyClass));
//        }
//        return context;
//    }
//    
//    /**
//     * Scans the mapping base package for classes annotated with {@link Document}.
//     * 
//     * @see #getMappingBasePackage()
//     * @return
//     * @throws ClassNotFoundException
//     */
//    protected Set<Class<?>> getInitialEntitySet() throws ClassNotFoundException {
//
//        Set<Class<?>> initialEntitySet = new HashSet<Class<?>>();
//
//        if (StringUtils.hasText(domainPackageName)) {
//            ClassPathScanningCandidateComponentProvider componentProvider = new ClassPathScanningCandidateComponentProvider(
//                    false);
//            componentProvider.addIncludeFilter(new AnnotationTypeFilter(Document.class));
//            componentProvider.addIncludeFilter(new AnnotationTypeFilter(Persistent.class));
//
//            for (BeanDefinition candidate : componentProvider.findCandidateComponents(domainPackageName)) {
//                initialEntitySet.add(ClassUtils.forName(candidate.getBeanClassName(),
//                        AbstractMongoConfiguration.class.getClassLoader()));
//            }
//        }
//
//        return initialEntitySet;
//    }

}
