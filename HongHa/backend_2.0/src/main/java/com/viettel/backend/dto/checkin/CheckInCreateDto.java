package com.viettel.backend.dto.checkin;

import java.io.Serializable;
import java.util.List;

import com.viettel.repository.common.entity.Location;

public class CheckInCreateDto implements Serializable {

    private static final long serialVersionUID = 1916615299734663004L;
    
    private String note;
    private List<String> photos;
    private Location location;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
