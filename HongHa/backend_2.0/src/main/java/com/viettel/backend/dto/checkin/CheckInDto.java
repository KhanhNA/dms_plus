package com.viettel.backend.dto.checkin;

import java.util.List;

import com.viettel.repository.common.domain.checkin.I_CheckIn;

public class CheckInDto extends CheckInListDto {

    private static final long serialVersionUID = 1498434975490164891L;
    
    private List<String> photos;
    
    public CheckInDto(I_CheckIn checkIn) {
        super(checkIn);
        
        this.photos = checkIn.getPhotos();
    }

    public List<String> getPhotos() {
        return photos;
    }

}
