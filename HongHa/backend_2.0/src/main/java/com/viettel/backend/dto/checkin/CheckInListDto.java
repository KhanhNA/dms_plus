package com.viettel.backend.dto.checkin;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.checkin.I_CheckIn;
import com.viettel.repository.common.entity.Location;

public class CheckInListDto extends DTOSimple {

    private static final long serialVersionUID = 8598434977768543771L;
    
    private UserSimpleDto createdBy;
    private String createdTime;
    
    private String note;
    private int nbPhoto;
    
    private Location location;
    private CategorySimpleDto province;
    
    public CheckInListDto(I_CheckIn checkIn) {
        super(checkIn);
        
        this.createdBy = new UserSimpleDto(checkIn.getCreatedBy());
        this.createdTime = checkIn.getCreatedTime().getIsoTime();
        
        this.note = checkIn.getNote();
        this.nbPhoto = checkIn.getNbPhotos();
        
        this.location = checkIn.getLocation();
        if (checkIn.getProvince() != null) {
            this.province = new CategorySimpleDto(checkIn.getProvince());
        }
    }

    public UserSimpleDto getCreatedBy() {
        return createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public String getNote() {
        return note;
    }
    
    public int getNbPhoto() {
        return nbPhoto;
    }
    
    public Location getLocation() {
        return location;
    }
    
    public CategorySimpleDto getProvince() {
        return province;
    }
    
}
