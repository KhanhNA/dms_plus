package com.viettel.backend.dto.common;

import com.viettel.repository.common.domain.common.I_Category;

public class CategorySelectionDto extends CategorySimpleDto {

    private static final long serialVersionUID = -6669912160394304510L;
    
    private boolean selected;

    public CategorySelectionDto(I_Category category, boolean selected) {
        super(category);
        this.selected = selected;
    }
    
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
