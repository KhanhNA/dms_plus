package com.viettel.backend.dto.common;

import java.io.Serializable;

import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.domain.common.I_POEmbed;

public abstract class DTOSimple implements Serializable {

    private static final long serialVersionUID = -862151765440311950L;

    private String id;
    
    public DTOSimple(String id) {
        super();
        this.id = id;
    }

    public DTOSimple(I_PO po) {
        super();

        if (po != null && po.getId() != null) {
            this.id = po.getId().toString();
        }
    }

    public DTOSimple(I_POEmbed po) {
        super();

        if (po != null && po.getId() != null) {
            this.id = po.getId().toString();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
