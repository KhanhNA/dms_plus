package com.viettel.backend.dto.common;

import java.io.InputStream;

public class ExportDto {

    private String fileName;
    private InputStream inputStream;
    
    public ExportDto(String fileName, InputStream inputStream) {
        super();
        
        this.fileName = fileName;
        this.inputStream = inputStream;
    }
    
    public String getFileName() {
        return fileName;
    }
    
    public InputStream getInputStream() {
        return inputStream;
    }

}
