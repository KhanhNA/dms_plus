package com.viettel.backend.dto.common;

import java.math.BigDecimal;

public interface I_ProductPriceFactory {

    public BigDecimal getPrice(String productId);

}
