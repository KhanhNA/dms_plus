package com.viettel.backend.dto.common;

import java.io.Serializable;

import org.springframework.util.Assert;

import com.viettel.repository.common.domain.common.I_PO;

public class IdDto implements Serializable {

    private static final long serialVersionUID = -602708292436783698L;

    private String id;

    public IdDto(String id) {
        this.id = id;
    }
    
    public IdDto(I_PO po) {
        Assert.notNull(po);
        Assert.notNull(po.getId());
        
        this.id = po.getId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
