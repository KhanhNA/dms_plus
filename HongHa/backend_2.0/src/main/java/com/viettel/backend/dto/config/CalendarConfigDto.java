package com.viettel.backend.dto.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.viettel.repository.common.domain.category.I_CalendarConfig;
import com.viettel.repository.common.entity.SimpleDate;

public class CalendarConfigDto implements Serializable {

    private static final long serialVersionUID = 1786366439733982764L;

    private List<Integer> workingDays;
    private List<String> holidays;
    
    public CalendarConfigDto() {
        super();
    }
    
    public CalendarConfigDto(I_CalendarConfig calendarConfig) {
        this();
        
        this.workingDays = calendarConfig.getWorkingDays();
        if (calendarConfig.getHolidays() != null) {
            this.holidays = new ArrayList<String>(calendarConfig.getHolidays().size());
            for (SimpleDate holiday : calendarConfig.getHolidays()) {
                this.holidays.add(holiday.getIsoDate());
            }
        }
    }

    public List<Integer> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<Integer> workingDays) {
        this.workingDays = workingDays;
    }

    public List<String> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<String> holidays) {
        this.holidays = holidays;
    }


}
