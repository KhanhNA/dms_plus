package com.viettel.backend.dto.config;

import java.io.Serializable;
import java.util.Set;

import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.entity.Location;

public class ClientConfigDto implements Serializable {

	private static final long serialVersionUID = 4618363876249230856L;

	private long visitDurationKPI;
	private double visitDistanceKPI;
	private boolean canEditCustomerLocation;
	private Location location;
	private Set<String> modules;
	private int numberDayInventoryExpire;
	private String productivityUnit;
	private int limitAccount;

	public ClientConfigDto() {
		super();
	}

	public ClientConfigDto(I_Config config) {
		this();

		this.visitDurationKPI = config.getVisitDurationKPI();
		this.visitDistanceKPI = config.getVisitDistanceKPI();
		this.canEditCustomerLocation = config.isCanEditCustomerLocation();
		this.location = config.getLocation();
		this.modules = config.getModules();
		this.numberDayInventoryExpire = config.getNumberDayInventoryExpire();
		this.productivityUnit = config.getProductivityUnit();
		this.limitAccount = config.getLimitAccount();
	}

	public long getVisitDurationKPI() {
		return visitDurationKPI;
	}

	public void setVisitDurationKPI(long visitDurationKPI) {
		this.visitDurationKPI = visitDurationKPI;
	}

	public double getVisitDistanceKPI() {
		return visitDistanceKPI;
	}

	public void setVisitDistanceKPI(double visitDistanceKPI) {
		this.visitDistanceKPI = visitDistanceKPI;
	}

	public boolean isCanEditCustomerLocation() {
		return canEditCustomerLocation;
	}

	public void setCanEditCustomerLocation(boolean canEditCustomerLocation) {
		this.canEditCustomerLocation = canEditCustomerLocation;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Set<String> getModules() {
		return modules;
	}

	public void setModules(Set<String> modules) {
		this.modules = modules;
	}

	public int getNumberDayInventoryExpire() {
		return numberDayInventoryExpire;
	}

	public void setNumberDayInventoryExpire(int numberDayInventoryExpire) {
		this.numberDayInventoryExpire = numberDayInventoryExpire;
	}

	public String getProductivityUnit() {
		return productivityUnit;
	}

	public void setProductivityUnit(String productivityUnit) {
		this.productivityUnit = productivityUnit;
	}

	public int getLimitAccount() {
		return limitAccount;
	}

	public void setLimitAccount(int limitAccount) {
		this.limitAccount = limitAccount;
	}

}
