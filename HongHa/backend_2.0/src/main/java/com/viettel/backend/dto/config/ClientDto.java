package com.viettel.backend.dto.config;

import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.repository.common.domain.category.I_CalendarConfig;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.common.I_Category;

public class ClientDto extends CategoryDto {

    private static final long serialVersionUID = -7626033792562012411L;

    private ClientConfigDto clientConfig;
    private CalendarConfigDto calendarConfig;

    public ClientDto(I_Category client, I_Config config, I_CalendarConfig calendarConfig) {
        super(client);

        this.clientConfig = new ClientConfigDto(config);
        this.calendarConfig = new CalendarConfigDto(calendarConfig);
    }

    public ClientConfigDto getClientConfig() {
        return clientConfig;
    }

    public void setClientConfig(ClientConfigDto clientConfig) {
        this.clientConfig = clientConfig;
    }

    public CalendarConfigDto getCalendarConfig() {
        return calendarConfig;
    }

    public void setCalendarConfig(CalendarConfigDto calendarConfig) {
        this.calendarConfig = calendarConfig;
    }

}
