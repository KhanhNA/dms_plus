package com.viettel.backend.dto.customer;

import java.util.Collections;
import java.util.List;

import com.viettel.repository.common.domain.customer.I_Customer;

public class CustomerDto extends CustomerListDto {

    private static final long serialVersionUID = 6617041574138019886L;

    private String email;
    private List<String> photos;
    
    public CustomerDto(I_Customer customer) {
        super(customer);

        this.email = customer.getEmail();
        //TODO remove if dont use
        this.photos = Collections.emptyList();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

}
