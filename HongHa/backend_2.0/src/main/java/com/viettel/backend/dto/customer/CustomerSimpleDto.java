package com.viettel.backend.dto.customer;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.repository.common.domain.customer.I_CustomerEmbed;

public class CustomerSimpleDto extends CategorySimpleDto {

    private static final long serialVersionUID = 1652073192406165389L;

    private CategorySimpleDto area;
    private CategorySimpleDto customerType;

    public CustomerSimpleDto(I_CustomerEmbed customer) {
        super(customer);

        if (customer.getArea() != null) {
            this.area = new CategorySimpleDto(customer.getArea());
        }

        if (customer.getCustomerType() != null) {
            this.customerType = new CategorySimpleDto(customer.getCustomerType());
        }
    }
    
    public CategorySimpleDto getArea() {
        return area;
    }

    public void setArea(CategorySimpleDto area) {
        this.area = area;
    }

    public CategorySimpleDto getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CategorySimpleDto customerType) {
        this.customerType = customerType;
    }

}
