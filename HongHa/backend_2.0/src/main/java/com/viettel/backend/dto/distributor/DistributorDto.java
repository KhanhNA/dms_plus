package com.viettel.backend.dto.distributor;

import java.util.Set;

import com.viettel.repository.common.domain.category.I_Distributor;

public class DistributorDto extends DistributorListDto {

    private static final long serialVersionUID = -4570514747523311583L;

    private Set<String> salesmanIds;

    public DistributorDto(I_Distributor distributor, Set<String> salesmanIds) {
        super(distributor);
        
        this.salesmanIds = salesmanIds;
    }

    public Set<String> getSalesmanIds() {
        return salesmanIds;
    }

    public void setSalesmanIds(Set<String> salesmanIds) {
        this.salesmanIds = salesmanIds;
    }

}
