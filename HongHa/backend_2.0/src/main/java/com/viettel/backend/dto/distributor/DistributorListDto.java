package com.viettel.backend.dto.distributor;

import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.category.I_Distributor;

public class DistributorListDto extends CategoryDto {

    private static final long serialVersionUID = -4570514747523311583L;

    private UserSimpleDto supervisor;
    
    public DistributorListDto(I_Distributor distributor) {
        super(distributor);
        
        if (distributor.getSupervisor() != null) {
            this.supervisor = new UserSimpleDto(distributor.getSupervisor());
        }
    }

    public UserSimpleDto getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(UserSimpleDto supervisor) {
        this.supervisor = supervisor;
    }
    
}
