package com.viettel.backend.dto.exchangereturn;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.product.ProductEmbedDto;
import com.viettel.repository.common.domain.data.I_ExchangeReturn;
import com.viettel.repository.common.domain.product.I_ProductQuantity;

public class ExchangeReturnDto extends ExchangeReturnSimpleDto {

    private static final long serialVersionUID = 8895086798968970698L;
    
    private List<ExchangeReturnDetailDto> details;

    public ExchangeReturnDto(I_ExchangeReturn exchangeReturn, I_ProductPhotoFactory productPhotoFactory) {
        super(exchangeReturn);

        if (exchangeReturn.getDetails() != null) {
            this.details = new ArrayList<ExchangeReturnDetailDto>(exchangeReturn.getDetails().size());
            for (I_ProductQuantity detail : exchangeReturn.getDetails()) {
                this.details.add(new ExchangeReturnDetailDto(detail, productPhotoFactory));
            }
        }

    }

    public List<ExchangeReturnDetailDto> getDetails() {
        return details;
    }

    public static class ExchangeReturnDetailDto implements Serializable {

        private static final long serialVersionUID = 8000317711310529954L;

        private ProductEmbedDto product;
        private BigDecimal quantity;
        
        public ExchangeReturnDetailDto(I_ProductQuantity productQuantity, I_ProductPhotoFactory productPhotoFactory) {
            this.product = new ProductEmbedDto(productQuantity, productPhotoFactory);
            this.quantity = productQuantity.getQuantity();
        }

        public ProductEmbedDto getProduct() {
            return product;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

    }

}
