package com.viettel.backend.dto.exchangereturn;

import java.math.BigDecimal;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.backend.dto.customer.CustomerSimpleDto;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.data.I_ExchangeReturnHeader;

public class ExchangeReturnSimpleDto extends DTOSimple {

    private static final long serialVersionUID = 8895086798968970698L;

    private CategorySimpleDto distributor;
    private CustomerSimpleDto customer;
    private UserSimpleDto createdBy;
    private String createdTime;
    private BigDecimal quantity;

    public ExchangeReturnSimpleDto(I_ExchangeReturnHeader exchangeReturn) {
        super(exchangeReturn);

        if (exchangeReturn.getDistributor() != null) {
            this.distributor = new CategorySimpleDto(exchangeReturn.getDistributor());
        }

        if (exchangeReturn.getCustomer() != null) {
            this.customer = new CustomerSimpleDto(exchangeReturn.getCustomer());
        }

        if (exchangeReturn.getCreatedBy() != null) {
            this.createdBy = new UserSimpleDto(exchangeReturn.getCreatedBy());
        }
        
        this.createdTime = exchangeReturn.getCreatedTime().getIsoTime();
        
        this.quantity = exchangeReturn.getQuantity();
    }

    public CategorySimpleDto getDistributor() {
        return distributor;
    }

    public CustomerSimpleDto getCustomer() {
        return customer;
    }

    public UserSimpleDto getCreatedBy() {
        return createdBy;
    }
    
    public String getCreatedTime() {
        return createdTime;
    }
    
    public BigDecimal getQuantity() {
        return quantity;
    }

}
