package com.viettel.backend.dto.feedback;

import java.util.List;

import com.viettel.repository.common.domain.data.I_Feedback;

public class FeedbackDto extends FeedbackListDto {

    private static final long serialVersionUID = 8310676852814109872L;

    private List<String> feedbacks;

    public FeedbackDto(I_Feedback feedback) {
        super(feedback);
        
        this.feedbacks = feedback.getFeedbacks();
    }
    
    public List<String> getFeedbacks() {
        return feedbacks;
    }

}
