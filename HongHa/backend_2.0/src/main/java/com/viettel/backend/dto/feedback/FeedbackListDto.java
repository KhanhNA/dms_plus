package com.viettel.backend.dto.feedback;

import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.repository.common.domain.data.I_FeedbackHeader;

public class FeedbackListDto extends DTOSimple {

    private static final long serialVersionUID = 8310676852814109872L;

    private String salesman;
    private String customer;
    private String createdTime;
    private boolean feedbacksReaded;
    private String firstMessage;

    public FeedbackListDto(I_FeedbackHeader feedback) {
        super(feedback.getId().toString());
        
        this.salesman = feedback.getCreatedBy().getFullname();
        this.customer = feedback.getCustomer().getName();
        this.createdTime = feedback.getCreatedTime().getIsoTime();
        this.feedbacksReaded = feedback.isReaded();
        this.firstMessage = feedback.getFirstMessage();
    }
    
    public String getSalesman() {
        return salesman;
    }

    public String getCustomer() {
        return customer;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public boolean isFeedbacksReaded() {
        return feedbacksReaded;
    }

    public String getFirstMessage() {
        return firstMessage;
    }
}
