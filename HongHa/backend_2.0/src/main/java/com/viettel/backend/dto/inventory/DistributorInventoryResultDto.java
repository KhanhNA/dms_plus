package com.viettel.backend.dto.inventory;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.inventory.I_Inventory;

public class DistributorInventoryResultDto extends CategorySimpleDto {

    private static final long serialVersionUID = 3641902226456577012L;

    private InventoryListDto lastInventory;
    private InventoryResultDto inventoryResult;
    
    public DistributorInventoryResultDto(I_Category distributor, InventoryResultDto inventoryResult) {
        super(distributor);
        this.inventoryResult = inventoryResult;
    }
    
    public DistributorInventoryResultDto(I_Inventory inventory, InventoryResultDto inventoryResult) {
        super(inventory.getDistributor());
        this.lastInventory = new InventoryListDto(inventory);
        this.inventoryResult = inventoryResult;
    }
    
    public InventoryListDto getLastInventory() {
        return lastInventory;
    }

	public InventoryResultDto getInventoryResult() {
		return inventoryResult;
	}

}
