package com.viettel.backend.dto.inventory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class InventoryCreateDto {

	private String time;
	private List<InventoryCreateDetailDto> details;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<InventoryCreateDetailDto> getDetails() {
		return details;
	}

	public void setDetails(List<InventoryCreateDetailDto> details) {
		this.details = details;
	}

	public static class InventoryCreateDetailDto implements Serializable {

		private static final long serialVersionUID = 6236136783748119701L;

		private String productId;
		private BigDecimal quantity;

		public String getProductId() {
			return productId;
		}

		public void setProductId(String productId) {
			this.productId = productId;
		}

		public BigDecimal getQuantity() {
			return quantity;
		}

		public void setQuantity(BigDecimal quantity) {
			this.quantity = quantity;
		}

	}

}
