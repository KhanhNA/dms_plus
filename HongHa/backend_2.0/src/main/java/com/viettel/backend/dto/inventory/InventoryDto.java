package com.viettel.backend.dto.inventory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.product.ProductEmbedDto;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_ProductEmbed;
import com.viettel.repository.common.domain.product.I_ProductQuantity;

public class InventoryDto extends InventoryListDto {

    private static final long serialVersionUID = 4294936505380930621L;

    private List<InventoryDetailDto> details;

    public InventoryDto(I_Inventory inventory, I_ProductPhotoFactory productPhotoFactory) {
        super(inventory);

        if (inventory.getDetails() != null && !inventory.getDetails().isEmpty()) {
            List<InventoryDetailDto> details = new ArrayList<>(inventory.getDetails().size());
            for (I_ProductQuantity productQuantity : inventory.getDetails()) {
                InventoryDetailDto dto = new InventoryDetailDto(productQuantity, productPhotoFactory,
                        productQuantity.getQuantity());
                details.add(dto);
            }
            this.details = details;
        } else {
            this.details = Collections.emptyList();
        }
    }

    public List<InventoryDetailDto> getDetails() {
        return details;
    }

    public void setDetails(List<InventoryDetailDto> details) {
        this.details = details;
    }

    public static class InventoryDetailDto extends ProductEmbedDto {

        private static final long serialVersionUID = 7240727794465791875L;

        private BigDecimal quantity;

        public InventoryDetailDto(I_ProductEmbed product, I_ProductPhotoFactory productPhotoFactory,
                BigDecimal quantity) {
            super(product, productPhotoFactory);

            this.quantity = quantity;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

    }

}
