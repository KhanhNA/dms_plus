package com.viettel.backend.dto.inventory;

import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.repository.common.domain.inventory.I_Inventory;

public class InventoryImportResultDto extends ImportResultDto {

    private static final long serialVersionUID = -3603279541110844179L;
    
    private String id;
    
    public InventoryImportResultDto(I_Inventory inventory, int total, int success) {
        super(total, success);
        this.id = inventory.getId();
    }
    
    public String getId() {
        return id;
    }

}
