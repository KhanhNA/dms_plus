package com.viettel.backend.dto.inventory;

import com.viettel.backend.dto.common.DTO;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.common.I_DataOfDistributor;
import com.viettel.repository.common.domain.inventory.I_InventoryHeader;

public class InventoryListDto extends DTO implements I_DataOfDistributor {

    private static final long serialVersionUID = 4294936505380930621L;
    
    private String time;
    private I_CategoryEmbed  distributor;
    
    public InventoryListDto(I_InventoryHeader inventory) {
        super(inventory);
        this.setTime(inventory.getTime() != null ? inventory.getTime().getIsoTime() : null);
        this.setDistributor(inventory.getDistributor());
    }
    
    public InventoryListDto(I_CategoryEmbed distributor) {
        this.setTime(null);
        this.setDistributor(distributor);
    }

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public I_CategoryEmbed getDistributor() {
		return distributor;
	}
	
	public void setDistributor(I_CategoryEmbed distributor){
		this.distributor = new CategoryEmbed(distributor);
	}
}
