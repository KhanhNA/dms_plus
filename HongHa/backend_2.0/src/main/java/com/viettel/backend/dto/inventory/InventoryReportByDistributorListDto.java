package com.viettel.backend.dto.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.inventory.I_Inventory;

public class InventoryReportByDistributorListDto extends CategorySimpleDto {

	private static final long serialVersionUID = 3641902226456577012L;

	private List<InventoryListDto> lastInventories;
	private List<ProductInventoryResultDto> products;
	private boolean multipleDistributor;

	public InventoryReportByDistributorListDto(I_Category distributor, I_Inventory inventory,
			List<ProductInventoryResultDto> products) {
		super(distributor);
		if (inventory != null) {
			this.lastInventories = Collections.singletonList(new InventoryListDto(inventory));
			this.multipleDistributor = false;
		}
		this.products = products;
	}

	public InventoryReportByDistributorListDto(List<I_Distributor> distributors, Map<String, I_Inventory> inventories,
			List<ProductInventoryResultDto> products) {
		super("", "", "");
		this.lastInventories = new ArrayList<>();
		for (I_Category distributor : distributors) {
			if (inventories.containsKey(distributor.getId())) {
				this.lastInventories.add(new InventoryListDto(inventories.get(distributor.getId())));
			} else {
				this.lastInventories.add(new InventoryListDto(distributor));
			}
		}
		this.multipleDistributor = true;
		this.products = products;
	}


	public List<ProductInventoryResultDto> getProducts() {
		return products;
	}

	public List<InventoryListDto> getLastInventory() {
		return lastInventories;
	}

	public boolean isMultipleDistributor() {
		return multipleDistributor;
	}

	public void setMultipleDistributor(boolean multipleDistributor) {
		this.multipleDistributor = multipleDistributor;
	}

}
