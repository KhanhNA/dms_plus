package com.viettel.backend.dto.inventory;

import java.util.List;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.product.ProductSimpleDto;
import com.viettel.repository.common.domain.product.I_Product;

public class InventoryReportByProductListDto extends ProductSimpleDto {

    private static final long serialVersionUID = 3641902226456577012L;
    
    private List<DistributorInventoryResultDto> distributors;
    // For multiple-product-report
	private List<ProductInventoryResultDto> products;
	private List<InventoryListDto> lastInventories;
	private boolean multipleProduct;

    public InventoryReportByProductListDto(I_Product product, I_ProductPhotoFactory productPhotoFactory, List<DistributorInventoryResultDto> distributors) {
        super(product, productPhotoFactory, null);
        this.distributors = distributors;
        this.multipleProduct = false;
    }
    
    public InventoryReportByProductListDto(List<InventoryListDto> lastInventories, List<ProductInventoryResultDto> products, I_ProductPhotoFactory productPhotoFactory ) {
    	super(productPhotoFactory);
    	this.lastInventories = lastInventories;
        this.products = products;
        this.multipleProduct = true;
    }

    public List<DistributorInventoryResultDto> getDistributors() {
        return distributors;
    }

    public void setDistributors(List<DistributorInventoryResultDto> distributors) {
        this.distributors = distributors;
    }

	public List<ProductInventoryResultDto> getProducts() {
		return products;
	}

	public void setProducts(List<ProductInventoryResultDto> products) {
		this.products = products;
	}

	public boolean isMultipleProduct() {
		return multipleProduct;
	}

	public void setMultipleProduct(boolean multipleProduct) {
		this.multipleProduct = multipleProduct;
	}

	public List<InventoryListDto> getLastInventories() {
		return lastInventories;
	}

	public void setLastInventories(List<InventoryListDto> lastInventories) {
		this.lastInventories = lastInventories;
	}
	

}
