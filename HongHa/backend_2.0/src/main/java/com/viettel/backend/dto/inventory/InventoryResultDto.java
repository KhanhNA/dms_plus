package com.viettel.backend.dto.inventory;

import java.io.Serializable;
import java.math.BigDecimal;

import org.springframework.util.Assert;

public class InventoryResultDto implements Serializable {

    private static final long serialVersionUID = 2868471630896589265L;

    private BigDecimal inventoryQuantity;
    private BigDecimal availableQuantity;
    private BigDecimal soldQuantity;
    private BigDecimal amount;

    public InventoryResultDto(BigDecimal inventoryQuantity, BigDecimal soldQuantity, BigDecimal price) {
        this.inventoryQuantity = inventoryQuantity;
        this.soldQuantity = soldQuantity;
        if (inventoryQuantity != null && soldQuantity != null) {
            Assert.notNull(price);
            this.availableQuantity = inventoryQuantity.subtract(soldQuantity);
            this.amount = this.availableQuantity.multiply(price);
        }
    }

	public BigDecimal getInventoryQuantity() {
		return inventoryQuantity;
	}

	public BigDecimal getAvailableQuantity() {
		return availableQuantity;
	}

	public BigDecimal getSoldQuantity() {
		return soldQuantity;
	}
	
	public BigDecimal getAmount() {
        return amount;
    }

}
