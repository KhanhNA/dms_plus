package com.viettel.backend.dto.inventory;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.product.ProductEmbedDto;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductEmbed;

public class ProductInventoryResultDto extends ProductEmbedDto {

    private static final long serialVersionUID = 3641902226456577012L;

    private InventoryResultDto inventoryResult;
    
    public ProductInventoryResultDto(I_Product product, I_ProductPhotoFactory productPhotoFactory,
            InventoryResultDto inventoryResult) {
        super(product, productPhotoFactory);

        this.setInventoryResult(inventoryResult);
    }

    public ProductInventoryResultDto(I_ProductEmbed product, I_ProductPhotoFactory productPhotoFactory,
            InventoryResultDto inventoryResult) {
        super(product, productPhotoFactory);

        this.setInventoryResult(inventoryResult);
    }

	public InventoryResultDto getInventoryResult() {
		return inventoryResult;
	}

	public void setInventoryResult(InventoryResultDto inventoryResult) {
		this.inventoryResult = inventoryResult;
	}

}
