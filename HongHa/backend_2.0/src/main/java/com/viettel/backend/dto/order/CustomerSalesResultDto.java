package com.viettel.backend.dto.order;

import com.viettel.backend.dto.customer.CustomerSimpleDto;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_CustomerEmbed;

public class CustomerSalesResultDto extends CustomerSimpleDto {

    private static final long serialVersionUID = 3641902226456577012L;

    private SalesResultDto salesResult;

    public CustomerSalesResultDto(I_CustomerEmbed customer, SalesResultDto salesResult) {
        super(customer);

        this.salesResult = salesResult;
    }
    
    public CustomerSalesResultDto(I_Customer customer, SalesResultDto salesResult) {
        super(customer);

        this.salesResult = salesResult;
    }

    public SalesResultDto getSalesResult() {
        return salesResult;
    }

    public void setSalesResult(SalesResultDto salesResult) {
        this.salesResult = salesResult;
    }

}
