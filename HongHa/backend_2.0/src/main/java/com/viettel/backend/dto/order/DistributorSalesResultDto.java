package com.viettel.backend.dto.order;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;

public class DistributorSalesResultDto extends CategorySimpleDto {

    private static final long serialVersionUID = 3641902226456577012L;

    private SalesResultDto salesResult;
    
    public DistributorSalesResultDto(I_Category distributor, SalesResultDto salesResult) {
        super(distributor);

        this.salesResult = salesResult;
    }

    public DistributorSalesResultDto(I_CategoryEmbed distributor, SalesResultDto salesResult) {
        super(distributor);

        this.salesResult = salesResult;
    }

    public SalesResultDto getSalesResult() {
        return salesResult;
    }

    public void setSalesResult(SalesResultDto salesResult) {
        this.salesResult = salesResult;
    }

}
