package com.viettel.backend.dto.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderDetail;
import com.viettel.repository.common.domain.data.I_OrderPromotion;

public class OrderDto extends OrderSimpleDto implements Serializable {

    private static final long serialVersionUID = 4372092863401688032L;

    private List<OrderDetailDto> details;
    private List<OrderPromotionDto> promotions;

    public OrderDto(I_Order order, I_ProductPhotoFactory productPhotoFactory) {
        super(order);

        if (order.getDetails() != null) {
            this.details = new ArrayList<OrderDetailDto>(order.getDetails().size());
            for (I_OrderDetail detail : order.getDetails()) {
                this.details.add(new OrderDetailDto(detail, productPhotoFactory));
            }
        }

        if (order.getPromotions() != null) {
            this.promotions = new ArrayList<OrderPromotionDto>(order.getDetails().size());
            for (I_OrderPromotion promotionResult : order.getPromotions()) {
                this.promotions.add(new OrderPromotionDto(promotionResult, productPhotoFactory));
            }
        }
    }

    public List<OrderDetailDto> getDetails() {
        return details;
    }

    public List<OrderPromotionDto> getPromotions() {
        return promotions;
    }
    
    public static class OrderDetailDto implements Serializable {

        private static final long serialVersionUID = 2190025456968539879L;

        private BigDecimal quantity;
        private OrderProductDto product;

        public OrderDetailDto(I_OrderDetail detail, I_ProductPhotoFactory productPhotoFactory) {
            super();

            this.quantity = detail.getQuantity();
            this.product = new OrderProductDto(detail.getProduct(), productPhotoFactory);
        }
        
        public BigDecimal getQuantity() {
            return quantity;
        }

        public void setQuantity(BigDecimal quantity) {
            this.quantity = quantity;
        }

        public OrderProductDto getProduct() {
            return product;
        }

        public void setProduct(OrderProductDto product) {
            this.product = product;
        }

        public BigDecimal getAmount() {
            if (this.product == null || this.quantity == null || this.quantity.signum() <= 0
                    || this.product.getPrice() == null || this.product.getPrice().signum() <= 0)
                return BigDecimal.ZERO;

            return this.product.getPrice().multiply(this.quantity);
        }

    }

}
