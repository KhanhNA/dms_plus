package com.viettel.backend.dto.order;

import java.math.BigDecimal;
import java.util.Map;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.product.ProductEmbedDto;
import com.viettel.repository.common.domain.data.I_OrderProduct;
import com.viettel.repository.common.domain.product.I_Product;

public class OrderProductDto extends ProductEmbedDto {

    private static final long serialVersionUID = 7240727794465791875L;

    private BigDecimal price;
    private BigDecimal productivity;

    public OrderProductDto(I_Product product, I_ProductPhotoFactory productPhotoFactory, Map<String, BigDecimal> priceList) {
        super(product, productPhotoFactory);

        BigDecimal price = null;
        if (priceList != null) {
            price = priceList.get(product.getId());
        }
        price = price == null ? product.getPrice() : price;
        this.price = price;
        
        this.productivity = product.getProductivity();
    }

    public OrderProductDto(I_OrderProduct product, I_ProductPhotoFactory productPhotoFactory) {
        super(product, productPhotoFactory);

        this.price = product.getPrice();
        this.productivity = product.getProductivity();
    }

    public OrderProductDto(String productName, I_ProductPhotoFactory productPhotoFactory) {
        super(productName, productPhotoFactory);
        
        this.price = BigDecimal.ZERO;
        this.productivity = BigDecimal.ZERO;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }
    
}
