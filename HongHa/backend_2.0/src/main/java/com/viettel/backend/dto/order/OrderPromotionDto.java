package com.viettel.backend.dto.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.product.ProductEmbedDto;
import com.viettel.repository.common.domain.data.I_OrderPromotion;
import com.viettel.repository.common.domain.data.I_OrderPromotionDetail;
import com.viettel.repository.common.domain.data.I_OrderPromotionReward;

public class OrderPromotionDto extends CategorySimpleDto {

    private static final long serialVersionUID = 4560950228432621757L;

    private List<OrderPromotionDetailDto> details;

    public OrderPromotionDto(I_OrderPromotion orderPromotion, I_ProductPhotoFactory productPhotoFactory) {
        super(orderPromotion);

        if (orderPromotion.getDetails() != null) {
            this.details = new ArrayList<OrderPromotionDetailDto>(orderPromotion.getDetails().size());
            for (I_OrderPromotionDetail detail : orderPromotion.getDetails()) {
                this.details.add(
                        new OrderPromotionDetailDto(detail, detail.getConditionProductName(), productPhotoFactory));
            }
        }
    }

    public List<OrderPromotionDetailDto> getDetails() {
        return details;
    }

    public void setDetails(List<OrderPromotionDetailDto> details) {
        this.details = details;
    }

    public void addDetail(OrderPromotionDetailDto detail) {
        if (this.details == null) {
            this.details = new LinkedList<OrderPromotionDetailDto>();
        }

        this.details.add(detail);
    }

    public static class OrderPromotionDetailDto extends DTOSimple {

        private static final long serialVersionUID = -386849109139576650L;

        private String conditionProductName;
        private OrderPromotionRewardDto reward;

        public OrderPromotionDetailDto(I_OrderPromotionDetail orderPromotionDetail, String conditionProductName,
                I_ProductPhotoFactory productPhotoFactory) {
            super(String.valueOf(orderPromotionDetail.getSeqNo()));

            this.conditionProductName = conditionProductName;
            this.reward = new OrderPromotionRewardDto(orderPromotionDetail.getReward(), productPhotoFactory);
        }

        public String getConditionProductName() {
            return conditionProductName;
        }

        public OrderPromotionRewardDto getReward() {
            return reward;
        }

    }

    public static class OrderPromotionRewardDto implements Serializable {

        private static final long serialVersionUID = -4119208471901748577L;

        private BigDecimal quantity;
        private ProductEmbedDto product;

        private BigDecimal amount;

        public OrderPromotionRewardDto(I_OrderPromotionReward reward, I_ProductPhotoFactory productPhotoFactory) {
            super();

            this.quantity = reward.getQuantity();

            if (reward.getProduct() != null) {
                this.product = new ProductEmbedDto(reward.getProduct(), productPhotoFactory);
            } else if (reward.getProductText() != null) {
                this.product = new ProductEmbedDto(reward.getProductText(), productPhotoFactory);
            }

            this.amount = reward.getAmount();
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        public ProductEmbedDto getProduct() {
            return product;
        }

        public BigDecimal getAmount() {
            return amount;
        }

    }

}
