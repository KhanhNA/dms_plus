package com.viettel.backend.dto.order;

import java.math.BigDecimal;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.backend.dto.customer.CustomerSimpleDto;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.data.I_OrderHeader;

public class OrderSimpleDto extends DTOSimple {

    private static final long serialVersionUID = -25582112767713146L;

    private CategorySimpleDto distributor;
    private CustomerSimpleDto customer;
    private UserSimpleDto createdBy;

    private String createdTime;

    private String code;
    private int approveStatus;

    private int deliveryType;
    private String deliveryTime;

    private String comment;

    private BigDecimal subTotal;
    private BigDecimal promotionAmt;

    private BigDecimal discountPercentage;
    private BigDecimal discountAmt;

    private BigDecimal grandTotal;
    private BigDecimal quantity;
    private BigDecimal productivity;

    private boolean isWithVisit;
    
    private boolean vanSales;

    public OrderSimpleDto(I_OrderHeader order) {
        super(order);

        this.createdTime = order.getCreatedTime() != null ? order.getCreatedTime().getIsoTime() : null;
        this.code = order.getCode();
        this.approveStatus = order.getApproveStatus();
        this.deliveryType = order.getDeliveryType();
        this.deliveryTime = order.getDeliveryTime() != null ? order.getDeliveryTime().getIsoTime() : null;
        this.comment = order.getComment();
        this.subTotal = order.getSubTotal();
        this.promotionAmt = order.getPromotionAmt();
        this.discountPercentage = order.getDiscountPercentage();
        this.discountAmt = order.getDiscountAmt();
        this.grandTotal = order.getGrandTotal();
        this.quantity = order.getQuantity();
        this.productivity = order.getProductivity();
        this.isWithVisit = order.isWithVisit();

        if (order.getDistributor() != null) {
            this.distributor = new CategorySimpleDto(order.getDistributor());
        }

        if (order.getCustomer() != null) {
            this.customer = new CustomerSimpleDto(order.getCustomer());
        }

        if (order.getCreatedBy() != null) {
            this.createdBy = new UserSimpleDto(order.getCreatedBy());
        }
        
        this.vanSales = order.isVanSales();
    }

    public CategorySimpleDto getDistributor() {
        return distributor;
    }

    public CustomerSimpleDto getCustomer() {
        return customer;
    }

    public UserSimpleDto getCreatedBy() {
        return createdBy;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public String getCode() {
        return code;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public BigDecimal getPromotionAmt() {
        return promotionAmt;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public boolean isWithVisit() {
        return isWithVisit;
    }

    public boolean isVanSales() {
        return vanSales;
    }

    
}
