package com.viettel.backend.dto.order;

import java.math.BigDecimal;
import java.util.Map;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.repository.common.domain.product.I_Product;

public class ProductForOrderDto extends OrderProductDto {

    private static final long serialVersionUID = -7661118391476167272L;

    private int seqNo;
    private BigDecimal availableQuantity;

    public ProductForOrderDto(I_Product product, I_ProductPhotoFactory productPhotoFactory,
            Map<String, BigDecimal> priceList) {
        this(product, productPhotoFactory, priceList, 0);
    }

    public ProductForOrderDto(I_Product product, I_ProductPhotoFactory productPhotoFactory,
            Map<String, BigDecimal> priceList, int seqNo) {
        super(product, productPhotoFactory, priceList);

        this.seqNo = seqNo;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public BigDecimal getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(BigDecimal availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

}
