package com.viettel.backend.dto.order;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.product.ProductEmbedDto;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductEmbed;

public class ProductSalesResultDto extends ProductEmbedDto {

    private static final long serialVersionUID = 3641902226456577012L;

    private SalesResultDto salesResult;

    public ProductSalesResultDto(I_Product product, I_ProductPhotoFactory productPhotoFactory,
            SalesResultDto salesResult) {
        super(product, productPhotoFactory);

        this.salesResult = salesResult;
    }

    public ProductSalesResultDto(I_ProductEmbed product, I_ProductPhotoFactory productPhotoFactory,
            SalesResultDto salesResult) {
        super(product, productPhotoFactory);

        this.salesResult = salesResult;
    }

    public SalesResultDto getSalesResult() {
        return salesResult;
    }

    public void setSalesResult(SalesResultDto salesResult) {
        this.salesResult = salesResult;
    }

}
