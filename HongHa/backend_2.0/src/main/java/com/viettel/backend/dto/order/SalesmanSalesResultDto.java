package com.viettel.backend.dto.order;

import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_UserEmbed;

public class SalesmanSalesResultDto extends UserSimpleDto {

    private static final long serialVersionUID = 3641902226456577012L;

    private SalesResultDto salesResult;

    public SalesmanSalesResultDto(I_User user, SalesResultDto salesResult) {
        super(user);

        this.salesResult = salesResult;
    }
    
    public SalesmanSalesResultDto(I_UserEmbed user, SalesResultDto salesResult) {
        super(user);

        this.salesResult = salesResult;
    }

    public SalesResultDto getSalesResult() {
        return salesResult;
    }

    public void setSalesResult(SalesResultDto salesResult) {
        this.salesResult = salesResult;
    }

}
