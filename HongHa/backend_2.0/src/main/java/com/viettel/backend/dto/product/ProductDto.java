package com.viettel.backend.dto.product;

import java.math.BigDecimal;
import java.util.Map;

import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.repository.common.domain.product.I_Product;

public class ProductDto extends CategoryDto {

    private static final long serialVersionUID = -3795654109422932796L;

    private String photo;
    private BigDecimal price;
    private BigDecimal productivity;
    private String description;

    private CategorySimpleDto uom;
    private CategorySimpleDto productCategory;

    private BigDecimal availableQuantity;
    
    public ProductDto(I_Product product, I_ProductPhotoFactory productPhotoFactory,
            Map<String, BigDecimal> priceList) {
        this(product, productPhotoFactory, priceList, null);
    }

    public ProductDto(I_Product product, I_ProductPhotoFactory productPhotoFactory, Map<String, BigDecimal> priceList,
            BigDecimal availableQuantity) {
        super(product);

        BigDecimal price = null;
        if (priceList != null) {
            price = priceList.get(product.getId());
        }
        price = price == null ? product.getPrice() : price;
        this.price = price;

        this.productivity = product.getProductivity();
        this.description = product.getDescription();

        if (product.getUom() != null) {
            this.uom = new CategorySimpleDto(product.getUom());
        }

        if (product.getProductCategory() != null) {
            this.productCategory = new CategorySimpleDto(product.getProductCategory());
        }

        if (productPhotoFactory != null) {
        	this.photo = productPhotoFactory.getPhoto(product.getId());
        }
        
        this.availableQuantity = availableQuantity;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public CategorySimpleDto getUom() {
        return uom;
    }

    public void setUom(CategorySimpleDto uom) {
        this.uom = uom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategorySimpleDto getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(CategorySimpleDto productCategory) {
        this.productCategory = productCategory;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public BigDecimal getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(BigDecimal availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

}
