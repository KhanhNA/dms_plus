package com.viettel.backend.dto.product;

import org.springframework.util.StringUtils;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.repository.common.domain.product.I_ProductEmbed;

public class ProductEmbedDto extends CategorySimpleDto {

    private static final long serialVersionUID = -2163586992054969389L;
    
    private String photo;
    private CategorySimpleDto uom;
    private CategorySimpleDto productCategory;

    public ProductEmbedDto(I_ProductEmbed product, I_ProductPhotoFactory productPhotoFactory) {
        super(product);
        
        if (product.getUom() != null) {
            this.uom = new CategorySimpleDto(product.getUom());
        }

        if (product.getProductCategory() != null) {
            this.productCategory = new CategorySimpleDto(product.getProductCategory());
        }
        
        this.photo = productPhotoFactory.getPhoto(product.getId());
    }
    
    public ProductEmbedDto(String productName, I_ProductPhotoFactory productPhotoFactory) {
        super(StringUtils.trimAllWhitespace(productName), productName, productName);
        setProductCategory(new CategorySimpleDto("no", "no", "no"));
        setUom(new CategorySimpleDto("no", "no", "no"));
        this.photo = productPhotoFactory.getPhoto(null);
    }
    
    public String getPhoto() {
        return photo;
    }

    public CategorySimpleDto getUom() {
        return uom;
    }

    public CategorySimpleDto getProductCategory() {
        return productCategory;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setUom(CategorySimpleDto uom) {
        this.uom = uom;
    }

    public void setProductCategory(CategorySimpleDto productCategory) {
        this.productCategory = productCategory;
    }

}
