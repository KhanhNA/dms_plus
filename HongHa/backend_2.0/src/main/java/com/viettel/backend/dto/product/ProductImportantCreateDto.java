package com.viettel.backend.dto.product;

import java.io.Serializable;
import java.util.Set;

public class ProductImportantCreateDto implements Serializable {

	private static final long serialVersionUID = -4450282711954150869L;

	private Set<String> productIds;

	public Set<String> getProductIds() {
		return productIds;
	}

	public void setProductIds(Set<String> productIds) {
		this.productIds = productIds;
	}

}
