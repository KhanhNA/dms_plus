package com.viettel.backend.dto.product;

import com.viettel.repository.common.domain.product.I_Product;

public class ProductImportantDto extends ProductDto{
	private static final long serialVersionUID = -4299844150381024703L;
	
	private boolean isImportant;

	public ProductImportantDto(I_Product product, boolean isImportant) {
		super(product, null, null);
		this.isImportant = isImportant;
	}

	public boolean isImportant() {
		return isImportant;
	}

	public void setImportant(boolean isImportant) {
		this.isImportant = isImportant;
	}	
}
