package com.viettel.backend.dto.product;

import java.math.BigDecimal;
import java.util.Map;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.repository.common.domain.data.I_OrderProduct;
import com.viettel.repository.common.domain.product.I_Product;

public class ProductSimpleDto extends ProductEmbedDto {

	private static final long serialVersionUID = -3795654109422932796L;

	private BigDecimal price;
	private BigDecimal productivity;
	private String description;

	public ProductSimpleDto(I_Product product, I_ProductPhotoFactory productPhotoFactory,
			Map<String, BigDecimal> priceList) {
		super(product, productPhotoFactory);

		BigDecimal price = null;
		if (priceList != null) {
			price = priceList.get(product.getId());
		}
		price = price == null ? product.getPrice() : price;
		this.price = price;

		this.productivity = product.getProductivity();

		this.description = product.getDescription();
	}

	public ProductSimpleDto(I_OrderProduct product, I_ProductPhotoFactory productPhotoFactory) {
		super(product, productPhotoFactory);

		this.price = product.getPrice();

		this.productivity = product.getProductivity();
	}

	public ProductSimpleDto(I_ProductPhotoFactory productPhotoFactory) {
		super("", productPhotoFactory);
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getProductivity() {
		return productivity;
	}

	public void setProductivity(BigDecimal productivity) {
		this.productivity = productivity;
	}

}
