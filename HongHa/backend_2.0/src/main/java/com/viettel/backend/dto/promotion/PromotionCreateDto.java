package com.viettel.backend.dto.promotion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import com.viettel.backend.dto.common.CategoryCreateDto;

public class PromotionCreateDto extends CategoryCreateDto {

    private static final long serialVersionUID = -1603649145435793534L;

    private String startDate;
    private String endDate;
    private String applyFor;
    private String description;
    private List<PromotionDetailCreateDto> details;
    private boolean forAllDistributor;
    private Set<String> distributorIds;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getApplyFor() {
        return applyFor;
    }

    public void setApplyFor(String applyFor) {
        this.applyFor = applyFor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDetails(List<PromotionDetailCreateDto> details) {
        this.details = details;
    }

    public List<PromotionDetailCreateDto> getDetails() {
        return details;
    }

    public boolean isForAllDistributor() {
        return forAllDistributor;
    }

    public void setForAllDistributor(boolean forAllDistributor) {
        this.forAllDistributor = forAllDistributor;
    }

    public Set<String> getDistributorIds() {
        return distributorIds;
    }

    public void setDistributorIds(Set<String> distributorIds) {
        this.distributorIds = distributorIds;
    }

    public static class PromotionDetailCreateDto implements Serializable {

        private static final long serialVersionUID = 1L;

        private int type;
        private PromotionConditionCreateDto condition;
        private PromotionRewardCreateDto reward;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public PromotionConditionCreateDto getCondition() {
            return condition;
        }

        public void setCondition(PromotionConditionCreateDto condition) {
            this.condition = condition;
        }

        public PromotionRewardCreateDto getReward() {
            return reward;
        }

        public void setReward(PromotionRewardCreateDto reward) {
            this.reward = reward;
        }

    }

    public static class PromotionConditionCreateDto implements Serializable {

        private static final long serialVersionUID = 1L;

        private String productId;
        private BigDecimal quantity;

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        public void setQuantity(BigDecimal quantity) {
            this.quantity = quantity;
        }

    }

    public static class PromotionRewardCreateDto implements Serializable {

        private static final long serialVersionUID = 1L;

        private String productText;
        private String productId;
        private BigDecimal percentage;
        private BigDecimal quantity;

        public String getProductText() {
            return productText;
        }

        public void setProductText(String productText) {
            this.productText = productText;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public BigDecimal getPercentage() {
            return percentage;
        }

        public void setPercentage(BigDecimal percentage) {
            this.percentage = percentage;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        public void setQuantity(BigDecimal quantity) {
            this.quantity = quantity;
        }

    }

}
