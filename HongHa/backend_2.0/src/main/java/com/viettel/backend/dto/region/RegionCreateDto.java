package com.viettel.backend.dto.region;

import java.util.List;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.service.core.pdto.CategoryEmbed;

public class RegionCreateDto extends CategoryCreateDto{

	private static final long serialVersionUID = 1045918106066437464L;
	private String parentRegionId;
	private List<CategoryEmbed> children;
	private List<CategoryEmbed> distributors;
	
	public String getParentRegionId() {
		return parentRegionId;
	}
	public void setParentRegionId(String parentRegionId) {
		this.parentRegionId = parentRegionId;
	}
	public List<CategoryEmbed> getChildren() {
		return children;
	}
	public void setChildren(List<CategoryEmbed> children) {
		this.children = children;
	}
	public List<CategoryEmbed> getDistributors() {
		return distributors;
	}
	public void setDistributors(List<CategoryEmbed> distributors) {
		this.distributors = distributors;
	}
	
	
}
