package com.viettel.backend.dto.region;


import com.viettel.repository.common.domain.data.I_Region;

public class RegionDto extends RegionListDto{

	private static final long serialVersionUID = -2163173277674381972L;
    
	public RegionDto(I_Region region) {
		super(region);
		
	}
}
