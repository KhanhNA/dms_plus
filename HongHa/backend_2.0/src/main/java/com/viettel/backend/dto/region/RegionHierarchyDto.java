package com.viettel.backend.dto.region;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.viettel.repository.common.domain.data.I_Region;

public class RegionHierarchyDto  extends RegionListDto implements Serializable{
	
	public RegionHierarchyDto(I_Region region) {
		super(region);
			}
	
	private static final long serialVersionUID = -7731529677909365535L;
	private String name;
	private String id;
	
	private List<RegionHierarchyDto> children;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<RegionHierarchyDto> getChildren() {
		return children;
	}
	public void setChildren(List<RegionHierarchyDto> children) {
		this.children = children;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void addChild(RegionHierarchyDto item){
		if(children ==null){
			children = new ArrayList<RegionHierarchyDto>();
		}
		children.add(item);
	}
	
}
