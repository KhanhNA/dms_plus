package com.viettel.backend.dto.region;

import java.util.HashSet;
import java.util.Set;

import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;

public class RegionListDto extends CategoryDto {

	private static final long serialVersionUID = 4007837718830522335L;

	CategorySimpleDto parent;
	Set<CategorySimpleDto> distributors;

	public RegionListDto(I_Region region) {
		super(region);
		if (region.getParent() != null) {
			this.parent = new CategorySimpleDto(region.getParent());
		}

		if (region.getDistributors() != null) {
			this.distributors = new HashSet<>();
			for (I_CategoryEmbed item : region.getDistributors()) {
				this.distributors.add(new CategorySimpleDto(item));
			}
		}
	}

	public CategorySimpleDto getParent() {
		return parent;
	}

	public void setParent(CategorySimpleDto parent) {
		this.parent = parent;
	}

	public Set<CategorySimpleDto> getDistributors() {
		return distributors;
	}

	public void setDistributors(Set<CategorySimpleDto> distributors) {
		this.distributors = distributors;
	}

}
