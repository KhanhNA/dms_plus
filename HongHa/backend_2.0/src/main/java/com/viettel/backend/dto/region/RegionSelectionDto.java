package com.viettel.backend.dto.region;

import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.data.I_Region;

public class RegionSelectionDto extends CategorySimpleDto {

	private static final long serialVersionUID = -7346806610967630694L;

	private boolean selected;
	List<RegionSelectionDto> children;

	public RegionSelectionDto(I_Category category, boolean selected) {
		super(category);
		this.selected = selected;
	}
	public RegionSelectionDto(I_Region region){
		super(region.getId(),region.getName(),region.getCode());
	}

	public List<RegionSelectionDto> getChildren() {
		return children;
	}

	public void setChildren(List<RegionSelectionDto> children) {
		this.children = children;
	}

	public void addChild(RegionSelectionDto item) {
		if (children == null) {
			children = new ArrayList<RegionSelectionDto>();
		}
		children.add(item);
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
