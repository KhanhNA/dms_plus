package com.viettel.backend.dto.report;

import java.io.Serializable;
import java.util.Set;

public class ReportDistributorListIdDto implements Serializable{
	private static final long serialVersionUID = 124294398648942862L;
	
	private Set<String> distributorIds;

	public Set<String> getDistributorIds() {
		return distributorIds;
	}

	public void setDistributorIds(Set<String> distributorIds) {
		this.distributorIds = distributorIds;
	}


}
