package com.viettel.backend.dto.schedule;

import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.customer.I_Route;

public class RouteDto extends CategoryDto {

    private static final long serialVersionUID = 7395682242004573136L;

    private UserSimpleDto salesman;
    
    public RouteDto(I_Route route) {
        super(route);
        
        if (route.getSalesman() != null) {
            this.salesman = new UserSimpleDto(route.getSalesman());
        }
    }

    public UserSimpleDto getSalesman() {
        return salesman;
    }

    public void setSalesman(UserSimpleDto salesman) {
        this.salesman = salesman;
    }

}
