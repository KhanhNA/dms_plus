package com.viettel.backend.dto.schedule;

import java.io.Serializable;

import com.viettel.repository.common.domain.customer.I_Schedule;

public class ScheduleDto implements Serializable {

    private static final long serialVersionUID = 4408779711806179090L;

    public String routeId;
    public ScheduleItemDto item;

    public ScheduleDto() {
    }

    public ScheduleDto(I_Schedule schedule) {
        this.routeId = schedule.getRouteId().toString();
        if (schedule.getItem() != null) {
            this.item = new ScheduleItemDto(schedule.getItem());
        }
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public ScheduleItemDto getItem() {
        return item;
    }

    public void setItem(ScheduleItemDto item) {
        this.item = item;
    }

}
