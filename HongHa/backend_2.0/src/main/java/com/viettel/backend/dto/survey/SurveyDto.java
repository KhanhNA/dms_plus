package com.viettel.backend.dto.survey;

import java.util.ArrayList;
import java.util.List;

import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyOption;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;

public class SurveyDto extends SurveyListDto {

    private static final long serialVersionUID = 4294936505380930621L;

    private List<SurveyQuestionDto> questions;

    public SurveyDto(I_Survey survey) {
        super(survey);

        if (survey.getQuestions() != null) {
            for (I_SurveyQuestion surveyQuestion : survey.getQuestions()) {
                addQuestion(new SurveyQuestionDto(surveyQuestion));
            }
        }
    }

    public List<SurveyQuestionDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<SurveyQuestionDto> questions) {
        this.questions = questions;
    }

    public void addQuestion(SurveyQuestionDto question) {
        if (this.questions == null) {
            this.questions = new ArrayList<SurveyQuestionDto>();
        }

        this.questions.add(question);
    }
    
    public static class SurveyQuestionDto extends DTOSimple {

        private static final long serialVersionUID = 5496584668724710642L;

        private String name;
        private boolean multipleChoice;
        private boolean required;

        private List<SurveyOptionDto> options;

        public SurveyQuestionDto(I_SurveyQuestion surveyQuestion) {
            super(String.valueOf(surveyQuestion.getSeqNo()));

            this.name = surveyQuestion.getName();
            this.multipleChoice = surveyQuestion.isMultipleChoice();
            this.required = surveyQuestion.isRequired();

            if (surveyQuestion.getOptions() != null) {
                this.options = new ArrayList<>(surveyQuestion.getOptions().size());
                for (I_SurveyOption surveyOption : surveyQuestion.getOptions()) {
                    this.options.add(new SurveyOptionDto(surveyOption));
                }
            }
        }

        public String getName() {
            return name;
        }
        
        public boolean isMultipleChoice() {
            return multipleChoice;
        }

        public boolean isRequired() {
            return required;
        }

        public List<SurveyOptionDto> getOptions() {
            return options;
        }
    }
    
    public static class SurveyOptionDto extends DTOSimple {

        private static final long serialVersionUID = 4085000556181222126L;

        private String name;
        
        public SurveyOptionDto(I_SurveyOption surveyOption) {
            super(String.valueOf(surveyOption.getSeqNo()));
            
            this.name = surveyOption.getName();
        }
        
        public String getName() {
            return name;
        }

    }

}
