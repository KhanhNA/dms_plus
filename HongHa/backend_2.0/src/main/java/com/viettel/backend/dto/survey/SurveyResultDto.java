package com.viettel.backend.dto.survey;

import java.io.Serializable;
import java.util.List;

import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyOption;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;

public class SurveyResultDto extends SurveyListDto {

    private static final long serialVersionUID = 8542679375956025478L;
    
    private List<SurveyQuestionResultDto> questions;

    public SurveyResultDto(I_Survey survey, List<SurveyQuestionResultDto> questions) {
        super(survey);
        
        this.questions = questions;
    }

    public List<SurveyQuestionResultDto> getQuestions() {
        return questions;
    }

    public void setQuestions(List<SurveyQuestionResultDto> questions) {
        this.questions = questions;
    }

    public static class SurveyQuestionResultDto implements Serializable {

        private static final long serialVersionUID = 5496584668724710642L;

        private int seqNo;
        private String name;
        private boolean multipleChoice;
        private boolean required;
        private List<SurveyOptionResultDto> options;

        public SurveyQuestionResultDto(I_SurveyQuestion question, List<SurveyOptionResultDto> options) {
            super();

            this.seqNo  = question.getSeqNo();
            this.name = question.getName();
            this.multipleChoice = question.isMultipleChoice();
            this.required = question.isRequired();
            this.options = options;
        }

        public int getSeqNo() {
            return seqNo;
        }
        
        public String getName() {
            return name;
        }
        
        public boolean isMultipleChoice() {
            return multipleChoice;
        }

        public boolean isRequired() {
            return required;
        }

        public List<SurveyOptionResultDto> getOptions() {
            return options;
        }

    }

    public static class SurveyOptionResultDto implements Serializable {

        private static final long serialVersionUID = 4085000556181222126L;

        private int seqNo;
        private String name;
        private int result;

        public SurveyOptionResultDto(I_SurveyOption option, int result) {
            super();
            
            this.seqNo  = option.getSeqNo();
            this.name = option.getName();
            this.result = result;
        }
        
        public int getSeqNo() {
            return seqNo;
        }
        
        public String getName() {
            return name;
        }

        public int getResult() {
            return result;
        }

    }

}
