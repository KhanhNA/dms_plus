package com.viettel.backend.dto.system;

import java.util.Collections;
import java.util.List;

import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.repository.common.domain.common.I_Category;

public class ClientInfoDto extends DTOSimple {

    private static final long serialVersionUID = 1171931428743195037L;
    
    private String name;
    private String code;
    private List<String> modules;

    public ClientInfoDto(I_Category client, List<String> modules) {
        super(client);
        this.name = client.getName();
        this.code = client.getCode();
        if (modules == null) {
            this.modules = Collections.emptyList();
        } else {
            this.modules = modules;
        }
    }

    public List<String> getModules() {
        return modules;
    }

    public void setModules(List<String> modules) {
        this.modules = modules;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
