package com.viettel.backend.dto.system;

import java.util.List;
import java.util.Set;

import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.entity.Location;

public class UserInfoDto extends DTOSimple {

    private static final long serialVersionUID = -8039769069456882676L;

    private String username;
    private String fullname;
    private String roleCode;
    private String clientCode;
    private String clientName;

    private I_Config config;

    private boolean vanSales;

    private List<String> languages;

    public UserInfoDto(String clientCode, String clientName, String userId, String username, String fullname,
            String roleCode, I_Config config, List<String> languages) {
        super(userId);

        this.username = username;
        this.fullname = fullname;
        this.roleCode = roleCode;
        this.clientCode = clientCode;
        this.clientName = clientName;

        this.config = config;
        this.languages = languages;

        
    }

    public UserInfoDto(I_User user, String clientCode, String clientName, I_Config config, List<String> languages) {
        this(clientCode, clientName, user.getId(), user.getUsername(), user.getFullname(), user.getRole(), config,
                languages);

        this.vanSales = user.isVanSales();
    }

    public String getClientCode() {
        return clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public String getUsername() {
        return username;
    }

    public String getFullname() {
        return fullname;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public String getDateFormat() {
        return config == null ? null : config.getDateFormat();
    }

    public String getProductPhoto() {
        return config == null ? null : config.getProductPhoto();
    }

    public Location getLocation() {
        return config == null ? null : config.getLocation();
    }

    public int getFirstDayOfWeek() {
        return config == null ? null : config.getFirstDayOfWeek();
    }

    public int getMinimalDaysInFirstWeek() {
        return config == null ? null : config.getMinimalDaysInFirstWeek();
    }

    public int getNumberWeekOfFrequency() {
        return config == null ? null : config.getNumberWeekOfFrequency();
    }

    public int getNumberDayOrderPendingExpire() {
        return config == null ? null : config.getNumberDayOrderPendingExpire();
    }

    public OrderDateType getOrderDateType() {
        return config == null ? null : config.getOrderDateType();
    }

    public long getVisitDurationKPI() {
        return config == null ? null : config.getVisitDurationKPI();
    }

    public double getVisitDistanceKPI() {
        return config == null ? null : config.getVisitDistanceKPI();
    }

    public boolean isCanEditCustomerLocation() {
        return config == null ? null : config.isCanEditCustomerLocation();
    }

    public Set<String> getModules() {
        return config == null ? null : config.getModules();
    }
    
    public String getProductivityUnit() {
        return config == null ? null : config.getProductivityUnit();
    }

    public boolean isVanSales() {
        return vanSales;
    }

    public List<String> getLanguages() {
        return languages;
    }

}
