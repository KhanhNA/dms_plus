package com.viettel.backend.dto.system;

import java.io.Serializable;
import java.util.Set;

public class UserLoginDto implements Serializable {

    private static final long serialVersionUID = -1767765201169200675L;

    private String clientId;
    private String clientCode;
    private String clientName;
    private String userId;
    private String username;
    private String role;
    private Set<String> modules;

    public UserLoginDto(String clientId, String clientCode, String clientName, String userId, String username,
            String role, Set<String> modules) {
        super();
        this.clientId = clientId;
        this.clientCode = clientCode;
        this.clientName = clientName;
        this.userId = userId;
        this.username = username;
        this.role = role;
        this.modules = modules;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getRole() {
        return role;
    }

    public Set<String> getModules() {
        return modules;
    }

}
