package com.viettel.backend.dto.target;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.LinkedList;

import org.springframework.util.Assert;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.common.ProgressDto;
import com.viettel.backend.dto.order.SalesResultDailyDto;
import com.viettel.backend.dto.product.ProductSimpleDto;
import com.viettel.backend.dto.user.UserDto;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_OrderProduct;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User;

public class PerformanceDto extends UserDto {

    private static final long serialVersionUID = -2017957272413725675L;

    private int nbDay;
    private ProgressDto revenue;
    private ProgressDto subRevenue;
//    private ProgressDto quantity;
    private ProgressDto productivity;
    private ProgressDto nbOrder;
    private ProgressDto newCustomer;
    private ProgressDto nbVisit;
    private int nbVisitErrorPosition;
    private int nbVisitErrorDuration;
    private int nbVisitHasOrder;

    private int totalSKU;
    private BigDecimal skuByOrderPlan;
    private BigDecimal productivityByOrderPlan;
    private BigDecimal revenueByOrderPlan;

    private Collection<SalesResultDailyDto> salesResultsDaily;
    private Collection<ProductCategorySalesQuantityDto> productCategoriesSalesQuantity;

    public PerformanceDto(I_User user) {
        super(user);

        this.nbDay = 0;
        this.revenue = new ProgressDto();
        this.subRevenue = new ProgressDto();
//        this.quantity = new ProgressDto();
        this.productivity = new ProgressDto();
        this.nbOrder = new ProgressDto();
        this.newCustomer = new ProgressDto();
        this.nbVisit = new ProgressDto();
        this.nbVisitErrorPosition = 0;
        this.nbVisitErrorDuration = 0;
        this.nbVisitHasOrder = 0;

        this.totalSKU = 0;
        this.skuByOrderPlan = BigDecimal.ZERO;
        this.productivityByOrderPlan = BigDecimal.ZERO;
        this.revenueByOrderPlan = BigDecimal.ZERO;
    }

    public int getNbDay() {
        return nbDay;
    }

    public void setNbDay(int nbDay) {
        this.nbDay = nbDay;
    }

    public ProgressDto getRevenue() {
        return revenue;
    }

    public ProgressDto getSubRevenue() {
        return subRevenue;
    }
//    public ProgressDto getQuantity() {
//        return quantity;
//    }

    public ProgressDto getProductivity() {
        return productivity;
    }

    public ProgressDto getNbOrder() {
        return nbOrder;
    }

    public ProgressDto getNewCustomer() {
        return newCustomer;
    }

    public ProgressDto getNbVisit() {
        return nbVisit;
    }

    public void incrementTotalSku(int totalSKU) {
        this.totalSKU += totalSKU;
    }

    public void setSkuByOrderPlan(BigDecimal skuByOrderPlan) {
        this.skuByOrderPlan = skuByOrderPlan;
    }

    public void setProductivityByOrderPlan(BigDecimal productivityByOrderPlan) {
        this.productivityByOrderPlan = productivityByOrderPlan;
    }

    public void setRevenueByOrderPlan(BigDecimal revenueByOrderPlan) {
        this.revenueByOrderPlan = revenueByOrderPlan;
    }

    public int getNbVisitErrorPosition() {
        return nbVisitErrorPosition;
    }

    public void setNbVisitErrorPosition(int nbVisitErrorPosition) {
        this.nbVisitErrorPosition = nbVisitErrorPosition;
    }

    public int getNbVisitErrorDuration() {
        return nbVisitErrorDuration;
    }

    public void setNbVisitErrorDuration(int nbVisitErrorDuration) {
        this.nbVisitErrorDuration = nbVisitErrorDuration;
    }

    public int getNbVisitHasOrder() {
        return nbVisitHasOrder;
    }

    public void setNbVisitHasOrder(int nbVisitHasOrder) {
        this.nbVisitHasOrder = nbVisitHasOrder;
    }

    public ProgressDto getSkuByOrder() {
        if (nbOrder.getActual().compareTo(BigDecimal.ZERO) > 0) {
            return new ProgressDto(skuByOrderPlan, new BigDecimal(totalSKU).divide(nbOrder.getActual(), 2, RoundingMode.HALF_UP));
        } else {
            return new ProgressDto(skuByOrderPlan, BigDecimal.ZERO);
        }
    }

    public ProgressDto getRevenueByOrder() {
        if (nbOrder.getActual().compareTo(BigDecimal.ZERO) > 0) {
            return new ProgressDto(revenueByOrderPlan, this.revenue.getActual().divide(nbOrder.getActual(), 2, RoundingMode.HALF_UP));
        } else {
            return new ProgressDto(revenueByOrderPlan, BigDecimal.ZERO);
        }
    }
    
    public ProgressDto getProductivityByOrder() {
        if (nbOrder.getActual().compareTo(BigDecimal.ZERO) > 0) {
            return new ProgressDto(productivityByOrderPlan,
                    this.productivity.getActual().divide(nbOrder.getActual(), 2, RoundingMode.HALF_UP));
        } else {
            return new ProgressDto(productivityByOrderPlan, BigDecimal.ZERO);
        }
    }

    public Collection<SalesResultDailyDto> getSalesResultsDaily() {
        return salesResultsDaily;
    }

    public void setSalesResultsDaily(Collection<SalesResultDailyDto> salesResultsDaily) {
        this.salesResultsDaily = salesResultsDaily;
    }

    public Collection<ProductCategorySalesQuantityDto> getProductCategoriesSalesQuantity() {
        return productCategoriesSalesQuantity;
    }

    public void setProductCategoriesSalesQuantity(
            Collection<ProductCategorySalesQuantityDto> productCategoriesSalesQuantity) {
        this.productCategoriesSalesQuantity = productCategoriesSalesQuantity;
    }

    public static class ProductCategorySalesQuantityDto extends CategorySimpleDto {

        private static final long serialVersionUID = -2912225008471926691L;

        private BigDecimal quantity;
        private Collection<ProductSalesQuantityDto> productsSalesQuantity;

        public ProductCategorySalesQuantityDto(I_Category productCategory) {
            super(productCategory);

            this.productsSalesQuantity = new LinkedList<>();
            this.quantity = BigDecimal.ZERO;
        }
        
        public ProductCategorySalesQuantityDto(I_CategoryEmbed productCategory) {
            super(productCategory);

            this.productsSalesQuantity = new LinkedList<>();
            this.quantity = BigDecimal.ZERO;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        public void incrementQuantity(BigDecimal quantity) {
            Assert.notNull(quantity);

            if (this.quantity != null) {
                this.quantity = this.quantity.add(quantity);
            } else {
                this.quantity = quantity;
            }
        }

        public Collection<ProductSalesQuantityDto> getProductsSalesQuantity() {
            return productsSalesQuantity;
        }

        public void addProductsSalesQuantity(ProductSalesQuantityDto productSalesQuantity) {
            this.productsSalesQuantity.add(productSalesQuantity);
        }

    }

    public static class ProductSalesQuantityDto extends ProductSimpleDto {

        private static final long serialVersionUID = -9168082537833676182L;

        private BigDecimal quantity;

        public ProductSalesQuantityDto(I_Product product, I_ProductPhotoFactory productPhotoFactory) {
            super(product, productPhotoFactory, null);

            this.quantity = BigDecimal.ZERO;
        }

        public ProductSalesQuantityDto(I_OrderProduct product, I_ProductPhotoFactory productPhotoFactory) {
            super(product, productPhotoFactory);

            this.quantity = BigDecimal.ZERO;
        }

        public BigDecimal getQuantity() {
            return quantity;
        }

        public void incrementQuantity(BigDecimal quantity) {
            Assert.notNull(quantity);

            if (this.quantity != null) {
                this.quantity = this.quantity.add(quantity);
            } else {
                this.quantity = quantity;
            }
        }

    }

}
