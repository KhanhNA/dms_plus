package com.viettel.backend.dto.target;

import java.math.BigDecimal;

import com.viettel.backend.dto.common.DTO;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.category.I_Target;
import com.viettel.repository.common.domain.user.I_User;

public class TargetDto extends DTO {

    private static final long serialVersionUID = -3377268163150327565L;

    private UserSimpleDto salesman;
    private int month;
    private int year;
    private BigDecimal revenue;
    private BigDecimal subRevenue;
    private BigDecimal quantity;
    private BigDecimal productivity;
    private int nbOrder;
    private BigDecimal revenueByOrder;
    private BigDecimal skuByOrder;
    private BigDecimal productivityByOrder;
    private int newCustomer;

    public TargetDto(I_User user, int month, int year) {
        super();

        this.setSalesman(new UserSimpleDto(user));
        this.setMonth(month);
        this.setYear(year);

        this.setRevenue(BigDecimal.ZERO);
        this.setSubRevenue(BigDecimal.ZERO);
        this.setQuantity(BigDecimal.ZERO);
        this.setProductivity(BigDecimal.ZERO);
        this.setNbOrder(0);
        this.setRevenueByOrder(BigDecimal.ZERO);
        this.setSkuByOrder(BigDecimal.ZERO);
        this.setProductivityByOrder(BigDecimal.ZERO);
        this.setNewCustomer(0);
    }

    public TargetDto(I_Target domain) {
        super(domain);

        this.setSalesman(new UserSimpleDto(domain.getSalesman()));
        this.setMonth(domain.getMonth());
        this.setYear(domain.getYear());

        this.setRevenue(domain.getRevenue() == null ? BigDecimal.ZERO : domain.getRevenue());
        this.setSubRevenue(domain.getSubRevenue() == null ? BigDecimal.ZERO : domain.getSubRevenue());
        this.setQuantity(domain.getQuantity() == null ? BigDecimal.ZERO : domain.getQuantity());
        this.setProductivity(domain.getProductivity() == null ? BigDecimal.ZERO : domain.getProductivity());
        this.setNbOrder(domain.getNbOrder());
        this.setRevenueByOrder(domain.getRevenueByOrder() == null ? BigDecimal.ZERO : domain.getRevenueByOrder());
        this.setSkuByOrder(domain.getSkuByOrder() == null ? BigDecimal.ZERO : domain.getSkuByOrder());
        this.setProductivityByOrder(
                domain.getProductivityByOrder() == null ? BigDecimal.ZERO : domain.getProductivityByOrder());
        this.setNewCustomer(domain.getNewCustomer());
    }

    public UserSimpleDto getSalesman() {
        return salesman;
    }

    public void setSalesman(UserSimpleDto salesman) {
        this.salesman = salesman;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public BigDecimal getSubRevenue() {
        return subRevenue;
    }

    public void setSubRevenue(BigDecimal subRevenue) {
        this.subRevenue = subRevenue;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public int getNbOrder() {
        return nbOrder;
    }

    public void setNbOrder(int nbOrder) {
        this.nbOrder = nbOrder;
    }

    public BigDecimal getRevenueByOrder() {
        return revenueByOrder;
    }

    public void setRevenueByOrder(BigDecimal revenueByOrder) {
        this.revenueByOrder = revenueByOrder;
    }

    public BigDecimal getSkuByOrder() {
        return skuByOrder;
    }

    public void setSkuByOrder(BigDecimal skuByOrder) {
        this.skuByOrder = skuByOrder;
    }

    public BigDecimal getProductivityByOrder() {
        return productivityByOrder;
    }

    public void setProductivityByOrder(BigDecimal productivityByOrder) {
        this.productivityByOrder = productivityByOrder;
    }

    public int getNewCustomer() {
        return newCustomer;
    }

    public void setNewCustomer(int newCustomer) {
        this.newCustomer = newCustomer;
    }   

}
