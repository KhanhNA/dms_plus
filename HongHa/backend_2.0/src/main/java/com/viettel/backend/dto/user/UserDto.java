package com.viettel.backend.dto.user;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.DTO;
import com.viettel.repository.common.domain.user.I_User;

public class UserDto extends DTO {

    private static final long serialVersionUID = 1L;

    private String username;
    private String usernameFull;
    private String fullname;
    private String role;
    private CategorySimpleDto distributor;

    private boolean vanSales;

    public UserDto(I_User user) {
        super(user);

        this.username = user.getUsername();
        this.usernameFull = user.getUsernameFull();
        this.fullname = user.getFullname();
        this.role = user.getRole();
        this.vanSales = user.isVanSales();

        if (user.getDistributor() != null) {
            this.distributor = new CategorySimpleDto(user.getDistributor());
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameFull() {
        return usernameFull;
    }

    public void setUsernameFull(String usernameFull) {
        this.usernameFull = usernameFull;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public CategorySimpleDto getDistributor() {
        return distributor;
    }

    public void setDistributor(CategorySimpleDto distributor) {
        this.distributor = distributor;
    }

    public boolean isVanSales() {
        return vanSales;
    }

    public void setVanSales(boolean vanSales) {
        this.vanSales = vanSales;
    }

}
