package com.viettel.backend.dto.user;

import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_UserEmbed;

public class UserSimpleDto extends DTOSimple {

    private static final long serialVersionUID = 5689092223088750722L;

    private String username;
    private String usernameFull;
    private String fullname;

    public UserSimpleDto(I_User user) {
        super(user);

        this.username = user.getUsername();
        this.usernameFull = user.getUsernameFull();
        this.fullname = user.getFullname();
    }

    public UserSimpleDto(I_UserEmbed user) {
        super(user);

        this.username = user.getUsername();
        this.fullname = user.getFullname();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameFull() {
        return usernameFull;
    }

    public void setUsernameFull(String usernameFull) {
        this.usernameFull = usernameFull;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

}
