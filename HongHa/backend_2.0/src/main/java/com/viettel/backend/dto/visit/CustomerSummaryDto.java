package com.viettel.backend.dto.visit;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.repository.common.domain.customer.I_Customer;

public class CustomerSummaryDto extends CustomerDto {

    private static final long serialVersionUID = 1713831819511695290L;

    private BigDecimal productivityLastMonth;
    private BigDecimal productivityThisMonth;
    private long nbOrderThisMonth;

    // {month, revenue}
    private List<Map<String, Object>> revenueLastThreeMonth;

    // {date, skuNumber, total}
    private List<Map<String, Object>> lastFiveOrders;

    private boolean canEditLocation;

    public CustomerSummaryDto(I_Customer customer) {
        super(customer);
    }

    public BigDecimal getProductivityLastMonth() {
        return productivityLastMonth;
    }

    public void setProductivityLastMonth(BigDecimal productivityLastMonth) {
        this.productivityLastMonth = productivityLastMonth;
    }

    public BigDecimal getProductivityThisMonth() {
        return productivityThisMonth;
    }

    public void setProductivityThisMonth(BigDecimal productivityThisMonth) {
        this.productivityThisMonth = productivityThisMonth;
    }

    public long getNbOrderThisMonth() {
        return nbOrderThisMonth;
    }

    public void setNbOrderThisMonth(long nbOrderThisMonth) {
        this.nbOrderThisMonth = nbOrderThisMonth;
    }

    public List<Map<String, Object>> getRevenueLastThreeMonth() {
        return revenueLastThreeMonth;
    }

    public void setRevenueLastThreeMonth(List<Map<String, Object>> revenueLastThreeMonth) {
        this.revenueLastThreeMonth = revenueLastThreeMonth;
    }

    public List<Map<String, Object>> getLastFiveOrders() {
        return lastFiveOrders;
    }

    public void setLastFiveOrders(List<Map<String, Object>> lastFiveOrders) {
        this.lastFiveOrders = lastFiveOrders;
    }

    public boolean isCanEditLocation() {
        return canEditLocation;
    }

    public void setCanEditLocation(boolean canEditLocation) {
        this.canEditLocation = canEditLocation;
    }

}
