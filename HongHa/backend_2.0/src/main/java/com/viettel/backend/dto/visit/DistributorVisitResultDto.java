package com.viettel.backend.dto.visit;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;

public class DistributorVisitResultDto extends CategorySimpleDto {

    private static final long serialVersionUID = 3641902226456577012L;

    private VisitResultDto visitResult;
    
    public DistributorVisitResultDto(I_Category distributor, VisitResultDto visitResult) {
        super(distributor);

        this.visitResult = visitResult;
    }

    public DistributorVisitResultDto(I_CategoryEmbed distributor, VisitResultDto visitResult) {
        super(distributor);

        this.visitResult = visitResult;
    }

    public VisitResultDto getVisitResult() {
        return visitResult;
    }
    
    public void setVisitResult(VisitResultDto visitResult) {
        this.visitResult = visitResult;
    }

}
