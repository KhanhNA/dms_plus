package com.viettel.backend.dto.visit;

import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_UserEmbed;

public class SalesmanVisitResultDto extends UserSimpleDto {

    private static final long serialVersionUID = 3641902226456577012L;

    private VisitResultDto visitResult;

    public SalesmanVisitResultDto(I_User user, VisitResultDto visitResult) {
        super(user);

        this.visitResult = visitResult;
    }

    public SalesmanVisitResultDto(I_UserEmbed user, VisitResultDto visitResult) {
        super(user);

        this.visitResult = visitResult;
    }

    public VisitResultDto getVisitResult() {
        return visitResult;
    }

    public void setVisitResult(VisitResultDto visitResult) {
        this.visitResult = visitResult;
    }

}
