package com.viettel.backend.dto.visit;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.viettel.repository.common.domain.data.I_SurveyAnswerContent;

public class SurveyAnswerDto implements Serializable {

    private static final long serialVersionUID = 3227127774937212395L;

    private String surveyId;
    private Set<String> options;

    public SurveyAnswerDto() {
        super();
    }

    public SurveyAnswerDto(I_SurveyAnswerContent surveyAnswer) {
        super();

        this.surveyId = surveyAnswer.getSurvey().getId();
        if (surveyAnswer.getOptions() != null) {
            this.options = new HashSet<String>(surveyAnswer.getOptions().size());
            for (Integer option : surveyAnswer.getOptions()) {
                this.options.add(String.valueOf(option));
            }
        }
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public Set<String> getOptions() {
        return options;
    }

    public void setOptions(Set<String> options) {
        this.options = options;
    }

}
