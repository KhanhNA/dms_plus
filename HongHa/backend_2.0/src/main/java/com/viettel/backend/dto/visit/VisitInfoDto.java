package com.viettel.backend.dto.visit;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.dto.survey.SurveyDto;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.repository.common.domain.data.I_SurveyAnswerContent;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.entity.Location;

public class VisitInfoDto extends OrderDto {

    private static final long serialVersionUID = -4244621688895579808L;

    private boolean hasOrder;

    private int visitStatus;
    private boolean closed;
    private String photo;

    private String startTime;
    private String endTime;

    private long duration;
    private boolean errorDuration;

    private int locationStatus;
    private Double distance;

    // FEEDBACK
    private List<String> feedbacks;

    // SURVEY ANSWER
    private List<SurveyDto> surveys;
    private List<SurveyAnswerDto> surveyAnswers;

    private Location location;
    private Location customerLocation;
    
    /** after this need set surveys and exhibition use in this visit */
    public VisitInfoDto(I_Visit visit, I_ProductPhotoFactory productPhotoFactory) {
        super(visit, productPhotoFactory);

        this.hasOrder = visit.isHasOrder();
        this.visitStatus = visit.getVisitStatus();
        this.closed = visit.isClosed();
        this.photo = visit.getPhoto();
        this.locationStatus = visit.getLocationStatus();
        this.distance = visit.getDistance();
        this.feedbacks = visit.getFeedbacks();
        this.startTime = visit.getStartTime() != null ? visit.getStartTime().getIsoTime() : null;
        this.endTime = visit.getEndTime() != null ? visit.getEndTime().getIsoTime() : null;
        this.duration = visit.getDuration();
        this.errorDuration = visit.isErrorDuration();
        this.location = visit.getLocation();
        this.customerLocation = visit.getCustomerLocation();

        if (visit.getSurveyAnswers() != null) {
            this.surveyAnswers = new ArrayList<>(visit.getSurveyAnswers().size());
            for (I_SurveyAnswerContent surveyAnswer : visit.getSurveyAnswers()) {
                this.surveyAnswers.add(new SurveyAnswerDto(surveyAnswer));
            }
        }
    }

    public boolean isHasOrder() {
        return hasOrder;
    }

    public void setHasOrder(boolean hasOrder) {
        this.hasOrder = hasOrder;
    }

    public int getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(int visitStatus) {
        this.visitStatus = visitStatus;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getPhoto() {
        return photo;
    }
    
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getLocationStatus() {
        return locationStatus;
    }

    public void setLocationStatus(int locationStatus) {
        this.locationStatus = locationStatus;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public List<String> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<String> feedbacks) {
        this.feedbacks = feedbacks;
    }

    public List<SurveyDto> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<SurveyDto> surveys) {
        this.surveys = surveys;
    }

    public void addSurvey(SurveyDto survey) {
        if (this.surveys == null) {
            this.surveys = new LinkedList<SurveyDto>();
        }

        this.surveys.add(survey);
    }

    public List<SurveyAnswerDto> getSurveyAnswers() {
        return surveyAnswers;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public long getDuration() {
        return duration;
    }

    public boolean isErrorDuration() {
        return errorDuration;
    }

    public Location getLocation() {
        return location;
    }

    public Location getCustomerLocation() {
        return customerLocation;
    }
    
    public UserSimpleDto getSalesman() {
        return super.getCreatedBy();
    }

}
