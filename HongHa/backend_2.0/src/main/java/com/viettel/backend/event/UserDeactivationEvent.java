package com.viettel.backend.event;

import com.viettel.repository.common.domain.user.I_User;

import reactor.bus.Event;

public class UserDeactivationEvent extends Event<I_User> {

    private static final long serialVersionUID = 2847953405629287495L;
    
    public static final String EVENT_NAME = "UserDeactivationEvent";

    public UserDeactivationEvent(I_User data) {
        super(data);
    }
    
    public I_User getUser() {
        return getData();
    }

}
