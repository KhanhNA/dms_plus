package com.viettel.backend.oauth2.core;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.viettel.backend.dto.system.UserLoginDto;
import com.viettel.backend.service.core.system.AuthenticationService;

/**
 * Provider for users of system
 * 
 * @author thanh
 */
public class UserAuthenticationProvider implements AuthenticationProvider {

    private static final String ROLE_PREFIX = "ROLE_";

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();
        
        UserLoginDto userLoginDto = authenticationService.authenticate(username, password);
        
        List<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
        
        // Always have role USER
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        grantedAuthorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + userLoginDto.getRole()));
        
        UserAuthenticationToken auth = new UserAuthenticationToken(username, authentication.getCredentials(),
                    grantedAuthorities);

        UserLogin userLogin = new UserLogin(userLoginDto.getClientId(), userLoginDto.getClientCode(),
                userLoginDto.getClientName(), userLoginDto.getUserId(), userLoginDto.getUsername(),
                userLoginDto.getRole(), userLoginDto.getModules());

        auth.setUserLogin(userLogin);

        return auth;
    }

    @Override
    public boolean supports(Class<?> arg0) {
        return true;
    }

}