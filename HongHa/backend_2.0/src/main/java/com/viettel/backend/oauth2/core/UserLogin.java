package com.viettel.backend.oauth2.core;

import java.io.Serializable;
import java.util.Set;

import org.springframework.util.StringUtils;

public class UserLogin implements Serializable {

    private static final long serialVersionUID = -7942635748301089503L;

    private String clientId;
    private String clientCode;
    private String clientName;
    private String userId;
    private String username;
    private String role;
    private Set<String> modules;

    public UserLogin(String clientId, String clientCode, String clientName, String userId, String username,
            String role, Set<String> modules) {
        super();

        this.clientId = clientId;
        this.clientCode = clientCode;
        this.clientName = clientName;
        this.userId = userId;
        this.username = username;
        this.role = role;
        this.modules = modules;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isRole(String role) {
        if (this.role == null || StringUtils.isEmpty(role)) {
            return false;
        }

        role = role.toUpperCase();
        return this.role.contains(role);
    }
    
    public boolean hasModule(String module) {
        return modules != null ? modules.contains(module) : false;
    }

    public Set<String> getModules() {
        return modules;
    }

    public void setModules(Set<String> modules) {
        this.modules = modules;
    }
    
}
