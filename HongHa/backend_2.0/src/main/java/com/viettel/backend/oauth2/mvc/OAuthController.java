package com.viettel.backend.oauth2.mvc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.viettel.backend.dto.system.ChangePasswordDto;
import com.viettel.backend.dto.system.UserInfoDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.SecurityContextHelper;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.restful.RestError;
import com.viettel.backend.service.core.system.AuthenticationService;

@Controller
public class OAuthController {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ConsumerTokenServices tokenServices;

	@Autowired
    private AuthenticationService authenticationService;

	@RequestMapping(value = "/oauth/revoke/{token}", method = RequestMethod.DELETE)
	public ResponseEntity<?> revokeToken(@PathVariable String token) {
		if (tokenServices.revokeToken(token)) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping("/oauth/userinfo")
	public ResponseEntity<?> getUserInfo() {
		UserLogin userLogin = SecurityContextHelper.getCurrentUser();
		try {
		    UserInfoDto userLoginDto = authenticationService.getUserInfoDto(userLogin);
	        return new ResponseEntity<UserInfoDto>(userLoginDto, HttpStatus.OK);
		} catch (UnsupportedOperationException ex) {
			logger.debug("Fail to load user info", ex);
			
		    RestError error = new RestError("OAuthException", HttpStatus.UNAUTHORIZED.value(), "user.not.found");
	        Envelope response = new Envelope(error);
	        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
		}
	}
	
	@RequestMapping(value = "/oauth/password", method = RequestMethod.PUT)
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDto dto) {
        try {
            authenticationService.changePassword(SecurityContextHelper.getCurrentUser(), dto);
            return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
        } catch (BusinessException ex) {
        	logger.debug("Fail to update user's password", ex);
        	
            RestError error = new RestError("BusinessException", HttpStatus.BAD_REQUEST.value(), ex.getCode());
            Envelope response = new Envelope(error);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
