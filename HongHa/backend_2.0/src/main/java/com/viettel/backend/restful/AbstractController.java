package com.viettel.backend.restful;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.oauth2.core.SecurityContextHelper;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public abstract class AbstractController {

    protected final UserLogin getUserLogin() {
        return SecurityContextHelper.getCurrentUser();
    }

    protected final PageSizeRequest getPageRequest(Integer page, Integer size) {
        if (page == null || page < 0) {
            page = 1;
        }

        if (size == null || size < 1) {
            size = 10;
        }

        return new PageSizeRequest(page - 1, size);
    }
    
    protected ResponseEntity<InputStreamResource> export(ExportDto exportDto) {
        InputStreamResource response = new InputStreamResource(exportDto.getInputStream());

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + exportDto.getFileName() + "\"");

        return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
    }
    
}
