package com.viettel.backend.restful;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.viettel.backend.service.core.common.CategoryReadonlyService;

public abstract class ReadonlyCategoryController extends AbstractController {

    @SuppressWarnings("rawtypes")
    protected abstract CategoryReadonlyService getReadonlyCategoryService();

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public final ResponseEntity<?> getAll(@RequestParam(required = false) String distributorId) {
        if (getReadonlyCategoryService() == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        
        return new Envelope(getReadonlyCategoryService().getAll(getUserLogin(), distributorId))
                .toResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> detail(@PathVariable String id) {
        if (getReadonlyCategoryService() == null) {
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        
        return new Envelope(getReadonlyCategoryService().getById(getUserLogin(), id))
                .toResponseEntity(HttpStatus.OK);
    }

}
