package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.checkin.CheckInDto;
import com.viettel.backend.dto.checkin.CheckInListDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.checkin.CheckInMonitoringService;

@RestController(value = "adminCheckInController")
@RequestMapping(value = "/admin/check-in")
public class CheckInController extends AbstractController {

    @Autowired
    private CheckInMonitoringService checkInMonitoringService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getList(@RequestParam(required = true) String createdById,
            @RequestParam(required = true) String fromDate, @RequestParam(required = true) String toDate,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListDto<CheckInListDto> dtos = checkInMonitoringService.getList(getUserLogin(), createdById, fromDate, toDate,
                getPageRequest(page, size));
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getById(@PathVariable String id) {
        CheckInDto dto = checkInMonitoringService.getById(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/by-date", method = RequestMethod.GET)
    public ResponseEntity<?> getCheckInByDate(@RequestParam(required = true) String createdById,
            @RequestParam(required = true) String date) {
        ListDto<CheckInDto> dtos = checkInMonitoringService.getCheckInByDate(getUserLogin(), createdById, date);
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

}
