package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerCreateDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.core.common.CategoryEditableService;
import com.viettel.backend.service.core.customer.CustomerApprovalService;
import com.viettel.backend.service.core.customer.CustomerEditableService;
import com.viettel.backend.service.core.customer.CustomerReadonlyService;

@RestController(value = "adminCustomerController")
@RequestMapping(value = "/admin/customer")
public class CustomerController extends EditableCategoryController<CustomerListDto, CustomerDto, CustomerCreateDto> {

	@Autowired
	private CustomerEditableService editableCustomerService;

	@Autowired
	private CustomerApprovalService customerApprovalService;

	@Autowired
	private CustomerReadonlyService customerService;

	@Override
	protected CategoryEditableService<CustomerListDto, CustomerDto, CustomerCreateDto> getEditableService() {
		return editableCustomerService;
	}

	@RequestMapping(value = "/pending", method = RequestMethod.GET)
	public final ResponseEntity<?> getPendingCustomers(@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size) {
		ListDto<CustomerListDto> dtos = customerApprovalService.getPendingCustomers(getUserLogin(),
				getPageRequest(page, size));
		return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/pending/{id}", method = RequestMethod.GET)
	public final ResponseEntity<?> getCustomerById(@PathVariable String id) {
		CustomerListDto dto = customerApprovalService.getCustomerById(getUserLogin(), id);
		return new Envelope(dto).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/pending/{id}/approve", method = RequestMethod.PUT)
	public final ResponseEntity<?> approve(@PathVariable String id) {
		customerApprovalService.approve(getUserLogin(), id);
		return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/pending_multi/{id}/approve", method = RequestMethod.PUT)
	public final ResponseEntity<?> approveMulti(@PathVariable String id) {
		customerApprovalService.approveMulti(getUserLogin(), id);
		return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/pending/{id}/reject", method = RequestMethod.PUT)
	public final ResponseEntity<?> reject(@PathVariable String id) {
		customerApprovalService.reject(getUserLogin(), id);
		return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/pending_multi/{id}/reject", method = RequestMethod.PUT)
	public final ResponseEntity<?> rejectMulti(@PathVariable String id) {
		customerApprovalService.rejectMulti(getUserLogin(), id);
		return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/for-report", method = RequestMethod.GET)
	public final ResponseEntity<?> getCustomerForReport(@RequestParam(required = false) String distributorId,
			@RequestParam(value = "q", required = false) String search, @RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size) {
		ListDto<CustomerListDto> dtos = customerService.getCustomerForReport(getUserLogin(), distributorId, search,
				getPageRequest(page, size));
		return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
	}

}
