package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.customer.CustomerTypeEditableService;
import com.viettel.backend.service.core.customer.CustomerTypeReadonlyService;
import com.viettel.backend.service.core.common.CategoryEditableService;

@RestController(value = "adminCustomerTypeController")
@RequestMapping(value = "/admin/customer-type")
public class CustomerTypeController extends
        EditableCategoryController<CategoryDto, CategoryDto, CategoryCreateDto> {

    @Autowired
    private CustomerTypeEditableService editableCustomerTypeService;

    @Autowired
    private CustomerTypeReadonlyService customerTypeService;

    @Override
    protected CategoryEditableService<CategoryDto, CategoryDto, CategoryCreateDto> getEditableService() {
        return editableCustomerTypeService;
    }

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return customerTypeService;
    }

}
