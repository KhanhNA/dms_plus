package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.distributor.DistributorCreateDto;
import com.viettel.backend.dto.distributor.DistributorDto;
import com.viettel.backend.dto.distributor.DistributorListDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.distributor.DistributorReadonlyService;
import com.viettel.backend.service.core.distributor.DistributorEditableService;
import com.viettel.backend.service.core.common.CategoryEditableService;

@RestController(value = "adminDistributorController")
@RequestMapping(value = "/admin/distributor")
public class DistributorController extends
        EditableCategoryController<DistributorListDto, DistributorDto, DistributorCreateDto> {

    @Autowired
    private DistributorEditableService editableDistributorService;
    
    @Autowired
    private DistributorReadonlyService distributorService;

    @Override
    protected CategoryEditableService<DistributorListDto, DistributorDto, DistributorCreateDto> getEditableService() {
        return editableDistributorService;
    }

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return distributorService;
    }
    
    @Override
    protected boolean canSetActive() {
        return false;
    }

}