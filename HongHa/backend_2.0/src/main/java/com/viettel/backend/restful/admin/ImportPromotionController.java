package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.promotion.PromotionImportService;

@RestController(value = "ImportPromotionController")
@RequestMapping(value = "/admin/import/promotion")
public class ImportPromotionController extends AbstractController {

	@Autowired
	private PromotionImportService importPromotionService;

	@RequestMapping(value = "/template", method = RequestMethod.GET)
	public ResponseEntity<?> getImportPromotionTemplate(@RequestParam String lang) {
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		header.set("Content-Disposition", "attachment; filename=\"Promotion.xlsx\"");
		ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(
				importPromotionService.getImportPromotionTemplate(getUserLogin(), lang), header, HttpStatus.OK);
		return result;
	}

	@RequestMapping(value = "/verify", method = RequestMethod.GET)
	public ResponseEntity<?> verify(@RequestParam(required = true) String fileId) {
		ImportConfirmDto dto = importPromotionService.verify(getUserLogin(), fileId);
		return new Envelope(dto).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/import", method = RequestMethod.POST)
	public ResponseEntity<?> importCustomer(@RequestParam(required = true) String fileId) {
		ImportResultDto dto = importPromotionService.importPromotion(getUserLogin(), fileId);
		return new Envelope(dto).toResponseEntity(HttpStatus.OK);
	}

}
