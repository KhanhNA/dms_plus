package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.product.ProductCategoryEditableService;
import com.viettel.backend.service.core.product.ProductCategoryReadonlyService;
import com.viettel.backend.service.core.common.CategoryEditableService;

@RestController(value = "adminProductCategoryController")
@RequestMapping(value = "/admin/product-category")
public class ProductCategoryController extends
        EditableCategoryController<CategoryDto, CategoryDto, CategoryCreateDto> {

    @Autowired
    private ProductCategoryEditableService editableProductCategoryService;

    @Autowired
    private ProductCategoryReadonlyService productCategoryService;

    @Override
    protected CategoryEditableService<CategoryDto, CategoryDto, CategoryCreateDto> getEditableService() {
        return editableProductCategoryService;
    }

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return productCategoryService;
    }

}
