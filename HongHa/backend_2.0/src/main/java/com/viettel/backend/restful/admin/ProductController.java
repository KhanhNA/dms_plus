package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.product.ProductCreateDto;
import com.viettel.backend.dto.product.ProductDto;
import com.viettel.backend.dto.product.ProductSimpleDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.product.ProductEditableService;
import com.viettel.backend.service.core.product.ProductReadonlyService;
import com.viettel.backend.service.core.common.CategoryEditableService;

@RestController(value = "adminProductController")
@RequestMapping(value = "/admin/product")
public class ProductController extends EditableCategoryController<ProductDto, ProductDto, ProductCreateDto> {

    @Autowired
    private ProductEditableService editableProductService;

    @Autowired
    private ProductReadonlyService productService;

    @Override
    protected CategoryEditableService<ProductDto, ProductDto, ProductCreateDto> getEditableService() {
        return editableProductService;
    }

    @Override
    protected CategoryReadonlyService<ProductSimpleDto> getReadonlyCategoryService() {
        return productService;
    }

}
