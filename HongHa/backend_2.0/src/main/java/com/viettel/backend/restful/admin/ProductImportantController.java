package com.viettel.backend.restful.admin;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.product.ProductImportantCreateDto;
import com.viettel.backend.dto.product.ProductImportantDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.core.product.ProductImportantService;

@RestController(value = "adminProductImportantController")
@RequestMapping(value = "/admin/product-important")
public class ProductImportantController extends AbstractController {
	@Autowired
	ProductImportantService productImportantService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getList() throws IOException {
		List<ProductImportantDto> productImportantDtoList =  productImportantService.getProductImportant(getUserLogin());
		ListDto<ProductImportantDto> dtos = new ListDto<ProductImportantDto>(productImportantDtoList);
		return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "", method = RequestMethod.PUT)
	public ResponseEntity<?> save(@RequestBody ProductImportantCreateDto createDto) throws IOException {
		productImportantService.saveProductImportant(getUserLogin(), createDto);
		return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
	}
}