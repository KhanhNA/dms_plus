package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.promotion.PromotionCreateDto;
import com.viettel.backend.dto.promotion.PromotionDto;
import com.viettel.backend.dto.promotion.PromotionListDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryEditableService;
import com.viettel.backend.service.promotion.PromotionEditableService;

@RestController(value = "adminPromotionController")
@RequestMapping(value = "/admin/promotion")
public class PromotionController
        extends EditableCategoryController<PromotionListDto, PromotionDto, PromotionCreateDto> {

    @Autowired
    private PromotionEditableService editablePromotionService;

    @Override
    protected CategoryEditableService<PromotionListDto, PromotionDto, PromotionCreateDto> getEditableService() {
        return editablePromotionService;
    }

    protected boolean canSetActive() {
        return false;
    }

}
