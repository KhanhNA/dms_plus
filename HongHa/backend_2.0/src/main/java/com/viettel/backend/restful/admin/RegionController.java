package com.viettel.backend.restful.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategorySelectionDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.region.RegionCreateDto;
import com.viettel.backend.dto.region.RegionDto;
import com.viettel.backend.dto.region.RegionHierarchyDto;
import com.viettel.backend.dto.region.RegionListDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.core.common.CategoryEditableService;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.region.RegionEditableService;
import com.viettel.backend.service.core.region.RegionReadonlyService;

@RestController(value = "adminRegionController")
@RequestMapping(value = "/admin/region")
public class RegionController  extends EditableCategoryController<RegionListDto, RegionDto, RegionCreateDto>{
	
	@Autowired 
	RegionEditableService regionEditableService;
	
	@Autowired
	RegionReadonlyService regionService;
	
	@Override
	protected CategoryEditableService<RegionListDto, RegionDto, RegionCreateDto> getEditableService() {
		return regionEditableService;
	}
	
	@Override
	protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
		return regionService;
	}
	
	@RequestMapping(value = "/hierarchy", method = RequestMethod.GET)
    public ResponseEntity<?> requestRegionHierarchy(){
		ListDto<RegionHierarchyDto> tree = regionService.getHierarchy(getUserLogin());
		return new Envelope(tree).toResponseEntity(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}/distributors", method = RequestMethod.GET)
    public final ResponseEntity<?> getDistributorOfObserver(@PathVariable String id) {
        ListDto<CategorySelectionDto> dtos = regionEditableService.getDistributorOfRegion(getUserLogin(), id);
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/distributors", method = RequestMethod.PUT)
    public final ResponseEntity<?> updateDistributorSetForObserver(@PathVariable String id,
            @RequestBody ListDto<String> listDto) {
        List<String> distributorIds = listDto == null ? null : listDto.getList();
        regionEditableService.updateDistributorsForRegion(getUserLogin(), id, distributorIds);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }
    
}
