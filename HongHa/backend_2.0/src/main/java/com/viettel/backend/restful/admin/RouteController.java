package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.schedule.RouteCreateDto;
import com.viettel.backend.dto.schedule.RouteDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.schedule.RouteEditableService;
import com.viettel.backend.service.core.schedule.RouteReadonlyService;
import com.viettel.backend.service.core.common.CategoryEditableService;

@RestController(value = "adminRouteController")
@RequestMapping(value = "/admin/route")
public class RouteController extends
        EditableCategoryController<RouteDto, RouteDto, RouteCreateDto> {

    @Autowired
    private RouteEditableService editableRouteService;

    @Autowired
    private RouteReadonlyService routeService;

    @Override
    protected CategoryEditableService<RouteDto, RouteDto, RouteCreateDto> getEditableService() {
        return editableRouteService;
    }

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return routeService;
    }

}
