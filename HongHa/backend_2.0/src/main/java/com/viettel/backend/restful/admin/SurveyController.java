package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.survey.SurveyCreateDto;
import com.viettel.backend.dto.survey.SurveyDto;
import com.viettel.backend.dto.survey.SurveyListDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryEditableService;
import com.viettel.backend.service.survey.SurveyEditableService;

@RestController(value = "adminSurveyController")
@RequestMapping(value = "/admin/survey")
public class SurveyController extends EditableCategoryController<SurveyListDto, SurveyDto, SurveyCreateDto> {

    @Autowired
    private SurveyEditableService editableSurveyService;

    @Override
    protected CategoryEditableService<SurveyListDto, SurveyDto, SurveyCreateDto> getEditableService() {
        return editableSurveyService;
    }
    
    @Override
    protected boolean canSetActive() {
        return false;
    }

}
