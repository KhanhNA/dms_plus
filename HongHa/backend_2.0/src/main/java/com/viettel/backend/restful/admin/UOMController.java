package com.viettel.backend.restful.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.product.UOMEditableService;
import com.viettel.backend.service.core.product.UOMReadonlyService;
import com.viettel.backend.service.core.common.CategoryEditableService;

@RestController(value = "adminUOMController")
@RequestMapping(value = "/admin/uom")
public class UOMController extends
        EditableCategoryController<CategoryDto, CategoryDto, CategoryCreateDto> {

    @Autowired
    private UOMEditableService editableUOMService;
    
    @Autowired
    private UOMReadonlyService uomService;

    @Override
    protected CategoryEditableService<CategoryDto, CategoryDto, CategoryCreateDto> getEditableService() {
        return editableUOMService;
    }
    
    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return uomService;
    }

}
