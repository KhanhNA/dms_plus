package com.viettel.backend.restful.advancesupervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.checkin.CheckInCreateDto;
import com.viettel.backend.dto.checkin.CheckInDto;
import com.viettel.backend.dto.checkin.CheckInListDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.checkin.CheckInCreatingService;
import com.viettel.backend.service.checkin.CheckInMonitoringService;

@RestController(value = "advanceSupervisorCheckInController")
@RequestMapping(value = "/advance_supervisor/check-in")
public class CheckInController extends AbstractController {

	@Autowired
	private CheckInMonitoringService checkInMonitoringService;

	@Autowired
	private CheckInCreatingService checkInCreatingService;

	// MANAGER CHECK IN
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getList(@RequestParam(required = true) String createdById,
			@RequestParam(required = true) String fromDate, @RequestParam(required = true) String toDate,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
		ListDto<CheckInListDto> dtos = checkInMonitoringService.getList(getUserLogin(), createdById, fromDate, toDate,
				getPageRequest(page, size));
		return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable String id) {
		CheckInDto dto = checkInMonitoringService.getById(getUserLogin(), id);
		return new Envelope(dto).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/by-date", method = RequestMethod.GET)
	public ResponseEntity<?> getCheckInByDate(@RequestParam(required = true) String createdById,
			@RequestParam(required = true) String date) {
		ListDto<CheckInDto> dtos = checkInMonitoringService.getCheckInByDate(getUserLogin(), createdById, date);
		return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
	}

	// CHECK IN
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody CheckInCreateDto createDto) {
		IdDto id = checkInCreatingService.create(getUserLogin(), createDto);
		return new Envelope(id).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<?> getMyCheckIns(@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size) {
		ListDto<CheckInListDto> dtos = checkInCreatingService.getMyCheckIns(getUserLogin(), getPageRequest(page, size));
		return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getMyCheckInById(@PathVariable String id) {
		CheckInDto dto = checkInCreatingService.getMyCheckInById(getUserLogin(), id);
		return new Envelope(dto).toResponseEntity(HttpStatus.OK);
	}

}
