package com.viettel.backend.restful.advancesupervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.restful.ReadonlyCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.customer.CustomerTypeReadonlyService;

@RestController(value = "advanceSupervisorCustomerTypeController")
@RequestMapping(value = "/advance_supervisor/customer-type")
public class CustomerTypeController extends ReadonlyCategoryController {

    @Autowired
    private CustomerTypeReadonlyService customerTypeService;

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return customerTypeService;
    }

}
