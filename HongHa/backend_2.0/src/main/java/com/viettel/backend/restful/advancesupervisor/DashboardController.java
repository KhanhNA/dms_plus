package com.viettel.backend.restful.advancesupervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.dashboard.WebDashboardDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.core.dashboard.WebDashboardRegionService;


@RestController(value = "advanceSupervisorDashboardController")
@RequestMapping(value = "/advance_supervisor/dashboard")
public class DashboardController extends AbstractController {

    @Autowired
    private WebDashboardRegionService dashboardRegionService;
    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public final ResponseEntity<?> getWebDashboard() {
        WebDashboardDto dto = dashboardRegionService.getWebDashboard(getUserLogin());
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
