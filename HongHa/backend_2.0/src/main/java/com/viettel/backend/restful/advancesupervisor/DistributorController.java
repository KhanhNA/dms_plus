package com.viettel.backend.restful.advancesupervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.ReadonlyCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.distributor.DistributorReadonlyService;

@RestController(value = "advanceSupervisorDistributorController")
@RequestMapping(value = "/advance_supervisor/distributor")
public class DistributorController extends ReadonlyCategoryController {

    @Autowired
    private DistributorReadonlyService distributorService;

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return distributorService;
    }
    
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getDistributorOfRegion(@RequestParam(value = "regionId", required = true)  String regionId) {
		ListDto<CategorySimpleDto> dtos = distributorService.getDistributorOfRegion(getUserLogin(), regionId);
		return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
	}
}