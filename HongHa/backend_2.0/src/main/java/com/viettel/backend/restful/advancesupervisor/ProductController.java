package com.viettel.backend.restful.advancesupervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.product.ProductSimpleDto;
import com.viettel.backend.restful.ReadonlyCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.product.ProductReadonlyService;

@RestController(value = "advanceSupervisorProductController")
@RequestMapping(value = "/advance_supervisor/product")
public class ProductController extends ReadonlyCategoryController {

	@Autowired
	private ProductReadonlyService productService;

	@Override
	protected CategoryReadonlyService<ProductSimpleDto> getReadonlyCategoryService() {
		return productService;
	}

}
