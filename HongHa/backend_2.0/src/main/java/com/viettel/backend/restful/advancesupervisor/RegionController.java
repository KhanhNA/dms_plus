package com.viettel.backend.restful.advancesupervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.ReadonlyCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.region.RegionReadonlyService;

@RestController(value = "advanceSupervisorRegionController")
@RequestMapping(value = "/advance_supervisor/region")
public class RegionController extends ReadonlyCategoryController {

	@Autowired
	RegionReadonlyService regionService;

	@Override
	protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
		return regionService;
	}

	@RequestMapping(value = "/all-region", method = RequestMethod.GET)
	public ResponseEntity<?> getAllAccessibleRegions() {
		ListDto<CategorySimpleDto> regions = regionService.getAllRegions(getUserLogin());
		return new Envelope(regions).toResponseEntity(HttpStatus.OK);
	}
}
