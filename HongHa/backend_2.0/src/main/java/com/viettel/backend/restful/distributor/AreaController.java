package com.viettel.backend.restful.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.restful.EditableCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.customer.AreaEditableService;
import com.viettel.backend.service.core.customer.AreaReadonlyService;
import com.viettel.backend.service.core.common.CategoryEditableService;

@RestController(value = "distributorAreaController")
@RequestMapping(value = "/distributor/area")
public class AreaController extends EditableCategoryController<CategoryDto, CategoryDto, CategoryCreateDto> {

    @Autowired
    private AreaEditableService editableAreaService;

    @Autowired
    private AreaReadonlyService areaService;

    @Override
    protected CategoryEditableService<CategoryDto, CategoryDto, CategoryCreateDto> getEditableService() {
        return editableAreaService;
    }

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return areaService;
    }

}
