package com.viettel.backend.restful.distributor;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.report.ReportDistributorListIdDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.service.core.order.OrderExportService;
import com.viettel.backend.service.core.product.ProductImportantService;
import com.viettel.backend.service.core.visit.VisitExportService;
import com.viettel.backend.service.exchangereturn.ExchangeReturnExportService;
import com.viettel.backend.service.inventory.InventoryExportService;

@RestController(value = "distributorExportController")
@RequestMapping(value = "/distributor/export")
public class ExportController extends AbstractController {

    @Autowired
    private OrderExportService orderExportService;

    @Autowired
    private VisitExportService visitExportService;

    @Autowired
    private ExchangeReturnExportService exchangeReturnExportService;

    @Autowired
    private InventoryExportService inventoryExportService;
    
    @Autowired
	private ProductImportantService productImportantService;

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public ResponseEntity<?> exportOrder(@RequestParam(required = false) String salesmanId,
            @RequestParam(required = false) String customerId, @RequestParam String fromDate,
            @RequestParam String toDate, @RequestParam String lang) throws IOException {
        InputStream inputStream = orderExportService.exportOrder(getUserLogin(), null, salesmanId, customerId, fromDate,
                toDate, lang);
        InputStreamResource response = new InputStreamResource(inputStream);

        String filename = "OrderList_" + fromDate + "_" + toDate + ".xlsx";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
    }

    @RequestMapping(value = "/order/detail", method = RequestMethod.GET)
    public ResponseEntity<?> exportOrderDetail(@RequestParam(required = false) String salesmanId,
            @RequestParam(required = false) String customerId, @RequestParam String fromDate,
            @RequestParam String toDate, @RequestParam String lang) throws IOException {
        InputStream inputStream = orderExportService.exportOrderDetail(getUserLogin(), null, salesmanId, customerId,
                fromDate, toDate, lang);
        InputStreamResource response = new InputStreamResource(inputStream);

        String filename = "OrderDetailList_" + fromDate + "_" + toDate + ".xlsx";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
    }

    @RequestMapping(value = "/visit", method = RequestMethod.GET)
    public ResponseEntity<?> exportVisit(@RequestParam(required = false) String salesmanId,
            @RequestParam(required = false) String customerId, @RequestParam String fromDate,
            @RequestParam String toDate, @RequestParam String lang) throws IOException {
        InputStream inputStream = visitExportService.exportVisit(getUserLogin(), null, salesmanId, customerId, fromDate,
                toDate, lang);
        InputStreamResource response = new InputStreamResource(inputStream);

        String filename = "VisitList_" + fromDate + "_" + toDate + ".xlsx";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
    }

    @RequestMapping(value = "/feedback", method = RequestMethod.GET)
    public ResponseEntity<?> exportFeedback(@RequestParam String fromDate, @RequestParam String toDate,
            @RequestParam String lang) throws IOException {
        InputStream inputStream = visitExportService.exportFeedback(getUserLogin(), null, fromDate, toDate, lang);
        InputStreamResource response = new InputStreamResource(inputStream);

        String filename = "FeedbackList_" + fromDate + "_" + toDate + ".xlsx";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
    }

    @RequestMapping(value = "/exchange-return", method = RequestMethod.GET)
    public ResponseEntity<?> exportExchangeReturn(@RequestParam String fromDate, @RequestParam String toDate,
            @RequestParam String lang) throws IOException {
        InputStream inputStream = exchangeReturnExportService.export(getUserLogin(), null, fromDate, toDate, lang);
        InputStreamResource response = new InputStreamResource(inputStream);

        String filename = "ExchangeReturn_" + fromDate + "_" + toDate + ".xlsx";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
    }

    @RequestMapping(value = "/inventory-report/by-distributor", method = RequestMethod.PUT)
    public ResponseEntity<?> exportInventoryReportByDistributor(@RequestParam String lang,@RequestBody ReportDistributorListIdDto dto) throws IOException {
    	IdDto idDto = inventoryExportService.exportInventoryReportByDistributor(getUserLogin(), null,"all", lang, dto);
    	return new ResponseEntity<IdDto>(idDto,HttpStatus.OK);
    }

    @RequestMapping(value = "/inventory-report/by-product", method = RequestMethod.GET)
    public ResponseEntity<?> exportInventoryReportByProduct(@RequestParam String productId, @RequestParam String lang)
            throws IOException {
        ExportDto exportDto = inventoryExportService.exportInventoryReportByProduct(getUserLogin(), productId, lang);
        return export(exportDto);
    }

    @RequestMapping(value = "/product-important", method = RequestMethod.GET)
	public ResponseEntity<?> exportProductImportant(@RequestParam(required = false) String categoryId,
			@RequestParam String lang) throws IOException {
		InputStream inputStream = productImportantService.exportProductImportant(getUserLogin(), categoryId, lang);
		InputStreamResource response = new InputStreamResource(inputStream);

		String filename = "ProductImportant.xlsx";

		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");

		return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
	}
}
