package com.viettel.backend.restful.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.inventory.InventoryCreateDto;
import com.viettel.backend.dto.inventory.InventoryDto;
import com.viettel.backend.dto.inventory.InventoryListDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.inventory.InventoryEditableService;
import com.viettel.backend.service.inventory.InventoryImportService;

@RestController(value = "distributorInventoryController")
@RequestMapping(value = "/distributor/inventory")
public class InventoryController extends AbstractController {

    @Autowired
    private InventoryEditableService inventoryEditableService;

    @Autowired
    private InventoryImportService inventoryImportService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public final ResponseEntity<?> create(@RequestBody InventoryCreateDto dto) {
        IdDto domainId = inventoryEditableService.create(getUserLogin(), dto);
        return new Envelope(domainId).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public final ResponseEntity<?> update(@PathVariable String id, @RequestBody InventoryCreateDto dto) {
        IdDto domainId = inventoryEditableService.update(getUserLogin(), id, dto);
        return new Envelope(domainId).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public final ResponseEntity<?> detail(@PathVariable String id) {
        InventoryDto dto = inventoryEditableService.getById(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public final ResponseEntity<?> delete(@PathVariable String id) {
        inventoryEditableService.delete(getUserLogin(), id);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    // LIST
    @RequestMapping(value = "", method = RequestMethod.GET)
    public final ResponseEntity<?> getList(@RequestParam(required = false) String distributorId,
            @RequestParam(value = "q", required = false) String search, @RequestParam(required = false) Boolean draft,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size,
            @RequestParam(required = false) Boolean active) {
        ListDto<InventoryListDto> dtos = inventoryEditableService.getInventoryList(getUserLogin(),
                getPageRequest(page, size));
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/enable", method = RequestMethod.POST)
    public final ResponseEntity<?> createAndEnable(@RequestBody InventoryCreateDto dto) {
        IdDto domainId = inventoryEditableService.create(getUserLogin(), dto);
        inventoryEditableService.enable(getUserLogin(), domainId.getId(), false);
        return new Envelope(domainId).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/enable", method = RequestMethod.PUT)
    public final ResponseEntity<?> enable(@PathVariable String id,
            @RequestBody(required = false) InventoryCreateDto dto) {
        if (dto != null) {
            inventoryEditableService.update(getUserLogin(), id, dto);
        }
        inventoryEditableService.enable(getUserLogin(), id, dto == null);
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/template", method = RequestMethod.GET)
    public ResponseEntity<?> getImportCustomerTemplate(@RequestParam String lang) {
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"Inventory.xlsx\"");
        ResponseEntity<byte[]> result = new ResponseEntity<byte[]>(
                inventoryImportService.getImportTemplate(getUserLogin(), lang), header, HttpStatus.OK);
        return result;
    }

    @RequestMapping(value = "/verify", method = RequestMethod.GET)
    public ResponseEntity<?> verify(@RequestParam(required = true) String time,
            @RequestParam(required = true) String fileId) {
        ImportConfirmDto dto = inventoryImportService.verifyImport(getUserLogin(), time, fileId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public ResponseEntity<?> importInventory(@RequestParam(required = true) String time,
            @RequestParam(required = true) String fileId) {
        ImportResultDto dto = inventoryImportService.importInventory(getUserLogin(), time, fileId);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
