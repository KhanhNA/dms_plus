package com.viettel.backend.restful.distributor;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.restful.AbstractController;

@RestController(value = "distributorPriceListController")
@RequestMapping(value = "/distributor/price-list")
public class PriceListController extends AbstractController {

//    @Autowired
//    private DistributorPriceListService distributorPriceListService;
//
//    // SET
//    @RequestMapping(value = "", method = RequestMethod.PUT)
//    public ResponseEntity<?> savePriceList(@RequestBody @Valid DistributorPriceListCreateDto createDto) {
//        distributorPriceListService.savePriceList(getUserLogin(), createDto);
//        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK);
//    }
//
//    // GET
//    @RequestMapping(value = "", method = RequestMethod.GET)
//    public ResponseEntity<?> getPriceList() {
//        ListDto<DistributorPriceDto> list = distributorPriceListService.getPriceList(getUserLogin());
//        return new Envelope(list).toResponseEntity(HttpStatus.OK);
//    }

}
