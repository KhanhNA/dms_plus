package com.viettel.backend.restful.distributor;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.inventory.InventoryReportByDistributorListDto;
import com.viettel.backend.dto.inventory.InventoryReportByProductListDto;
import com.viettel.backend.dto.order.DistributorSalesResultDto;
import com.viettel.backend.dto.order.ProductSalesResultDto;
import com.viettel.backend.dto.order.SalesResultDailyDto;
import com.viettel.backend.dto.order.SalesmanSalesResultDto;
import com.viettel.backend.dto.report.ReportDistributorListIdDto;
import com.viettel.backend.dto.survey.SurveyListDto;
import com.viettel.backend.dto.survey.SurveyResultDto;
import com.viettel.backend.dto.visit.DistributorVisitResultDto;
import com.viettel.backend.dto.visit.SalesmanVisitResultDto;
import com.viettel.backend.dto.visit.VisitResultDailyDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.core.order.SalesReportService;
import com.viettel.backend.service.core.visit.VisitReportService;
import com.viettel.backend.service.inventory.InventoryReportService;
import com.viettel.backend.service.survey.SurveyReportService;

@RestController(value = "distributorReportController")
@RequestMapping(value = "/distributor/report")
public class ReportController extends AbstractController {

    @Autowired
    private SalesReportService salesReportService;

    @Autowired
    private VisitReportService visitReportService;

    @Autowired
    private SurveyReportService surveyReportService;

    @Autowired
    private InventoryReportService inventoryReportService;

    // SALES
    @RequestMapping(value = "/sales/daily", method = RequestMethod.GET)
    public final ResponseEntity<?> getSalesResultDaily(@RequestParam(required = true) String fromDate,
            @RequestParam(required = true) String toDate) {
        ListDto<SalesResultDailyDto> list = salesReportService.getSalesResultDaily(getUserLogin(), null,null, fromDate,
                toDate);
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/sales/by-distributor", method = RequestMethod.GET)
    public final ResponseEntity<?> getDistributorSalesResult(@RequestParam(required = true) String fromDate,
            @RequestParam(required = true) String toDate) {
        ListDto<DistributorSalesResultDto> list = salesReportService.getDistributorSalesResult(getUserLogin(), fromDate,
                toDate);
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/sales/by-product", method = RequestMethod.GET)
    public final ResponseEntity<?> getProductSalesResult(@RequestParam(required = false) String distributorId,
            @RequestParam(required = true) String fromDate, @RequestParam(required = true) String toDate,
            @RequestParam(required = false) String productCategoryId, @RequestParam(required = true) String isImportantProduct) {
        ListDto<ProductSalesResultDto> list = salesReportService.getProductSalesResult(getUserLogin(), null,distributorId,
                fromDate, toDate, productCategoryId, isImportantProduct);
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/sales/by-salesman", method = RequestMethod.GET)
    public final ResponseEntity<?> getSalesmanSalesResult(@RequestParam(required = true) String fromDate,
            @RequestParam(required = true) String toDate) {
        ListDto<SalesmanSalesResultDto> list = salesReportService.getSalesmanSalesResult(getUserLogin(), null, fromDate,
                toDate);
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    // VISIT
    @RequestMapping(value = "/visit/daily", method = RequestMethod.GET)
    public final ResponseEntity<?> getVisitResultDaily(@RequestParam(required = true) String fromDate,
            @RequestParam(required = true) String toDate) {
        ListDto<VisitResultDailyDto> list = visitReportService.getVisitResultDaily(getUserLogin(), null,null, fromDate,
                toDate);
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/visit/by-distributor", method = RequestMethod.GET)
    public final ResponseEntity<?> getDistributorVisitResult(@RequestParam(required = true) String fromDate,
            @RequestParam(required = true) String toDate) {
        ListDto<DistributorVisitResultDto> list = visitReportService.getDistributorVisitResult(getUserLogin(), fromDate,
                toDate);
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/visit/by-salesman", method = RequestMethod.GET)
    public final ResponseEntity<?> getSalesmanVisitResult(@RequestParam(required = true) String fromDate,
            @RequestParam(required = true) String toDate) {
        ListDto<SalesmanVisitResultDto> list = visitReportService.getSalesmanVisitResult(getUserLogin(), null, fromDate,
                toDate);
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    // SURVEY
    @RequestMapping(value = "/survey", method = RequestMethod.GET)
    public ResponseEntity<?> getSurveyReport(@RequestParam(required = false) String search,
            @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        ListDto<SurveyListDto> list = surveyReportService.getSurveys(getUserLogin(), search,
                getPageRequest(page, size));
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/survey/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getSurveyReport(@PathVariable String id) {
        SurveyResultDto dto = surveyReportService.getSurveyReport(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/survey/{id}/export", method = RequestMethod.GET)
    public ResponseEntity<?> exportSurveyResult(@PathVariable String id, @RequestParam String lang) throws IOException {
        InputStream inputStream = surveyReportService.exportSurveyReport(getUserLogin(), id, lang);
        InputStreamResource response = new InputStreamResource(inputStream);

        String filename = "SurveyAnswer.xlsx";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
    }

	@RequestMapping(value = "/inventory/by-distributor", method = RequestMethod.PUT)
	public final ResponseEntity<?> getInventoryReportByDistributorResult(
			@RequestParam(required = false) String way,@RequestBody ReportDistributorListIdDto body) {
		InventoryReportByDistributorListDto list = inventoryReportService
				.getInventoryReportByDistributor(getUserLogin(),null, way,body);
		return new Envelope(list).toResponseEntity(HttpStatus.OK);
	}

    @RequestMapping(value = "/inventory/by-product", method = RequestMethod.GET)
    public final ResponseEntity<?> getInventoryReportByProductResult(@RequestParam String productId) {
        InventoryReportByProductListDto list = inventoryReportService.getInventoryReportByProduct(getUserLogin(),
                productId);
        return new Envelope(list).toResponseEntity(HttpStatus.OK);
    }

}
