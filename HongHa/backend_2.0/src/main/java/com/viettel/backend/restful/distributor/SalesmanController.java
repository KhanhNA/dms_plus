package com.viettel.backend.restful.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.core.user.UserReadonlyService;

@RestController(value = "distributorSalesmanController")
@RequestMapping(value = "/distributor/salesman")
public class SalesmanController extends AbstractController {

    @Autowired
    private UserReadonlyService userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public final ResponseEntity<?> getSalesmen() {
        ListDto<UserSimpleDto> dtos = userService.getSalesmen(getUserLogin(), null);
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public final ResponseEntity<?> getSalesman(@RequestParam(required = true) String id) {
        UserSimpleDto dto = userService.getSalesman(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
