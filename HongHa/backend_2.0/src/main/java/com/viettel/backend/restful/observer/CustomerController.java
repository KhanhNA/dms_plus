package com.viettel.backend.restful.observer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.core.customer.CustomerReadonlyService;

@RestController(value = "observerCustomerController")
@RequestMapping(value = "/observer/customer")
public class CustomerController extends AbstractController {

    @Autowired
    private CustomerReadonlyService customerService;

    @RequestMapping(value = "/for-report", method = RequestMethod.GET)
    public final ResponseEntity<?> getCustomerForReport(@RequestParam(required = false) String distributorId,
            @RequestParam(value = "q", required = false) String search, @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size) {
        ListDto<CustomerListDto> dtos = customerService.getCustomerForReport(getUserLogin(), distributorId, search,
                getPageRequest(page, size));
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

}
