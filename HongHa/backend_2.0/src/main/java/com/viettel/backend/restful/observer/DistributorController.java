package com.viettel.backend.restful.observer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.restful.ReadonlyCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.distributor.DistributorReadonlyService;

@RestController(value = "observerDistributorController")
@RequestMapping(value = "/observer/distributor")
public class DistributorController extends ReadonlyCategoryController {

    @Autowired
    private DistributorReadonlyService distributorService;

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return distributorService;
    }

}