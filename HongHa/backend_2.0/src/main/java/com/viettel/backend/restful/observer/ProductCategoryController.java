package com.viettel.backend.restful.observer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.restful.ReadonlyCategoryController;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.backend.service.core.product.ProductCategoryReadonlyService;

@RestController(value = "observerProductCategoryController")
@RequestMapping(value = "/observer/product-category")
public class ProductCategoryController extends ReadonlyCategoryController {

    @Autowired
    private ProductCategoryReadonlyService productCategoryService;

    @Override
    protected CategoryReadonlyService<CategorySimpleDto> getReadonlyCategoryService() {
        return productCategoryService;
    }

}
