package com.viettel.backend.restful.superadmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.restful.Envelope;
import com.viettel.backend.restful.Meta;
import com.viettel.backend.service.core.system.CacheInitiatorService;
import com.viettel.backend.service.core.system.GenerateBigDataService;

@RestController(value="superAdminSystemController")
@RequestMapping(value = "/super-admin/system")
public class SystemController {

    @Autowired
    private CacheInitiatorService cacheInitiator;
    
    @Autowired
    private GenerateBigDataService generateBigData;
    
    @RequestMapping(value = "/reset-cache", method = RequestMethod.PUT)
    public ResponseEntity<?> resetCache() {
        cacheInitiator.resetCache();
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK); 
    }
    
    @RequestMapping(value = "/generate-big-data", method = RequestMethod.POST)
    public ResponseEntity<?> generateBigData() {
        generateBigData.generateBigData();
        return new Envelope(Meta.OK).toResponseEntity(HttpStatus.OK); 
    }
    
}
