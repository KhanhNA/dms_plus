package com.viettel.backend.restful.supervisor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.checkin.CheckInCreateDto;
import com.viettel.backend.dto.checkin.CheckInDto;
import com.viettel.backend.dto.checkin.CheckInListDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.checkin.CheckInCreatingService;

@RestController(value = "supervisorCheckInController")
@RequestMapping(value = "/supervisor/check-in")
public class CheckInController extends AbstractController {

    @Autowired
    private CheckInCreatingService checkInCreatingService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody CheckInCreateDto createDto) {
        IdDto id = checkInCreatingService.create(getUserLogin(), createDto);
        return new Envelope(id).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getMyCheckIns(@RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size) {
        ListDto<CheckInListDto> dtos = checkInCreatingService.getMyCheckIns(getUserLogin(), getPageRequest(page, size));
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getMyCheckInById(@PathVariable String id) {
        CheckInDto dto = checkInCreatingService.getMyCheckInById(getUserLogin(), id);
        return new Envelope(dto).toResponseEntity(HttpStatus.OK);
    }

}
