package com.viettel.backend.restful.supervisor;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.visit.VisitPhotoDto;
import com.viettel.backend.restful.AbstractController;
import com.viettel.backend.restful.Envelope;
import com.viettel.backend.service.core.visit.VisitPhotoService;

@RestController(value = "supervisorVisitPhotoController")
@RequestMapping(value = "/supervisor/visit-photo")
public class VisitPhotoController extends AbstractController {

    @Autowired
    private VisitPhotoService visitPhotoService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public final ResponseEntity<?> getVisitPhotos(@RequestParam(required = true) String salesmanId,
            @RequestParam(required = true) String fromDate, @RequestParam(required = true) String toDate) {
        ListDto<VisitPhotoDto> dtos = visitPhotoService.getVisitPhotos(getUserLogin(), salesmanId, fromDate, toDate);
        return new Envelope(dtos).toResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value = "/zip", method = RequestMethod.GET)
    public ResponseEntity<?> getVisitPhotosZip(@RequestParam(required = true) String salesmanId,
            @RequestParam(required = true) String fromDate, @RequestParam(required = true) String toDate) {
        InputStream inputStream = visitPhotoService.getVisitPhotosZip(getUserLogin(), salesmanId, fromDate, toDate);
        InputStreamResource response = new InputStreamResource(inputStream);

        String filename = "Photos_" + fromDate + "_" + toDate + ".zip";

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.set("Content-Disposition", "attachment; filename=\"" + filename + "\"");

        return new ResponseEntity<InputStreamResource>(response, header, HttpStatus.OK);
    }

}
