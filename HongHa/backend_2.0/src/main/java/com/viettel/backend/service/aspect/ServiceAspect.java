package com.viettel.backend.service.aspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.viettel.backend.oauth2.core.UserLogin;

@Aspect
@Component
public class ServiceAspect {
    
    private static final Logger logger = LoggerFactory.getLogger(ServiceAspect.class);

    @Pointcut(value = "execution(public * *(com.viettel.backend.oauth2.core.UserLogin,..))")
    public void anyPublicMethodWithUserLogin() { }

    @Pointcut(value = "(@within(com.viettel.backend.service.aspect.RolePermission) || @annotation(com.viettel.backend.service.aspect.RolePermission))")
    public void annotatedWithRolePermission() { }
    
    @Pointcut(value = "(@within(com.viettel.backend.service.aspect.RequireModule) || @annotation(com.viettel.backend.service.aspect.RequireModule))")
    public void annotatedWithRequireModule() { }

    @Before(value = "anyPublicMethodWithUserLogin() && annotatedWithRolePermission()")
    public void checkRolePermission(JoinPoint joinPoint) {

        RolePermission rolePermission = getAnnotation(joinPoint, RolePermission.class);
        
        if (rolePermission == null) {
            // Shoud never happen
            throw new UnsupportedOperationException("RolePermission annotation not found on method %s" + joinPoint.getSignature().toShortString() 
                    + ". This may be a configuration mistake with AspectJ");
        }

        String[] roleAlloweds = rolePermission.value();

        Object[] args = joinPoint.getArgs();
        if (args.length == 0) {
            // Should never happen
            return;
        }
        if (args[0] == null || !(args[0] instanceof UserLogin)) {
            throw new UnsupportedOperationException("UserLogin is required to access this method");
        }
        
        UserLogin userLogin = (UserLogin) args[0];

        if (roleAlloweds != null) {
            for (String roleAllowed : roleAlloweds) {
                if (userLogin.isRole(roleAllowed)) {
                    return;
                }
            }
        }

        throw new UnsupportedOperationException(String.format("Role '%s' not allow for this method",
                userLogin.getRole()));
    }

    @Before(value = "anyPublicMethodWithUserLogin() && annotatedWithRequireModule()")
    public void checkModuleAccessible(JoinPoint joinPoint) {
        RequireModule moduleAnnotation = getAnnotation(joinPoint, RequireModule.class);
        
        if (moduleAnnotation == null) {
            // Shoud never happen
            throw new UnsupportedOperationException("RequireModule annotation not found on method %s" + joinPoint.getSignature().toShortString()
                    + ". This may be a configuration mistake with AspectJ");
        }

        String requiredModule = moduleAnnotation.value();

        Object[] args = joinPoint.getArgs();
        if (args.length == 0) {
            // Should never happen
            return;
        }
        if (args[0] == null || !(args[0] instanceof UserLogin)) {
            throw new UnsupportedOperationException("UserLogin is required to access this method");
        }
        
        UserLogin userLogin = (UserLogin) args[0];

        if (StringUtils.isEmpty(requiredModule)) {
            logger.info("Value for annotation Module for method %s is empty. Module access check will be skip for this method", joinPoint.getSignature().toShortString());
        }
        
        if (!userLogin.hasModule(requiredModule)) {
            throw new UnsupportedOperationException(String.format("Client '%s' not allow for this method. Require module '%s'",
                    userLogin.getClientName(), requiredModule));
        }
    }
    
    private <T extends Annotation> T getAnnotation(JoinPoint joinPoint, Class<T> annotationtype) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        T annotation = AnnotationUtils.findAnnotation(method, annotationtype);
        if (annotation == null) {
            annotation = AnnotationUtils.findAnnotation(method.getDeclaringClass(), annotationtype);
        }
        return annotation;
    }

}
