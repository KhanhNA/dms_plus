package com.viettel.backend.service.checkin;

import com.viettel.backend.dto.checkin.CheckInCreateDto;
import com.viettel.backend.dto.checkin.CheckInDto;
import com.viettel.backend.dto.checkin.CheckInListDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface CheckInCreatingService {

    public IdDto create(UserLogin userLogin, CheckInCreateDto createDto);

    public ListDto<CheckInListDto> getMyCheckIns(UserLogin userLogin, PageSizeRequest pageSizeRequest);

    public CheckInDto getMyCheckInById(UserLogin userLogin, String id);

}
