package com.viettel.backend.service.checkin;

import com.viettel.backend.dto.checkin.CheckInDto;
import com.viettel.backend.dto.checkin.CheckInListDto;
import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface CheckInMonitoringService {

    public ListDto<CheckInListDto> getList(UserLogin userLogin, String createdById, String fromDate, String toDate,
            PageSizeRequest pageSizeRequest);

    public ListDto<CheckInDto> getCheckInByDate(UserLogin userLogin, String createdById, String date);

    public CheckInDto getById(UserLogin userLogin, String id);

    public ExportDto export(UserLogin userLogin, String createdById, String fromDate, String toDate, String lang);

}
