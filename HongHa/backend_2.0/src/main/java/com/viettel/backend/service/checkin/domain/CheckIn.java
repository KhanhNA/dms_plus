package com.viettel.backend.service.checkin.domain;

import java.util.List;

import com.viettel.backend.service.core.pdto.PO;
import com.viettel.repository.common.domain.checkin.I_CheckIn;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

public class CheckIn extends PO implements I_CheckIn {

    private static final long serialVersionUID = -9078755597035818920L;
    
    private I_UserEmbed createdBy;
    private SimpleDate createdTime;

    private String note;
    private List<String> photos;
    private Location location;
    private I_CategoryEmbed province;

    public I_UserEmbed getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(I_UserEmbed createdBy) {
        this.createdBy = createdBy;
    }

    public SimpleDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        this.createdTime = createdTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public I_CategoryEmbed getProvince() {
        return province;
    }

    public void setProvince(I_CategoryEmbed province) {
        this.province = province;
    }

    @Override
    public int getNbPhotos() {
        return getPhotos() == null ? 0 : getPhotos().size();
    }

}
