package com.viettel.backend.service.checkin.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.checkin.CheckInCreateDto;
import com.viettel.backend.dto.checkin.CheckInDto;
import com.viettel.backend.dto.checkin.CheckInListDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.checkin.CheckInCreatingService;
import com.viettel.backend.service.checkin.domain.CheckIn;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.UserEmbed;
import com.viettel.repository.common.CheckInRepository;
import com.viettel.repository.common.ProvinceRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.checkin.I_CheckIn;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.util.DateTimeUtils;
import com.viettel.repository.common.util.LocationUtils;

@RolePermission(value = { Role.OBSERVER, Role.SUPERVISOR })
@RequireModule(Module.CHECK_IN)
@Service
public class CheckInCreatingServiceImpl extends AbstractService implements CheckInCreatingService {

    @Autowired
    private CheckInRepository checkInRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Override
    public IdDto create(UserLogin userLogin, CheckInCreateDto createDto) {
        BusinessAssert.notNull(createDto);
        BusinessAssert.notNull(createDto.getNote());

        CheckIn checkIn = new CheckIn();
        initiatePO(userLogin.getClientId(), checkIn);
        checkIn.setCreatedBy(new UserEmbed(getCurrentUser(userLogin)));
        checkIn.setCreatedTime(DateTimeUtils.getCurrentTime());

        checkIn.setNote(createDto.getNote());
        checkIn.setPhotos(createDto.getPhotos());

        if (LocationUtils.checkLocationValid(createDto.getLocation())) {
            BusinessAssert.isTrue(LocationUtils.checkLocationValid(createDto.getLocation()));
            checkIn.setLocation(createDto.getLocation());
            I_Category province = provinceRepository.getProvinceByLocation(createDto.getLocation());
            if (province != null) {
                checkIn.setProvince(new CategoryEmbed(province));
            }
        }

        checkInRepository.save(userLogin.getClientId(), checkIn);

        return null;
    }

    @Override
    public ListDto<CheckInListDto> getMyCheckIns(UserLogin userLogin, PageSizeRequest pageSizeRequest) {
        List<I_CheckIn> checkIns = checkInRepository.getCheckIns(userLogin.getClientId(),
                Collections.singleton(userLogin.getUserId()), null, pageSizeRequest);

        List<CheckInListDto> dtos = new ArrayList<>(checkIns.size());
        for (I_CheckIn checkIn : checkIns) {
            dtos.add(new CheckInListDto(checkIn));
        }

        long count = checkInRepository.countCheckIns(userLogin.getClientId(),
                Collections.singleton(userLogin.getUserId()), null);

        return new ListDto<>(dtos, count);
    }

    @Override
    public CheckInDto getMyCheckInById(UserLogin userLogin, String _id) {
        I_CheckIn checkIn = getMandatoryPO(userLogin, _id, checkInRepository);

        BusinessAssert.equals(userLogin.getUserId(), checkIn.getCreatedBy().getId());

        return new CheckInDto(checkIn);
    }

}
