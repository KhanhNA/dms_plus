package com.viettel.backend.service.checkin.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.checkin.CheckInDto;
import com.viettel.backend.dto.checkin.CheckInListDto;
import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.checkin.CheckInMonitoringService;
import com.viettel.backend.service.core.common.AbstractExportService;
import com.viettel.repository.common.CheckInRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.checkin.I_CheckIn;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.ADVANCE_SUPERVISOR })
@RequireModule(Module.CHECK_IN)
@Service
public class CheckInMonitoringServiceImpl extends AbstractExportService implements CheckInMonitoringService {

	@Autowired
	private CheckInRepository checkInRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public ListDto<CheckInListDto> getList(UserLogin userLogin, String createdById, String _fromDate, String _toDate,
			PageSizeRequest pageSizeRequest) {
		BusinessAssert.notNull(createdById);
		BusinessAssert.notNull(_fromDate);
		BusinessAssert.notNull(_toDate);

		I_User createdBy = getMandatoryPO(userLogin, createdById, userRepository);

		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		List<I_CheckIn> checkIns = checkInRepository.getCheckIns(userLogin.getClientId(),
				Collections.singleton(createdBy.getId()), period, pageSizeRequest);

		List<CheckInListDto> dtos = new ArrayList<>(checkIns.size());
		for (I_CheckIn checkIn : checkIns) {
			dtos.add(new CheckInListDto(checkIn));
		}

		long count = checkInRepository.countCheckIns(userLogin.getClientId(), Collections.singleton(createdBy.getId()),
				period);

		return new ListDto<>(dtos, count);
	}

	@Override
	public ListDto<CheckInDto> getCheckInByDate(UserLogin userLogin, String createdById, String date) {
		BusinessAssert.notNull(createdById);
		BusinessAssert.notNull(date);

		I_User createdBy = getMandatoryPO(userLogin, createdById, userRepository);

		Period todayPeriod = DateTimeUtils.getPeriodOneDay(getMandatoryIsoDate(date));

		List<I_CheckIn> checkIns = checkInRepository.getCheckIns(userLogin.getClientId(),
				Collections.singleton(createdBy.getId()), todayPeriod, null);

		List<CheckInDto> dtos = new ArrayList<>(checkIns.size());
		for (I_CheckIn checkIn : checkIns) {
			dtos.add(new CheckInDto(checkIn));
		}

		return new ListDto<>(dtos);
	}

	@Override
	public CheckInDto getById(UserLogin userLogin, String id) {
		I_CheckIn checkIn = getMandatoryPO(userLogin, id, checkInRepository);
		return new CheckInDto(checkIn);
	}

	@Override
	public ExportDto export(UserLogin userLogin, String createdById, String _fromDate, String _toDate, String lang) {
		lang = getLang(lang);

		BusinessAssert.notNull(_fromDate);
		BusinessAssert.notNull(_toDate);

		Collection<String> createdByIds = null;
		if (createdById != null) {
			I_User createdBy = getMandatoryPO(userLogin, createdById, userRepository);
			createdByIds = Collections.singleton(createdBy.getId());
		}else {
			// If role is A_SUP, we will get all supervisor and advance supervisor of management
			if(Role.ADVANCE_SUPERVISOR.equalsIgnoreCase(userLogin.getRole())){
				// Firstly add all supervisor
				List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
				if (distributors !=null && distributors.size() > 0){
					createdByIds = new ArrayList<>();
					for (I_Distributor dis : distributors){
						createdByIds.add(dis.getSupervisor().getId());
					}
				}
			}
		}

		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		I_Config config = getConfig(userLogin);

		List<I_CheckIn> checkIns = checkInRepository.getCheckIns(userLogin.getClientId(), createdByIds, period, null);
		Collections.sort(checkIns, new Comparator<I_CheckIn>() {

			@Override
			public int compare(I_CheckIn o1, I_CheckIn o2) {
				if (o1.getCreatedBy().getFullname().equals(o2.getCreatedBy().getFullname())) {
					if (o1.getCreatedTime().getIsoDate().equals(o2.getCreatedTime().getIsoDate())) {
						return o1.getCreatedTime().getIsoTime().compareTo(o2.getCreatedTime().getIsoTime());
					} else {
						return o1.getCreatedTime().getIsoDate().compareTo(o2.getCreatedTime().getIsoDate());
					}
				} else {
					return o1.getCreatedBy().getFullname().compareTo(o2.getCreatedBy().getFullname());
				}
			}
		});

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "check.in"));

			Row row;

			String[] headers = new String[] { "check.in.created.by", "created.date", "time", "note", "number.of.photo",
					"location", "location.link" };

			int lastCol = headers.length - 1;

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "check.in.list"));

			row = sheet.createRow(3);
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "from.date") + ": " + fromDate.format(config.getDateFormat()));

			row = sheet.createRow(4);
			sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "to.date") + ": " + toDate.format(config.getDateFormat()));

			// Create table headers
			row = sheet.createRow(6);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			textCellStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);
			numberCellStyle.setVerticalAlignment(CellStyle.VERTICAL_TOP);

			int rownum = 7;

			String createdByFullname = null;
			String isoDate = null;
			int createdByStartIndex = rownum;
			int dateStartIndex = rownum;

			for (I_CheckIn checkIn : checkIns) {
				row = sheet.createRow(rownum);

				if (createdByFullname == null) {
					createdByFullname = checkIn.getCreatedBy().getFullname();
				}
				if (isoDate == null) {
					isoDate = checkIn.getCreatedTime().getIsoDate();
				}

				if (!createdByFullname.equals(checkIn.getCreatedBy().getFullname())) {
					if (createdByStartIndex < rownum - 1) {
						sheet.addMergedRegion(new CellRangeAddress(createdByStartIndex, rownum - 1, 0, 0));
					}
					if (dateStartIndex < rownum - 1) {
						sheet.addMergedRegion(new CellRangeAddress(dateStartIndex, rownum - 1, 1, 1));
					}
					createdByStartIndex = rownum;
					createdByFullname = checkIn.getCreatedBy().getFullname();
					dateStartIndex = rownum;
					isoDate = checkIn.getCreatedTime().getIsoDate();
				} else {
					if (!isoDate.equals(checkIn.getCreatedTime().getIsoDate())) {
						if (dateStartIndex < rownum - 1) {
							sheet.addMergedRegion(new CellRangeAddress(dateStartIndex, rownum - 1, 1, 1));
						}
						dateStartIndex = rownum;
						isoDate = checkIn.getCreatedTime().getIsoDate();
					}
				}

				createCell(row, 0, textCellStyle, checkIn.getCreatedBy().getFullname());
				createCell(row, 1, textCellStyle, checkIn.getCreatedTime().format("dd/MM/yyyy"));
				createCell(row, 2, textCellStyle, checkIn.getCreatedTime().format("HH:mm:ss"));
				createCell(row, 3, textCellStyle, checkIn.getNote());
				createCell(row, 4, numberCellStyle, checkIn.getPhotos() == null ? 0 : checkIn.getPhotos().size());

				if (checkIn.getLocation() == null) {
					createCell(row, 5, textCellStyle, translate(lang, "location.undefined"));
				} else if (checkIn.getProvince() == null) {
					createCell(row, 5, textCellStyle, translate(lang, "address.undefined"));
				} else {
					createCell(row, 5, textCellStyle, checkIn.getProvince().getName());
				}
				if(checkIn.getLocation() != null){
					createCell(workbook,row, 6, getLinkLocation(checkIn.getLocation()),translate(lang, "location"));
				}
				rownum++;
			}

			if (createdByStartIndex < rownum - 1) {
				sheet.addMergedRegion(new CellRangeAddress(createdByStartIndex, rownum - 1, 0, 0));
			}
			if (dateStartIndex < rownum - 1) {
				sheet.addMergedRegion(new CellRangeAddress(dateStartIndex, rownum - 1, 1, 1));
			}

			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
			createCell(row, 0, footerTextCellStyle, checkIns.size() + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			StringBuilder fileName = new StringBuilder();
			fileName.append("CheckIn_");
			fileName.append(fromDate.format("ddMMyyyy") + "_");
			fileName.append(toDate.format("ddMMyyyy"));
			fileName.append(".xlsx");

			File outTempFile = File.createTempFile(fileName.toString() + "_" + System.currentTimeMillis(), ".tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);
			workbook.dispose();

			return new ExportDto(fileName.toString(), new FileInputStream(outTempFile));
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}
	
	private String getLinkLocation(Location location){
		String latitude = Double.toString(location.getLatitude());
		String longitude = Double.toString(location.getLongitude());
		 return "http://maps.google.com/maps?q="+latitude+","+longitude+"&ll="+latitude+","+longitude+"&z=12";
	}

}
