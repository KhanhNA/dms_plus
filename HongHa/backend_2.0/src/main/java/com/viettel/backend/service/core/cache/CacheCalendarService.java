package com.viettel.backend.service.core.cache;

import java.util.List;

import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;

public interface CacheCalendarService extends I_CacheElement {
    
    public List<SimpleDate> getWorkingDays(String clientId, Period period);
    
    public SimpleDate getPreviousWorkingDay(String clientId, SimpleDate date);
    
}
