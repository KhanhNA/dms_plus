package com.viettel.backend.service.core.cache;

public interface CacheManagerService {
    
    public void register(I_CacheElement cacheElement);
    
    public void clearAll();
    
    /**
     * @param client mandatory
     */
    public void clearByClient(String clientId);
    
    public void init(String clientId);
    
}
