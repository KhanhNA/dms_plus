package com.viettel.backend.service.core.cache;

import java.util.Collection;
import java.util.Map;

import com.viettel.repository.common.entity.SimpleDate.Period;

public interface CacheScheduleService extends I_CacheElement {
    
    public Map<String, Integer> getNbVisitPlannedByRoute(String clientId, Collection<String> distributorIds,
            Period period);

}
