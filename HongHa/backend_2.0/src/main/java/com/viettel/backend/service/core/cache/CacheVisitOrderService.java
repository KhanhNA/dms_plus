package com.viettel.backend.service.core.cache;

import java.util.Collection;
import java.util.Map;

import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.entity.SimpleDate;

public interface CacheVisitOrderService extends I_CacheElement {

    public static enum DateType {
        DAILY, MONTHLY
    }

    public void addNewApprovedOrder(I_Order order);

    public void addNewVisited(I_Visit visit);

    public double getRevenueByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);
    
    public double getSubRevenueByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);

    public double getProductivityByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);

    public double getNbOrderByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);

    public double getNbOrderNoVisitByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);

    public double getNbVisitByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);

    public double getNbVisitWithOrderByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);

    public double getNbVisitErrorDurationByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);

    public double getNbVisitErrorPositionByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds);

    // CREATED BY
    public double getRevenueByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds);
    
    public double getSubRevenueByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds);

    public double getProductivityByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds);

    public double getNbOrderByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds);

    public double getNbOrderNoVisitByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds);

    // SALESMAN
    public double getNbVisitBySalesman(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> salesmanIds);

    public Map<String, Double> getNbVisitBySalesmanMap(String clientId, DateType dateType, SimpleDate date,
            String distributorId);

    public double getNbVisitWithOrderBySalesman(String clientId, DateType dateType, SimpleDate date,
            String distributorId, Collection<String> salesmanIds);

    public double getNbVisitErrorDurationBySalesman(String clientId, DateType dateType, SimpleDate date,
            String distributorId, Collection<String> salesmanIds);

    public double getNbVisitErrorPositionBySalesman(String clientId, DateType dateType, SimpleDate date,
            String distributorId, Collection<String> salesmanIds);

    // PRODUCT
    public Map<String, Double> getQuantityProductMap(String clientId, DateType dateType, SimpleDate date,
            String distributorId);

    public Map<String, Double> getQuantitySoldByProductFromLastInventoryUpdate(String clientId, String distributorId);
    
    public void clearQuantitySoldByProductFromLastInventoryUpdate(String clientId, String distributorId);

}
