package com.viettel.backend.service.core.cache;

public interface I_CacheElement {

    public void clearAll();
    
    /**
     * @param client mandatory
     */
    public void clearByClient(String clientId);
    
    /**
     * @param client optional
     * @param distributorId optional
     */
    public void init(String clientId, String distributorId);
    
    public void onChange(String clientId, String distributorId);
    
}
