package com.viettel.backend.service.core.cache.impl;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.cache.CacheManagerService;
import com.viettel.repository.common.CalendarConfigRepository;
import com.viettel.repository.common.cache.HashCache;
import com.viettel.repository.common.cache.RedisCache;
import com.viettel.repository.common.domain.category.I_CalendarConfig;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@Service
public class CacheCalendarServiceImpl implements CacheCalendarService, InitializingBean {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String CACHE_LIST_WORKING_DAYS = "LIST_WORKING_DAYS";

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;

    @Autowired
    private CacheManagerService cacheManagerService;

    @Override
    public void afterPropertiesSet() throws Exception {
        cacheManagerService.register(this);
    }

    @Override
    public void clearAll() {
        getCacheListWorkingDays().clear();
    }

    @Override
    public void clearByClient(String clientId) {
        clearWorkingDaysCache(clientId);
    }

    @Override
    public void init(String clientId, String distributorId) {
        // DO NOTHING
    }

    @Override
    public void onChange(String clientId, String distributorId) {
        clearWorkingDaysCache(clientId);
    }

    @Override
    public List<SimpleDate> getWorkingDays(String clientId, Period period) {
        List<SimpleDate> workingDays = getWorkingDaysCache(clientId, period);
        if (workingDays != null && workingDays.size() > 0) {
            return workingDays;
        }

        I_CalendarConfig calendarConfig = calendarConfigRepository.getCalendarConfig(clientId);
        if (calendarConfig == null) {
            throw new UnsupportedOperationException("calendar config not found");
        }

        workingDays = new LinkedList<SimpleDate>();
        SimpleDate temp = period.getFromDate();
        while (temp.compareTo(period.getToDate()) < 0) {
            if (isWorkingDay(calendarConfig, temp)) {
                workingDays.add(temp);
            }

            temp = DateTimeUtils.addDays(temp, 1);
        }

        setWorkingDaysCache(clientId, period, workingDays);

        return workingDays;
    }

    @Override
    public SimpleDate getPreviousWorkingDay(String clientId, SimpleDate date) {
        I_CalendarConfig calendarConfig = calendarConfigRepository.getCalendarConfig(clientId);
        if (calendarConfig == null) {
            throw new UnsupportedOperationException("calendar config not found");
        }

        int i = 0;
        SimpleDate temp = date;
        while (true) {
            if (i > 365) {
                throw new UnsupportedOperationException("not found working day from one year");
            }

            temp = DateTimeUtils.addDays(temp, -1);
            if (isWorkingDay(calendarConfig, temp)) {
                return temp;
            }
            i++;
        }
    }

    // PRIVATE
    private boolean isWorkingDay(I_CalendarConfig calendarConfig, SimpleDate _date) {
        Assert.notNull(calendarConfig);
        Assert.notNull(_date);

        int day = _date.getDayOfWeek();
        int date = _date.getDate();
        int month = _date.getMonth();
        int year = _date.getYear();

        // NGAY NGHI DAC BIET
        if (calendarConfig.getHolidays() != null) {
            for (SimpleDate simpleDate : calendarConfig.getHolidays()) {
                if (simpleDate.getDate() == date && simpleDate.getMonth() == month && simpleDate.getYear() == year) {
                    return false;
                }
            }
        }

        // BO WEEKEND
        if (calendarConfig.getWorkingDays() != null && calendarConfig.getWorkingDays().contains(day)) {
            return true;
        }

        return false;
    }

    // LIST WORKING DAY
    @SuppressWarnings("rawtypes")
    private HashCache<String, Period, List> getCacheListWorkingDays() {
        return redisCache.getHashCache(CACHE_LIST_WORKING_DAYS, String.class, Period.class, List.class);
    }

    @SuppressWarnings("unchecked")
    private List<SimpleDate> getWorkingDaysCache(String clientId, Period period) {
        Assert.notNull(clientId);

        try {
            return getCacheListWorkingDays().get(clientId.toString()).get(period);
        } catch (Exception e) {
            logger.error("Cannot get list working days from cache", e);
            return null;
        }
    }

    private void setWorkingDaysCache(String clientId, Period period, List<SimpleDate> workingDays) {
        Assert.notNull(clientId);

        try {
            getCacheListWorkingDays().get(clientId.toString()).put(period, workingDays);
        } catch (Exception e) {
            logger.error("Cannot set list working days to cache", e);
        }
    }

    private void clearWorkingDaysCache(String clientId) {
        Assert.notNull(clientId);

        try {
            getCacheListWorkingDays().delete(clientId.toString());
        } catch (Exception e) {
            logger.error("Cannot clear list working days to cache", e);
        }
    }

}
