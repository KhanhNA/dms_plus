package com.viettel.backend.service.core.cache.impl;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.service.core.cache.CacheManagerService;
import com.viettel.backend.service.core.cache.I_CacheElement;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;

@Service
public class CacheManagerServiceImpl implements CacheManagerService {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private List<I_CacheElement> cacheElements = new LinkedList<>();

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CommonRepository commonRepository;

    @Override
    public void register(I_CacheElement cacheElement) {
        this.cacheElements.add(cacheElement);
    }

    @Override
    public void clearAll() {
        for (I_CacheElement cacheElement : cacheElements) {
            cacheElement.clearAll();
        }
    }

    @Override
    public void clearByClient(String clientId) {
        for (I_CacheElement cacheElement : cacheElements) {
            cacheElement.clearByClient(clientId);
        }
    }

    @Override
    public void init(String clientId) {
        ExecutorService es = Executors.newFixedThreadPool(10);

        List<I_Category> clients;
        if (clientId == null) {
            clients = clientRepository.getAll(commonRepository.getRootId(), null);
        } else {
            I_Category client = clientRepository.getById(commonRepository.getRootId(), clientId);
            clients = Collections.singletonList(client);
        }

        for (I_Category client : clients) {
            List<I_Distributor> distributors = distributorRepository.getAll(client.getId(), null);
            for (I_Distributor distributor : distributors) {
                InitDistributorCache initDistributorCache = new InitDistributorCache(client.getId(), distributor.getId(),
                        cacheElements);
                es.execute(initDistributorCache);
            }
        }

        try {
            es.shutdown();
            es.awaitTermination(10, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            logger.error("InterruptedException", e);
            Thread.currentThread().interrupt();
            throw new UnsupportedOperationException(e);
        }
    }

    private class InitDistributorCache implements Runnable {

        private String clientId;
        private String distributorId;
        private List<I_CacheElement> cacheElements;

        protected InitDistributorCache(String clientId, String distributorId, List<I_CacheElement> cacheElements) {
            super();
            this.clientId = clientId;
            this.distributorId = distributorId;
            this.cacheElements = cacheElements;
        }

        @Override
        public void run() {
            for (I_CacheElement cacheElement : cacheElements) {
                cacheElement.init(clientId, distributorId);
            }
        }

    }

}
