package com.viettel.backend.service.core.cache.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.cache.CacheManagerService;
import com.viettel.backend.service.core.cache.CacheScheduleService;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.cache.HashCache;
import com.viettel.repository.common.cache.RedisCache;
import com.viettel.repository.common.cache.SetCache;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.customer.I_ScheduleItem;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@Service
public class CacheScheduleServiceImpl implements CacheScheduleService, InitializingBean {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String CACHE_MAP_NBVISIT_PLANNED_BY_ROUTE_DAY_WEEK = "MAP_NBVISIT_PLANNED_BY_ROUTE_DAY_WEEK";
    private static final String CACHE_MAP_NBVISIT_PLANNED_BY_ROUTE = "MAP_NBVISIT_PLANNED_BY_ROUTE";
    private static final String CACHE_MAP_NBVISIT_PLANNED_BY_ROUTE_KEYS = "MAP_NBVISIT_PLANNED_BY_ROUTE_KEYS";

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private CacheCalendarService calendarCacheService;

    @Autowired
    private CacheManagerService cacheManagerService;

    @Override
    public void afterPropertiesSet() throws Exception {
        cacheManagerService.register(this);
    }

    @Override
    public void clearAll() {
        getCacheMapNbVisitPlannedByRouteDayWeek().clear();
        getCacheMapNbVisitPlannedByRoute().clear();
        getCacheMapNbVisitPlannedByRouteKeys().clear();
    }

    @Override
    public void clearByClient(String clientId) {
        clearMapNbVisitPlannedByRouteDayWeekCache(clientId, null);
        clearNbVisitPlannedByRouteCache(clientId, null);
    }

    @Override
    public void init(String clientId, String distributorId) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        getMapNbVisitPlannedByRouteDayWeek(clientId, distributorId);

        SimpleDate today = DateTimeUtils.getToday();

        getNbVisitPlannedByRoute(clientId, Collections.singleton(distributorId), DateTimeUtils.getPeriodThisMonth());
        getNbVisitPlannedByRoute(clientId, Collections.singleton(distributorId),
                new Period(DateTimeUtils.getFirstOfThisMonth(), today));
        getNbVisitPlannedByRoute(clientId, Collections.singleton(distributorId), DateTimeUtils.getPeriodToday());
    }

    @Override
    public void onChange(String clientId, String distributorId) {
        clearMapNbVisitPlannedByRouteDayWeekCache(clientId, distributorId);
        clearNbVisitPlannedByRouteCache(clientId, distributorId);
    }

    @Override
    public Map<String, Integer> getNbVisitPlannedByRoute(String clientId, Collection<String> distributorIds,
            Period period) {
        Assert.notNull(clientId);
        Assert.notNull(distributorIds);
        Assert.notNull(period);

        if (distributorIds.isEmpty()) {
            Collections.emptyMap();
        }

        Map<String, Integer> map = new HashMap<String, Integer>();

        // Set<String> distributorsDontHaveCache = new
        // HashSet<>(distributorIds);

        Set<String> distributorsDontHaveCache = new HashSet<>();
        for (String distributorId : distributorIds) {
            Map<String, Integer> _map = getNbVisitPlannedByRouteCache(clientId, distributorId, period);
            if (_map != null) {
                map.putAll(_map);
            } else {
                distributorsDontHaveCache.add(distributorId);
            }
        }

        if (!distributorsDontHaveCache.isEmpty()) {
            Map<String, Integer> _map = _getNbVisitPlannedByRouteNoCache(clientId, distributorsDontHaveCache, period);
            map.putAll(_map);
        }

        return map;
    }

    // PRIVATE
    private Map<String, Integer> _getNbVisitPlannedByRouteNoCache(String clientId, Collection<String> distributorIds,
            Period period) {
        Map<String, Integer> map = new HashMap<String, Integer>();

        // **********
        // GET MAP DAY
        HashMap<String, Integer> mapNbDayByDayWeekIndex = new HashMap<String, Integer>();
        List<SimpleDate> workingDays = calendarCacheService.getWorkingDays(clientId, period);
        I_Config config = configRepository.getConfig(clientId);
        if (config == null) {
            throw new UnsupportedOperationException("systemConfig not found");
        }

        if (workingDays.isEmpty()) {
            return Collections.emptyMap();
        }

        for (SimpleDate date : workingDays) {
            String key = date.getDayOfWeek() + "_" + config.getWeekIndex(date);

            Integer value = mapNbDayByDayWeekIndex.get(key);
            value = (value == null ? 0 : value) + 1;
            mapNbDayByDayWeekIndex.put(key, value);
        }

        for (String distributorId : distributorIds) {
            Map<String, Map<String, Integer>> nbVisitPlannedByRouteDayWeek = getMapNbVisitPlannedByRouteDayWeek(
                    clientId, distributorId);

            // **********
            // **********
            Map<String, Integer> _map = new HashMap<String, Integer>();
            for (String routeId : nbVisitPlannedByRouteDayWeek.keySet()) {
                int nbVisitPlanned = 0;

                for (Entry<String, Integer> entry : mapNbDayByDayWeekIndex.entrySet()) {
                    Map<String, Integer> tmpMap = nbVisitPlannedByRouteDayWeek.get(routeId);
                    if (tmpMap == null) {
                        break;
                    }

                    Integer value = tmpMap.get(entry.getKey());
                    if (value != null) {
                        nbVisitPlanned = nbVisitPlanned + (value * entry.getValue());
                    }
                }

                _map.put(routeId, nbVisitPlanned);
            }

            setNbVisitPlannedByRouteCache(clientId, distributorId, period, _map);

            map.putAll(_map);
        }

        return map;
    }

    /**
     * @return Map<RouteId, Map<day_week, nbVisit>
     */
    private Map<String, Map<String, Integer>> getMapNbVisitPlannedByRouteDayWeek(String clientId,
            String distributorId) {
        Map<String, Map<String, Integer>> map1 = getMapNbVisitPlannedByRouteDayWeekCache(clientId, distributorId);
        if (map1 != null) {
            // FOUND IT IN CACHE
            return map1;
        }

        map1 = new HashMap<String, Map<String, Integer>>();

        // GET ALL ROUTES OF THIS DISTRIBUTOR
        List<I_Route> routes = routeRepository.getAll(clientId, distributorId);
        if (routes.isEmpty()) {
            setMapNbVisitPlannedByRouteDayWeekCache(clientId, distributorId, map1);
            return map1;
        }
        Set<String> routeIds = new HashSet<String>();
        for (I_Route route : routes) {
            routeIds.add(route.getId());
        }

        // GET ALL CUSTOMER
        List<I_Customer> customers = customerRepository.getCustomersByRoutes(clientId, distributorId, routeIds, null);
        I_Config config = configRepository.getConfig(clientId);
        for (I_Customer customer : customers) {
            if (customer.getSchedule() != null && customer.getSchedule().getRouteId() != null
                    && customer.getSchedule().getItem() != null) {
                I_ScheduleItem item = customer.getSchedule().getItem();

                List<Integer> weeks;
                if (config.getNumberWeekOfFrequency() > 1) {
                    weeks = item.getWeeks();
                } else {
                    weeks = Collections.singletonList(0);
                }

                if (weeks != null) {
                    for (int week : weeks) {
                        for (int day : item.getDays()) {
                            String key = day + "_" + week;

                            Map<String, Integer> map2 = map1.get(customer.getSchedule().getRouteId());
                            if (map2 == null) {
                                map2 = new HashMap<String, Integer>();
                                map1.put(customer.getSchedule().getRouteId(), map2);
                            }

                            Integer value = (Integer) map2.get(key);
                            value = (value == null ? 0 : value) + 1;
                            map2.put(key, value);
                        }
                    }
                }
            }
        }

        setMapNbVisitPlannedByRouteDayWeekCache(clientId, distributorId, map1);
        return map1;
    }

    // ********** CACHE **********
    @SuppressWarnings("rawtypes")
    private HashCache<String, String, Map> getCacheMapNbVisitPlannedByRouteDayWeek() {
        return redisCache.getHashCache(CACHE_MAP_NBVISIT_PLANNED_BY_ROUTE_DAY_WEEK, String.class, String.class,
                Map.class);
    }

    @SuppressWarnings("rawtypes")
    private HashCache<String, String, Map> getCacheMapNbVisitPlannedByRoute() {
        return redisCache.getHashCache(CACHE_MAP_NBVISIT_PLANNED_BY_ROUTE, String.class, String.class, Map.class);
    }

    private SetCache<String, String> getCacheMapNbVisitPlannedByRouteKeys() {
        return redisCache.getSetCache(CACHE_MAP_NBVISIT_PLANNED_BY_ROUTE_KEYS, String.class, String.class);
    }

    // MAP NB VISIT PLANNED BY ROUTE DAY WEEK
    /**
     * @return Map<RouteId, Map<day_week, nbVisit>
     */
    @SuppressWarnings("unchecked")
    private Map<String, Map<String, Integer>> getMapNbVisitPlannedByRouteDayWeekCache(String clientId,
            String distributorId) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        try {
            return getCacheMapNbVisitPlannedByRouteDayWeek().get(clientId).get(distributorId);
        } catch (Exception e) {
            logger.error("Cannot get map number visit planned by route cache", e);
            return null;
        }
    }

    private void setMapNbVisitPlannedByRouteDayWeekCache(String clientId, String distributorId,
            Map<String, Map<String, Integer>> map) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        try {
            getCacheMapNbVisitPlannedByRouteDayWeek().get(clientId).put(distributorId, map);
        } catch (Exception e) {
            logger.error("Cannot set map number visit planned by route cache", e);
        }
    }

    private void clearMapNbVisitPlannedByRouteDayWeekCache(String clientId, String distributorId) {
        try {
            if (clientId != null) {
                if (distributorId != null) {
                    getCacheMapNbVisitPlannedByRouteDayWeek().get(clientId).delete(distributorId);
                } else {
                    getCacheMapNbVisitPlannedByRouteDayWeek().delete(clientId);
                }
            } else {
                getCacheMapNbVisitPlannedByRouteDayWeek().clear();
            }

        } catch (Exception e) {
            logger.error("Cannot clear map number visit planned by route cache", e);
        }
    }

    // MAP NB VISIT PLANNED BY ROUTE
    @SuppressWarnings("unchecked")
    private Map<String, Integer> getNbVisitPlannedByRouteCache(String clientId, String distributorId,
            Period period) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        try {
            return getCacheMapNbVisitPlannedByRoute().get(clientId + "@" + distributorId).get(period.toString());
        } catch (Exception e) {
            logger.error("Cannot get map number visit planned by route cache", e);
            return null;
        }
    }

    private void setNbVisitPlannedByRouteCache(String clientId, String distributorId, Period period,
            Map<String, Integer> map) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        try {
            getCacheMapNbVisitPlannedByRouteKeys().get(clientId).add(clientId + "@" + distributorId);

            getCacheMapNbVisitPlannedByRoute().get(clientId + "@" + distributorId).put(period.toString(), map);
        } catch (Exception e) {
            logger.error("Cannot set map number visit planned by route cache", e);
        }
    }

    private void clearNbVisitPlannedByRouteCache(String clientId, String distributorId) {
        try {
            if (clientId != null) {
                if (distributorId != null) {
                    getCacheMapNbVisitPlannedByRoute().delete(clientId + "@" + distributorId);
                } else {
                    Set<String> keyToDeletes = getCacheMapNbVisitPlannedByRouteKeys().get(clientId).members();

                    getCacheMapNbVisitPlannedByRoute().delete(keyToDeletes);
                }
            } else {
                getCacheMapNbVisitPlannedByRoute().clear();
                getCacheMapNbVisitPlannedByRouteKeys().clear();
            }
        } catch (Exception e) {
            logger.error("Cannot clear map number visit planned by route cache", e);
        }
    }

}
