package com.viettel.backend.service.core.cache.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.cache.CacheManagerService;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.InventoryRepository;
import com.viettel.repository.common.OrderSummaryRepository;
import com.viettel.repository.common.VisitRepository;
import com.viettel.repository.common.VisitSummaryRepository;
import com.viettel.repository.common.cache.RedisCache;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderDetail;
import com.viettel.repository.common.domain.data.I_OrderSummary;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.domain.data.I_VisitSummary;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@Service
public class CacheVisitOrderServiceImpl implements CacheVisitOrderService, InitializingBean {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String CACHE_CACHENAMES_BY_CLIENT = "CACHENAMES_BY_CLIENT";
    private static final String CACHE_KEYS_BY_CLIENT = "CACHE_KEYS_BY_CLIENT";

    private static final String CACHE_REVENUE = "REVENUE";
    private static final String CACHE_SUB_REVENUE = "SUBREVENUE";
    private static final String CACHE_PRODUCTIVITY = "PRODUCTIVITY";
    private static final String CACHE_NBORDER = "NBORDER";
    private static final String CACHE_NBORDER_NOVISIT = "NBORDER_NOVISIT";
    private static final String CACHE_QUANTITY = "QUANTITY";

    private static final String CACHE_NBVISIT = "NBVISIT";
    private static final String CACHE_NBVISIT_WITH_ORDER = "CACHE_NBVISIT_WITH_ORDER";
    private static final String CACHE_NBVISIT_ERROR_DURATION = "NBVISIT_ERROR_DURATION";
    private static final String CACHE_NBVISIT_ERROR_POSITION = "NBVISIT_ERROR_POSITION";

    private static final String THIRD_KEY_CREATED = "CREATED";
    private static final String THIRD_KEY_PRODUCT = "PRODUCT";

    private static final String CACHE_QUANTITY_SOLD_FROM_LAST_INVENTORY = "CACHE_QUANTITY_SOLD_FROM_LAST_INVENTORY";

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private CacheManagerService cacheManagerService;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private OrderSummaryRepository orderSummaryRepository;

    @Autowired
    private VisitSummaryRepository visitSummaryRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private CacheCalendarService cacheCalendarService;

    @Override
    public void afterPropertiesSet() throws Exception {
        cacheManagerService.register(this);
    }

    @Override
    public void clearAll() {
        this.redisCache.clear(CACHE_REVENUE);
        this.redisCache.clear(CACHE_SUB_REVENUE);
        this.redisCache.clear(CACHE_PRODUCTIVITY);
        this.redisCache.clear(CACHE_NBORDER);
        this.redisCache.clear(CACHE_NBORDER_NOVISIT);

        this.redisCache.clear(CACHE_QUANTITY);

        this.redisCache.clear(CACHE_NBVISIT);
        this.redisCache.clear(CACHE_NBVISIT_WITH_ORDER);
        this.redisCache.clear(CACHE_NBVISIT_ERROR_DURATION);
        this.redisCache.clear(CACHE_NBVISIT_ERROR_POSITION);

        this.redisCache.clear(CACHE_QUANTITY_SOLD_FROM_LAST_INVENTORY);

        this.redisCache.clear(CACHE_KEYS_BY_CLIENT);
        this.redisCache.clear(CACHE_CACHENAMES_BY_CLIENT);
    }

    @Override
    public void clearByClient(String clientId) {
        Set<String> cacheNames = this.redisCache.getSetCache(CACHE_CACHENAMES_BY_CLIENT, String.class, String.class)
                .get(clientId.toString()).members();

        if (cacheNames != null && !cacheNames.isEmpty()) {
            for (String cacheName : cacheNames) {
                Set<String> keys = this.redisCache.getSetCache(CACHE_KEYS_BY_CLIENT, String.class, String.class)
                        .get(clientId.toString() + "@" + cacheName).members();

                this.redisCache.getHashCache(cacheName, String.class, String.class, Double.class).delete(keys);

                this.redisCache.getSetCache(CACHE_KEYS_BY_CLIENT, String.class, String.class)
                        .delete(clientId.toString() + "@" + cacheName);
            }
        }

        this.redisCache.getSetCache(CACHE_CACHENAMES_BY_CLIENT, String.class, String.class).delete(clientId.toString());
    }

    @Override
    public void init(String clientId, String distributorId) {
        Period todayPeriod = DateTimeUtils.getPeriodToday();
        Period thisMonth = DateTimeUtils.getPeriodThisMonth();
        Period lastMonth = DateTimeUtils.getPeriodLastMonth();
        List<SimpleDate> wdThisMonthUntilToday = cacheCalendarService.getWorkingDays(clientId,
                DateTimeUtils.getPeriodThisMonthUntilToday());

        LocalCache localCache = new LocalCache();

        I_Config config = configRepository.getConfig(clientId);

        OrderDateType orderDateType = config.getOrderDateType();

        // ORDER SUMMARY
        Map<String, I_OrderSummary> osBySalesmanThisMonth = orderSummaryRepository.getOrderSummaryBySalesman(clientId,
                Collections.singleton(distributorId), thisMonth, orderDateType);
        for (Entry<String, I_OrderSummary> entry : osBySalesmanThisMonth.entrySet()) {
            incrementOrderSummary(localCache, clientId, thisMonth.getFromDate(), DateType.MONTHLY, distributorId,
                    entry.getKey(), entry.getValue());
        }

        Map<String, I_OrderSummary> osBySalesmanLastMonth = orderSummaryRepository.getOrderSummaryBySalesman(clientId,
        		Collections.singleton(distributorId), lastMonth, orderDateType);
        for (Entry<String, I_OrderSummary> entry : osBySalesmanLastMonth.entrySet()) {
            incrementOrderSummary(localCache, clientId, lastMonth.getFromDate(), DateType.MONTHLY, distributorId,
                    entry.getKey(), entry.getValue());
        }

        for (SimpleDate date : wdThisMonthUntilToday) {
            Map<String, I_OrderSummary> osBySalesmanOneDay = orderSummaryRepository.getOrderSummaryBySalesman(clientId,
            		Collections.singleton(distributorId), DateTimeUtils.getPeriodOneDay(date), orderDateType);
            for (Entry<String, I_OrderSummary> entry : osBySalesmanOneDay.entrySet()) {
                incrementOrderSummary(localCache, clientId, date, DateType.DAILY, distributorId, entry.getKey(),
                        entry.getValue());
            }
        }

        // VISIT SUMMARY
        Long nbVisitedLastMonth = visitRepository.countVisitedsByDistributor(clientId, distributorId, null, null,
                lastMonth);
        increment(localCache, CACHE_NBVISIT, clientId, lastMonth.getFromDate(), DateType.MONTHLY, distributorId, null,
                null, nbVisitedLastMonth);

        Map<String, I_VisitSummary> vsBySalesmanThisMonth = visitSummaryRepository.getVisitSummaryBySalesman(clientId,
        		Collections.singleton(distributorId), thisMonth);
        for (Entry<String, I_VisitSummary> entry : vsBySalesmanThisMonth.entrySet()) {
            incrementVisitSummary(localCache, clientId, thisMonth.getFromDate(), DateType.MONTHLY, distributorId,
                    entry.getKey(), entry.getValue());
        }

        Map<String, I_VisitSummary> vsBySalesmanToday = visitSummaryRepository.getVisitSummaryBySalesman(clientId,
        		Collections.singleton(distributorId), todayPeriod);
        for (Entry<String, I_VisitSummary> entry : vsBySalesmanToday.entrySet()) {
            incrementVisitSummary(localCache, clientId, todayPeriod.getFromDate(), DateType.DAILY, distributorId,
                    entry.getKey(), entry.getValue());
        }

        // PRODUCT QUANTITY
        Map<String, BigDecimal> productSoldThisMonth = orderSummaryRepository.getProductSoldMap(clientId, distributorId,
                null, null, thisMonth, OrderDateType.CREATED_DATE);
        for (Entry<String, BigDecimal> entry : productSoldThisMonth.entrySet()) {
            incrementQuantity(localCache, clientId, thisMonth.getFromDate(), DateType.MONTHLY, distributorId,
                    entry.getKey(), entry.getValue());
        }

        Map<String, BigDecimal> productSoldToday = orderSummaryRepository.getProductSoldMap(clientId, distributorId,
                null, null, todayPeriod, OrderDateType.CREATED_DATE);
        for (Entry<String, BigDecimal> entry : productSoldToday.entrySet()) {
            incrementQuantity(localCache, clientId, todayPeriod.getFromDate(), DateType.DAILY, distributorId,
                    entry.getKey(), entry.getValue());
        }

        // PRODUCT SOLD FROM LAST INVENTORY UPDATE
        I_Inventory inventory = inventoryRepository.getLatestInventory(clientId, distributorId);
        if (inventory != null) {
            Map<String, BigDecimal> productSoldFromInventoryUpdate = orderSummaryRepository.getProductSoldMap(clientId,
                    distributorId, null, null, new Period(inventory.getTime(), DateTimeUtils.getTomorrow()),
                    OrderDateType.CREATED_DATE);
            for (Entry<String, BigDecimal> entry : productSoldFromInventoryUpdate.entrySet()) {
                incrementQuantitySoldFromLastInventory(localCache, clientId, distributorId, entry.getKey(),
                        entry.getValue().doubleValue());
            }
        }

        localCache.applyToCacheManager(redisCache, 63l, TimeUnit.DAYS);
    }

    @Override
    public void onChange(String clientId, String distributorId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addNewApprovedOrder(I_Order order) {
        I_Config config = configRepository.getConfig(order.getClientId());

        Assert.notNull(order);
        Assert.notNull(order.getDistributor());
        Assert.notNull(order.getCreatedBy());
        Assert.notNull(order.getCustomer());
        Assert.notNull(order.getCreatedTime());
        Assert.notNull(order.getApprovedTime());

        String distributorId = order.getDistributor().getId();
        String createdId = order.getCreatedBy().getId();

        SimpleDate date = (config.getOrderDateType() == OrderDateType.CREATED_DATE) ? order.getCreatedTime()
                : order.getApprovedTime();

        // REVENUE
        double revenue = order.getGrandTotal().doubleValue();
        increment(CACHE_REVENUE, order.getClientId(), date, DateType.DAILY, distributorId, null, null, revenue);
        increment(CACHE_REVENUE, order.getClientId(), date, DateType.MONTHLY, distributorId, null, null, revenue);
        increment(CACHE_REVENUE, order.getClientId(), date, DateType.DAILY, distributorId, THIRD_KEY_CREATED, createdId,
                revenue);
        increment(CACHE_REVENUE, order.getClientId(), date, DateType.MONTHLY, distributorId, THIRD_KEY_CREATED,
                createdId, revenue);

        // SUB REVENUE
        double subRevenue = order.getSubTotal().doubleValue();
        increment(CACHE_SUB_REVENUE, order.getClientId(), date, DateType.DAILY, distributorId, null, null, subRevenue);
        increment(CACHE_SUB_REVENUE, order.getClientId(), date, DateType.MONTHLY, distributorId, null, null,
                subRevenue);
        increment(CACHE_SUB_REVENUE, order.getClientId(), date, DateType.DAILY, distributorId, THIRD_KEY_CREATED,
                createdId, subRevenue);
        increment(CACHE_SUB_REVENUE, order.getClientId(), date, DateType.MONTHLY, distributorId, THIRD_KEY_CREATED,
                createdId, subRevenue);

        // PRODUCTTIVITY
        double productivity = order.getProductivity().doubleValue();
        increment(CACHE_PRODUCTIVITY, order.getClientId(), date, DateType.DAILY, distributorId, null, null,
                productivity);
        increment(CACHE_PRODUCTIVITY, order.getClientId(), date, DateType.MONTHLY, distributorId, null, null,
                productivity);
        increment(CACHE_PRODUCTIVITY, order.getClientId(), date, DateType.DAILY, distributorId, THIRD_KEY_CREATED,
                createdId, productivity);
        increment(CACHE_PRODUCTIVITY, order.getClientId(), date, DateType.MONTHLY, distributorId, THIRD_KEY_CREATED,
                createdId, productivity);

        // NBORDER
        increment(CACHE_NBORDER, order.getClientId(), date, DateType.DAILY, distributorId, null, null, 1);
        increment(CACHE_NBORDER, order.getClientId(), date, DateType.MONTHLY, distributorId, null, null, 1);
        increment(CACHE_NBORDER, order.getClientId(), date, DateType.DAILY, distributorId, THIRD_KEY_CREATED, createdId,
                1);
        increment(CACHE_NBORDER, order.getClientId(), date, DateType.MONTHLY, distributorId, THIRD_KEY_CREATED,
                createdId, 1);

        // NBORDER NOVISIT
        if (!order.isWithVisit()) {
            increment(CACHE_NBORDER_NOVISIT, order.getClientId(), date, DateType.DAILY, distributorId, null, null, 1);
            increment(CACHE_NBORDER_NOVISIT, order.getClientId(), date, DateType.MONTHLY, distributorId, null, null, 1);
            increment(CACHE_NBORDER_NOVISIT, order.getClientId(), date, DateType.DAILY, distributorId,
                    THIRD_KEY_CREATED, createdId, 1);
            increment(CACHE_NBORDER_NOVISIT, order.getClientId(), date, DateType.MONTHLY, distributorId,
                    THIRD_KEY_CREATED, createdId, 1);
        }

        I_Inventory inventory = inventoryRepository.getLatestInventory(order.getClientId(), distributorId);
        boolean updateQuantitySold = inventory != null && inventory.getTime().compareTo(order.getCreatedTime()) <= 0;

        // QUANTITY
        for (I_OrderDetail orderDetail : order.getDetails()) {
            String productId = orderDetail.getProduct().getId();
            double quantity = orderDetail.getQuantity().doubleValue();

            increment(CACHE_QUANTITY, order.getClientId(), date, DateType.DAILY, distributorId, THIRD_KEY_PRODUCT,
                    productId, quantity);
            increment(CACHE_QUANTITY, order.getClientId(), date, DateType.MONTHLY, distributorId, THIRD_KEY_PRODUCT,
                    productId, quantity);

            if (updateQuantitySold) {
                incrementQuantitySoldFromLastInventory(order.getClientId(), distributorId, productId, quantity);
            }
        }
    }

    @Override
    public void addNewVisited(I_Visit visit) {
        Assert.notNull(visit);
        Assert.notNull(visit.getDistributor());
        Assert.notNull(visit.getSalesman());

        String distributorId = visit.getDistributor().getId();
        String salesmanId = visit.getCreatedBy().getId();

        SimpleDate date = visit.getStartTime();

        // NB VISIT
        increment(CACHE_NBVISIT, visit.getClientId(), date, DateType.DAILY, distributorId, null, null, 1);
        increment(CACHE_NBVISIT, visit.getClientId(), date, DateType.MONTHLY, distributorId, null, null, 1);
        increment(CACHE_NBVISIT, visit.getClientId(), date, DateType.DAILY, distributorId, THIRD_KEY_CREATED,
                salesmanId, 1);
        increment(CACHE_NBVISIT, visit.getClientId(), date, DateType.MONTHLY, distributorId, THIRD_KEY_CREATED,
                salesmanId, 1);

        if (visit.isHasOrder()) {
            increment(CACHE_NBVISIT_WITH_ORDER, visit.getClientId(), date, DateType.DAILY, distributorId, null, null,
                    1);
            increment(CACHE_NBVISIT_WITH_ORDER, visit.getClientId(), date, DateType.MONTHLY, distributorId, null, null,
                    1);
            increment(CACHE_NBVISIT_WITH_ORDER, visit.getClientId(), date, DateType.DAILY, distributorId,
                    THIRD_KEY_CREATED, salesmanId, 1);
            increment(CACHE_NBVISIT_WITH_ORDER, visit.getClientId(), date, DateType.MONTHLY, distributorId,
                    THIRD_KEY_CREATED, salesmanId, 1);
        }

        if (visit.isErrorDuration()) {
            increment(CACHE_NBVISIT_ERROR_DURATION, visit.getClientId(), date, DateType.DAILY, distributorId, null,
                    null, 1);
            increment(CACHE_NBVISIT_ERROR_DURATION, visit.getClientId(), date, DateType.MONTHLY, distributorId, null,
                    null, 1);
            increment(CACHE_NBVISIT_ERROR_DURATION, visit.getClientId(), date, DateType.DAILY, distributorId,
                    THIRD_KEY_CREATED, salesmanId, 1);
            increment(CACHE_NBVISIT_ERROR_DURATION, visit.getClientId(), date, DateType.MONTHLY, distributorId,
                    THIRD_KEY_CREATED, salesmanId, 1);
        }

        if (visit.getLocationStatus() != I_Visit.LOCATION_STATUS_LOCATED) {
            increment(CACHE_NBVISIT_ERROR_POSITION, visit.getClientId(), date, DateType.DAILY, distributorId, null,
                    null, 1);
            increment(CACHE_NBVISIT_ERROR_POSITION, visit.getClientId(), date, DateType.MONTHLY, distributorId, null,
                    null, 1);
            increment(CACHE_NBVISIT_ERROR_POSITION, visit.getClientId(), date, DateType.DAILY, distributorId,
                    THIRD_KEY_CREATED, salesmanId, 1);
            increment(CACHE_NBVISIT_ERROR_POSITION, visit.getClientId(), date, DateType.MONTHLY, distributorId,
                    THIRD_KEY_CREATED, salesmanId, 1);
        }
    }

    // DISTRIBUTOR
    @Override
    public double getRevenueByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_REVENUE, clientId, date, dateType, distributorIds);
    }

    @Override
    public double getSubRevenueByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_SUB_REVENUE, clientId, date, dateType, distributorIds);
    }

    @Override
    public double getProductivityByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_PRODUCTIVITY, clientId, date, dateType, distributorIds);
    }

    @Override
    public double getNbOrderByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_NBORDER, clientId, date, dateType, distributorIds);
    }

    @Override
    public double getNbOrderNoVisitByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_NBORDER_NOVISIT, clientId, date, dateType, distributorIds);
    }

    @Override
    public double getNbVisitByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_NBVISIT, clientId, date, dateType, distributorIds);
    }

    @Override
    public double getNbVisitWithOrderByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_NBVISIT_WITH_ORDER, clientId, date, dateType, distributorIds);
    }

    @Override
    public double getNbVisitErrorDurationByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_NBVISIT_ERROR_DURATION, clientId, date, dateType, distributorIds);
    }

    @Override
    public double getNbVisitErrorPositionByDistributor(String clientId, DateType dateType, SimpleDate date,
            Collection<String> distributorIds) {
        return getValueSum(CACHE_NBVISIT_ERROR_POSITION, clientId, date, dateType, distributorIds);
    }

    // CREATED BY
    @Override
    public double getRevenueByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds) {
        return getValueSum(CACHE_REVENUE, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdIds);
    }

    @Override
    public double getSubRevenueByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds) {
        return getValueSum(CACHE_SUB_REVENUE, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdIds);
    }

    @Override
    public double getProductivityByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds) {
        return getValueSum(CACHE_PRODUCTIVITY, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdIds);
    }

    @Override
    public double getNbOrderByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds) {
        return getValueSum(CACHE_NBORDER, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdIds);
    }

    @Override
    public double getNbOrderNoVisitByCreated(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> createdIds) {
        return getValueSum(CACHE_NBORDER_NOVISIT, clientId, date, dateType, distributorId, THIRD_KEY_CREATED,
                createdIds);
    }

    // SALESMAN
    @Override
    public double getNbVisitBySalesman(String clientId, DateType dateType, SimpleDate date, String distributorId,
            Collection<String> salesmanIds) {
        return getValueSum(CACHE_NBVISIT, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, salesmanIds);
    }

    @Override
    public Map<String, Double> getNbVisitBySalesmanMap(String clientId, DateType dateType, SimpleDate date,
            String distributorId) {
        return getMapValue(CACHE_NBVISIT, clientId, date, dateType, distributorId, THIRD_KEY_CREATED);
    }

    @Override
    public double getNbVisitWithOrderBySalesman(String clientId, DateType dateType, SimpleDate date,
            String distributorId, Collection<String> salesmanIds) {
        return getValueSum(CACHE_NBVISIT_WITH_ORDER, clientId, date, dateType, distributorId, THIRD_KEY_CREATED,
                salesmanIds);
    }

    @Override
    public double getNbVisitErrorDurationBySalesman(String clientId, DateType dateType, SimpleDate date,
            String distributorId, Collection<String> salesmanIds) {
        return getValueSum(CACHE_NBVISIT_ERROR_DURATION, clientId, date, dateType, distributorId, THIRD_KEY_CREATED,
                salesmanIds);
    }

    @Override
    public double getNbVisitErrorPositionBySalesman(String clientId, DateType dateType, SimpleDate date,
            String distributorId, Collection<String> salesmanIds) {
        return getValueSum(CACHE_NBVISIT_ERROR_POSITION, clientId, date, dateType, distributorId, THIRD_KEY_CREATED,
                salesmanIds);
    }

    // QUANTITY
    @Override
    public Map<String, Double> getQuantityProductMap(String clientId, DateType dateType, SimpleDate date,
            String distributorId) {
        return getMapValue(CACHE_QUANTITY, clientId, date, dateType, distributorId, THIRD_KEY_PRODUCT);
    }

    @Override
    public Map<String, Double> getQuantitySoldByProductFromLastInventoryUpdate(String clientId, String distributorId) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        try {
            return redisCache
                    .getHashCache(CACHE_QUANTITY_SOLD_FROM_LAST_INVENTORY, String.class, String.class, Double.class)
                    .get(clientId.toString() + "@" + distributorId.toString()).entries();
        } catch (Exception e) {
            logger.error("Cannot current inventory map by distributor cache", e);
            return null;
        }
    }

    @Override
    public void clearQuantitySoldByProductFromLastInventoryUpdate(String clientId, String distributorId) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        try {
            this.redisCache
                    .getHashCache(CACHE_QUANTITY_SOLD_FROM_LAST_INVENTORY, String.class, String.class, Double.class)
                    .delete(clientId.toString() + "@" + distributorId.toString());
        } catch (Exception e) {
            logger.error("Cannot current inventory map by distributor cache", e);
        }
    }

    // ************************************************************
    // ************************************************************
    // ************************************************************
    // ************************************************************
    // ************************************************************

    // PRIVATE
    private void storeCacheKeyByClient(String clientId, String cacheName, String key) {
        this.redisCache.getSetCache(CACHE_CACHENAMES_BY_CLIENT, String.class, String.class).get(clientId.toString())
                .add(cacheName);
        this.redisCache.getSetCache(CACHE_KEYS_BY_CLIENT, String.class, String.class)
                .get(clientId.toString() + "@" + cacheName).add(key);
    }

    private static class LocalCache extends HashMap<String, Map<String, Map<String, Double>>> {

        private static final long serialVersionUID = -1407667477206125091L;

        private transient Logger logger = LoggerFactory.getLogger(this.getClass());

        private HashMap<String, Map<String, Set<String>>> keyMapByClient;

        private LocalCache() {
            super();
            this.keyMapByClient = new HashMap<>();
        }

        protected void increment(String cacheName, String clientId, String firstKey, String secondKey, double _value) {
            Map<String, Map<String, Double>> map1 = this.get(cacheName);
            if (map1 == null) {
                map1 = new HashMap<>();
                this.put(cacheName, map1);
            }

            Map<String, Double> map2 = map1.get(firstKey);
            if (map2 == null) {
                map2 = new HashMap<>();
                map1.put(firstKey, map2);
            }

            Double value = map2.get(secondKey);
            if (value == null) {
                value = 0.0;
            }
            map2.put(secondKey, value + _value);

            Map<String, Set<String>> keyMap = this.keyMapByClient.get(clientId);
            Set<String> keys;
            if (keyMap == null) {
                keyMap = new HashMap<>();
                this.keyMapByClient.put(clientId, keyMap);
            }

            keys = keyMap.get(cacheName);
            if (keys == null) {
                keys = new HashSet<>();
                keyMap.put(cacheName, keys);
                this.keyMapByClient.put(clientId, keyMap);
            }

            keys.add(firstKey);
        }

        protected void applyToCacheManager(RedisCache redisCache, Long timeout, TimeUnit unit) {
            try {
                for (Map.Entry<String, Map<String, Map<String, Double>>> entry1 : this.entrySet()) {
                    for (Map.Entry<String, Map<String, Double>> entry2 : entry1.getValue().entrySet()) {
                        redisCache.getHashCache(entry1.getKey(), String.class, String.class, Double.class)
                                .get(entry2.getKey()).putAll(entry2.getValue());
                        if (timeout != null && unit != null) {
                            redisCache.getHashCache(entry1.getKey(), String.class, String.class, Double.class)
                                    .get(entry2.getKey()).expire(timeout, unit);
                        }
                    }
                }

                for (Map.Entry<String, Map<String, Set<String>>> keyMapEntry : this.getKeyMapByClient().entrySet()) {
                    for (Map.Entry<String, Set<String>> keysEntry : keyMapEntry.getValue().entrySet()) {
                        Set<String> keys = keysEntry.getValue();
                        if (keys != null && !keys.isEmpty()) {
                            String[] keyArray = new String[keys.size()];
                            keys.toArray(keyArray);

                            redisCache.getSetCache(CACHE_CACHENAMES_BY_CLIENT, String.class, String.class)
                                    .get(keyMapEntry.getKey().toString()).add(keysEntry.getKey());

                            redisCache.getSetCache(CACHE_KEYS_BY_CLIENT, String.class, String.class)
                                    .get(keyMapEntry.getKey().toString() + "@" + keysEntry.getKey()).add(keyArray);
                        }
                    }
                }

            } catch (Exception e) {
                logger.error("cache error when increment", e);
            }
        }

        public HashMap<String, Map<String, Set<String>>> getKeyMapByClient() {
            return keyMapByClient;
        }

    }

    private void increment(LocalCache localCache, String cacheName, String clientId, SimpleDate date, DateType dateType,
            String distributorId, String thirdKeyName, String thirdKeyId, double value) {
        Assert.notNull(clientId);
        Assert.notNull(date);
        Assert.notNull(distributorId);
        Assert.isTrue((thirdKeyName == null && thirdKeyId == null) || (thirdKeyName != null && thirdKeyId != null));

        if (dateType == DateType.DAILY && date.compareTo(DateTimeUtils.getFirstOfThisMonth()) < 0) {
            // DO NOT AND DAILY CACHE IF IS NOT CURRENT MONTH
            return;
        }

        String dateKey;
        if (dateType == DateType.DAILY) {
            dateKey = date.getIsoDate();
        } else {
            dateKey = date.getIsoMonth();
        }

        String firstKey = clientId.toString() + "@" + dateKey;
        String secondKey = distributorId;
        if (thirdKeyName != null && thirdKeyId != null) {
            firstKey = firstKey + '@' + distributorId.toString() + '@' + thirdKeyName;
            secondKey = thirdKeyId;
        }
        if (localCache == null) {
            try {
                this.redisCache.getHashCache(cacheName, String.class, String.class, Double.class).get(firstKey)
                        .increment(secondKey, value);

                this.redisCache.getHashCache(cacheName, String.class, String.class, Double.class).get(firstKey)
                        .expire(63, TimeUnit.DAYS);

                storeCacheKeyByClient(clientId, cacheName, firstKey);
            } catch (Exception e) {
                logger.error("cache error when increment", e);
            }
        } else {
            localCache.increment(cacheName, clientId, firstKey, secondKey, value);
        }
    }

    private void increment(String cacheName, String clientId, SimpleDate date, DateType dateType, String distributorId,
            String thirdKeyName, String thirdKeyId, double value) {
        increment(null, cacheName, clientId, date, dateType, distributorId, thirdKeyName, thirdKeyId, value);
    }

    private void incrementQuantitySoldFromLastInventory(String clientId, String distributorId, String productId,
            double quantity) {
        incrementQuantitySoldFromLastInventory(null, clientId, distributorId, productId, quantity);
    }

    private void incrementQuantitySoldFromLastInventory(LocalCache localCache, String clientId, String distributorId,
            String productId, double quantity) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);
        Assert.notNull(productId);
        Assert.isTrue(quantity >= 0);

        String firstKey = clientId.toString() + "@" + distributorId.toString();
        if (localCache == null) {
            try {
                this.redisCache
                        .getHashCache(CACHE_QUANTITY_SOLD_FROM_LAST_INVENTORY, String.class, String.class, Double.class)
                        .get(firstKey).increment(productId, quantity);

                storeCacheKeyByClient(clientId, CACHE_QUANTITY_SOLD_FROM_LAST_INVENTORY, firstKey);

            } catch (Exception e) {
                logger.error("cache error when increment", e);
            }
        } else {
            localCache.increment(CACHE_QUANTITY_SOLD_FROM_LAST_INVENTORY, clientId, firstKey, productId, quantity);
        }
    }

    private double getValueSum(String cacheName, String clientId, SimpleDate date, DateType dateType,
            Collection<String> distributorIds) {
        Assert.notNull(date);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return 0;
        }

        String dateKey;
        if (dateType == DateType.DAILY) {
            dateKey = date.getIsoDate();
        } else {
            dateKey = date.getIsoMonth();
        }

        String firstKey = clientId.toString() + "@" + dateKey;

        List<Double> values = this.redisCache.getHashCache(cacheName, String.class, String.class, Double.class)
                .get(firstKey).multiGet(distributorIds);

        if (values == null || values.isEmpty()) {
            return 0;
        } else {
            double value = 0;
            for (Double _value : values) {
                value += _value == null ? 0 : _value.doubleValue();
            }

            return value;
        }
    }

    private double getValueSum(String cacheName, String clientId, SimpleDate date, DateType dateType,
            String distributorId, String thirdKeyName, Collection<String> thirdKeyIds) {
        Assert.notNull(date);
        Assert.notNull(date);
        Assert.notNull(distributorId);
        Assert.isTrue(thirdKeyName != null && thirdKeyIds != null);

        if (thirdKeyIds.isEmpty()) {
            return 0;
        }

        String dateKey;
        if (dateType == DateType.DAILY) {
            dateKey = date.getIsoDate();
        } else {
            dateKey = date.getIsoMonth();
        }

        String firstKey = clientId.toString() + "@" + dateKey + '@' + distributorId.toString() + '@' + thirdKeyName;

        try {
            List<Double> values = this.redisCache.getHashCache(cacheName, String.class, String.class, Double.class)
                    .get(firstKey).multiGet(thirdKeyIds);

            if (values == null || values.isEmpty()) {
                return 0;
            } else {
                double value = 0;
                for (Double _value : values) {
                    value += _value == null ? 0 : _value.doubleValue();
                }

                return value;
            }
        } catch (Exception e) {
            logger.error("cache error when get value", e);
            return 0;
        }
    }

    private Map<String, Double> getMapValue(String cacheName, String clientId, SimpleDate date, DateType dateType,
            String distributorId, String thirdKeyName) {
        Assert.notNull(date);
        Assert.notNull(distributorId);

        String dateKey;
        if (dateType == DateType.DAILY) {
            dateKey = date.getIsoDate();
        } else {
            dateKey = date.getIsoMonth();
        }

        String firstKey = clientId.toString() + "@" + dateKey;
        if (thirdKeyName != null) {
            firstKey = firstKey + '@' + distributorId.toString() + '@' + thirdKeyName;
        }

        try {
            return this.redisCache.getHashCache(cacheName, String.class, String.class, Double.class).get(firstKey)
                    .entries();
        } catch (Exception e) {
            logger.error("cache error when get value", e);
            return Collections.emptyMap();
        }
    }

    private void incrementOrderSummary(LocalCache localCache, String clientId, SimpleDate date, DateType dateType,
            String distributorId, String createdId, I_OrderSummary orderSummary) {
        increment(localCache, CACHE_REVENUE, clientId, date, dateType, distributorId, null, null,
                orderSummary.getRevenue().doubleValue());
        increment(localCache, CACHE_REVENUE, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdId,
                orderSummary.getRevenue().doubleValue());

        increment(localCache, CACHE_SUB_REVENUE, clientId, date, dateType, distributorId, null, null,
                orderSummary.getSubRevenue().doubleValue());
        increment(localCache, CACHE_SUB_REVENUE, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdId,
                orderSummary.getSubRevenue().doubleValue());

        increment(localCache, CACHE_PRODUCTIVITY, clientId, date, dateType, distributorId, null, null,
                orderSummary.getProductivity().doubleValue());
        increment(localCache, CACHE_PRODUCTIVITY, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdId,
                orderSummary.getProductivity().doubleValue());

        increment(localCache, CACHE_NBORDER, clientId, date, dateType, distributorId, null, null,
                orderSummary.getNbOrder());
        increment(localCache, CACHE_NBORDER, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdId,
                orderSummary.getNbOrder());

        increment(localCache, CACHE_NBORDER_NOVISIT, clientId, date, dateType, distributorId, null, null,
                orderSummary.getNbOrderWithoutVisit());
        increment(localCache, CACHE_NBORDER_NOVISIT, clientId, date, dateType, distributorId, THIRD_KEY_CREATED,
                createdId, orderSummary.getNbOrderWithoutVisit());
    }

    private void incrementVisitSummary(LocalCache localCache, String clientId, SimpleDate date, DateType dateType,
            String distributorId, String createdId, I_VisitSummary visitSummary) {
        increment(localCache, CACHE_NBVISIT, clientId, date, dateType, distributorId, null, null,
                visitSummary.getNbVisit());
        increment(localCache, CACHE_NBVISIT, clientId, date, dateType, distributorId, THIRD_KEY_CREATED, createdId,
                visitSummary.getNbVisit());

        increment(localCache, CACHE_NBVISIT_WITH_ORDER, clientId, date, dateType, distributorId, null, null,
                visitSummary.getNbVisitWithOrder());
        increment(localCache, CACHE_NBVISIT_WITH_ORDER, clientId, date, dateType, distributorId, THIRD_KEY_CREATED,
                createdId, visitSummary.getNbVisitWithOrder());

        increment(localCache, CACHE_NBVISIT_ERROR_DURATION, clientId, date, dateType, distributorId, null, null,
                visitSummary.getNbVisitErrorDuration());
        increment(localCache, CACHE_NBVISIT_ERROR_DURATION, clientId, date, dateType, distributorId, THIRD_KEY_CREATED,
                createdId, visitSummary.getNbVisitErrorDuration());

        increment(localCache, CACHE_NBVISIT_ERROR_POSITION, clientId, date, dateType, distributorId, null, null,
                visitSummary.getNbVisitErrorPosition());
        increment(localCache, CACHE_NBVISIT_ERROR_POSITION, clientId, date, dateType, distributorId, THIRD_KEY_CREATED,
                createdId, visitSummary.getNbVisitErrorPosition());
    }

    private void incrementQuantity(LocalCache localCache, String clientId, SimpleDate date, DateType dateType,
            String distributorId, String productId, BigDecimal _quantity) {
        double quantity = _quantity == null ? 0d : _quantity.doubleValue();
        increment(localCache, CACHE_QUANTITY, clientId, date, dateType, distributorId, THIRD_KEY_PRODUCT, productId,
                quantity);
    }

}
