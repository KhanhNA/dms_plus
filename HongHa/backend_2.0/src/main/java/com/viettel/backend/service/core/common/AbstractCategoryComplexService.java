package com.viettel.backend.service.core.common;

import java.util.Set;

import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.CategoryComplexBasicRepository;
import com.viettel.repository.common.domain.common.I_Category;

public abstract class AbstractCategoryComplexService<LIST extends I_Category, DETAIL extends I_Category, WRITE extends I_Category>
        extends AbstractService {

    protected abstract CategoryComplexBasicRepository<LIST, DETAIL, WRITE> getRepository();

    protected boolean isUseDistributor() {
        return false;
    }
    
    protected void checkAccessible(UserLogin userLogin, DETAIL domain) {
        if (isUseDistributor()) {
            Set<String> accessibleDistributorIds = getIdSet(getAccessibleDistributors(userLogin));
            BusinessAssert.notNull(domain.getDistributor());
            BusinessAssert.contain(accessibleDistributorIds, domain.getDistributor().getId(),
                    "Cannot access Distributor with ID=" + domain.getDistributor().getId());
        }
    }

}
