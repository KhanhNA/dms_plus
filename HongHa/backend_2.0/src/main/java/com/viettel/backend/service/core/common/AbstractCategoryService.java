package com.viettel.backend.service.core.common;

import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.domain.common.I_Category;

public abstract class AbstractCategoryService<DOMAIN extends I_Category>
        extends AbstractCategoryComplexService<DOMAIN, DOMAIN, DOMAIN> {
            
     protected abstract CategoryBasicRepository<DOMAIN> getRepository();

}
