package com.viettel.backend.service.core.common;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

public class AbstractExportService extends AbstractService {

	protected Cell createCell(Row row, int column, CellStyle cs, String value) {
		Cell cell = row.createCell(column);
		cell.setCellStyle(cs);
		cell.setCellValue(value != null ? value : "");
		return cell;
	}

	protected Cell createCell(Row row, int column, CellStyle cs, double value) {
		Cell cell = row.createCell(column);
		cell.setCellStyle(cs);
		cell.setCellValue(value);
		return cell;
	}

	protected Cell createCell(Workbook workbook, Row row, int column, String link, String text) {
		Cell cell = row.createCell(column);
		CellStyle cellStyleLink = workbook.createCellStyle();
		Font hlinkfont = workbook.createFont();
		hlinkfont.setUnderline(Font.U_SINGLE);
		hlinkfont.setColor(HSSFColor.BLUE.index);
		cellStyleLink.setFont(hlinkfont);
		cellStyleLink.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyleLink.setBorderTop(CellStyle.BORDER_THIN);
		cellStyleLink.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyleLink.setBorderRight(CellStyle.BORDER_THIN);
		cellStyleLink.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

		CreationHelper createHelper = workbook.getCreationHelper();
		Hyperlink hyperlink = createHelper.createHyperlink(Hyperlink.LINK_URL);
		hyperlink.setAddress(link);

		cell.setCellStyle(cellStyleLink);
		cell.setHyperlink(hyperlink);
		cell.setCellValue(text != null ? text : "");
		return cell;
	}

	protected Cell createCellNumber(Row row, int column, CellStyle cs, String value) {
		if (value == null) {
			return createCell(row, column, cs, "N/A");
		}
		Cell cell = row.createCell(column);
		cell.setCellType(Cell.CELL_TYPE_NUMERIC);
		cs.setDataFormat(row.getSheet().getWorkbook().createDataFormat().getFormat("#,##0"));
		cell.setCellStyle(cs);
		cell.setCellValue(Float.parseFloat(value));
		return cell;
	}

	protected CellStyle getCellStyle(Workbook workbook, boolean bold, boolean border, short height, short align) {
		Font font = workbook.createFont();
		if (bold) {
			font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		}
		font.setFontName("Arial");
		font.setFontHeightInPoints(height);

		CellStyle cs = workbook.createCellStyle();
		cs.setFont(font);
		cs.setAlignment(align);
		cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		if (border) {
			cs.setBorderBottom(CellStyle.BORDER_THIN);
			cs.setBorderTop(CellStyle.BORDER_THIN);
			cs.setBorderLeft(CellStyle.BORDER_THIN);
			cs.setBorderRight(CellStyle.BORDER_THIN);
		}

		return cs;
	}

}
