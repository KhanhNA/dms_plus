package com.viettel.backend.service.core.common;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.POIXMLException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportConfirmDto.RowData;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.domain.file.I_File;
import com.viettel.repository.common.util.DateTimeUtils;
import com.viettel.repository.common.util.LocationUtils;

public abstract class AbstractImportService extends AbstractService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private int lamda = 0;

	public void setLamda(int lamda) {
		this.lamda = lamda;
	}

	@Autowired
	private FileRepository fileRepository;

	protected ImportConfirmDto getErrorRows(UserLogin userLogin, String fileId, I_CellValidator[] validators) {
		BusinessAssert.notNull(fileId, "file id null");
		I_File file = fileRepository.getFileById(userLogin.getClientId(), fileId);
		BusinessAssert.notNull(file, "file not found");

		if (validators == null) {
			throw new IllegalArgumentException("validators is null");
		}

		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(file.getInputStream());
			XSSFSheet sheet = wb.getSheetAt(0);

			List<RowData> rowDatas = new LinkedList<RowData>();
			int lastRowNum = sheet.getLastRowNum();

			for (int i = 1 + lamda; i <= lastRowNum; i++) {

				XSSFRow row = sheet.getRow(i);
				RowData rowData = new RowData(i + 1, new LinkedList<String>(), new LinkedList<Boolean>(), null);

				boolean isError = false;

				for (int j = 0; j < validators.length; j++) {
					I_CellValidator validator = validators[j];

					if (validator != null) {
						XSSFCell cell = row.getCell(j);

						if (cell == null) {
							cell = row.createCell(j);
							cell.setCellValue("");
						}
						CheckResult result = validator.check(cell, rowData, j);

						if (!result.isValid()) {
							isError = true;
							rowData.getErrors().add(true);
						} else {
							rowData.getErrors().add(false);
						}

						rowData.getDataTexts().add(result.getDisplay());
					}
				}

				if (isError) {
					rowDatas.add(rowData);
				}
			}

			return new ImportConfirmDto(false, lastRowNum - lamda, rowDatas);

		} catch (IOException e) {
			logger.error("error when read excel for import customer", e);
			throw new UnsupportedOperationException(e);
		} catch (POIXMLException e) {
			throw new BusinessException(BusinessExceptionCode.FILE_INVALID, "it's not excel file", e);
		} finally {
			IOUtils.closeQuietly(wb);
		}
	}

	protected ImportConfirmDto getValidRows(UserLogin userLogin, String fileId, I_CellValidator[] validators) {
		BusinessAssert.notNull(fileId, "file id null");
		I_File file = fileRepository.getFileById(userLogin.getClientId(), fileId);
		BusinessAssert.notNull(file, "file not found");

		if (validators == null) {
			throw new IllegalArgumentException("validators is null");
		}

		XSSFWorkbook wb = null;
		try {
			wb = new XSSFWorkbook(file.getInputStream());
			XSSFSheet sheet = wb.getSheetAt(0);

			List<RowData> rowDatas = new LinkedList<RowData>();
			int lastRowNum = sheet.getLastRowNum();

			for (int i = 1 + lamda; i <= lastRowNum; i++) {

				XSSFRow row = sheet.getRow(i);
				RowData rowData = new RowData(i + 1, new LinkedList<String>(), null, new LinkedList<Object>());

				boolean isError = false;

				for (int j = 0; j < validators.length; j++) {
					I_CellValidator validator = validators[j];

					XSSFCell cell = row.getCell(j);

					if (cell == null) {
						cell = row.createCell(j);
						cell.setCellValue("");
					}

					if (validator != null) {

						CheckResult result = validator.check(cell, rowData, j);

						if (!result.isValid()) {
							isError = true;
							break;
						}

						rowData.getDatas().add(result.getValue());
						rowData.getDataTexts().add(result.getDisplay());
					} else {
						rowData.getDatas().add(getStringCellValue(cell));
						rowData.getDataTexts().add(getStringCellValue(cell));
					}
				}

				if (!isError) {
					rowDatas.add(rowData);
				}
			}

			return new ImportConfirmDto(false, lastRowNum - lamda, rowDatas);
		} catch (IOException e) {
			logger.error("error when read excel for import customer", e);
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(wb);
		}
	}

	// PRIVATE
	protected static final String getStringCellValue(XSSFCell cell) {
		if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
			return null;
		}

		try {
			String value = cell.getStringCellValue();
			if (StringUtils.isNullOrEmpty(value, true)) {
				return null;
			}
			return value.trim();
		} catch (IllegalStateException e1) {
			try {
				double valueDouble = cell.getNumericCellValue();
				String pattern = "###.############";
				DecimalFormat decimalFormat = new DecimalFormat(pattern);

				String value = decimalFormat.format(valueDouble);
				if (value.endsWith(".0")) {
					value = value.substring(0, value.length() - 2);
				}
				return value;
			} catch (IllegalStateException | NumberFormatException e2) {
				return null;
			}
		}
	}

	protected static BigDecimal getNumericCellValue(XSSFCell cell, int scale) {
		String _value = getStringCellValue(cell);
		if (_value != null) {
			try {
				BigDecimal value = new BigDecimal(_value);
				value = value.setScale(scale, RoundingMode.HALF_UP);
				return value;
			} catch (NumberFormatException ex) {
				return null;
			}
		}
		return null;
	}

	// VALIDATOR
	public static class CheckResult {

		private Object value;
		private String display;
		private boolean valid;

		public CheckResult(boolean valid) {
			super();

			this.value = null;
			this.display = "";
			this.valid = valid;
		}

		public CheckResult(Object value, String display, boolean valid) {
			super();

			this.value = value;
			this.display = display;
			this.valid = valid;
		}

		public boolean isValid() {
			return valid;
		}

		public void setValid(boolean valid) {
			this.valid = valid;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}

		public String getDisplay() {
			return display;
		}

		public void setDisplay(String display) {
			this.display = display;
		}

	}

	public static interface I_CellValidator {

		public CheckResult check(XSSFCell cell, RowData rowData, int index);

	}

	public static class NoValidator implements I_CellValidator {

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			String value = getStringCellValue(cell);

			if (StringUtils.isNullOrEmpty(value, true)) {
				return new CheckResult(true);
			}

			value = value.trim();

			return new CheckResult(value, value, true);
		}

	}

	public static class MultiCellValidator implements I_CellValidator {

		private List<I_CellValidator> validators;

		public MultiCellValidator(I_CellValidator... validators) {
			if (validators == null || validators.length == 0) {
				throw new IllegalArgumentException("validators cannot null or empty");
			}

			this.validators = Arrays.asList(validators);
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			CheckResult result = null;

			for (I_CellValidator validator : validators) {
				result = validator.check(cell, rowData, index);

				if (!result.isValid()) {
					return result;
				}
			}

			return result;
		}

	}

	public static class StringMandatoryCellValidator implements I_CellValidator {

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			String value = getStringCellValue(cell);

			if (StringUtils.isNullOrEmpty(value, true)) {
				return new CheckResult(false);
			}

			value = value.trim();

			return new CheckResult(value, value, true);
		}

	}

	public static class StringUniqueCellValidator implements I_CellValidator {

		private Set<String> values;

		public StringUniqueCellValidator(Set<String> values) {
			this.values = values;
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			String value = getStringCellValue(cell);

			if (StringUtils.isNullOrEmpty(value, true)) {
				return new CheckResult(true);
			}

			value = value.trim();

			if (values.contains(value.toUpperCase())) {
				return new CheckResult(value, value, false);
			}

			values.add(value.toUpperCase());

			return new CheckResult(value, value, true);
		}

	}

	public static class MaxLengthCellValidator implements I_CellValidator {

		private int maxLength;

		public MaxLengthCellValidator(int maxLength) {
			this.maxLength = maxLength;
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			String value = getStringCellValue(cell);

			if (StringUtils.isNullOrEmpty(value, true)) {
				return new CheckResult(true);
			}

			value = value.trim();

			return new CheckResult(value, value, value.length() <= maxLength);
		}

	}

	public static class RegexCellValidator implements I_CellValidator {

		private String regex;

		public RegexCellValidator(String regex) {
			this.regex = regex;
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			String value = getStringCellValue(cell);

			if (StringUtils.isNullOrEmpty(value, true)) {
				return new CheckResult("".matches(regex));
			}

			value = value.trim();

			return new CheckResult(value, value, value.matches(regex));
		}

	}

	public static class ReferenceCellValidator<T> implements I_CellValidator {

		private Map<String, T> map;

		public ReferenceCellValidator(Map<String, T> map) {
			this.map = map;
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			String value = getStringCellValue(cell);

			if (StringUtils.isNullOrEmpty(value, true)) {
				return new CheckResult(true);
			}

			value = value.trim();

			T o = map.get(value.toUpperCase());

			if (o == null) {
				return new CheckResult(o, value, false);
			}

			return new CheckResult(o, value, true);
		}

	}

	public static class NumberMandatoryCellValidator implements I_CellValidator {

		private int scale;

		public NumberMandatoryCellValidator(int scale) {
			this.scale = scale;
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			BigDecimal value = getNumericCellValue(cell, scale);

			if (value == null) {
				return new CheckResult(false);
			}

			return new CheckResult(value, value.toString(), true);
		}

	}

	public static class IntegerMandatoryCellValidator extends NumberMandatoryCellValidator implements I_CellValidator {

		public IntegerMandatoryCellValidator() {
			super(0);
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			CheckResult result = super.check(cell, rowData, index);

			if (!result.isValid()) {
				return result;
			}

			BigDecimal value = (BigDecimal) result.getValue();

			return new CheckResult(value.intValue(), String.valueOf(value.intValue()), true);
		}

	}

	public static class NumberMinMaxCellValidator extends NumberMandatoryCellValidator implements I_CellValidator {

		private BigDecimal min;
		private BigDecimal max;

		public NumberMinMaxCellValidator(BigDecimal min, BigDecimal max, int scale) {
			super(scale);
			this.min = min;
			this.max = max;
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			CheckResult result = super.check(cell, rowData, index);

			if (!result.isValid()) {
				return result;
			}

			BigDecimal value = (BigDecimal) result.getValue();

			if (min != null && value.compareTo(min) < 0) {
				result.setValid(false);
			}

			if (max != null && value.compareTo(max) > 0) {
				result.setValid(false);
			}

			return result;
		}

	}

	public static class IntegerMinMaxCellValidator extends IntegerMandatoryCellValidator implements I_CellValidator {

		private Integer min;
		private Integer max;

		public IntegerMinMaxCellValidator(Integer min, Integer max) {
			this.min = min;
			this.max = max;
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			CheckResult result = super.check(cell, rowData, index);

			if (!result.isValid()) {
				return result;
			}

			if (min != null && ((int) result.getValue()) < min) {
				result.setValid(false);
			}

			if (max != null && ((int) result.getValue()) > max) {
				result.setValid(false);
			}

			return result;
		}

	}

	public static class LatitudeCellValidator extends NumberMandatoryCellValidator implements I_CellValidator {

		public LatitudeCellValidator() {
			super(10);
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			CheckResult result = super.check(cell, rowData, index);

			if (!result.isValid()) {
				return result;
			}

			BigDecimal value = (BigDecimal) result.getValue();

			if (!LocationUtils.checkLatitude(value.doubleValue())) {
				result.setValid(false);
			}

			return result;
		}

	}

	public static class LongitudeCellValidator extends NumberMandatoryCellValidator implements I_CellValidator {

		public LongitudeCellValidator() {
			super(10);
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			CheckResult result = super.check(cell, rowData, index);

			if (!result.isValid()) {
				return result;
			}

			BigDecimal value = (BigDecimal) result.getValue();

			if (!LocationUtils.checkLongitude(value.doubleValue())) {
				result.setValid(false);
			}

			return result;
		}

	}

	public static class DateCellValidator implements I_CellValidator {
		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			String value = getStringCellValue(cell);

			if (StringUtils.isNullOrEmpty(value, true)) {
				return new CheckResult(true);
			}

			value = value.trim();
			Date date = DateTimeUtils.getDate(value);
			Boolean isValid = (date != null);

			return new CheckResult(date, value, isValid);
		}
	}

	public static class MultiReferenceCellValidation<T> implements I_CellValidator {

		private List<Map<String, T>> maps;

		public MultiReferenceCellValidation(List<Map<String, T>> maps) {
			this.maps = maps;
		}

		@Override
		public CheckResult check(XSSFCell cell, RowData rowData, int index) {
			String value = getStringCellValue(cell);

			if (StringUtils.isNullOrEmpty(value)) {
				return new CheckResult(true);
			}
			List<T> Ts = new ArrayList<>();
			if (!"ALL".equals(value.toUpperCase())) {
				String[] values = value.trim().toUpperCase().split(",");
				for (String s : values) {
					T o = null;
					for (Map<String, T> map : maps) {
						if (o == null) {
							o = map.get(s.trim());
						} else {
							break;
						}
					}
					if (o == null) {
						return new CheckResult(o, value, false);
					} else {
						Ts.add(o);
					}
				}
			}
			return new CheckResult(Ts, value, true);
		}

	}

}
