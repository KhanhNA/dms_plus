package com.viettel.backend.service.core.common;

import java.io.Serializable;

import com.viettel.backend.dto.common.DTO;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.POBasicRepository;
import com.viettel.repository.common.domain.common.I_PO;

public abstract class AbstractPOEditableService<DETAIL extends I_PO, WRITE extends I_PO, LIST_SIMPLE_DTO extends DTO, LIST_DETAIL_DTO extends LIST_SIMPLE_DTO, CREATE_DTO extends Serializable>
        extends AbstractService {

    protected abstract POBasicRepository<DETAIL, WRITE> getRepository();

    protected abstract LIST_DETAIL_DTO createDetailDto(UserLogin userLogin, DETAIL domain);

    public abstract WRITE getObjectForCreate(UserLogin userLogin, CREATE_DTO createdto);

    public abstract WRITE getObjectForUpdate(UserLogin userLogin, DETAIL oldDomain, CREATE_DTO createdto);

    protected boolean isClientRootFixed() {
        return false;
    }
    
    protected void checkAccessible(UserLogin userLogin, DETAIL domain) {
     // DO NOTHING
    }

    protected void beforeUpdate(UserLogin userLogin, DETAIL domain) {
        // DO NOTHING
    }

    protected void afterUpdate(UserLogin userLogin, DETAIL domain) {
        // DO NOTHING
    }

    protected void beforeDelete(UserLogin userLogin, DETAIL domain) {
        // DO NOTHING
    }

    protected void beforeSetActive(UserLogin userLogin, DETAIL domain, boolean active) {
        // DO NOTHING
    }

    protected void afterSetActive(UserLogin userLogin, DETAIL domain, boolean active) {
        // DO NOTHING
    }

    public LIST_DETAIL_DTO getById(UserLogin userLogin, String _id) {
        DETAIL domain = getMandatoryPO(userLogin, _id, null, null, getRepository());

        checkAccessible(userLogin, domain);

        return createDetailDto(userLogin, domain);
    }

    public IdDto create(UserLogin userLogin, CREATE_DTO createDto) {
        BusinessAssert.notNull(createDto, "Create DTO cannot be null");

        WRITE domain = getObjectForCreate(userLogin, createDto);

        return new IdDto(getRepository().save(userLogin.getClientId(), domain));
    }

    public IdDto update(UserLogin userLogin, String _id, CREATE_DTO createDto) {
        BusinessAssert.notNull(createDto, "Create DTO cannot be null");

        DETAIL oldDomain = getMandatoryPO(userLogin, _id, true, true, getRepository());

        WRITE domain = getObjectForUpdate(userLogin, oldDomain, createDto);

        return new IdDto(getRepository().save(userLogin.getClientId(), domain));
    }

    public IdDto createAndEnable(UserLogin userLogin, CREATE_DTO createDto) {
        BusinessAssert.notNull(createDto, "Create DTO cannot be null");

        WRITE domain = getObjectForCreate(userLogin, createDto);

        return new IdDto(getRepository().saveAndEnable(userLogin.getClientId(), domain));
    }

    public IdDto updateAndEnable(UserLogin userLogin, String _id, CREATE_DTO createDto) {
        BusinessAssert.notNull(createDto, "Create DTO cannot be null");

        DETAIL oldDomain = getMandatoryPO(userLogin, _id, true, true, getRepository());

        WRITE domain = getObjectForUpdate(userLogin, oldDomain, createDto);

        return new IdDto(getRepository().saveAndEnable(userLogin.getClientId(), domain));
    }

    public boolean enable(UserLogin userLogin, String _id) {
        DETAIL domain = getMandatoryPO(userLogin, _id, true, true, getRepository());

        checkAccessible(userLogin, domain);

        getRepository().enable(userLogin.getClientId(), domain.getId());

        return true;
    }

    public boolean delete(UserLogin userLogin, String _id) {
        DETAIL domain = getMandatoryPO(userLogin, _id, true, true, getRepository());

        checkAccessible(userLogin, domain);

        beforeDelete(userLogin, domain);

        return this.getRepository().delete(userLogin.getClientId(), domain.getId());
    }

    public void setActive(UserLogin userLogin, String _id, boolean active) {
        DETAIL domain = getMandatoryPO(userLogin, _id, false, !active, getRepository());

        checkAccessible(userLogin, domain);

        beforeSetActive(userLogin, domain, active);

        if (active) {
            getRepository().activate(userLogin.getClientId(), domain.getId());
        } else {
            getRepository().deactivate(userLogin.getClientId(), domain.getId());
        }

        afterSetActive(userLogin, domain, active);
    }

}
