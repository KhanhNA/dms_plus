package com.viettel.backend.service.core.common;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.util.Assert;

import com.viettel.backend.config.root.AppProperties;
import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.pdto.PO;
import com.viettel.backend.service.core.pdto.User;
import com.viettel.persistence.mongo.domain.Distributor;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.DistributorPriceListRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.POBasicRepository;
import com.viettel.repository.common.POGetterRepository;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;

public abstract class AbstractService {

	private static final Logger logger = LoggerFactory.getLogger(AbstractService.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private ConfigRepository configRepository;

	@Autowired
	private ProductCategoryRepository productCategoryRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private RouteRepository routeRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private DistributorPriceListRepository distributorPriceListRepository;

	@Autowired
	private AppProperties appProperties;

	protected void initiatePO(String clientId, PO po) {
		Assert.notNull(clientId);

		po.setClientId(clientId);
		po.setId(null);
		po.setDraft(false);
		po.setActive(true);
	}

	/**
	 * Get {@link User} from an {@link UserLogin}
	 * 
	 * @param userLogin
	 * @return an {@link User} object, never <code>null<code>
	 * @throws IllegalArgumentException
	 *             if user not found
	 */
	protected I_User getCurrentUser(UserLogin userLogin) {
		I_User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
		Assert.notNull(user, "User not found");
		return user;
	}

	protected Set<String> getIdSet(Collection<? extends I_PO> pos) {
		if (pos == null) {
			return null;
		}
		if (pos.isEmpty()) {
			return Collections.emptySet();
		}

		Set<String> idList = new HashSet<String>(pos.size());
		for (I_PO po : pos) {
			idList.add(po.getId());
		}
		return idList;
	}

	protected final <D extends I_PO> HashMap<String, D> getPOMap(Collection<D> domains) {
		HashMap<String, D> map = new HashMap<String, D>();
		if (domains != null && !domains.isEmpty()) {
			for (D domain : domains) {
				map.put(domain.getId(), domain);
			}
		}

		return map;
	}

	protected SimpleDate getMandatoryIsoDate(String isoDate) {
		BusinessAssert.notNull(isoDate);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = sdf.parse(isoDate);

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			SimpleDate simpleDate = new SimpleDate();

			simpleDate.setYear(cal.get(Calendar.YEAR));
			simpleDate.setMonth(cal.get(Calendar.MONTH));
			simpleDate.setDate(cal.get(Calendar.DAY_OF_MONTH));

			return simpleDate;
		} catch (ParseException e) {
			throw new BusinessException(BusinessExceptionCode.INVALID_PARAM, "iso date wrong format");
		}
	}

	protected SimpleDate getMandatoryIsoTime(String isoTime) {
		BusinessAssert.notNull(isoTime);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			Date date = sdf.parse(isoTime);

			Calendar cal = Calendar.getInstance();
			cal.setTime(date);

			SimpleDate simpleDate = new SimpleDate();

			simpleDate.setYear(cal.get(Calendar.YEAR));
			simpleDate.setMonth(cal.get(Calendar.MONTH));
			simpleDate.setDate(cal.get(Calendar.DAY_OF_MONTH));
			simpleDate.setHour(cal.get(Calendar.HOUR_OF_DAY));
			simpleDate.setMinute(cal.get(Calendar.MINUTE));
			simpleDate.setSecond(cal.get(Calendar.SECOND));

			return simpleDate;
		} catch (ParseException e) {
			throw new BusinessException(BusinessExceptionCode.INVALID_PARAM, "iso time wrong format");
		}
	}

	protected <D extends I_PO> D getMandatoryPO(UserLogin userLogin, String id, POGetterRepository<D> repository) {
		D domain = repository.getById(userLogin.getClientId(), id);
		String message = String.format("Cannot find record [Type=%s, Id=%s]", repository.getClass().getSimpleName(),
				id);

		BusinessAssert.notNull(domain, BusinessExceptionCode.INVALID_PARAM, message);

		return domain;
	}

	protected <D extends I_PO> D getMandatoryPO(UserLogin userLogin, String id, Boolean draft, Boolean active,
			POBasicRepository<D, ? extends I_PO> repository) {
		D domain = repository.getById(userLogin.getClientId(), draft, active, id);
		String message = String.format("Cannot find record [Type=%s, Id=%s]", repository.getClass().getSimpleName(),
				id);

		BusinessAssert.notNull(domain, BusinessExceptionCode.INVALID_PARAM, message);

		return domain;
	}

	protected I_Config getConfig(UserLogin userLogin) {
		I_Config config = configRepository.getConfig(userLogin.getClientId());
		if (config == null) {
			throw new UnsupportedOperationException("system config not found");
		}
		return config;
	}

	protected I_ProductPhotoFactory getProductPhotoFactory(UserLogin userLogin) {
		final Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), null);
		final I_Config config = getConfig(userLogin);

		return new I_ProductPhotoFactory() {

			@Override
			public String getPhoto(String productId) {
				if (productId != null) {
					I_Product product = productMap.get(productId);
					if (product != null && product.getPhoto() != null) {
						return product.getPhoto();
					}
				}

				return config.getProductPhoto();
			}

		};
	}

	// ACCESSIBLE CHECK
	protected boolean isManager(UserLogin userLogin) {
		if (userLogin.isRole(Role.ADMIN) || userLogin.isRole(Role.OBSERVER) || userLogin.isRole(Role.SUPERVISOR)
				|| userLogin.isRole(Role.DISTRIBUTOR_ADMIN)) {
			return true;
		}

		return false;
	}

	protected List<I_Distributor> getAccessibleDistributors(UserLogin userLogin) {
		if (userLogin.isRole(Role.ADMIN)) {
			return distributorRepository.getAll(userLogin.getClientId(), null);
		} else if (userLogin.isRole(Role.OBSERVER) || userLogin.isRole(Role.ADVANCE_SUPERVISOR)) {
			return userRepository.getDistributorsOfManager(userLogin.getClientId(), userLogin.getUserId());
		} else if (userLogin.isRole(Role.SUPERVISOR)) {
			return distributorRepository.getDistributorsBySupervisors(userLogin.getClientId(),
					Collections.singletonList(userLogin.getUserId()));
		} else if (userLogin.isRole(Role.DISTRIBUTOR_ADMIN) || userLogin.isRole(Role.SALESMAN)) {
			return Collections.singletonList(getDefaultDistributor(userLogin));
		}
		return Collections.emptyList();
	}

	protected List<I_Region> getAccessibleRegion(UserLogin userLogin) {
		Assert.isTrue(userLogin.isRole(Role.ADVANCE_SUPERVISOR));
		return userRepository.getRegionsOfManager(userLogin.getClientId(), userLogin.getUserId());
	}

	protected List<I_Distributor> getDistributorsByRegion(UserLogin userLogin, String regionId) {
		Assert.isTrue(userLogin.isRole(Role.ADVANCE_SUPERVISOR));

		List<I_Distributor> distributors = new ArrayList<>();
		distributors = getDistributorOfRegionRecursively(userLogin.getClientId(),regionId,distributors);
		return distributors;
	}

	private List<I_Distributor> getDistributorOfRegionRecursively(String clientId, String regionId,
			List<I_Distributor> result) {
		Assert.notNull(regionId);
		Assert.notNull(result);
		I_Region region = regionRepository.getById(clientId, false, true, regionId);
		Assert.notNull(region);

		List<I_CategoryEmbed> distributors = regionRepository.getDistributorsOfRegion(clientId, regionId);
		for (I_CategoryEmbed r : distributors) {
			Distributor dis = new Distributor(r);
			result.add(dis);
		}

		if (region.getChildren() != null && region.getChildren().size() > 0) {
			for (I_CategoryEmbed child : region.getChildren()) {
				result = getDistributorOfRegionRecursively(clientId, child.getId(), result);
			}
		}

		return result;
	}

	protected boolean checkAccessible(UserLogin userLogin, String distributorId) {
		Assert.notNull(distributorId);

		Set<String> accessibleDistributorIds = getIdSet(getAccessibleDistributors(userLogin));
		return accessibleDistributorIds.contains(distributorId);
	}

	protected boolean checkAccessible(UserLogin userLogin, String distributorId, I_Customer customer) {
		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		Set<String> distributorIds = getIdSet(distributors);

		if (distributorId != null) {
			if (!distributorIds.contains(distributorId)) {
				return false;
			}
		}

		if (customer != null) {
			if (customer.getDistributor() == null || !distributorIds.contains(customer.getDistributor().getId())) {
				return false;
			}

			if (userLogin.isRole(Role.SALESMAN)) {
				Set<String> salesmanIds = Collections.singleton(userLogin.getUserId());

				List<I_Route> routes = routeRepository.getRoutesBySalesmen(userLogin.getClientId(),
						customer.getDistributor().getId(), salesmanIds);
				Set<String> routeIds = getIdSet(routes);

				if (!customerRepository.checkCustomerInRoute(userLogin.getClientId(), customer.getId(), routeIds)) {
					return false;
				}
			}
		}

		return true;
	}

	protected I_Distributor getDefaultDistributor(UserLogin userLogin) {
		if (userLogin.isRole(Role.SALESMAN) || userLogin.isRole(Role.DISTRIBUTOR_ADMIN)) {
			I_User user = getCurrentUser(userLogin);
			Assert.notNull(user.getDistributor(),
					String.format("User with ID: %s doesn't have Distributor", user.getId()));

			I_Distributor distributor = distributorRepository.getById(userLogin.getClientId(),
					user.getDistributor().getId());

			Assert.notNull(distributor, "Cannot find Distributor with ID: " + user.getDistributor().getId());

			return distributor;
		}
		return null;
	}

	protected final I_Distributor getMandatoryDistributor(UserLogin userLogin, String distributorId) {
		if (distributorId == null) {
			I_Distributor defaultDistributor = getDefaultDistributor(userLogin);

			if (defaultDistributor == null) {
				List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
				if (distributors.size() == 1) {
					defaultDistributor = distributors.get(0);
				}
			}

			BusinessAssert.notNull(defaultDistributor, "distributorId is required");
			return defaultDistributor;
		} else {
			List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
			BusinessAssert.isTrue(distributors != null && !distributors.isEmpty(), "you don't have to any distributor");

			Set<String> accessibleDistributorIds = getIdSet(distributors);
			BusinessAssert.contain(accessibleDistributorIds, distributorId,
					"Invalid Distributor with ID=" + distributorId);

			I_Distributor distributor = getMandatoryPO(userLogin, distributorId, distributorRepository);
			return distributor;
		}
	}

	protected String getLang(String lang) {
		if (lang == null || !appProperties.getLanguages().contains(lang)) {
			return appProperties.getLanguages().get(0);
		} else {
			return lang;
		}
	}

	protected String translate(String lang, String code) {
		try {
			return messageSource.getMessage(code, null, new Locale(lang));
		} catch (NoSuchMessageException ex) {
			if (logger.isDebugEnabled()) {
				logger.debug("No translation for key '" + code + "'");
			}
			logger.error("NoSuchMessageException in translate", ex);
			return code;
		}
	}

	protected Map<String, BigDecimal> getDistributorPriceList(UserLogin userLogin) {
		I_Distributor distributor = getDefaultDistributor(userLogin);
		if (distributor != null) {
			Map<String, BigDecimal> priceList = distributorPriceListRepository.getPriceList(userLogin.getClientId(),
					distributor.getId());
			return priceList;
		} else {
			return null;
		}
	}

	protected final I_Category getMandatoryCategory(UserLogin userLogin, String categoryId) {
		I_Category category = getMandatoryPO(userLogin, categoryId, productCategoryRepository);
		return category;
	}
}
