package com.viettel.backend.service.core.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.pdto.Category;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.entity.PageSizeRequest;

public abstract class CategoryComplexEditableServiceImpl<LIST extends I_Category, DETAIL extends I_Category, WRITE extends I_Category, LIST_SIMPLE_DTO extends CategoryDto, LIST_DETAIL_DTO extends LIST_SIMPLE_DTO, CREATE_DTO extends CategoryCreateDto>
		extends AbstractCategoryComplexService<LIST, DETAIL, WRITE>
		implements CategoryEditableService<LIST_SIMPLE_DTO, LIST_DETAIL_DTO, CREATE_DTO> {

	// @Autowired
	// private UserRepository userRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private CommonRepository commonRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private UserRepository userRepository;

	protected abstract LIST_SIMPLE_DTO createListSimpleDto(UserLogin userLogin, LIST domain);

	protected abstract LIST_DETAIL_DTO createListDetailDto(UserLogin userLogin, DETAIL domain);

	public abstract WRITE getObjectForCreate(UserLogin userLogin, CREATE_DTO createdto);

	public abstract WRITE getObjectForUpdate(UserLogin userLogin, DETAIL oldDomain, CREATE_DTO createdto);

	protected boolean isClientRootFixed() {
		return false;
	}

	protected boolean isBigData() {
		return false;
	}

	protected boolean isUseCode() {
		return false;
	}

	protected boolean isAutoGenerateCode() {
		return false;
	}

	protected boolean isCanChangeName() {
		return false;
	}

	protected boolean isCanChangeCode() {
		return false;
	}

	protected void beforeUpdate(UserLogin userLogin, DETAIL domain) {
		// DO NOTHING
	}

	protected void afterUpdate(UserLogin userLogin, DETAIL domain) {
		// DO NOTHING
	}

	protected void beforeDelete(UserLogin userLogin, DETAIL domain) {
		// DO NOTHING
	}

	protected void beforeSetActive(UserLogin userLogin, DETAIL domain, boolean active) {
		// DO NOTHING
	}

	protected void afterSetActive(UserLogin userLogin, DETAIL domain, boolean active) {
		// DO NOTHING
	}

	@Override
	public ListDto<LIST_SIMPLE_DTO> getList(UserLogin userLogin, String search, Boolean active, Boolean draft,
			String _distributorId, PageSizeRequest pageSizeRequest) {
		Set<String> distributorsToQuery = null;

		String searchName = search;
		String searchCode = search == null ? null : search.toUpperCase();

		if (isUseDistributor()) {
			Set<String> accessibleDistributorIds = getIdSet(getAccessibleDistributors(userLogin));
			if (_distributorId == null) {
				if (isBigData()) {
					BusinessAssert.isTrue(accessibleDistributorIds.size() == 1);
					distributorsToQuery = accessibleDistributorIds;
				} else {
					distributorsToQuery = accessibleDistributorIds;
				}
			} else if (_distributorId.startsWith("all_")) {
				_distributorId = _distributorId.substring(_distributorId.indexOf("_") + 1);
				I_Region region = regionRepository.getById(userLogin.getClientId(), _distributorId);
				distributorsToQuery = PO.convertIdsToSet(userRepository
						.getDistributorForRegionRecursively(userLogin.getClientId(), region, new ArrayList<>()));
			} else {
				I_Distributor distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
				BusinessAssert.contain(accessibleDistributorIds, distributor.getId());
				distributorsToQuery = Collections.singleton(distributor.getId());
			}
		}

		List<LIST> domains = getRepository().getList(userLogin.getClientId(), draft, active, distributorsToQuery,
				searchName, searchCode, pageSizeRequest);
		if (CollectionUtils.isEmpty(domains)) {
			if (pageSizeRequest != null) {
				if (pageSizeRequest.getPageNumber() == 0) {
					return ListDto.emptyList();
				}
			} else {
				return ListDto.emptyList();
			}
		}

		List<LIST_SIMPLE_DTO> dtos = new ArrayList<LIST_SIMPLE_DTO>(domains.size());
		for (LIST domain : domains) {
			dtos.add(createListSimpleDto(userLogin, domain));
		}

		long size = Long.valueOf(dtos.size());
		if (pageSizeRequest != null) {
			if (pageSizeRequest.getPageNumber() > 0 || size == pageSizeRequest.getPageSize()) {
				size = getRepository().count(userLogin.getClientId(), draft, active, distributorsToQuery, searchName,
						searchCode);
			}
		}

		return new ListDto<LIST_SIMPLE_DTO>(dtos, size);
	}

	@Override
	public LIST_DETAIL_DTO getById(UserLogin userLogin, String _id) {
		DETAIL domain = getMandatoryPO(userLogin, _id, null, null, getRepository());

		checkAccessible(userLogin, domain);

		return createListDetailDto(userLogin, domain);
	}

	@Override
	public IdDto create(UserLogin userLogin, CREATE_DTO createDto) {
		BusinessAssert.notNull(createDto, "Create DTO cannot be null");

		WRITE domain = getObjectForCreate(userLogin, createDto);

		return new IdDto(getRepository().save(userLogin.getClientId(), domain));
	}

	@Override
	public IdDto update(UserLogin userLogin, String _id, CREATE_DTO createDto) {
		BusinessAssert.notNull(createDto, "Create DTO cannot be null");

		DETAIL oldDomain = getMandatoryPO(userLogin, _id, null, true, getRepository());

		WRITE domain = getObjectForUpdate(userLogin, oldDomain, createDto);

		return new IdDto(getRepository().save(userLogin.getClientId(), domain));
	}

	@Override
	public IdDto createAndEnable(UserLogin userLogin, CREATE_DTO createDto) {
		BusinessAssert.notNull(createDto, "Create DTO cannot be null");

		WRITE domain = getObjectForCreate(userLogin, createDto);

		return new IdDto(getRepository().saveAndEnable(userLogin.getClientId(), domain));
	}

	@Override
	public IdDto updateAndEnable(UserLogin userLogin, String _id, CREATE_DTO createDto) {
		BusinessAssert.notNull(createDto, "Create DTO cannot be null");

		DETAIL oldDomain = getMandatoryPO(userLogin, _id, true, true, getRepository());

		WRITE domain = getObjectForUpdate(userLogin, oldDomain, createDto);

		return new IdDto(getRepository().saveAndEnable(userLogin.getClientId(), domain));
	}

	@Override
	public boolean enable(UserLogin userLogin, String _id) {
		DETAIL domain = getMandatoryPO(userLogin, _id, true, true, getRepository());

		checkAccessible(userLogin, domain);

		getRepository().enable(userLogin.getClientId(), domain.getId());

		return true;
	}

	@Override
	public boolean delete(UserLogin userLogin, String _id) {
		DETAIL domain = getMandatoryPO(userLogin, _id, true, true, getRepository());

		checkAccessible(userLogin, domain);

		beforeDelete(userLogin, domain);

		return this.getRepository().delete(userLogin.getClientId(), domain.getId());
	}

	@Override
	public void setActive(UserLogin userLogin, String _id, boolean active) {
		DETAIL domain = getMandatoryPO(userLogin, _id, null, !active, getRepository());

		checkAccessible(userLogin, domain);

		beforeSetActive(userLogin, domain, active);

		if (active) {
			getRepository().activate(userLogin.getClientId(), domain.getId());
		} else {
			getRepository().deactivate(userLogin.getClientId(), domain.getId());
		}

		afterSetActive(userLogin, domain, active);
	}

	protected Category _getObjectForCreate(UserLogin userLogin, final CREATE_DTO createdto) {
		BusinessAssert.notNull(createdto);

		Category newCategory = new Category();
		String clientId = isClientRootFixed() ? commonRepository.getRootId() : userLogin.getClientId();

		initiatePO(clientId, newCategory);
		newCategory.setDraft(true);

		return updateCategoryFieldFromDto(userLogin, newCategory, createdto);
	}

	protected Category _getObjectForUpdate(UserLogin userLogin, I_Category oldCategory, final CREATE_DTO createdto) {
		BusinessAssert.notNull(createdto);

		Category newCategory = new Category(oldCategory);

		return updateCategoryFieldFromDto(userLogin, newCategory, createdto);
	}

	protected <D extends Category> D updateCategoryFieldFromDto(UserLogin userLogin, D category, CREATE_DTO createdto) {
		String distributorId = null;
		if (category.isDraft() && isUseDistributor()) {
			if (createdto.getDistributorId() != null) {
				I_Distributor distributor = getMandatoryPO(userLogin, createdto.getDistributorId(),
						distributorRepository);
				category.setDistributor(new CategoryEmbed(distributor));
			} else {
				I_Distributor distributor = getDefaultDistributor(userLogin);
				BusinessAssert.notNull(distributor,
						"No default Distributor found, consisder provide distributorId within Create DTO");

				category.setDistributor(new CategoryEmbed(distributor));
			}
		}
		if (category.getDistributor() != null) {
			distributorId = category.getDistributor().getId();
		}

		if (category.isDraft() || isCanChangeName()) {
			BusinessAssert.notNull(createdto.getName());
			BusinessAssert.notTrue(
					getRepository().checkNameExist(userLogin.getClientId(), category.getId(), createdto.getName(),
							distributorId),
					BusinessExceptionCode.NAME_USED,
					String.format("Record with [name=%s] already exist", createdto.getName()));
			category.setName(createdto.getName());
		}

		if (isUseCode() && !isAutoGenerateCode() && (category.isDraft() || isCanChangeCode())) {
			BusinessAssert.notNull(createdto.getCode());
			BusinessAssert.notTrue(
					getRepository().checkCodeExist(userLogin.getClientId(), category.getId(), createdto.getCode(),
							distributorId),
					BusinessExceptionCode.CODE_USED,
					String.format("Record with [code=%s] already exist", createdto.getCode()));
			category.setCode(createdto.getCode() != null ? createdto.getCode().toUpperCase() : null);
		}

		return category;
	}

}
