package com.viettel.backend.service.core.common;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.domain.common.I_Category;

public abstract class CategoryEditableServiceImpl<
        DOMAIN extends I_Category, 
        LIST_SIMPLE_DTO extends CategoryDto, LIST_DETAIL_DTO extends LIST_SIMPLE_DTO, CREATE_DTO extends CategoryCreateDto>
    extends
        CategoryComplexEditableServiceImpl<DOMAIN, DOMAIN, DOMAIN, LIST_SIMPLE_DTO, LIST_DETAIL_DTO, CREATE_DTO>
    implements CategoryEditableService<LIST_SIMPLE_DTO, LIST_DETAIL_DTO, CREATE_DTO> {

    protected abstract CategoryBasicRepository<DOMAIN> getRepository();

}
