package com.viettel.backend.service.core.common;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.oauth2.core.UserLogin;

/**
 * return only draft = false and active = true
 */
public interface CategoryReadonlyService<SIMPLE_DTO extends CategorySimpleDto> {
    
    public ListDto<SIMPLE_DTO> getAll(UserLogin userLogin, String distributorId);
    
    public SIMPLE_DTO getById(UserLogin userLogin, String id);
    
    public Class<SIMPLE_DTO> getSimpleDtoClass();
    
}
