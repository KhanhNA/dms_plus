package com.viettel.backend.service.core.common;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;

public abstract class CategoryReadonlyServiceImpl<DOMAIN extends I_Category, SIMPLE_DTO extends CategorySimpleDto>
        extends AbstractCategoryService<DOMAIN> implements CategoryReadonlyService<SIMPLE_DTO> {

    public abstract SIMPLE_DTO createSimpleDto(UserLogin userLogin, DOMAIN domain);

    private Class<SIMPLE_DTO> simpleDtoClass;

    @SuppressWarnings("unchecked")
    public CategoryReadonlyServiceImpl() {
        Class<?> superClazz = getClass();
        Type superType = superClazz.getGenericSuperclass();
        while (!(superType instanceof ParameterizedType)) {
            superClazz = superClazz.getSuperclass();
            superType = superClazz.getGenericSuperclass();
        }

        int paraIndex = 0;
        ParameterizedType genericSuperclass = (ParameterizedType) superType;
        this.simpleDtoClass = (Class<SIMPLE_DTO>) genericSuperclass.getActualTypeArguments()[paraIndex++];
    }

    @Override
    public ListDto<SIMPLE_DTO> getAll(UserLogin userLogin, String distributorId) {
        if (isUseDistributor()) {
            I_Distributor distributor = getMandatoryDistributor(userLogin, distributorId);
            distributorId = distributor.getId();
        } else {
            distributorId = null;
        }
        
        List<DOMAIN> domains = getRepository().getAll(userLogin.getClientId(), distributorId);
        if (CollectionUtils.isEmpty(domains)) {
            return ListDto.emptyList();
        }

        List<SIMPLE_DTO> dtos = new ArrayList<SIMPLE_DTO>(domains.size());
        for (DOMAIN domain : domains) {
            dtos.add(createSimpleDto(userLogin, domain));
        }

        return new ListDto<SIMPLE_DTO>(dtos, Long.valueOf(dtos.size()));
    }

    @Override
    public SIMPLE_DTO getById(UserLogin userLogin, String _id) {
        DOMAIN domain = getMandatoryPO(userLogin, _id, null, null, getRepository());

        checkAccessible(userLogin, domain);

        return createSimpleDto(userLogin, domain);
    }

    @Override
    public Class<SIMPLE_DTO> getSimpleDtoClass() {
        return simpleDtoClass;
    }

}
