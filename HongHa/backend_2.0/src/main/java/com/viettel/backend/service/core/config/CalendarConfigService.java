package com.viettel.backend.service.core.config;

import com.viettel.backend.dto.config.CalendarConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface CalendarConfigService {

    public CalendarConfigDto get(UserLogin userLogin);
    
    public void set(UserLogin userLogin, CalendarConfigDto dto);
    
}
