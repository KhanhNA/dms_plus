package com.viettel.backend.service.core.config;

import com.viettel.backend.dto.config.ClientConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface ClientConfigService {

    public ClientConfigDto get(UserLogin userLogin);
    
    public void set(UserLogin userLogin, ClientConfigDto dto);
    
}
