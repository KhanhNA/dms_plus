package com.viettel.backend.service.core.config;

import java.io.InputStream;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface MasterDataImportService {

    public InputStream getMasterDataTemplate(String lang);
    
    public ImportConfirmDto verify(UserLogin userLogin, String fileId);
    
    public void importMasterData(UserLogin userLogin, String clientId, String fileId);
    
}
