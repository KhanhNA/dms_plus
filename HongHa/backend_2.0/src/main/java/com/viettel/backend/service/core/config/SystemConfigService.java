package com.viettel.backend.service.core.config;

import com.viettel.backend.dto.config.SystemConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SystemConfigService {

    public SystemConfigDto get(UserLogin userLogin);
    
    public void set(UserLogin userLogin, SystemConfigDto dto);
    
}
