package com.viettel.backend.service.core.config.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.config.CalendarConfigDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.config.CalendarConfigService;
import com.viettel.backend.service.core.pdto.CalendarConfig;
import com.viettel.repository.common.CalendarConfigRepository;
import com.viettel.repository.common.domain.category.I_CalendarConfig;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;

@RolePermission(value={ Role.ADMIN })
@Service
public class CalendarConfigServiceImpl extends AbstractService implements CalendarConfigService {

    @Autowired
    private CalendarConfigRepository calendarConfigRepository;
    
    @Autowired
    private CacheCalendarService cacheCalendarService;
    
    @Override
    public CalendarConfigDto get(UserLogin userLogin) {
        I_CalendarConfig calendarConfig = calendarConfigRepository.getCalendarConfig(userLogin.getClientId());
        
        if (calendarConfig == null) {
            return new CalendarConfigDto();
        } else {
            return new CalendarConfigDto(calendarConfig);
        }
    }

    @Override
    public void set(UserLogin userLogin, CalendarConfigDto dto) {
        BusinessAssert.notNull(dto);
        BusinessAssert.notEmpty(dto.getWorkingDays());
        
        cacheCalendarService.clearByClient(userLogin.getClientId());
        
        CalendarConfig calendarConfig = new CalendarConfig();
        
        calendarConfig.setWorkingDays(dto.getWorkingDays());
        if (dto.getHolidays() != null) {
            List<SimpleDate> holidays = new ArrayList<SimpleDate>(dto.getHolidays().size());
            for (String holiday : dto.getHolidays()) {
                holidays.add(getMandatoryIsoDate(holiday));
            }
            
            Collections.sort(holidays);
            
            calendarConfig.setHolidays(holidays);
        }
        
        calendarConfigRepository.save(userLogin.getClientId(), calendarConfig);
    }

}
