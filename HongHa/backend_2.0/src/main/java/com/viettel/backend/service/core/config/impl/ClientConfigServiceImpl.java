package com.viettel.backend.service.core.config.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.config.ClientConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.config.ClientConfigService;
import com.viettel.backend.service.core.pdto.ClientConfig;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value={ Role.ADMIN, Role.SUPER_ADMIN })
@Service
public class ClientConfigServiceImpl extends AbstractService implements ClientConfigService {

    @Autowired
    private ConfigRepository configRepository;
    
    @Override
    public ClientConfigDto get(UserLogin userLogin) {
        I_Config config = configRepository.getConfig(userLogin.getClientId());
        
        if (config == null) {
            return new ClientConfigDto();
        } else {
            return new ClientConfigDto(config);
        }
    }

    @Override
    public void set(UserLogin userLogin, ClientConfigDto dto) {
        I_Config _config = configRepository.getConfig(userLogin.getClientId());

        ClientConfig clientConfig;
        
        if (_config != null) {
            clientConfig = new ClientConfig(_config);
        } else {
            clientConfig = new ClientConfig();
        }
        
        clientConfig.setVisitDistanceKPI(dto.getVisitDistanceKPI());
        clientConfig.setVisitDurationKPI(dto.getVisitDurationKPI());
        clientConfig.setCanEditCustomerLocation(dto.isCanEditCustomerLocation());
        clientConfig.setLocation(dto.getLocation());
        clientConfig.setProductivityUnit(dto.getProductivityUnit());
        
        configRepository.save(userLogin.getClientId(), clientConfig);
    }

}
