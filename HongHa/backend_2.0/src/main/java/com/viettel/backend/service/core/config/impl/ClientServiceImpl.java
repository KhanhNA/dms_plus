package com.viettel.backend.service.core.config.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.config.ClientCreateDto;
import com.viettel.backend.dto.config.ClientDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.common.AbstractCategoryService;
import com.viettel.backend.service.core.config.ClientService;
import com.viettel.backend.service.core.pdto.CalendarConfig;
import com.viettel.backend.service.core.pdto.Category;
import com.viettel.backend.service.core.pdto.ClientConfig;
import com.viettel.backend.service.core.pdto.Region;
import com.viettel.backend.service.core.pdto.User;
import com.viettel.backend.service.core.sub.GenerateDataService;
import com.viettel.backend.service.core.sub.MasterDataService;
import com.viettel.backend.util.PasswordUtils;
import com.viettel.repository.common.CalendarConfigRepository;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_CalendarConfig;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.SUPER_ADMIN, Role.SUPPORTER })
@Service
public class ClientServiceImpl extends AbstractCategoryService<I_Category> implements ClientService {

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private ConfigRepository configRepository;

	@Autowired
	private CalendarConfigRepository calendarConfigRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private CommonRepository commonRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private GenerateDataService generateDataService;

	@Autowired
	private CacheCalendarService cacheCalendarService;

	@Override
	protected ClientRepository getRepository() {
		return clientRepository;
	}

	@Override
	public ListDto<CategoryDto> getList(UserLogin userLogin, String search, Boolean active, Boolean draft,
			String distributorId, PageSizeRequest pageSizeRequest) {

		String searchName = search;
		String searchCode = search == null ? null : search.toUpperCase();

		List<I_Category> clients = getRepository().getList(userLogin.getClientId(), draft, active, null, searchName,
				searchCode, pageSizeRequest);

		if (CollectionUtils.isEmpty(clients) && pageSizeRequest.getPageNumber() == 0) {
			return ListDto.emptyList();
		}

		List<CategoryDto> dtos = new ArrayList<CategoryDto>(clients.size());
		for (I_Category client : clients) {
			dtos.add(new CategoryDto(client));
		}

		long size = Long.valueOf(dtos.size());
		if (pageSizeRequest != null) {
			if (pageSizeRequest.getPageNumber() > 0 || size == pageSizeRequest.getPageSize()) {
				size = getRepository().count(userLogin.getClientId(), draft, active, null, searchName, searchCode);
			}
		}

		return new ListDto<CategoryDto>(dtos, size);
	}

	@Override
	public ClientDto getById(UserLogin userLogin, String _id) {
		I_Category client = getMandatoryPO(userLogin, _id, clientRepository);

		I_Config config = configRepository.getConfig(client.getId());
		I_CalendarConfig calendarConfig = calendarConfigRepository.getCalendarConfig(client.getId());

		return new ClientDto(client, config, calendarConfig);
	}

	@Override
	public IdDto create(UserLogin userLogin, ClientCreateDto dto) {
		BusinessAssert.notNull(dto, "Create DTO cannot be null");
		BusinessAssert.notNull(dto.getCode());
		BusinessAssert.notNull(dto.getClientConfig());
		BusinessAssert.notNull(dto.getCalendarConfig());
		BusinessAssert.notNull(dto.getCalendarConfig().getWorkingDays());

		cacheCalendarService.clearByClient(userLogin.getClientId());

		Category _client = new Category();
		initiatePO(commonRepository.getRootId(), _client);

		_client.setDraft(false);
		_client.setName(dto.getName());
		_client.setCode(dto.getCode().toUpperCase());

		// Check exist
		BusinessAssert.notTrue(
				getRepository().checkNameExist(userLogin.getClientId(), _client.getId(), _client.getName(), null),
				BusinessExceptionCode.NAME_USED,
				String.format("Record with [name=%s] already exist", _client.getName()));

		BusinessAssert.notTrue(
				getRepository().checkCodeExist(userLogin.getClientId(), _client.getId(), _client.getCode(), null),
				BusinessExceptionCode.CODE_USED,
				String.format("Record with [code=%s] already exist", _client.getCode()));

		I_Category client = getRepository().save(userLogin.getClientId(), _client);

		// Create Admin
		User admin = new User();
		admin.setClientId(client.getId());
		admin.setActive(true);
		admin.setDraft(false);
		admin.setPassword(PasswordUtils.getDefaultPassword(passwordEncoder));
		admin.setUsername(client.getCode(), "admin");
		admin.setFullname("Admin");
		admin.setRole(Role.ADMIN);
		admin.setDefaultAdmin(true);
		userRepository.save(client.getId(), admin);

		// Client Config
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.setVisitDistanceKPI(dto.getClientConfig().getVisitDistanceKPI());
		clientConfig.setVisitDurationKPI(dto.getClientConfig().getVisitDurationKPI());
		clientConfig.setCanEditCustomerLocation(dto.getClientConfig().isCanEditCustomerLocation());
		clientConfig.setLocation(dto.getClientConfig().getLocation());
		clientConfig.setModules(dto.getClientConfig().getModules());
		clientConfig.setNumberDayInventoryExpire(dto.getClientConfig().getNumberDayInventoryExpire());
		clientConfig.setProductivityUnit(dto.getClientConfig().getProductivityUnit());

		configRepository.save(client.getId(), clientConfig);

		// Calendar config
		CalendarConfig calendarConfig = new CalendarConfig();
		calendarConfig.setWorkingDays(dto.getCalendarConfig().getWorkingDays());
		if (dto.getCalendarConfig().getHolidays() != null) {
			List<SimpleDate> holidays = new ArrayList<SimpleDate>(dto.getCalendarConfig().getHolidays().size());
			for (String holiday : dto.getCalendarConfig().getHolidays()) {
				holidays.add(getMandatoryIsoDate(holiday));
			}

			Collections.sort(holidays);

			calendarConfig.setHolidays(holidays);
		}

		calendarConfigRepository.save(client.getId(), calendarConfig);

		// Regions root
		Region root = new Region();
		root.setClientId(client.getId());
		root.setName(client.getName());
		root.setActive(true);
		root.setParent(null);
		root.setRoot(true);
		root.setLeaf(false);
		root.setChildren(Collections.emptyList());
		regionRepository.save(client.getId(), root);

		return new IdDto(client.getId());
	}

	@Override
	public IdDto update(UserLogin userLogin, String _id, ClientCreateDto dto) {
		BusinessAssert.notNull(dto, "Create DTO cannot be null");
		BusinessAssert.notNull(dto.getClientConfig());
		BusinessAssert.notNull(dto.getCalendarConfig());
		BusinessAssert.notNull(dto.getCalendarConfig().getWorkingDays());

		cacheCalendarService.clearByClient(userLogin.getClientId());

		I_Category client = getMandatoryPO(userLogin, _id, getRepository());

		// Client Config
		I_Config config = configRepository.getConfig(client.getId());
		ClientConfig _clientConfig;
		if (config == null) {
			_clientConfig = new ClientConfig();
		} else {
			_clientConfig = new ClientConfig(config);
		}

		_clientConfig.setVisitDistanceKPI(dto.getClientConfig().getVisitDistanceKPI());
		_clientConfig.setVisitDurationKPI(dto.getClientConfig().getVisitDurationKPI());
		_clientConfig.setCanEditCustomerLocation(dto.getClientConfig().isCanEditCustomerLocation());
		_clientConfig.setLocation(dto.getClientConfig().getLocation());
		_clientConfig.setModules(dto.getClientConfig().getModules());
		_clientConfig.setNumberDayInventoryExpire(dto.getClientConfig().getNumberDayInventoryExpire());
		_clientConfig.setProductivityUnit(dto.getClientConfig().getProductivityUnit());
		_clientConfig.setLimitAccount(dto.getClientConfig().getLimitAccount());

		configRepository.save(client.getId(), _clientConfig);

		// Calendar Config
		I_CalendarConfig calendarConfig = calendarConfigRepository.getCalendarConfig(client.getId());
		CalendarConfig _calendarConfig;
		if (calendarConfig == null) {
			_calendarConfig = new CalendarConfig();
		} else {
			_calendarConfig = new CalendarConfig(calendarConfig);
		}

		_calendarConfig.setWorkingDays(dto.getCalendarConfig().getWorkingDays());
		if (dto.getCalendarConfig().getHolidays() != null) {
			List<SimpleDate> holidays = new ArrayList<SimpleDate>(dto.getCalendarConfig().getHolidays().size());
			for (String holiday : dto.getCalendarConfig().getHolidays()) {
				holidays.add(getMandatoryIsoDate(holiday));
			}

			Collections.sort(holidays);

			_calendarConfig.setHolidays(holidays);
		}

		calendarConfigRepository.save(client.getId(), _calendarConfig);

		return new IdDto(client.getId());
	}

	@Override
	public boolean enable(UserLogin userLogin, String id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean delete(UserLogin userLogin, String id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setActive(UserLogin userLogin, String id, boolean active) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IdDto createAndEnable(UserLogin userLogin, ClientCreateDto createDto) {
		throw new UnsupportedOperationException();
	}

	@Override
	public IdDto updateAndEnable(UserLogin userLogin, String id, ClientCreateDto createDto) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void createSampleMasterData(UserLogin userLogin, String clientId) {
		I_Category client = getMandatoryPO(userLogin, clientId, clientRepository);
		ClassPathResource classPathResource = new ClassPathResource("demo-data.xlsx");
		try {
			masterDataService.importMasterData(client.getId(), classPathResource.getInputStream());
		} catch (IOException ex) {
			throw new UnsupportedOperationException(ex);
		}
	}

	@Override
	public void generateVisitAndOrder(UserLogin userLogin, String clientId) {
		I_Category client = getMandatoryPO(userLogin, clientId, clientRepository);
		generateDataService.generateVisitAndOrder(client, DateTimeUtils.getFirstOfLastMonth(),
				DateTimeUtils.getTomorrow());
	}

}