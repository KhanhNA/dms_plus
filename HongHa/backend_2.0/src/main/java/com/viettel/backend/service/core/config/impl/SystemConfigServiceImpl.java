package com.viettel.backend.service.core.config.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.config.SystemConfigDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheScheduleService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.config.SystemConfigService;
import com.viettel.backend.service.core.pdto.SystemConfig;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.SUPER_ADMIN })
@Service
public class SystemConfigServiceImpl extends AbstractService implements SystemConfigService {

    @Autowired
    private ConfigRepository configRepository;
    
    @Autowired
    private CacheScheduleService cacheScheduleCache;
    
    @Autowired
    private CommonRepository commonRepository;

    @Override
    public SystemConfigDto get(UserLogin userLogin) {
        I_Config config = configRepository.getConfig(commonRepository.getRootId());

        if (config == null) {
            return new SystemConfigDto();
        } else {
            return new SystemConfigDto(config);
        }
    }

    @Override
    public void set(UserLogin userLogin, SystemConfigDto dto) {
        cacheScheduleCache.onChange(null, null);
        
        String rootId = commonRepository.getRootId();
        
        I_Config _config = configRepository.getConfig(rootId);

        SystemConfig systemConfig;
        
        if (_config != null) {
            systemConfig = new SystemConfig(_config);
        } else {
            systemConfig = new SystemConfig();
        }

        systemConfig.setProductPhoto(dto.getProductPhoto());
        systemConfig.setDateFormat(dto.getDateFormat());
        systemConfig.setLocation(dto.getLocation());

        systemConfig.setFirstDayOfWeek(dto.getFirstDayOfWeek());
        systemConfig.setMinimalDaysInFirstWeek(dto.getMinimalDaysInFirstWeek());
        
        systemConfig.setNumberWeekOfFrequency(dto.getNumberWeekOfFrequency());
        
        systemConfig.setOrderDateType(dto.getOrderDateType());
        systemConfig.setNumberDayOrderPendingExpire(dto.getNumberDayOrderPendingExpire());

        configRepository.save(systemConfig);
    }

}
