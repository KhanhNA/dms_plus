package com.viettel.backend.service.core.customer;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface AreaEditableService extends CategoryEditableService<CategoryDto, CategoryDto, CategoryCreateDto> {

}
