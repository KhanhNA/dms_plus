package com.viettel.backend.service.core.customer;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface CustomerApprovalService {

	public ListDto<CustomerListDto> getPendingCustomers(UserLogin userLogin, PageSizeRequest pageSizeRequest);

	public CustomerDto getCustomerById(UserLogin userLogin, String customerId);

	public void approve(UserLogin userLogin, String customerId);

	public void approveMulti(UserLogin userLogin, String customerId);

	public void reject(UserLogin userLogin, String customerId);

	public void rejectMulti(UserLogin userLogin, String customerId);

	public ExportDto exportCustomer(UserLogin userLogin, String lang, String distributorId, String search,
			String fromDate, String toDate);

	public ExportDto exportPendingCustomer(UserLogin userLogin, String lang);

}
