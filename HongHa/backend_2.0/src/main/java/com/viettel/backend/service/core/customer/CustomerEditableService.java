package com.viettel.backend.service.core.customer;

import com.viettel.backend.dto.customer.CustomerCreateDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface CustomerEditableService
		extends CategoryEditableService<CustomerListDto, CustomerDto, CustomerCreateDto> {

}
