package com.viettel.backend.service.core.customer;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface CustomerImportService {
    
    public byte[] getImportCustomerTemplate(UserLogin userLogin, String _distributorId, String lang);
    
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId);
    
    public ImportResultDto importCustomer(UserLogin userLogin, String _distributorId, String fileId);
    
}
