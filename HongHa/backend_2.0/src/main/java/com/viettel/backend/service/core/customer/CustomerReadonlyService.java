package com.viettel.backend.service.core.customer;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface CustomerReadonlyService {

    public ListDto<CustomerListDto> getCustomerForReport(UserLogin userLogin, String distributorId, String search,
            PageSizeRequest pageSizeRequest);

    public CustomerDto getById(UserLogin userLogin, String id);

}
