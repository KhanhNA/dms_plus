package com.viettel.backend.service.core.customer;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerCreateDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface CustomerRegisterService {

    public IdDto registerCustomer(UserLogin userLogin, CustomerCreateDto customerCreateDto, boolean autoApprove);

    public ListDto<CustomerListDto> getCustomersRegistered(UserLogin userLogin, String search,
            PageSizeRequest pageSizeRequest);

}
