package com.viettel.backend.service.core.customer;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.service.core.common.CategoryReadonlyService;

public interface CustomerTypeReadonlyService extends CategoryReadonlyService<CategorySimpleDto> {

}
