package com.viettel.backend.service.core.customer.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractExportService;
import com.viettel.backend.service.core.customer.CustomerApprovalService;
import com.viettel.backend.service.core.customer.CustomerEditableService;
import com.viettel.backend.service.core.pdto.Customer;
import com.viettel.backend.service.core.sub.WebNotificationService;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.CustomerPendingRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.SUPERVISOR })
@Service
public class CustomerApprovalServiceImpl extends AbstractExportService implements CustomerApprovalService {

	@Autowired
	private CustomerPendingRepository customerPendingRepository;

	@Autowired
	private WebNotificationService webNotificationEngine;

	@Autowired
	private CustomerEditableService customerEditableService;

	@Override
	public ListDto<CustomerListDto> getPendingCustomers(UserLogin userLogin, PageSizeRequest pageSizeRequest) {
		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		if (distributors == null || distributors.isEmpty()) {
			return ListDto.emptyList();
		}

		Set<String> distributorIds = getIdSet(distributors);

		Collection<I_Customer> customers = customerPendingRepository
				.getPendingCustomersByDistributors(userLogin.getClientId(), distributorIds, pageSizeRequest);
		if (CollectionUtils.isEmpty(customers)) {
			if (pageSizeRequest != null) {
				if (pageSizeRequest.getPageNumber() == 0) {
					return ListDto.emptyList();
				}
			} else {
				return ListDto.emptyList();
			}
		}

		List<CustomerListDto> dtos = new ArrayList<CustomerListDto>(customers.size());
		for (I_Customer customer : customers) {
			dtos.add(new CustomerListDto(customer));
		}

		long size = Long.valueOf(dtos.size());
		if (pageSizeRequest != null) {
			if (pageSizeRequest.getPageNumber() > 0 || pageSizeRequest.getPageSize() == size) {
				size = customerPendingRepository.countPendingCustomersByDistributors(userLogin.getClientId(),
						distributorIds);
			}
		}

		return new ListDto<CustomerListDto>(dtos, size);
	}

	@Override
	public CustomerDto getCustomerById(UserLogin userLogin, String customerId) {
		I_Customer customer = getMandatoryPO(userLogin, customerId, customerPendingRepository);
		BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer is not accessible");
		BusinessAssert.equals(customer.getApproveStatus(), Customer.APPROVE_STATUS_PENDING, "customer not pending");

		CustomerDto dto = new CustomerDto(customer);

		return dto;
	}

	@Override
	public void approve(UserLogin userLogin, String customerId) {
		I_Customer customer = getMandatoryPO(userLogin, customerId, customerPendingRepository);

		BusinessAssert.isTrue(checkAccessible(userLogin, customer.getDistributor().getId()),
				"Distributor is not accessible");

		BusinessAssert.equals(customer.getApproveStatus(), Customer.APPROVE_STATUS_PENDING, "Customer is not pending");

		customerPendingRepository.approve(userLogin.getClientId(), getCurrentUser(userLogin),
				DateTimeUtils.getCurrentTime(), customer.getId());

		// Notify approved customer
		webNotificationEngine.notifyChangedCustomer(userLogin, customer,
				WebNotificationService.ACTION_CUSTOMER_APPROVE);
	}

	@Override
	public void approveMulti(UserLogin userLogin, String customerId) {

		List<String> ids = StringUtils.split(customerId, "_");
		List<I_Customer> customers = new ArrayList<>();
		for (String id : ids) {
			I_Customer customer = getMandatoryPO(userLogin, id, customerPendingRepository);

			BusinessAssert.isTrue(checkAccessible(userLogin, customer.getDistributor().getId()),
					"Distributor is not accessible");

			BusinessAssert.equals(customer.getApproveStatus(), Customer.APPROVE_STATUS_PENDING,
					"Customer is not pending");
			customers.add(customer);
		}

		try {
			customerPendingRepository.approveMulti(userLogin.getClientId(), getCurrentUser(userLogin),
					DateTimeUtils.getCurrentTime(), ids);
			for (I_Customer customer : customers) {
				// Notify approved customer
				webNotificationEngine.notifyChangedCustomer(userLogin, customer,
						WebNotificationService.ACTION_CUSTOMER_APPROVE);
			}
		} catch (Exception e) {

		}
	}

	@Override
	public void reject(UserLogin userLogin, String customerId) {
		I_Customer customer = getMandatoryPO(userLogin, customerId, customerPendingRepository);

		BusinessAssert.isTrue(checkAccessible(userLogin, customer.getDistributor().getId()),
				"Distributor is not accessible");

		BusinessAssert.equals(customer.getApproveStatus(), Customer.APPROVE_STATUS_PENDING, "Customer is not pending");

		customerPendingRepository.reject(userLogin.getClientId(), getCurrentUser(userLogin),
				DateTimeUtils.getCurrentTime(), customer.getId());

		// Notify rejected customer
		webNotificationEngine.notifyChangedCustomer(userLogin, customer, WebNotificationService.ACTION_CUSTOMER_REJECT);
	}

	@Override
	public void rejectMulti(UserLogin userLogin, String customerId) {
		List<String> ids = StringUtils.split(customerId, "_");
		List<I_Customer> customers = new ArrayList<>();
		for (String id : ids) {
			I_Customer customer = getMandatoryPO(userLogin, id, customerPendingRepository);

			BusinessAssert.isTrue(checkAccessible(userLogin, customer.getDistributor().getId()),
					"Distributor is not accessible");

			BusinessAssert.equals(customer.getApproveStatus(), Customer.APPROVE_STATUS_PENDING,
					"Customer is not pending");
			customers.add(customer);
		}

		customerPendingRepository.rejectMulti(userLogin.getClientId(), getCurrentUser(userLogin),
				DateTimeUtils.getCurrentTime(), ids);
		for (I_Customer customer : customers) {
			// Notify approved customer
			webNotificationEngine.notifyChangedCustomer(userLogin, customer,
					WebNotificationService.ACTION_CUSTOMER_REJECT);
		}
	}

	@Override
	public ExportDto exportCustomer(UserLogin userLogin, String lang, String distributorId, String search,
			String _fromDate, String _toDate) {
		lang = getLang(lang);
		I_Config config = getConfig(userLogin);
		List<CustomerListDto> listAllCustomer = customerEditableService.getList(userLogin, search, null, false,
				"all".equalsIgnoreCase(distributorId) ? null : distributorId, null).getList();
		for (CustomerListDto customer : listAllCustomer) {
			customer.setLastModifiedTime(customer.getLastModifiedTime() == null
					? (customer.getApprovedTime() == null ? customer.getCreatedTime() : customer.getApprovedTime())
					: customer.getLastModifiedTime());
		}
		Collections.sort(listAllCustomer, new Comparator<CustomerListDto>() {
			@Override
			public int compare(CustomerListDto o1, CustomerListDto o2) {
				if (o1.getDistributor().getName().compareToIgnoreCase(o2.getDistributor().getName()) == 0) {
					if (o1.getArea().getName().compareToIgnoreCase(o2.getArea().getName()) == 0) {
						return o1.getName().compareToIgnoreCase(o1.getName());
					} else {
						return o1.getArea().getName().compareToIgnoreCase(o2.getArea().getName());
					}
				} else {
					return o1.getDistributor().getName().compareToIgnoreCase(o2.getDistributor().getName());
				}
			}
		});

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "customer"));

			Row row;

			String[] headers = new String[] { "customer.name", "customer.code", "distributor.name", "customer.area",
					"mobile", "customer.type", "created.by", "created.date", "customer.approved.by",
					"customer.approved.time", "active" };

			int lastCol = headers.length - 1;

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "customer"));
			row = sheet.createRow(2);
			sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "export.date") + ": " + DateTimeUtils.getToday().format(config.getDateFormat()));

			// Create table headers
			int rownum = 4;
			row = sheet.createRow(rownum++);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);

			for (CustomerListDto customer : listAllCustomer) {
				int col = 0;
				row = sheet.createRow(rownum++);
				createCell(row, col++, textCellStyle, customer.getName());
				createCell(row, col++, textCellStyle, customer.getCode());
				createCell(row, col++, textCellStyle, customer.getDistributor().getName());
				createCell(row, col++, textCellStyle, customer.getArea().getName());
				createCell(row, col++, textCellStyle, customer.getMobile());
				createCell(row, col++, textCellStyle, customer.getCustomerType().getName());
				createCell(row, col++, textCellStyle, customer.getCreatedBy().getFullname());
				createCell(row, col++, textCellStyle,
						getMandatoryIsoDate(customer.getCreatedTime()).format(config.getDateFormat()));
				createCell(row, col++, textCellStyle,
						customer.getApprovedBy() == null ? "" : customer.getApprovedBy().getFullname());
				createCell(row, col++, textCellStyle, customer.getApprovedTime() == null ? ""
						: getMandatoryIsoDate(customer.getApprovedTime()).format(config.getDateFormat()));
				createCell(row, col++, textCellStyle, customer.isActive() ? "x" : "");
			}

			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
			createCell(row, 0, footerTextCellStyle, listAllCustomer.size() + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			StringBuilder fileName = new StringBuilder();
			fileName.append("CustomerList_").append(DateTimeUtils.getCurrentTime().format("yyyyMMddHHmm"))
					.append(".xlsx");

			File outTempFile = File.createTempFile(fileName + "_" + System.currentTimeMillis(), ".tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new ExportDto(fileName.toString(), new FileInputStream(outTempFile));
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

	@Override
	public ExportDto exportPendingCustomer(UserLogin userLogin, String lang) {
		lang = getLang(lang);
		I_Config config = getConfig(userLogin);
		ListDto<CustomerListDto> listCustomerDtoPending = getPendingCustomers(userLogin, null);
		List<CustomerListDto> listCustomerPending = listCustomerDtoPending.getList();

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "customer.pending"));

			Row row;

			String[] headers = new String[] { "distributor.name", "customer.name", "customer.code", "customer.type",
					"customer.area", "mobile", "created.by", "created.date" };

			int lastCol = headers.length - 1;

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "customer.pending"));
			row = sheet.createRow(2);
			sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "export.date") + ": " + DateTimeUtils.getToday().format(config.getDateFormat()));

			// Create table headers
			int rownum = 4;
			row = sheet.createRow(rownum++);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);

			for (CustomerListDto customer : listCustomerPending) {
				int col = 0;
				row = sheet.createRow(rownum++);
				createCell(row, col++, textCellStyle, customer.getDistributor().getName());
				createCell(row, col++, textCellStyle, customer.getName());
				createCell(row, col++, textCellStyle, customer.getCode());
				createCell(row, col++, textCellStyle, customer.getCustomerType().getName());
				createCell(row, col++, textCellStyle, customer.getArea().getName());
				createCell(row, col++, textCellStyle, customer.getMobile());
				createCell(row, col++, textCellStyle, customer.getCreatedBy().getFullname());
				createCell(row, col++, textCellStyle,
						getMandatoryIsoDate(customer.getCreatedTime()).format(config.getDateFormat()));
			}

			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
			createCell(row, 0, footerTextCellStyle, listCustomerPending.size() + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			StringBuilder fileName = new StringBuilder();
			fileName.append("CustomerPendingList_").append(DateTimeUtils.getCurrentTime().format("yyyyMMddHHmm"))
					.append(".xlsx");

			File outTempFile = File.createTempFile(fileName + "_" + System.currentTimeMillis(), ".tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new ExportDto(fileName.toString(), new FileInputStream(outTempFile));
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}
}
