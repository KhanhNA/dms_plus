package com.viettel.backend.service.core.customer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.customer.CustomerCreateDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.CategoryEditableServiceImpl;
import com.viettel.backend.service.core.customer.CustomerEditableService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.Customer;
import com.viettel.repository.common.AreaRepository;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.CustomerTypeRepository;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;
import com.viettel.repository.common.util.LocationUtils;

@RolePermission(value = { Role.ADMIN, Role.ADVANCE_SUPERVISOR })
@Service
public class CustomerEditableServiceImpl
		extends CategoryEditableServiceImpl<I_Customer, CustomerListDto, CustomerDto, CustomerCreateDto>
		implements CustomerEditableService {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private AreaRepository areaRepository;

	@Autowired
	private CustomerTypeRepository customerTypeRepository;

	@Autowired
	private CodeGeneratorRepository codeGeneratorRepository;

	@Override
	protected boolean isUseDistributor() {
		return true;
	}

	@Override
	protected boolean isUseCode() {
		return true;
	}

	@Override
	protected boolean isAutoGenerateCode() {
		return true;
	}

	@Override
	protected CategoryBasicRepository<I_Customer> getRepository() {
		return customerRepository;
	}

	@Override
	protected void beforeSetActive(UserLogin userLogin, I_Customer domain, boolean active) {
		if (active) {
			// ACTIVE
			if (domain.getArea() != null) {
				BusinessAssert.isTrue(
						areaRepository.exists(userLogin.getClientId(), false, true, domain.getArea().getId()),
						BusinessExceptionCode.AREA_NOT_FOUND, "area not found");
			}

			if (domain.getCustomerType() != null) {
				BusinessAssert.isTrue(
						customerTypeRepository.exists(userLogin.getClientId(), false, true,
								domain.getCustomerType().getId()),
						BusinessExceptionCode.CUSTOMER_TYPE_NOT_FOUND, "customer type not found");
			}

		} else {
			// DEACTIVE
			BusinessAssert.isTrue(domain.getSchedule() == null, BusinessExceptionCode.CUSTOMER_HAS_SCHEDULE,
					"customer has schedule");
		}
	}

	@Override
	public CustomerListDto createListSimpleDto(UserLogin userLogin, I_Customer domain) {
		return new CustomerListDto(domain);
	}

	@Override
	public CustomerDto createListDetailDto(UserLogin userLogin, I_Customer domain) {
		return new CustomerDto(domain);
	}

	@Override
	public I_Customer getObjectForCreate(UserLogin userLogin, CustomerCreateDto createdto) {
		BusinessAssert.notNull(createdto);

		Customer customer = new Customer();
		initiatePO(userLogin.getClientId(), customer);
		customer.setDraft(true);

		SimpleDate currentTime = DateTimeUtils.getCurrentTime();
		I_User currentUser = getCurrentUser(userLogin);

		customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
		customer.setCreatedTime(currentTime);
		customer.setCreatedBy(currentUser);
		customer.setApprovedTime(currentTime);
		customer.setApprovedBy(currentUser);
		customer.setCode(codeGeneratorRepository.getCustomerCode(userLogin.getClientId().toString()));

		return getObjectForUpdate(userLogin, customer, createdto);
	}

	@Override
	public I_Customer getObjectForUpdate(UserLogin userLogin, I_Customer oldDomain, CustomerCreateDto createdto) {
		BusinessAssert.notNull(createdto);
		BusinessAssert.notNull(createdto.getMobile());
		BusinessAssert.notNull(createdto.getLocation());
		BusinessAssert.isTrue(LocationUtils.checkLocationValid(createdto.getLocation()));

		Customer customer = new Customer(oldDomain);
		customer = updateCategoryFieldFromDto(userLogin, customer, createdto);

		if (customer.isDraft()) {
			BusinessAssert.notNull(createdto.getCustomerTypeId());
			BusinessAssert.notNull(createdto.getAreaId());

			I_Category customerType = getMandatoryPO(userLogin, createdto.getCustomerTypeId(), customerTypeRepository);
			customer.setCustomerType(new CategoryEmbed(customerType));

			I_Category area = getMandatoryPO(userLogin, createdto.getAreaId(), areaRepository);
			customer.setArea(new CategoryEmbed(area));
		}

		customer.setMobile(createdto.getMobile());
		customer.setPhone(createdto.getPhone());
		customer.setContact(createdto.getContact());
		customer.setEmail(createdto.getEmail());
		customer.setLocation(createdto.getLocation());

		return customer;
	}

}
