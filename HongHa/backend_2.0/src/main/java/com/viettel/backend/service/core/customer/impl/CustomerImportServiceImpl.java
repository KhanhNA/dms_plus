package com.viettel.backend.service.core.customer.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.dto.common.ImportConfirmDto.RowData;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractImportService;
import com.viettel.backend.service.core.customer.CustomerImportService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.Customer;
import com.viettel.backend.service.core.pdto.UserEmbed;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.AreaRepository;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.CustomerTypeRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.SUPERVISOR })
@Service
public class CustomerImportServiceImpl extends AbstractImportService implements CustomerImportService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    // PUBLIC
    @Override
    public byte[] getImportCustomerTemplate(UserLogin userLogin, String _distributorId, String lang) {
        lang = getLang(lang);

        I_Distributor distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet customerSheet = workbook.createSheet(translate(lang, "customer"));
        XSSFRow row = customerSheet.createRow(0);
        row.createCell(0).setCellValue(translate(lang, "name") + "*");
        row.createCell(1).setCellValue(translate(lang, "customer.type") + "*");
        row.createCell(2).setCellValue(translate(lang, "area") + "*");
        row.createCell(3).setCellValue(translate(lang, "mobile") + "*");
        row.createCell(4).setCellValue(translate(lang, "phone"));
        row.createCell(5).setCellValue(translate(lang, "contact"));
        row.createCell(6).setCellValue(translate(lang, "email"));
        row.createCell(7).setCellValue(translate(lang, "latitude") + "*");
        row.createCell(8).setCellValue(translate(lang, "longitude") + "*");

        // CUSTOMER TYPE
        List<I_Category> customerTypes = customerTypeRepository.getAll(userLogin.getClientId(), null);
        int index = 0;
        XSSFSheet routeSheet = workbook.createSheet(translate(lang, "customer.type"));
        for (I_Category customerType : customerTypes) {
            row = routeSheet.createRow(index);
            row.createCell(0).setCellValue(customerType.getName());
            index++;
        }

        // AREA
        List<I_Category> areas = areaRepository.getAll(userLogin.getClientId(), distributor.getId());
        index = 0;
        XSSFSheet areaSheet = workbook.createSheet(translate(lang, "area"));
        for (I_Category area : areas) {
            row = areaSheet.createRow(index);
            row.createCell(0).setCellValue(area.getName());
            index++;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);

            return baos.toByteArray();
        } catch (IOException e) {
            logger.error("error write file", e);
            throw new UnsupportedOperationException(e);
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }

    private I_CellValidator[] getValidators(UserLogin userLogin, String distributorId) {
        Map<String, I_Category> customerTypeMap = getCustomerTypeMap(userLogin.getClientId());
        Map<String, I_Category> areaMap = getAreaMap(userLogin.getClientId());

        List<I_Customer> customers = customerRepository.getAll(userLogin.getClientId(), distributorId);
        HashSet<String> customerNames = new HashSet<String>();
        for (I_Customer customer : customers) {
            if (!StringUtils.isNullOrEmpty(customer.getName(), true)) {
                customerNames.add(customer.getName().trim().toUpperCase());
            }
        }

        I_CellValidator[] validators = new I_CellValidator[] {
                new MultiCellValidator(new StringMandatoryCellValidator(), new StringUniqueCellValidator(customerNames),
                        new MaxLengthCellValidator(50)),
                new MultiCellValidator(new StringMandatoryCellValidator(),
                        new ReferenceCellValidator<I_Category>(customerTypeMap)),
                new MultiCellValidator(new StringMandatoryCellValidator(),
                        new ReferenceCellValidator<I_Category>(areaMap)),
                new MultiCellValidator(new StringMandatoryCellValidator(), new MaxLengthCellValidator(15)), null, null,
                null, new LatitudeCellValidator(), new LongitudeCellValidator() };
        return validators;
    }

    @Override
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId) {
        I_Distributor distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");

        I_CellValidator[] validators = getValidators(userLogin, distributor.getId());
        return getErrorRows(userLogin, fileId, validators);
    }

    @Override
    public ImportResultDto importCustomer(UserLogin userLogin, String _distributorId, String fileId) {
        I_Distributor distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");

        I_User currentUser = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());

        I_CellValidator[] validators = getValidators(userLogin, distributor.getId());
        ImportConfirmDto dto = getValidRows(userLogin, fileId, validators);

        List<String> codes = codeGeneratorRepository.getCustomerCodeBatch(userLogin.getClientId().toString(),
                dto.getRowDatas().size());

        List<I_Customer> customersToInsert = new ArrayList<I_Customer>(dto.getRowDatas().size());
        int index = 0;
        for (RowData row : dto.getRowDatas()) {
            Customer customer = new Customer();
            initiatePO(userLogin.getClientId(), customer);
            customer.setDraft(false);
            customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
            customer.setCreatedTime(DateTimeUtils.getCurrentTime());
            customer.setApprovedTime(DateTimeUtils.getCurrentTime());
            customer.setCreatedBy(new UserEmbed(currentUser));
            customer.setApprovedBy(new UserEmbed(currentUser));
            customer.setCode(codes.get(index));
            customer.setDistributor(new CategoryEmbed(distributor));

            customer.setName((String) row.getDatas().get(0));
            customer.setCustomerType((I_Category) row.getDatas().get(1));
            customer.setArea((I_Category) row.getDatas().get(2));
            customer.setMobile((String) row.getDatas().get(3));
            customer.setPhone(row.getDatas().get(4) == null ? null : (String) row.getDatas().get(4));
            customer.setContact(row.getDatas().get(5) == null ? null : (String) row.getDatas().get(5));
            customer.setEmail(row.getDatas().get(6) == null ? null : (String) row.getDatas().get(6));

            BigDecimal latitude = (BigDecimal) row.getDatas().get(7);
            BigDecimal longitude = (BigDecimal) row.getDatas().get(8);

            customer.setLocation(new Location(latitude == null ? null : latitude.doubleValue(),
                    longitude == null ? null : longitude.doubleValue()));
            customersToInsert.add(customer);

            index++;
        }

        if (!customersToInsert.isEmpty()) {
            customerRepository.insertBatch(userLogin.getClientId(), customersToInsert);
        }

        return new ImportResultDto(dto.getTotal(), customersToInsert.size());
    }

    private Map<String, I_Category> getCustomerTypeMap(String clientId) {
        List<I_Category> customerTypes = customerTypeRepository.getAll(clientId, null);

        HashMap<String, I_Category> map = new HashMap<String, I_Category>();

        if (customerTypes != null) {
            for (I_Category customerType : customerTypes) {
                map.put(customerType.getName().toUpperCase(), customerType);
            }
        }

        return map;
    }

    private Map<String, I_Category> getAreaMap(String clientId) {
        List<I_Category> areas = areaRepository.getAll(clientId, null);

        HashMap<String, I_Category> map = new HashMap<String, I_Category>();

        if (areas != null) {
            for (I_Category area : areas) {
                map.put(area.getName().toUpperCase(), area);
            }
        }

        return map;
    }

}
