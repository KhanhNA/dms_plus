package com.viettel.backend.service.core.customer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.customer.CustomerEditableService;
import com.viettel.backend.service.core.customer.CustomerReadonlyService;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN, Role.ADVANCE_SUPERVISOR })
@Service
public class CustomerReadonlyServiceImpl implements CustomerReadonlyService {

	@Autowired
	private CustomerEditableService editableCustomerService;

	@Override
	public ListDto<CustomerListDto> getCustomerForReport(UserLogin userLogin, String _distributorId, String search,
			PageSizeRequest pageSizeRequest) {
		return editableCustomerService.getList(userLogin, search, true, false, "all".equalsIgnoreCase(_distributorId) ? null : _distributorId, pageSizeRequest);
	}

	@Override
	public CustomerDto getById(UserLogin userLogin, String id) {
		return editableCustomerService.getById(userLogin, id);
	}

}
