package com.viettel.backend.service.core.customer.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerCreateDto;
import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.customer.CustomerRegisterService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.Customer;
import com.viettel.backend.service.core.pdto.UserEmbed;
import com.viettel.backend.service.core.sub.WebNotificationService;
import com.viettel.repository.common.AreaRepository;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.CustomerPendingRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.CustomerTypeRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.util.DateTimeUtils;
import com.viettel.repository.common.util.LocationUtils;

@RolePermission(value = { Role.SALESMAN })
@Service
public class CustomerRegisterServiceImpl extends AbstractService implements CustomerRegisterService {

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private WebNotificationService webNotificationEngine;

    @Override
    public IdDto registerCustomer(UserLogin userLogin, CustomerCreateDto createdto, boolean autoApprove) {
        BusinessAssert.notNull(createdto, "Create DTO cannot be null");
        BusinessAssert.notNull(createdto.getMobile());
        BusinessAssert.notNull(createdto.getLocation());
        BusinessAssert.notNull(createdto.getAreaId());
        BusinessAssert.notNull(createdto.getCustomerTypeId());

        BusinessAssert.isTrue(LocationUtils.checkLocationValid(createdto.getLocation()), "Invalid location param");

        I_User createdUser = getCurrentUser(userLogin);

        Customer customer = new Customer();
        initiatePO(userLogin.getClientId(), customer);
        customer.setDraft(false);

        if (autoApprove) {
            customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
        } else {
            customer.setApproveStatus(Customer.APPROVE_STATUS_PENDING);
        }
        customer.setName(createdto.getName());
        customer.setCode(codeGeneratorRepository.getCustomerCode(userLogin.getClientId().toString()));
        customer.setCreatedTime(DateTimeUtils.getCurrentTime());
        customer.setCreatedBy(new UserEmbed(createdUser));

        customer.setMobile(createdto.getMobile());
        customer.setPhone(createdto.getPhone());
        customer.setContact(createdto.getContact());
        customer.setEmail(createdto.getEmail());

        I_Distributor distributor;
        if (createdto.getDistributorId() != null) {
            distributor = getMandatoryPO(userLogin, createdto.getDistributorId(), distributorRepository);

            BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "Distributor is not accessible");

            customer.setDistributor(new CategoryEmbed(distributor));
        } else {
            distributor = getDefaultDistributor(userLogin);

            BusinessAssert.notNull(distributor, "Distributor is required");

            customer.setDistributor(new CategoryEmbed(distributor));
        }

        I_Category customerType = getMandatoryPO(userLogin, createdto.getCustomerTypeId(), customerTypeRepository);
        customer.setCustomerType(new CategoryEmbed(customerType));

        I_Category area = getMandatoryPO(userLogin, createdto.getAreaId(), areaRepository);
        customer.setArea(new CategoryEmbed(area));

        customer.setLocation(createdto.getLocation());

        // Check exist
        BusinessAssert.notTrue(
                customerRepository.checkNameExist(userLogin.getClientId(), customer.getId(), customer.getName(),
                        distributor.getId()),
                BusinessExceptionCode.NAME_USED,
                String.format("Record with [name=%s] already exist", customer.getName()));


        I_Customer _customer = customerRepository.save(userLogin.getClientId(), customer);
        
        if (!autoApprove) {
            // Notify
            webNotificationEngine.notifyChangedCustomer(userLogin, _customer, WebNotificationService.ACTION_CUSTOMER_ADD);
        }

        return new IdDto(_customer);
    }

    @Override
    public ListDto<CustomerListDto> getCustomersRegistered(UserLogin userLogin, String search,
            PageSizeRequest pageSizeRequest) {
        String clientId = userLogin.getClientId();
        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();
        Collection<String> salesmanIds = Collections.singleton(salesman.getId());

        Collection<I_Customer> customers = customerPendingRepository.getCustomersByCreatedUsers(clientId,
                distributorId, salesmanIds, null, search, search, pageSizeRequest);
        
        if (CollectionUtils.isEmpty(customers) && pageSizeRequest.getPageNumber() == 0) {
            return ListDto.emptyList();
        }

        List<CustomerListDto> dtos = new ArrayList<CustomerListDto>(customers.size());
        for (I_Customer customer : customers) {
            CustomerListDto dto = new CustomerListDto(customer);
            dtos.add(dto);
        }

        long size = customers.size();
        if (pageSizeRequest != null) {
            if (pageSizeRequest.getPageNumber() > 0 || pageSizeRequest.getPageSize() == size) {
                size = customerPendingRepository.countCustomersByCreatedUsers(clientId, distributorId, salesmanIds,
                        null, search, search);
            }
        }

        return new ListDto<CustomerListDto>(dtos, size);
    }

}
