package com.viettel.backend.service.core.customer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.CategoryReadonlyServiceImpl;
import com.viettel.backend.service.core.customer.CustomerTypeReadonlyService;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.CustomerTypeRepository;
import com.viettel.repository.common.domain.common.I_Category;

@Service
public class CustomerTypeReadonlyServiceImpl extends CategoryReadonlyServiceImpl<I_Category, CategorySimpleDto>
        implements CustomerTypeReadonlyService {

    @Autowired
    private CustomerTypeRepository customerTypeRepository;

    @Override
    public CategorySimpleDto createSimpleDto(UserLogin userLogin, I_Category domain) {
        return new CategorySimpleDto(domain);
    }

    @Override
    protected CategoryBasicRepository<I_Category> getRepository() {
        return customerTypeRepository;
    }

}
