package com.viettel.backend.service.core.dashboard;

import com.viettel.backend.dto.dashboard.WebDashboardDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface WebDashboardRegionService {

    public WebDashboardDto getWebDashboard(UserLogin userLogin);
    
}
