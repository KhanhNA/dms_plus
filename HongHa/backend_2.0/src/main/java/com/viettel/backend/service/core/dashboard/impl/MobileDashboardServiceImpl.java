package com.viettel.backend.service.core.dashboard.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.common.ProgressDto;
import com.viettel.backend.dto.dashboard.MobileDashboardDto;
import com.viettel.backend.dto.order.CustomerSalesResultDto;
import com.viettel.backend.dto.order.OrderSimpleDto;
import com.viettel.backend.dto.order.SalesResultDailyDto;
import com.viettel.backend.dto.order.SalesResultDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.cache.CacheScheduleService;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.cache.CacheVisitOrderService.DateType;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.dashboard.MobileDashboardService;
import com.viettel.backend.util.entity.IncrementHashMap;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.OrderRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.TargetRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Target;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_CustomerEmbed;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.SALESMAN })
@Service
public class MobileDashboardServiceImpl extends AbstractService implements MobileDashboardService {

    @Autowired
    private CacheVisitOrderService cacheVisitOrderService;

    @Autowired
    private CacheCalendarService cacheCalendarService;

    @Autowired
    private CacheScheduleService cacheScheduleService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public MobileDashboardDto getMobileDashboard(UserLogin userLogin) {
        SimpleDate today = DateTimeUtils.getToday();

        List<SimpleDate> workingDaysAllMonth = cacheCalendarService.getWorkingDays(userLogin.getClientId(),
                DateTimeUtils.getPeriodThisMonth());

        List<SimpleDate> workingDaysUntilToday = cacheCalendarService.getWorkingDays(userLogin.getClientId(),
                DateTimeUtils.getPeriodThisMonthUntilToday());

        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();
        Collection<String> salesmanIds = Collections.singleton(salesman.getId());

        double revenue = cacheVisitOrderService.getRevenueByCreated(userLogin.getClientId(), DateType.MONTHLY, today,
                distributorId, salesmanIds);
        
        double subRevenue = cacheVisitOrderService.getSubRevenueByCreated(userLogin.getClientId(), DateType.MONTHLY, today,
                distributorId, salesmanIds);
        
        double productitvity = cacheVisitOrderService.getProductivityByCreated(userLogin.getClientId(),
                DateType.MONTHLY, today, distributorId, salesmanIds);

        int nbVisit = (int) cacheVisitOrderService.getNbVisitBySalesman(userLogin.getClientId(), DateType.MONTHLY,
                today, distributorId, salesmanIds);

        // GET TARGET
        List<I_Target> targets = targetRepository.getTargetsBySalesmen(userLogin.getClientId(), salesmanIds,
                today.getMonth(), today.getYear(), false);
        BigDecimal revenueTarget = BigDecimal.ZERO;
        BigDecimal subRevenueTarget = BigDecimal.ZERO;
        if (targets != null && !targets.isEmpty()) {
            for (I_Target target : targets) {
                revenueTarget = revenueTarget.add(target.getRevenue());
                subRevenueTarget = subRevenueTarget.add(target.getSubRevenue());
            }
        }

        // VISIT PLANNED
        Map<String, Integer> nbVisitPlannedByRouteThisMonth = cacheScheduleService.getNbVisitPlannedByRoute(
                userLogin.getClientId(), Collections.singleton(distributorId), DateTimeUtils.getPeriodThisMonth());
        List<I_Route> routes = routeRepository.getRoutesBySalesmen(userLogin.getClientId(), distributorId, salesmanIds);

        Integer nbVisitPlanned = 0;
        for (I_Route route : routes) {
            Integer tmp = nbVisitPlannedByRouteThisMonth.get(route.getId());
            tmp = tmp == null ? 0 : tmp;
            nbVisitPlanned += tmp;
        }

        return new MobileDashboardDto(new ProgressDto(revenueTarget.doubleValue(), revenue),new ProgressDto(subRevenueTarget.doubleValue(), subRevenue),
                new ProgressDto(0.0, productitvity), new ProgressDto(nbVisitPlanned, nbVisit),
                new ProgressDto(workingDaysAllMonth.size(), workingDaysUntilToday.size()));
    }

    @Override
    public ListDto<CustomerSalesResultDto> getCustomerSalesResultsThisMonth(UserLogin userLogin) {
        Period thisMonthPeriod = DateTimeUtils.getPeriodThisMonth();

        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();

        I_Config config = getConfig(userLogin);

        List<I_OrderHeader> orders = orderRepository.getOrderHeadersByDistributor(userLogin.getClientId(),
                distributorId, salesman.getId(), null, thisMonthPeriod, config.getOrderDateType(), null, false);

        if (orders == null || orders.isEmpty()) {
            return ListDto.emptyList();
        }

        Map<String, I_CustomerEmbed> customerById = new HashMap<>();
        IncrementHashMap<String> revenueByCustomer = new IncrementHashMap<>();
        IncrementHashMap<String> subRevenueByCustomer = new IncrementHashMap<>();
        IncrementHashMap<String> productivityByCustomer = new IncrementHashMap<>();
        IncrementHashMap<String> nbOrderByCustomer = new IncrementHashMap<>();

        for (I_OrderHeader order : orders) {
            customerById.put(order.getCustomer().getId(), order.getCustomer());
            revenueByCustomer.increment(order.getCustomer().getId(), order.getGrandTotal());
            subRevenueByCustomer.increment(order.getCustomer().getId(), order.getSubTotal());
            productivityByCustomer.increment(order.getCustomer().getId(), order.getProductivity());
            nbOrderByCustomer.increment(order.getCustomer().getId(), 1);
        }

        List<CustomerSalesResultDto> dtos = new ArrayList<>(customerById.size());
        for (I_CustomerEmbed customer : customerById.values()) {
            BigDecimal revenue = revenueByCustomer.get(customer.getId());
            BigDecimal subRevenue = subRevenueByCustomer.get(customer.getId());
            BigDecimal productivity = productivityByCustomer.get(customer.getId());
            int nbOrder = nbOrderByCustomer.getIntValue(customer.getId());

            CustomerSalesResultDto dto = new CustomerSalesResultDto(customer,
                    new SalesResultDto(revenue, subRevenue, productivity, nbOrder));

            dtos.add(dto);
        }

        return new ListDto<>(dtos);
    }

    @Override
    public ListDto<OrderSimpleDto> getOrderByCustomerThisMonth(UserLogin userLogin, String customerId) {
        I_Customer customer = getMandatoryPO(userLogin, customerId, false, null, customerRepository);
        // CHECK ONLY DISTRIBUTOR
        BusinessAssert.isTrue(checkAccessible(userLogin, customer.getDistributor().getId()), "customer not accessible");

        Period thisMonthPeriod = DateTimeUtils.getPeriodThisMonth();

        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();

        I_Config config = getConfig(userLogin);

        List<I_OrderHeader> orders = orderRepository.getOrderHeadersByDistributor(userLogin.getClientId(),
                distributorId, salesman.getId(), customerId, thisMonthPeriod, config.getOrderDateType(), null, false);

        if (orders == null || orders.isEmpty()) {
            return ListDto.emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<>(orders.size());
        for (I_OrderHeader order : orders) {
            dtos.add(new OrderSimpleDto(order));
        }

        return new ListDto<>(dtos);
    }

    @Override
    public ListDto<SalesResultDailyDto> getSalesResultDailyThisMonth(UserLogin userLogin) {
        List<SimpleDate> workingDaysUntilToday = cacheCalendarService.getWorkingDays(userLogin.getClientId(),
                DateTimeUtils.getPeriodThisMonthUntilToday());

        if (workingDaysUntilToday == null || workingDaysUntilToday.isEmpty()) {
            return ListDto.emptyList();
        }

        I_User salesman = getCurrentUser(userLogin);
        Collection<String> salesmanIds = Collections.singleton(salesman.getId());

        List<SalesResultDailyDto> dtos = new ArrayList<>(workingDaysUntilToday.size());
        for (SimpleDate date : workingDaysUntilToday) {
            double revenue = cacheVisitOrderService.getRevenueByCreated(userLogin.getClientId(), DateType.DAILY, date,
                    getDefaultDistributor(userLogin).getId(), salesmanIds);

            double subRevenue = cacheVisitOrderService.getSubRevenueByCreated(userLogin.getClientId(), DateType.DAILY,
                    date, getDefaultDistributor(userLogin).getId(), salesmanIds);

            double productivity = cacheVisitOrderService.getProductivityByCreated(userLogin.getClientId(),
                    DateType.DAILY, date, getDefaultDistributor(userLogin).getId(), salesmanIds);

            double nbOrder = cacheVisitOrderService.getNbOrderByCreated(userLogin.getClientId(), DateType.DAILY, date,
                    getDefaultDistributor(userLogin).getId(), salesmanIds);

            dtos.add(new SalesResultDailyDto(date.getIsoDate(), new SalesResultDto(BigDecimal.valueOf(revenue),
                    BigDecimal.valueOf(subRevenue), BigDecimal.valueOf(productivity), (int) nbOrder)));
        }

        return new ListDto<>(dtos);
    }

    @Override
    public ListDto<OrderSimpleDto> getOrderByDateThisMonth(UserLogin userLogin, String _date) {
        SimpleDate date = getMandatoryIsoDate(_date);

        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();

        I_Config config = getConfig(userLogin);

        List<I_OrderHeader> orders = orderRepository.getOrderHeadersByDistributor(userLogin.getClientId(),
                distributorId, salesman.getId(), null, DateTimeUtils.getPeriodOneDay(date), config.getOrderDateType(),
                null, false);

        if (orders == null || orders.isEmpty()) {
            return ListDto.emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<>(orders.size());
        for (I_OrderHeader order : orders) {
            dtos.add(new OrderSimpleDto(order));
        }

        return new ListDto<>(dtos);
    }

}
