package com.viettel.backend.service.core.dashboard.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ProgressDto;
import com.viettel.backend.dto.dashboard.WebDashboardDto;
import com.viettel.backend.dto.dashboard.WebDashboardDto.BestSellerItem;
import com.viettel.backend.dto.dashboard.WebDashboardDto.ProgressWarningItem;
import com.viettel.backend.dto.dashboard.WebDashboardDto.RegionInfo;
import com.viettel.backend.dto.dashboard.WebDashboardDto.Result;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.cache.CacheScheduleService;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.cache.CacheVisitOrderService.DateType;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.dashboard.WebDashboardRegionService;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.TargetRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.category.I_Target;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADVANCE_SUPERVISOR })
@Service
public class WebDashboardRegionServiceImpl extends AbstractService implements WebDashboardRegionService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RouteRepository routeRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private TargetRepository targetRepository;

	@Autowired
	private CacheCalendarService cacheCalendarService;

	@Autowired
	private CacheScheduleService cacheScheduleService;

	@Autowired
	private CacheVisitOrderService cacheVisitOrderService;

	@Override
	public WebDashboardDto getWebDashboard(UserLogin userLogin) {
		SimpleDate today = DateTimeUtils.getToday();

		WebDashboardDto dto = new WebDashboardDto(today);

		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		Set<String> distributorIds = getIdSet(distributors);

		List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(), distributorIds);
		Set<String> salesmanIds = getIdSet(salesmen);

		// CALCULATE NB VISIT PLANNED
		List<I_Route> routes = routeRepository.getAll(userLogin.getClientId(), distributorIds);

		Map<String, Integer> nbVisitPlannedByRouteToday = cacheScheduleService
				.getNbVisitPlannedByRoute(userLogin.getClientId(), distributorIds, DateTimeUtils.getPeriodToday());
		int nbVisitPlannedToday = 0;

		Map<String, Integer> nbVisitPlannedByRouteThisMonth = cacheScheduleService.getNbVisitPlannedByRoute(
				userLogin.getClientId(), distributorIds, new Period(DateTimeUtils.getFirstOfThisMonth(), today));
		Map<String, Integer> nbVisitPlannedBySalesmanThisMonth = new HashMap<String, Integer>();

		for (I_Route route : routes) {
			Integer tmpToday = nbVisitPlannedByRouteToday.get(route.getId());
			tmpToday = tmpToday == null ? 0 : tmpToday;

			nbVisitPlannedToday += tmpToday;

			Integer tmpThisMonth = nbVisitPlannedByRouteThisMonth.get(route.getId());
			tmpThisMonth = tmpThisMonth == null ? 0 : tmpThisMonth;

			if (route.getSalesman() != null) {
				Integer value = nbVisitPlannedBySalesmanThisMonth.get(route.getSalesman().getId());
				value = value == null ? 0 : value;

				nbVisitPlannedBySalesmanThisMonth.put(route.getSalesman().getId(), value + tmpThisMonth);
			}
		}

		// TODAY
		Result todayResult = new Result();
		todayResult.setRevenue(new ProgressDto(cacheVisitOrderService.getRevenueByDistributor(userLogin.getClientId(),
				DateType.DAILY, today, distributorIds)));
		todayResult.setSubRevenue(BigDecimal.valueOf(cacheVisitOrderService
				.getSubRevenueByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorIds)));
		todayResult.setOrder(new ProgressDto(cacheVisitOrderService.getNbOrderByDistributor(userLogin.getClientId(),
				DateType.DAILY, today, distributorIds)));
		todayResult.setOrderNoVisit(new ProgressDto(cacheVisitOrderService
				.getNbOrderNoVisitByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorIds)));

		todayResult.setVisit(new ProgressDto((double) nbVisitPlannedToday, cacheVisitOrderService
				.getNbVisitByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorIds)));
		todayResult.setVisitHasOrder(new ProgressDto(cacheVisitOrderService
				.getNbVisitWithOrderByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorIds)));
		todayResult.setVisitErrorDuration(new ProgressDto(cacheVisitOrderService
				.getNbVisitErrorDurationByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorIds)));
		todayResult.setVisitErrorPosition(new ProgressDto(cacheVisitOrderService
				.getNbVisitErrorPositionByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorIds)));

		dto.setTodayResult(todayResult);

		// THIS MONTH
		// PLAN
		List<I_Target> targets = targetRepository.getTargetsBySalesmen(userLogin.getClientId(), salesmanIds,
				today.getMonth(), today.getYear(), false);
		BigDecimal revenueTargetThisMonth = BigDecimal.ZERO;
		if (targets != null) {
			for (I_Target target : targets) {
				revenueTargetThisMonth = revenueTargetThisMonth.add(target.getRevenue());
			}
		}
		// ACTUAL
		Result thisMonthResult = new Result();
		thisMonthResult.setNbDay(cacheCalendarService
				.getWorkingDays(userLogin.getClientId(), new Period(DateTimeUtils.getFirstOfThisMonth(), today))
				.size());
		thisMonthResult
				.setRevenue(
						new ProgressDto(revenueTargetThisMonth.doubleValue(),
								cacheVisitOrderService.getRevenueByDistributor(userLogin.getClientId(),
										DateType.MONTHLY, today, distributorIds)
										- todayResult.getRevenue().getActual().doubleValue()));
		thisMonthResult.setSubRevenue(
				BigDecimal.valueOf(cacheVisitOrderService.getSubRevenueByDistributor(userLogin.getClientId(),
						DateType.MONTHLY, today, distributorIds) - todayResult.getSubRevenue().doubleValue()));
		thisMonthResult.setOrder(new ProgressDto(cacheVisitOrderService.getNbOrderByDistributor(userLogin.getClientId(),
				DateType.MONTHLY, today, distributorIds) - todayResult.getOrder().getActual().doubleValue()));
		thisMonthResult.setVisit(new ProgressDto(cacheVisitOrderService.getNbVisitByDistributor(userLogin.getClientId(),
				DateType.MONTHLY, today, distributorIds) - todayResult.getVisit().getActual().doubleValue()));

		// SALESMAN THIS MONTH
		HashMap<String, Double> nbVisitedBySalesmanThisMonth = new HashMap<>();
		HashMap<String, Double> nbVisitedBySalesmanToday = new HashMap<>();
		for (String distributorId : distributorIds) {
			nbVisitedBySalesmanThisMonth.putAll(cacheVisitOrderService.getNbVisitBySalesmanMap(userLogin.getClientId(),
					DateType.MONTHLY, today, distributorId));
			nbVisitedBySalesmanToday.putAll(cacheVisitOrderService.getNbVisitBySalesmanMap(userLogin.getClientId(),
					DateType.DAILY, today, distributorId));
		}

		List<ProgressWarningItem> salesmanProgress = new ArrayList<>(salesmen.size());
		for (I_User salesman : salesmen) {
			Integer nbVisitPlanned = nbVisitPlannedBySalesmanThisMonth.get(salesman.getId());

			if (nbVisitPlanned != null && nbVisitPlanned > 0) {
				Double actualThisMonth = nbVisitedBySalesmanThisMonth.get(salesman.getId());
				Double actualToday = nbVisitedBySalesmanToday.get(salesman.getId());
				int actual = (int) ((actualThisMonth == null ? 0 : actualThisMonth)
						- (actualToday == null ? 0 : actualToday));

				ProgressWarningItem item = new ProgressWarningItem(salesman.getFullname(),
						new ProgressDto(nbVisitPlanned, actual));
				salesmanProgress.add(item);
			}
		}

		Collections.sort(salesmanProgress, new Comparator<ProgressWarningItem>() {

			@Override
			public int compare(ProgressWarningItem o1, ProgressWarningItem o2) {
				return o1.getProgress().getPercentage().compareTo(o2.getProgress().getPercentage());
			}

		});
		dto.setProgressWarnings(salesmanProgress.subList(0, salesmanProgress.size() > 5 ? 5 : salesmanProgress.size()));

		// BEST SELLERS
		HashMap<String, Double> quantityByProductThisMonth = new HashMap<>();
		HashMap<String, Double> quantityByProductToday = new HashMap<>();
		for (String distributorId : distributorIds) {
			Map<String, Double> cacheThisMonth = cacheVisitOrderService.getQuantityProductMap(userLogin.getClientId(),
					DateType.MONTHLY, today, distributorId);
			if (quantityByProductThisMonth.isEmpty()) {
				quantityByProductThisMonth.putAll(cacheThisMonth);
			} else {
				for (Entry<String, Double> entry : cacheThisMonth.entrySet()) {
					Double quantity = quantityByProductThisMonth.get(entry.getKey());
					if (quantity != null) {
						quantity = quantity + entry.getValue();
					} else {
						quantity = entry.getValue();
					}
					quantityByProductThisMonth.put(entry.getKey(), quantity);
				}
			}

			Map<String, Double> cacheToday = cacheVisitOrderService.getQuantityProductMap(userLogin.getClientId(),
					DateType.DAILY, today, distributorId);
			if (quantityByProductToday.isEmpty()) {
				quantityByProductToday.putAll(cacheToday);
			} else {
				for (Entry<String, Double> entry : cacheToday.entrySet()) {
					Double quantity = quantityByProductToday.get(entry.getKey());
					if (quantity != null) {
						quantity = quantity + entry.getValue();
					} else {
						quantity = entry.getValue();
					}
					quantityByProductToday.put(entry.getKey(), quantity);
				}
			}
		}

		List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
		List<BestSellerItem> bestSellers = new ArrayList<>(products.size());
		for (I_Product product : products) {
			Double quantityThisMonth = quantityByProductThisMonth.get(product.getId());
			Double quantityToday = quantityByProductToday.get(product.getId());
			double quantity = (quantityThisMonth == null ? 0 : quantityThisMonth)
					- (quantityToday == null ? 0 : quantityToday);

			BestSellerItem item = new BestSellerItem(product.getName(), BigDecimal.valueOf(quantity));
			bestSellers.add(item);
		}
		Collections.sort(bestSellers, new Comparator<BestSellerItem>() {

			@Override
			public int compare(BestSellerItem o1, BestSellerItem o2) {
				return o2.getResult().compareTo(o1.getResult());
			}

		});
		dto.setBestSellers(bestSellers.subList(0, bestSellers.size() > 5 ? 5 : bestSellers.size()));

		dto.setThisMonthResult(thisMonthResult);

		// LAST MONTH
		SimpleDate firstOfLastMonth = DateTimeUtils.getFirstOfLastMonth();

		Result lastMonthResult = new Result();
		lastMonthResult.setNbDay(cacheCalendarService
				.getWorkingDays(userLogin.getClientId(), DateTimeUtils.getPeriodLastMonth()).size());
		lastMonthResult.setRevenue(new ProgressDto(cacheVisitOrderService
				.getRevenueByDistributor(userLogin.getClientId(), DateType.MONTHLY, firstOfLastMonth, distributorIds)));
		lastMonthResult.setOrder(new ProgressDto(cacheVisitOrderService.getNbOrderByDistributor(userLogin.getClientId(),
				DateType.MONTHLY, firstOfLastMonth, distributorIds)));
		lastMonthResult.setVisit(new ProgressDto(cacheVisitOrderService.getNbVisitByDistributor(userLogin.getClientId(),
				DateType.MONTHLY, firstOfLastMonth, distributorIds)));

		dto.setLastMonthResult(lastMonthResult);

		// ------------------- REGIONS --------------------
		List<I_Region> regions = getAccessibleRegion(userLogin);
		List<RegionInfo> regionsInfo = new ArrayList<>();
		for (I_Region region : regions) {
			RegionInfo regionInfo = new RegionInfo();
			List<I_Distributor> distributorsOfRegion = getDistributorsByRegion(userLogin, region.getId());
			List<String> distributorsOfRegionIds = new ArrayList<String>();
			for (I_CategoryEmbed dis : distributorsOfRegion) {
				distributorsOfRegionIds.add(dis.getId());
			}
			regionInfo.setName(region.getName());

			// CALCULATE NB VISIT PLANNED
			List<I_Route> routesForRegions = routeRepository.getAll(userLogin.getClientId(), distributorsOfRegionIds);

			Map<String, Integer> nbVisitPlannedByRouteTodayForRegions = cacheScheduleService
					.getNbVisitPlannedByRoute(userLogin.getClientId(), distributorIds, DateTimeUtils.getPeriodToday());
			int nbVisitPlannedTodayForRegions = 0;

			for (I_Route route : routesForRegions) {
				Integer tmpToday = nbVisitPlannedByRouteTodayForRegions.get(route.getId());
				tmpToday = tmpToday == null ? 0 : tmpToday;

				nbVisitPlannedTodayForRegions += tmpToday;

			}

			// TODAY REGIONS
			Result todayRegionResult = new Result();
			todayRegionResult.setRevenue(new ProgressDto(cacheVisitOrderService
					.getRevenueByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorsOfRegionIds)));
			todayRegionResult.setSubRevenue(BigDecimal.valueOf(cacheVisitOrderService.getSubRevenueByDistributor(
					userLogin.getClientId(), DateType.DAILY, today, distributorsOfRegionIds)));
			todayRegionResult.setOrder(new ProgressDto(cacheVisitOrderService
					.getNbOrderByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorsOfRegionIds)));
			todayRegionResult.setOrderNoVisit(new ProgressDto(cacheVisitOrderService.getNbOrderNoVisitByDistributor(
					userLogin.getClientId(), DateType.DAILY, today, distributorsOfRegionIds)));

			todayRegionResult.setVisit(new ProgressDto((double) nbVisitPlannedTodayForRegions, cacheVisitOrderService
					.getNbVisitByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorsOfRegionIds)));
			todayRegionResult.setVisitHasOrder(new ProgressDto(cacheVisitOrderService.getNbVisitWithOrderByDistributor(
					userLogin.getClientId(), DateType.DAILY, today, distributorsOfRegionIds)));
			todayRegionResult.setVisitErrorDuration(
					new ProgressDto(cacheVisitOrderService.getNbVisitErrorDurationByDistributor(userLogin.getClientId(),
							DateType.DAILY, today, distributorsOfRegionIds)));
			todayRegionResult.setVisitErrorPosition(
					new ProgressDto(cacheVisitOrderService.getNbVisitErrorPositionByDistributor(userLogin.getClientId(),
							DateType.DAILY, today, distributorsOfRegionIds)));

			regionInfo.setTodayResult(todayRegionResult);

			// THIS MONTH REGIONS
			// PLAN
			List<I_Target> regionTargets = targetRepository.getTargetsBySalesmen(userLogin.getClientId(), salesmanIds,
					today.getMonth(), today.getYear(), false);
			BigDecimal revenueRegionTargetThisMonth = BigDecimal.ZERO;
			if (regionTargets != null) {
				for (I_Target target : regionTargets) {
					revenueRegionTargetThisMonth = revenueRegionTargetThisMonth.add(target.getRevenue());
				}
			}
			// ACTUAL
			Result thisMonthResultOfRegion = new Result();
			thisMonthResultOfRegion.setNbDay(cacheCalendarService
					.getWorkingDays(userLogin.getClientId(), new Period(DateTimeUtils.getFirstOfThisMonth(), today))
					.size());
			thisMonthResultOfRegion
					.setRevenue(new ProgressDto(revenueRegionTargetThisMonth.doubleValue(),
							cacheVisitOrderService.getRevenueByDistributor(userLogin.getClientId(), DateType.MONTHLY,
									today, distributorsOfRegionIds)
									- todayRegionResult.getRevenue().getActual().doubleValue()));
			thisMonthResultOfRegion.setSubRevenue(BigDecimal.valueOf(
					cacheVisitOrderService.getSubRevenueByDistributor(userLogin.getClientId(), DateType.MONTHLY, today,
							distributorsOfRegionIds) - todayRegionResult.getSubRevenue().doubleValue()));
			thisMonthResultOfRegion.setOrder(new ProgressDto(
					cacheVisitOrderService.getNbOrderByDistributor(userLogin.getClientId(), DateType.MONTHLY, today,
							distributorsOfRegionIds) - todayRegionResult.getOrder().getActual().doubleValue()));
			thisMonthResultOfRegion.setVisit(new ProgressDto(
					cacheVisitOrderService.getNbVisitByDistributor(userLogin.getClientId(), DateType.MONTHLY, today,
							distributorsOfRegionIds) - todayRegionResult.getVisit().getActual().doubleValue()));
			regionInfo.setThisMonthResult(thisMonthResultOfRegion);

			// LAST MONTH REGIONS
			SimpleDate firstOfLastMonthOfRegion = DateTimeUtils.getFirstOfLastMonth();

			Result lastMonthResultOfRegion = new Result();
			lastMonthResultOfRegion.setNbDay(cacheCalendarService
					.getWorkingDays(userLogin.getClientId(), DateTimeUtils.getPeriodLastMonth()).size());
			lastMonthResultOfRegion
					.setRevenue(new ProgressDto(cacheVisitOrderService.getRevenueByDistributor(userLogin.getClientId(),
							DateType.MONTHLY, firstOfLastMonthOfRegion, distributorsOfRegionIds)));
			lastMonthResultOfRegion
					.setOrder(new ProgressDto(cacheVisitOrderService.getNbOrderByDistributor(userLogin.getClientId(),
							DateType.MONTHLY, firstOfLastMonthOfRegion, distributorsOfRegionIds)));
			lastMonthResultOfRegion
					.setVisit(new ProgressDto(cacheVisitOrderService.getNbVisitByDistributor(userLogin.getClientId(),
							DateType.MONTHLY, firstOfLastMonthOfRegion, distributorsOfRegionIds)));
			regionInfo.setLastMonthResult(lastMonthResultOfRegion);

			regionsInfo.add(regionInfo);
		}
		dto.setRegionInfo(regionsInfo);
		return dto;
	}

}
