package com.viettel.backend.service.core.distributor;

import com.viettel.backend.dto.distributor.DistributorCreateDto;
import com.viettel.backend.dto.distributor.DistributorDto;
import com.viettel.backend.dto.distributor.DistributorListDto;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface DistributorEditableService extends
        CategoryEditableService<DistributorListDto, DistributorDto, DistributorCreateDto> {

}
