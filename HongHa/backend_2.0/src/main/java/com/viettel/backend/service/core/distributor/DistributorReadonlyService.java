package com.viettel.backend.service.core.distributor;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.CategoryReadonlyService;

public interface DistributorReadonlyService extends CategoryReadonlyService<CategorySimpleDto> {
	ListDto<CategorySimpleDto> getAccessibleDistributor(UserLogin userLogin,String _regionId);
	
	public ListDto<CategorySimpleDto> getDistributorOfRegion(UserLogin userLogin,String regionId); 
}
