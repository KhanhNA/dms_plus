package com.viettel.backend.service.core.distributor.impl;

import java.util.Arrays;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.distributor.DistributorCreateDto;
import com.viettel.backend.dto.distributor.DistributorDto;
import com.viettel.backend.dto.distributor.DistributorListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.CategoryEditableServiceImpl;
import com.viettel.backend.service.core.distributor.DistributorEditableService;
import com.viettel.backend.service.core.pdto.Distributor;
import com.viettel.backend.service.core.pdto.UserEmbed;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.ADMIN })
@Service
public class DistributorEditableServiceImpl
        extends CategoryEditableServiceImpl<I_Distributor, DistributorListDto, DistributorDto, DistributorCreateDto>
        implements DistributorEditableService {

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    protected boolean isUseCode() {
        return true;
    }
    
    @Override
    protected boolean isAutoGenerateCode() {
        return false;
    }
    @Override
    protected boolean isCanChangeCode() {
    	return true;
    }

    @Override
    protected CategoryBasicRepository<I_Distributor> getRepository() {
        return distributorRepository;
    }

    @Override
    protected void beforeSetActive(UserLogin userLogin, I_Distributor domain, boolean active) {
        throw new UnsupportedOperationException();
    }

    @Override
    public DistributorListDto createListSimpleDto(UserLogin userLogin, I_Distributor domain) {
        return new DistributorListDto(domain);
    }

    @Override
    public DistributorDto createListDetailDto(UserLogin userLogin, I_Distributor domain) {
        if (domain.getId() != null) {
            // TODO recheck if necessaire
            Set<String> salesmanIds = getIdSet(
                    userRepository.getSalesmen(userLogin.getClientId(), Arrays.asList(domain.getId())));

            return new DistributorDto(domain, salesmanIds);
        }

        return new DistributorDto(domain, null);
    }

    @Override
    public I_Distributor getObjectForCreate(UserLogin userLogin, DistributorCreateDto createdto) {
        BusinessAssert.notNull(createdto);
        BusinessAssert.notNull(createdto.getSupervisorId());

        Distributor distributor = new Distributor();
        initiatePO(userLogin.getClientId(), distributor);
        distributor.setDraft(true);
        distributor.setCode(createdto.getCode());

        return getObjectForUpdate(userLogin, distributor, createdto);
    }

    @Override
    public I_Distributor getObjectForUpdate(UserLogin userLogin, I_Distributor oldDomain,
            DistributorCreateDto createdto) {
        BusinessAssert.notNull(createdto);
        BusinessAssert.notNull(createdto.getSupervisorId());

        Distributor distributor = new Distributor(oldDomain);
        
        distributor = updateCategoryFieldFromDto(userLogin, distributor, createdto);
        
        I_User supervisor = getMandatoryPO(userLogin, createdto.getSupervisorId(), userRepository);
        BusinessAssert.isTrue(supervisor.getRole().equals(Role.SUPERVISOR));
        distributor.setSupervisor(new UserEmbed(supervisor));
        
        return distributor;
    }

}
