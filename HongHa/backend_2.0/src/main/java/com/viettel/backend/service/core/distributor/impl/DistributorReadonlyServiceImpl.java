package com.viettel.backend.service.core.distributor.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.distributor.DistributorReadonlyService;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.data.I_Region;

@Service
public class DistributorReadonlyServiceImpl extends AbstractService implements DistributorReadonlyService {

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public Class<CategorySimpleDto> getSimpleDtoClass() {
		return CategorySimpleDto.class;
	}

	@Override
	public ListDto<CategorySimpleDto> getAll(UserLogin userLogin, String _distributorId) {
		List<I_Distributor> domains = getAccessibleDistributors(userLogin);
		if (CollectionUtils.isEmpty(domains)) {
			return ListDto.emptyList();
		}

		List<CategorySimpleDto> dtos = new ArrayList<CategorySimpleDto>(domains.size());
		for (I_Distributor domain : domains) {
			dtos.add(new CategorySimpleDto(domain));
		}

		return new ListDto<CategorySimpleDto>(dtos);
	}

	@Override
	public CategorySimpleDto getById(UserLogin userLogin, String id) {
		I_Distributor distributor = getMandatoryPO(userLogin, id, distributorRepository);
		Set<String> accessibleDistributorIds = getIdSet(getAccessibleDistributors(userLogin));

		BusinessAssert.isTrue(accessibleDistributorIds.contains(distributor.getId()));

		return new CategorySimpleDto(distributor);
	}

	@Override
	public ListDto<CategorySimpleDto> getAccessibleDistributor(UserLogin userLogin, String _regionId) {
		List<I_Distributor> domains = getAccessibleDistributors(userLogin);

		if (CollectionUtils.isEmpty(domains)) {
			return ListDto.emptyList();
		}

		List<CategorySimpleDto> dtos = new ArrayList<CategorySimpleDto>(domains.size());
		for (I_Distributor domain : domains) {
			dtos.add(new CategorySimpleDto(domain));
		}

		return new ListDto<CategorySimpleDto>(dtos);
	}

	@Override
	public ListDto<CategorySimpleDto> getDistributorOfRegion(UserLogin userLogin, String regionId) {
		List<I_Distributor> domains = new ArrayList<>();
		if ("all".equals(regionId)) {
			domains = getAccessibleDistributors(userLogin);
		} else {
			domains = getDistributorsByRegion(userLogin, regionId);
		}
		if (CollectionUtils.isEmpty(domains)) {
			return ListDto.emptyList();
		}

		List<CategorySimpleDto> dtos = new ArrayList<CategorySimpleDto>(domains.size());
		for (I_Distributor domain : domains) {
			dtos.add(new CategorySimpleDto(domain));
		}
		return new ListDto<CategorySimpleDto>(dtos);
	}

}
