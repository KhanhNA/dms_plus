package com.viettel.backend.service.core.feedback;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.feedback.FeedbackDto;
import com.viettel.backend.dto.feedback.FeedbackListDto;
import com.viettel.backend.dto.feedback.FeedbackSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface FeedbackService {

    public ListDto<FeedbackSimpleDto> getLast10FeedbacksFromCustomer(UserLogin userLogin, String _customerId);

    public ListDto<FeedbackListDto> getFeedbacks(UserLogin userLogin, PageSizeRequest pageSizeRequest, Boolean readStatus);

    public FeedbackDto readFeedback(UserLogin userLogin, String feedbackId);

}
