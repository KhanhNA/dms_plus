package com.viettel.backend.service.core.feedback.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.feedback.FeedbackDto;
import com.viettel.backend.dto.feedback.FeedbackListDto;
import com.viettel.backend.dto.feedback.FeedbackSimpleDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.feedback.FeedbackService;
import com.viettel.backend.service.core.sub.WebNotificationService;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.FeedbackRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_Feedback;
import com.viettel.repository.common.domain.data.I_FeedbackHeader;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;

@Service
public class FeedbackServiceImpl extends AbstractService implements FeedbackService {

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private WebNotificationService webNotificationEngine;

    @RolePermission(value = { Role.SALESMAN })
    @Override
    public ListDto<FeedbackSimpleDto> getLast10FeedbacksFromCustomer(UserLogin userLogin, String _customerId) {
        I_Customer customer = getMandatoryPO(userLogin, _customerId, customerRepository);
        checkAccessible(userLogin, null, customer);

        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();

        List<I_Feedback> feedbacks = feedbackRepository.getLastFeedbackByCustomer(userLogin.getClientId(),
                distributorId, customer.getId(), 10);

        List<FeedbackSimpleDto> messages = new ArrayList<>();
        for (I_Feedback feedback : feedbacks) {
            if (feedback.getFeedbacks() != null) {
                String createdTime = feedback.getCreatedTime().getIsoTime();
                for (String message : feedback.getFeedbacks()) {
                    FeedbackSimpleDto dto = new FeedbackSimpleDto(createdTime, message);
                    messages.add(dto);
                }
            }
        }

        return new ListDto<FeedbackSimpleDto>(messages);
    }

    @RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
    @Override
    public ListDto<FeedbackListDto> getFeedbacks(UserLogin userLogin, PageSizeRequest pageSizeRequest,
            Boolean readStatus) {
        List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
        Set<String> distributorIds = getIdSet(distributors);

        if (distributorIds == null || distributorIds.isEmpty()) {
            return ListDto.emptyList();
        }

        List<I_FeedbackHeader> feedbacks = feedbackRepository.getFeedbackByDistributors(userLogin.getClientId(),
                distributorIds, readStatus, pageSizeRequest);
        if (CollectionUtils.isEmpty(feedbacks) && pageSizeRequest.getPageNumber() == 0) {
            return ListDto.emptyList();
        }

        List<FeedbackListDto> dtos = new ArrayList<FeedbackListDto>(feedbacks.size());
        for (I_FeedbackHeader feedback : feedbacks) {
            FeedbackListDto dto = new FeedbackListDto(feedback);
            dtos.add(dto);
        }

        long count = Long.valueOf(dtos.size());
        if (pageSizeRequest != null) {
            if (pageSizeRequest.getPageNumber() > 0 || count == pageSizeRequest.getPageSize()) {
                count = feedbackRepository.countFeedbackByDistributors(userLogin.getClientId(), distributorIds,
                        readStatus);
            }
        }

        return new ListDto<FeedbackListDto>(dtos, count);
    }

    @RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
    @Override
    public FeedbackDto readFeedback(UserLogin userLogin, String feedbackId) {
        I_Feedback feedback = feedbackRepository.getById(userLogin.getClientId(), feedbackId);
        BusinessAssert.notNull(feedback, String.format("Cannot find feedback id: %s", feedbackId));

        BusinessAssert.isTrue(checkAccessible(userLogin, feedback.getDistributor().getId()),
                "distributor accessible failed");

        if (!feedback.isReaded()) {
            feedbackRepository.markAsRead(userLogin.getClientId(), feedbackId);
            webNotificationEngine.notifyChangedFeedback(userLogin, feedback,
                    WebNotificationService.ACTION_FEEDBACK_READ);
        }

        FeedbackDto dto = new FeedbackDto(feedback);
        return dto;
    }

}
