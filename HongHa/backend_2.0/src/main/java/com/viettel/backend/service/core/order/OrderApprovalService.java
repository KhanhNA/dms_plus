package com.viettel.backend.service.core.order;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.dto.order.OrderSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface OrderApprovalService {
    
    public ListDto<OrderSimpleDto> getPendingOrders(UserLogin userLogin, PageSizeRequest pageSizeRequest);

    public OrderDto getOrderById(UserLogin userLogin, String orderId);

    public void approve(UserLogin userLogin, String orderId);

    public void reject(UserLogin userLogin, String orderId);

}
