package com.viettel.backend.service.core.order;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.order.OrderCreateDto;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface OrderCreatingByDistributorService {

    public OrderDto calculatePromotion(UserLogin userLogin, OrderCreateDto orderCreateDto);

    public IdDto createOrder(UserLogin userLogin, OrderCreateDto orderCreateDto);

}
