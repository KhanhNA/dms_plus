package com.viettel.backend.service.core.order;

import java.io.InputStream;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface OrderExportService {

	public InputStream exportOrder(UserLogin userLogin, String distributorId, String salesmanId, String customerId,
			String fromDate, String toDate, String lang);

	public InputStream exportOrderDetail(UserLogin userLogin, String distributorId, String salesmanId,
			String customerId, String fromDate, String toDate, String lang);

	public ExportDto exportOrderToday(UserLogin userLogin,String lang, boolean isVisit, boolean hasOrder);
}
