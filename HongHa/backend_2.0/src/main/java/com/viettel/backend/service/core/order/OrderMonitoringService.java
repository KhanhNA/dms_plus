package com.viettel.backend.service.core.order;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.dto.order.OrderSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface OrderMonitoringService {

    public ListDto<OrderSimpleDto> getOrders(UserLogin userLogin, String distributorId, String salesmanId,
            String customerId, String fromDate, String toDate, PageSizeRequest pageSizeRequest);

    public IdDto getOrderByCode(UserLogin userLogin, String code);

    public OrderDto getOrderById(UserLogin userLogin, String orderId);

}
