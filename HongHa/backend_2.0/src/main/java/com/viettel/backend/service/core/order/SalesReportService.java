package com.viettel.backend.service.core.order;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.order.DistributorSalesResultDto;
import com.viettel.backend.dto.order.ProductSalesResultDto;
import com.viettel.backend.dto.order.SalesResultDailyDto;
import com.viettel.backend.dto.order.SalesmanSalesResultDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SalesReportService {

    public ListDto<SalesResultDailyDto> getSalesResultDaily(UserLogin userLogin,String regionId, String distributorId, String fromDate,
            String toDate);

    public ListDto<DistributorSalesResultDto> getDistributorSalesResult(UserLogin userLogin, String fromDate,
            String toDate);

    public ListDto<ProductSalesResultDto> getProductSalesResult(UserLogin userLogin, String regionId,String distributorId,
            String fromDate, String toDate, String productCategoryId, String isImportantProduct);

    public ListDto<SalesmanSalesResultDto> getSalesmanSalesResult(UserLogin userLogin, String distributorId,
            String fromDate, String toDate);

}
