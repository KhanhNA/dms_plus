package com.viettel.backend.service.core.order.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.dto.order.OrderSimpleDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.order.OrderApprovalService;
import com.viettel.backend.service.core.sub.WebNotificationService;
import com.viettel.repository.common.OrderPendingRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class OrderApprovalServiceImpl extends AbstractService implements OrderApprovalService {

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CacheVisitOrderService cacheVisitOrderService;

    @Autowired
    private WebNotificationService webNotificationEngine;

    @Override
    public ListDto<OrderSimpleDto> getPendingOrders(UserLogin userLogin, PageSizeRequest pageSizeRequest) {
        List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
        if (distributors == null || distributors.isEmpty()) {
            return ListDto.emptyList();
        }

        Set<String> distributorIds = getIdSet(distributors);

        Collection<I_OrderHeader> orders = orderPendingRepository
                .getPendingOrdersByDistributors(userLogin.getClientId(), distributorIds, pageSizeRequest);
        if (CollectionUtils.isEmpty(orders) && pageSizeRequest.getPageNumber() == 0) {
            return ListDto.emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<OrderSimpleDto>(orders.size());
        for (I_OrderHeader order : orders) {
            dtos.add(new OrderSimpleDto(order));
        }

        long size = Long.valueOf(dtos.size());
        if (pageSizeRequest != null) {
            if (pageSizeRequest.getPageNumber() > 0 || pageSizeRequest.getPageSize() == size) {
                size = orderPendingRepository.countPendingOrdersByDistributors(userLogin.getClientId(), distributorIds);
            }
        }

        return new ListDto<OrderSimpleDto>(dtos, size);
    }

    @Override
    public OrderDto getOrderById(UserLogin userLogin, String _orderId) {
        I_Order order = getMandatoryPO(userLogin, _orderId, orderPendingRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, order.getDistributor().getId()), "distributor not accessible");
        BusinessAssert.equals(order.getApproveStatus(), I_Customer.APPROVE_STATUS_PENDING, "order not pending");

        return new OrderDto(order, getProductPhotoFactory(userLogin));
    }

    @Override
    public void approve(UserLogin userLogin, String _orderId) {
        I_Order order = getMandatoryPO(userLogin, _orderId, orderPendingRepository);

        BusinessAssert.isTrue(checkAccessible(userLogin, order.getDistributor().getId()),
                "distributor is not accessible");

        BusinessAssert.equals(order.getApproveStatus(), I_Order.APPROVE_STATUS_PENDING, "order is not pending");

        orderPendingRepository.approve(userLogin.getClientId(), getCurrentUser(userLogin),
                DateTimeUtils.getCurrentTime(), order.getId());

        order = orderPendingRepository.getById(userLogin.getClientId(), order.getId());
        
        cacheVisitOrderService.addNewApprovedOrder(order);

        webNotificationEngine.notifyChangedOrder(userLogin, order, WebNotificationService.ACTION_ORDER_APPROVE);
    }

    @Override
    public void reject(UserLogin userLogin, String _orderId) {
        I_Order order = getMandatoryPO(userLogin, _orderId, orderPendingRepository);

        BusinessAssert.isTrue(checkAccessible(userLogin, order.getDistributor().getId()),
                "distributor is not accessible");

        BusinessAssert.equals(order.getApproveStatus(), I_Order.APPROVE_STATUS_PENDING, "order is not pending");

        orderPendingRepository.reject(userLogin.getClientId(), getCurrentUser(userLogin),
                DateTimeUtils.getCurrentTime(), order.getId());

        webNotificationEngine.notifyChangedOrder(userLogin, order, WebNotificationService.ACTION_ORDER_REJECT);
    }

}
