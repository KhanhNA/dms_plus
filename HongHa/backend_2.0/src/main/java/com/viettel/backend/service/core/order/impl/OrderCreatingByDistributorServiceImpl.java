package com.viettel.backend.service.core.order.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.order.OrderCreateDto;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.order.OrderCreatingByDistributorService;
import com.viettel.backend.service.core.pdto.Order;
import com.viettel.backend.service.core.sub.OrderCalculationService;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.OrderPendingRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.DISTRIBUTOR_ADMIN })
@Service
public class OrderCreatingByDistributorServiceImpl extends AbstractService implements OrderCreatingByDistributorService {

    @Autowired
    private OrderCalculationService orderCalculationService;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private CacheVisitOrderService cacheVisitOrderService;
    
    @Override
    public OrderDto calculatePromotion(UserLogin userLogin, OrderCreateDto dto) {
        BusinessAssert.notNull(dto);
        BusinessAssert.notNull(dto.getDetails());
        BusinessAssert.notNull(dto.getCustomerId());
        BusinessAssert.notNull(dto.getSalesmanId());

        I_Distributor distributor;
        if (dto.getDistributorId() != null) {
            distributor = getMandatoryPO(userLogin, dto.getDistributorId(), distributorRepository);
        } else {
            distributor = getDefaultDistributor(userLogin);
            BusinessAssert.notNull(distributor, "Distributor is required");
        }

        I_Customer customer = getMandatoryPO(userLogin, dto.getCustomerId(), customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId(), customer),
                "distributor or customer is not accessible");

        I_User salesman = getMandatoryPO(userLogin, dto.getSalesmanId(), userRepository);

        Order order = new Order();
        initiatePO(userLogin.getClientId(), order);

        order.setCreatedBy(salesman);
        order.setCustomer(customer);
        order.setDistributor(distributor);

        order.setDeliveryType(dto.getDeliveryType());
        if (dto.getDeliveryTime() != null) {
            order.setDeliveryTime(getMandatoryIsoTime(dto.getDeliveryTime()));
        }

        order.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
        order.setApprovedTime(DateTimeUtils.getCurrentTime());
        order.setApprovedBy(getCurrentUser(userLogin));
        order.setVanSales(false);

        orderCalculationService.calculate(userLogin, distributor.getId(), dto, order,
                getDistributorPriceList(userLogin));

        return new OrderDto(order, getProductPhotoFactory(userLogin));
    }

    @Override
    public IdDto createOrder(UserLogin userLogin, OrderCreateDto dto) {
        BusinessAssert.notNull(dto);
        BusinessAssert.notNull(dto.getDetails());
        BusinessAssert.notNull(dto.getCustomerId());
        BusinessAssert.notNull(dto.getSalesmanId());

        I_Distributor distributor;
        if (dto.getDistributorId() != null) {
            distributor = getMandatoryPO(userLogin, dto.getDistributorId(), distributorRepository);
        } else {
            distributor = getDefaultDistributor(userLogin);
            BusinessAssert.notNull(distributor, "Distributor is required");
        }

        I_Customer customer = getMandatoryPO(userLogin, dto.getCustomerId(), customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId(), customer),
                "distributor or customer is not accessible");

        I_User salesman = getMandatoryPO(userLogin, dto.getSalesmanId(), userRepository);

        Order order = new Order();
        initiatePO(userLogin.getClientId(), order);

        order.setCreatedBy(salesman);
        order.setCustomer(customer);
        order.setDistributor(distributor);

        order.setCreatedTime(DateTimeUtils.getCurrentTime());
        order.setCode(codeGeneratorRepository.getOrderCode(userLogin.getClientId().toString(), order.getCreatedTime()));

        order.setDeliveryType(dto.getDeliveryType());
        if (dto.getDeliveryTime() != null) {
            order.setDeliveryTime(getMandatoryIsoTime(dto.getDeliveryTime()));
        }

        order.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
        order.setApprovedTime(DateTimeUtils.getCurrentTime());
        order.setApprovedBy(getCurrentUser(userLogin));
        order.setVanSales(false);

        orderCalculationService.calculate(userLogin, distributor.getId(), dto, order,
                getDistributorPriceList(userLogin));

        I_Order _order = orderPendingRepository.create(userLogin.getClientId(), order);
        
        cacheVisitOrderService.addNewApprovedOrder(_order);

        return new IdDto(_order);
    }

}
