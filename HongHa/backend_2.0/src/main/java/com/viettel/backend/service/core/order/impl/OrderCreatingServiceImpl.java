package com.viettel.backend.service.core.order.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.customer.CustomerSimpleDto;
import com.viettel.backend.dto.order.OrderCreateDto;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.dto.order.OrderPromotionDto;
import com.viettel.backend.dto.order.OrderSimpleDto;
import com.viettel.backend.dto.order.ProductForOrderDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.order.OrderCreatingService;
import com.viettel.backend.service.core.pdto.Order;
import com.viettel.backend.service.core.sub.OrderCalculationService;
import com.viettel.backend.service.core.sub.WebNotificationService;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.InventoryRepository;
import com.viettel.repository.common.OrderPendingRepository;
import com.viettel.repository.common.OrderRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderDetail;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.domain.data.I_OrderProduct;
import com.viettel.repository.common.domain.data.I_OrderPromotion;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.SALESMAN })
@Service
public class OrderCreatingServiceImpl extends AbstractService implements OrderCreatingService {

    @Autowired
    private OrderCalculationService orderCalculationService;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CacheVisitOrderService cacheVisitOrderService;

    @Autowired
    private WebNotificationService webNotificationEngine;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    public ListDto<CustomerSimpleDto> getCustomersForOrder(UserLogin userLogin, String _distributorId, String search,
            PageSizeRequest pageSizeRequest) {
        if (isManager(userLogin)) {
            String searchName = search;
            String searchCode = search == null ? null : search.toUpperCase();

            List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
            if (distributors == null || distributors.isEmpty()) {
                return ListDto.emptyList();
            }

            String distributorId;
            if (distributors.size() == 1) {
                distributorId = distributors.get(0).getId();
            } else {
                I_Distributor distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
                Set<String> distributorIds = getIdSet(distributors);
                distributorId = distributor.getId();
                BusinessAssert.contain(distributorIds, distributorId);
            }
            Set<String> distributorIds = Collections.singleton(distributorId);

            List<I_Customer> customers = customerRepository.getList(userLogin.getClientId(), false, true,
                    distributorIds, searchName, searchCode, pageSizeRequest);
            if (CollectionUtils.isEmpty(customers) && pageSizeRequest.getPageNumber() == 0) {
                return ListDto.emptyList();
            }

            List<CustomerSimpleDto> dtos = new ArrayList<CustomerSimpleDto>(customers.size());
            for (I_Customer customer : customers) {
                dtos.add(new CustomerSimpleDto(customer));
            }

            long size = Long.valueOf(dtos.size());
            if (pageSizeRequest != null) {
                if (pageSizeRequest.getPageNumber() > 0 || size == pageSizeRequest.getPageSize()) {
                    size = customerRepository.count(userLogin.getClientId(), false, true, distributorIds, searchName,
                            searchCode);
                }
            }

            return new ListDto<CustomerSimpleDto>(dtos, size);
        } else {
            I_User salesman = getCurrentUser(userLogin);
            String distributorId = salesman.getDistributor().getId();
            Collection<String> salesmanIds = Collections.singleton(salesman.getId());

            List<I_Route> routes = routeRepository.getRoutesBySalesmen(userLogin.getClientId(), distributorId,
                    salesmanIds);
            Set<String> routeIds = getIdSet(routes);

            List<I_Customer> customers = customerRepository.getCustomersByRoutes(userLogin.getClientId(), distributorId,
                    routeIds, null);
            if (CollectionUtils.isEmpty(customers)) {
                return ListDto.emptyList();
            }

            List<CustomerSimpleDto> dtos = new ArrayList<CustomerSimpleDto>(customers.size());
            for (I_Customer customer : customers) {
                dtos.add(new CustomerSimpleDto(customer));
            }

            return new ListDto<CustomerSimpleDto>(dtos);
        }
    }

    @Override
    public ListDto<ProductForOrderDto> getProductsForOrder(UserLogin userLogin, String customerId) {
        I_Customer customer = getMandatoryPO(userLogin, customerId, customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer is not accessible");

        String clientId = userLogin.getClientId();

        Collection<I_Product> products = productRepository.getAll(clientId, null);
        I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);

        Set<String> productFavoriteIds = new HashSet<String>();

        // get last order from one month
        SimpleDate tomorrow = DateTimeUtils.getTomorrow();
        Period oneMonthAgo = new Period(DateTimeUtils.addMonths(tomorrow, -1), tomorrow);

        List<I_Order> lastPOs = orderRepository.getOrdersByDistributor(clientId, customer.getDistributor().getId(),
                null, customer.getId(), oneMonthAgo, OrderDateType.CREATED_DATE, new PageSizeRequest(0, 1), false);
        if (lastPOs != null && !lastPOs.isEmpty()) {
            for (I_Order order : lastPOs) {
                List<I_OrderDetail> details = order.getDetails();
                if (details == null || details.isEmpty()) {
                    continue;
                }

                for (I_OrderDetail detail : details) {
                    I_OrderProduct product = detail.getProduct();
                    if (product == null || product.getId() == null) {
                        continue;
                    }
                    productFavoriteIds.add(product.getId());
                }
            }
        }

        // Calculate available quantity
        Map<String, BigDecimal> availableMap = Collections.emptyMap();
        // Check if this client use Inventory
        if (userLogin.hasModule(Module.INVENTORY)) {
            // Only calculate for Salesman and Distributor
            List<I_Distributor> accessibleDistributors = getAccessibleDistributors(userLogin);
            if (accessibleDistributors.size() == 1) {
                availableMap = calculateProductAvailable(userLogin, products, accessibleDistributors.get(0));
            }
        }

        int favoriteIndex = 0;
        int noFavoriteIndex = products.size();

        List<ProductForOrderDto> dtos = new LinkedList<ProductForOrderDto>();
        for (I_Product product : products) {
            ProductForOrderDto dto = new ProductForOrderDto(product, productPhotoFactory,
                    getDistributorPriceList(userLogin));

            if (productFavoriteIds.contains((product.getId()))) {
                dto.setSeqNo(favoriteIndex);
                favoriteIndex++;
            } else {
                dto.setSeqNo(noFavoriteIndex);
                noFavoriteIndex++;
            }

            dto.setAvailableQuantity(availableMap.get(product.getId()));

            dtos.add(dto);
        }

        return new ListDto<ProductForOrderDto>(dtos);
    }

    @Override
    public ListDto<OrderPromotionDto> calculatePromotion(UserLogin userLogin, OrderCreateDto orderCreateDto) {
        I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);

        I_Distributor distributor = getDefaultDistributor(userLogin);
        Assert.notNull(distributor);

        List<I_OrderPromotion> orderPromotions = orderCalculationService.getOrderPromotions(userLogin,
                distributor.getId(), orderCreateDto, getDistributorPriceList(userLogin));

        if (orderPromotions == null || orderPromotions.isEmpty()) {
            return ListDto.emptyList();
        }

        List<OrderPromotionDto> dtos = new ArrayList<>(orderPromotions.size());
        for (I_OrderPromotion orderPromotion : orderPromotions) {
            dtos.add(new OrderPromotionDto(orderPromotion, productPhotoFactory));
        }

        return new ListDto<>(dtos);
    }

    @Override
    public IdDto createOrder(UserLogin userLogin, OrderCreateDto dto) {
        BusinessAssert.notNull(dto);
        BusinessAssert.notNull(dto.getDetails());
        BusinessAssert.notNull(dto.getCustomerId());

        I_Distributor distributor;
        if (dto.getDistributorId() != null) {
            distributor = getMandatoryPO(userLogin, dto.getDistributorId(), distributorRepository);
        } else {
            distributor = getDefaultDistributor(userLogin);
            BusinessAssert.notNull(distributor, "Distributor is required");
        }

        I_Customer customer = getMandatoryPO(userLogin, dto.getCustomerId(), customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId(), customer),
                "distributor or customer is not accessible");

        I_User currentUser = getCurrentUser(userLogin);

        Order order = new Order();
        initiatePO(userLogin.getClientId(), order);

        order.setCreatedBy(currentUser);
        order.setCustomer(customer);
        order.setDistributor(distributor);

        order.setCreatedTime(DateTimeUtils.getCurrentTime());
        order.setCode(codeGeneratorRepository.getOrderCode(userLogin.getClientId().toString(), order.getCreatedTime()));

        order.setDeliveryType(dto.getDeliveryType());
        if (dto.getDeliveryTime() != null) {
            order.setDeliveryTime(getMandatoryIsoTime(dto.getDeliveryTime()));
        }

        if (dto.isVanSales()) {
            order.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
            order.setApprovedTime(DateTimeUtils.getCurrentTime());
            order.setApprovedBy(currentUser);
            order.setVanSales(true);
        } else {
            order.setApproveStatus(Order.APPROVE_STATUS_PENDING);
            order.setVanSales(false);
        }

        orderCalculationService.calculate(userLogin, distributor.getId(), dto, order,
                getDistributorPriceList(userLogin));

        I_Order _order = orderPendingRepository.create(userLogin.getClientId(), order);

        if (_order.getApproveStatus() == Order.APPROVE_STATUS_PENDING) {
            // Notify
            webNotificationEngine.notifyChangedOrder(userLogin, _order, WebNotificationService.ACTION_ORDER_ADD);
        } else {
            cacheVisitOrderService.addNewApprovedOrder(_order);
        }

        return new IdDto(_order);
    }

    @Override
    public ListDto<OrderSimpleDto> getOrdersCreatedToday(UserLogin userLogin, String _customerId) {
        Collection<String> customerIds = null;
        if (_customerId != null) {
            customerIds = Collections.singleton(_customerId);
        }

        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();

        List<I_OrderHeader> orders = orderPendingRepository.getOrdersCreatedToday(userLogin.getClientId(),
                distributorId, salesman.getId(), customerIds);

        if (orders.isEmpty()) {
            return ListDto.emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<OrderSimpleDto>(orders.size());
        for (I_OrderHeader order : orders) {
            OrderSimpleDto dto = new OrderSimpleDto(order);
            dtos.add(dto);
        }

        return new ListDto<OrderSimpleDto>(dtos);
    }

    @Override
    public OrderDto getOrderById(UserLogin userLogin, String _orderId) {
        I_Order order = getMandatoryPO(userLogin, _orderId, orderPendingRepository);

        I_User user = getCurrentUser(userLogin);
        BusinessAssert.equals(user.getId(), order.getCreatedBy().getId(), "order is not created by me");

        return new OrderDto(order, getProductPhotoFactory(userLogin));
    }

    private Map<String, BigDecimal> calculateProductAvailable(UserLogin userLogin, Collection<I_Product> products,
            I_Distributor distributor) {
        String clientId = userLogin.getClientId();
        Map<String, I_Product> productMap = getPOMap(products);

        I_Inventory inventory = inventoryRepository.getLatestInventory(clientId, distributor.getId());
        if (inventory == null) {
            return Collections.emptyMap();
        }

        Map<String, BigDecimal> availableMap = new HashMap<>(products.size());
        // Put original available to map
        for (I_ProductQuantity productQuantity : inventory.getDetails()) {
            // Skip products not contains in list
            if (!productMap.containsKey(productQuantity.getId())) {
                continue;
            }
            availableMap.put(productQuantity.getId(), productQuantity.getQuantity());
        }

        // Subtract sold quantity from map
        Map<String, Double> productSoldMap = cacheVisitOrderService
                .getQuantitySoldByProductFromLastInventoryUpdate(clientId, distributor.getId());

        if (productSoldMap != null) {
            for (Map.Entry<String, Double> entry : productSoldMap.entrySet()) {
                BigDecimal original = availableMap.get(entry.getKey());
                // Skip products not have inventory
                if (original == null) {
                    continue;
                }
                availableMap.put(entry.getKey(), original.subtract(new BigDecimal(entry.getValue())));
            }
        }

        return availableMap;
    }

}
