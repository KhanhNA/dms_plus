package com.viettel.backend.service.core.order.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractExportService;
import com.viettel.backend.service.core.order.OrderExportService;
import com.viettel.backend.service.core.pdto.Order;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.OrderRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.VisitRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderDetail;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class OrderExportServiceImpl extends AbstractExportService implements OrderExportService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private VisitRepository visitRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Override
	public InputStream exportOrder(UserLogin userLogin, String _distributorId, String _salesmanId, String _customerId,
			String _fromDate, String _toDate, String lang) {
		lang = getLang(lang);

		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		Collection<String> distributorIds = null;
		if (_distributorId == null) {
			distributorIds = Collections.singleton(getMandatoryDistributor(userLogin, _distributorId).getId());
		} else if ("all".equalsIgnoreCase(_distributorId)) {
			distributorIds = getIdSet(getAccessibleDistributors(userLogin));
		} else if (_distributorId.startsWith("all_")) {
			_distributorId = _distributorId.substring(_distributorId.indexOf("_") + 1);
			if ("all".equalsIgnoreCase(_distributorId)) {
				distributorIds = getIdSet(getAccessibleDistributors(userLogin));
			} else {
				I_Region region = regionRepository.getById(userLogin.getClientId(), _distributorId);
				distributorIds = userRepository.getDistributorForRegionRecursively(userLogin.getClientId(), region,
						new ArrayList<>());
			}
		} else {
			distributorIds = Collections.singleton(getMandatoryDistributor(userLogin, _distributorId).getId());
		}

		String salesmanId = null;
		if (_salesmanId != null) {
			I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);
			BusinessAssert.equals(salesman.getRole(), Role.SALESMAN);
			BusinessAssert.notNull(salesman.getDistributor());
			BusinessAssert.contain(distributorIds, salesman.getDistributor().getId());
			salesmanId = salesman.getId();
		}

		String customerId = null;
		if (_customerId != null) {
			I_Customer customer = getMandatoryPO(userLogin, _customerId, customerRepository);
			BusinessAssert.notNull(customer.getDistributor());
			// BusinessAssert.equals(customer.getDistributor().getId(),
			// distributorId);
			BusinessAssert.contain(distributorIds, customer.getDistributor().getId());
			customerId = customer.getId();
		}

		I_Config config = getConfig(userLogin);

		SimpleDate today = DateTimeUtils.getToday();

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "order.list"));

			Row row;

			String[] headers = new String[] { "distributor.name", "distributor.code", "code", "created.date",
					"with.visit", "salesman.fullname", "salesman.username", "customer.name", "customer.code",
					"customer.area", "customer.type", "delivery", "quantity", "sub.total", "promotion.amt",
					"discount.amt", "grand.total" };

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, headers.length - 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "order.list"));

			row = sheet.createRow(3);
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 1));
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "from.date") + ": " + fromDate.format(config.getDateFormat()));
			createCell(row, 2, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "to.date") + ": " + toDate.format(config.getDateFormat()));

			row = sheet.createRow(4);
			sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 3));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "issued.date") + ": " + today.format(config.getDateFormat()));

			// Create table headers
			row = sheet.createRow(7);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			BigDecimal totalQuantity = BigDecimal.ZERO;
			BigDecimal totalSubTotal = BigDecimal.ZERO;
			BigDecimal totalPromotionAmt = BigDecimal.ZERO;
			BigDecimal totalDiscountAmt = BigDecimal.ZERO;
			BigDecimal totalGrandTotal = BigDecimal.ZERO;

			int rownum = 8;
			long count = 0;
			for (String distributorId : distributorIds) {
				I_Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);
				row = sheet.createRow(rownum++);
				createCell(row, 0, textCellStyle, distributor.getName());
				createCell(row, 1, textCellStyle, distributor.getCode());
				createCell(row, 2, textCellStyle, "");
				createCell(row, 3, textCellStyle, "");
				createCell(row, 4, textCellStyle, "");
				createCell(row, 5, textCellStyle, "");
				createCell(row, 6, textCellStyle, "");
				createCell(row, 7, textCellStyle, "");
				createCell(row, 8, textCellStyle, "");
				createCell(row, 9, textCellStyle, "");
				createCell(row, 10, textCellStyle, "");
				createCell(row, 11, textCellStyle, "");
				createCell(row, 12, textCellStyle, "");
				createCell(row, 13, textCellStyle, "");
				createCell(row, 14, textCellStyle, "");
				createCell(row, 15, textCellStyle, "");
				createCell(row, 16, textCellStyle, "");

				long countByDistributor = orderRepository.countOrdersByDistributor(userLogin.getClientId(),
						distributorId, salesmanId, customerId, period, OrderDateType.CREATED_DATE);
				count += countByDistributor;
				int nbRecordByQuery = 2000;
				int nbQuery = ((int) countByDistributor / nbRecordByQuery)
						+ ((countByDistributor % nbRecordByQuery) == 0 ? 0 : 1);

				for (int page = 1; page <= nbQuery; page++) {
					PageSizeRequest pageSizeRequest = new PageSizeRequest(page - 1, nbRecordByQuery);

					List<I_OrderHeader> orders = orderRepository.getOrderHeadersByDistributor(userLogin.getClientId(),
							distributorId, salesmanId, customerId, period, OrderDateType.CREATED_DATE, pageSizeRequest,
							true);

					for (int i = 0, length = orders.size(); i < length; i++) {
						I_OrderHeader order = orders.get(i);

						row = sheet.createRow(rownum++);

						createCell(row, 0, textCellStyle, "");
						createCell(row, 1, textCellStyle, "");

						createCell(row, 2, textCellStyle, order.getCode());
						createCell(row, 3, textCellStyle, order.getCreatedTime().format(config.getDateFormat()));

						if (order.isWithVisit()) {
							createCell(row, 4, textCellStyle, "x");
						} else {
							createCell(row, 4, textCellStyle, "");
						}

						createCell(row, 5, textCellStyle, order.getCreatedBy().getFullname());
						createCell(row, 6, textCellStyle, order.getCreatedBy().getUsername());

						createCell(row, 7, textCellStyle, order.getCustomer().getName());
						createCell(row, 8, textCellStyle, order.getCustomer().getCode());
						createCell(row, 9, textCellStyle, order.getCustomer().getArea().getName());
						createCell(row, 10, textCellStyle, order.getCustomer().getCustomerType().getName());

						// Delivery
						if (order.isVanSales()) {
							createCell(row, 11, textCellStyle, translate(lang, "van.sales"));
						} else {
							if (order.getDeliveryType() == I_Order.DELIVERY_TYPE_ANOTHER_DAY
									&& order.getDeliveryTime() != null) {
								createCell(row, 11, textCellStyle,
										order.getDeliveryTime().format(config.getDateFormat()));
							} else if (order.getDeliveryType() == I_Order.DELIVERY_TYPE_IN_DAY) {
								createCell(row, 11, textCellStyle, translate(lang, "same.day"));
							} else {
								createCell(row, 11, textCellStyle, translate(lang, "immediately"));
							}
						}

						createCell(row, 12, numberCellStyle, order.getQuantity().doubleValue());
						totalQuantity = totalQuantity.add(order.getQuantity());

						createCell(row, 13, numberCellStyle, order.getSubTotal().doubleValue());
						totalSubTotal = totalSubTotal.add(order.getSubTotal());

						if (order.getPromotionAmt() != null) {
							createCell(row, 14, numberCellStyle, order.getPromotionAmt().doubleValue());
							totalPromotionAmt = totalPromotionAmt.add(order.getPromotionAmt());
						} else {
							createCell(row, 14, numberCellStyle, 0.0);
						}

						if (order.getDiscountAmt() != null) {
							createCell(row, 15, numberCellStyle, order.getDiscountAmt().doubleValue());
							totalDiscountAmt = totalDiscountAmt.add(order.getDiscountAmt());
						} else {
							createCell(row, 15, numberCellStyle, 0.0);
						}

						createCell(row, 16, numberCellStyle, order.getGrandTotal().doubleValue());
						totalGrandTotal = totalGrandTotal.add(order.getGrandTotal());

					}
				}
			}
			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle footerNumberCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_RIGHT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, 1));
			createCell(row, 0, footerTextCellStyle, count + " " + translate(lang, "record(s)"));
			createCell(row, headers.length - 5, footerNumberCellStyle, totalQuantity.doubleValue());
			createCell(row, headers.length - 4, footerNumberCellStyle, totalSubTotal.doubleValue());
			createCell(row, headers.length - 3, footerNumberCellStyle, totalPromotionAmt.doubleValue());
			createCell(row, headers.length - 2, footerNumberCellStyle, totalDiscountAmt.doubleValue());
			createCell(row, headers.length - 1, footerNumberCellStyle, totalGrandTotal.doubleValue());

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			File outTempFile = File
					.createTempFile("OrderList" + userLogin.getClientId() + "_" + System.currentTimeMillis(), "tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new FileInputStream(outTempFile);
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

	@Override
	public InputStream exportOrderDetail(UserLogin userLogin, String _distributorId, String _salesmanId,
			String _customerId, String _fromDate, String _toDate, String lang) {
		lang = getLang(lang);

		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		Collection<String> distributorIds = null;
		if (_distributorId == null) {
			distributorIds = Collections.singleton(getMandatoryDistributor(userLogin, _distributorId).getId());
		} else if ("all".equalsIgnoreCase(_distributorId)) {
			distributorIds = getIdSet(getAccessibleDistributors(userLogin));
		} else if (_distributorId.startsWith("all_")) {
			_distributorId = _distributorId.substring(_distributorId.indexOf("_") + 1);
			if ("all".equalsIgnoreCase(_distributorId)) {
				distributorIds = getIdSet(getAccessibleDistributors(userLogin));
			} else {
				I_Region region = regionRepository.getById(userLogin.getClientId(), _distributorId);
				distributorIds = userRepository.getDistributorForRegionRecursively(userLogin.getClientId(), region,
						new ArrayList<>());
			}
		} else {
			distributorIds = Collections.singleton(getMandatoryDistributor(userLogin, _distributorId).getId());
		}
		String salesmanId = null;
		if (_salesmanId != null) {
			I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);
			BusinessAssert.equals(salesman.getRole(), Role.SALESMAN);
			BusinessAssert.notNull(salesman.getDistributor());
			BusinessAssert.contain(distributorIds, salesman.getDistributor().getId());
			salesmanId = salesman.getId();
		}

		String customerId = null;
		if (_customerId != null) {
			I_Customer customer = getMandatoryPO(userLogin, _customerId, customerRepository);
			BusinessAssert.notNull(customer.getDistributor());
			BusinessAssert.contain(distributorIds, customer.getDistributor().getId());
			customerId = customer.getId();
		}

		I_Config config = getConfig(userLogin);

		SimpleDate today = DateTimeUtils.getToday();

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "order.detail.list"));

			Row row;

			String[] headers = new String[] { "distributor.name", "distributor.code", "code", "created.date",
					"with.visit", "salesman.fullname", "salesman.username", "customer.name", "customer.code",
					"customer.area", "customer.type", "delivery", "product.code", "product.name", "uom", "price",
					"quantity", "amount" };

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 8));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "order.detail.list"));

			row = sheet.createRow(3);
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 1));
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "from.date") + ": " + fromDate.format(config.getDateFormat()));
			createCell(row, 2, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "to.date") + ": " + toDate.format(config.getDateFormat()));

			row = sheet.createRow(4);
			sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "issued.date") + ": " + today.format(config.getDateFormat()));

			// Create table headers

			row = sheet.createRow(7);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			int rownum = 8;
			long count = 0;
			for (String distributorId : distributorIds) {
				I_Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);
				row = sheet.createRow(rownum++);
				createCell(row, 0, textCellStyle, distributor.getName());
				createCell(row, 1, textCellStyle, distributor.getCode());
				createCell(row, 2, textCellStyle, "");
				createCell(row, 3, textCellStyle, "");
				createCell(row, 4, textCellStyle, "");
				createCell(row, 5, textCellStyle, "");
				createCell(row, 6, textCellStyle, "");
				createCell(row, 7, textCellStyle, "");
				createCell(row, 8, textCellStyle, "");
				createCell(row, 9, textCellStyle, "");
				createCell(row, 10, textCellStyle, "");
				createCell(row, 11, textCellStyle, "");
				createCell(row, 12, textCellStyle, "");
				createCell(row, 13, textCellStyle, "");
				createCell(row, 14, textCellStyle, "");
				createCell(row, 15, textCellStyle, "");
				createCell(row, 16, textCellStyle, "");
				createCell(row, 17, textCellStyle, "");

				long countByDistributor = orderRepository.countOrdersByDistributor(userLogin.getClientId(),
						distributorId, salesmanId, customerId, period, OrderDateType.CREATED_DATE);
				count += countByDistributor;
				int nbRecordByQuery = 2000;
				int nbQuery = ((int) countByDistributor / nbRecordByQuery)
						+ ((countByDistributor % nbRecordByQuery) == 0 ? 0 : 1);

				for (int page = 1; page <= nbQuery; page++) {
					PageSizeRequest pageSizeRequest = new PageSizeRequest(page - 1, nbRecordByQuery);

					List<I_Order> orders = orderRepository.getOrdersByDistributor(userLogin.getClientId(),
							distributorId, salesmanId, customerId, period, OrderDateType.CREATED_DATE, pageSizeRequest,
							true);

					for (int i = 0, length = orders.size(); i < length; i++) {
						I_Order order = orders.get(i);

						for (int j = 0, dl = order.getDetails().size(); j < dl; j++) {
							I_OrderDetail detail = order.getDetails().get(j);

							row = sheet.createRow(rownum++);

							createCell(row, 0, textCellStyle, "");
							createCell(row, 1, textCellStyle, "");

							createCell(row, 2, textCellStyle, order.getCode());
							createCell(row, 3, textCellStyle, order.getCreatedTime().format(config.getDateFormat()));

							if (order.isWithVisit()) {
								createCell(row, 4, textCellStyle, "x");
							} else {
								createCell(row, 4, textCellStyle, "");
							}

							createCell(row, 5, textCellStyle, order.getCreatedBy().getFullname());
							createCell(row, 6, textCellStyle, order.getCreatedBy().getUsername());

							createCell(row, 7, textCellStyle, order.getCustomer().getName());
							createCell(row, 8, textCellStyle, order.getCustomer().getCode());
							createCell(row, 9, textCellStyle, order.getCustomer().getArea().getName());
							createCell(row, 10, textCellStyle, order.getCustomer().getCustomerType().getName());

							// Delivery
							if (order.isVanSales()) {
								createCell(row, 11, textCellStyle, translate(lang, "van.sales"));
							} else {
								if (order.getDeliveryType() == I_Order.DELIVERY_TYPE_ANOTHER_DAY
										&& order.getDeliveryTime() != null) {
									createCell(row, 11, textCellStyle,
											order.getDeliveryTime().format(config.getDateFormat()));
								} else if (order.getDeliveryType() == I_Order.DELIVERY_TYPE_IN_DAY) {
									createCell(row, 11, textCellStyle, translate(lang, "same.day"));
								} else {
									createCell(row, 11, textCellStyle, translate(lang, "immediately"));
								}
							}

							createCell(row, 12, textCellStyle, detail.getProduct().getCode());
							createCell(row, 13, textCellStyle, detail.getProduct().getName());
							createCell(row, 14, textCellStyle, detail.getProduct().getUom().getName());
							createCell(row, 15, numberCellStyle, detail.getProduct().getPrice().doubleValue());
							createCell(row, 16, numberCellStyle, detail.getQuantity().doubleValue());
							createCell(row, 17, numberCellStyle, detail.getAmount().doubleValue());
						}

					}
				}
			}
			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);
			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, 1));
			createCell(row, 0, footerTextCellStyle, count + " " + translate(lang, "order"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			File outTempFile = File.createTempFile(
					"OrderDetailList" + userLogin.getClientId() + "_" + System.currentTimeMillis(), "tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new FileInputStream(outTempFile);
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

	@Override
	public ExportDto exportOrderToday(UserLogin userLogin, String lang, boolean isVisit, boolean hasOrder) {
		lang = getLang(lang);
		Period period = DateTimeUtils.getPeriodToday();
		List<I_Distributor> accessibleDistributors = getAccessibleDistributors(userLogin);

		I_Config config = getConfig(userLogin);

		SimpleDate today = DateTimeUtils.getCurrentTime();

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "order.list"));

			Row row;

			String[] headers = new String[] { "distributor.name", "distributor.code", "code", "salesman.fullname",
					"salesman.username", "customer.name", "customer.code", "customer.area", "customer.type", "approved",
					"delivery", "quantity", "sub.total", "promotion.amt", "discount.amt", "grand.total" };

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, headers.length - 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, isVisit ? "export.order.in.visit" : "export.order.not.in.visit"));

			row = sheet.createRow(2);
			sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, headers.length - 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					today.format("dd/MM/yyyy HH:mm"));

			// Create table headers
			row = sheet.createRow(4);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			BigDecimal totalQuantity = BigDecimal.ZERO;
			BigDecimal totalSubTotal = BigDecimal.ZERO;
			BigDecimal totalPromotionAmt = BigDecimal.ZERO;
			BigDecimal totalDiscountAmt = BigDecimal.ZERO;
			BigDecimal totalGrandTotal = BigDecimal.ZERO;

			int rownum = 5;
			long count = 0;
			for (I_Distributor distributor : accessibleDistributors) {

				row = sheet.createRow(rownum++);
				createCell(row, 0, textCellStyle, distributor.getName());
				createCell(row, 1, textCellStyle, distributor.getCode());
				for (int i = 2; i < headers.length; i++) {
					createCell(row, i, textCellStyle, "");
				}
				long countByDistributor = 0l;
				if (!isVisit) {
					countByDistributor = orderRepository.countOrdersByDistributor(userLogin.getClientId(),
							distributor.getId(), isVisit, hasOrder, period, OrderDateType.CREATED_DATE);
				} else {
					countByDistributor = visitRepository.countVisitedsByDistributor(userLogin.getClientId(),
							distributor.getId(), null, null, period);
				}
				count += countByDistributor;
				int nbRecordByQuery = 2000;
				int nbQuery = ((int) countByDistributor / nbRecordByQuery)
						+ ((countByDistributor % nbRecordByQuery) == 0 ? 0 : 1);

				for (int page = 1; page <= nbQuery; page++) {
					PageSizeRequest pageSizeRequest = new PageSizeRequest(page - 1, nbRecordByQuery);

					List<I_OrderHeader> orders = new ArrayList<>();
					if (!isVisit) {
						orders = orderRepository.getOrdersHeaderByDistributor(userLogin.getClientId(),
								distributor.getId(), isVisit, hasOrder, period, OrderDateType.CREATED_DATE,
								pageSizeRequest, true);
					} else {
						orders = visitRepository.getOrderByDistributor(userLogin.getClientId(), distributor.getId(),
								period, pageSizeRequest, true);
					}

					for (int i = 0, length = orders.size(); i < length; i++) {
						I_OrderHeader order = orders.get(i);

						row = sheet.createRow(rownum++);

						createCell(row, 0, textCellStyle, "");
						createCell(row, 1, textCellStyle, "");

						createCell(row, 2, textCellStyle, order.getCode());

						createCell(row, 3, textCellStyle, order.getCreatedBy().getFullname());
						createCell(row, 4, textCellStyle, order.getCreatedBy().getUsername());

						createCell(row, 5, textCellStyle, order.getCustomer().getName());
						createCell(row, 6, textCellStyle, order.getCustomer().getCode());
						createCell(row, 7, textCellStyle, order.getCustomer().getArea().getName());
						createCell(row, 8, textCellStyle, order.getCustomer().getCustomerType().getName());
						createCell(row, 9, textCellStyle,
								order.getApproveStatus() == Order.APPROVE_STATUS_APPROVED ? "x" : "");
						// Delivery
						if (order.isVanSales()) {
							createCell(row, 10, textCellStyle, translate(lang, "van.sales"));
						} else {
							if (order.getDeliveryType() == I_Order.DELIVERY_TYPE_ANOTHER_DAY
									&& order.getDeliveryTime() != null) {
								createCell(row, 10, textCellStyle,
										order.getDeliveryTime().format(config.getDateFormat()));
							} else if (order.getDeliveryType() == I_Order.DELIVERY_TYPE_IN_DAY) {
								createCell(row, 10, textCellStyle, translate(lang, "same.day"));
							} else {
								createCell(row, 10, textCellStyle, translate(lang, "immediately"));
							}
						}

						createCell(row, 11, numberCellStyle, order.getQuantity().doubleValue());
						totalQuantity = totalQuantity.add(order.getQuantity());

						createCell(row, 12, numberCellStyle, order.getSubTotal().doubleValue());
						totalSubTotal = totalSubTotal.add(order.getSubTotal());

						if (order.getPromotionAmt() != null) {
							createCell(row, 13, numberCellStyle, order.getPromotionAmt().doubleValue());
							totalPromotionAmt = totalPromotionAmt.add(order.getPromotionAmt());
						} else {
							createCell(row, 13, numberCellStyle, 0.0);
						}

						if (order.getDiscountAmt() != null) {
							createCell(row, 14, numberCellStyle, order.getDiscountAmt().doubleValue());
							totalDiscountAmt = totalDiscountAmt.add(order.getDiscountAmt());
						} else {
							createCell(row, 14, numberCellStyle, 0.0);
						}

						createCell(row, 15, numberCellStyle, order.getGrandTotal().doubleValue());
						totalGrandTotal = totalGrandTotal.add(order.getGrandTotal());

					}
				}
			}
			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle footerNumberCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_RIGHT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, 1));
			createCell(row, 0, footerTextCellStyle, count + " " + translate(lang, "record(s)"));
			createCell(row, headers.length - 5, footerNumberCellStyle, totalQuantity.doubleValue());
			createCell(row, headers.length - 4, footerNumberCellStyle, totalSubTotal.doubleValue());
			createCell(row, headers.length - 3, footerNumberCellStyle, totalPromotionAmt.doubleValue());
			createCell(row, headers.length - 2, footerNumberCellStyle, totalDiscountAmt.doubleValue());
			createCell(row, headers.length - 1, footerNumberCellStyle, totalGrandTotal.doubleValue());

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			StringBuilder fileName = new StringBuilder();
			fileName.append("OrderToday_").append(DateTimeUtils.getCurrentTime().format("yyyyMMddHHmm"))
					.append(".xlsx");

			File outTempFile = File.createTempFile(fileName + "_" + System.currentTimeMillis(), ".tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new ExportDto(fileName.toString(), new FileInputStream(outTempFile));
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}
}
