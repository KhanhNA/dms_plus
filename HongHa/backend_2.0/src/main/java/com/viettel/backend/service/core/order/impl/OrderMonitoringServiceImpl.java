package com.viettel.backend.service.core.order.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.dto.order.OrderSimpleDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.order.OrderMonitoringService;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.OrderPendingRepository;
import com.viettel.repository.common.OrderRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class OrderMonitoringServiceImpl extends AbstractService implements OrderMonitoringService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderRepository orderRepository;
    
    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Override
    public ListDto<OrderSimpleDto> getOrders(UserLogin userLogin, String _distributorId, String _salesmanId,
            String _customerId, String _fromDate, String _toDate, PageSizeRequest pageSizeRequest) {
        SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
        SimpleDate toDate = getMandatoryIsoDate(_toDate);
        BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
        BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

        Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

        I_Distributor distributor = getMandatoryDistributor(userLogin, _distributorId);
        String distributorId = distributor.getId();

        String salesmanId = null;
        if (_salesmanId != null) {
            I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);
            BusinessAssert.equals(salesman.getRole(), Role.SALESMAN);
            BusinessAssert.notNull(salesman.getDistributor());
            BusinessAssert.equals(salesman.getDistributor().getId(), distributorId);
            salesmanId = salesman.getId();
        }

        String customerId = null;
        if (_customerId != null) {
            I_Customer customer = getMandatoryPO(userLogin, _customerId, customerRepository);
            BusinessAssert.notNull(customer.getDistributor());
            BusinessAssert.equals(customer.getDistributor().getId(), distributorId);
            customerId = customer.getId();
        }

        I_Config config = getConfig(userLogin);

        Collection<I_Order> orders = orderRepository.getOrdersByDistributor(userLogin.getClientId(), distributorId,
                salesmanId, customerId, period, config.getOrderDateType(), pageSizeRequest, false);
        if (CollectionUtils.isEmpty(orders) && pageSizeRequest.getPageNumber() == 0) {
            ListDto.emptyList();
        }

        List<OrderSimpleDto> dtos = new ArrayList<OrderSimpleDto>(orders.size());
        for (I_Order order : orders) {
            dtos.add(new OrderSimpleDto(order));
        }

        long size = Long.valueOf(dtos.size());
        if (pageSizeRequest != null) {
            if (pageSizeRequest.getPageNumber() > 0 || pageSizeRequest.getPageSize() == size) {
                size = orderRepository.countOrdersByDistributor(userLogin.getClientId(), distributorId, salesmanId,
                        customerId, period, config.getOrderDateType());
            }
        }

        return new ListDto<OrderSimpleDto>(dtos, size);
    }

    @Override
    public IdDto getOrderByCode(UserLogin userLogin, String code) {
        I_Order order = orderRepository.getOrderByCode(userLogin.getClientId(), code);

        if (order == null || !checkAccessible(userLogin, order.getDistributor().getId())) {
            return null;
        }

        return new IdDto(order);
    }

    @Override
    public OrderDto getOrderById(UserLogin userLogin, String _orderId) {
        I_Order order = getMandatoryPO(userLogin, _orderId, orderPendingRepository);

        BusinessAssert.isTrue(order.getApproveStatus() == I_Order.APPROVE_STATUS_APPROVED);
        
        BusinessAssert.isTrue(checkAccessible(userLogin, order.getDistributor().getId()),
                "distributor is not accessible");

        return new OrderDto(order, getProductPhotoFactory(userLogin));
    }

}
