package com.viettel.backend.service.core.order.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.order.DistributorSalesResultDto;
import com.viettel.backend.dto.order.ProductSalesResultDto;
import com.viettel.backend.dto.order.SalesResultDailyDto;
import com.viettel.backend.dto.order.SalesResultDto;
import com.viettel.backend.dto.order.SalesmanSalesResultDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.order.SalesReportService;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.OrderSummaryRepository;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.ProductImportantRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.data.I_OrderSummary;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN, Role.ADVANCE_SUPERVISOR })
@Service
public class SalesReportServiceImpl extends AbstractService implements SalesReportService {

	@Autowired
	private ProductImportantRepository productImportantRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductCategoryRepository productCategoryRepository;

	@Autowired
	private OrderSummaryRepository orderSummaryRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Override
	public ListDto<SalesResultDailyDto> getSalesResultDaily(UserLogin userLogin, String regionId, String distributorId,
			String _fromDate, String _toDate) {
		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		Set<String> accessibleDistributorIds = new HashSet<>();
		if (Role.ADVANCE_SUPERVISOR.equalsIgnoreCase(userLogin.getRole())) {
			accessibleDistributorIds = getIdSet(getDistributorsByRegion(userLogin, regionId));
		} else {
			accessibleDistributorIds = getIdSet(getAccessibleDistributors(userLogin));
		}

		if (CollectionUtils.isEmpty(accessibleDistributorIds)) {
			return ListDto.emptyList();
		}

		Set<String> distributorIds;
		if (!"all".equalsIgnoreCase(distributorId)) {
			I_Distributor distributor = getMandatoryPO(userLogin, distributorId, distributorRepository);
			BusinessAssert.contain(accessibleDistributorIds, distributor.getId());
			distributorIds = Collections.singleton(distributor.getId());
		} else {
			distributorIds = accessibleDistributorIds;
		}

		I_Config config = getConfig(userLogin);

		Map<String, I_OrderSummary> orderSummaryDaily = orderSummaryRepository
				.getOrderSummaryDaily(userLogin.getClientId(), distributorIds, period, config.getOrderDateType());

		List<SalesResultDailyDto> dtos = new LinkedList<SalesResultDailyDto>();
		SimpleDate tempDate = period.getFromDate();
		while (tempDate.compareTo(period.getToDate()) < 0) {
			String isoDate = tempDate.getIsoDate();
			I_OrderSummary orderSummary = orderSummaryDaily.get(isoDate);

			SalesResultDto salesResultDto;
			if (orderSummary == null) {
				salesResultDto = new SalesResultDto();
			} else {
				salesResultDto = new SalesResultDto(orderSummary);
			}

			dtos.add(new SalesResultDailyDto(isoDate, salesResultDto));

			tempDate = DateTimeUtils.addDays(tempDate, 1);
		}

		return new ListDto<SalesResultDailyDto>(dtos);
	}

	@Override
	public ListDto<DistributorSalesResultDto> getDistributorSalesResult(UserLogin userLogin, String _fromDate,
			String _toDate) {
		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		Set<String> distributorIds = getIdSet(distributors);

		I_Config config = getConfig(userLogin);

		Map<String, I_OrderSummary> orderSummaryByDistributor = orderSummaryRepository.getOrderSummaryByDistributor(
				userLogin.getClientId(), distributorIds, period, config.getOrderDateType());

		List<DistributorSalesResultDto> dtos = new LinkedList<DistributorSalesResultDto>();
		for (I_Distributor distributor : distributors) {
			I_OrderSummary orderSummary = orderSummaryByDistributor.get(distributor.getId());

			SalesResultDto salesResultDto;
			if (orderSummary == null) {
				salesResultDto = new SalesResultDto();
			} else {
				salesResultDto = new SalesResultDto(orderSummary);
			}

			dtos.add(new DistributorSalesResultDto(distributor, salesResultDto));
		}

		return new ListDto<DistributorSalesResultDto>(dtos);
	}

	@Override
	public ListDto<ProductSalesResultDto> getProductSalesResult(UserLogin userLogin, String regionId,
			String distributorId, String _fromDate, String _toDate, String _productCategoryId,
			String _isImportantProduct) {
		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		Boolean isImportantProduct = BooleanUtils.toBooleanObject(_isImportantProduct);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		Set<String> accessibleDistributorIds = new HashSet<>();
		if (Role.ADVANCE_SUPERVISOR.equalsIgnoreCase(userLogin.getRole())) {
			accessibleDistributorIds = getIdSet(getDistributorsByRegion(userLogin, regionId));
		} else {
			accessibleDistributorIds = getIdSet(getAccessibleDistributors(userLogin));
		}

		if (CollectionUtils.isEmpty(accessibleDistributorIds)) {
			return ListDto.emptyList();
		}

		Set<String> distributorIds;
		if (!"all".equalsIgnoreCase(distributorId)) {
			I_Distributor distributor = getMandatoryPO(userLogin, distributorId, distributorRepository);
			BusinessAssert.contain(accessibleDistributorIds, distributor.getId());
			distributorIds = Collections.singleton(distributor.getId());
		} else {
			distributorIds = accessibleDistributorIds;
		}

		I_Config config = getConfig(userLogin);

		List<I_Product> products = Collections.emptyList();
		Collection<String> productCategoryIds;
		if (!"all".equalsIgnoreCase(_productCategoryId)) {
			productCategoryIds = Collections.singleton(_productCategoryId);
			I_Category productCategory = getMandatoryPO(userLogin, _productCategoryId, productCategoryRepository);
			products = productRepository.getProductsByCategories(userLogin.getClientId(),
					Collections.singleton(productCategory.getId()));
			if (products == null || products.isEmpty()) {
				return ListDto.emptyList();
			}
		} else {
			products = productRepository.getList(userLogin.getClientId(), false, true, null, null, null, null);
			productCategoryIds = getIdSet(
					productCategoryRepository.getList(userLogin.getClientId(), false, true, null, null, null, null));
		}

		if (isImportantProduct) {// xxxx
			Set<String> productImportantIds = productImportantRepository.getProductImportant(userLogin.getClientId());
			List<I_Product> temp = new ArrayList<>();
			for (I_Product product : products) {
				if (productImportantIds.contains(product.getId())) {
					temp.add(product);
				}
			}
			products = temp;
		}

		I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);

		Map<String, I_OrderSummary> orderSummaryByProduct = orderSummaryRepository.getOrderSummaryByProduct(
				userLogin.getClientId(), distributorIds, productCategoryIds, period, config.getOrderDateType());

		List<ProductSalesResultDto> dtos = new ArrayList<ProductSalesResultDto>(products.size());
		for (I_Product product : products) {
			I_OrderSummary orderSummary = orderSummaryByProduct.get(product.getId());

			SalesResultDto salesResultDto;
			if (orderSummary == null) {
				salesResultDto = new SalesResultDto();
			} else {
				salesResultDto = new SalesResultDto(orderSummary);
			}

			dtos.add(new ProductSalesResultDto(product, productPhotoFactory, salesResultDto));
		}

		return new ListDto<ProductSalesResultDto>(dtos);
	}

	@Override
	public ListDto<SalesmanSalesResultDto> getSalesmanSalesResult(UserLogin userLogin, String _distributorId,
			String _fromDate, String _toDate) {
		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		Set<String> distributorIds = Collections.emptySet();
		if ("all".equalsIgnoreCase(_distributorId)) {
			List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
			distributorIds = getIdSet(distributors);
		} else {
			I_Distributor distributor = getMandatoryDistributor(userLogin, _distributorId);
			distributorIds = Collections.singleton(distributor.getId());
		}

		I_Config config = getConfig(userLogin);

		Map<String, I_OrderSummary> orderSummaryBySalesman = orderSummaryRepository
				.getOrderSummaryBySalesman(userLogin.getClientId(), distributorIds, period, config.getOrderDateType());

		List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(), distributorIds);

		List<SalesmanSalesResultDto> dtos = new ArrayList<SalesmanSalesResultDto>(salesmen.size());
		for (I_User salesman : salesmen) {
			I_OrderSummary orderSummary = orderSummaryBySalesman.get(salesman.getId());

			SalesResultDto salesResultDto;
			if (orderSummary == null) {
				salesResultDto = new SalesResultDto();
			} else {
				salesResultDto = new SalesResultDto(orderSummary);
			}

			dtos.add(new SalesmanSalesResultDto(salesman, salesResultDto));
		}

		return new ListDto<SalesmanSalesResultDto>(dtos);
	}

}
