package com.viettel.backend.service.core.pdto;

import java.io.Serializable;
import java.util.List;

import com.viettel.repository.common.domain.category.I_CalendarConfig;
import com.viettel.repository.common.entity.SimpleDate;

public class CalendarConfig implements Serializable, I_CalendarConfig {

    private static final long serialVersionUID = 7763979551635207290L;

    private List<Integer> workingDays;
    private List<SimpleDate> holidays;
    
    public CalendarConfig() {
        super();
    }
    
    public CalendarConfig(I_CalendarConfig calendarConfig) {
        super();
        
        this.workingDays = calendarConfig.getWorkingDays();
        this.holidays = calendarConfig.getHolidays();
    }

    public List<Integer> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<Integer> workingDays) {
        this.workingDays = workingDays;
    }

    public List<SimpleDate> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<SimpleDate> holidays) {
        this.holidays = holidays;
    }

}
