package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.common.I_Category;

public class Category extends PO implements I_Category {

    private static final long serialVersionUID = -3739256558534756621L;
    
    private String name;
    private String code;
    private CategoryEmbed distributor;
    
    public Category() {
        super();
    }
    
    public Category(I_Category category) {
        super(category);
        
        this.name = category.getName();
        this.code = category.getCode();
        if (category.getDistributor() != null) {
            this.distributor = new CategoryEmbed(category.getDistributor());
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(CategoryEmbed distributor) {
        this.distributor = distributor;
    }

}
