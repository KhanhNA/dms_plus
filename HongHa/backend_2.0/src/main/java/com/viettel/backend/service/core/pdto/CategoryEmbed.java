package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;

public class CategoryEmbed extends POEmbed implements I_CategoryEmbed {

    private static final long serialVersionUID = -3109639278504351840L;
    
    private String name;
    private String code;
    
    public CategoryEmbed() {
        super();
    }
    
    public CategoryEmbed(I_CategoryEmbed categoryEmbed) {
        super(categoryEmbed);
        
        this.name = categoryEmbed.getName();
        this.code = categoryEmbed.getCode();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
