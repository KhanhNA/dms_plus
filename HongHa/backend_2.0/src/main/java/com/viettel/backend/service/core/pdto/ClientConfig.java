package com.viettel.backend.service.core.pdto;

import java.util.Set;

import com.viettel.repository.common.domain.category.I_ClientConfig;
import com.viettel.repository.common.entity.Location;

public class ClientConfig implements I_ClientConfig {

	private static final long serialVersionUID = 5202363354970843896L;

	// DEFAULT
	private Location location;

	private long visitDurationKPI;
	private double visitDistanceKPI;
	private boolean canEditCustomerLocation;
	private int limitAccount;

	private Set<String> modules;
	private int numberDayInventoryExpire;
	private String productivityUnit;

	public ClientConfig() {
		super();
	}

	public ClientConfig(I_ClientConfig config) {
		super();

		this.location = config.getLocation();

		this.visitDurationKPI = config.getVisitDurationKPI();
		this.visitDistanceKPI = config.getVisitDistanceKPI();
		this.canEditCustomerLocation = config.isCanEditCustomerLocation();

		this.modules = config.getModules();
		this.numberDayInventoryExpire = config.getNumberDayInventoryExpire();

		this.productivityUnit = config.getProductivityUnit();
		this.limitAccount = config.getLimitAccount();
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public long getVisitDurationKPI() {
		return visitDurationKPI;
	}

	public void setVisitDurationKPI(long visitDurationKPI) {
		this.visitDurationKPI = visitDurationKPI;
	}

	public double getVisitDistanceKPI() {
		return visitDistanceKPI;
	}

	public void setVisitDistanceKPI(double visitDistanceKPI) {
		this.visitDistanceKPI = visitDistanceKPI;
	}

	public boolean isCanEditCustomerLocation() {
		return canEditCustomerLocation;
	}

	public void setCanEditCustomerLocation(boolean canEditCustomerLocation) {
		this.canEditCustomerLocation = canEditCustomerLocation;
	}

	public Set<String> getModules() {
		return modules;
	}

	public void setModules(Set<String> modules) {
		this.modules = modules;
	}

	public int getNumberDayInventoryExpire() {
		return numberDayInventoryExpire;
	}

	public void setNumberDayInventoryExpire(int numberDayInventoryExpire) {
		this.numberDayInventoryExpire = numberDayInventoryExpire;
	}

	public String getProductivityUnit() {
		return productivityUnit;
	}

	public void setProductivityUnit(String productivityUnit) {
		this.productivityUnit = productivityUnit;
	}

	@Override
	public int getLimitAccount() {
		return limitAccount;
	}

	public void setLimitAccount(int limitAccount) {
		this.limitAccount = limitAccount;
	}

}
