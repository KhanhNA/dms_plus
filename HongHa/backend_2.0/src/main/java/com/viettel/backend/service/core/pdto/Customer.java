package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Schedule;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

public class Customer extends Category implements I_Customer {

	private static final long serialVersionUID = -5188339569449161376L;

	private String contact;
	private String mobile;
	private String phone;
	private String email;
	private I_CategoryEmbed customerType;
	private I_CategoryEmbed area;
	private Location location;
	private I_Schedule schedule;

	private int approveStatus;
	private I_UserEmbed createdBy;
	private SimpleDate createdTime;
	private I_UserEmbed approvedBy;
	private SimpleDate approvedTime;
	private SimpleDate lastModifiedTime;

	public Customer() {
		super();
	}

	public Customer(I_Customer customer) {
		super(customer);

		this.contact = customer.getContact();
		this.mobile = customer.getMobile();
		this.phone = customer.getPhone();
		this.email = customer.getEmail();

		if (customer.getCustomerType() != null) {
			this.customerType = customer.getCustomerType();
		}

		if (customer.getArea() != null) {
			this.area = customer.getArea();
		}

		this.location = customer.getLocation();

		this.approveStatus = customer.getApproveStatus();
		this.createdBy = customer.getCreatedBy();
		this.createdTime = customer.getCreatedTime();
		this.approvedBy = customer.getApprovedBy();
		this.approvedTime = customer.getApprovedTime();
		this.lastModifiedTime = customer.getLastModifiedTime();
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public I_CategoryEmbed getCustomerType() {
		return customerType;
	}

	public void setCustomerType(I_CategoryEmbed customerType) {
		this.customerType = customerType;
	}

	public I_CategoryEmbed getArea() {
		return area;
	}

	public void setArea(I_CategoryEmbed area) {
		this.area = area;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public I_Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(I_Schedule schedule) {
		this.schedule = schedule;
	}

	public int getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(int approveStatus) {
		this.approveStatus = approveStatus;
	}

	public I_UserEmbed getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(I_UserEmbed createdBy) {
		this.createdBy = createdBy;
	}

	public SimpleDate getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(SimpleDate createdTime) {
		this.createdTime = createdTime;
	}

	public I_UserEmbed getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(I_UserEmbed approvedBy) {
		this.approvedBy = approvedBy;
	}

	public SimpleDate getApprovedTime() {
		return approvedTime;
	}

	public void setApprovedTime(SimpleDate approvedTime) {
		this.approvedTime = approvedTime;
	}

	public SimpleDate getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(SimpleDate lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

}
