package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.customer.I_CustomerEmbed;

public class CustomerEmbed extends CategoryEmbed implements I_CustomerEmbed {

    private static final long serialVersionUID = -5971479327026500287L;
    
    public I_CategoryEmbed area;
    public I_CategoryEmbed customerType;
    
    public CustomerEmbed() {
        super();
    }
    
    public CustomerEmbed(I_CustomerEmbed customerEmbed) {
        super(customerEmbed);
        
        this.area = customerEmbed.getArea();
        this.customerType = customerEmbed.getCustomerType();
    }

    public I_CategoryEmbed getArea() {
        return area;
    }

    public void setArea(I_CategoryEmbed area) {
        this.area = area;
    }

    public I_CategoryEmbed getCustomerType() {
        return customerType;
    }

    public void setCustomerType(I_CategoryEmbed customerType) {
        this.customerType = customerType;
    }

}
