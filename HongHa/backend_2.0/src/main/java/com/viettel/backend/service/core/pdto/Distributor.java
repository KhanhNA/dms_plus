package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.common.I_DataOfRegion;

public class Distributor extends Category implements I_Distributor , I_DataOfRegion{

    private static final long serialVersionUID = 337158758699876976L;
    
    private UserEmbed supervisor;
    private CategoryEmbed region;
    
    public Distributor() {
        super();
    }
    
    public Distributor(I_Distributor distributor) {
        super(distributor);
        
        if (distributor.getSupervisor() != null) {
            this.supervisor = new UserEmbed(distributor.getSupervisor());
        }
        if(distributor.getRegion() !=null){
        	this.region = new CategoryEmbed(distributor.getRegion());
        }
    }

    public UserEmbed getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(UserEmbed supervisor) {
        this.supervisor = supervisor;
    }

	@Override
	public I_CategoryEmbed getRegion() {
		return region;
	}

	public void setRegion(CategoryEmbed region) {
		this.region = region;
	}
	
}
