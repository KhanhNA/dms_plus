package com.viettel.backend.service.core.pdto;

import java.math.BigDecimal;
import java.util.List;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.customer.I_CustomerEmbed;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderDetail;
import com.viettel.repository.common.domain.data.I_OrderPromotion;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.SimpleDate;

public class Order extends PO implements I_Order {

    private static final long serialVersionUID = 4782818770530638760L;

    public I_CategoryEmbed distributor;

    public I_CustomerEmbed customer;

    public I_UserEmbed createdBy;

    public SimpleDate createdTime;

    public int approveStatus;

    public I_UserEmbed approvedBy;

    public SimpleDate approvedTime;

    public boolean withVisit;

    public String code;

    public int deliveryType;

    public SimpleDate deliveryTime;

    public String comment;

    public BigDecimal subTotal;

    public BigDecimal promotionAmt;

    public BigDecimal discountPercentage;

    public BigDecimal discountAmt;

    public BigDecimal grandTotal;

    public BigDecimal quantity;

    public BigDecimal productivity;

    public boolean vanSales;

    public List<I_OrderDetail> details;

    public List<I_OrderPromotion> promotions;

    public Order() {
        super();
    }

    public Order(I_Order order) {
        super(order);

        this.distributor = order.getDistributor();
        this.customer = order.getCustomer();
        this.createdBy = order.getCreatedBy();
        this.createdTime = order.getCreatedTime();
        this.approveStatus = order.getApproveStatus();
        this.approvedBy = order.getApprovedBy();
        this.approvedTime = order.getApprovedTime();
        this.withVisit = order.isWithVisit();
        this.code = order.getCode();
        this.deliveryType = order.getDeliveryType();
        this.deliveryTime = order.getDeliveryTime();
        this.comment = order.getComment();
        this.subTotal = order.getSubTotal();
        this.promotionAmt = order.getPromotionAmt();
        this.discountPercentage = order.getDiscountPercentage();
        this.discountAmt = order.getDiscountAmt();
        this.grandTotal = order.getGrandTotal();
        this.quantity = order.getQuantity();
        this.productivity = order.getProductivity();
        this.vanSales = order.isVanSales();
        this.details = order.getDetails();
        this.promotions = order.getPromotions();
    }

    public I_CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(I_CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public I_CustomerEmbed getCustomer() {
        return customer;
    }

    public void setCustomer(I_CustomerEmbed customer) {
        this.customer = customer;
    }

    public I_UserEmbed getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(I_UserEmbed createdBy) {
        this.createdBy = createdBy;
    }

    public SimpleDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        this.createdTime = createdTime;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }

    public I_UserEmbed getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(I_UserEmbed approvedBy) {
        this.approvedBy = approvedBy;
    }

    public SimpleDate getApprovedTime() {
        return approvedTime;
    }

    public void setApprovedTime(SimpleDate approvedTime) {
        this.approvedTime = approvedTime;
    }

    public boolean isWithVisit() {
        return withVisit;
    }

    public void setWithVisit(boolean withVisit) {
        this.withVisit = withVisit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }

    public SimpleDate getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(SimpleDate deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getPromotionAmt() {
        return promotionAmt;
    }

    public void setPromotionAmt(BigDecimal promotionAmt) {
        this.promotionAmt = promotionAmt;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public int getNbSKU() {
        return this.getDetails() == null ? 0 : this.getDetails().size();
    }

    public boolean isVanSales() {
        return vanSales;
    }

    public void setVanSales(boolean vanSales) {
        this.vanSales = vanSales;
    }

    public List<I_OrderDetail> getDetails() {
        return details;
    }

    public void setDetails(List<I_OrderDetail> details) {
        this.details = details;
    }

    public List<I_OrderPromotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<I_OrderPromotion> promotions) {
        this.promotions = promotions;
    }

}
