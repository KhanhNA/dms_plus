package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.common.I_PO;

public class PO extends POEmbed implements I_PO {

    private static final long serialVersionUID = 3963450445768937857L;

    private String clientId;
    private boolean draft;
    private boolean active;

    public PO() {
        super();
    }

    public PO(I_PO po) {
        super(po);

        this.clientId = po.getClientId();
        this.draft = po.isDraft();
        this.active = po.isActive();
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
