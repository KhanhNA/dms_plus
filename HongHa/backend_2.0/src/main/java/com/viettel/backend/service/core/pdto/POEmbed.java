package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.common.I_POEmbed;

public class POEmbed implements I_POEmbed {

    private static final long serialVersionUID = 3963450445768937857L;
    
    private String id;

    public POEmbed() {
        super();
    }

    public POEmbed(I_POEmbed po) {
        super();
        
        this.id = po.getId();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
