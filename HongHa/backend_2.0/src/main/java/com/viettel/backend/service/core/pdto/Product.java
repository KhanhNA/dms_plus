package com.viettel.backend.service.core.pdto;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.product.I_Product;

public class Product extends Category implements I_Product {

    private static final long serialVersionUID = -4694292220947870518L;

    private I_CategoryEmbed productCategory;
    private I_CategoryEmbed uom;
    private BigDecimal price;
    private BigDecimal productivity;
    private String photo;
    private String description;
    
    public Product() {
        super();
    }
    
    public Product(I_Product product) {
        super(product);
        
        this.productCategory = product.getProductCategory();
        this.uom = product.getUom();
        
        this.price = product.getPrice();
        this.productivity = product.getProductivity();
        this.photo = product.getPhoto();
        this.description = product.getDescription();
    }

    public I_CategoryEmbed getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(I_CategoryEmbed productCategory) {
        this.productCategory = productCategory;
    }

    public I_CategoryEmbed getUom() {
        return uom;
    }

    public void setUom(I_CategoryEmbed uom) {
        this.uom = uom;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
