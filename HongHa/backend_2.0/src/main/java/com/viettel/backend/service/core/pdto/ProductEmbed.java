package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.product.I_ProductEmbed;

public class ProductEmbed extends CategoryEmbed implements I_ProductEmbed {

    private static final long serialVersionUID = -4694292220947870518L;

    private CategoryEmbed productCategory;
    private CategoryEmbed uom;
    
    public ProductEmbed() {
        super();
    }
    
    public ProductEmbed(I_ProductEmbed product) {
        super(product);
        
        if (product.getProductCategory() != null) {
            this.productCategory = new CategoryEmbed(product.getProductCategory());
        }
        
        if (product.getUom() != null) {
            this.uom = new CategoryEmbed(product.getUom());
        }
    }

    public CategoryEmbed getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(CategoryEmbed productCategory) {
        this.productCategory = productCategory;
    }

    public CategoryEmbed getUom() {
        return uom;
    }

    public void setUom(CategoryEmbed uom) {
        this.uom = uom;
    }

}
