package com.viettel.backend.service.core.pdto;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.product.I_ProductEmbed;
import com.viettel.repository.common.domain.product.I_ProductQuantity;

public class ProductQuantity extends ProductEmbed implements I_ProductQuantity {

    private static final long serialVersionUID = -4694292220947870518L;

    private BigDecimal quantity;
    
    public ProductQuantity() {
        super();
    }
    
    public ProductQuantity(I_ProductEmbed product) {
        super(product);
    }
    
    public ProductQuantity(I_ProductQuantity product) {
        super(product);
        
        this.quantity = product.getQuantity();
    }

    public BigDecimal getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

}
