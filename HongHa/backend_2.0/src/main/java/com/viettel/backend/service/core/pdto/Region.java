package com.viettel.backend.service.core.pdto;

import java.util.List;
import java.util.Set;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;

public class Region extends Category implements I_Region {

	private static final long serialVersionUID = 3531321852420398883L;

	private I_CategoryEmbed parent;
	private List<I_CategoryEmbed> children;
	private Set<I_CategoryEmbed> distributors;
	private boolean root;
	private boolean leaf;
	
	public Region() {
		super();
	}

	public Region(I_Region region) {
		super(region);
		root = region.isRoot();
		leaf = region.isLeaf();
	}

	@Override
	public I_CategoryEmbed getParent() {
		return parent;
	}

	@Override
	public List<I_CategoryEmbed> getChildren() {
		return children;
	}

	@Override
	public boolean isRoot() {
		return root;
	}

	@Override
	public boolean isLeaf() {
		return leaf;
	}

	public void setChildren(List<I_CategoryEmbed> children) {
		this.children = children;
	}

	public void setParent(I_CategoryEmbed parent) {
		this.parent = parent;
	}

	public void setRoot(boolean root) {
		this.root = root;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	@Override
	public Set<I_CategoryEmbed> getDistributors() {
		return distributors;
	}
	
	public void setDistributors(Set<I_CategoryEmbed> distributors) {
		this.distributors = distributors;
	}

}
