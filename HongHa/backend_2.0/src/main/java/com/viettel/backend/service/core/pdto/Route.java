package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.customer.I_Route;

public class Route extends Category implements I_Route {

    private static final long serialVersionUID = 3921293706373309273L;
    
    private UserEmbed salesman;
    
    public Route() {
        super();
    }
    
    public Route(I_Route route) {
        super(route);
        
        if (route.getSalesman() != null) {
            this.salesman = new UserEmbed(route.getSalesman());
        }
    }

    public UserEmbed getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbed salesman) {
        this.salesman = salesman;
    }

}
