package com.viettel.backend.service.core.pdto;

import java.io.Serializable;

import com.viettel.repository.common.domain.customer.I_Schedule;
import com.viettel.repository.common.domain.customer.I_ScheduleItem;

public class Schedule implements Serializable, I_Schedule {

    private static final long serialVersionUID = -5829759422637671225L;

    private String routeId;
    private I_ScheduleItem item;

    public Schedule() {
        super();
    }

    public Schedule(I_Schedule schedule) {
        super();

        this.routeId = schedule.getRouteId();
        this.item = schedule.getItem();
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public I_ScheduleItem getItem() {
        return item;
    }

    public void setItem(I_ScheduleItem item) {
        this.item = item;
    }

}
