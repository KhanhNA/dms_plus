package com.viettel.backend.service.core.pdto;

import java.io.Serializable;
import java.util.Set;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_SurveyAnswerContent;

public class SurveyAnswerContent implements Serializable, I_SurveyAnswerContent {

    private static final long serialVersionUID = -2372398576104412020L;
    
    public I_CategoryEmbed survey;
    public Set<Integer> options;

    public I_CategoryEmbed getSurvey() {
        return survey;
    }

    public void setSurvey(I_CategoryEmbed survey) {
        this.survey = survey;
    }

    public Set<Integer> getOptions() {
        return options;
    }

    public void setOptions(Set<Integer> options) {
        this.options = options;
    }

}
