package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.category.I_SystemConfig;
import com.viettel.repository.common.entity.Location;

public class SystemConfig implements I_SystemConfig {

    private static final long serialVersionUID = 5202363354970843896L;
    
    // DEFAULT
    private String dateFormat;
    private String productPhoto;
    private Location location;

    // CALENDAR
    private int firstDayOfWeek;
    private int minimalDaysInFirstWeek;

    // SCHEDULE
    private int numberWeekOfFrequency;

    // NUMBER DAY ORDER PENDING EXPIRE
    private int numberDayOrderPendingExpire;
    // ORDER DATE TYPE
    private OrderDateType orderDateType;
    
    public SystemConfig() {
        super();
    }
    
    public SystemConfig(I_SystemConfig config) {
        super();
        
        this.dateFormat = config.getDateFormat();
        this.productPhoto = config.getProductPhoto();
        this.location = config.getLocation();
        
        this.firstDayOfWeek = config.getFirstDayOfWeek();
        this.minimalDaysInFirstWeek = config.getMinimalDaysInFirstWeek();
        
        this.numberWeekOfFrequency = config.getNumberWeekOfFrequency();
        
        this.numberDayOrderPendingExpire = config.getNumberDayOrderPendingExpire();
        this.orderDateType = config.getOrderDateType();
    }
    

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getProductPhoto() {
        return productPhoto;
    }

    public void setProductPhoto(String productPhoto) {
        this.productPhoto = productPhoto;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(int firstDayOfWeek) {
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public int getMinimalDaysInFirstWeek() {
        return minimalDaysInFirstWeek;
    }

    public void setMinimalDaysInFirstWeek(int minimalDaysInFirstWeek) {
        this.minimalDaysInFirstWeek = minimalDaysInFirstWeek;
    }

    public int getNumberWeekOfFrequency() {
        return numberWeekOfFrequency;
    }

    public void setNumberWeekOfFrequency(int numberWeekOfFrequency) {
        this.numberWeekOfFrequency = numberWeekOfFrequency;
    }

    public int getNumberDayOrderPendingExpire() {
        return numberDayOrderPendingExpire;
    }

    public void setNumberDayOrderPendingExpire(int numberDayOrderPendingExpire) {
        this.numberDayOrderPendingExpire = numberDayOrderPendingExpire;
    }

    public OrderDateType getOrderDateType() {
        return orderDateType;
    }

    public void setOrderDateType(OrderDateType orderDateType) {
        this.orderDateType = orderDateType;
    }

}
