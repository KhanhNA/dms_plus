package com.viettel.backend.service.core.pdto;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.category.I_Target;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.user.I_UserEmbed;

public class Target extends PO implements I_Target {

    private static final long serialVersionUID = -2468026471821196643L;

    private I_CategoryEmbed distributor;
    private I_UserEmbed salesman;
    private int month;
    private int year;
    private BigDecimal revenue;
    private BigDecimal subRevenue;
    private BigDecimal quantity;
    private BigDecimal productivity;
    private int nbOrder;
    private BigDecimal revenueByOrder;
    private BigDecimal skuByOrder;
    private BigDecimal productivityByOrder;
    private int newCustomer;

    public Target() {
        super();
    }

    public Target(I_Target target) {
        super(target);

        this.distributor = target.getDistributor();
        this.salesman = target.getSalesman();
        this.month = target.getMonth();
        this.year = target.getYear();
        this.revenue = target.getRevenue();
        this.subRevenue = target.getSubRevenue();
        this.quantity = target.getQuantity();
        this.productivity = target.getProductivity();
        this.nbOrder = target.getNbOrder();
        this.revenueByOrder = target.getRevenueByOrder();
        this.skuByOrder = target.getSkuByOrder();
        this.productivityByOrder = target.getProductivityByOrder();
        this.newCustomer = target.getNewCustomer();
    }

    public I_CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(I_CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public I_UserEmbed getSalesman() {
        return salesman;
    }

    public void setSalesman(I_UserEmbed salesman) {
        this.salesman = salesman;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {
        this.revenue = revenue;
    }

    public BigDecimal getSubRevenue() {
        return subRevenue;
    }

    public void setSubRevenue(BigDecimal subRevenue) {
        this.subRevenue = subRevenue;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public int getNbOrder() {
        return nbOrder;
    }

    public void setNbOrder(int nbOrder) {
        this.nbOrder = nbOrder;
    }

    public BigDecimal getRevenueByOrder() {
        return revenueByOrder;
    }

    public void setRevenueByOrder(BigDecimal revenueByOrder) {
        this.revenueByOrder = revenueByOrder;
    }

    public BigDecimal getSkuByOrder() {
        return skuByOrder;
    }

    public void setSkuByOrder(BigDecimal skuByOrder) {
        this.skuByOrder = skuByOrder;
    }

    public BigDecimal getProductivityByOrder() {
        return productivityByOrder;
    }

    public void setProductivityByOrder(BigDecimal productivityByOrder) {
        this.productivityByOrder = productivityByOrder;
    }

    public int getNewCustomer() {
        return newCustomer;
    }

    public void setNewCustomer(int newCustomer) {
        this.newCustomer = newCustomer;
    }

}
