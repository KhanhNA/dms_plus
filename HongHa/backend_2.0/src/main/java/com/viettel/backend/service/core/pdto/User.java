package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.user.I_User;

public class User extends PO implements I_User {

    private static final long serialVersionUID = -7801805872486069690L;
    
    private String username;
    private String fullname;
    private boolean isDefaultAdmin;
    private I_CategoryEmbed distributor;
    private String usernameFull;
    private String password;
    private String role;
    private boolean vanSales;
    
    public User() {
        super();
    }
    
    public User(I_User user) {
        super(user);
        
        this.username = user.getUsername();
        this.fullname = user.getFullname();
        this.isDefaultAdmin = user.isDefaultAdmin();
        this.distributor = user.getDistributor();
        this.usernameFull = user.getUsernameFull();
        this.password = user.getPassword();
        this.role = user.getRole();
        this.vanSales = user.isVanSales();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String clientCode, String username) {
        this.username = username.toLowerCase();
        this.usernameFull = clientCode.toLowerCase() + "_" + username.toLowerCase();
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isDefaultAdmin() {
        return this.isDefaultAdmin;
    }
    
    public void setDefaultAdmin(boolean isDefaultAdmin) {
        this.isDefaultAdmin = isDefaultAdmin;
    }
    
    public I_CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(I_CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public String getUsernameFull() {
        return usernameFull;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isVanSales() {
        return vanSales;
    }

    public void setVanSales(boolean vanSales) {
        this.vanSales = vanSales;
    }

}
