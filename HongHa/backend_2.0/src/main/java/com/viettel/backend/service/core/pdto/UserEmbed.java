package com.viettel.backend.service.core.pdto;

import com.viettel.repository.common.domain.user.I_UserEmbed;

public class UserEmbed extends POEmbed implements I_UserEmbed {

    private static final long serialVersionUID = -7526848843745899814L;
    
    private String username;
    private String fullname;
    
    public UserEmbed() {
        super();
    }
    
    public UserEmbed(I_UserEmbed userEmbed) {
        super(userEmbed);
        this.username = userEmbed.getUsername();
        this.fullname = userEmbed.getFullname();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

}
