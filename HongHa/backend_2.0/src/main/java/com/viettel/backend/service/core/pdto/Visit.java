package com.viettel.backend.service.core.pdto;

import java.util.List;

import com.viettel.repository.common.domain.data.I_SurveyAnswerContent;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

public class Visit extends Order implements I_Visit {

    private static final long serialVersionUID = 7248716614988497636L;
    
    private SimpleDate endTime;
    private int visitStatus;
    private boolean closed;
    private String photo;
    private Location location;
    private Location customerLocation;
    private int locationStatus;
    private Double distance;
    private long duration;
    private boolean errorDuration;

    private boolean readed;
    private List<String> feedbacks;

    private List<I_SurveyAnswerContent> surveyAnswers;

    public Visit() {
        super();
    }

    public Visit(I_Visit visit) {
        super(visit);

        this.endTime = visit.getEndTime();
        this.visitStatus = visit.getVisitStatus();
        this.closed = visit.isClosed();
        this.photo = visit.getPhoto();
        this.location = visit.getLocation();
        this.customerLocation = visit.getCustomerLocation();
        this.locationStatus = visit.getLocationStatus();
        this.distance = visit.getDistance();
        this.duration = visit.getDuration();
        this.errorDuration = visit.isErrorDuration();
        this.readed = visit.isReaded();
        this.feedbacks = visit.getFeedbacks();
        this.surveyAnswers = visit.getSurveyAnswers();
    }

    public void setStartTime(SimpleDate startTime) {
        this.setCreatedTime(startTime);
    }

    public SimpleDate getEndTime() {
        return endTime;
    }

    public void setEndTime(SimpleDate endTime) {
        this.endTime = endTime;
    }

    public void setSalesman(I_UserEmbed salesman) {
        this.setCreatedBy(salesman);
    }

    public boolean isHasOrder() {
        return this.getDetails() != null && !this.getDetails().isEmpty();
    }

    public int getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(int visitStatus) {
        this.visitStatus = visitStatus;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getCustomerLocation() {
        return customerLocation;
    }

    public void setCustomerLocation(Location customerLocation) {
        this.customerLocation = customerLocation;
    }

    public int getLocationStatus() {
        return locationStatus;
    }

    public void setLocationStatus(int locationStatus) {
        this.locationStatus = locationStatus;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isErrorDuration() {
        return errorDuration;
    }

    public void setErrorDuration(boolean errorDuration) {
        this.errorDuration = errorDuration;
    }

    public boolean isReaded() {
        return readed;
    }

    public void setReaded(boolean readed) {
        this.readed = readed;
    }

    public List<String> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<String> feedbacks) {
        this.feedbacks = feedbacks;
    }

    @Override
    public String getFirstMessage() {
        if (feedbacks != null && !feedbacks.isEmpty()) {
            return feedbacks.get(0);
        } else {
            return null;
        }
    }

    public List<I_SurveyAnswerContent> getSurveyAnswers() {
        return surveyAnswers;
    }

    public void setSurveyAnswers(List<I_SurveyAnswerContent> surveyAnswers) {
        this.surveyAnswers = surveyAnswers;
    }

}
