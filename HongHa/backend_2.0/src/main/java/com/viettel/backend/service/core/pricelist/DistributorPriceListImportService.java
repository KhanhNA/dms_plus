package com.viettel.backend.service.core.pricelist;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface DistributorPriceListImportService {

    public byte[] getTemplate(UserLogin userLogin, String _distributorId, String lang);
    
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId);
    
    public ImportResultDto doImport(UserLogin userLogin, String _distributorId, String fileId);

}
