package com.viettel.backend.service.core.pricelist.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.dto.common.ImportConfirmDto.RowData;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractImportService;
import com.viettel.backend.service.core.pricelist.DistributorPriceListImportService;
import com.viettel.repository.common.DistributorPriceListRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.DISTRIBUTOR_ADMIN })
@Service
public class DistributorPriceListImportServiceImpl extends AbstractImportService implements DistributorPriceListImportService {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DistributorPriceListRepository distributorPriceListRepository;

    @Override
    public byte[] getTemplate(UserLogin userLogin, String _distributorId, String lang) {
        lang = getLang(lang);
        
        I_Distributor distributor = getDefaultDistributor(userLogin);
        if (distributor == null) {
            distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
            BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");
        }
                
        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);

        Map<String, BigDecimal> priceList = getDistributorPriceList(userLogin);
        
        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet scheduleSheet = workbook.createSheet(translate(lang, "product.price.list"));

        XSSFRow row = scheduleSheet.createRow(0);
        row.createCell(0).setCellValue(translate(lang, "code"));
        row.createCell(1).setCellValue(translate(lang, "name"));
        row.createCell(2).setCellValue(translate(lang, "product.category"));
        row.createCell(3).setCellValue(translate(lang, "uom"));
        row.createCell(4).setCellValue(translate(lang, "company.price"));
        row.createCell(5).setCellValue(translate(lang, "distributor.price"));

        Collections.sort(products, new Comparator<I_Product>() {

            @Override
            public int compare(I_Product p1, I_Product p2) {
                if (p1.getProductCategory().getId().equals(p2.getProductCategory().getId())) {
                    return p1.getName().compareTo(p2.getName());
                } else {
                    return p1.getProductCategory().getName().compareTo(p2.getProductCategory().getName());
                }
            }
            
        });
        
        int index = 1;
        for (I_Product product : products) {
            row = scheduleSheet.createRow(index);

            row.createCell(0).setCellValue(product.getCode());
            row.createCell(1).setCellValue(product.getName());
            row.createCell(2).setCellValue(product.getProductCategory().getName());
            row.createCell(3).setCellValue(product.getUom().getName());
            row.createCell(4).setCellValue(product.getPrice().doubleValue());
            BigDecimal distributorPrice = priceList.get(product.getId());
            if (distributorPrice == null) {
                row.createCell(5).setCellValue("");
            } else {
                row.createCell(5).setCellValue(distributorPrice.doubleValue());
            }
            
            index++;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);

            return baos.toByteArray();
        } catch (IOException e) {
            logger.error("error write file", e);
            throw new UnsupportedOperationException(e);
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }

    private I_CellValidator[] getValidators(UserLogin userLogin) {
        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
        Map<String, I_Product> productByCode = new HashMap<String, I_Product>();
        for (I_Product product : products) {
            productByCode.put(product.getCode().trim().toUpperCase(), product);
        }

        LinkedList<I_CellValidator> validators = new LinkedList<AbstractImportService.I_CellValidator>();
        validators.add(new ReferenceCellValidator<I_Product>(productByCode));
        validators.add(new NoValidator());
        validators.add(new NoValidator());
        validators.add(new NoValidator());
        validators.add(new NoValidator());
        validators.add(new I_CellValidator() {
            
            @Override
            public CheckResult check(XSSFCell cell, RowData rowData, int index) {
                BigDecimal value = getNumericCellValue(cell, 0);

                if (value == null) {
                    return new CheckResult(true);
                }
                
                if (value.compareTo(BigDecimal.ZERO) < 0) {
                    return new CheckResult(value, value.toString(), false);
                }
                
                if (value.compareTo(new BigDecimal(1000000000)) > 0) {
                    return new CheckResult(value, value.toString(), false);
                }
                
                if (value.scale() > 0) {
                    return new CheckResult(value, value.toString(), false);
                }

                return new CheckResult(value, value.toString(), true);
            }
            
        });

        I_CellValidator[] array = new I_CellValidator[validators.size()];
        validators.toArray(array);
        return array;
    }

    @Override
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId) {
        I_CellValidator[] validators = getValidators(userLogin);
        return getErrorRows(userLogin, fileId, validators);
    }

    @Override
    public ImportResultDto doImport(UserLogin userLogin, String _distributorId, String fileId) {
        I_Distributor distributor = getDefaultDistributor(userLogin);
        if (distributor == null) {
            distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
            BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");
        }
        
        I_CellValidator[] validators = getValidators(userLogin);

        ImportConfirmDto dto = getValidRows(userLogin, fileId, validators);

        Map<String, BigDecimal> priceList = new HashMap<>();
        
        for (RowData row : dto.getRowDatas()) {
            I_Product product = (I_Product) row.getDatas().get(0);
            BigDecimal distributorPrice = (BigDecimal) row.getDatas().get(5);
            if (distributorPrice != null) {
                priceList.put(product.getId(), distributorPrice);
            }
        }

        distributorPriceListRepository.savePriceList(userLogin.getClientId(), distributor.getId(), priceList);
        
        return new ImportResultDto(dto.getTotal(), dto.getRowDatas().size());
    }

}
