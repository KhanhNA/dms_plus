package com.viettel.backend.service.core.pricelist.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.pricelist.DistributorPriceDto;
import com.viettel.backend.dto.pricelist.DistributorPriceListCreateDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pricelist.DistributorPriceListService;
import com.viettel.repository.common.DistributorPriceListRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.DISTRIBUTOR_ADMIN })
@Service
public class DistributorPriceListServiceImpl extends AbstractService implements DistributorPriceListService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DistributorPriceListRepository distributorPriceListRepository;

    @Override
    public ListDto<DistributorPriceDto> getPriceList(UserLogin userLogin) {
        I_Distributor distributor = getDefaultDistributor(userLogin);
        Map<String, BigDecimal> distributorPriceList = distributorPriceListRepository
                .getPriceList(userLogin.getClientId(), distributor.getId());

        I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);

        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
        List<DistributorPriceDto> dtos = new ArrayList<>(products.size());
        for (I_Product product : products) {
            dtos.add(new DistributorPriceDto(product, productPhotoFactory, distributorPriceList.get(product.getId())));
        }

        return new ListDto<>(dtos);
    }

    @Override
    public void savePriceList(UserLogin userLogin, DistributorPriceListCreateDto createDto) {
        I_Distributor distributor = getDefaultDistributor(userLogin);

        BusinessAssert.notNull(createDto);

        Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);

        Map<String, BigDecimal> _priceList = createDto.getPriceList();
        Map<String, BigDecimal> priceList = new HashMap<>();

        for (Entry<String, BigDecimal> entry : _priceList.entrySet()) {
            if (entry.getValue() != null) {
                BusinessAssert.isTrue(entry.getValue().compareTo(BigDecimal.ZERO) >= 0);
                
                String productId = entry.getKey();
                BusinessAssert.isTrue(productMap.containsKey(productId));

                priceList.put(productId, entry.getValue());
            }
        }

        distributorPriceListRepository.savePriceList(userLogin.getClientId(), distributor.getId(), priceList);
    }

}
