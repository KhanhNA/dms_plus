package com.viettel.backend.service.core.product;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.service.core.common.CategoryReadonlyService;

public interface ProductCategoryReadonlyService extends CategoryReadonlyService<CategorySimpleDto> {

}
