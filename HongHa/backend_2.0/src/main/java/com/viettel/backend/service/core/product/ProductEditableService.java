package com.viettel.backend.service.core.product;

import com.viettel.backend.dto.product.ProductCreateDto;
import com.viettel.backend.dto.product.ProductDto;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface ProductEditableService extends CategoryEditableService<ProductDto, ProductDto, ProductCreateDto> {

}
