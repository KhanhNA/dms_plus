package com.viettel.backend.service.core.product;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.dto.product.ImportProductPhotoDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface ProductImportService {

    public byte[] getImportProductTemplate(UserLogin userLogin, String lang);

    public ImportConfirmDto verify(UserLogin userLogin, String fileId);
    
    public ImportConfirmDto confirm(UserLogin userLogin, String fileId);

    public ImportResultDto importProduct(UserLogin userLogin, String fileId, ImportProductPhotoDto photoDto);

}
