package com.viettel.backend.service.core.product;

import java.io.InputStream;
import java.util.List;

import com.viettel.backend.dto.product.ProductImportantCreateDto;
import com.viettel.backend.dto.product.ProductImportantDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface ProductImportantService {
    
    public void saveProductImportant(UserLogin userLogin, ProductImportantCreateDto createDto);

	public List<ProductImportantDto> getProductImportant(UserLogin userLogin);
	
	public List<ProductImportantDto> getProductImportantByCategory(UserLogin userLogin, String categoryId);
 
	public InputStream exportProductImportant(UserLogin userLogin, String categoryId, String lang);
}
