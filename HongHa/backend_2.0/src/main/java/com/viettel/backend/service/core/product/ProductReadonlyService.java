package com.viettel.backend.service.core.product;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.product.ProductDto;
import com.viettel.backend.dto.product.ProductSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface ProductReadonlyService extends CategoryReadonlyService<ProductSimpleDto> {

    public ListDto<ProductDto> getList(UserLogin userLogin, String search, String _distributorId,
            PageSizeRequest pageSizeRequest);

}
