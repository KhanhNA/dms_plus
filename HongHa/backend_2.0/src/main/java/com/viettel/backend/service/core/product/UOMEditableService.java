package com.viettel.backend.service.core.product;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface UOMEditableService extends CategoryEditableService<CategoryDto, CategoryDto, CategoryCreateDto> {

}
