package com.viettel.backend.service.core.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.CategoryEditableServiceImpl;
import com.viettel.backend.service.core.product.ProductCategoryEditableService;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value={ Role.ADMIN })
@Service
public class ProductCategoryEditableServiceImpl extends
        CategoryEditableServiceImpl<I_Category, CategoryDto, CategoryDto, CategoryCreateDto> implements
        ProductCategoryEditableService {

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    protected CategoryBasicRepository<I_Category> getRepository() {
        return productCategoryRepository;
    }

    @Override
    protected void beforeSetActive(UserLogin userLogin, I_Category domain, boolean active) {
        if (active) {
            // ACTIVE

        } else {
            // DEACTIVE
            BusinessAssert.notTrue(productRepository.checkProductCategoryUsed(userLogin.getClientId(), domain.getId()),
                    BusinessExceptionCode.RECORD_USED_IN_PRODUCT, "uom used in product");
        }
    }

    @Override
    public CategoryDto createListSimpleDto(UserLogin userLogin, I_Category domain) {
        return new CategoryDto(domain);
    }

    @Override
    public CategoryDto createListDetailDto(UserLogin userLogin, I_Category domain) {
        return new CategoryDto(domain);
    }

    @Override
    public I_Category getObjectForCreate(UserLogin userLogin, CategoryCreateDto createdto) {
        return _getObjectForCreate(userLogin, createdto);
    }

    @Override
    public I_Category getObjectForUpdate(UserLogin userLogin, I_Category oldDomain, CategoryCreateDto createdto) {
        return _getObjectForUpdate(userLogin, oldDomain, createdto);
    }

}
