package com.viettel.backend.service.core.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.product.ProductCreateDto;
import com.viettel.backend.dto.product.ProductDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.CategoryEditableServiceImpl;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.Product;
import com.viettel.backend.service.core.product.ProductEditableService;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.UOMRepository;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.ADMIN })
@Service
public class ProductEditableServiceImpl
        extends CategoryEditableServiceImpl<I_Product, ProductDto, ProductDto, ProductCreateDto>
        implements ProductEditableService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private UOMRepository uomRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Override
    protected boolean isUseCode() {
        return true;
    }

    @Override
    protected CategoryBasicRepository<I_Product> getRepository() {
        return productRepository;
    }

    @Override
    protected void beforeSetActive(UserLogin userLogin, I_Product domain, boolean active) {
        if (active) {
            // ACTIVE
            if (domain.getUom() != null) {
                BusinessAssert.isTrue(
                        uomRepository.exists(userLogin.getClientId(), false, true, domain.getUom().getId()),
                        BusinessExceptionCode.UOM_NOT_FOUND, "uom not found");
            }

            if (domain.getProductCategory() != null) {
                BusinessAssert.isTrue(
                        productCategoryRepository.exists(userLogin.getClientId(), false, true,
                                domain.getProductCategory().getId()),
                        BusinessExceptionCode.PRODUCT_CATEGORY_NOT_FOUND, "product category not found");
            }

        } else {
            // DEACTIVE
        }
    }

    @Override
    public ProductDto createListSimpleDto(UserLogin userLogin, I_Product domain) {
        return createListDetailDto(userLogin, domain);
    }

    @Override
    public ProductDto createListDetailDto(final UserLogin userLogin, final I_Product domain) {
        return new ProductDto(domain, new I_ProductPhotoFactory() {

            @Override
            public String getPhoto(String productId) {
                if (StringUtils.isEmpty(domain.getPhoto())) {
                    return getConfig(userLogin).getProductPhoto();
                }
                return domain.getPhoto();
            }
        }, null);
    }

    @Override
    public I_Product getObjectForCreate(UserLogin userLogin, ProductCreateDto createdto) {
        BusinessAssert.notNull(createdto);

        Product product = new Product();
        initiatePO(userLogin.getClientId(), product);
        product.setDraft(true);

        return getObjectForUpdate(userLogin, product, createdto);
    }

    @Override
    public I_Product getObjectForUpdate(UserLogin userLogin, I_Product oldDomain, ProductCreateDto createdto) {
        BusinessAssert.notNull(createdto);
        BusinessAssert.notNull(createdto.getPhoto());
        BusinessAssert.notNull(createdto.getPrice());
        BusinessAssert.notNull(createdto.getProductivity());

        Product product = new Product(oldDomain);
        product = updateCategoryFieldFromDto(userLogin, product, createdto);

        if (product.isDraft()) {
            BusinessAssert.notNull(createdto.getUomId());
            BusinessAssert.notNull(createdto.getProductCategoryId());

            I_Category uom = getMandatoryPO(userLogin, createdto.getUomId(), uomRepository);
            product.setUom(new CategoryEmbed(uom));

            I_Category productCategory = getMandatoryPO(userLogin, createdto.getProductCategoryId(),
                    productCategoryRepository);
            product.setProductCategory(new CategoryEmbed(productCategory));
        }

        product.setPrice(createdto.getPrice());
        product.setProductivity(createdto.getProductivity());
        product.setDescription(createdto.getDescription());

        BusinessAssert.isTrue(fileRepository.exists(userLogin.getClientId(), createdto.getPhoto()), "Photo not exist");
        product.setPhoto(createdto.getPhoto());

        return product;
    }

}
