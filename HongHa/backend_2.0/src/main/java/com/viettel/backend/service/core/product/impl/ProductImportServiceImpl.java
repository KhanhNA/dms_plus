package com.viettel.backend.service.core.product.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportConfirmDto.RowData;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.dto.product.ImportProductPhotoDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractImportService;
import com.viettel.backend.service.core.pdto.Product;
import com.viettel.backend.service.core.product.ProductImportService;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.UOMRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.ADMIN })
@Service
public class ProductImportServiceImpl extends AbstractImportService implements ProductImportService {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private UOMRepository uomRepository;

    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private ConfigRepository configRepository;
    
    // PUBLIC
    @Override
    public byte[] getImportProductTemplate(UserLogin userLogin, String lang) {
        lang = getLang(lang);
        
        I_Config config = configRepository.getConfig(userLogin.getClientId());
        
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet productSheet = workbook.createSheet(translate(lang, "product"));
        XSSFRow row = productSheet.createRow(0);
        row.createCell(0).setCellValue(translate(lang, "code") + "*");
        row.createCell(1).setCellValue(translate(lang, "name") + "*");
        row.createCell(2).setCellValue(translate(lang, "product.category") + "*");
        row.createCell(3).setCellValue(translate(lang, "uom") + "*");
        row.createCell(4).setCellValue(translate(lang, "price") + "*");
        row.createCell(5).setCellValue(translate(lang, "productitvity") + " (" + config.getProductivityUnit() + ")");
        row.createCell(6).setCellValue(translate(lang, "description"));

        // PRODUCT CATEGORY
        List<I_Category> productCategories = productCategoryRepository.getAll(userLogin.getClientId(), null);
        int index = 0;
        XSSFSheet productCategorySheet = workbook.createSheet(translate(lang, "product.category"));
        for (I_Category productCategory : productCategories) {
            row = productCategorySheet.createRow(index);
            row.createCell(0).setCellValue(productCategory.getName());
            index++;
        }
        
        // UOM
        List<I_Category> uoms = uomRepository.getAll(userLogin.getClientId(), null);
        index = 0;
        XSSFSheet uomSheet = workbook.createSheet(translate(lang, "uom"));
        for (I_Category uom : uoms) {
            row = uomSheet.createRow(index);
            row.createCell(0).setCellValue(uom.getName());
            index++;
        }
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);

            return baos.toByteArray();
        } catch (IOException e) {
            logger.error("error write file", e);
            throw new UnsupportedOperationException(e);
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }
    
    private I_CellValidator[] getValidators(UserLogin userLogin) {
        Map<String, I_Category> productCategoryMap = getProductCategory(userLogin.getClientId());
        Map<String, I_Category> uomMap = getUom(userLogin.getClientId());

        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
        HashSet<String> productNames = new HashSet<String>();
        HashSet<String> productCodes = new HashSet<String>();
        for (I_Product product : products) {
            if (!StringUtils.isNullOrEmpty(product.getName(), true)) {
                productNames.add(product.getName().trim().toUpperCase());
            }

            if (!StringUtils.isNullOrEmpty(product.getCode(), true)) {
                productCodes.add(product.getCode().trim().toUpperCase());
            }
        }
        
        I_CellValidator productivityCellValidator = new I_CellValidator() {
            
            @Override
            public CheckResult check(XSSFCell cell, RowData rowData, int index) {
                BigDecimal value = getNumericCellValue(cell, 2);

                if (value == null) {
                    return new CheckResult(true);
                }
                
                CheckResult result = new CheckResult(value, value.toString(), true);
                
                if (value.compareTo(BigDecimal.ZERO) <= 0) {
                    result.setValid(false);
                }
                
                if (value.compareTo(new BigDecimal(1000000000)) > 0) {
                    result.setValid(false);
                }
                
                if (value.stripTrailingZeros().scale() > 2) {
                    result.setValid(false);
                }

                return result;
            }
        };
        

        return new I_CellValidator[] {
                new MultiCellValidator(new StringMandatoryCellValidator(), new StringUniqueCellValidator(productCodes),
                        new MaxLengthCellValidator(30), new RegexCellValidator("^\\S+$")),
                new MultiCellValidator(new StringMandatoryCellValidator(), new StringUniqueCellValidator(productNames),
                        new MaxLengthCellValidator(50)),
                new MultiCellValidator(new StringMandatoryCellValidator(), new ReferenceCellValidator<I_Category>(
                        productCategoryMap)),
                new MultiCellValidator(new StringMandatoryCellValidator(), new ReferenceCellValidator<I_Category>(uomMap)),
                new NumberMinMaxCellValidator(BigDecimal.ONE, new BigDecimal(1000000000), 0), productivityCellValidator, null };
    }

    @Override
    public ImportConfirmDto verify(UserLogin userLogin, String fileId) {
        I_CellValidator[] validators = getValidators(userLogin);
        return getErrorRows(userLogin, fileId, validators);
    }

    @Override
    public ImportConfirmDto confirm(UserLogin userLogin, String fileId) {
        I_CellValidator[] validators = getValidators(userLogin);
        return getValidRows(userLogin, fileId, validators);
    }

    @Override
    public ImportResultDto importProduct(UserLogin userLogin, String fileId, ImportProductPhotoDto photoDto) {
        I_CellValidator[] validators = getValidators(userLogin);
        ImportConfirmDto dto = getValidRows(userLogin, fileId, validators);

        BusinessAssert.notNull(photoDto.getPhotos());
        BusinessAssert.equals(photoDto.getPhotos().length, dto.getRowDatas().size());
        
        List<I_Product> productToInsert = new ArrayList<I_Product>(dto.getRowDatas().size());
        int index = 0;
        for (RowData row : dto.getRowDatas()) {
            String photo = photoDto.getPhotos()[index];

            Product product = new Product();
            initiatePO(userLogin.getClientId(), product);
            product.setDraft(false);

            product.setCode(((String) row.getDatas().get(0)).toUpperCase());
            product.setName((String) row.getDatas().get(1));
            product.setProductCategory((I_Category) row.getDatas().get(2));
            product.setUom((I_Category) row.getDatas().get(3));
            product.setPrice((BigDecimal) row.getDatas().get(4));
            
            BigDecimal productivity = (BigDecimal) row.getDatas().get(5);
            productivity = productivity == null ? BigDecimal.ONE : productivity;
            product.setProductivity(productivity);
            
            product.setDescription((String) row.getDatas().get(6));

            product.setPhoto(photo);

            productToInsert.add(product);

            index++;
        }

        if (!productToInsert.isEmpty()) {
            productRepository.insertBatch(userLogin.getClientId(), productToInsert);
        }

        return new ImportResultDto(dto.getTotal(), productToInsert.size());
    }

    private Map<String, I_Category> getProductCategory(String clientId) {
        List<I_Category> productCategories = productCategoryRepository.getAll(clientId, null);

        HashMap<String, I_Category> map = new HashMap<String, I_Category>();

        if (productCategories != null) {
            for (I_Category productCategory : productCategories) {
                map.put(productCategory.getName().toUpperCase(), productCategory);
            }
        }

        return map;
    }

    private Map<String, I_Category> getUom(String clientId) {
        List<I_Category> uoms = uomRepository.getAll(clientId, null);

        HashMap<String, I_Category> map = new HashMap<String, I_Category>();

        if (uoms != null) {
            for (I_Category uom : uoms) {
                map.put(uom.getName().toUpperCase(), uom);
            }
        }

        return map;
    }

}
