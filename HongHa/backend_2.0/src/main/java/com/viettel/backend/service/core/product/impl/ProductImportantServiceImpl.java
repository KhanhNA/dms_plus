package com.viettel.backend.service.core.product.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.product.ProductImportantCreateDto;
import com.viettel.backend.dto.product.ProductImportantDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.AbstractExportService;
import com.viettel.backend.service.core.product.ProductImportantService;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.ProductImportantRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.product.I_Product;

@Service
public class ProductImportantServiceImpl extends AbstractExportService implements ProductImportantService {

	@Autowired
	ProductImportantRepository productImportantRepository;

	@Autowired
	ProductCategoryRepository productCategoryRepository;

	@Autowired
	ProductRepository productRepository;

	public void saveProductImportant(UserLogin userLogin, ProductImportantCreateDto productImportantCreateDto) {
		productImportantRepository.saveProductImportant(userLogin.getClientId(),
				productImportantCreateDto.getProductIds());
	}

	@Override
	public List<ProductImportantDto> getProductImportant(UserLogin userLogin) {
		Map<String, I_Product> products = productRepository.getProductMap(userLogin.getClientId(), true);
		Set<String> productImportantIds = productImportantRepository.getProductImportant(userLogin.getClientId());
		List<ProductImportantDto> dtos = new ArrayList<ProductImportantDto>();
		for (I_Product product : products.values()) {
			boolean isImportant = false;
			if (productImportantIds.contains(product.getId())) {
				isImportant = true;
			}
			ProductImportantDto dto = new ProductImportantDto(product, isImportant);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<ProductImportantDto> getProductImportantByCategory(UserLogin userLogin, String categoryId) {
		Map<String, I_Product> products = productRepository.getProductMap(userLogin.getClientId(), true);
		Set<String> productImportantIds = productImportantRepository.getProductImportant(userLogin.getClientId());
		List<ProductImportantDto> dtos = new ArrayList<ProductImportantDto>();
		for (I_Product product : products.values()) {
			boolean isImportant = false;
			if (productImportantIds.contains(product.getId())
					&& product.getProductCategory().getId().equals(categoryId)) {
				isImportant = true;
			}
			if (StringUtils.isNullOrEmpty(categoryId) || isImportant) {
				ProductImportantDto dto = new ProductImportantDto(product, isImportant);
				dtos.add(dto);
			}
		}
		return dtos;
	}

	@Override
	public InputStream exportProductImportant(UserLogin userLogin, String _categoryId, String lang) {
		lang = getLang(lang);

		if (!StringUtils.isNullOrEmpty(_categoryId)) {
			I_Category category = getMandatoryCategory(userLogin, _categoryId);
			_categoryId = category.getId();
		}

		List<ProductImportantDto> productImportants = getProductImportantByCategory(userLogin, _categoryId);
		// Group by Category
		Map<String, List<ProductImportantDto>> mapPI = new HashMap<>();
		for (ProductImportantDto dto : productImportants) {
			String categoryName = dto.getProductCategory().getName();
			if (!mapPI.containsKey(categoryName)) {
				List<ProductImportantDto> listTemp = new ArrayList<>();
				listTemp.add(dto);
				mapPI.put(categoryName, listTemp);
			} else {
				mapPI.get(categoryName).add(dto);
			}
		}

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "product.list"));

			Row row;

			String[] headers = new String[] { "STT", "product.name", "product.code", "price" };

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, headers.length - 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "product.list"));

			int rownum = 3;
			// Create table headers
			row = sheet.createRow(rownum);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle textBoldCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			int stt = 0;
			for (String key : mapPI.keySet()) {
				row = sheet.createRow(++rownum);
				for (int i = 0; i < headers.length; ++i) {
					Cell cell = row.createCell(i);
					cell.setCellStyle(textBoldCellStyle);
					if (i == 1) {
						cell.setCellValue(key);
					}
				}
				sheet.addMergedRegion(new CellRangeAddress(rownum, rownum, 1, headers.length - 1));
				List<ProductImportantDto> productImportantsByCategory = mapPI.get(key);
				for (ProductImportantDto productImportant : productImportantsByCategory) {
					row = sheet.createRow(++rownum);
					createCell(row, 0, numberCellStyle, ++stt);
					createCell(row, 1, textCellStyle, productImportant.getName());
					createCell(row, 2, textCellStyle, productImportant.getCode());
					createCell(row, 3, numberCellStyle, productImportant.getPrice().toString());
				}
			}

			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);

			int lastRownum = ++rownum;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(rownum, rownum, 0, 1));
			createCell(row, 0, footerTextCellStyle, productImportants.size() + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			File outTempFile = File.createTempFile("ProductImportantList_" + System.currentTimeMillis(), "tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new FileInputStream(outTempFile);
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

}
