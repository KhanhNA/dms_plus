package com.viettel.backend.service.core.product.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.product.ProductDto;
import com.viettel.backend.dto.product.ProductSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.common.CategoryReadonlyServiceImpl;
import com.viettel.backend.service.core.product.ProductReadonlyService;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.InventoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.entity.PageSizeRequest;

@Service
public class ProductReadonlyServiceImpl extends CategoryReadonlyServiceImpl<I_Product, ProductSimpleDto>
        implements ProductReadonlyService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private CacheVisitOrderService cacheVisitOrderService;

    @Override
    public ProductSimpleDto createSimpleDto(UserLogin userLogin, I_Product domain) {
        return new ProductSimpleDto(domain, getProductPhotoFactory(userLogin), null);
    }

    @Override
    protected CategoryBasicRepository<I_Product> getRepository() {
        return productRepository;
    }

    @Override
    public ListDto<ProductSimpleDto> getAll(UserLogin userLogin, String distributorId) {
        List<I_Product> domains = productRepository.getAll(userLogin.getClientId(), distributorId);
        if (CollectionUtils.isEmpty(domains)) {
            return ListDto.emptyList();
        }

        final I_Config config = getConfig(userLogin);

        List<ProductSimpleDto> dtos = new ArrayList<ProductSimpleDto>(domains.size());
        for (final I_Product domain : domains) {
            dtos.add(new ProductSimpleDto(domain, new I_ProductPhotoFactory() {
                
                @Override
                public String getPhoto(String productId) {
                    String photo = domain.getPhoto();
                    if (photo == null) {
                        return config.getProductPhoto();
                    }
                    return photo;
                }
            }, null));
        }

        return new ListDto<ProductSimpleDto>(dtos);
    }
    
    @Override
    public ListDto<ProductDto> getList(UserLogin userLogin, String search, String _distributorId,
            PageSizeRequest pageSizeRequest) {
        String searchName = search;
        String searchCode = search == null ? null : search.toUpperCase();

        List<I_Product> domains = getRepository().getList(userLogin.getClientId(), false, true, null, searchName,
                searchCode, pageSizeRequest);
        if (CollectionUtils.isEmpty(domains) && pageSizeRequest.getPageNumber() == 0) {
            return ListDto.emptyList();
        }

        // Calculate available quantity
        Map<String, BigDecimal> availableMap = Collections.emptyMap();
        // Check if this client use Inventory
        if (userLogin.hasModule(Module.INVENTORY)) {
            // Only calculate for Salesman and Distributor
            List<I_Distributor> accessibleDistributors = getAccessibleDistributors(userLogin);
            if (accessibleDistributors.size() == 1) {
                availableMap = calculateProductAvailable(userLogin, domains, accessibleDistributors.get(0));
            }
        }

        I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);

        List<ProductDto> dtos = new ArrayList<ProductDto>(domains.size());
        for (I_Product domain : domains) {
            dtos.add(new ProductDto(domain, productPhotoFactory, null, availableMap.get(domain.getId())));
        }

        long size = Long.valueOf(dtos.size());
        if (pageSizeRequest != null) {
            if (pageSizeRequest.getPageNumber() > 0 || size == pageSizeRequest.getPageSize()) {
                size = getRepository().count(userLogin.getClientId(), false, true, null, searchName, searchCode);
            }
        }

        return new ListDto<ProductDto>(dtos, size);
    }

    private Map<String, BigDecimal> calculateProductAvailable(UserLogin userLogin, Collection<I_Product> products,
            I_Distributor distributor) {
        Map<String, I_Product> productMap = getPOMap(products);

        I_Inventory inventory = inventoryRepository.getLatestInventory(userLogin.getClientId(), distributor.getId());
        if (inventory == null) {
            return Collections.emptyMap();
        }

        Map<String, BigDecimal> availableMap = new HashMap<>(products.size());
        // Put original available to map
        for (I_ProductQuantity inventoryDetail : inventory.getDetails()) {
            // Skip products not contains in list
            if (!productMap.containsKey(inventoryDetail.getId())) {
                continue;
            }
            availableMap.put(inventoryDetail.getId(), inventoryDetail.getQuantity());
        }

        // Subtract sold quantity from map
        Map<String, Double> productSoldMap = cacheVisitOrderService
                .getQuantitySoldByProductFromLastInventoryUpdate(userLogin.getClientId(), distributor.getId());

        if (productSoldMap != null) {
            for (Map.Entry<String, Double> entry : productSoldMap.entrySet()) {
                BigDecimal original = availableMap.get(entry.getKey());
                // Skip products not have inventory
                if (original == null) {
                    continue;
                }
                availableMap.put(entry.getKey(), original.subtract(new BigDecimal(entry.getValue())));
            }
        }

        return availableMap;
    }

}
