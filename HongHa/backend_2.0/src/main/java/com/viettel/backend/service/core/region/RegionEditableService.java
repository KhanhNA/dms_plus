package com.viettel.backend.service.core.region;

import java.util.List;

import com.viettel.backend.dto.common.CategorySelectionDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.region.RegionCreateDto;
import com.viettel.backend.dto.region.RegionDto;
import com.viettel.backend.dto.region.RegionListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface RegionEditableService extends CategoryEditableService<RegionListDto, RegionDto, RegionCreateDto> {

	public ListDto<CategorySelectionDto> getDistributorOfRegion(UserLogin userLogin, String id);

	public void updateDistributorsForRegion(UserLogin userLogin, String id, List<String> distributorIds);
}
