package com.viettel.backend.service.core.region;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.region.RegionHierarchyDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.CategoryReadonlyService;
import com.viettel.repository.common.domain.data.I_Region;

public interface RegionReadonlyService extends CategoryReadonlyService<CategorySimpleDto> {
	
	public I_Region getRoot(UserLogin userLogin);

	public ListDto<RegionHierarchyDto> getHierarchy(UserLogin userLogin);
	
	public ListDto<CategorySimpleDto> getAllRegions(UserLogin userLogin);
	
}
