package com.viettel.backend.service.core.region.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.CategorySelectionDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.region.RegionCreateDto;
import com.viettel.backend.dto.region.RegionDto;
import com.viettel.backend.dto.region.RegionListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.CategoryEditableServiceImpl;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.Distributor;
import com.viettel.backend.service.core.pdto.Region;
import com.viettel.backend.service.core.region.RegionEditableService;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.ADMIN })
@Service
public class RegionEditableServiceImpl
		extends CategoryEditableServiceImpl<I_Region, RegionListDto, RegionDto, RegionCreateDto>
		implements RegionEditableService {

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private CodeGeneratorRepository codeGeneratorRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Override
	protected CategoryBasicRepository<I_Region> getRepository() {
		return regionRepository;
	}

	@Override
	protected boolean isUseDistributor() {
		return false;
	}

	@Override
	protected RegionListDto createListSimpleDto(UserLogin userLogin, I_Region domain) {
		return new RegionListDto(domain);
	}

	@Override
	protected RegionDto createListDetailDto(UserLogin userLogin, I_Region domain) {
		return new RegionDto(domain);
	}

	@Override
	public boolean enable(UserLogin userLogin, String _id) {
		super.enable(userLogin, _id);
		I_Region current = getMandatoryPO(userLogin, _id, regionRepository);
		updateParentNode(userLogin, _id, current.getParent().getId(), true);
		return true;
	}

	@Override
	protected void afterSetActive(UserLogin userLogin, I_Region domain, boolean active) {
		updateParentNode(userLogin, domain.getId(), domain.getParent().getId(), active);
		updateChildrenNode(userLogin, domain.getId(), active);
	}

	private void updateParentNode(UserLogin userLogin, String currentId, String parentId, boolean active) {
		I_Region current = getMandatoryPO(userLogin, currentId, regionRepository);
		I_Region parent = getMandatoryPO(userLogin, parentId, regionRepository);

		BusinessAssert.notNull(current);
		BusinessAssert.notNull(parent);

		RegionCreateDto parentDto = new RegionCreateDto();
		parentDto.setId(parentId);
		parentDto.setParentRegionId(parent.getParent() != null ? parent.getParent().getId() : null);
		parentDto.setCode(parent.getCode());
		parentDto.setName(parent.getName());
		// Copy distributors
		if (parent.getDistributors() != null && parent.getDistributors().size() > 0) {
			List<CategoryEmbed> distributors = new ArrayList<>();
			for (I_CategoryEmbed dis : parent.getDistributors()) {
				distributors.add(new CategoryEmbed(dis));
			}
			parentDto.setDistributors(distributors);
		}
		// Copy children
		List<CategoryEmbed> children = new ArrayList<>();
		for (I_CategoryEmbed child : parent.getChildren()) {
			children.add(new CategoryEmbed(child));
		}
		if (active) {
			CategoryEmbed node = new CategoryEmbed();
			node.setId(current.getId());
			node.setName(current.getName());
			children.add(node);

		} else {
			for (Iterator<CategoryEmbed> iter = children.listIterator(); iter.hasNext();) {
				CategoryEmbed item = iter.next();
				if (item.getId().equalsIgnoreCase(currentId)) {
					iter.remove();
					break;
				}
			}
		}
		parentDto.setChildren(children);
		update(userLogin, parentId, parentDto);
	}

	/**
	 * Only using in case of deactivate region. All children region must to set
	 * default parent as root
	 */
	private void updateChildrenNode(UserLogin userLogin, String currentId, boolean active) {
		if (active == true)
			return;
		I_Region current = getMandatoryPO(userLogin, currentId, regionRepository);
		I_Region root = regionRepository.getRootNode(userLogin.getClientId());
		BusinessAssert.notNull(current);
		BusinessAssert.notNull(root);

		List<String> childrenIds = new ArrayList<>();
		for (I_CategoryEmbed item : current.getChildren()) {
			childrenIds.add(item.getId());
		}
		if (childrenIds.isEmpty())
			return;

		for (String childId : childrenIds) {
			I_Region child = getMandatoryPO(userLogin, childId, regionRepository);
			RegionCreateDto childDto = new RegionCreateDto();
			childDto.setParentRegionId(child.getParent() != null ? child.getParent().getId() : null);
			childDto.setCode(child.getCode());
			childDto.setName(child.getName());
			// Copy distributors
			if (child.getDistributors() != null) {
				List<CategoryEmbed> distributors = new ArrayList<>();
				for (I_CategoryEmbed dis : child.getDistributors()) {
					distributors.add(new CategoryEmbed(dis));
				}
				childDto.setDistributors(distributors);
			}
			// Copy children
			List<CategoryEmbed> children = new ArrayList<>();
			for (I_CategoryEmbed c : child.getChildren()) {
				children.add(new CategoryEmbed(c));
			}
			childDto.setChildren(children);
			childDto.setParentRegionId(root.getId());
			update(userLogin, childId, childDto);
		}
	}

	@Override
	public I_Region getObjectForCreate(UserLogin userLogin, RegionCreateDto createdto) {
		BusinessAssert.notNull(createdto);
		BusinessAssert.notNull(createdto.getName());
		BusinessAssert.notNull(createdto.getParentRegionId(), "Create new region must set its parent");

		// Check exist
		BusinessAssert.notTrue(getRepository().checkNameExist(userLogin.getClientId(), null, createdto.getName(), null),
				BusinessExceptionCode.NAME_USED,
				String.format("Record with [name=%s] already exist", createdto.getName()));

		Region region = new Region();
		initiatePO(userLogin.getClientId(), region);
		region.setDraft(true);
		region.setRoot(false);
		region.setLeaf(true);
		region.setName(createdto.getName());
		region.setChildren(Collections.emptyList());
		region.setDistributors(Collections.emptySet());
		region.setCode(codeGeneratorRepository.getRegionCode(userLogin.getClientId().toString()));

		I_CategoryEmbed parent = getMandatoryPO(userLogin, createdto.getParentRegionId(), regionRepository);
		region.setParent(new CategoryEmbed(parent));

		return region;
	}

	@Override
	public I_Region getObjectForUpdate(UserLogin userLogin, I_Region oldDomain, RegionCreateDto createdto) {
		BusinessAssert.notNull(createdto);
		BusinessAssert.notNull(createdto.getName());
		if (!oldDomain.isRoot()) {
			BusinessAssert.notNull(createdto.getParentRegionId(), "Create new region must set its parent");
		}
		BusinessAssert.isTrue(checkValidParentRegion(userLogin, oldDomain.getId(), createdto.getParentRegionId()),
				"Candidate parent of this region is not valid cause of being its children");

		Region region = new Region(oldDomain);
		region = updateCategoryFieldFromDto(userLogin, region, createdto);
		region.setName(createdto.getName());
		// COPY Distributors
		if (createdto.getDistributors() != null) {
			region.setDistributors(new HashSet<I_CategoryEmbed>(createdto.getDistributors()));
		}
		// UPDATE parent region
		if (createdto.getParentRegionId() != null && (oldDomain.getParent() == null
				|| !oldDomain.getParent().getId().equalsIgnoreCase(createdto.getParentRegionId()))) {
			I_CategoryEmbed parentNew = getMandatoryPO(userLogin, createdto.getParentRegionId(), regionRepository);
			// update children of ex-parent
			updateParentNode(userLogin, oldDomain.getId(), oldDomain.getParent().getId(), false);
			// update children of new parent
			updateParentNode(userLogin, oldDomain.getId(), createdto.getParentRegionId(), true);
			region.setParent(new CategoryEmbed(parentNew));
		} else {
			region.setParent(oldDomain.getParent());
		}

		// UPDATE Children
		if (createdto.getChildren() != null) {
			List<I_CategoryEmbed> children = new ArrayList<>();
			for (CategoryEmbed child : createdto.getChildren()) {
				children.add(child);
			}
			region.setChildren(children);
		}

		return region;
	}

	private boolean checkValidParentRegion(UserLogin userLogin, String currentId, String candidateParent) {
		if (currentId.equalsIgnoreCase(candidateParent)) {
			return false;
		}
		I_Region current = getMandatoryPO(userLogin, currentId, regionRepository);
		for (I_CategoryEmbed child : current.getChildren()) {
			if (!checkValidParentRegion(userLogin, child.getId(), candidateParent))
				return false;
		}
		return true;
	}

	@Override
	public ListDto<CategorySelectionDto> getDistributorOfRegion(UserLogin userLogin, String id) {
		List<I_CategoryEmbed> distributors = regionRepository.getDistributorsOfRegion(userLogin.getClientId(), id);
		List<I_Distributor> notAssignedDistributors = distributorRepository
				.getDistributorNotAssignedToRegion(userLogin.getClientId());

		List<CategorySelectionDto> dtos = new ArrayList<>();
		for (I_CategoryEmbed distributor : distributors) {
			Distributor dis = new Distributor();
			dis.setId(distributor.getId());
			dis.setName(distributor.getName());
			dis.setCode(distributor.getCode());
			dtos.add(new CategorySelectionDto(dis, true));
		}
		for (I_Distributor distributor : notAssignedDistributors) {
			dtos.add(new CategorySelectionDto(distributor, false));
		}
		return new ListDto<>(dtos);
	}

	@Override
	public void updateDistributorsForRegion(UserLogin userLogin, String id, List<String> distributorIds) {
		I_Region region = regionRepository.getById(userLogin.getClientId(), id);
		// Delete link from distributor to this region
		if (region.getDistributors() != null) {
			for (I_CategoryEmbed item : region.getDistributors()) {
				if (!distributorIds.contains(item.getId())) {
					I_Distributor dis = distributorRepository.getById(userLogin.getClientId(), item.getId());
					Distributor dis2 = new Distributor(dis);
					dis2.setRegion(new CategoryEmbed());
					distributorRepository.save(userLogin.getClientId(), dis2);
				}
			}
		}
		// Add link to this region from distributor
		for (String disId : distributorIds) {
			I_Distributor dis = distributorRepository.getById(userLogin.getClientId(), disId);
			Distributor dis2 = new Distributor(dis);
			CategoryEmbed cate = new CategoryEmbed();
			cate.setName(region.getName());
			cate.setId(region.getId());
			cate.setCode(region.getCode());
			dis2.setRegion(cate);
			distributorRepository.save(userLogin.getClientId(), dis2);
		}

		if (distributorIds == null || distributorIds.size() == 0) {
			regionRepository.setDistributorsOfManager(userLogin.getClientId(), id, Collections.emptySet());
		} else {
			regionRepository.setDistributorsOfManager(userLogin.getClientId(), id, distributorIds);
		}
	}
}
