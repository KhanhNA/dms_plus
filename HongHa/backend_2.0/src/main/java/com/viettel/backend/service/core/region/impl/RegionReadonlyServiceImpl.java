package com.viettel.backend.service.core.region.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.region.RegionHierarchyDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.CategoryReadonlyServiceImpl;
import com.viettel.backend.service.core.region.RegionReadonlyService;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;

@Service
public class RegionReadonlyServiceImpl extends CategoryReadonlyServiceImpl<I_Region, CategorySimpleDto>
		implements RegionReadonlyService {

	@Autowired
	RegionRepository regionRepository;

	@Override
	public CategorySimpleDto createSimpleDto(UserLogin userLogin, I_Region domain) {
		return new CategorySimpleDto(domain);
	}

	@Override
	protected CategoryBasicRepository<I_Region> getRepository() {
		return regionRepository;
	}

	@Override
	public I_Region getRoot(UserLogin userLogin) {
		return regionRepository.getRootNode(userLogin.getClientId());
	}

	@Override
	public ListDto<RegionHierarchyDto> getHierarchy(UserLogin userLogin) {
		I_Region root = getRoot(userLogin);
		RegionHierarchyDto tree = new RegionHierarchyDto(root);
		tranversal(tree, root, userLogin);
		List<RegionHierarchyDto> lst = new ArrayList<RegionHierarchyDto>(2);
		lst.add(tree);
		return new ListDto<RegionHierarchyDto>(lst);
	}

	private void tranversal(RegionHierarchyDto tree, I_Region node, UserLogin userLogin) {
		tree.setName(node.getName());
		tree.setId(node.getId());
		if (node.getChildren() == null || node.getChildren().isEmpty()) {
			tree.setChildren(Collections.emptyList());
			return;
		}
		for (I_CategoryEmbed item : node.getChildren()) {
			I_Region data = getMandatoryPO(userLogin, item.getId(), regionRepository);
			RegionHierarchyDto child = new RegionHierarchyDto(data);
			tranversal(child, data, userLogin);
			tree.addChild(child);
		}
	}

	@Override
	public ListDto<CategorySimpleDto> getAllRegions(UserLogin userLogin) {
		List<I_Region> domains = getAccessibleRegion(userLogin);
		if (CollectionUtils.isEmpty(domains)) {
			return ListDto.emptyList();
		}

		List<CategorySimpleDto> dtos = new ArrayList<CategorySimpleDto>(domains.size());
		for (I_Region domain : domains) {
			dtos.add(new CategorySimpleDto(domain));
		}
		return new ListDto<CategorySimpleDto>(dtos);
	}

}
