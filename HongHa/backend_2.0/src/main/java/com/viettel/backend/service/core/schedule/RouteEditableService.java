package com.viettel.backend.service.core.schedule;

import com.viettel.backend.dto.schedule.RouteCreateDto;
import com.viettel.backend.dto.schedule.RouteDto;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface RouteEditableService extends CategoryEditableService<RouteDto, RouteDto, RouteCreateDto> {

}
