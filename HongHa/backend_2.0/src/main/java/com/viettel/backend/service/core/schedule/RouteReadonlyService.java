package com.viettel.backend.service.core.schedule;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.service.core.common.CategoryReadonlyService;

public interface RouteReadonlyService extends CategoryReadonlyService<CategorySimpleDto> {

}
