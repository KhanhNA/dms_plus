package com.viettel.backend.service.core.schedule.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.dto.common.ImportConfirmDto.RowData;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheScheduleService;
import com.viettel.backend.service.core.common.AbstractImportService;
import com.viettel.backend.service.core.pdto.Schedule;
import com.viettel.backend.service.core.pdto.ScheduleItem;
import com.viettel.backend.service.core.schedule.CustomerScheduleImportService;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.customer.I_Schedule;
import com.viettel.repository.common.domain.customer.I_ScheduleItem;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.ADMIN, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class CustomerScheduleImportServiceImpl extends AbstractImportService implements CustomerScheduleImportService {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private CacheScheduleService cacheScheduleService;

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public byte[] getTemplate(UserLogin userLogin, String _distributorId, String lang) {
        lang = getLang(lang);

        I_Distributor distributor = getDefaultDistributor(userLogin);
        if (distributor == null) {
            distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
            BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");
        }

        List<I_Route> routes = routeRepository.getAll(userLogin.getClientId(), distributor.getId());
        HashMap<String, I_Route> routeMap = getPOMap(routes);

        List<I_Customer> customers = customerRepository.getAll(userLogin.getClientId(), distributor.getId());

        I_Config config = getConfig(userLogin);

        // Finds the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet scheduleSheet = workbook.createSheet(translate(lang, "schedule"));

        XSSFRow row = scheduleSheet.createRow(0);
        row.createCell(0).setCellValue(translate(lang, "code"));
        row.createCell(1).setCellValue(translate(lang, "name"));
        row.createCell(2).setCellValue(translate(lang, "area"));
        row.createCell(3).setCellValue(translate(lang, "route"));
        row.createCell(4).setCellValue(translate(lang, "monday"));
        row.createCell(5).setCellValue(translate(lang, "tuesday"));
        row.createCell(6).setCellValue(translate(lang, "wednesday"));
        row.createCell(7).setCellValue(translate(lang, "thursday"));
        row.createCell(8).setCellValue(translate(lang, "friday"));
        row.createCell(9).setCellValue(translate(lang, "saturday"));
        row.createCell(10).setCellValue(translate(lang, "sunday"));

        if (config.getNumberWeekOfFrequency() > 1) {
            for (int i = 1; i <= config.getNumberWeekOfFrequency(); i++) {
                row.createCell(10 + i).setCellValue("W" + i);
            }
        }

        int index = 1;
        for (I_Customer customer : customers) {
            row = scheduleSheet.createRow(index);

            I_Schedule schedule = customer.getSchedule();
            if (schedule != null) {
                I_Route route = routeMap.get(schedule.getRouteId());

                if (schedule.getItem() != null) {
                    fillDataToRow(row, config, customer, route, schedule.getItem());
                    index++;
                }
            } else {
                fillDataToRow(row, config, customer, null, null);
                index++;
            }
        }

        index = 0;
        XSSFSheet routeSheet = workbook.createSheet(translate(lang, "route"));
        for (I_Route route : routes) {
            row = routeSheet.createRow(index);
            row.createCell(0).setCellValue(route.getName());
            index++;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);
            return baos.toByteArray();
        } catch (IOException e) {
            logger.error("error write file", e);
            throw new UnsupportedOperationException(e);
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }

    private void fillDataToRow(XSSFRow row, I_Config config, I_Customer customer, I_Route route, I_ScheduleItem item) {
        row.createCell(0).setCellValue(customer.getCode());
        row.createCell(1).setCellValue(customer.getName());
        row.createCell(2).setCellValue(customer.getArea().getName());

        if (route != null) {
            row.createCell(3).setCellValue(route.getName());
        }

        if (item != null) {
            row.createCell(4).setCellValue(!item.isMonday() ? null : "x");
            row.createCell(5).setCellValue(!item.isTuesday() ? null : "x");
            row.createCell(6).setCellValue(!item.isWednesday() ? null : "x");
            row.createCell(7).setCellValue(!item.isThursday() ? null : "x");
            row.createCell(8).setCellValue(!item.isFriday() ? null : "x");
            row.createCell(9).setCellValue(!item.isSaturday() ? null : "x");
            row.createCell(10).setCellValue(!item.isSunday() ? null : "x");

            if (config.getFirstDayOfWeek() > 1) {
                for (int i = 1; i <= config.getNumberWeekOfFrequency(); i++) {
                    if (item.getWeeks() != null && item.getWeeks().contains(i)) {
                        row.createCell(10 + i).setCellValue("x");
                    }
                }
            }
        }
    }

    private I_CellValidator[] getValidators(UserLogin userLogin, String distributorId) {
        I_Config config = getConfig(userLogin);

        List<I_Customer> customers = customerRepository.getAll(userLogin.getClientId(), distributorId);
        Map<String, I_Customer> customerByCode = new HashMap<String, I_Customer>();
        for (I_Customer customer : customers) {
            customerByCode.put(customer.getCode().trim().toUpperCase(), customer);
        }

        List<I_Route> routes = routeRepository.getAll(userLogin.getClientId(), distributorId);
        Map<String, I_Route> routeByName = new HashMap<String, I_Route>();
        for (I_Route route : routes) {
            routeByName.put(route.getName().trim().toUpperCase(), route);
        }

        LinkedList<I_CellValidator> validators = new LinkedList<AbstractImportService.I_CellValidator>();
        validators.add(new MultiCellValidator(new StringMandatoryCellValidator(),
                new ReferenceCellValidator<I_Customer>(customerByCode)));
        validators.add(new NoValidator());
        validators.add(new NoValidator());
        validators.add(new ReferenceCellValidator<I_Route>(routeByName));
        validators.add(null);
        validators.add(null);
        validators.add(null);
        validators.add(null);
        validators.add(null);
        validators.add(null);
        validators.add(null);

        if (config.getNumberWeekOfFrequency() > 1) {
            for (int i = 0; i < config.getNumberWeekOfFrequency(); i++) {
                validators.add(null);
            }
        }

        I_CellValidator[] array = new I_CellValidator[validators.size()];
        validators.toArray(array);
        return array;
    }

    @Override
    public ImportConfirmDto verify(UserLogin userLogin, String _distributorId, String fileId) {
        I_Distributor distributor = getDefaultDistributor(userLogin);
        if (distributor == null) {
            distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
            BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");
        }

        I_CellValidator[] validators = getValidators(userLogin, distributor.getId());
        return getErrorRows(userLogin, fileId, validators);
    }

    @Override
    public ImportResultDto doImport(UserLogin userLogin, String _distributorId, String fileId) {
        I_Distributor distributor = getDefaultDistributor(userLogin);
        if (distributor == null) {
            distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
            BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");
        }

        I_Config config = getConfig(userLogin);

        I_CellValidator[] validators = getValidators(userLogin, distributor.getId());

        ImportConfirmDto dto = getValidRows(userLogin, fileId, validators);

        for (RowData row : dto.getRowDatas()) {
            I_Customer customer = (I_Customer) row.getDatas().get(0);
            I_Route route = (I_Route) row.getDatas().get(3);
            if (route != null) {
                ScheduleItem item = new ScheduleItem();
                item.setMonday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(4), true));
                item.setTuesday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(5), true));
                item.setWednesday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(6), true));
                item.setThursday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(7), true));
                item.setFriday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(8), true));
                item.setSaturday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(9), true));
                item.setSunday(!StringUtils.isNullOrEmpty((String) row.getDatas().get(10), true));

                boolean weeksValid = true;
                if (config.getNumberWeekOfFrequency() > 1) {
                    weeksValid = false;
                    List<Integer> weeks = new LinkedList<Integer>();
                    for (int i = 1; i <= config.getNumberWeekOfFrequency(); i++) {
                        if (!StringUtils.isNullOrEmpty((String) row.getDatas().get(10 + i))) {
                            weeks.add(i);
                            weeksValid = true;
                        }
                    }
                    item.setWeeks(weeks);
                }

                if ((item.isMonday() || item.isTuesday() || item.isWednesday() || item.isThursday() || item.isFriday()
                        || item.isSaturday() || item.isSunday()) && weeksValid) {
                    Schedule schedule = new Schedule();
                    schedule.setRouteId(route.getId());
                    schedule.setItem(item);

                    customerRepository.updateSchedule(userLogin.getClientId(), customer.getId(), schedule);
                } else {
                    customerRepository.updateSchedule(userLogin.getClientId(), customer.getId(), null);
                }
            } else {
                customerRepository.updateSchedule(userLogin.getClientId(), customer.getId(), null);
            }
        }
        
        cacheScheduleService.onChange(userLogin.getClientId(), distributor.getId());

        return new ImportResultDto(dto.getTotal(), dto.getRowDatas().size());
    }

}
