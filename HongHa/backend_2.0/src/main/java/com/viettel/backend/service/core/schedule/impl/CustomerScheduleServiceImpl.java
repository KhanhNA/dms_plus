package com.viettel.backend.service.core.schedule.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.schedule.CustomerScheduleCreateDto;
import com.viettel.backend.dto.schedule.CustomerScheduleDto;
import com.viettel.backend.dto.schedule.ScheduleItemDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheScheduleService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pdto.Schedule;
import com.viettel.backend.service.core.pdto.ScheduleItem;
import com.viettel.backend.service.core.schedule.CustomerScheduleService;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.VisitingRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.customer.I_Schedule;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;

@RolePermission(value = { Role.ADMIN, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class CustomerScheduleServiceImpl extends AbstractService implements CustomerScheduleService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private VisitingRepository visitingRepository;

    @Autowired
    private CacheScheduleService cacheScheduleService;

    @Override
    public ListDto<CustomerScheduleDto> getCustomerSchedules(UserLogin userLogin, String _distributorId,
            boolean searchByRoute, String _routeId, String search, Integer dayOfWeek, PageSizeRequest pageSizeRequest) {
        I_Distributor distributor = getMandatoryDistributor(userLogin, _distributorId);

        String routeId = null;
        if (searchByRoute && _routeId != null) {
            I_Route route = getMandatoryPO(userLogin, _routeId, routeRepository);
            BusinessAssert.equals(route.getDistributor().getId(), distributor.getId(), "route not of this distributor");
            routeId = route.getId();
        }

        if (dayOfWeek != null) {
            BusinessAssert.isTrue(dayOfWeek >= Calendar.SUNDAY && dayOfWeek <= Calendar.SATURDAY,
                    "day of week invalid");
        }

        List<I_Customer> customers = customerRepository.getList(userLogin.getClientId(), distributor.getId(),
                searchByRoute, routeId, dayOfWeek, search, search, pageSizeRequest);
        if (CollectionUtils.isEmpty(customers) && pageSizeRequest.getPageNumber() == 0) {
            return ListDto.emptyList();
        }

        List<CustomerScheduleDto> dtos = new ArrayList<CustomerScheduleDto>(customers.size());
        for (I_Customer customer : customers) {
            dtos.add(new CustomerScheduleDto(customer));
        }

        long size = Long.valueOf(dtos.size());
        if (pageSizeRequest != null) {
            if (pageSizeRequest.getPageNumber() > 0 || size == pageSizeRequest.getPageSize()) {
                size = customerRepository.count(userLogin.getClientId(), distributor.getId(), searchByRoute, routeId,
                        dayOfWeek, search, search);
            }
        }

        return new ListDto<CustomerScheduleDto>(dtos, size);
    }

    @Override
    public CustomerScheduleDto getCustomerSchedule(UserLogin userLogin, String customerId) {
        I_Customer customer = getMandatoryPO(userLogin, customerId, customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer no accessible");
        return new CustomerScheduleDto(customer);
    }

    @Override
    public void saveCustomerScheduleByDistributor(UserLogin userLogin, String _distributorId,
            ListDto<CustomerScheduleCreateDto> list) {
        BusinessAssert.notNull(list);

        I_Distributor distributor = getMandatoryDistributor(userLogin, _distributorId);

        I_Config config = getConfig(userLogin);

        if (list.getList() != null && !list.getList().isEmpty()) {
            // CHECK IF CUSTOMER IS VISITING
            Set<String> customerIds = new HashSet<>();
            for (CustomerScheduleCreateDto dto : list.getList()) {
                customerIds.add(dto.getId());
            }
            BusinessAssert.notTrue(
                    visitingRepository.checkVisiting(userLogin.getClientId(), distributor.getId(), customerIds),
                    BusinessExceptionCode.CUSTOMER_IS_VISITING, "customer is visiting");

            HashMap<String, I_Schedule> scheduleByCustomerId = new HashMap<String, I_Schedule>();
            for (CustomerScheduleCreateDto dto : list.getList()) {
                BusinessAssert.notNull(dto);

                Schedule schedule = null;
                if (dto.getSchedule() != null && dto.getSchedule().getRouteId() != null) {
                    I_Route route = getMandatoryPO(userLogin, dto.getSchedule().getRouteId(), routeRepository);

                    if (dto.getSchedule().getItem() != null) {
                        ScheduleItem item = getScheduleItem(dto.getSchedule().getItem(), config);
                        if (item == null) {
                            break;
                        }
                        schedule = new Schedule();
                        schedule.setRouteId(route.getId());
                        schedule.setItem(item);
                        
                    }
                }

                scheduleByCustomerId.put(dto.getId(), schedule);
            }

            List<I_Customer> customers = customerRepository.getListByIds(userLogin.getClientId(),
                    scheduleByCustomerId.keySet());
            BusinessAssert.equals(customers.size(), scheduleByCustomerId.size());

            for (I_Customer customer : customers) {
                customerRepository.updateSchedule(userLogin.getClientId(), customer.getId(),
                        scheduleByCustomerId.get(customer.getId()));
            }

            cacheScheduleService.onChange(userLogin.getClientId(), distributor == null ? null : distributor.getId());
        }
    }

    @Override
    public void saveCustomerSchedule(UserLogin userLogin, CustomerScheduleCreateDto dto) {
        BusinessAssert.notNull(dto);

        I_Customer customer = getMandatoryPO(userLogin, dto.getId(), customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer no accessible");

        BusinessAssert.notTrue(
                visitingRepository.checkVisiting(userLogin.getClientId(), customer.getDistributor().getId(),
                        Collections.singleton(customer.getId())),
                BusinessExceptionCode.CUSTOMER_IS_VISITING, "customer is visiting");

        I_Config config = getConfig(userLogin);

        Schedule schedule = null;
        if (dto.getSchedule() != null && dto.getSchedule().getRouteId() != null) {
            I_Route route = getMandatoryPO(userLogin, dto.getSchedule().getRouteId(), routeRepository);

            if (dto.getSchedule().getItem() != null) {
                boolean valid = true;

                ScheduleItem item = getScheduleItem(dto.getSchedule().getItem(), config);
                if (item == null) {
                    valid = false;
                }

                if (valid) {
                    schedule = new Schedule();
                    schedule.setRouteId(route.getId());
                    schedule.setItem(item);
                }
            }
        }

        customerRepository.updateSchedule(userLogin.getClientId(), customer.getId(), schedule);
        cacheScheduleService.onChange(userLogin.getClientId(), customer.getDistributor().getId());
    }

    // PRIVATE
    private ScheduleItem getScheduleItem(ScheduleItemDto dto, I_Config config) {
        if (dto != null) {
            if (!(dto.isMonday() || dto.isTuesday() || dto.isWednesday() || dto.isThursday() || dto.isFriday()
                    || dto.isSaturday() || dto.isSunday())) {
                return null;
            }

            if (config.getNumberWeekOfFrequency() > 1) {
                if (dto.getWeeks() == null || dto.getWeeks().isEmpty()) {
                    return null;
                }
            }

            ScheduleItem item = new ScheduleItem();
            item.setMonday(dto.isMonday());
            item.setTuesday(dto.isTuesday());
            item.setWednesday(dto.isWednesday());
            item.setThursday(dto.isThursday());
            item.setFriday(dto.isFriday());
            item.setSaturday(dto.isSaturday());
            item.setSunday(dto.isSunday());
            item.setWeeks(dto.getWeeks());
            return item;
        }

        return null;
    }

}
