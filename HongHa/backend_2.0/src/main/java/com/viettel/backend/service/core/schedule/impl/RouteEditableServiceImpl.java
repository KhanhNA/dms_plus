package com.viettel.backend.service.core.schedule.impl;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.schedule.RouteCreateDto;
import com.viettel.backend.dto.schedule.RouteDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.CategoryEditableServiceImpl;
import com.viettel.backend.service.core.pdto.Route;
import com.viettel.backend.service.core.pdto.UserEmbed;
import com.viettel.backend.service.core.schedule.RouteEditableService;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.VisitingRepository;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.ADMIN, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class RouteEditableServiceImpl extends CategoryEditableServiceImpl<I_Route, RouteDto, RouteDto, RouteCreateDto>
        implements RouteEditableService {

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private VisitingRepository visitingRepository;

    @Override
    protected boolean isUseDistributor() {
        return true;
    }
    
    @Override
    protected boolean isCanChangeName() {
        return true;
    }
    
    @Override
    protected CategoryBasicRepository<I_Route> getRepository() {
        return routeRepository;
    }

    @Override
    protected void beforeSetActive(UserLogin userLogin, I_Route domain, boolean active) {
        if (active) {
            // ACTIVE

        } else {
            // DESACTIVE
            BusinessAssert
                    .notTrue(
                            customerRepository.checkRouteUsed(userLogin.getClientId(), domain.getDistributor().getId(),
                                    domain.getId()),
                            BusinessExceptionCode.RECORD_USED_IN_SCHEDULE, "route used in customer");
        }
    }
    
    @Override
    public RouteDto createListSimpleDto(UserLogin userLogin, I_Route domain) {
        return createListDetailDto(userLogin, domain);
    }

    @Override
    public RouteDto createListDetailDto(final UserLogin userLogin, final I_Route domain) {
        return new RouteDto(domain);
    }

    @Override
    public I_Route getObjectForCreate(UserLogin userLogin, RouteCreateDto createdto) {
        BusinessAssert.notNull(createdto);

        Route route = new Route();
        initiatePO(userLogin.getClientId(), route);
        route.setDraft(true);

        return getObjectForUpdate(userLogin, route, createdto);
    }

    @Override
    public I_Route getObjectForUpdate(UserLogin userLogin, I_Route oldDomain, RouteCreateDto createdto) {
        BusinessAssert.notNull(createdto);
        
        Route route = new Route(oldDomain);
        route = updateCategoryFieldFromDto(userLogin, route, createdto);
        
        // CANNOT UPDATE ROUTE WHEN CUSTOMER IN THIS ROUTE IS VISITING
        if (!route.isDraft()) {
            List<I_Customer> customers = customerRepository.getCustomersByRoutes(userLogin.getClientId(),
                    route.getDistributor().getId(), Collections.singleton(route.getId()), null);
            Set<String> customerIds = getIdSet(customers);
            BusinessAssert.notTrue(
                    visitingRepository.checkVisiting(userLogin.getClientId(), route.getDistributor().getId(), customerIds),
                    BusinessExceptionCode.CUSTOMER_IS_VISITING, "customer is visiting");
        }
        
        if (createdto.getSalesmanId() != null) {
            I_User salesman = getMandatoryPO(userLogin, createdto.getSalesmanId(), userRepository);

            BusinessAssert.notNull(salesman.getDistributor(), "salesman distributor null");
            BusinessAssert.isTrue(salesman.getDistributor().getId().equals(route.getDistributor().getId()),
                    "salesman is not belong to this distributor");

            route.setSalesman(new UserEmbed(salesman));
        } else {
            route.setSalesman(null);
        }

        return route;
    }

}
