package com.viettel.backend.service.core.schedule.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.CategoryReadonlyServiceImpl;
import com.viettel.backend.service.core.schedule.RouteReadonlyService;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.domain.customer.I_Route;

@Service
public class RouteReadonlyServiceImpl extends CategoryReadonlyServiceImpl<I_Route, CategorySimpleDto> implements
        RouteReadonlyService {

    @Autowired
    private RouteRepository routeRepository;
    
    @Override
    protected boolean isUseDistributor() {
        return true;
    }

    @Override
    public CategorySimpleDto createSimpleDto(UserLogin userLogin, I_Route domain) {
        return new CategorySimpleDto(domain);
    }

    @Override
    protected CategoryBasicRepository<I_Route> getRepository() {
        return routeRepository;
    }

}
