package com.viettel.backend.service.core.sub;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.order.OrderCreateDto;
import com.viettel.backend.dto.order.OrderCreateDto.OrderDetailCreatedDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.cache.CacheManagerService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.CustomerEmbed;
import com.viettel.backend.service.core.pdto.Order;
import com.viettel.backend.service.core.pdto.UserEmbed;
import com.viettel.backend.service.core.pdto.Visit;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.MasterDataRepository;
import com.viettel.repository.common.OrderPendingRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.VisitingRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;
import com.viettel.repository.common.util.LocationUtils;

@Service
public class GenerateDataService extends AbstractService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private VisitingRepository visitingRepository;

    @Autowired
    private OrderPendingRepository orderPendingRepository;

    @Autowired
    private OrderCalculationService orderCalculationService;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private MasterDataRepository masterDataRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private CacheManagerService cacheManagerService;

    @Autowired
    private CacheCalendarService cacheCalendarService;

    public void generateVisitAndOrder(I_Category client, SimpleDate fromDate, SimpleDate toDate) {
        String clientId = client.getId();

        masterDataRepository.deleteDatasWithoutCategory(clientId);
        cacheManagerService.clearByClient(clientId);

        I_User defaultAdmin = userRepository.getDefaultAdmin(clientId);
        UserLogin userLogin = new UserLogin(clientId, client.getCode(), client.getName(), defaultAdmin.getId(),
                defaultAdmin.getUsername(), defaultAdmin.getRole(), Module.ALL);

        HashMap<String, Set<String>> routeIdsBySalesman = new HashMap<String, Set<String>>();

        I_Config config = configRepository.getConfig(clientId);
        List<I_Product> products = productRepository.getAll(clientId, null);

        List<SimpleDate> dates = cacheCalendarService.getWorkingDays(clientId, new Period(fromDate, toDate));

        List<I_User> supervisors = userRepository.getUserByRole(clientId, Role.SUPERVISOR);
        HashMap<String, List<I_Distributor>> distributorBySupervisor = new HashMap<>();
        HashMap<String, List<I_User>> salesmanByDistributor = new HashMap<>();
        HashMap<String, List<I_Customer>> customerBySalesman = new HashMap<>();

        for (I_User supervisor : supervisors) {
            List<I_Distributor> distributors = distributorRepository.getDistributorsBySupervisors(clientId,
                    Arrays.asList(supervisor.getId()));
            distributorBySupervisor.put(supervisor.getId(), distributors);

            for (I_Distributor distributor : distributors) {
                List<I_User> salesmen = userRepository.getSalesmen(clientId, Arrays.asList(distributor.getId()));
                salesmanByDistributor.put(distributor.getId(), salesmen);

                for (I_User salesman : salesmen) {
                    List<I_Route> routes = routeRepository.getRoutesBySalesmen(clientId, distributor.getId(),
                            Collections.singleton(salesman.getId()));
                    Set<String> routeIds = getIdSet(routes);
                    routeIdsBySalesman.put(salesman.getId(), routeIds);

                    customerBySalesman.put(salesman.getId(),
                            customerRepository.getCustomersByRoutes(clientId, distributor.getId(), routeIds, null));
                }
            }
        }

        ClassPathResource closePhotoResource = new ClassPathResource("customer-close-photo.jpeg");
        ClassPathResource openPhotoResource = new ClassPathResource("customer-open-photo.jpg");
        String closePhoto = null;
        String openPhoto = null;
        try {
            closePhoto = fileRepository.store(clientId, closePhotoResource.getInputStream(),
                    closePhotoResource.getFilename(), "image/jpeg").toString();

            openPhoto = fileRepository
                    .store(clientId, openPhotoResource.getInputStream(), openPhotoResource.getFilename(), "image/jpeg")
                    .toString();
        } catch (IOException e) {
            logger.error("error store photo", e);
            throw new UnsupportedOperationException(e);
        }

        Random random = new Random();

        ExecutorService es = Executors.newFixedThreadPool(30);
        for (SimpleDate currentDate : dates) {
            for (I_User supervisor : supervisors) {
                List<I_Distributor> distributors = distributorBySupervisor.get(supervisor.getId());
                for (I_Distributor distributor : distributors) {
                    List<I_User> salesmen = salesmanByDistributor.get(distributor.getId());
                    for (I_User salesman : salesmen) {

                        SalesmanDataGenerate salesmanDataGenerate = new SalesmanDataGenerate(userLogin, random, config,
                                currentDate, supervisor, distributor, salesman, products, customerBySalesman,
                                routeIdsBySalesman, openPhoto, closePhoto);

                        es.execute(salesmanDataGenerate);
                    }
                }
            }
        }

        try {
            es.shutdown();
            es.awaitTermination(10, TimeUnit.MINUTES);
            cacheManagerService.init(clientId);
        } catch (InterruptedException e) {
            logger.error("InterruptedException", e);
            Thread.currentThread().interrupt();
            throw new UnsupportedOperationException(e);
        }
    }

    // PRIVATE
    private Visit createVisit(UserLogin userLogin, I_Config config, I_User salesman, I_Distributor distributor,
            I_Customer customer, Location location) {
        Visit visit = new Visit();

        visit.setClientId(userLogin.getClientId());
        visit.setActive(true);
        visit.setDraft(false);

        // LOCATION STATUS
        if (!LocationUtils.checkLocationValid(customer.getLocation())) {
            visit.setLocationStatus(I_Visit.LOCATION_STATUS_CUSTOMER_UNLOCATED);
            visit.setLocation(location);
            visit.setCustomerLocation(null);
        } else {
            if (!LocationUtils.checkLocationValid(location)) {
                visit.setLocationStatus(Visit.LOCATION_STATUS_UNLOCATED);
                visit.setLocation(null);
                visit.setCustomerLocation(customer.getLocation());
            } else {
                double distance = LocationUtils.calculateDistance(customer.getLocation().getLatitude(),
                        customer.getLocation().getLongitude(), location.getLatitude(), location.getLongitude());
                if (distance > config.getVisitDistanceKPI()) {
                    visit.setLocationStatus(Visit.LOCATION_STATUS_TOO_FAR);
                } else {
                    visit.setLocationStatus(Visit.LOCATION_STATUS_LOCATED);
                }

                visit.setLocation(location);
                visit.setCustomerLocation(customer.getLocation());
                visit.setDistance(distance);
            }
        }

        visit.setDistributor(new CategoryEmbed(distributor));
        visit.setSalesman(new UserEmbed(salesman));
        visit.setCustomer(new CustomerEmbed(customer));

        return visit;
    }

    /** distance in km */
    private double[] generateLocation(double longitude, double latitude, double minDistance, double maxDistance) {
        Random random = new Random();

        double distance = -1;

        double foundLongitude = -1;
        double foundLatitude = -1;

        while (distance <= minDistance || distance >= maxDistance) {
            // Convert radius from meters to degrees
            double radiusInDegrees = (maxDistance * 1000) / 111000f;

            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west
            // distances
            double new_x = x / Math.cos(latitude);

            foundLongitude = new_x + longitude;
            foundLatitude = y + latitude;

            distance = LocationUtils.calculateDistance(latitude, longitude, foundLatitude, foundLongitude);
        }

        return new double[] { foundLongitude, foundLatitude };
    }

    private OrderCreateDto getRandomOrder(List<I_Product> products) {
        OrderCreateDto order = new OrderCreateDto();
        order.setDiscountAmt(BigDecimal.ZERO);
        List<OrderDetailCreatedDto> details = new LinkedList<OrderDetailCreatedDto>();
        Random random = new Random();

        double nbProduct = products.size();

        int nbProductOfOrder = 2 + ((int) (random.nextDouble() * 3.0));
        List<Integer> productIndexs = new ArrayList<Integer>(nbProductOfOrder);

        for (int i = 0; i < nbProductOfOrder; i++) {
            int index = (int) (random.nextDouble() * nbProduct);
            while (productIndexs.contains(index) || index < 0 || index >= products.size()) {
                index = (int) (random.nextDouble() * nbProduct);
            }
            productIndexs.add(index);
        }

        for (Integer productIndex : productIndexs) {
            int quantity = 3 + ((int) (random.nextDouble() * 4.0));
            details.add(createOrderDetail(products.get(productIndex).getId().toString(), quantity));
        }

        order.setDetails(details);

        return order;
    }

    private OrderDetailCreatedDto createOrderDetail(String productId, int quantity) {
        OrderDetailCreatedDto dto = new OrderDetailCreatedDto();
        dto.setProductId(productId);
        dto.setQuantity(new BigDecimal(quantity));

        return dto;
    }

    private class SalesmanDataGenerate implements Runnable {

        private UserLogin userLogin;
        private Random random;
        private I_Config config;
        private SimpleDate currentDate;
        private I_User supervisor;
        private I_Distributor distributor;
        private I_User salesman;
        private List<I_Product> products;
        private Map<String, List<I_Customer>> customerBySalesman = new HashMap<>();
        private Map<String, Set<String>> routeIdsBySalesman;
        private String openPhoto;
        private String closePhoto;

        public SalesmanDataGenerate(UserLogin userLogin, Random random, I_Config config, SimpleDate currentDate,
                I_User supervisor, I_Distributor distributor, I_User salesman, List<I_Product> products,
                Map<String, List<I_Customer>> customerBySalesman, Map<String, Set<String>> routeIdsBySalesman,
                String openPhoto, String closePhoto) {
            super();
            this.userLogin = userLogin;
            this.random = random;
            this.config = config;
            this.currentDate = currentDate;
            this.supervisor = supervisor;
            this.distributor = distributor;
            this.salesman = salesman;
            this.products = products;
            this.customerBySalesman = customerBySalesman;
            this.routeIdsBySalesman = routeIdsBySalesman;
            this.openPhoto = openPhoto;
            this.closePhoto = closePhoto;
        }

        @Override
        public void run() {
            System.out.println(currentDate.getIsoDate() + " " + salesman.getFullname() + "start");

            String clientId = userLogin.getClientId();
            Set<String> routeIds = routeIdsBySalesman.get(salesman.getId());
            List<I_Customer> customers = customerRepository.getCustomersByRoutes(clientId, distributor.getId(),
                    routeIds, currentDate);

            SimpleDate time = new SimpleDate(currentDate);
            time.setHour(8);

            Set<String> customeIds = new HashSet<>();
            for (I_Customer customer : customers) {
                customeIds.add(customer.getId());

                // 90% visit
                if (random.nextDouble() < 0.6) {
                    Location location = null;
                    // 90% xac dinh vi tri
                    if (random.nextDouble() < 0.9) {

                        // 10% far
                        if (random.nextDouble() < 0.3) {
                            double[] l = generateLocation(customer.getLocation().getLongitude(),
                                    customer.getLocation().getLatitude(), config.getVisitDistanceKPI(),
                                    config.getVisitDistanceKPI() * 2);
                            location = new Location(l);
                        } else {
                            double[] l = generateLocation(customer.getLocation().getLongitude(),
                                    customer.getLocation().getLatitude(), 0.0, config.getVisitDistanceKPI());
                            location = new Location(l);
                        }

                    }

                    Visit visit = createVisit(userLogin, config, salesman, distributor, customer, location);

                    // 10% dong cua
                    if (random.nextDouble() < 0.1) {
                        visit.setClosed(true);
                        visit.setStartTime(time);
                        visit.setEndTime(time);
                        visit.setDuration(0);
                        visit.setErrorDuration(false);
                        visit.setPhoto(closePhoto);
                        visit.setVisitStatus(Visit.VISIT_STATUS_VISITED);
                    } else {
                        visit.setClosed(false);
                        visit.setStartTime(time);
                        // from 3 min -> 10 min
                        double duration = 3 + (7 * random.nextDouble());
                        time = DateTimeUtils.addMinutes(time, (int) duration);
                        visit.setEndTime(time);

                        visit.setDuration(SimpleDate.getDuration(visit.getStartTime(), visit.getEndTime()));
                        visit.setErrorDuration(visit.getDuration() < config.getVisitDurationKPI());

                        visit.setVisitStatus(Visit.VISIT_STATUS_VISITED);

                        // 20 co photo
                        if (random.nextDouble() < 0.2) {
                            visit.setPhoto(openPhoto);
                        }

                        // 80% co order
                        if (random.nextDouble() < 0.8) {
                            OrderCreateDto orderDto = getRandomOrder(products);
                            visit.setCode(
                                    codeGeneratorRepository.getOrderCode(clientId.toString(), visit.getCreatedTime()));
                            visit.setDeliveryType(Order.DELIVERY_TYPE_IMMEDIATE);

                            orderCalculationService.calculate(userLogin, customer.getDistributor().getId(), orderDto,
                                    visit, null);

                            visit.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
                            visit.setApprovedTime(time);

                            // 30% Van Sales
                            if (random.nextDouble() < 0.3) {
                                visit.setApprovedBy(new UserEmbed(salesman));
                                visit.setVanSales(true);
                            } else {
                                visit.setApprovedBy(new UserEmbed(supervisor));
                                visit.setVanSales(false);
                            }
                        }
                    }

                    visitingRepository.save(clientId, visit);

                    // lan ghe tham tiep theo sau 10 phut
                    time = DateTimeUtils.addMinutes(time, 2);
                }
            }

            int numberOrderUnplanned = (int) (3.0 * random.nextDouble());
            for (int i = 0; i < numberOrderUnplanned; i++) {
                List<I_Customer> allCustomers = customerBySalesman.get(salesman.getId());
                I_Customer customer = null;
                for (I_Customer c : allCustomers) {
                    if (!customeIds.contains(c.getId())) {
                        customer = c;
                        break;
                    }
                }

                if (customer == null) {
                    break;
                }

                Order order = new Order();
                order.setClientId(clientId);
                order.setActive(true);
                order.setDraft(false);
                order.setCreatedBy(new UserEmbed(salesman));
                order.setCustomer(new CustomerEmbed(customer));
                order.setDistributor(new CategoryEmbed(distributor));
                order.setCreatedTime(time);
                order.setCode(codeGeneratorRepository.getOrderCode(clientId.toString(), order.getCreatedTime()));

                order.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
                order.setApprovedTime(time);
                order.setApprovedBy(new UserEmbed(supervisor));

                order.setDeliveryType(Order.DELIVERY_TYPE_IMMEDIATE);

                orderCalculationService.calculate(userLogin, distributor.getId(), getRandomOrder(products), order,
                        null);

                orderPendingRepository.create(clientId, order);

                time = DateTimeUtils.addMinutes(time, 10);
            }

            System.out.println(currentDate.getIsoDate() + " " + salesman.getFullname() + "end");
        }
    }
}
