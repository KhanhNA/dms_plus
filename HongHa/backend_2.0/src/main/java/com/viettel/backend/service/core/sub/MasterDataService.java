package com.viettel.backend.service.core.sub;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.viettel.backend.event.UserDeactivationEvent;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.service.core.cache.CacheManagerService;
import com.viettel.backend.service.core.common.AbstractImportService;
import com.viettel.backend.service.core.pdto.Category;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.Customer;
import com.viettel.backend.service.core.pdto.Distributor;
import com.viettel.backend.service.core.pdto.PO;
import com.viettel.backend.service.core.pdto.Product;
import com.viettel.backend.service.core.pdto.Route;
import com.viettel.backend.service.core.pdto.Schedule;
import com.viettel.backend.service.core.pdto.ScheduleItem;
import com.viettel.backend.service.core.pdto.User;
import com.viettel.backend.service.core.pdto.UserEmbed;
import com.viettel.backend.util.PasswordUtils;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.AreaRepository;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.CustomerTypeRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.MasterDataRepository;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.UOMRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.util.DateTimeUtils;
import com.viettel.repository.common.util.LocationUtils;

import reactor.bus.EventBus;

@Service
public class MasterDataService extends AbstractImportService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private EventBus eventBus;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private DistributorRepository distributorRepository;
    
    @Autowired
    private UOMRepository uomRepository;
    
    @Autowired
    private ProductCategoryRepository productCategoryRepository;
    
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private AreaRepository areaRepository;
    
    @Autowired
    private CustomerTypeRepository customerTypeRepository;
    
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private RouteRepository routeRepository;
    
    @Autowired
    private MasterDataRepository masterDataRepository;
    
    @Autowired
    private CommonRepository commonRepository;
    
    @Autowired
    private CacheManagerService cacheManagerService;

    public void importMasterData(String clientId, InputStream inputStream) {
        Assert.notNull(clientId);
        Assert.notNull(inputStream);

        clearDatas(clientId);

        String rootId = commonRepository.getRootId();
        
        I_Config config = configRepository.getConfig(clientId);
        I_User defaultAdmin = userRepository.getDefaultAdmin(clientId);
        I_Category client = clientRepository.getById(rootId, clientId);

        List<I_User> supervisors = new LinkedList<>();
        HashMap<String, I_User> supervisorMap = null;
        
        List<I_Distributor> distributors = new LinkedList<>();
        HashMap<String, I_Distributor> distributorMap = null;
        
        List<I_User> distributorAdmins = new LinkedList<>();
        
        List<I_User> salesmen = new LinkedList<>();
        HashMap<String, I_User> salesmanMap = null;
        
        List<I_Category> uoms = new LinkedList<>();
        HashMap<String, I_Category> uomMap = null;
        
        List<I_Category> productCategories = new LinkedList<>();
        HashMap<String, I_Category> productCategoryMap = null;
        
        List<I_Product> products = new LinkedList<>();
        
        List<I_Category> areas = new LinkedList<>();
        HashMap<String, I_Category> areaMap = null;
        
        List<I_Category> customerTypes = new LinkedList<>();
        HashMap<String, I_Category> customerTypeMap = null;
        
        List<I_Route> routes = new LinkedList<>();
        HashMap<String, I_Route> routeMap = null;
        
        List<I_Customer> customers = new LinkedList<>();

        XSSFWorkbook wb = null;
        try {
            wb = new XSSFWorkbook(inputStream);
            int lastRowNum = 1;

            // SUPERVISOR
            XSSFSheet supervisorSheet = wb.getSheetAt(0);
            lastRowNum = supervisorSheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = supervisorSheet.getRow(i);
                String fullname = getStringCellValue(row.getCell(0));
                String username = getStringCellValue(row.getCell(1));

                User supervisor = new User();
                supervisor.setDraft(false);
                supervisor.setPassword(PasswordUtils.getDefaultPassword(passwordEncoder));
                supervisor.setUsername(client.getCode(), username);
                supervisor.setFullname(fullname);
                supervisor.setRole(Role.SUPERVISOR);
                initDomain(clientId, supervisor);
                supervisors.add(supervisor);
            }
            userRepository.insertBatch(clientId, supervisors);
            supervisors = userRepository.getUserByRole(clientId, Role.SUPERVISOR);
            supervisorMap = new HashMap<>();
            for (I_User supervisor : supervisors) {
                supervisorMap.put(supervisor.getUsername().toLowerCase(), supervisor);
            }
            

            // DISTRIBUTOR
            XSSFSheet distributorSheet = wb.getSheetAt(1);
            lastRowNum = distributorSheet.getLastRowNum();
            HashMap<String, String> disAdminUsernameByDistributorName = new HashMap<>();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = distributorSheet.getRow(i);
                String name = getStringCellValue(row.getCell(0));
                String supervisorUsername = getStringCellValue(row.getCell(1));
                String username = getStringCellValue(row.getCell(2));

                I_User supervisor = supervisorMap.get(supervisorUsername.toLowerCase());
                BusinessAssert.notNull(supervisor);
                UserEmbed supervisorEmbed = new UserEmbed(supervisor);

                // DISTRIBUTOR
                Distributor distributor = new Distributor();
                distributor.setName(name);
                distributor.setCode(codeGeneratorRepository.getDistributorCode(clientId.toString()));
                distributor.setSupervisor(supervisorEmbed);
                initDomain(clientId, distributor);
                distributors.add(distributor);
                
                disAdminUsernameByDistributorName.put(name.toLowerCase(), username);
            }
            distributorRepository.insertBatch(clientId, distributors);
            distributors = distributorRepository.getAll(clientId, null);
            distributorMap = new HashMap<>();
            for (I_Distributor distributor : distributors) {
                distributorMap.put(distributor.getName().toLowerCase(), distributor);
                
                // DISTRIBUTOR ADMIN
                String username = disAdminUsernameByDistributorName.get(distributor.getName().toLowerCase());
                User distributorAdmin = new User();
                distributorAdmin.setPassword(PasswordUtils.getDefaultPassword(passwordEncoder));
                distributorAdmin.setUsername(client.getCode(), username);
                distributorAdmin.setFullname(distributor.getName());
                distributorAdmin.setRole(Role.DISTRIBUTOR_ADMIN);
                CategoryEmbed distributorEmbed = new CategoryEmbed(distributor);
                distributorAdmin.setDistributor(distributorEmbed);
                initDomain(clientId, distributorAdmin);
                distributorAdmins.add(distributorAdmin);
            }
            userRepository.insertBatch(clientId, distributorAdmins);
                
            // SALESMAN
            XSSFSheet salesmanSheet = wb.getSheetAt(2);
            lastRowNum = salesmanSheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = salesmanSheet.getRow(i);
                String fullname = getStringCellValue(row.getCell(0));
                String username = getStringCellValue(row.getCell(1));
                String distributorName = getStringCellValue(row.getCell(2));

                I_Distributor distributor = distributorMap.get(distributorName.toLowerCase());
                BusinessAssert.notNull(distributor);
                CategoryEmbed distributorEmbed = new CategoryEmbed(distributor);

                User salesman = new User();
                salesman.setPassword(PasswordUtils.getDefaultPassword(passwordEncoder));
                salesman.setUsername(client.getCode(), username);
                salesman.setFullname(fullname);
                salesman.setRole(Role.SALESMAN);
                salesman.setDistributor(distributorEmbed);
                initDomain(clientId, salesman);
                salesmen.add(salesman);
            }
            userRepository.insertBatch(clientId, salesmen);
            salesmen = userRepository.getUserByRole(clientId, Role.SALESMAN);
            salesmanMap = new HashMap<>();
            for (I_User salesman : salesmen) {
                salesmanMap.put(salesman.getUsername().toLowerCase(), salesman);
            }

            // UOM
            XSSFSheet uomSheet = wb.getSheetAt(3);
            lastRowNum = uomSheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = uomSheet.getRow(i);
                String name = getStringCellValue(row.getCell(0));
                String code = getStringCellValue(row.getCell(1));

                Category uom = new Category();
                uom.setName(name);
                uom.setCode(code.toUpperCase());
                initDomain(clientId, uom);
                uoms.add(uom);
            }
            uomRepository.insertBatch(clientId, uoms);
            uoms = uomRepository.getAll(clientId, null);
            uomMap = new HashMap<>();
            for (I_Category uom : uoms) {
                uomMap.put(uom.getName().toLowerCase(), uom);
            }

            // PRODUCT CATEGORY
            XSSFSheet productCategorySheet = wb.getSheetAt(4);
            lastRowNum = productCategorySheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = productCategorySheet.getRow(i);
                String name = getStringCellValue(row.getCell(0));

                Category productCategory = new Category();
                productCategory.setName(name);
                initDomain(clientId, productCategory);
                productCategories.add(productCategory);
            }
            productCategoryRepository.insertBatch(clientId, productCategories);
            productCategories = productCategoryRepository.getAll(clientId, null);
            productCategoryMap = new HashMap<>();
            for (I_Category productCategory : productCategories) {
                productCategoryMap.put(productCategory.getName().toLowerCase(), productCategory);
            }

            // PRODUCT
            XSSFSheet productSheet = wb.getSheetAt(5);
            lastRowNum = productSheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = productSheet.getRow(i);
                String name = getStringCellValue(row.getCell(0));
                String code = getStringCellValue(row.getCell(1));
                String uomName = getStringCellValue(row.getCell(2));
                String productCategoryName = getStringCellValue(row.getCell(3));
                BigDecimal price = getNumericCellValue(row.getCell(4), 0);
                BigDecimal productivity = getNumericCellValue(row.getCell(5), 2);
                String description = getStringCellValue(row.getCell(6));

                I_Category uom = uomMap.get(uomName.toLowerCase());
                BusinessAssert.notNull(uom);
                CategoryEmbed uomEmbed = new CategoryEmbed(uom);

                I_Category productCategory = productCategoryMap.get(productCategoryName.toLowerCase());
                BusinessAssert.notNull(productCategory);
                CategoryEmbed productCategoryEmbed = new CategoryEmbed(productCategory);

                Product product = new Product();
                product.setName(name);
                product.setCode(code.toUpperCase());
                product.setUom(uomEmbed);
                product.setProductCategory(productCategoryEmbed);
                product.setPrice(price);
                product.setProductivity(productivity);
                product.setPhoto(config.getProductPhoto());
                product.setDescription(description);
                initDomain(clientId, product);
                products.add(product);
            }
            productRepository.insertBatch(clientId, products);

            // AREA
            XSSFSheet areaSheet = wb.getSheetAt(6);
            lastRowNum = areaSheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = areaSheet.getRow(i);
                String name = getStringCellValue(row.getCell(0));
                String distributorName = getStringCellValue(row.getCell(1));

                I_Distributor distributor = distributorMap.get(distributorName.toLowerCase());
                BusinessAssert.notNull(distributor);
                CategoryEmbed distributorEmbed = new CategoryEmbed(distributor);

                Category area = new Category();
                area.setName(name);
                area.setDistributor(distributorEmbed);
                initDomain(clientId, area);
                areas.add(area);
            }
            areaRepository.insertBatch(clientId, areas);
            areas = areaRepository.getAll(clientId, null);
            areaMap = new HashMap<>();
            for (I_Category area : areas) {
                areaMap.put(area.getName().toLowerCase(), area);
            }

            // CUSTOMER TYPE
            XSSFSheet customerTypeSheet = wb.getSheetAt(7);
            lastRowNum = customerTypeSheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = customerTypeSheet.getRow(i);
                String name = getStringCellValue(row.getCell(0));

                Category customerType = new Category();
                customerType.setName(name);
                initDomain(clientId, customerType);
                customerTypes.add(customerType);
            }
            customerTypeRepository.insertBatch(clientId, customerTypes);
            customerTypes = customerTypeRepository.getAll(clientId, null);
            customerTypeMap = new HashMap<>();
            for (I_Category customerType : customerTypes) {
                customerTypeMap.put(customerType.getName().toLowerCase(), customerType);
            }

            // ROUTE
            XSSFSheet routeSheet = wb.getSheetAt(8);
            lastRowNum = routeSheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = routeSheet.getRow(i);
                String name = getStringCellValue(row.getCell(0));
                String distributorName = getStringCellValue(row.getCell(1));
                String salesmanUsername = getStringCellValue(row.getCell(2));

                I_Distributor distributor = distributorMap.get(distributorName.toLowerCase());
                BusinessAssert.notNull(distributor);
                CategoryEmbed distributorEmbed = new CategoryEmbed(distributor);

                UserEmbed salesmanEmbed = null;
                if (salesmanUsername != null) {
                    I_User salesman = salesmanMap.get(salesmanUsername.toLowerCase());
                    BusinessAssert.notNull(salesman);
                    salesmanEmbed = new UserEmbed(salesman);
                }

                Route route = new Route();
                route.setName(name);
                route.setDistributor(distributorEmbed);
                if (salesmanEmbed != null) {
                    route.setSalesman(salesmanEmbed);
                }
                initDomain(clientId, route);
                routes.add(route);
            }
            routeRepository.insertBatch(clientId, routes);
            routes = routeRepository.getAll(clientId, (String) null);
            routeMap = new HashMap<>();
            for (I_Route route : routes) {
                routeMap.put(route.getName().toLowerCase(), route);
            }
            

            // CUSTOMER
            XSSFSheet customerSheet = wb.getSheetAt(9);
            lastRowNum = customerSheet.getLastRowNum();
            for (int i = 1; i <= lastRowNum; i++) {
                XSSFRow row = customerSheet.getRow(i);
                String name = getStringCellValue(row.getCell(0));
                String distributorName = getStringCellValue(row.getCell(1));
                String areaName = getStringCellValue(row.getCell(2));
                String customerTypeName = getStringCellValue(row.getCell(3));
                String mobile = getStringCellValue(row.getCell(4));
                String phone = getStringCellValue(row.getCell(5));
                String contact = getStringCellValue(row.getCell(6));
                String email = getStringCellValue(row.getCell(7));
                BigDecimal latitude = getNumericCellValue(row.getCell(8), 10);
                BigDecimal longitude = getNumericCellValue(row.getCell(9), 10);
                String routeName = getStringCellValue(row.getCell(10));

                I_Distributor distributor = distributorMap.get(distributorName.toLowerCase());
                BusinessAssert.notNull(distributor);
                CategoryEmbed distributorEmbed = new CategoryEmbed(distributor);

                I_Category area = areaMap.get(areaName.toLowerCase());
                BusinessAssert.notNull(area);
                CategoryEmbed areaEmbed = new CategoryEmbed(area);

                I_Category customerType = customerTypeMap.get(customerTypeName.toLowerCase());
                BusinessAssert.notNull(customerType);
                CategoryEmbed customerTypeEmbed = new CategoryEmbed(customerType);

                Customer customer = new Customer();
                customer.setName(name);
                customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
                customer.setCreatedTime(DateTimeUtils.getCurrentTime());
                customer.setCreatedBy(new UserEmbed(defaultAdmin));
                customer.setCode(codeGeneratorRepository.getCustomerCode(clientId.toString()));
                customer.setMobile(mobile);
                customer.setDistributor(distributorEmbed);
                customer.setCustomerType(customerTypeEmbed);
                customer.setArea(areaEmbed);
                customer.setPhone(phone);
                customer.setContact(contact);
                customer.setEmail(email);
                if (longitude != null && latitude != null && LocationUtils.checkLocationValid(longitude.doubleValue(), latitude.doubleValue())){
                    customer.setLocation(new Location(latitude.doubleValue(), longitude.doubleValue()));
                } else {
                    customer.setLocation(new Location(config.getLocation().getLatitude(), config.getLocation().getLongitude()));
                }

                if (routeName != null) {
                    I_Route route = routeMap.get(routeName.toLowerCase());
                    BusinessAssert.notNull(route);

                    Schedule schedule = new Schedule();
                    schedule.setRouteId(route.getId());
                    ScheduleItem item = new ScheduleItem();
                    item.setMonday(!StringUtils.isNullOrEmpty(getStringCellValue(row.getCell(11))));
                    item.setTuesday(!StringUtils.isNullOrEmpty(getStringCellValue(row.getCell(12))));
                    item.setWednesday(!StringUtils.isNullOrEmpty(getStringCellValue(row.getCell(13))));
                    item.setThursday(!StringUtils.isNullOrEmpty(getStringCellValue(row.getCell(14))));
                    item.setFriday(!StringUtils.isNullOrEmpty(getStringCellValue(row.getCell(15))));
                    item.setSaturday(!StringUtils.isNullOrEmpty(getStringCellValue(row.getCell(16))));
                    item.setSunday(!StringUtils.isNullOrEmpty(getStringCellValue(row.getCell(17))));
                    schedule.setItem(item);
                    
                    customer.setSchedule(schedule);
                }
                initDomain(clientId, customer);
                customers.add(customer);
            }
            customerRepository.insertBatch(clientId, customers);
            
        } catch (IOException e) {
            logger.error("error when read excel for import customer", e);
            throw new UnsupportedOperationException(e);
        } finally {
            IOUtils.closeQuietly(wb);
        }
        
        cacheManagerService.init(clientId);
    }

    private void initDomain(String clientId, PO domain) {
        domain.setClientId(clientId);
        domain.setActive(true);
        domain.setDraft(false);
    }
    
    private void clearDatas(String clientId) {
        List<I_User> users = userRepository.getAll(clientId);
        for (I_User user : users) {
            eventBus.notify(UserDeactivationEvent.EVENT_NAME, new UserDeactivationEvent(user));
        }
        
        masterDataRepository.deleteAllDatas(clientId);
        cacheManagerService.clearByClient(clientId);
    }

}
