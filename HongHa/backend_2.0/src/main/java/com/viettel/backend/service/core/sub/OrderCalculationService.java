package com.viettel.backend.service.core.sub;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.order.OrderCreateDto;
import com.viettel.backend.dto.order.OrderCreateDto.OrderDetailCreatedDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pdto.Order;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.PromotionRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_OrderDetail;
import com.viettel.repository.common.domain.data.I_OrderProduct;
import com.viettel.repository.common.domain.data.I_OrderPromotion;
import com.viettel.repository.common.domain.data.I_OrderPromotionDetail;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.promotion.I_Promotion;

@Service
public class OrderCalculationService extends AbstractService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PromotionRepository promotionRepository;

    @Autowired
    private PromotionCalculationService promotionCalculationService;

    public Order calculate(UserLogin userLogin, String distributorId, OrderCreateDto dto, Order order,
            Map<String, BigDecimal> priceList) {
        BusinessAssert.notNull(order, "order null");
        BusinessAssert.notNull(dto, "order dto null");
        BusinessAssert.notEmpty(dto.getDetails(), "order detail dto empty");

        final Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);

        order.setDiscountPercentage(dto.getDiscountPercentage());
        order.setDiscountAmt(dto.getDiscountAmt());

        BigDecimal subTotal = BigDecimal.ZERO;
        BigDecimal discountAmt = BigDecimal.ZERO;
        BigDecimal promotionAmt = BigDecimal.ZERO;
        BigDecimal grandTotal;

        BigDecimal quantity = BigDecimal.ZERO;
        BigDecimal productivity = BigDecimal.ZERO;

        List<I_OrderDetail> details = new ArrayList<I_OrderDetail>(dto.getDetails().size());
        for (OrderDetailCreatedDto detailDto : dto.getDetails()) {
            I_Product product = productMap.get(detailDto.getProductId());

            BusinessAssert.notNull(product);

            I_OrderDetail detail = createOrderDetail(product, priceList, detailDto.getQuantity());

            subTotal = subTotal.add(detail.getAmount());

            details.add(detail);

            quantity = quantity.add(detailDto.getQuantity());
            productivity = productivity.add(detail.getProductivity());
        }

        order.setDetails(details);

        order.setQuantity(quantity);
        order.setProductivity(productivity);

        if (userLogin.hasModule(Module.PROMOTION)) {
            List<I_Promotion> promotions = promotionRepository.getPromotionsAvailableToday(userLogin.getClientId(),
                    Collections.singleton(distributorId));

            List<I_OrderPromotion> orderPromotions = promotionCalculationService.calculate(promotions, order,
                    productMap);

            if (orderPromotions != null && !orderPromotions.isEmpty()) {
                order.setPromotions(orderPromotions);

                for (I_OrderPromotion orderPromotion : orderPromotions) {
                    for (I_OrderPromotionDetail orderPromotionDetail : orderPromotion.getDetails()) {
                        if (orderPromotionDetail.getReward().getAmount() != null) {
                            promotionAmt = promotionAmt.add(orderPromotionDetail.getReward().getAmount());
                        }
                    }
                }
            }
        }

        if (subTotal.subtract(promotionAmt).signum() == -1) {
            throw new UnsupportedOperationException("error when calculate promotion");
        }

        if (dto.getDiscountPercentage() != null) {
            order.setDiscountPercentage(dto.getDiscountPercentage());
            discountAmt = subTotal.subtract(promotionAmt).multiply(dto.getDiscountPercentage())
                    .divide(new BigDecimal(100), 0, RoundingMode.DOWN);
        } else if (dto.getDiscountAmt() != null) {
            discountAmt = dto.getDiscountAmt();
        }

        if (subTotal.subtract(promotionAmt).compareTo(discountAmt) < 0) {
            discountAmt = subTotal.subtract(promotionAmt);
        }
        
        grandTotal = subTotal.subtract(promotionAmt).subtract(discountAmt);

        order.setSubTotal(subTotal);
        order.setPromotionAmt(promotionAmt);
        order.setDiscountAmt(discountAmt);
        order.setGrandTotal(grandTotal);

        return order;
    }

    public List<I_OrderPromotion> getOrderPromotions(UserLogin userLogin, String distributorId, OrderCreateDto dto,
            Map<String, BigDecimal> priceList) {
        BusinessAssert.notNull(dto, "order dto null");
        BusinessAssert.notEmpty(dto.getDetails(), "order detail dto empty");

        final Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);
        List<I_Promotion> promotions = promotionRepository.getPromotionsAvailableToday(userLogin.getClientId(),
                Collections.singleton(distributorId));

        List<I_OrderDetail> details = new ArrayList<I_OrderDetail>(dto.getDetails().size());
        for (OrderDetailCreatedDto detailDto : dto.getDetails()) {
            I_Product product = productMap.get(detailDto.getProductId());
            BusinessAssert.notNull(product, BusinessExceptionCode.PRODUCT_NOT_FOUND, "product not found");

            details.add(createOrderDetail(product, priceList, detailDto.getQuantity()));
        }

        Order order = new Order();
        order.setDetails(details);

        return promotionCalculationService.calculate(promotions, order, productMap);
    }

    private I_OrderDetail createOrderDetail(final I_Product product, Map<String, BigDecimal> priceList,
            final BigDecimal quantity) {
        BigDecimal price = product.getPrice();
        if (priceList != null && priceList.containsKey(product.getId())) {
            if (priceList.get(product.getId()) != null
                    && priceList.get(product.getId()).compareTo(BigDecimal.ZERO) >= 0) {
                price = priceList.get(product.getId());
            }
        }
        final BigDecimal priceFinal = price;

        return new I_OrderDetail() {

            private static final long serialVersionUID = 1769955326787767414L;

            @Override
            public BigDecimal getQuantity() {
                return quantity;
            }

            @Override
            public I_OrderProduct getProduct() {
                return new I_OrderProduct() {

                    private static final long serialVersionUID = -6613104059195500026L;

                    @Override
                    public String getId() {
                        return product.getId();
                    }

                    @Override
                    public String getName() {
                        return product.getName();
                    }

                    @Override
                    public String getCode() {
                        return product.getCode();
                    }

                    @Override
                    public I_CategoryEmbed getUom() {
                        return product.getUom();
                    }

                    @Override
                    public I_CategoryEmbed getProductCategory() {
                        return product.getProductCategory();
                    }

                    @Override
                    public BigDecimal getProductivity() {
                        return product.getProductivity();
                    }

                    @Override
                    public BigDecimal getPrice() {
                        return priceFinal;
                    }
                };
            }
        };
    }

}
