package com.viettel.backend.service.core.sub;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderDetail;
import com.viettel.repository.common.domain.data.I_OrderPromotion;
import com.viettel.repository.common.domain.data.I_OrderPromotionDetail;
import com.viettel.repository.common.domain.data.I_OrderPromotionReward;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductEmbed;
import com.viettel.repository.common.domain.promotion.I_Promotion;
import com.viettel.repository.common.domain.promotion.I_PromotionDetail;

@Service
public class PromotionCalculationService {
    
    public static class PromotionDetailType {
        public static final int C_PRODUCT_QTY_R_PERCENTAGE_AMT = 0;
        public static final int C_PRODUCT_QTY_R_PRODUCT_QTY = 1;
        public static final Set<Integer> TYPES = Collections.unmodifiableSet(new HashSet<Integer>(
                Arrays.asList(C_PRODUCT_QTY_R_PERCENTAGE_AMT, C_PRODUCT_QTY_R_PRODUCT_QTY)));
    }

    public List<I_OrderPromotion> calculate(List<I_Promotion> promotions, I_Order order, Map<String, I_Product> productMap) {
        Assert.notNull(order);
        Assert.notNull(order.getDetails());

        if (promotions == null || promotions.isEmpty()) {
            return null;
        }

        // TAO MAP ORDER DETAIL THEO PRODUCT ID
        Map<String, I_OrderDetail> mapOrderDetails = new HashMap<String, I_OrderDetail>(order.getDetails().size());
        for (I_OrderDetail orderDetail : order.getDetails()) {
            mapOrderDetails.put(orderDetail.getProduct().getId(), orderDetail);
        }

        // CHAY QUA TUNG PROMOTION
        List<I_OrderPromotion> promotionResults = new LinkedList<I_OrderPromotion>();
        for (I_Promotion promotion : promotions) {

            List<I_PromotionDetail> promotionDetails = promotion.getDetails();
            if (promotionDetails == null || promotionDetails.isEmpty()) {
                continue;
            }

            List<I_OrderPromotionDetail> detailResults = new LinkedList<I_OrderPromotionDetail>();

            // CHAY QUA TUNG PROMOTION DETAIL
            for (I_PromotionDetail promotionDetail : promotionDetails) {
                String productID = promotionDetail.getCondition().getProductId();
                if (mapOrderDetails.containsKey(productID)) {
                    I_OrderDetail orderDetail = mapOrderDetails.get(productID);
                    I_OrderPromotionDetail promotionDetailResult = calculateDetail(orderDetail, promotionDetail, productMap);
                    if (promotionDetailResult != null) {
                        detailResults.add(promotionDetailResult);

                        // TODO verify 1 san pham hien tai chay duoc nhieu khuyen mai
                        // mapOrderDetails.remove(productID);

                        if (mapOrderDetails.isEmpty()) {
                            // TAT CA CAC SAN PHAM DEU DEU DA CO PROMOTION
                            break;
                        }
                    }
                }

            }

            if (!detailResults.isEmpty()) {
                promotionResults.add(createOrderPromotion(promotion, detailResults));
            }

            if (mapOrderDetails.isEmpty()) {
                // TAT CA CAC SAN PHAM DEU DEU DA CO PROMOTION
                break;
            }
        }

        if (!promotionResults.isEmpty()) {
            return promotionResults;
        }

        return null;
    }

    private I_OrderPromotionDetail calculateDetail(I_OrderDetail orderDetail, I_PromotionDetail promotionDetail, Map<String, I_Product> productMap) {

        if (!orderDetail.getProduct().getId().equals(promotionDetail.getCondition().getProductId())) {
            return null;
        }

        I_OrderPromotionReward rewardResult = null;

        // Mua lon hon mot so luong san pham duoc giam tien
        if (promotionDetail.getType() == PromotionDetailType.C_PRODUCT_QTY_R_PERCENTAGE_AMT) {
            if (orderDetail.getQuantity().compareTo(promotionDetail.getCondition().getQuantity()) < 0) {
                return null;
            }

            BigDecimal promotionAmount = orderDetail.getQuantity().multiply(orderDetail.getProduct().getPrice())
                    .multiply(promotionDetail.getReward().getPercentage())
                    .divide(new BigDecimal(100), 0, RoundingMode.DOWN);

            rewardResult = createOrderPromotionReward(null, null, null, promotionAmount);
        }
        // Mua X san pham tang Y san pham
        else if (promotionDetail.getType() == PromotionDetailType.C_PRODUCT_QTY_R_PRODUCT_QTY) {
            if (orderDetail.getQuantity().compareTo(promotionDetail.getCondition().getQuantity()) < 0) {
                return null;
            }
            
            
            I_Product product = null;
            if (promotionDetail.getReward().getProductId() != null) {
                product = productMap.get(promotionDetail.getReward().getProductId());
                if (product == null) {
                    return null;
                }
            }
            
            String productText = promotionDetail.getReward().getProductText();
            BigDecimal quantity = orderDetail.getQuantity()
                    .divide(promotionDetail.getCondition().getQuantity(), 0, RoundingMode.DOWN)
                    .multiply(promotionDetail.getReward().getQuantity());
            
            rewardResult = createOrderPromotionReward(quantity, productText, product, null);
        }

        if (rewardResult != null) {
            I_Product conditionProduct = productMap.get(promotionDetail.getCondition().getProductId());
            return createOrderPromotionDetail(promotionDetail.getSeqNo(), conditionProduct.getName(), rewardResult);
        }

        return null;
    }
    
    private I_OrderPromotion createOrderPromotion(final I_Promotion promotion, final List<I_OrderPromotionDetail> details) {
        return new I_OrderPromotion() {
            
            private static final long serialVersionUID = 218064141797221170L;

            @Override
            public String getId() {
                return promotion.getId();
            }
            
            @Override
            public String getName() {
                return promotion.getName();
            }
            
            @Override
            public String getCode() {
                return promotion.getCode();
            }
            
            @Override
            public List<I_OrderPromotionDetail> getDetails() {
                return details;
            }
        };
    }
    
    private I_OrderPromotionDetail createOrderPromotionDetail(final int promotionDetailSeqNo,
            final String conditionProductName, final I_OrderPromotionReward reward) {
        return new I_OrderPromotionDetail() {

            private static final long serialVersionUID = -3311133482530433531L;

            @Override
            public int getSeqNo() {
                return promotionDetailSeqNo;
            }

            @Override
            public I_OrderPromotionReward getReward() {
                return reward;
            }

            @Override
            public String getConditionProductName() {
                return conditionProductName;
            }
        };
    }
    
    private I_OrderPromotionReward createOrderPromotionReward(final BigDecimal quantity, final String productText,
            final I_ProductEmbed product, final BigDecimal amount) {
        return new I_OrderPromotionReward() {

            private static final long serialVersionUID = -4167120778335463415L;

            @Override
            public BigDecimal getQuantity() {
                return quantity;
            }

            @Override
            public String getProductText() {
                return productText;
            }

            @Override
            public I_ProductEmbed getProduct() {
                return product;
            }

            @Override
            public BigDecimal getAmount() {
                return amount;
            }
        };
    }

}
