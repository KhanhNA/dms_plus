package com.viettel.backend.service.core.sub;

import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_FeedbackHeader;
import com.viettel.repository.common.domain.data.I_OrderHeader;

public interface WebNotificationService {
    
    public static final String ACTION_ORDER_ADD = "add";
    public static final String ACTION_ORDER_APPROVE = "approve";
    public static final String ACTION_ORDER_REJECT = "reject";
    
    public static final String ACTION_FEEDBACK_ADD = "add";
    public static final String ACTION_FEEDBACK_READ = "read";

    public static final String ACTION_CUSTOMER_ADD = "add";
    public static final String ACTION_CUSTOMER_APPROVE = "approve";
    public static final String ACTION_CUSTOMER_REJECT = "reject";

    public void notifyChangedOrder(UserLogin userLogin, I_OrderHeader order, String action);

    public void notifyChangedFeedback(UserLogin userLogin, I_FeedbackHeader feedback, String action);

    public void notifyChangedCustomer(UserLogin userLogin, I_Customer customer, String action);

}
