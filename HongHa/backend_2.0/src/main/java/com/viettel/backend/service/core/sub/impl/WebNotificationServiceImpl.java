package com.viettel.backend.service.core.sub.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.customer.CustomerListDto;
import com.viettel.backend.dto.feedback.FeedbackListDto;
import com.viettel.backend.dto.order.OrderSimpleDto;
import com.viettel.backend.event.WebsocketSendEvent;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.sub.WebNotificationService;
import com.viettel.repository.common.CustomerPendingRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.FeedbackRepository;
import com.viettel.repository.common.OrderPendingRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_FeedbackHeader;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;

import reactor.bus.EventBus;

@Component
@Async
public class WebNotificationServiceImpl implements WebNotificationService {

	private static final String QUEUE = "/queue/notify";

	private static final String TYPE_ORDER = "order";
	private static final String TYPE_CUSTOMER = "customer";
	private static final String TYPE_FEEDBACK = "feedback";

	@Autowired
	private EventBus eventBus;

	@Autowired
	private OrderPendingRepository orderPendingRepository;

	@Autowired
	private CustomerPendingRepository customerPendingRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private FeedbackRepository feedbackRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Override
	public void notifyChangedOrder(UserLogin userLogin, I_OrderHeader order, String action) {

		String senderId = userLogin.getUserId();
		String distributorId = order.getDistributor().getId();
		I_Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);

		// Notify distributor
		List<I_User> distributorAdmins = userRepository.getDistributorAdmins(userLogin.getClientId(),
				Arrays.asList(distributorId));

		if (!CollectionUtils.isEmpty(distributorAdmins)) {
			long pendingOrderCount = orderPendingRepository.countPendingOrdersByDistributors(userLogin.getClientId(),
					Collections.singletonList(distributorId));

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", pendingOrderCount);
			data.put("order", new OrderSimpleDto(order));
			for (I_User user : distributorAdmins) {
				notifyUser(senderId, user.getId(), TYPE_ORDER, data);
			}
		}

		// Notify supervisor
		String supervisorId = distributor.getSupervisor().getId();
		List<I_Distributor> distributorOfSupervisor = distributorRepository
				.getDistributorsBySupervisors(userLogin.getClientId(), Collections.singletonList(supervisorId));
		if (!CollectionUtils.isEmpty(distributorOfSupervisor)) {
			Set<String> distributorIds = getIdSet(distributorOfSupervisor);
			long pendingOrderCount = orderPendingRepository.countPendingOrdersByDistributors(userLogin.getClientId(),
					distributorIds);

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", pendingOrderCount);
			data.put("order", new OrderSimpleDto(order));

			notifyUser(senderId, supervisorId, TYPE_ORDER, data);
		}

		// Notify advance supervisor
		if (distributor.getRegion() != null) {
			List<String> notifyAdvanceSupervisorIds = new ArrayList<>();
			String regionId = distributor.getRegion().getId();
			List<String> ancestorRegionIds = new ArrayList<>();
			ancestorRegionIds =	getAncestorRegions(userLogin.getClientId(), regionId, ancestorRegionIds);
			
			List<I_User> advanceSupervisors =  userRepository.getUserByRole(userLogin.getClientId(), Role.ADVANCE_SUPERVISOR);
			for (Iterator<I_User> iter = advanceSupervisors.listIterator();iter.hasNext();){
				I_User user = iter.next();
				List<I_Region> regions = userRepository.getRegionsOfManager(userLogin.getClientId(), user.getId());
				for (I_Region region : regions){
					if(ancestorRegionIds.contains(region.getId())){
						notifyAdvanceSupervisorIds.add(user.getId());
						break;
					}
				}
			}
			
			if(!CollectionUtils.isEmpty(notifyAdvanceSupervisorIds)){
				for (String advanceSupervisorId : notifyAdvanceSupervisorIds){
					List<I_Distributor> distributors = userRepository.getDistributorsOfManager(userLogin.getClientId(), advanceSupervisorId);
					Set<String> distributorIds = getIdSet(distributors);
					long pendingOrderCount = orderPendingRepository.countPendingOrdersByDistributors(userLogin.getClientId(),
							distributorIds);
					HashMap<String, Serializable> data = new HashMap<>();
					data.put("action", action);
					data.put("count", pendingOrderCount);
					data.put("order", new OrderSimpleDto(order));

					notifyUser(senderId, advanceSupervisorId, TYPE_ORDER, data);
				}
				
				
			}
		}

		// Notify administrator
		List<I_Distributor> distributorOfAdministrators = distributorRepository.getAll(userLogin.getClientId(), null);
		if (!CollectionUtils.isEmpty(distributorOfAdministrators)) {
			long pendingOrderCount = orderPendingRepository.countPendingOrdersByDistributors(userLogin.getClientId(),
					getIdSet(distributorOfAdministrators));

			List<I_User> administrators = userRepository.getUserByRole(userLogin.getClientId(), Role.ADMIN);

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", pendingOrderCount);
			data.put("order", new OrderSimpleDto(order));

			for (I_User user : administrators) {
				notifyUser(senderId, user.getId(), TYPE_ORDER, data);
			}
		}
	}

	List<String> getAncestorRegions(String clientId, String regionId, List<String> regionIds) {
		if (regionIds == null)
			return null;
		regionIds.add(regionId);
		I_Region region = regionRepository.getById(clientId, false, true, regionId);
		if (region != null && region.getParent() != null) {
			regionIds = getAncestorRegions(clientId, region.getParent().getId(), regionIds);
		}
		return regionIds;
	}

	@Override
	public void notifyChangedFeedback(UserLogin userLogin, I_FeedbackHeader feedback, String action) {
		String senderId = userLogin.getUserId();
		String distributorId = feedback.getDistributor().getId();
		I_Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);

		// Notify distributor
		List<I_User> distributorAdmins = userRepository.getDistributorAdmins(userLogin.getClientId(),
				Arrays.asList(distributorId));

		if (!CollectionUtils.isEmpty(distributorAdmins)) {
			long unreadFeedbackCount = feedbackRepository.countFeedbackByDistributors(userLogin.getClientId(),
					Collections.singletonList(distributorId), false);

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", unreadFeedbackCount);
			data.put("feedback", new FeedbackListDto(feedback));
			for (I_User user : distributorAdmins) {
				notifyUser(senderId, user.getId(), TYPE_FEEDBACK, data);
			}
		}

		// Notify supervisor
		String supervisorId = distributor.getSupervisor().getId();
		List<I_Distributor> distributorOfSupervisor = distributorRepository
				.getDistributorsBySupervisors(userLogin.getClientId(), Collections.singletonList(supervisorId));
		if (!CollectionUtils.isEmpty(distributorOfSupervisor)) {
			Set<String> distributorIds = getIdSet(distributorOfSupervisor);
			long unreadFeedbackCount = feedbackRepository.countFeedbackByDistributors(userLogin.getClientId(),
					distributorIds, false);

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", unreadFeedbackCount);
			data.put("feedback", new FeedbackListDto(feedback));

			notifyUser(senderId, supervisorId, TYPE_FEEDBACK, data);
		}
		
		// Notify advance supervisor
		if (distributor.getRegion() != null) {
			List<String> notifyAdvanceSupervisorIds = new ArrayList<>();
			String regionId = distributor.getRegion().getId();
			List<String> ancestorRegionIds = new ArrayList<>();
			ancestorRegionIds =	getAncestorRegions(userLogin.getClientId(), regionId, ancestorRegionIds);
			
			List<I_User> advanceSupervisors =  userRepository.getUserByRole(userLogin.getClientId(), Role.ADVANCE_SUPERVISOR);
			for (Iterator<I_User> iter = advanceSupervisors.listIterator();iter.hasNext();){
				I_User user = iter.next();
				List<I_Region> regions = userRepository.getRegionsOfManager(userLogin.getClientId(), user.getId());
				for (I_Region region : regions){
					if(ancestorRegionIds.contains(region.getId())){
						notifyAdvanceSupervisorIds.add(user.getId());
						break;
					}
				}
			}
			
			if(!CollectionUtils.isEmpty(notifyAdvanceSupervisorIds)){
				for (String advanceSupervisorId : notifyAdvanceSupervisorIds){
					List<I_Distributor> distributors = userRepository.getDistributorsOfManager(userLogin.getClientId(), advanceSupervisorId);
					Set<String> distributorIds = getIdSet(distributors);
					long unreadFeedbackCount = feedbackRepository.countFeedbackByDistributors(userLogin.getClientId(),
							distributorIds,false);
					HashMap<String, Serializable> data = new HashMap<>();
					data.put("action", action);
					data.put("count", unreadFeedbackCount);
					data.put("order", new FeedbackListDto(feedback));

					notifyUser(senderId, advanceSupervisorId, TYPE_FEEDBACK, data);
				}
			}
		}

		// Notify administrator
		List<I_Distributor> distributorOfAdministrators = distributorRepository.getAll(userLogin.getClientId(), null);
		if (!CollectionUtils.isEmpty(distributorOfAdministrators)) {
			long unreadFeedbackCount = feedbackRepository.countFeedbackByDistributors(userLogin.getClientId(),
					getIdSet(distributorOfAdministrators), false);

			List<I_User> administrators = userRepository.getUserByRole(userLogin.getClientId(), Role.ADMIN);

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", unreadFeedbackCount);
			data.put("feedback", new FeedbackListDto(feedback));

			for (I_User user : administrators) {
				notifyUser(senderId, user.getId(), TYPE_FEEDBACK, data);
			}
		}
	}

	@Override
	public void notifyChangedCustomer(UserLogin userLogin, I_Customer customer, String action) {
		String senderId = userLogin.getUserId();
		String distributorId = customer.getDistributor().getId();
		I_Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);

		// Notify distributor
		List<I_User> distributorAdmins = userRepository.getDistributorAdmins(userLogin.getClientId(),
				Arrays.asList(distributorId));

		if (!CollectionUtils.isEmpty(distributorAdmins)) {
			long pendingCustomerCount = customerPendingRepository.countPendingCustomersByDistributors(
					userLogin.getClientId(), Collections.singletonList(distributorId));

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", pendingCustomerCount);
			data.put("customer", new CustomerListDto(customer));
			for (I_User user : distributorAdmins) {
				notifyUser(senderId, user.getId(), TYPE_CUSTOMER, data);
			}
		}

		// Notify supervisor
		String supervisorId = distributor.getSupervisor().getId();
		List<I_Distributor> distributorOfSupervisor = distributorRepository
				.getDistributorsBySupervisors(userLogin.getClientId(), Collections.singletonList(supervisorId));
		if (!CollectionUtils.isEmpty(distributorOfSupervisor)) {
			long pendingCustomerCount = customerPendingRepository
					.countPendingCustomersByDistributors(userLogin.getClientId(), getIdSet(distributorOfSupervisor));

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", pendingCustomerCount);
			data.put("customer", new CustomerListDto(customer));
			notifyUser(senderId, supervisorId, TYPE_CUSTOMER, data);
		}
		// Notify advance supervisor
		if (distributor.getRegion() != null) {
			List<String> notifyAdvanceSupervisorIds = new ArrayList<>();
			String regionId = distributor.getRegion().getId();
			List<String> ancestorRegionIds = new ArrayList<>();
			ancestorRegionIds =	getAncestorRegions(userLogin.getClientId(), regionId, ancestorRegionIds);
			
			List<I_User> advanceSupervisors =  userRepository.getUserByRole(userLogin.getClientId(), Role.ADVANCE_SUPERVISOR);
			for (Iterator<I_User> iter = advanceSupervisors.listIterator();iter.hasNext();){
				I_User user = iter.next();
				List<I_Region> regions = userRepository.getRegionsOfManager(userLogin.getClientId(), user.getId());
				for (I_Region region : regions){
					if(ancestorRegionIds.contains(region.getId())){
						notifyAdvanceSupervisorIds.add(user.getId());
						break;
					}
				}
			}
			
			if(!CollectionUtils.isEmpty(notifyAdvanceSupervisorIds)){
				for (String advanceSupervisorId : notifyAdvanceSupervisorIds){
					List<I_Distributor> distributors = userRepository.getDistributorsOfManager(userLogin.getClientId(), advanceSupervisorId);
					Set<String> distributorIds = getIdSet(distributors);
					long pendingCustomerCount = customerPendingRepository
							.countPendingCustomersByDistributors(userLogin.getClientId(),
							distributorIds);
					
					HashMap<String, Serializable> data = new HashMap<>();
					data.put("action", action);
					data.put("count", pendingCustomerCount);
					data.put("order", new CustomerListDto(customer));

					notifyUser(senderId, advanceSupervisorId, TYPE_CUSTOMER, data);
				}
			}
		}
		// Notify administrator
		List<I_Distributor> distributorOfAdministrators = distributorRepository.getAll(userLogin.getClientId(), null);
		if (!CollectionUtils.isEmpty(distributorOfAdministrators)) {
			long pendingCustomerCount = customerPendingRepository.countPendingCustomersByDistributors(
					userLogin.getClientId(), getIdSet(distributorOfAdministrators));

			List<I_User> administrators = userRepository.getUserByRole(userLogin.getClientId(), Role.ADMIN);

			HashMap<String, Serializable> data = new HashMap<>();
			data.put("action", action);
			data.put("count", pendingCustomerCount);
			data.put("customer", new CustomerListDto(customer));

			for (I_User user : administrators) {
				notifyUser(senderId, user.getId(), TYPE_CUSTOMER, data);
			}
		}
	}

	protected void notifyUser(String senderId, String recipientId, String type, Serializable data) {
		eventBus.notify(WebsocketSendEvent.EVENT_NAME,
				new WebsocketSendEvent(senderId.toString(), recipientId.toString(), QUEUE, type, data));
	}

	protected Set<String> getIdSet(Collection<? extends I_PO> pos) {
		if (pos == null) {
			return null;
		}
		if (pos.isEmpty()) {
			return Collections.emptySet();
		}

		Set<String> idList = new HashSet<String>(pos.size());
		for (I_PO po : pos) {
			idList.add(po.getId());
		}
		return idList;
	}

}
