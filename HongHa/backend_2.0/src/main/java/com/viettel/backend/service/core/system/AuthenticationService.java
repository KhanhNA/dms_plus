package com.viettel.backend.service.core.system;

import com.viettel.backend.dto.system.ChangePasswordDto;
import com.viettel.backend.dto.system.UserInfoDto;
import com.viettel.backend.dto.system.UserLoginDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface AuthenticationService {
    
    public UserLoginDto authenticate(String username, String password);

    public UserInfoDto getUserInfoDto(UserLogin userLogin);

    public void changePassword(UserLogin userLogin, ChangePasswordDto changePasswordDto);

}
