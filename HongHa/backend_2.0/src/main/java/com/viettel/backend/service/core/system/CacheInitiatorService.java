package com.viettel.backend.service.core.system;

public interface CacheInitiatorService {

    public void resetCache();
    
}
