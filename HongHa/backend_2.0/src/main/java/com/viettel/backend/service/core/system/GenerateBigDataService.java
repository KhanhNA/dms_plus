package com.viettel.backend.service.core.system;

import org.springframework.stereotype.Service;

import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.SUPER_ADMIN })
@Service
public class GenerateBigDataService extends AbstractService {

//    @SuppressWarnings("unused")
//    private final Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//
//    @Autowired
//    private ConfigRepository configRepository;
//    
//    @Autowired
//    private CalendarConfigRepository calendarConfigRepository;
//
//    @Autowired
//    private UserRepository userRepository;
//    
//    @Autowired
//    private ClientRepository clientRepository;
//
//    @Autowired
//    private DistributorRepository distributorRepository;
//    
//    @Autowired
//    private UOMRepository uomRepository;
//    
//    @Autowired
//    private ProductCategoryRepository productCategoryRepository;
//    
//    @Autowired
//    private ProductRepository productRepository;
//    
//    @Autowired
//    private AreaRepository areaRepository;
//    
//    @Autowired
//    private CustomerTypeRepository customerTypeRepository;
//    
//    @Autowired
//    private CustomerRepository customerRepository;
//    
//    @Autowired
//    private RouteRepository routeRepository;
//    
//    @Autowired
//    private SurveyRepository surveyRepository;
//    
//    @Autowired
//    private PromotionRepository promotionRepository;
//
//    @Autowired
//    private CommonRepository commonRepository;
//    
//    @Autowired
//    private MasterDataRepository masterDataRepository;
//    
//    @Autowired
//    private FileRepository fileRepository;
//    
//    @Autowired
//    private PromotionCalculationService promotionCalculationService;
//
//    private final static Location DEFAULT_LOCATION = new Location(21.027921, 105.852284);
//    private final static Location NEAR_LOACATION = new Location(21.025423, 105.853238);
//    private final static double NEAR_DISTANCE = LocationUtils.calculateDistance(DEFAULT_LOCATION, NEAR_LOACATION);
//    private final static Location FAR_LOACATION = new Location(21.017480, 105.783022);
//    private final static double FAR_DISTANCE = LocationUtils.calculateDistance(DEFAULT_LOCATION, FAR_LOACATION);

    public void generateBigData() {
//        generateClient(1, 50, 10, 30, 53);
    }

//    // PRIVATE
//    private void initDomain(String clientId, PO domain) {
//        domain.setClientId(clientId);
//        domain.setActive(true);
//        domain.setDraft(false);
//    }
//
//    private void generateClient(int nbClient, int nbDistributorByClient, int nbSalesmanByDistributor,
//            int nbCustomerByDay, int nbWeekForData) {
//        String rootId = commonRepository.getRootId();
//        
//        I_Config rootConfig = configRepository.getConfig(rootId);
//
//        HashMap<Integer, SimpleDate> dateByDay = new HashMap<>();
//        SimpleDate today = DateTimeUtils.getToday();
//        dateByDay.put(today.getDayOfWeek(), today);
//        for (int i = 1; i <= 6; i++) {
//            SimpleDate tmp = DateTimeUtils.addDays(today, -i);
//            dateByDay.put(tmp.getDayOfWeek(), tmp);
//        }
//
//        BatchInsertRepository batchInsertRepository = new BatchInsertRepository(masterDataRepository);
//
//        ExecutorService es = Executors.newFixedThreadPool(5);
//        for (int clientIndex = 1; clientIndex <= nbClient; clientIndex++) {
//            ClientGenerator clientGenerator = new ClientGenerator(rootConfig, clientIndex, nbDistributorByClient,
//                    nbSalesmanByDistributor, nbCustomerByDay, nbWeekForData, dateByDay, batchInsertRepository);
//            es.execute(clientGenerator);
//        }
//
//        try {
//            es.shutdown();
//            es.awaitTermination(30, TimeUnit.MINUTES);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//            throw new UnsupportedOperationException();
//        }
//
//        batchInsertRepository.end();
//    }
//
//    private class ClientGenerator implements Runnable {
//
//        private String rootId;
//        private I_Config rootConfig;
//        private int clientIndex;
//        private int nbDistributorByClient;
//        private int nbSalesmanByDistributor;
//        private int nbCustomerByDay;
//        private int nbWeekForData;
//        private HashMap<Integer, SimpleDate> dateByDay;
//
//        private BatchInsertRepository batchInsertRepository;
//
//        public ClientGenerator(String rootId, I_Config rootConfig, int clientIndex, int nbDistributorByClient,
//                int nbSalesmanByDistributor, int nbCustomerByDay, int nbWeekForData,
//                HashMap<Integer, SimpleDate> dateByDay, BatchInsertRepository batchInsertRepository) {
//            super();
//            
//            this.rootId = rootId;
//            this.rootConfig = rootConfig;
//            this.clientIndex = clientIndex;
//            this.nbDistributorByClient = nbDistributorByClient;
//            this.nbSalesmanByDistributor = nbSalesmanByDistributor;
//            this.nbCustomerByDay = nbCustomerByDay;
//            this.nbWeekForData = nbWeekForData;
//            this.dateByDay = dateByDay;
//
//            this.batchInsertRepository = batchInsertRepository;
//        }
//
//        @Override
//        public void run() {
//            System.out.println("Start Client " + clientIndex);
//
//            // CLIENT
//            Category _client = new Category();
//            _client.setName("Client " + clientIndex);
//            _client.setCode("C" + clientIndex);
//            initDomain(rootId, _client);
//            I_Category client = clientRepository.save(rootId, _client);
//            String clientId = client.getId();
//
//            User _defaultAdmin = new User();
//            _defaultAdmin.setPassword(PasswordUtils.getDefaultPassword(passwordEncoder));
//            _defaultAdmin.setUsername(client.getCode(), "admin");
//            _defaultAdmin.setFullname("Admin");
//            _defaultAdmin.setRole(Role.ADMIN);
//            _defaultAdmin.setDefaultAdmin(true);
//            initDomain(clientId, _defaultAdmin);
//            I_User defaultAdmin = userRepository.save(clientId, _defaultAdmin);
//            I_UserEmbed defaultAdminEmbed = new UserEmbed(defaultAdmin);
//
//            Config _config = new Config();
//            _config.setVisitDistanceKPI(0.3);
//            _config.setVisitDurationKPI(300000);
//            _config.setCanEditCustomerLocation(true);
//            _config.setLocation(DEFAULT_LOCATION);
//            _config.setModules(Module.ALL);
//            I_Config config = configRepository.save(clientId, _config);
//
//            CalendarConfig _calendarConfig = new CalendarConfig();
//            _calendarConfig.setWorkingDays(Arrays.asList(2, 3, 4, 5, 6, 7));
//            I_CalendarConfig calendarConfig = calendarConfigRepository.save(clientId, _calendarConfig);
//            
//            // PRODUCT + UOM + PRODUCT CATEGORY
//            Category _uom = new Category();
//            _uom.setName("Bag");
//            _uom.setCode("B");
//            initDomain(clientId, _uom);
//            I_Category uom = uomRepository.save(clientId, _uom);
//            I_CategoryEmbed uomEmbed = new CategoryEmbed(uom);
//
//            Category _productCategory = new Category();
//            _productCategory.setName("Category X");
//            initDomain(clientId, _productCategory);
//            I_Category productCategory = productCategoryRepository.save(clientId, _productCategory);
//            I_CategoryEmbed productCategoryEmbed = new CategoryEmbed(productCategory);
//
//            List<I_Product> products = new LinkedList<>();
//            for (int productIndex = 1; productIndex <= 20; productIndex++) {
//                Product product = new Product();
//                product.setName(String.format("Product %03d", productIndex));
//                product.setCode("P" + productIndex);
//                product.setUom(uomEmbed);
//                product.setProductCategory(productCategoryEmbed);
//                product.setPrice(new BigDecimal(productIndex * 3000));
//                product.setProductivity(new BigDecimal(1));
//                product.setPhoto(rootConfig.getProductPhoto());
//                product.setDescription(null);
//                initDomain(clientId, product);
//                products.add(product);
//            }
//            
//            productRepository.insertBatch(clientId, products);
//            products = productRepository.getAll(clientId, null);
//            Map<String, I_Product> productMap = getPOMap(products);
//
//            I_Order sampleOrder = getSampleOrder(productMap);
//
//            // SURVEY + PROMOTION
//            List<I_Survey> surveys = new LinkedList<>();
//            List<I_Promotion> promotions = new LinkedList<>();
//            SimpleDate tmpDate = DateTimeUtils.getToday();
//            String product1Id = products.get(0).getId();
//            String product2Id = products.get(1).getId();
//            String product3Id = products.get(2).getId();
//            for (int i = 0; i < nbWeekForData; i++) {
//                tmpDate = DateTimeUtils.addWeeks(tmpDate, -i);
//                SimpleDate startDate = DateTimeUtils.addDays(DateTimeUtils.addWeeks(tmpDate, -1), 1);
//                SimpleDate endDate = tmpDate;
//
//                // SURVEY
//                Survey survey = new Survey();
//                survey.setName(String.format("Survey %03d", i));
//                survey.setStartDate(startDate);
//                survey.setEndDate(endDate);
//                survey.setRequired(true);
//
//                List<I_SurveyQuestion> questions = new ArrayList<>(5);
//                for (int j = 0; j < 5; j++) {
//                    SurveyQuestion question = new SurveyQuestion();
//                    question.setSeqNo(j + 1);
//                    question.setName("Question " + j);
//                    question.setMultipleChoice(false);
//                    question.setRequired(true);
//
//                    List<I_SurveyOption> options = new ArrayList<>(2);
//                    for (int k = 0; k < 2; k++) {
//                        SurveyOption option = new SurveyOption();
//                        option.setSeqNo((j * 2) + (k + 1));
//                        option.setName("Option " + k);
//                        options.add(option);
//                    }
//
//                    question.setOptions(options);
//                    questions.add(question);
//                }
//
//                survey.setQuestions(questions);
//                initDomain(clientId, survey);
//                surveys.add(surveyRepository.save(clientId, survey));
//
//                // PROMOTION
//                Promotion promotion = new Promotion();
//                promotion.setName(String.format("Promotion %03d", i));
//                promotion.setDescription("Promotion " + startDate.getIsoDate() + "-" + endDate.getIsoDate());
//                promotion.setApplyFor("Apply For All");
//                promotion.setStartDate(startDate);
//                promotion.setEndDate(endDate);
//
//                // MUA PRODUCT 1 tang PRODUCT 2
//                PromotionReward reward1 = new PromotionReward();
//                reward1.setQuantity(BigDecimal.ONE);
//                reward1.setProductId(product2Id);
//                PromotionCondition promotionCondition1 = new PromotionCondition();
//                promotionCondition1.setProductId(product1Id);
//                promotionCondition1.setQuantity(BigDecimal.ONE);
//                PromotionDetail detail1 = new PromotionDetail();
//                detail1.setSeqNo(1);
//                detail1.setType(PromotionDetailType.C_PRODUCT_QTY_R_PRODUCT_QTY);
//                detail1.setCondition(promotionCondition1);
//                detail1.setReward(reward1);
//
//                // GIAM 50% PRODUCT 3
//                PromotionReward reward2 = new PromotionReward();
//                reward2.setPercentage(new BigDecimal(50));
//                PromotionCondition promotionCondition2 = new PromotionCondition();
//                promotionCondition2.setProductId(product3Id);
//                promotionCondition2.setQuantity(BigDecimal.ONE);
//                PromotionDetail detail2 = new PromotionDetail();
//                detail2.setSeqNo(1);
//                detail2.setType(PromotionDetailType.C_PRODUCT_QTY_R_PERCENTAGE_AMT);
//                detail2.setCondition(promotionCondition2);
//                detail2.setReward(reward2);
//
//                promotion.setDetails(Arrays.asList(detail1, detail2));
//                initDomain(clientId, promotion);
//                promotions.add(promotionRepository.save(clientId, promotion));
//            }
//
//            // DISTRIBUTOR
//            User _supervisor = new User();
//            _supervisor.setDraft(false);
//            _supervisor.setPassword(PasswordUtils.getDefaultPassword(passwordEncoder));
//            _supervisor.setUsername(client.getCode(), "sup");
//            _supervisor.setFullname("Supervisor");
//            _supervisor.setRole(Role.SUPERVISOR);
//            initDomain(clientId, _supervisor);
//            I_User supervisor = userRepository.save(clientId, _supervisor);
//            I_UserEmbed supervisorEmbed = new UserEmbed(supervisor);
//
//            Category _customerType = new Category();
//            _customerType.setName("Store");
//            initDomain(clientId, _customerType);
//            I_Category customerType = customerTypeRepository.save(clientId, _customerType);
//            I_CategoryEmbed customerTypeEmbed = new CategoryEmbed(customerType);
//
//            ExecutorService es = Executors.newFixedThreadPool(5);
//            for (int disIndex = 1; disIndex <= nbDistributorByClient; disIndex++) {
//                DistributorGenerator distributorGenerator = new DistributorGenerator(client, disIndex,
//                        nbSalesmanByDistributor, nbCustomerByDay, nbWeekForData, dateByDay, sampleOrder,
//                        defaultAdminEmbed, supervisorEmbed, customerTypeEmbed, promotions, surveys,
//                        batchInsertRepository);
//
//                es.execute(distributorGenerator);
//            }
//
//            try {
//                es.shutdown();
//                es.awaitTermination(30, TimeUnit.MINUTES);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//                throw new UnsupportedOperationException();
//            }
//
//            System.out.println("End Client " + clientIndex);
//        }
//    }
//
//    private class DistributorGenerator implements Runnable {
//
//        private Category client;
//        private int disIndex;
//        private int nbSalesmanByDistributor;
//        private int nbCustomerByDay;
//        private int nbWeekForData;
//        private HashMap<Integer, SimpleDate> dateByDay;
//        private Order sampleOrder;
//        private UserEmbed defaultAdminEmbed;
//        private UserEmbed supervisorEmbed;
//        private CategoryEmbed customerTypeEmbed;
//        private List<Promotion> promotions;
//        private List<Survey> surveys;
//
//        private BatchInsertRepository batchInsertRepository;
//
//        public DistributorGenerator(Category client, int disIndex, int nbSalesmanByDistributor, int nbCustomerByDay,
//                int nbWeekForData, HashMap<Integer, SimpleDate> dateByDay, Order sampleOrder,
//                UserEmbed defaultAdminEmbed, UserEmbed supervisorEmbed, CategoryEmbed customerTypeEmbed,
//                List<Promotion> promotions, List<Survey> surveys, BatchInsertRepository batchInsertRepository) {
//            super();
//            this.client = client;
//            this.disIndex = disIndex;
//            this.nbSalesmanByDistributor = nbSalesmanByDistributor;
//            this.nbCustomerByDay = nbCustomerByDay;
//            this.nbWeekForData = nbWeekForData;
//            this.dateByDay = dateByDay;
//            this.sampleOrder = sampleOrder;
//            this.defaultAdminEmbed = defaultAdminEmbed;
//            this.supervisorEmbed = supervisorEmbed;
//            this.customerTypeEmbed = customerTypeEmbed;
//            this.promotions = promotions;
//            this.surveys = surveys;
//            this.batchInsertRepository = batchInsertRepository;
//        }
//
//        @Override
//        public void run() {
//            System.out.println("Start Distributor " + disIndex + " of " + client.getName());
//
//            List<User> users = new LinkedList<>();
//            List<I_Distributor> distributors = new LinkedList<>();
//            List<I_Category> areas = new LinkedList<>();
//            List<I_Route> routes = new LinkedList<>();
//
//            ObjectId clientId = client.getId();
//
//            Distributor distributor = new Distributor();
//            distributor.setName(String.format("Distributor %03d", disIndex));
//            distributor.setCode("DIS_GEN_" + disIndex);
//            distributor.setSupervisor(supervisorEmbed);
//            initDomain(clientId, distributor);
//            distributors.add(distributor);
//            CategoryEmbed distributorEmbed = new CategoryEmbed(distributor);
//
//            Area area = new Area();
//            area.setName("Area" + disIndex);
//            area.setDistributor(distributorEmbed);
//            initDomain(clientId, area);
//            areas.add(area);
//            CategoryEmbed areaEmbed = new CategoryEmbed(area);
//
//            ExecutorService es = Executors.newFixedThreadPool(5);
//
//            for (int smIndex = 1; smIndex <= nbSalesmanByDistributor; smIndex++) {
//                int index1 = ((disIndex - 1) * nbSalesmanByDistributor) + smIndex;
//
//                User salesman = new User();
//                salesman.setPassword(PasswordUtils.getDefaultPassword(passwordEncoder));
//                salesman.setUsername(client.getCode(), "sm" + index1);
//                salesman.setFullname(String.format("Salesman %04d", index1));
//                salesman.setRole(Role.SALESMAN);
//                salesman.setDistributor(distributorEmbed);
//                initDomain(clientId, salesman);
//                users.add(salesman);
//                UserEmbed salesmanEmbed = new UserEmbed(salesman);
//
//                Route route = new Route();
//                route.setName("Route " + index1);
//                route.setDistributor(distributorEmbed);
//                route.setSalesman(salesmanEmbed);
//                initDomain(clientId, route);
//                routes.add(route);
//
//                for (int dayIndex = 2; dayIndex <= 7; dayIndex++) {
//                    for (int cusIndex = 1; cusIndex <= nbCustomerByDay; cusIndex++) {
//                        int index2 = ((index1 - 1) * 6 * nbCustomerByDay) + ((dayIndex - 2) * nbCustomerByDay)
//                                + cusIndex;
//
//                        Customer customer = new Customer();
//                        customer.setName(String.format("Customer %06d", index2));
//                        customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
//                        customer.setCreatedTime(DateTimeUtils.getCurrentTime());
//                        customer.setCreatedBy(defaultAdminEmbed);
//                        customer.setCode("CUS_GEN_" + index2);
//                        customer.setMobile("0988888888");
//                        customer.setDistributor(distributorEmbed);
//                        customer.setCustomerType(customerTypeEmbed);
//                        customer.setArea(areaEmbed);
//                        customer.setPhone("0438288888");
//                        customer.setContact("Mr X");
//                        customer.setEmail("xxx@xxx.xxx");
//                        customer.setLocation(
//                                new double[] { DEFAULT_LOCATION.getLongitude(), DEFAULT_LOCATION.getLatitude() });
//
//                        Schedule schedule = new Schedule();
//                        schedule.setRouteId(route.getId());
//                        ScheduleItem item = new ScheduleItem();
//                        item.setMonday(dayIndex == 2);
//                        item.setTuesday(dayIndex == 3);
//                        item.setWednesday(dayIndex == 4);
//                        item.setThursday(dayIndex == 5);
//                        item.setFriday(dayIndex == 6);
//                        item.setSaturday(dayIndex == 7);
//                        item.setSunday(false);
//                        schedule.setItems(Collections.singletonList(item));
//                        customer.setSchedule(schedule);
//                        initDomain(clientId, customer);
//
//                        batchInsertRepository.insert(customer);
//
//                        CustomerDataGenerator customerDataGenerator = new CustomerDataGenerator(clientId, nbWeekForData,
//                                dateByDay, sampleOrder, salesmanEmbed, distributorEmbed, customer, cusIndex, promotions,
//                                surveys, batchInsertRepository);
//                        es.execute(customerDataGenerator);
//                    }
//                }
//            }
//
//            masterDataRepository._insertBatch(User.class, users);
//            masterDataRepository._insertBatch(Distributor.class, distributors);
//            masterDataRepository._insertBatch(Area.class, areas);
//            masterDataRepository._insertBatch(Route.class, routes);
//
//            try {
//                es.shutdown();
//                es.awaitTermination(30, TimeUnit.MINUTES);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//                throw new UnsupportedOperationException();
//            }
//
//            System.out.println("End Distributor " + disIndex + " of " + client.getName());
//        }
//    }
//
//    private class CustomerDataGenerator implements Runnable {
//
//        private String clientId;
//        private int nbWeekForData;
//        private HashMap<Integer, SimpleDate> dateByDay;
//        private Order sampleOrder;
//
//        private UserEmbed salesmanEmbed;
//        private CategoryEmbed distributorEmbed;
//        private I_Customer customer;
//        private int cusIndex;
//        private List<Promotion> promotions;
//        private List<Survey> surveys;
//
//        private BatchInsertRepository batchInsertRepository;
//
//        public CustomerDataGenerator(String clientId, int nbWeekForData, HashMap<Integer, SimpleDate> dateByDay,
//                Order sampleOrder, UserEmbed salesmanEmbed, CategoryEmbed distributorEmbed, I_Customer customer,
//                int cusIndex, List<Promotion> promotions, List<Survey> surveys,
//                BatchInsertRepository batchInsertRepository) {
//            super();
//            this.clientId = clientId;
//            this.nbWeekForData = nbWeekForData;
//            this.dateByDay = dateByDay;
//            this.sampleOrder = sampleOrder;
//            this.salesmanEmbed = salesmanEmbed;
//            this.distributorEmbed = distributorEmbed;
//            this.customer = customer;
//            this.cusIndex = cusIndex;
//            this.promotions = promotions;
//            this.surveys = surveys;
//            this.batchInsertRepository = batchInsertRepository;
//        }
//
//        @Override
//        public void run() {
//            System.out.println("Start " + customer.getName());
//
//            CustomerEmbed customerEmbed = new CustomerEmbed(customer);
//
//            for (int i = 0; i < nbWeekForData; i++) {
//                String orderCode = "PO_" + customer.getCode() + "_" + (i + 1);
//
//                SimpleDate startTime = dateByDay.get(customer.getSchedule().getItems().get(0).getDays().get(0));
//                startTime = DateTimeUtils.addMinutes(DateTimeUtils.addWeeks(startTime, -i), cusIndex * 10);
//
//                Random random = new Random();
//                if (random.nextDouble() < 0.8) {
//                    boolean locationUndefined = random.nextDouble() < 0.1;
//                    boolean far = !locationUndefined && random.nextDouble() < 0.3;
//                    boolean closed = random.nextDouble() < 0.1;
//                    boolean hasPhoto = !closed && random.nextDouble() < 0.1;
//                    boolean hasOrder = !closed && random.nextDouble() < 0.8;
//                    boolean hasFeedback = !closed && random.nextDouble() < 0.1;
//                    boolean feedbacksReaded = i != 0;
//                    
//                    Visit visit = createVisit(clientId, salesmanEmbed, distributorEmbed, customerEmbed, startTime,
//                            locationUndefined, far, closed, hasPhoto, hasOrder, orderCode, sampleOrder, surveys.get(i),
//                            promotions.get(i), hasFeedback, feedbacksReaded);
//                    batchInsertRepository.insert(visit);
//                } else {
//                    Order order = createOrder(clientId, salesmanEmbed, distributorEmbed, customerEmbed, startTime,
//                            orderCode, sampleOrder, surveys.get(i), promotions.get(i));
//                    batchInsertRepository.insert(order);
//                }
//            }
//
//            System.out.println("End " + customer.getName());
//        }
//
//    }
//
//    private Visit createVisit(ObjectId clientId, UserEmbed salesmanEmbed, CategoryEmbed distributorEmbed,
//            CustomerEmbed customerEmbed, SimpleDate startTime, boolean locationUndefined, boolean far, boolean closed,
//            boolean hasPhoto, boolean hasOrder, String orderCode, Order sampleOrder, Survey survey, Promotion promotion,
//            boolean hasFeedback, boolean feedbacksReaded, Map<String, I_Product> productMap) {
//        Visit visit = new Visit();
//
//        visit.setDistributor(distributorEmbed);
//        visit.setSalesman(salesmanEmbed);
//        visit.setCustomer(customerEmbed);
//        
//        // STATUS
//        visit.setVisitStatus(Visit.VISIT_STATUS_VISITED);
//        
//        // LOCATION
//        visit.setCustomerLocation(new double[] { DEFAULT_LOCATION.getLongitude(), DEFAULT_LOCATION.getLatitude() });
//        if (locationUndefined) {
//            visit.setLocationStatus(Visit.LOCATION_STATUS_UNLOCATED);
//        } else {
//            if (far) {
//                visit.setLocation(new double[] { FAR_LOACATION.getLongitude(), FAR_LOACATION.getLatitude() });
//                visit.setDistance(FAR_DISTANCE);
//                visit.setLocationStatus(Visit.LOCATION_STATUS_TOO_FAR);
//            } else {
//                visit.setLocation(new double[] { NEAR_LOACATION.getLongitude(), NEAR_LOACATION.getLatitude() });
//                visit.setDistance(NEAR_DISTANCE);
//                visit.setLocationStatus(Visit.LOCATION_STATUS_LOCATED);
//            }
//        }
//        
//        // CLOSED
//        if (closed) {
//            visit.setClosed(true);
//            
//            visit.setStartTime(startTime);
//            visit.setEndTime(startTime);
//            visit.setDuration(0);
//            visit.setErrorDuration(false);
//
//            visit.setPhoto(getCustomerClosePhoto(clientId));
//        } else {
//            visit.setClosed(false);
//            
//            // PHOTO
//            if (hasPhoto) {
//                visit.setPhoto(getCustomerOpenPhoto(clientId));
//            }
//
//            // TIME
//            visit.setStartTime(startTime);
//            visit.setEndTime(DateTimeUtils.addMinutes(startTime, 5));
//            visit.setDuration(5 * 60 * 1000);
//            visit.setErrorDuration(false);
//
//            // SURVEY
//            SurveyAnswerContent surveyAnswer = new SurveyAnswerContent();
//            surveyAnswer.setSurveyId(survey.getId());
//            Set<ObjectId> options = new HashSet<>();
//            Random random = new Random();
//            for (SurveyQuestion question : survey.getQuestions()) {
//                options.add(question.getOptions().get(random.nextInt(2)).getId());
//            }
//            surveyAnswer.setOptions(options);
//            visit.setSurveyAnswers(Collections.singletonList(surveyAnswer));
//
//            // FEEDBACK
//            if (hasFeedback) {
//                visit.setFeedbacks(Collections.singletonList("Coucou"));
//                visit.setFeedbacksReaded(feedbacksReaded);
//            }
//
//            if (hasOrder) {
//                // ORDER + PROMOTION
//                fillDataToOrder(clientId, visit, salesmanEmbed, distributorEmbed, customerEmbed, startTime, orderCode,
//                        sampleOrder, survey, promotion, productMap);
//            }
//        }
//
//        initDomain(clientId, visit);
//
//        return visit;
//    }
//    
//    private Order fillDataToOrder(String clientId, Order order, UserEmbed salesmanEmbed, CategoryEmbed distributorEmbed,
//            I_CustomerEmbed customerEmbed, SimpleDate startTime, String orderCode, Order sampleOrder, Survey survey,
//            Promotion promotion, Map<String, I_Product> productMap) {
//        order.setCreatedBy(salesmanEmbed);
//        order.setCustomer(customerEmbed);
//        order.setDistributor(distributorEmbed);
//
//        order.setCreatedTime(startTime);
//        order.setCode(orderCode);
//        order.setDeliveryType(Order.DELIVERY_TYPE_IMMEDIATE);
//        order.setDetails(sampleOrder.getDetails());
//        order.setQuantity(sampleOrder.getQuantity());
//        order.setProductivity(sampleOrder.getProductivity());
//        order.setDiscountPercentage(sampleOrder.getDiscountPercentage());
//        order.setDiscountAmt(sampleOrder.getDiscountAmt());
//        order.setSubTotal(sampleOrder.getSubTotal());
//        
//        BigDecimal promotionAmt = BigDecimal.ZERO;
//        
//        List<I_OrderPromotion> orderPromotions = promotionCalculationService.calculate(Collections.singletonList(promotion), order, productMap);
//        
//        if (orderPromotions != null && !orderPromotions.isEmpty()) {
//            order.setPromotions(orderPromotions);
//
//            for (I_OrderPromotion orderPromotion : orderPromotions) {
//                for (I_OrderPromotionDetail orderPromotionDetail : orderPromotion.getDetails()) {
//                    if (orderPromotionDetail.getReward().getAmount() != null) {
//                        promotionAmt = promotionAmt.add(orderPromotionDetail.getReward().getAmount());
//                    }
//                }
//            }
//        }
//
//        if (order.getSubTotal().subtract(promotionAmt).signum() == -1) {
//            throw new UnsupportedOperationException("error when calculate promotion");
//        }
//
//        order.setPromotionAmt(promotionAmt);
//        order.setGrandTotal(order.getSubTotal().subtract(promotionAmt).subtract(order.getDiscountAmt()));
//        
//        order.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
//        order.setApprovedTime(order.getCreatedTime());
//        order.setApprovedBy(salesmanEmbed);
//        order.setVanSales(true);
//        
//        return order;
//    }
//
//    private I_Order getSampleOrder(Map<String, I_Product> productMap) {
//        Order order = new Order();
//        
//        order.setDiscountPercentage(null);
//        order.setDiscountAmt(null);
//
//        BigDecimal subTotal = BigDecimal.ZERO;
//
//        BigDecimal quantity = BigDecimal.ZERO;
//        BigDecimal productivity = BigDecimal.ZERO;
//
//        List<I_OrderDetail> details = new ArrayList<I_OrderDetail>(productMap.size());
//        for (final I_Product product : productMap.values()) {
//            I_OrderDetail detail = new I_OrderDetail() {
//                
//                private static final long serialVersionUID = -3826273118768412564L;
//
//                @Override
//                public BigDecimal getQuantity() {
//                    return BigDecimal.ONE;
//                }
//                
//                @Override
//                public I_OrderProduct getProduct() {
//                    return new I_OrderProduct() {
//                        
//                        private static final long serialVersionUID = -2104324247001911390L;
//
//                        @Override
//                        public String getId() {
//                            return product.getId();
//                        }
//                        
//                        @Override
//                        public String getName() {
//                            return product.getName();
//                        }
//                        
//                        @Override
//                        public String getCode() {
//                            return product.getCode();
//                        }
//                        
//                        @Override
//                        public I_CategoryEmbed getUom() {
//                            return product.getUom();
//                        }
//                        
//                        @Override
//                        public I_CategoryEmbed getProductCategory() {
//                            return product.getProductCategory();
//                        }
//                        
//                        @Override
//                        public BigDecimal getProductivity() {
//                            return product.getProductivity();
//                        }
//                        
//                        @Override
//                        public BigDecimal getPrice() {
//                            return product.getPrice();
//                        }
//                    };
//                }
//            };
//            
//            subTotal = subTotal.add(detail.getAmount());
//
//            details.add(detail);
//
//            quantity = quantity.add(detail.getQuantity());
//            productivity = productivity.add(detail.getProductivity());
//        }
//        
//        order.setDetails(details);
//
//        order.setQuantity(quantity);
//        order.setProductivity(productivity);
//        
//        order.setSubTotal(subTotal);
//
//        return order;
//    }
//    
//    private String getCustomerClosePhoto(String clientId) {
//        ClassPathResource closePhotoResource = new ClassPathResource("customer-close-photo.jpeg");
//        try {
//            return fileRepository.store(clientId, closePhotoResource.getInputStream(), closePhotoResource.getFilename(),
//                    "image/jpeg").toString();
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new UnsupportedOperationException();
//        }
//    }
//    
//    private String getCustomerOpenPhoto(String clientId) {
//        ClassPathResource openPhotoResource = new ClassPathResource("customer-open-photo.jpg");
//        try {
//            return fileRepository.store(clientId, openPhotoResource.getInputStream(), openPhotoResource.getFilename(),
//                    "image/jpeg").toString();
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new UnsupportedOperationException();
//        }
//    }
//    
//    private I_OrderPromotion createOrderPromotion(final I_Promotion promotion, final List<I_OrderPromotionDetail> details) {
//        return new I_OrderPromotion() {
//            
//            private static final long serialVersionUID = 218064141797221170L;
//
//            @Override
//            public String getId() {
//                return promotion.getId();
//            }
//            
//            @Override
//            public String getName() {
//                return promotion.getName();
//            }
//            
//            @Override
//            public String getCode() {
//                return promotion.getCode();
//            }
//            
//            @Override
//            public List<I_OrderPromotionDetail> getDetails() {
//                return details;
//            }
//        };
//    }
//    
//    private I_OrderPromotionDetail createOrderPromotionDetail(final int promotionDetailSeqNo,
//            final String conditionProductName, final I_OrderPromotionReward reward) {
//        return new I_OrderPromotionDetail() {
//
//            private static final long serialVersionUID = -3311133482530433531L;
//
//            @Override
//            public int getSeqNo() {
//                return promotionDetailSeqNo;
//            }
//
//            @Override
//            public I_OrderPromotionReward getReward() {
//                return reward;
//            }
//
//            @Override
//            public String getConditionProductName() {
//                return conditionProductName;
//            }
//        };
//    }
//    
//    private I_OrderPromotionReward createOrderPromotionReward(final BigDecimal quantity, final String productText,
//            final I_ProductEmbed product, final BigDecimal amount) {
//        return new I_OrderPromotionReward() {
//
//            private static final long serialVersionUID = -4167120778335463415L;
//
//            @Override
//            public BigDecimal getQuantity() {
//                return quantity;
//            }
//
//            @Override
//            public String getProductText() {
//                return productText;
//            }
//
//            @Override
//            public I_ProductEmbed getProduct() {
//                return product;
//            }
//
//            @Override
//            public BigDecimal getAmount() {
//                return amount;
//            }
//        };
//    }
//
//
//    private static class BatchInsertRepository {
//
//        private final static int NUMBER_RECORD_PER_INSERT = 1000;
//
//        private MasterDataRepository masterDataRepository;
//
//        private HashMap<Class<? extends PO>, List<? extends PO>> map;
//        private ExecutorService executorService;
//
//        public BatchInsertRepository(MasterDataRepository masterDataRepository) {
//            this.masterDataRepository = masterDataRepository;
//
//            this.map = new HashMap<>();
//            this.executorService = Executors.newFixedThreadPool(10);
//        }
//
//        @SuppressWarnings({ "unchecked", "rawtypes" })
//        public synchronized <D extends PO> void insert(D domain) {
//            List<D> domains = (List<D>) map.get(domain.getClass());
//            if (domains == null) {
//                domains = new ArrayList<>(NUMBER_RECORD_PER_INSERT);
//                map.put(domain.getClass(), domains);
//            }
//
//            domains.add(domain);
//
//            if (domains.size() >= NUMBER_RECORD_PER_INSERT) {
//                System.out.println("Insert " + domain.getClass().getName());
//
//                BatchInsert batchInsert = new BatchInsert(masterDataRepository, domain.getClass(), domains);
//                executorService.execute(batchInsert);
//
//                map.put(domain.getClass(), new ArrayList<D>(NUMBER_RECORD_PER_INSERT));
//            }
//        }
//
//        @SuppressWarnings({ "rawtypes", "unchecked" })
//        public synchronized void end() {
//            for (Entry<Class<? extends PO>, List<? extends PO>> entry : map.entrySet()) {
//                if (entry.getValue() != null && !entry.getValue().isEmpty()) {
//                    BatchInsert batchInsert = new BatchInsert(masterDataRepository, entry.getKey(), entry.getValue());
//                    executorService.execute(batchInsert);
//                }
//            }
//
//            map.clear();
//
//            try {
//                executorService.shutdown();
//                executorService.awaitTermination(30, TimeUnit.MINUTES);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//                throw new UnsupportedOperationException();
//            }
//        }
//
//        private static class BatchInsert<D extends PO> implements Runnable {
//
//            private MasterDataRepository masterDataRepository;
//            private List<D> domains;
//            private Class<D> domainClazz;
//
//            public BatchInsert(MasterDataRepository masterDataRepository, Class<D> domainClazz, List<D> domains) {
//                this.masterDataRepository = masterDataRepository;
//                this.domains = domains;
//                this.domainClazz = domainClazz;
//            }
//
//            @Override
//            public void run() {
//                masterDataRepository._insertBatch(domainClazz, domains);
//            }
//        }
//        
//    }
}
