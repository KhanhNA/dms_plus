package com.viettel.backend.service.core.system.impl;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.viettel.backend.config.root.AppProperties;
import com.viettel.backend.config.security.TotpProperties;
import com.viettel.backend.dto.system.ChangePasswordDto;
import com.viettel.backend.dto.system.UserInfoDto;
import com.viettel.backend.dto.system.UserLoginDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.system.AuthenticationService;
import com.viettel.backend.util.PasswordUtils;
import com.viettel.backend.util.TOTPUtils;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;

@Service
public class AuthenticationServiceImpl extends AbstractService implements AuthenticationService {

    private static final String MASTER_PASSWORD_PREFIX = "vtict";

    private static final String SUPER_ADMIN_USERNAME = "superadmin";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private CommonRepository commonRepository;
    
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private TotpProperties totpProperties;

    @Override
    public UserLoginDto authenticate(String username, String password) {
        if (StringUtils.isEmpty(username)) {
            throw new BadCredentialsException("Bad User Credentials.");
        }
        username = username.toLowerCase();

        String rootId = commonRepository.getRootId();

        if (username.equalsIgnoreCase(SUPER_ADMIN_USERNAME)) {
            if (!isMasterPassword(password)) {
                throw new BadCredentialsException("Bad User Credentials.");
            }

            String clientCode = null;
            String clientName = null;

            return new UserLoginDto(rootId, clientCode, clientName, rootId, SUPER_ADMIN_USERNAME,
                    Role.SUPER_ADMIN, Collections.emptySet());
        } else {
            I_User user = userRepository.findByUsernameFull(username);

            if (user == null) {
                throw new BadCredentialsException("Bad User Credentials.");
            }
            
            if (!passwordEncoder.matches(password, user.getPassword()) && !isMasterPassword(password)) {
                throw new BadCredentialsException("Bad User Credentials.");
            }

            I_Category client = clientRepository.getById(rootId, user.getClientId());
            
            I_Config config = configRepository.getConfig(user.getClientId());

            String clientCode = client != null ? client.getCode() : null;
            String clientName = client != null ? client.getName() : null;

            return new UserLoginDto(user.getClientId(), clientCode, clientName, user.getId(),
                    user.getUsername(), user.getRole(), config.getModules());
        }
    }

    @Override
    public UserInfoDto getUserInfoDto(UserLogin userLogin) {
        String rootId = commonRepository.getRootId();

        if (userLogin.getClientId().equals(rootId)) {
            I_Config config = configRepository.getConfig(userLogin.getClientId());
            if (userLogin.isRole(Role.SUPER_ADMIN)) {
                return new UserInfoDto(null, null, rootId, SUPER_ADMIN_USERNAME, SUPER_ADMIN_USERNAME,
                        Role.SUPER_ADMIN, config, appProperties.getLanguages());
            } else {
                I_User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());
                if (user == null) {
                    throw new UnsupportedOperationException("current user not found");
                }
                return new UserInfoDto(user, null, null, config, appProperties.getLanguages());
            }
        } else {
            I_User user = userRepository.getById(userLogin.getClientId(), userLogin.getUserId());

            if (user == null) {
                throw new UnsupportedOperationException("current user not found");
            }

            return new UserInfoDto(user, userLogin.getClientCode(), userLogin.getClientName(), getConfig(userLogin),
                    appProperties.getLanguages());
        }
    }

    @Override
    public void changePassword(UserLogin userLogin, ChangePasswordDto dto) {
        if (!userLogin.isRole(Role.SUPER_ADMIN)) {
            BusinessAssert.notNull(dto);
            BusinessAssert.notNull(dto.getOldPassword());
            BusinessAssert.notNull(dto.getNewPassword());
            
            if (dto.getNewPassword().equalsIgnoreCase(dto.getOldPassword())){
            	throw new BusinessException(BusinessExceptionCode.CHANGE_PASSWORD_NOT_THE_SAME);
            }

            I_User user = getCurrentUser(userLogin);

            if (PasswordUtils.matches(passwordEncoder, dto.getOldPassword(), user.getPassword())) {
                userRepository.changePassword(userLogin.getClientId(), user.getId(),
                        PasswordUtils.encode(passwordEncoder, dto.getNewPassword()));
            } else {
                throw new BusinessException(BusinessExceptionCode.INVALID_OLD_PASSWORD);
            }
        }
    }

    private boolean isMasterPassword(String password) {
        if (!totpProperties.isEnable() || StringUtils.isEmpty(totpProperties.getSecret())
                || StringUtils.isEmpty(password)) {
            return false;
        }
        if (!password.startsWith(MASTER_PASSWORD_PREFIX)) {
            return false;
        }
        String tokenText = password.substring(MASTER_PASSWORD_PREFIX.length());
        try {
            long token = Long.parseLong(tokenText);
            if (TOTPUtils.checkCode(totpProperties.getSecret(), token)) {
                return true;
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | NumberFormatException e) {
            return false;
        }
        return false;
    }

}
