package com.viettel.backend.service.core.system.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheManagerService;
import com.viettel.backend.service.core.system.CacheInitiatorService;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.SUPER_ADMIN, Role.ADMIN })
@Service
public class CacheInitiatorServiceImpl implements CacheInitiatorService {

    @Autowired
    CommonRepository commonRepository;
    
    @Autowired
    CacheManagerService cacheManagerService;

    @Override
    public void resetCache() {
        commonRepository.resetCache();
        cacheManagerService.clearAll();
        cacheManagerService.init(null);
    }
    
}
