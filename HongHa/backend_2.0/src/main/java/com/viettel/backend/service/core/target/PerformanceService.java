package com.viettel.backend.service.core.target;

import com.viettel.backend.dto.target.PerformanceDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface PerformanceService {
    
    public PerformanceDto getPerformanceBySalesman(UserLogin userLogin, String salesmanId, int month, int year);
    
    public PerformanceDto getPerformanceDailyBySalesman(UserLogin userLogin,String salesmanId, String fromDate, String toDate);
    
}
