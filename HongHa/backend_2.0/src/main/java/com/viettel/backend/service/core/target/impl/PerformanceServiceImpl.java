package com.viettel.backend.service.core.target.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.order.SalesResultDailyDto;
import com.viettel.backend.dto.order.SalesResultDto;
import com.viettel.backend.dto.target.PerformanceDto;
import com.viettel.backend.dto.target.PerformanceDto.ProductCategorySalesQuantityDto;
import com.viettel.backend.dto.target.PerformanceDto.ProductSalesQuantityDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheCalendarService;
import com.viettel.backend.service.core.cache.CacheScheduleService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.target.PerformanceService;
import com.viettel.repository.common.CustomerPendingRepository;
import com.viettel.repository.common.OrderSummaryRepository;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.TargetRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.VisitSummaryRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.category.I_Target;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.data.I_OrderSummary;
import com.viettel.repository.common.domain.data.I_VisitSummary;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class PerformanceServiceImpl extends AbstractService implements PerformanceService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private CustomerPendingRepository customerPendingRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private OrderSummaryRepository orderSummaryRepository;

    @Autowired
    private VisitSummaryRepository visitSummaryRepository;

    @Autowired
    private TargetRepository targetRepository;

    @Autowired
    private CacheCalendarService cacheCalendarService;

    @Autowired
    private CacheScheduleService cacheScheduleService;

    @Override
    public PerformanceDto getPerformanceBySalesman(UserLogin userLogin, String salesmanId, int month, int year) {
        I_User salesman = getMandatoryPO(userLogin, salesmanId, userRepository);
        BusinessAssert.isTrue(checkSalesmanAccessible(userLogin, salesman.getId()), "salesman not accessible");

        Collection<String> salesmanIds = Collections.singleton(salesman.getId());
        String distributorId = salesman.getDistributor().getId();
        Collection<String> distributorIds = Collections.singleton(distributorId);
        Period period = DateTimeUtils.getPeriodByMonth(month, year);

        List<SimpleDate> workingDays = cacheCalendarService.getWorkingDays(userLogin.getClientId(), period);

        PerformanceDto dto = new PerformanceDto(salesman);
        dto.setNbDay(workingDays.size());

        // CALCULATE NB VISIT PLANNED
        List<I_Route> routes = routeRepository.getRoutesBySalesmen(userLogin.getClientId(), distributorId, salesmanIds);
        Map<String, Integer> nbVisitPlannedThisMonthMap = cacheScheduleService
                .getNbVisitPlannedByRoute(userLogin.getClientId(), distributorIds, period);
        int nbVisitPlannedThisMonth = 0;
        for (I_Route route : routes) {
            Integer temp = nbVisitPlannedThisMonthMap.get(route.getId());
            temp = temp == null ? 0 : temp;
            nbVisitPlannedThisMonth += temp;
        }
        dto.getNbVisit().setPlan(nbVisitPlannedThisMonth);

        I_Target target = targetRepository.getTargetBySalesman(userLogin.getClientId(), salesman.getId(), month, year);
        if (target != null) {
            dto.getRevenue().setPlan(target.getRevenue());
            dto.getSubRevenue().setPlan(target.getSubRevenue());
            dto.getProductivity().setPlan(target.getProductivity());
//            dto.getQuantity().setPlan(target.getQuantity());
            dto.getNbOrder().setPlan(target.getNbOrder());
            dto.setRevenueByOrderPlan(target.getRevenueByOrder());
            dto.setSkuByOrderPlan(target.getSkuByOrder());
            dto.setProductivityByOrderPlan(target.getProductivityByOrder());
            dto.getNewCustomer().setPlan(target.getNewCustomer());
        }

        I_Config config = getConfig(userLogin);
        String clientId = userLogin.getClientId();
        OrderDateType orderDateType = config.getOrderDateType();

        // ORDER
        I_OrderSummary orderSummaryMonthly = orderSummaryRepository.getOrderSummary(clientId, distributorId, salesmanId,
                null, period, orderDateType);
        dto.getNbOrder().incrementActual(orderSummaryMonthly.getNbOrder());
        dto.getRevenue().incrementActual(orderSummaryMonthly.getRevenue());
        dto.getSubRevenue().incrementActual(orderSummaryMonthly.getSubRevenue() == null ? BigDecimal.ZERO : orderSummaryMonthly.getSubRevenue());
        dto.getProductivity().incrementActual(orderSummaryMonthly.getProductivity());
        dto.incrementTotalSku(orderSummaryMonthly.getNbSKU());

        // PRODUCT
        I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);
        Map<String, BigDecimal> productSoldMap = orderSummaryRepository.getProductSoldMap(clientId, distributorId,
                salesmanId, null, period, orderDateType);
        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
        HashMap<String, ProductCategorySalesQuantityDto> productCategorySalesQuantityMap = new HashMap<>();
        for (I_Product product : products) {
            String productCategoryId = product.getProductCategory().getId();

            ProductCategorySalesQuantityDto productCategorySalesQuantity = productCategorySalesQuantityMap
                    .get(productCategoryId);
            if (productCategorySalesQuantity == null) {
                productCategorySalesQuantity = new ProductCategorySalesQuantityDto(product.getProductCategory());
                productCategorySalesQuantityMap.put(productCategoryId, productCategorySalesQuantity);
            }

            BigDecimal quantity = productSoldMap.get(product.getId());
            quantity = quantity == null ? BigDecimal.ZERO : quantity;

            ProductSalesQuantityDto productSalesQuantity = new ProductSalesQuantityDto(product, productPhotoFactory);
            productSalesQuantity.incrementQuantity(quantity);

            productCategorySalesQuantity.addProductsSalesQuantity(productSalesQuantity);
            productCategorySalesQuantity.incrementQuantity(productSalesQuantity.getQuantity());
        }
        List<I_Category> productCategories = productCategoryRepository.getAll(userLogin.getClientId(), null);
        List<ProductCategorySalesQuantityDto> productCategoriesSalesQuantity = new ArrayList<>(
                productCategories.size());
        for (I_Category productCategory : productCategories) {
            ProductCategorySalesQuantityDto temp = productCategorySalesQuantityMap.get(productCategory.getId());
            if (temp == null) {
                temp = new ProductCategorySalesQuantityDto(productCategory);
            }
            productCategoriesSalesQuantity.add(temp);
        }
        dto.setProductCategoriesSalesQuantity(productCategoriesSalesQuantity);

        // DAILY
        Map<String, I_OrderSummary> orderSummaryDaily = orderSummaryRepository.getOrderSummaryDaily(clientId,
                distributorId, salesmanId, null, period, orderDateType);
        List<SimpleDate> dates = period.getDates();
        List<SalesResultDailyDto> salesResultsDaily = new ArrayList<>(dates.size());
        for (SimpleDate date : dates) {
            String isoDate = date.getIsoDate();
            I_OrderSummary orderSummary = orderSummaryDaily.get(isoDate);

            SalesResultDailyDto salesResult = new SalesResultDailyDto(isoDate, new SalesResultDto());
            if (orderSummary != null) {
                salesResult.getSalesResult().incrementRevenue(orderSummary.getRevenue());
                salesResult.getSalesResult().incrementProductivity(orderSummary.getProductivity());
                salesResult.getSalesResult().incrementNbOrder(orderSummary.getNbOrder());
            }
            
            salesResultsDaily.add(salesResult);
        }
        dto.setSalesResultsDaily(salesResultsDaily);

        // VISIT
        I_VisitSummary visitSummary = visitSummaryRepository.getVisitSummary(clientId, distributorId, salesmanId, null,
                period);
        dto.getNbVisit().incrementActual(visitSummary.getNbVisit());
        dto.setNbVisitErrorPosition(visitSummary.getNbVisitErrorPosition());
        dto.setNbVisitErrorDuration(visitSummary.getNbVisitErrorDuration());
        dto.setNbVisitHasOrder(visitSummary.getNbVisitWithOrder());

        // NEW CUSTOMER
        long newCustomer = customerPendingRepository.countCustomersByCreatedUsers(userLogin.getClientId(),
                distributorId, salesmanIds, I_Customer.APPROVE_STATUS_APPROVED, null, null);
        dto.getNewCustomer().incrementActual((int) newCustomer);

        return dto;
    }

    private boolean checkSalesmanAccessible(UserLogin userLogin, String salesmanId) {
        Assert.notNull(salesmanId);

        List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
        Set<String> distributorIds = getIdSet(distributors);

        List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(), distributorIds);
        Set<String> salesmanIds = getIdSet(salesmen);

        return salesmanIds.contains(salesmanId);
    }

	@Override
	public PerformanceDto getPerformanceDailyBySalesman(UserLogin userLogin,String salesmanId, String _fromDate, String _toDate) {
		 	SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
	        SimpleDate toDate = getMandatoryIsoDate(_toDate);
	        BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
	        BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");
	        
	        I_User salesman = getMandatoryPO(userLogin, salesmanId, userRepository);
	        BusinessAssert.isTrue(checkSalesmanAccessible(userLogin, salesman.getId()), "salesman not accessible");

	        Collection<String> salesmanIds = Collections.singleton(salesman.getId());
	        String distributorId = salesman.getDistributor().getId();
	        Collection<String> distributorIds = Collections.singleton(distributorId);
	        
	        Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

	        List<SimpleDate> workingDays = cacheCalendarService.getWorkingDays(userLogin.getClientId(), period);

	        PerformanceDto dto = new PerformanceDto(salesman);
	        dto.setNbDay(workingDays.size());

	        // CALCULATE NB VISIT PLANNED
	        List<I_Route> routes = routeRepository.getRoutesBySalesmen(userLogin.getClientId(), distributorId, salesmanIds);
	        Map<String, Integer> nbVisitPlannedThisMonthMap = cacheScheduleService
	                .getNbVisitPlannedByRoute(userLogin.getClientId(), distributorIds, period);
	        int nbVisitPlannedThisMonth = 0;
	        for (I_Route route : routes) {
	            Integer temp = nbVisitPlannedThisMonthMap.get(route.getId());
	            temp = temp == null ? 0 : temp;
	            nbVisitPlannedThisMonth += temp;
	        }
	        dto.getNbVisit().setPlan(nbVisitPlannedThisMonth);

//	        I_Target target = targetRepository.getTargetBySalesman(userLogin.getClientId(), salesman.getId(), month, year);
//	        if (target != null) {
//	            dto.getRevenue().setPlan(target.getRevenue());
//	            dto.getSubRevenue().setPlan(target.getSubRevenue());
//	            dto.getProductivity().setPlan(target.getProductivity());
////	            dto.getQuantity().setPlan(target.getQuantity());
//	            dto.getNbOrder().setPlan(target.getNbOrder());
//	            dto.setRevenueByOrderPlan(target.getRevenueByOrder());
//	            dto.setSkuByOrderPlan(target.getSkuByOrder());
//	            dto.setProductivityByOrderPlan(target.getProductivityByOrder());
//	            dto.getNewCustomer().setPlan(target.getNewCustomer());
//	        }

	        I_Config config = getConfig(userLogin);
	        String clientId = userLogin.getClientId();
	        OrderDateType orderDateType = config.getOrderDateType();

	        // ORDER
	        I_OrderSummary orderSummaryMonthly = orderSummaryRepository.getOrderSummary(clientId, distributorId, salesmanId,
	                null, period, orderDateType);
	        dto.getNbOrder().incrementActual(orderSummaryMonthly.getNbOrder());
	        dto.getRevenue().incrementActual(orderSummaryMonthly.getRevenue());
	        dto.getSubRevenue().incrementActual(orderSummaryMonthly.getSubRevenue() == null ? BigDecimal.ZERO : orderSummaryMonthly.getSubRevenue());
	        dto.getProductivity().incrementActual(orderSummaryMonthly.getProductivity());
	        dto.incrementTotalSku(orderSummaryMonthly.getNbSKU());

	        // PRODUCT
	        I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);
	        Map<String, BigDecimal> productSoldMap = orderSummaryRepository.getProductSoldMap(clientId, distributorId,
	                salesmanId, null, period, orderDateType);
	        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
	        HashMap<String, ProductCategorySalesQuantityDto> productCategorySalesQuantityMap = new HashMap<>();
	        for (I_Product product : products) {
	            String productCategoryId = product.getProductCategory().getId();

	            ProductCategorySalesQuantityDto productCategorySalesQuantity = productCategorySalesQuantityMap
	                    .get(productCategoryId);
	            if (productCategorySalesQuantity == null) {
	                productCategorySalesQuantity = new ProductCategorySalesQuantityDto(product.getProductCategory());
	                productCategorySalesQuantityMap.put(productCategoryId, productCategorySalesQuantity);
	            }

	            BigDecimal quantity = productSoldMap.get(product.getId());
	            quantity = quantity == null ? BigDecimal.ZERO : quantity;

	            ProductSalesQuantityDto productSalesQuantity = new ProductSalesQuantityDto(product, productPhotoFactory);
	            productSalesQuantity.incrementQuantity(quantity);

	            productCategorySalesQuantity.addProductsSalesQuantity(productSalesQuantity);
	            productCategorySalesQuantity.incrementQuantity(productSalesQuantity.getQuantity());
	        }
	        List<I_Category> productCategories = productCategoryRepository.getAll(userLogin.getClientId(), null);
	        List<ProductCategorySalesQuantityDto> productCategoriesSalesQuantity = new ArrayList<>(
	                productCategories.size());
	        for (I_Category productCategory : productCategories) {
	            ProductCategorySalesQuantityDto temp = productCategorySalesQuantityMap.get(productCategory.getId());
	            if (temp == null) {
	                temp = new ProductCategorySalesQuantityDto(productCategory);
	            }
	            productCategoriesSalesQuantity.add(temp);
	        }
	        dto.setProductCategoriesSalesQuantity(productCategoriesSalesQuantity);

	        // DAILY
	        Map<String, I_OrderSummary> orderSummaryDaily = orderSummaryRepository.getOrderSummaryDaily(clientId,
	                distributorId, salesmanId, null, period, orderDateType);
	        List<SimpleDate> dates = period.getDates();
	        List<SalesResultDailyDto> salesResultsDaily = new ArrayList<>(dates.size());
	        for (SimpleDate date : dates) {
	            String isoDate = date.getIsoDate();
	            I_OrderSummary orderSummary = orderSummaryDaily.get(isoDate);

	            SalesResultDailyDto salesResult = new SalesResultDailyDto(isoDate, new SalesResultDto());
	            if (orderSummary != null) {
	                salesResult.getSalesResult().incrementRevenue(orderSummary.getRevenue());
	                salesResult.getSalesResult().incrementProductivity(orderSummary.getProductivity());
	                salesResult.getSalesResult().incrementNbOrder(orderSummary.getNbOrder());
	            }
	            
	            salesResultsDaily.add(salesResult);
	        }
	        dto.setSalesResultsDaily(salesResultsDaily);

	        // VISIT
	        I_VisitSummary visitSummary = visitSummaryRepository.getVisitSummary(clientId, distributorId, salesmanId, null,
	                period);
	        dto.getNbVisit().incrementActual(visitSummary.getNbVisit());
	        dto.setNbVisitErrorPosition(visitSummary.getNbVisitErrorPosition());
	        dto.setNbVisitErrorDuration(visitSummary.getNbVisitErrorDuration());
	        dto.setNbVisitHasOrder(visitSummary.getNbVisitWithOrder());

	        // NEW CUSTOMER
	        long newCustomer = customerPendingRepository.countCustomersByCreatedUsers(userLogin.getClientId(),
	                distributorId, salesmanIds, I_Customer.APPROVE_STATUS_APPROVED, null, null);
	        dto.getNewCustomer().incrementActual((int) newCustomer);

	        return dto;

		
	}

}
