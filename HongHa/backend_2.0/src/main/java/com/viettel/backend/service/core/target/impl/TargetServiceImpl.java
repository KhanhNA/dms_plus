package com.viettel.backend.service.core.target.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.target.TargetCreateDto;
import com.viettel.backend.dto.target.TargetDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pdto.Target;
import com.viettel.backend.service.core.target.TargetService;
import com.viettel.repository.common.TargetRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.category.I_Target;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.SUPERVISOR })
@Service
public class TargetServiceImpl extends AbstractService implements TargetService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TargetRepository targetRepository;

	@Override
	public ListDto<TargetDto> getTargets(UserLogin userLogin, int month, int year, String distributorId) {
		Set<String> distributorIds = new HashSet<String>();
		if (userLogin.getRole().equalsIgnoreCase(Role.ADVANCE_SUPERVISOR)) {
			Assert.notNull(distributorId);
			distributorIds.add(distributorId);
		} else {
			List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
			distributorIds = getIdSet(distributors);
		}
		List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(), distributorIds);
		Set<String> salesmanIds = getIdSet(salesmen);

		List<I_Target> targets = targetRepository.getTargetsBySalesmen(userLogin.getClientId(), salesmanIds, month,
				year, true);
		HashMap<String, I_Target> targetBySalesman = new HashMap<>();
		if (targets != null) {
			for (I_Target target : targets) {
				targetBySalesman.put(target.getSalesman().getId(), target);
			}
		}

		List<TargetDto> dtos = new LinkedList<TargetDto>();
		for (I_User salesman : salesmen) {
			I_Target target = targetBySalesman.get(salesman.getId());
			if (target != null) {
				dtos.add(new TargetDto(target));
			} else {
				dtos.add(new TargetDto(salesman, month, year));
			}
		}

		return new ListDto<TargetDto>(dtos);
	}

	@Override
	public TargetDto getTarget(UserLogin userLogin, int month, int year, String _salesmanId) {
		I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);

		BusinessAssert.isTrue(checkSalesmanAccessible(userLogin, salesman.getId()), "salesman not accessible");

		I_Target target = targetRepository.getTargetBySalesman(userLogin.getClientId(), salesman.getId(), month, year);

		if (target != null) {
			return new TargetDto(target);
		} else {
			return new TargetDto(salesman, month, year);
		}
	}

	@Override
	public IdDto save(UserLogin userLogin, TargetCreateDto dto) {
		BusinessAssert.notNull(dto);
		BusinessAssert.notNull(dto.getYear());
		BusinessAssert.notNull(dto.getMonth());
		BusinessAssert.notNull(dto.getSalesmanId());
		BusinessAssert.notNull(dto.getRevenue());

		BusinessAssert.isTrue(dto.getMonth() >= 0 && dto.getMonth() <= 11, "invalid month");
		BusinessAssert.isTrue(dto.getYear() >= 1900, "invalid year");

		I_User salesman = getMandatoryPO(userLogin, dto.getSalesmanId(), userRepository);
		BusinessAssert.isTrue(checkSalesmanAccessible(userLogin, salesman.getId()), "salesman not accessible");

		I_Target _target = targetRepository.getTargetBySalesman(userLogin.getClientId(), salesman.getId(),
				dto.getMonth(), dto.getYear());

		Target target;
		if (_target == null) {
			target = new Target();
			initiatePO(userLogin.getClientId(), target);
			target.setMonth(dto.getMonth());
			target.setYear(dto.getYear());
			target.setDistributor(salesman.getDistributor());
			target.setSalesman(salesman);
		} else {
			target = new Target(_target);
		}

		target.setRevenue(dto.getRevenue() == null ? BigDecimal.ZERO : dto.getRevenue());
		target.setSubRevenue(dto.getSubRevenue() == null ? BigDecimal.ZERO : dto.getSubRevenue());
		target.setQuantity(dto.getQuantity() == null ? BigDecimal.ZERO : dto.getQuantity());
		target.setProductivity(dto.getProductivity() == null ? BigDecimal.ZERO : dto.getProductivity());
		target.setNbOrder(dto.getNbOrder());
		target.setRevenueByOrder(dto.getRevenueByOrder() == null ? BigDecimal.ZERO : dto.getRevenueByOrder());
		target.setSkuByOrder(dto.getSkuByOrder() == null ? BigDecimal.ZERO : dto.getSkuByOrder());
		target.setProductivityByOrder(
				dto.getProductivityByOrder() == null ? BigDecimal.ZERO : dto.getProductivityByOrder());
		target.setNewCustomer(dto.getNewCustomer());

		_target = targetRepository.save(userLogin.getClientId(), target);

		return new IdDto(_target);
	}

	@Override
	public boolean delete(UserLogin userLogin, int month, int year, String salesmanId) {
		I_User salesman = getMandatoryPO(userLogin, salesmanId, userRepository);
		BusinessAssert.isTrue(checkSalesmanAccessible(userLogin, salesman.getId()), "salesman not accessible");

		I_Target target = targetRepository.getTargetBySalesman(userLogin.getClientId(), salesman.getId(), month, year);
		if (target != null) {
			return targetRepository.delete(userLogin.getClientId(), target.getId());
		} else {
			return true;
		}
	}

	private boolean checkSalesmanAccessible(UserLogin userLogin, String salesmanId) {
		Assert.notNull(salesmanId);

		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		Set<String> distributorIds = getIdSet(distributors);

		List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(), distributorIds);
		Set<String> salesmanIds = getIdSet(salesmen);

		return salesmanIds.contains(salesmanId);
	}

}
