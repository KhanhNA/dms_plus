package com.viettel.backend.service.core.user;

import java.util.List;

import com.viettel.backend.dto.common.CategorySelectionDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.region.RegionSelectionDto;
import com.viettel.backend.dto.user.UserCreateDto;
import com.viettel.backend.dto.user.UserDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface UserEditableService {

    /** Get list of POs with <code>draft=any</code> and <code>active=any</code> */
    public ListDto<UserDto> getList(UserLogin userLogin, String search, Boolean active, Boolean draft, String role,
            String distributorId, PageSizeRequest pageSizeRequest);

    /** Get PO detail with <code>draft=any</code> and <code>active=any</code> */
    public UserDto getById(UserLogin userLogin, String id);

    /** Create Draft PO with <code>draft=true</code> and <code>active=true</code> */
    public IdDto create(UserLogin userLogin, UserCreateDto createDto);

    /** Update PO with <code>draft=any</code> and <code>active=true</code> */
    public IdDto update(UserLogin userLogin, String id, UserCreateDto createDto);
    
    /** Create and Enable PO with <code>draft=any</code> and <code>active=true</code> */
    public IdDto createAndEnable(UserLogin userLogin, UserCreateDto createDto);
    
    /** Update and Enable PO with <code>draft=any</code> and <code>active=true</code> */
    public IdDto updateAndEnable(UserLogin userLogin, String id, UserCreateDto createDto);

    /** Enable Draft PO */
    public boolean enable(UserLogin userLogin, String id);

    /** Delete Draft PO */
    public boolean delete(UserLogin userLogin, String id);
    
    /** Set PO <code>active</code> status, only affected PO with <code>draft=false</code> */
    public void setActive(UserLogin userLogin, String id, boolean active);
    
    public void resetPassword(UserLogin userLogin, String id);

    public ListDto<CategorySelectionDto> getDistributorOfObserver(UserLogin userLogin, String id);

    public void updateDistributorSetForObserver(UserLogin userLogin, String id, List<String> distributorIds);
        
    public ListDto<CategorySelectionDto> getRegionsOfAdvanceSupervisor(UserLogin userLogin, String id);
    
    public void updateRegionSetForAdvanceSupervisor(UserLogin userLogin, String id, List<String> regionIds);
    
    public ListDto<RegionSelectionDto> getRegionHierarchyOfAdvanceSupervisor(UserLogin userLogin,String id);
}
