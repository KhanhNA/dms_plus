package com.viettel.backend.service.core.user;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface UserReadonlyService {

	public ListDto<UserSimpleDto> getSupervisors(UserLogin userLogin);

	public ListDto<UserSimpleDto> getAdvanceSupervisors(UserLogin userLogin);

	public UserSimpleDto getSupervisor(UserLogin userLogin, String id);

	public ListDto<UserSimpleDto> getSalesmen(UserLogin userLogin, String _distributorId);

	public UserSimpleDto getSalesman(UserLogin userLogin, String id);

	public ListDto<UserSimpleDto> getObservers(UserLogin userLogin);

	public ListDto<UserSimpleDto> getSupervisorsInAccessibleRegion(UserLogin userLogin);

	public ListDto<UserSimpleDto> getObserversInAccessibleRegion(UserLogin userLogin);

}
