package com.viettel.backend.service.core.user.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.CategorySelectionDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.region.RegionSelectionDto;
import com.viettel.backend.dto.user.UserCreateDto;
import com.viettel.backend.dto.user.UserDto;
import com.viettel.backend.event.UserDeactivationEvent;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractPOEditableService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.User;
import com.viettel.backend.service.core.user.UserEditableService;
import com.viettel.backend.util.PasswordUtils;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.POBasicRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;

import reactor.bus.EventBus;

@RolePermission(value = { Role.ADMIN })
@Service
public class UserEditableServiceImpl extends AbstractPOEditableService<I_User, I_User, UserDto, UserDto, UserCreateDto>
		implements UserEditableService {

	@Autowired
	private EventBus eventBus;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private RouteRepository routeRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ConfigRepository configRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Override
	protected POBasicRepository<I_User, I_User> getRepository() {
		return userRepository;
	}

	protected void checkAccessible(UserLogin userLogin, I_User domain) {
		BusinessAssert.notTrue(domain.isDefaultAdmin(), "cannot access default admin");
	}

	protected void beforeSetActive(UserLogin userLogin, I_User user, boolean active) {
		if (active) {
			// ACTIVE
			String clientId = userLogin.getClientId();
			I_Config config = configRepository.getConfig(clientId);
			int limitAccount = config.getLimitAccount();
			long numberOfUsers = userRepository.count(clientId, false, true, null, null, null, null);
			if (limitAccount != 0 && limitAccount < numberOfUsers) {
				throw new BusinessException(BusinessExceptionCode.EXCEED_MAXIMUM_VALUE_USER);
			}
		} else {
			// DEACTIVE
			if (user.getId().equals(userLogin.getUserId())) {
				throw new BusinessException(BusinessExceptionCode.CANNOT_CHANGE_CURRENT_USER);
			}

			if (user.getRole().equals(Role.SUPERVISOR)) {
				BusinessAssert.notTrue(distributorRepository.checkSupervisorUsed(userLogin.getClientId(), user.getId()),
						BusinessExceptionCode.RECORD_USED_IN_DISTRIBUTOR, "supervisor used in distributor");
			} else if (user.getRole().equals(Role.SALESMAN)) {
				BusinessAssert
						.notTrue(
								routeRepository.checkSalesmanUsed(userLogin.getClientId(),
										user.getDistributor().getId(), user.getId()),
								BusinessExceptionCode.RECORD_USED_IN_ROUTE, "salesman used in route");
			}
		}
	}

	protected void afterSetActive(UserLogin userLogin, I_User domain, boolean active) {
		// If deactivating user, log them out
		if (!active) {
			eventBus.notify(UserDeactivationEvent.EVENT_NAME, new UserDeactivationEvent(domain));
		}
	}

	@Override
	protected UserDto createDetailDto(UserLogin userLogin, I_User domain) {
		return new UserDto(domain);
	}
	/**
	 * Allow edit fullname of user
	 * @since 2016-12-18
	 */
	@Override
	public IdDto update(UserLogin userLogin, String _id, UserCreateDto createDto) {
		   BusinessAssert.notNull(createDto, "Create DTO cannot be null");
		   I_User oldDomain = getMandatoryPO(userLogin, _id, null, true, getRepository());
		   I_User domain = getObjectForUpdate(userLogin, oldDomain, createDto);
	       return new IdDto(getRepository().save(userLogin.getClientId(), domain));
	}
	@Override
	public I_User getObjectForCreate(UserLogin userLogin, UserCreateDto createdto) {
		BusinessAssert.notNull(createdto);
		String clientId = userLogin.getClientId();
		I_Config config = configRepository.getConfig(clientId);
		int limitAccount = config.getLimitAccount();
		long numberOfUsers = userRepository.count(clientId, false, true, null, null, null, null);

		if (limitAccount != 0 && limitAccount < numberOfUsers) {
			throw new BusinessException(BusinessExceptionCode.EXCEED_MAXIMUM_VALUE_USER);
		}
		User user = new User();
		initiatePO(userLogin.getClientId(), user);
		user.setDraft(true);
		user.setPassword(PasswordUtils.getDefaultPassword(passwordEncoder));

		return getObjectForUpdate(userLogin, user, createdto);
	}

	@Override
	public I_User getObjectForUpdate(UserLogin userLogin, I_User oldDomain, UserCreateDto createdto) {
		// Not allow update non draft record
//		BusinessAssert.isTrue(oldDomain.isDraft(), "Modify non-draft record are not allowed");
		String clientId = userLogin.getClientId();
		I_Config config = configRepository.getConfig(clientId);
		int limitAccount = config.getLimitAccount();
		long numberOfUsers = userRepository.count(clientId, false, true, null, null, null, null);

		if (limitAccount != 0 && limitAccount < numberOfUsers) {
			throw new BusinessException(BusinessExceptionCode.EXCEED_MAXIMUM_VALUE_USER);
		}
		BusinessAssert.notNull(createdto);
		BusinessAssert.notNull(createdto.getUsername());
		BusinessAssert.notNull(createdto.getFullname());
		BusinessAssert.notNull(createdto.getRole());

		User user = new User(oldDomain);

		BusinessAssert.notTrue(
				userRepository.checkUsernameExist(userLogin.getClientId(), user.getId(), createdto.getUsername()),
				BusinessExceptionCode.USERNAME_USED, "username already exists");

		BusinessAssert.notTrue(
				userRepository.checkFullnameExist(userLogin.getClientId(), user.getId(), createdto.getFullname()),
				BusinessExceptionCode.FULLNAME_USED, "fullname already exists");

		user.setUsername(userLogin.getClientCode(), createdto.getUsername());
		user.setFullname(createdto.getFullname());
		user.setRole(createdto.getRole());

		if (user.getRole().equals(Role.DISTRIBUTOR_ADMIN) || user.getRole().equals(Role.SALESMAN)) {
			I_Distributor distributor = getMandatoryPO(userLogin, createdto.getDistributorId(), distributorRepository);
			user.setDistributor(new CategoryEmbed(distributor));
		}

		if (user.getRole().equals(Role.SALESMAN)) {
			user.setVanSales(createdto.isVanSales());
		}

		return user;
	}

	@Override
	public ListDto<UserDto> getList(UserLogin userLogin, String search, Boolean active, Boolean draft, String role,
			String distributorId, PageSizeRequest pageSizeRequest) {
		String searchFullname = search;
		String searchUsername = search == null ? null : search.toLowerCase();

		if (role != null) {
			BusinessAssert.isTrue(role.equals(Role.ADMIN) || role.equals(Role.OBSERVER) || role.equals(Role.SUPERVISOR)
					|| role.equals(Role.DISTRIBUTOR_ADMIN) || role.equals(Role.SALESMAN)
					|| role.equals(Role.ADVANCE_SUPERVISOR));

			if (role.equals(Role.DISTRIBUTOR_ADMIN) || role.equals(Role.SALESMAN)) {
				if (distributorId != null) {
					I_Distributor distributor = getMandatoryPO(userLogin, distributorId, distributorRepository);
					distributorId = distributor.getId();
				}
			}
		}

		List<I_User> domains = userRepository.getList(userLogin.getClientId(), draft, active, role, distributorId,
				searchFullname, searchUsername, pageSizeRequest, true);
		if (CollectionUtils.isEmpty(domains) && pageSizeRequest.getPageNumber() == 0) {
			return ListDto.emptyList();
		}

		List<UserDto> dtos = new ArrayList<UserDto>(domains.size());
		for (I_User domain : domains) {
			dtos.add(new UserDto(domain));
		}

		long size = Long.valueOf(dtos.size());
		if (pageSizeRequest != null) {
			if (pageSizeRequest.getPageNumber() > 0 || size == pageSizeRequest.getPageSize()) {
				size = userRepository.count(userLogin.getClientId(), draft, active, role, distributorId, searchFullname,
						searchUsername);
			}
		}

		return new ListDto<UserDto>(dtos, size);
	}

	@Override
	public void resetPassword(UserLogin userLogin, String id) {
		BusinessAssert.isTrue(userRepository.exists(userLogin.getClientId(), null, null, id));
		userRepository.changePassword(userLogin.getClientId(), id, PasswordUtils.getDefaultPassword(passwordEncoder));
	}

	@Override
	public ListDto<CategorySelectionDto> getDistributorOfObserver(UserLogin userLogin, String id) {
		I_User user = getMandatoryPO(userLogin, id, userRepository);

		BusinessAssert.equals(user.getRole(), Role.OBSERVER);

		List<I_Distributor> distributors = userRepository.getDistributorsOfManager(userLogin.getClientId(), id);
		Set<String> distributorIds = getIdSet(distributors);
		distributorIds = distributorIds == null ? Collections.<String>emptySet() : distributorIds;

		List<I_Distributor> allDistributors = distributorRepository.getAll(userLogin.getClientId(), null);
		List<CategorySelectionDto> dtos = new ArrayList<>(distributors.size());
		for (I_Distributor distributor : allDistributors) {
			dtos.add(new CategorySelectionDto(distributor, distributorIds.contains(distributor.getId())));
		}

		return new ListDto<>(dtos);
	}

	@Override
	public void updateDistributorSetForObserver(UserLogin userLogin, String id, List<String> distributorIds) {
		I_User user = getMandatoryPO(userLogin, id, userRepository);

		BusinessAssert.equals(user.getRole(), Role.OBSERVER);

		if (distributorIds == null || distributorIds.isEmpty()) {
			userRepository.setDistributorsOfManager(userLogin.getClientId(), id, Collections.emptySet());
		} else {
			List<I_Distributor> distributors = distributorRepository.getListByIds(userLogin.getClientId(),
					distributorIds);
			BusinessAssert.equals(distributorIds.size(), distributors.size());

			userRepository.setDistributorsOfManager(userLogin.getClientId(), id, distributorIds);
		}
	}

	@Override
	public ListDto<CategorySelectionDto> getRegionsOfAdvanceSupervisor(UserLogin userLogin, String id) {
		I_User user = getMandatoryPO(userLogin, id, userRepository);
		BusinessAssert.equals(user.getRole(), Role.ADVANCE_SUPERVISOR);
		List<I_Region> regions = userRepository.getRegionsOfManager(userLogin.getClientId(), id);
		List<CategorySelectionDto> dtos = new ArrayList<>(regions.size());

		for (I_Region region : regions) {
			dtos.add(new CategorySelectionDto(region, true));
		}

		return new ListDto<>(dtos);
	}

	@Override
	public void updateRegionSetForAdvanceSupervisor(UserLogin userLogin, String id, List<String> regionIds) {
		I_User user = getMandatoryPO(userLogin, id, userRepository);

		BusinessAssert.equals(user.getRole(), Role.ADVANCE_SUPERVISOR);

		if (regionIds == null || regionIds.isEmpty()) {
			userRepository.setRegionsOfManager(userLogin.getClientId(), id, Collections.emptySet());
		} else {
			List<I_Region> regions = regionRepository.getListByIds(userLogin.getClientId(), regionIds);
			BusinessAssert.equals(regionIds.size(), regions.size());

			userRepository.setRegionsOfManager(userLogin.getClientId(), id, regionIds);
		}
	}

	@Override
	public ListDto<RegionSelectionDto> getRegionHierarchyOfAdvanceSupervisor(UserLogin userLogin, String id) {
		I_User user = getMandatoryPO(userLogin, id, userRepository);
		BusinessAssert.equals(user.getRole(), Role.ADVANCE_SUPERVISOR);
		List<I_Region> regions = userRepository.getRegionsOfManager(userLogin.getClientId(), id);
		List<String> regionIds = new ArrayList<>();
		for (I_Region r : regions) {
			regionIds.add(r.getId());
		}

		I_Region root = regionRepository.getRootNode(userLogin.getClientId());
		RegionSelectionDto tree = new RegionSelectionDto(root);
		transerval(tree, root, userLogin, regionIds, false);
		List<RegionSelectionDto> lst = new ArrayList<RegionSelectionDto>(2);
		lst.add(tree);
		return new ListDto<RegionSelectionDto>(lst);

	}

	private void transerval(RegionSelectionDto tree, I_Region node, UserLogin userLogin, List<String> manageRegionIds,
			boolean isAncentorManaged) {
		isAncentorManaged = isAncentorManaged || manageRegionIds.contains(node.getId());
		tree.setName(node.getName());
		tree.setId(node.getId());
		tree.setSelected(isAncentorManaged);

		if (node.getChildren() == null || node.getChildren().isEmpty()) {
			tree.setChildren(Collections.emptyList());
			return;
		}

		for (I_CategoryEmbed item : node.getChildren()) {
			I_Region data = getMandatoryPO(userLogin, item.getId(), regionRepository);
			RegionSelectionDto child = new RegionSelectionDto(data);
			transerval(child, data, userLogin, manageRegionIds, isAncentorManaged);
			tree.addChild(child);
		}
	}

}
