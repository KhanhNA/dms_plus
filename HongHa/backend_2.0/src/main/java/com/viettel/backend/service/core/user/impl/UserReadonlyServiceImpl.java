package com.viettel.backend.service.core.user.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.user.UserSimpleDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.user.UserReadonlyService;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;

@Service
public class UserReadonlyServiceImpl extends AbstractService implements UserReadonlyService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private RegionRepository regionRepository;

	@RolePermission(value = { Role.ADMIN })
	@Override
	public ListDto<UserSimpleDto> getSupervisors(UserLogin userLogin) {
		List<I_User> supervisors = userRepository.getUserByRole(userLogin.getClientId(), Role.SUPERVISOR);
		if (supervisors == null || supervisors.isEmpty()) {
			return ListDto.emptyList();
		}

		List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(supervisors.size());
		for (I_User supervisor : supervisors) {
			dtos.add(new UserSimpleDto(supervisor));
		}

		return new ListDto<UserSimpleDto>(dtos);
	}

	@RolePermission(value = { Role.ADMIN })
	@Override
	public ListDto<UserSimpleDto> getAdvanceSupervisors(UserLogin userLogin) {
		List<I_User> advanceSupervisors = userRepository.getUserByRole(userLogin.getClientId(),
				Role.ADVANCE_SUPERVISOR);
		if (advanceSupervisors == null || advanceSupervisors.isEmpty()) {
			return ListDto.emptyList();
		}

		List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(advanceSupervisors.size());
		for (I_User as : advanceSupervisors) {
			dtos.add(new UserSimpleDto(as));
		}

		return new ListDto<UserSimpleDto>(dtos);
	}

	@RolePermission(value = { Role.ADMIN })
	@Override
	public UserSimpleDto getSupervisor(UserLogin userLogin, String id) {
		I_User user = getMandatoryPO(userLogin, id, userRepository);

		BusinessAssert.equals(user.getRole(), Role.SUPERVISOR);

		return new UserSimpleDto(user);
	}

	@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN,
			Role.ADVANCE_SUPERVISOR })
	@Override
	public ListDto<UserSimpleDto> getSalesmen(UserLogin userLogin, String _distributorId) {
		Set<String> distributorIds = new HashSet<>();
		I_Distributor distributor = null;
		if (_distributorId == null) {
			distributor = getDefaultDistributor(userLogin);
			if (distributor == null) {
				distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
				BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accisible");
			}
		} else if ("all".equalsIgnoreCase(_distributorId)) {
			distributorIds = getIdSet(getAccessibleDistributors(userLogin));
		} else if (_distributorId.startsWith("all_")) {
			_distributorId = _distributorId.substring(_distributorId.indexOf("_") + 1);
			if ("all".equalsIgnoreCase(_distributorId)) {
				distributorIds = getIdSet(getAccessibleDistributors(userLogin));
			} else {
				I_Region region = regionRepository.getById(userLogin.getClientId(), _distributorId);
				distributorIds = PO.convertIdsToSet(userRepository
						.getDistributorForRegionRecursively(userLogin.getClientId(), region, new ArrayList<>()));
			}
		} else {
			distributor = getDefaultDistributor(userLogin);
			if (distributor == null) {
				distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
				BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accisible");
			}
		}
		if (distributor != null) {
			distributorIds.add(distributor.getId());
		}

		List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(), distributorIds);
		if (salesmen == null || salesmen.isEmpty()) {
			return ListDto.emptyList();
		}

		List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(salesmen.size());
		for (I_User salesman : salesmen) {
			dtos.add(new UserSimpleDto(salesman));
		}

		return new ListDto<UserSimpleDto>(dtos);
	}

	@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN,
			Role.ADVANCE_SUPERVISOR })
	@Override
	public UserSimpleDto getSalesman(UserLogin userLogin, String id) {
		I_User user = getMandatoryPO(userLogin, id, userRepository);

		BusinessAssert.equals(user.getRole(), Role.SALESMAN);
		BusinessAssert.notNull(user.getDistributor());
		BusinessAssert.isTrue(checkAccessible(userLogin, user.getDistributor().getId()));

		return new UserSimpleDto(user);
	}

	@RolePermission(value = { Role.ADMIN })
	@Override
	public ListDto<UserSimpleDto> getObservers(UserLogin userLogin) {
		List<I_User> observers = userRepository.getUserByRole(userLogin.getClientId(), Role.OBSERVER);
		if (observers == null || observers.isEmpty()) {
			return ListDto.emptyList();
		}

		List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(observers.size());
		for (I_User observer : observers) {
			dtos.add(new UserSimpleDto(observer));
		}

		return new ListDto<UserSimpleDto>(dtos);
	}

	@RolePermission(value = { Role.ADVANCE_SUPERVISOR })
	@Override
	public ListDto<UserSimpleDto> getSupervisorsInAccessibleRegion(UserLogin userLogin) {
		HashSet<String> supervisorIds = new HashSet<>();
		List<I_Distributor> distributors = userRepository.getDistributorsOfManager(userLogin.getClientId(),
				userLogin.getUserId());
		for (I_Distributor distributor : distributors) {
			supervisorIds.add(distributor.getSupervisor().getId());
		}
		List<I_User> supervisors = userRepository.getListByIds(userLogin.getClientId(), supervisorIds);
		if (supervisors == null || supervisors.isEmpty()) {
			return ListDto.emptyList();
		}

		List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(supervisors.size());
		for (I_User supervisor : supervisors) {
			dtos.add(new UserSimpleDto(supervisor));
		}

		return new ListDto<UserSimpleDto>(dtos);
	}

	@RolePermission(value = { Role.ADVANCE_SUPERVISOR })
	@Override
	public ListDto<UserSimpleDto> getObserversInAccessibleRegion(UserLogin userLogin) {
		List<I_Distributor> distributors = userRepository.getDistributorsOfManager(userLogin.getClientId(),
				userLogin.getUserId());
		List<I_User> observers = userRepository.getUserByRole(userLogin.getClientId(), Role.OBSERVER,
				getIdSet(distributors));
		if (observers == null || observers.isEmpty()) {
			return ListDto.emptyList();
		}

		List<UserSimpleDto> dtos = new ArrayList<UserSimpleDto>(observers.size());
		for (I_User observer : observers) {
			dtos.add(new UserSimpleDto(observer));
		}

		return new ListDto<UserSimpleDto>(dtos);
	}

}
