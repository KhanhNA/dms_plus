package com.viettel.backend.service.core.vansales.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.user.UserDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.vansales.VanSalesService;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.ADMIN, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class VanSalesServiceImpl extends AbstractService implements VanSalesService {

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ListDto<UserDto> getSalesman(UserLogin userLogin, String _distributorId) {
        I_Distributor distributor = getDefaultDistributor(userLogin);
        if (distributor == null) {
            distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
            BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");
        }

        List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(),
                Collections.singleton(distributor.getId()));
        List<UserDto> dtos = new ArrayList<>(salesmen.size());

        for (I_User salesman : salesmen) {
            dtos.add(new UserDto(salesman));
        }

        return new ListDto<>(dtos);
    }

    @Override
    public void updateVanSalesStatus(UserLogin userLogin, String _distributorId, Map<String, Boolean> vanSalesStatus) {
        I_Distributor distributor = getDefaultDistributor(userLogin);
        if (distributor == null) {
            distributor = getMandatoryPO(userLogin, _distributorId, distributorRepository);
            BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");
        }

        List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(),
                Collections.singleton(distributor.getId()));
        Map<String, I_User> salesmanMap = getPOMap(salesmen);

        Set<String> vanSales = new HashSet<>();
        Set<String> noVanSales = new HashSet<>();

        for (Entry<String, Boolean> entry : vanSalesStatus.entrySet()) {
            String salesmanId = entry.getKey();
            BusinessAssert.isTrue(salesmanMap.containsKey(salesmanId));
            if (entry.getValue() != null && entry.getValue()) {
                vanSales.add(salesmanId);
            } else {
                noVanSales.add(salesmanId);
            }
        }

        userRepository.updateVanSalesStatus(userLogin.getClientId(), vanSales, true);
        userRepository.updateVanSalesStatus(userLogin.getClientId(), noVanSales, false);
    }

}
