package com.viettel.backend.service.core.visit;

import java.io.InputStream;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface VisitExportService {

    public InputStream exportVisit(UserLogin userLogin, String distributorId, String salesmanId, String customerId,
            String fromDate, String toDate, String lang);

    public InputStream exportFeedback(UserLogin userLogin, String distributorId, String fromDate, String toDate,
            String lang);

	public ExportDto exportVisit(UserLogin userLogin, String lang, int errorVisitDuration);

}
