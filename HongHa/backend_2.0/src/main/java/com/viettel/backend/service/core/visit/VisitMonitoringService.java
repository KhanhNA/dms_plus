package com.viettel.backend.service.core.visit;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.visit.CustomerForVisitDto;
import com.viettel.backend.dto.visit.VisitInfoDto;
import com.viettel.backend.dto.visit.VisitInfoListDto;
import com.viettel.backend.dto.visit.VisitTodaySummary;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface VisitMonitoringService {

    public ListDto<CustomerForVisitDto> getCustomersTodayBySalesman(UserLogin userLogin, String _salesmanId);

    public ListDto<VisitInfoListDto> getVisitsToday(UserLogin userLogin, String _distributorId, String _salesmanId,
            PageSizeRequest pageSizeRequest);

    public ListDto<VisitInfoListDto> getVisits(UserLogin userLogin, String _distributorId, String _salesmanId,
            String _customerId, String fromDate, String toDate, PageSizeRequest pageSizeRequest);

    public VisitInfoDto getVisitInfoById(UserLogin userLogin, String id);

    public VisitTodaySummary getVisitTodaySummary(UserLogin userLogin, String _distributorId, String _salesmanId);

}
