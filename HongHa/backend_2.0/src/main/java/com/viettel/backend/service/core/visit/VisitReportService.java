package com.viettel.backend.service.core.visit;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.visit.DistributorVisitResultDto;
import com.viettel.backend.dto.visit.SalesmanVisitResultDto;
import com.viettel.backend.dto.visit.VisitResultDailyDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface VisitReportService {

    public ListDto<VisitResultDailyDto> getVisitResultDaily(UserLogin userLogin, String regionId,String distributorId, String fromDate,
            String toDate);

    public ListDto<DistributorVisitResultDto> getDistributorVisitResult(UserLogin userLogin, String fromDate,
            String toDate);

    public ListDto<SalesmanVisitResultDto> getSalesmanVisitResult(UserLogin userLogin, String distributorId,
            String fromDate, String toDate);

}
