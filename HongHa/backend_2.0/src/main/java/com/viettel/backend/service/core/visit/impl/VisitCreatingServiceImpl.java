package com.viettel.backend.service.core.visit.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.survey.SurveyDto;
import com.viettel.backend.dto.visit.CustomerForVisitDto;
import com.viettel.backend.dto.visit.CustomerSummaryDto;
import com.viettel.backend.dto.visit.SurveyAnswerDto;
import com.viettel.backend.dto.visit.VisitClosingDto;
import com.viettel.backend.dto.visit.VisitEndDto;
import com.viettel.backend.dto.visit.VisitInfoDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pdto.SurveyAnswerContent;
import com.viettel.backend.service.core.pdto.Visit;
import com.viettel.backend.service.core.sub.OrderCalculationService;
import com.viettel.backend.service.core.sub.WebNotificationService;
import com.viettel.backend.service.core.visit.VisitCreatingService;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.CodeGeneratorRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.OrderRepository;
import com.viettel.repository.common.OrderSummaryRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.SurveyRepository;
import com.viettel.repository.common.VisitRepository;
import com.viettel.repository.common.VisitingRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.domain.data.I_OrderSummary;
import com.viettel.repository.common.domain.data.I_SurveyAnswerContent;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.domain.data.I_VisitHeader;
import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyOption;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;
import com.viettel.repository.common.util.LocationUtils;

@RolePermission(value = { Role.SALESMAN })
@Service
public class VisitCreatingServiceImpl extends AbstractService implements VisitCreatingService {

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private VisitingRepository visitingRepository;

    @Autowired
    private CodeGeneratorRepository codeGeneratorRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderSummaryRepository orderSummaryRepository;

    @Autowired
    private OrderCalculationService orderCalculationService;

    @Autowired
    private SurveyRepository surveyRepository;

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private CacheVisitOrderService cacheVisitOrderService;

    @Autowired
    private WebNotificationService webNotificationEngine;

    @Override
    public ListDto<CustomerForVisitDto> getCustomersForVisit(UserLogin userLogin, Boolean plannedToday, String search) {
        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();
        Set<String> salesmanIds = Collections.singleton(salesman.getId());

        SimpleDate today = DateTimeUtils.getToday();

        Set<String> routeIds = getIdSet(
                routeRepository.getRoutesBySalesmen(userLogin.getClientId(), distributorId, salesmanIds));

        List<I_Customer> customers;
        if (plannedToday != null && plannedToday) {
            customers = customerRepository.getCustomersByRoutes(userLogin.getClientId(), distributorId, routeIds,
                    today);
        } else {
            customers = customerRepository.getCustomersByRoutes(userLogin.getClientId(), distributorId, routeIds, null);
        }

        if (customers == null || customers.isEmpty()) {
            return ListDto.emptyList();
        }

        Set<String> customerIds = getIdSet(customers);

        // GET VISITED TODAY
        Map<String, I_VisitHeader> visitByCustomer = visitingRepository
                .getVisitHeaderByCustomerTodayMap(userLogin.getClientId(), distributorId, customerIds);

        // GET VISITED LAST WEEK FOR SMART ORDER
        List<I_VisitHeader> visitsLastWeek = visitRepository.getVisitByDistributor(userLogin.getClientId(),
                distributorId, salesman.getId(), null, DateTimeUtils.getPeriodOneDay(DateTimeUtils.addDays(today, -7)),
                null, true);

        List<String> customersVisitLastWeek = new ArrayList<String>(visitsLastWeek.size());
        if (visitsLastWeek != null && !visitsLastWeek.isEmpty()) {
            for (I_VisitHeader visit : visitsLastWeek) {
                customersVisitLastWeek.add(visit.getCustomer().getId());
            }
        }

        List<CustomerForVisitDto> dtos = new LinkedList<CustomerForVisitDto>();
        I_Config config = getConfig(userLogin);
        int weekIndex = config.getWeekIndex(today);
        int dayOfWeek = today.getDayOfWeek();
        for (I_Customer customer : customers) {
            boolean planned;
            if (plannedToday != null) {
                planned = plannedToday;
                if (!plannedToday) {
                    if (customer.checkScheduleDate(config, dayOfWeek, weekIndex)) {
                        continue;
                    }
                }
            } else {
                planned = customer.checkScheduleDate(config, dayOfWeek, weekIndex);
            }

            I_VisitHeader visit = visitByCustomer != null ? visitByCustomer.get(customer.getId()) : null;

            int seqNo = customers.size();
            if (planned) {
                int index = customersVisitLastWeek.indexOf(customer.getId());
                if (index > -1) {
                    seqNo = index;
                }
            }

            dtos.add(new CustomerForVisitDto(customer, planned, seqNo, visit));
        }

        return new ListDto<CustomerForVisitDto>(dtos);
    }

    @Override
    public CustomerSummaryDto getCustomerSummary(UserLogin userLogin, String id) {
        I_Customer customer = getMandatoryPO(userLogin, id, customerRepository);

        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "Customer is not accessible");

        String clientId = userLogin.getClientId();

        CustomerSummaryDto dto = new CustomerSummaryDto(customer);

        String distributorId = customer.getDistributor().getId();

        I_Config config = getConfig(userLogin);

        Period thisMonthPeriod = DateTimeUtils.getPeriodThisMonth();
        Period lastMonthPeriod = DateTimeUtils.getPeriodLastMonth();
        Period twoMonthAgoPeriod = DateTimeUtils.getPeriodLastsMonth(2);
        Period threeMonthAgoPeriod = DateTimeUtils.getPeriodLastsMonth(3);

        // THIS MONTH
        I_OrderSummary osThisMonth = orderSummaryRepository.getOrderSummary(clientId, distributorId, null,
                customer.getId(), thisMonthPeriod, config.getOrderDateType());
        dto.setProductivityThisMonth(osThisMonth.getProductivity());
        dto.setNbOrderThisMonth(osThisMonth.getNbOrder());

        List<Map<String, Object>> revenueLastThreeMonth = new ArrayList<Map<String, Object>>(3);
        // LAST MONTH
        I_OrderSummary osLastMonth = orderSummaryRepository.getOrderSummary(clientId, distributorId, null,
                customer.getId(), lastMonthPeriod, config.getOrderDateType());
        dto.setProductivityLastMonth(osLastMonth.getProductivity());
        Map<String, Object> revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", lastMonthPeriod.getFromDate().format("MM/yyyy"));
        revenueDto.put("revenue", osLastMonth.getRevenue());
        revenueLastThreeMonth.add(revenueDto);

        // 2 MONTH AGO
        I_OrderSummary os2MonthAgo = orderSummaryRepository.getOrderSummary(clientId, distributorId, null,
                customer.getId(), twoMonthAgoPeriod, config.getOrderDateType());
        revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", twoMonthAgoPeriod.getFromDate().format("MM/yyyy"));
        revenueDto.put("revenue", os2MonthAgo.getRevenue());
        revenueLastThreeMonth.add(revenueDto);

        // 3 MONTH AGO
        I_OrderSummary os3MonthAgo = orderSummaryRepository.getOrderSummary(clientId, distributorId, null,
                customer.getId(), threeMonthAgoPeriod, config.getOrderDateType());
        revenueDto = new HashMap<String, Object>();
        revenueDto.put("month", threeMonthAgoPeriod.getFromDate().format("MM/yyyy"));
        revenueDto.put("revenue", os3MonthAgo.getRevenue());
        revenueLastThreeMonth.add(revenueDto);

        dto.setRevenueLastThreeMonth(revenueLastThreeMonth);

        // Last 5 Order
        SimpleDate tomorrow = DateTimeUtils.getTomorrow();
        Period oneMonthAgo = new Period(DateTimeUtils.addMonths(tomorrow, -1), tomorrow);
        List<I_OrderHeader> lastFiveOrders = orderRepository.getOrderHeadersByDistributor(clientId,
                customer.getDistributor().getId(), null, customer.getId(), oneMonthAgo, OrderDateType.CREATED_DATE,
                new PageSizeRequest(0, 5), false);

        if (lastFiveOrders == null || lastFiveOrders.isEmpty()) {
            dto.setLastFiveOrders(Collections.<Map<String, Object>> emptyList());
        } else {
            List<Map<String, Object>> lastFiveOrderDtos = new ArrayList<Map<String, Object>>(5);
            for (I_OrderHeader order : lastFiveOrders) {
                Map<String, Object> orderDto = new HashMap<String, Object>();
                orderDto.put("date", order.getCreatedTime().getIsoTime());
                orderDto.put("skuNumber", order.getNbSKU());
                orderDto.put("total", order.getGrandTotal());
                lastFiveOrderDtos.add(orderDto);
            }

            dto.setLastFiveOrders(lastFiveOrderDtos);
        }

        dto.setCanEditLocation(getConfig(userLogin).isCanEditCustomerLocation());

        return dto;
    }

    @Override
    public void updatePhone(UserLogin userLogin, String id, String phone) {
        BusinessAssert.notNull(phone);

        I_Customer customer = getMandatoryPO(userLogin, id, customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer is not accessible");

        customerRepository.updatePhone(userLogin.getClientId(), customer.getId(), phone);
    }

    @Override
    public void updateMobile(UserLogin userLogin, String id, String mobile) {
        BusinessAssert.notNull(mobile);

        I_Customer customer = getMandatoryPO(userLogin, id, customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer is not accessible");

        customerRepository.updateMobile(userLogin.getClientId(), customer.getId(), mobile);
    }

    @Override
    public void updateLocation(UserLogin userLogin, String id, Location location) {
        if (getConfig(userLogin).isCanEditCustomerLocation()) {
            I_Customer customer = getMandatoryPO(userLogin, id, customerRepository);
            BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer is not accessible");

            BusinessAssert.isTrue(LocationUtils.checkLocationValid(location), "Invalid location param");

            customerRepository.updateLocation(userLogin.getClientId(), customer.getId(), location);
        }
    }

    @Override
    public IdDto startVisit(UserLogin userLogin, String customerId, Location location) {
        I_User salesman = getCurrentUser(userLogin);
        I_Customer customer = getMandatoryPO(userLogin, customerId, customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer is not accessible");

        String distributorId = customer.getDistributor().getId();

        I_VisitHeader visitHeader = visitingRepository.getVisitHeaderByCustomerToday(userLogin.getClientId(),
                distributorId, customer.getId());
        if (visitHeader != null) {
            BusinessAssert.equals(visitHeader.getVisitStatus(), I_VisitHeader.VISIT_STATUS_VISITING, "visit ended");

            return new IdDto(visitHeader.getId());
        }

        Visit visit = createVisit(userLogin, salesman, salesman.getDistributor(), customer, location);

        visit.setStartTime(DateTimeUtils.getCurrentTime());
        visit.setEndTime(null);
        visit.setVisitStatus(Visit.VISIT_STATUS_VISITING);
        visit.setClosed(false);

        return new IdDto(visitingRepository.save(userLogin.getClientId(), visit));
    }

    @Override
    public VisitInfoDto endVisit(UserLogin userLogin, String visitId, VisitEndDto dto) {
        I_Visit _visit = getMandatoryPO(userLogin, visitId, visitingRepository);
        BusinessAssert.equals(_visit.getVisitStatus(), Visit.VISIT_STATUS_VISITING,
                BusinessExceptionCode.VISIT_WAS_ENDED, "visit ended");

        // CHECK CORRECT SALESMAN
        BusinessAssert.equals(userLogin.getUserId(), _visit.getSalesman().getId(), "another salesman");

        List<I_Survey> surveys = null;

        Visit visit = new Visit(_visit);
        visit.setEndTime(DateTimeUtils.getCurrentTime());
        visit.setDuration(SimpleDate.getDuration(visit.getStartTime(), visit.getEndTime()));
        visit.setErrorDuration(visit.getDuration() < getConfig(userLogin).getVisitDurationKPI());
        visit.setVisitStatus(Visit.VISIT_STATUS_VISITED);

        if (dto != null) {
            if (dto.getPhoto() != null) {
                BusinessAssert.isTrue(fileRepository.exists(userLogin.getClientId(), dto.getPhoto()),
                        "Photo not exist");

                visit.setPhoto(dto.getPhoto());
            }

            // FEEDBACK
            List<String> feedbacks = new LinkedList<String>();
            if (dto.getFeedbacks() != null && !dto.getFeedbacks().isEmpty()) {
                for (String feedback : dto.getFeedbacks()) {
                    if (!StringUtils.isNullOrEmpty(feedback)) {
                        feedbacks.add(feedback.trim());
                    }
                }
            }

            visit.setReaded(false);
            visit.setFeedbacks(feedbacks);

            // ORDER
            if (dto.getOrder() != null) {
                BusinessAssert.notEmpty(dto.getOrder().getDetails(), "order detail dto null");

                visit.setCode(codeGeneratorRepository.getOrderCode(userLogin.getClientId().toString(),
                        visit.getCreatedTime()));

                if (dto.getOrder().isVanSales()) {
                    visit.setApproveStatus(I_Order.APPROVE_STATUS_APPROVED);
                    visit.setApprovedTime(DateTimeUtils.getCurrentTime());
                    visit.setApprovedBy(getCurrentUser(userLogin));
                    visit.setVanSales(true);
                } else {
                    visit.setApproveStatus(I_Order.APPROVE_STATUS_PENDING);
                    visit.setVanSales(false);
                }

                visit.setDeliveryType(dto.getOrder().getDeliveryType());
                if (dto.getOrder().getDeliveryTime() != null) {
                    visit.setDeliveryTime(getMandatoryIsoTime(dto.getOrder().getDeliveryTime()));
                }

                orderCalculationService.calculate(userLogin, _visit.getDistributor().getId(), dto.getOrder(), visit,
                        getDistributorPriceList(userLogin));
            }

            // SURVEY ANSWER
            Set<String> surveyIds = new HashSet<String>();
            if (userLogin.hasModule(Module.SURVEY) && dto.getSurveyAnswers() != null
                    && !dto.getSurveyAnswers().isEmpty()) {
                surveys = new ArrayList<>(dto.getSurveyAnswers().size());
                List<I_SurveyAnswerContent> surveyAnswers = new ArrayList<I_SurveyAnswerContent>(
                        dto.getSurveyAnswers().size());
                for (SurveyAnswerDto surveyAnswerDto : dto.getSurveyAnswers()) {
                    BusinessAssert.isTrue(!surveyIds.contains(surveyAnswerDto.getSurveyId()));
                    surveyIds.add(surveyAnswerDto.getSurveyId());

                    I_Survey survey = getMandatoryPO(userLogin, surveyAnswerDto.getSurveyId(), surveyRepository);
                    surveys.add(survey);
                    Set<Integer> optionValids = new HashSet<>();
                    for (I_SurveyQuestion surveyQuestion : survey.getQuestions()) {
                        for (I_SurveyOption surveyOption : surveyQuestion.getOptions()) {
                            optionValids.add(surveyOption.getSeqNo());
                        }
                    }

                    SurveyAnswerContent surveyAnswer = new SurveyAnswerContent();
                    surveyAnswer.setSurvey(survey);

                    Set<Integer> options = new HashSet<>();
                    if (!CollectionUtils.isEmpty(surveyAnswerDto.getOptions())) {
                        for (String option : surveyAnswerDto.getOptions()) {
                            try {
                                int optionSeqNo = Integer.parseInt(option);
                                BusinessAssert.contain(optionValids, optionSeqNo);
                                options.add(optionSeqNo);
                            } catch (NumberFormatException ex) {
                                BusinessAssert.isTrue(false, "option id must be int");
                            }
                        }
                    }
                    surveyAnswer.setOptions(options);

                    surveyAnswers.add(surveyAnswer);
                }
                visit.setSurveyAnswers(surveyAnswers);
            }
        }

        _visit = visitingRepository.save(userLogin.getClientId(), visit);

        // ADD TO CACHE
        cacheVisitOrderService.addNewVisited(_visit);

        if (_visit.isHasOrder()) {
            if (_visit.getApproveStatus() == I_Order.APPROVE_STATUS_PENDING) {
                webNotificationEngine.notifyChangedOrder(userLogin, _visit, WebNotificationService.ACTION_ORDER_ADD);
            } else {
                cacheVisitOrderService.addNewApprovedOrder(_visit);
            }
        }

        if (!CollectionUtils.isEmpty(_visit.getFeedbacks())) {
            webNotificationEngine.notifyChangedFeedback(userLogin, _visit, WebNotificationService.ACTION_FEEDBACK_ADD);
        }

        VisitInfoDto visitInfoDto = new VisitInfoDto(_visit, getProductPhotoFactory(userLogin));

        if (surveys != null) {
            for (I_Survey survey : surveys) {
                visitInfoDto.addSurvey(new SurveyDto(survey));
            }
        }

        return visitInfoDto;
    }

    @Override
    public IdDto markAsClosed(UserLogin userLogin, String customerId, VisitClosingDto dto) {
        I_User salesman = getCurrentUser(userLogin);
        I_Customer customer = getMandatoryPO(userLogin, customerId, customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer not accessible");

        BusinessAssert.notNull(dto, "dto null");
        BusinessAssert.notNull(dto.getClosingPhoto(), "photo null");
        BusinessAssert.isTrue(fileRepository.exists(userLogin.getClientId(), dto.getClosingPhoto()), "Photo not exist");

        Visit visit = createVisit(userLogin, salesman, salesman.getDistributor(), customer, dto.getLocation());

        SimpleDate currentTime = DateTimeUtils.getCurrentTime();
        visit.setStartTime(currentTime);
        visit.setEndTime(currentTime);

        visit.setDuration(0);
        visit.setErrorDuration(false);

        visit.setPhoto(dto.getClosingPhoto());

        visit.setVisitStatus(Visit.VISIT_STATUS_VISITED);
        visit.setClosed(true);

        I_Visit _visit = visitingRepository.save(userLogin.getClientId(), visit);

        // ADD TO CACHE
        cacheVisitOrderService.addNewVisited(_visit);

        return new IdDto(_visit.getId());
    }

    @Override
    public VisitInfoDto getVisitedTodayInfo(UserLogin userLogin, String customerId) {
        I_Customer customer = getMandatoryPO(userLogin, customerId, customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, null, customer), "customer not accessible");

        String distributorId = customer.getDistributor().getId();

        I_Visit visit = visitingRepository.getVisitByCustomerToday(userLogin.getClientId(), distributorId,
                customer.getId());
        BusinessAssert.notNull(visit, "visit null");
        BusinessAssert.equals(visit.getVisitStatus(), Visit.VISIT_STATUS_VISITED, "visit not ended");

        VisitInfoDto visitInfoDto = new VisitInfoDto(visit, getProductPhotoFactory(userLogin));

        if (visitInfoDto.getSurveyAnswers() != null) {
            Set<String> surveyIds = new HashSet<String>();
            for (SurveyAnswerDto surveyAnswerDto : visitInfoDto.getSurveyAnswers()) {
                surveyIds.add(surveyAnswerDto.getSurveyId());
            }

            List<I_Survey> surveys = surveyRepository.getListByIds(userLogin.getClientId(), surveyIds);
            if (surveys != null) {
                for (I_Survey survey : surveys) {
                    visitInfoDto.addSurvey(new SurveyDto(survey));
                }
            }
        }

        return visitInfoDto;
    }

    // PRIVATE
    private Visit createVisit(UserLogin userLogin, I_User salesman, I_CategoryEmbed distributor, I_Customer customer,
            Location location) {
        Visit visit = new Visit();
        initiatePO(userLogin.getClientId(), visit);

        // LOCATION STATUS
        if (!LocationUtils.checkLocationValid(customer.getLocation())) {
            visit.setLocationStatus(Visit.LOCATION_STATUS_CUSTOMER_UNLOCATED);
            visit.setLocation(location);
            visit.setCustomerLocation(null);
        } else {
            if (!LocationUtils.checkLocationValid(location)) {
                visit.setLocationStatus(Visit.LOCATION_STATUS_UNLOCATED);
                visit.setLocation(null);
                visit.setCustomerLocation(customer.getLocation());
            } else {
                double distance = LocationUtils.calculateDistance(customer.getLocation().getLatitude(),
                        customer.getLocation().getLongitude(), location.getLatitude(), location.getLongitude());
                if (distance > getConfig(userLogin).getVisitDistanceKPI()) {
                    visit.setLocationStatus(Visit.LOCATION_STATUS_TOO_FAR);
                } else {
                    visit.setLocationStatus(Visit.LOCATION_STATUS_LOCATED);
                }

                visit.setLocation(location);
                visit.setCustomerLocation(customer.getLocation());
                visit.setDistance(distance);
            }
        }

        visit.setDistributor(distributor);
        visit.setSalesman(salesman);
        visit.setCustomer(customer);

        return visit;
    }

}
