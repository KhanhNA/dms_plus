package com.viettel.backend.service.core.visit.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractExportService;
import com.viettel.backend.service.core.pdto.Visit;
import com.viettel.backend.service.core.visit.VisitExportService;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.FeedbackRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.VisitRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_Feedback;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.data.I_VisitHeader;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class VisitExportServiceImpl extends AbstractExportService implements VisitExportService {

	@Autowired
	private VisitRepository visitRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private FeedbackRepository feedbackRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Override
	public InputStream exportVisit(UserLogin userLogin, String _distributorId, String _salesmanId, String _customerId,
			String _fromDate, String _toDate, String lang) {
		lang = getLang(lang);

		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		Collection<String> distributorIds = null;
		if (_distributorId == null) {
			distributorIds = Collections.singleton(getMandatoryDistributor(userLogin, _distributorId).getId());
		} else if (_distributorId.startsWith("all_")) {
			_distributorId = _distributorId.substring(_distributorId.indexOf("_") + 1);
			I_Region region = regionRepository.getById(userLogin.getClientId(), _distributorId);
			distributorIds = userRepository.getDistributorForRegionRecursively(userLogin.getClientId(), region,
					new ArrayList<>());
		} else if ("all".equalsIgnoreCase(_distributorId)) {
			distributorIds = getIdSet(getAccessibleDistributors(userLogin));
		} else {
			distributorIds = Collections.singleton(getMandatoryDistributor(userLogin, _distributorId).getId());
		}

		// I_Distributor distributor = getMandatoryDistributor(userLogin,
		// _distributorId);
		// String distributorId = distributor.getId();

		String salesmanId = null;
		if (_salesmanId != null) {
			I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);
			BusinessAssert.equals(salesman.getRole(), Role.SALESMAN);
			BusinessAssert.notNull(salesman.getDistributor());
			// BusinessAssert.equals(salesman.getDistributor().getId(),
			// distributorId);
			BusinessAssert.contain(distributorIds, salesman.getDistributor().getId());
			salesmanId = salesman.getId();
		}

		String customerId = null;
		if (_customerId != null) {
			I_Customer customer = getMandatoryPO(userLogin, _customerId, customerRepository);
			BusinessAssert.notNull(customer.getDistributor());
			// BusinessAssert.equals(customer.getDistributor().getId(),
			// distributorId);
			BusinessAssert.contain(distributorIds, customer.getDistributor().getId());
			customerId = customer.getId();
		}

		I_Config config = getConfig(userLogin);

		SimpleDate today = DateTimeUtils.getToday();

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "order.list"));

			Row row;

			String[] headers = new String[] { "distributor.name", "distributor.code", "created.date", "start.time",
					"end.time", "duration", "salesman.fullname", "salesman.username", "customer.name", "customer.code",
					"customer.approved.time", "customer.area", "customer.type", "location.status", "distance", "closed",
					"has.photo", "has.order", "order.code", "order", "order.status", "van.sales", "feedback" };

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, headers.length - 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "visit.list"));

			row = sheet.createRow(3);
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 1));
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "from.date") + ": " + fromDate.format(config.getDateFormat()));
			createCell(row, 2, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "to.date") + ": " + toDate.format(config.getDateFormat()));

			row = sheet.createRow(4);
			sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 3));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "issued.date") + ": " + today.format(config.getDateFormat()));

			// Create table headers
			row = sheet.createRow(7);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			int rownum = 8;
			long count = 0;
			for (String distributorId : distributorIds) {
				I_Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);
				row = sheet.createRow(rownum++);
				createCell(row, 0, textCellStyle, distributor.getName());
				createCell(row, 1, textCellStyle, distributor.getCode());
				createCell(row, 2, textCellStyle, "");
				createCell(row, 3, textCellStyle, "");
				createCell(row, 4, textCellStyle, "");
				createCell(row, 5, textCellStyle, "");
				createCell(row, 6, textCellStyle, "");
				createCell(row, 7, textCellStyle, "");
				createCell(row, 8, textCellStyle, "");
				createCell(row, 9, textCellStyle, "");
				createCell(row, 10, textCellStyle, "");
				createCell(row, 11, textCellStyle, "");
				createCell(row, 12, textCellStyle, "");
				createCell(row, 13, textCellStyle, "");
				createCell(row, 14, textCellStyle, "");
				createCell(row, 15, textCellStyle, "");
				createCell(row, 16, textCellStyle, "");
				createCell(row, 17, textCellStyle, "");
				createCell(row, 18, textCellStyle, "");
				createCell(row, 19, textCellStyle, "");
				createCell(row, 20, textCellStyle, "");
				createCell(row, 21, textCellStyle, "");
				createCell(row, 22, textCellStyle, "");

				long countByDistributor = visitRepository.countVisitedsByDistributor(userLogin.getClientId(),
						distributorId, salesmanId, customerId, period);
				count += countByDistributor;
				int nbRecordByQuery = 2000;
				int nbQuery = ((int) countByDistributor / nbRecordByQuery)
						+ ((countByDistributor % nbRecordByQuery) == 0 ? 0 : 1);
				for (int page = 1; page <= nbQuery; page++) {
					PageSizeRequest pageSizeRequest = new PageSizeRequest(page - 1, nbRecordByQuery);

					List<I_VisitHeader> visits = visitRepository.getVisitByDistributor(userLogin.getClientId(),
							distributorId, salesmanId, customerId, period, pageSizeRequest, true);

					for (int i = 0, length = visits.size(); i < length; i++) {
						I_VisitHeader visit = visits.get(i);

						row = sheet.createRow(rownum++);
						createCell(row, 0, textCellStyle, "");
						createCell(row, 1, textCellStyle, "");

						createCell(row, 2, textCellStyle, visit.getStartTime().format(config.getDateFormat()));
						createCell(row, 3, textCellStyle, visit.getStartTime().format("HH:mm:ss"));
						createCell(row, 4, textCellStyle, visit.getEndTime().format("HH:mm:ss"));
						createCell(row, 5, textCellStyle, getDurationDisplay(visit.getDuration()));

						createCell(row, 6, textCellStyle, visit.getSalesman().getFullname());
						createCell(row, 7, textCellStyle, visit.getSalesman().getUsername());

						createCell(row, 8, textCellStyle, visit.getCustomer().getName());
						createCell(row, 9, textCellStyle, visit.getCustomer().getCode());
						I_Customer customerInfo = customerRepository.getById(userLogin.getClientId(), false, null,
								visit.getCustomer().getId());
						createCell(row, 10, textCellStyle, customerInfo.getApprovedTime() != null
								? customerInfo.getApprovedTime().format(config.getDateFormat()) : "");

						createCell(row, 11, textCellStyle, visit.getCustomer().getArea().getName());
						createCell(row, 12, textCellStyle, visit.getCustomer().getCustomerType().getName());

						String pattern = "###,##0.##";
						DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
						decimalFormatSymbols.setDecimalSeparator(',');
						decimalFormatSymbols.setGroupingSeparator('.');
						DecimalFormat decimalFormat = new DecimalFormat(pattern, decimalFormatSymbols);

						DecimalFormat df = new DecimalFormat();
						df.setMinimumFractionDigits(3);
						df.setMaximumFractionDigits(3);
						df.setMinimumIntegerDigits(1);
						df.setMaximumIntegerDigits(3);
						df.setGroupingSize(20);

						if (visit.getLocationStatus() == Visit.LOCATION_STATUS_LOCATED) {
							createCell(row, 13, textCellStyle, translate(lang, "correct"));
							createCell(row, 14, textCellStyle, decimalFormat.format(visit.getDistance() * 1000) + " m");
						} else if (visit.getLocationStatus() == Visit.LOCATION_STATUS_TOO_FAR) {
							createCell(row, 13, textCellStyle, translate(lang, "too.far"));
							createCell(row, 14, textCellStyle, decimalFormat.format(visit.getDistance() * 1000) + " m");
						} else {
							createCell(row, 13, textCellStyle, translate(lang, "undefined"));
							createCell(row, 14, textCellStyle, "");
						}

						if (visit.isClosed()) {
							createCell(row, 15, textCellStyle, "x");
						} else {
							createCell(row, 15, textCellStyle, "");
						}

						if (visit.getPhoto() != null) {
							createCell(row, 16, textCellStyle, "x");
						} else {
							createCell(row, 16, textCellStyle, "");
						}

						if (visit.isHasOrder()) {
							createCell(row, 17, textCellStyle, "x");
							createCell(row, 18, textCellStyle, visit.getCode());
							createCell(row, 19, numberCellStyle, visit.getGrandTotal().doubleValue());

							if (visit.getApproveStatus() == Visit.APPROVE_STATUS_APPROVED) {
								createCell(row, 20, textCellStyle, translate(lang, "approved"));
							} else if (visit.getApproveStatus() == Visit.APPROVE_STATUS_REJECTED) {
								createCell(row, 20, textCellStyle, translate(lang, "rejected"));
							} else {
								createCell(row, 20, textCellStyle, translate(lang, "pending"));
							}

							if (visit.isVanSales()) {
								createCell(row, 21, textCellStyle, "x");
							} else {
								createCell(row, 21, textCellStyle, "");
							}

						} else {
							createCell(row, 17, textCellStyle, "");
							createCell(row, 18, textCellStyle, "");
							createCell(row, 19, textCellStyle, "");
							createCell(row, 20, textCellStyle, "");
							createCell(row, 21, textCellStyle, "");
						}
						if (visit.getFeedbacks() != null && visit.getFeedbacks().size() > 0) {
							StringBuilder messages = new StringBuilder();
							boolean isFirst = true;
							for (String message : visit.getFeedbacks()) {
								if (!isFirst) {
									messages.append("; ");
								}
								messages.append(message);
								isFirst = false;
							}
							createCell(row, 22, textCellStyle, messages.toString());
						} else {
							createCell(row, 22, textCellStyle, "");
						}

					}
				}
			}
			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
			createCell(row, 0, footerTextCellStyle, count + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			File outTempFile = File
					.createTempFile("VisitList" + userLogin.getClientId() + "_" + System.currentTimeMillis(), "tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new FileInputStream(outTempFile);
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

	@Override
	public InputStream exportFeedback(UserLogin userLogin, String _distributorId, String _fromDate, String _toDate,
			String lang) {
		lang = getLang(lang);

		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		BusinessAssert.isTrue(distributors != null && !distributors.isEmpty(), "you don't have to any distributor");
		Set<String> accessibleDistributorIds = getIdSet(distributors);
		Set<String> distributorIds;
		if (_distributorId == null) {
			distributorIds = accessibleDistributorIds;
		} else if (_distributorId.startsWith("all_")) {
			_distributorId = _distributorId.substring(_distributorId.indexOf("_") + 1);
			I_Region region = regionRepository.getById(userLogin.getClientId(), _distributorId);
			distributorIds = PO.convertIdsToSet(userRepository
					.getDistributorForRegionRecursively(userLogin.getClientId(), region, new ArrayList<>()));
		} else {
			BusinessAssert.contain(accessibleDistributorIds, _distributorId,
					"Invalid Distributor with ID=" + _distributorId);
			distributorIds = Collections.singleton(_distributorId);
		}

		I_Config config = getConfig(userLogin);

		SimpleDate today = DateTimeUtils.getToday();

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "order.list"));

			Row row;

			String[] headers = new String[] { "distributor.code", "distributor.name", "created.date", "time",
					"salesman.fullname", "salesman.username", "customer.name", "customer.code", "customer.area",
					"customer.type", "feedback" };

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, headers.length - 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "feedback.list"));

			row = sheet.createRow(3);
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, 1));
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 2, 3));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "from.date") + ": " + fromDate.format(config.getDateFormat()));
			createCell(row, 2, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "to.date") + ": " + toDate.format(config.getDateFormat()));

			row = sheet.createRow(4);
			sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, 3));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "issued.date") + ": " + today.format(config.getDateFormat()));

			// Create table headers
			row = sheet.createRow(7);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);

			int rownum = 8;

			long count = 0;
			for (String distributorId : distributorIds) {
				long countByDistributor = feedbackRepository.countFeedbackByDistributors(userLogin.getClientId(),
						Collections.singleton(distributorId), period);
				count += countByDistributor;
				int nbRecordByQuery = 2000;
				int nbQuery = ((int) count / nbRecordByQuery) + ((count % nbRecordByQuery) == 0 ? 0 : 1);

				I_Distributor distributor = distributorRepository.getById(userLogin.getClientId(), distributorId);
				row = sheet.createRow(rownum++);
				int col = 0;
				createCell(row, col++, textCellStyle, distributor.getCode());
				createCell(row, col++, textCellStyle, distributor.getName());
				createCell(row, col++, textCellStyle, "");
				createCell(row, col++, textCellStyle, "");
				createCell(row, col++, textCellStyle, "");
				createCell(row, col++, textCellStyle, "");
				createCell(row, col++, textCellStyle, "");
				createCell(row, col++, textCellStyle, "");
				createCell(row, col++, textCellStyle, "");
				createCell(row, col++, textCellStyle, "");
				createCell(row, col++, textCellStyle, "");

				for (int page = 1; page <= nbQuery; page++) {
					PageSizeRequest pageSizeRequest = new PageSizeRequest(page - 1, nbRecordByQuery);

					List<I_Feedback> feedbacks = feedbackRepository.getFeedbackByDistributors(userLogin.getClientId(),
							Collections.singleton(distributorId), period, pageSizeRequest);

					for (int i = 0, length = feedbacks.size(); i < length; i++) {
						I_Feedback feedback = feedbacks.get(i);

						row = sheet.createRow(rownum++);
						col = 0;
						createCell(row, col++, textCellStyle, "");
						createCell(row, col++, textCellStyle, "");

						createCell(row, col++, textCellStyle, feedback.getCreatedTime().format(config.getDateFormat()));
						createCell(row, col++, textCellStyle, feedback.getCreatedTime().format("HH:mm:ss"));

						createCell(row, col++, textCellStyle, feedback.getCreatedBy().getFullname());
						createCell(row, col++, textCellStyle, feedback.getCreatedBy().getUsername());

						createCell(row, col++, textCellStyle, feedback.getCustomer().getName());
						createCell(row, col++, textCellStyle, feedback.getCustomer().getCode());
						createCell(row, col++, textCellStyle, feedback.getCustomer().getArea().getName());
						createCell(row, col++, textCellStyle, feedback.getCustomer().getCustomerType().getName());

						StringBuilder messages = new StringBuilder();
						boolean isFirst = true;
						for (String message : feedback.getFeedbacks()) {
							if (!isFirst) {
								messages.append("; ");
							}
							messages.append(message);
							isFirst = false;
						}

						createCell(row, col++, textCellStyle, messages.toString());
					}
				}
			}

			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
			createCell(row, 0, footerTextCellStyle, count + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			File outTempFile = File.createTempFile("FeedbackList" + System.currentTimeMillis(), "tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new FileInputStream(outTempFile);
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

	private String getDurationDisplay(long duration) {
		if (duration > 0) {
			long minute = duration / 60000;
			long second = (duration / 1000) % 60;
			if (minute > 0) {
				return minute + "m " + second + "s";
			} else {
				return second + "s";
			}

		} else {
			return "0s";
		}
	}

	@Override
	public ExportDto exportVisit(UserLogin userLogin, String lang, int error) {
		BusinessAssert.isTrue(error == com.viettel.persistence.mongo.domain.Visit.ERROR_VISIT_POSITION
				|| error == com.viettel.persistence.mongo.domain.Visit.ERROR_VISIT_DURATION);
		lang = getLang(lang);
		Period period = DateTimeUtils.getPeriodToday();
		SimpleDate today = DateTimeUtils.getCurrentTime();
		List<I_Distributor> accessibleDistributors = getAccessibleDistributors(userLogin);
		I_Config config = getConfig(userLogin);

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "visit.list"));

			Row row;

			String[] headers = new String[] { "distributor.name", "distributor.code", "created.date", "start.time",
					"end.time", "duration", "salesman.fullname", "salesman.username", "customer.name", "customer.code",
					"customer.area", "customer.type", "location.status", "distance", "closed",
					"has.photo", "has.order", "order.code", "order", "order.status", "van.sales", "feedback" };

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, headers.length - 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, error == com.viettel.persistence.mongo.domain.Visit.ERROR_VISIT_DURATION
							? "export.visit.lack.of.time" : "export.visit.wrong.location"));
			row = sheet.createRow(2);
			sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, headers.length - 1));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					today.format("dd/MM/yyyy HH:mm"));

			// Create table headers
			row = sheet.createRow(4);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			int rownum = 5;
			long count = 0;
			for (I_Distributor distributor : accessibleDistributors) {

				row = sheet.createRow(rownum++);
				createCell(row, 0, textCellStyle, distributor.getName());
				createCell(row, 1, textCellStyle, distributor.getCode());
				createCell(row, 2, textCellStyle, "");
				createCell(row, 3, textCellStyle, "");
				createCell(row, 4, textCellStyle, "");
				createCell(row, 5, textCellStyle, "");
				createCell(row, 6, textCellStyle, "");
				createCell(row, 7, textCellStyle, "");
				createCell(row, 8, textCellStyle, "");
				createCell(row, 9, textCellStyle, "");
				createCell(row, 10, textCellStyle, "");
				createCell(row, 11, textCellStyle, "");
				createCell(row, 12, textCellStyle, "");
				createCell(row, 13, textCellStyle, "");
				createCell(row, 14, textCellStyle, "");
				createCell(row, 15, textCellStyle, "");
				createCell(row, 16, textCellStyle, "");
				createCell(row, 17, textCellStyle, "");
				createCell(row, 18, textCellStyle, "");
				createCell(row, 19, textCellStyle, "");
				createCell(row, 20, textCellStyle, "");
				createCell(row, 21, textCellStyle, "");

				long countByDistributor = visitRepository.countVisitedsByDistributor(userLogin.getClientId(),
						distributor.getId(), error, period);
				count += countByDistributor;
				int nbRecordByQuery = 2000;
				int nbQuery = ((int) countByDistributor / nbRecordByQuery)
						+ ((countByDistributor % nbRecordByQuery) == 0 ? 0 : 1);
				for (int page = 1; page <= nbQuery; page++) {
					PageSizeRequest pageSizeRequest = new PageSizeRequest(page - 1, nbRecordByQuery);

					List<I_VisitHeader> visits = visitRepository.getVisitByDistributor(userLogin.getClientId(),
							distributor.getId(), error, period, pageSizeRequest, true);

					for (int i = 0, length = visits.size(); i < length; i++) {
						I_VisitHeader visit = visits.get(i);

						row = sheet.createRow(rownum++);
						createCell(row, 0, textCellStyle, "");
						createCell(row, 1, textCellStyle, "");

						createCell(row, 2, textCellStyle, visit.getStartTime().format(config.getDateFormat()));
						createCell(row, 3, textCellStyle, visit.getStartTime().format("HH:mm:ss"));
						createCell(row, 4, textCellStyle, visit.getEndTime().format("HH:mm:ss"));
						createCell(row, 5, textCellStyle, getDurationDisplay(visit.getDuration()));

						createCell(row, 6, textCellStyle, visit.getSalesman().getFullname());
						createCell(row, 7, textCellStyle, visit.getSalesman().getUsername());

						createCell(row, 8, textCellStyle, visit.getCustomer().getName());
						createCell(row, 9, textCellStyle, visit.getCustomer().getCode());

						createCell(row, 10, textCellStyle, visit.getCustomer().getArea().getName());
						createCell(row, 11, textCellStyle, visit.getCustomer().getCustomerType().getName());

						String pattern = "###,##0.##";
						DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
						decimalFormatSymbols.setDecimalSeparator(',');
						decimalFormatSymbols.setGroupingSeparator('.');
						DecimalFormat decimalFormat = new DecimalFormat(pattern, decimalFormatSymbols);

						DecimalFormat df = new DecimalFormat();
						df.setMinimumFractionDigits(3);
						df.setMaximumFractionDigits(3);
						df.setMinimumIntegerDigits(1);
						df.setMaximumIntegerDigits(3);
						df.setGroupingSize(20);

						if (visit.getLocationStatus() == Visit.LOCATION_STATUS_LOCATED) {
							createCell(row, 12, textCellStyle, translate(lang, "correct"));
							createCell(row, 13, textCellStyle, decimalFormat.format(visit.getDistance() * 1000) + " m");
						} else if (visit.getLocationStatus() == Visit.LOCATION_STATUS_TOO_FAR) {
							createCell(row, 12, textCellStyle, translate(lang, "too.far"));
							createCell(row, 13, textCellStyle, decimalFormat.format(visit.getDistance() * 1000) + " m");
						} else {
							createCell(row, 12, textCellStyle, translate(lang, "undefined"));
							createCell(row, 13, textCellStyle, "");
						}

						if (visit.isClosed()) {
							createCell(row, 14, textCellStyle, "x");
						} else {
							createCell(row, 14, textCellStyle, "");
						}

						if (visit.getPhoto() != null) {
							createCell(row, 15, textCellStyle, "x");
						} else {
							createCell(row, 15, textCellStyle, "");
						}

						if (visit.isHasOrder()) {
							createCell(row, 16, textCellStyle, "x");
							createCell(row, 17, textCellStyle, visit.getCode());
							createCell(row, 18, numberCellStyle, visit.getGrandTotal().doubleValue());

							if (visit.getApproveStatus() == Visit.APPROVE_STATUS_APPROVED) {
								createCell(row, 19, textCellStyle, translate(lang, "approved"));
							} else if (visit.getApproveStatus() == Visit.APPROVE_STATUS_REJECTED) {
								createCell(row, 19, textCellStyle, translate(lang, "rejected"));
							} else {
								createCell(row, 19, textCellStyle, translate(lang, "pending"));
							}

							if (visit.isVanSales()) {
								createCell(row, 20, textCellStyle, "x");
							} else {
								createCell(row, 20, textCellStyle, "");
							}

						} else {
							createCell(row, 16, textCellStyle, "");
							createCell(row, 17, textCellStyle, "");
							createCell(row, 18, textCellStyle, "");
							createCell(row, 19, textCellStyle, "");
							createCell(row, 20, textCellStyle, "");
						}
						if (visit.getFeedbacks() != null && visit.getFeedbacks().size() > 0) {
							StringBuilder messages = new StringBuilder();
							boolean isFirst = true;
							for (String message : visit.getFeedbacks()) {
								if (!isFirst) {
									messages.append("; ");
								}
								messages.append(message);
								isFirst = false;
							}
							createCell(row, 21, textCellStyle, messages.toString());
						} else {
							createCell(row, 21, textCellStyle, "");
						}

					}
				}
			}
			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
			createCell(row, 0, footerTextCellStyle, count + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			StringBuilder fileName = new StringBuilder();
			fileName.append(error == com.viettel.persistence.mongo.domain.Visit.ERROR_VISIT_DURATION ? "ErrorDuration_"
					: "ErrorPosition_");
			fileName.append(DateTimeUtils.getCurrentTime().format("yyyyMMddHHmm")).append(".xlsx");

			File outTempFile = File.createTempFile(fileName + "_" + System.currentTimeMillis(), ".tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new ExportDto(fileName.toString(), new FileInputStream(outTempFile));
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

}
