package com.viettel.backend.service.core.visit.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.common.ProgressDto;
import com.viettel.backend.dto.visit.CustomerForVisitDto;
import com.viettel.backend.dto.visit.VisitInfoDto;
import com.viettel.backend.dto.visit.VisitInfoListDto;
import com.viettel.backend.dto.visit.VisitTodaySummary;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheScheduleService;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.cache.CacheVisitOrderService.DateType;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.visit.VisitMonitoringService;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.VisitRepository;
import com.viettel.repository.common.VisitingRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.domain.data.I_VisitHeader;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN, Role.ADVANCE_SUPERVISOR })
@Service
public class VisitMonitoringServiceImpl extends AbstractService implements VisitMonitoringService {

	@Autowired
	private VisitRepository visitRepository;

	@Autowired
	private VisitingRepository visitingRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RouteRepository routeRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CacheScheduleService cacheScheduleService;

	@Autowired
	private CacheVisitOrderService cacheVisitOrderService;

	@Override
	public ListDto<VisitInfoListDto> getVisitsToday(UserLogin userLogin, String _distributorId, String _salesmanId,
			PageSizeRequest pageSizeRequest) {
		Period todayPeriod = DateTimeUtils.getPeriodToday();

		I_Distributor distributor = getMandatoryDistributor(userLogin, _distributorId);
		String distributorId = distributor.getId();

		String salesmanId = null;
		if (_salesmanId != null) {
			I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);
			BusinessAssert.equals(salesman.getRole(), Role.SALESMAN);
			BusinessAssert.notNull(salesman.getDistributor());
			BusinessAssert.equals(salesman.getDistributor().getId(), distributorId);
			salesmanId = salesman.getId();
		}

		List<I_VisitHeader> visits = visitRepository.getVisitByDistributor(userLogin.getClientId(), distributorId,
				salesmanId, null, todayPeriod, pageSizeRequest, false);

		if (CollectionUtils.isEmpty(visits) && pageSizeRequest.getPageNumber() == 0) {
			ListDto.emptyList();
		}

		List<VisitInfoListDto> dtos = new ArrayList<VisitInfoListDto>(visits.size());
		for (I_VisitHeader visit : visits) {
			dtos.add(new VisitInfoListDto(visit));
		}

		long size = Long.valueOf(dtos.size());
		if (pageSizeRequest != null) {
			if (pageSizeRequest.getPageNumber() > 0 || pageSizeRequest.getPageSize() == size) {
				size = visitRepository.countVisitedsByDistributor(userLogin.getClientId(), distributorId, salesmanId,
						null, todayPeriod);
			}
		}

		return new ListDto<VisitInfoListDto>(dtos, size);
	}

	@Override
	public ListDto<CustomerForVisitDto> getCustomersTodayBySalesman(UserLogin userLogin, String _salesmanId) {
		I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);
		BusinessAssert.isTrue(checkAccessible(userLogin, salesman.getDistributor().getId()), "salesman not accessible");

		SimpleDate today = DateTimeUtils.getToday();

		String distributorId = salesman.getDistributor().getId();

		Set<String> routeIds = getIdSet(routeRepository.getRoutesBySalesmen(userLogin.getClientId(), distributorId,
				Collections.singleton(salesman.getId())));

		List<I_Customer> customers = customerRepository.getCustomersByRoutes(userLogin.getClientId(), distributorId,
				routeIds, today);

		if (customers == null || customers.isEmpty()) {
			return ListDto.emptyList();
		}

		Set<String> customerIds = getIdSet(customers);

		// GET VISITED TODAY
		Map<String, I_VisitHeader> visitByCustomer = visitingRepository
				.getVisitHeaderByCustomerTodayMap(userLogin.getClientId(), distributorId, customerIds);

		List<CustomerForVisitDto> dtos = new LinkedList<CustomerForVisitDto>();
		for (I_Customer customer : customers) {
			I_VisitHeader visit = visitByCustomer != null ? visitByCustomer.get(customer.getId()) : null;
			dtos.add(new CustomerForVisitDto(customer, true, 0, visit));
		}

		return new ListDto<CustomerForVisitDto>(dtos);
	}

	@Override
	public ListDto<VisitInfoListDto> getVisits(UserLogin userLogin, String _distributorId, String _salesmanId,
			String _customerId, String _fromDate, String _toDate, PageSizeRequest pageSizeRequest) {
		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		I_Distributor distributor = getMandatoryDistributor(userLogin, _distributorId);
		String distributorId = distributor.getId();

		String salesmanId = null;
		if (_salesmanId != null) {
			I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);
			BusinessAssert.equals(salesman.getRole(), Role.SALESMAN);
			BusinessAssert.notNull(salesman.getDistributor());
			BusinessAssert.equals(salesman.getDistributor().getId(), distributorId);
			salesmanId = salesman.getId();
		}

		String customerId = null;
		if (_customerId != null) {
			I_Customer customer = getMandatoryPO(userLogin, _customerId, customerRepository);
			BusinessAssert.notNull(customer.getDistributor());
			BusinessAssert.equals(customer.getDistributor().getId(), distributorId);
			customerId = customer.getId();
		}

		List<I_VisitHeader> visits = visitRepository.getVisitByDistributor(userLogin.getClientId(), distributorId,
				salesmanId, customerId, period, pageSizeRequest, false);

		if (CollectionUtils.isEmpty(visits) && pageSizeRequest.getPageNumber() == 0) {
			ListDto.emptyList();
		}

		List<VisitInfoListDto> dtos = new ArrayList<VisitInfoListDto>(visits.size());
		for (I_VisitHeader visit : visits) {
			dtos.add(new VisitInfoListDto(visit));
		}

		long size = Long.valueOf(dtos.size());
		if (pageSizeRequest != null) {
			if (pageSizeRequest.getPageNumber() > 0 || pageSizeRequest.getPageSize() == size) {
				size = visitRepository.countVisitedsByDistributor(userLogin.getClientId(), distributorId, salesmanId,
						customerId, period);
			}
		}

		return new ListDto<VisitInfoListDto>(dtos, size);
	}

	@Override
	public VisitInfoDto getVisitInfoById(UserLogin userLogin, String id) {
		I_Visit visit = getMandatoryPO(userLogin, id, visitingRepository);
		BusinessAssert.isTrue(visit.getVisitStatus() == I_Visit.VISIT_STATUS_VISITED);
		checkAccessible(userLogin, visit.getDistributor().getId());
		return new VisitInfoDto(visit, getProductPhotoFactory(userLogin));
	}

	@Override
	public VisitTodaySummary getVisitTodaySummary(UserLogin userLogin, String _distributorId, String _salesmanId) {
		I_Distributor distributor = getMandatoryDistributor(userLogin, _distributorId);
		String distributorId = distributor.getId();

		SimpleDate today = DateTimeUtils.getToday();
		Collection<String> distributorIds = Collections.singleton(distributor.getId());
		Map<String, Integer> nbVisitPlannedByRouteToday = cacheScheduleService
				.getNbVisitPlannedByRoute(userLogin.getClientId(), distributorIds, DateTimeUtils.getPeriodToday());

		if (_salesmanId != null) {
			I_User salesman = getMandatoryPO(userLogin, _salesmanId, userRepository);
			BusinessAssert.equals(salesman.getRole(), Role.SALESMAN, "not salesman");
			BusinessAssert.notNull(salesman.getDistributor());
			BusinessAssert.equals(salesman.getDistributor().getId(), distributor.getId(),
					"salesman not of distributor");

			Collection<String> salesmanIds = Collections.singleton(salesman.getId());

			int nbVisitPlannedToday = 0;
			List<I_Route> routes = routeRepository.getRoutesBySalesmen(userLogin.getClientId(), distributor.getId(),
					salesmanIds);
			for (I_Route route : routes) {
				Integer tmpToday = nbVisitPlannedByRouteToday.get(route.getId());
				tmpToday = tmpToday == null ? 0 : tmpToday;
				nbVisitPlannedToday += tmpToday;
			}

			VisitTodaySummary visitTodaySummary = new VisitTodaySummary();
			visitTodaySummary
					.setVisit(new ProgressDto((double) nbVisitPlannedToday, cacheVisitOrderService.getNbVisitBySalesman(
							userLogin.getClientId(), DateType.DAILY, today, distributor.getId(), salesmanIds)));
			visitTodaySummary.setVisitErrorDuration(
					new ProgressDto(cacheVisitOrderService.getNbVisitErrorDurationBySalesman(userLogin.getClientId(),
							DateType.DAILY, today, distributor.getId(), salesmanIds)));
			visitTodaySummary.setVisitErrorPosition(
					new ProgressDto(cacheVisitOrderService.getNbVisitErrorPositionBySalesman(userLogin.getClientId(),
							DateType.DAILY, today, distributor.getId(), salesmanIds)));

			return visitTodaySummary;
		} else {
			int nbVisitPlannedToday = 0;
			List<I_Route> routes = routeRepository.getAll(userLogin.getClientId(), distributorId);
			for (I_Route route : routes) {
				Integer tmpToday = nbVisitPlannedByRouteToday.get(route.getId());
				tmpToday = tmpToday == null ? 0 : tmpToday;
				nbVisitPlannedToday += tmpToday;
			}

			VisitTodaySummary visitTodaySummary = new VisitTodaySummary();
			visitTodaySummary.setVisit(new ProgressDto((double) nbVisitPlannedToday, cacheVisitOrderService
					.getNbVisitByDistributor(userLogin.getClientId(), DateType.DAILY, today, distributorIds)));
			visitTodaySummary.setVisitErrorDuration(
					new ProgressDto(cacheVisitOrderService.getNbVisitErrorDurationByDistributor(userLogin.getClientId(),
							DateType.DAILY, today, distributorIds)));
			visitTodaySummary.setVisitErrorPosition(
					new ProgressDto(cacheVisitOrderService.getNbVisitErrorPositionByDistributor(userLogin.getClientId(),
							DateType.DAILY, today, distributorIds)));

			return visitTodaySummary;
		}
	}

}
