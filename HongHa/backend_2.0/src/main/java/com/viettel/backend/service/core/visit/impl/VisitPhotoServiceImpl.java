package com.viettel.backend.service.core.visit.impl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.visit.VisitPhotoDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.visit.VisitPhotoService;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.VisitRepository;
import com.viettel.repository.common.domain.data.I_VisitPhoto;
import com.viettel.repository.common.domain.file.I_File;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@Service
public class VisitPhotoServiceImpl extends AbstractService implements VisitPhotoService {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VisitRepository visitRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileRepository fileRepository;
    
    @Override
    public ListDto<VisitPhotoDto> getVisitPhotos(UserLogin userLogin, String salesmanId, String _fromDate,
            String _toDate) {
        I_User salesman = getMandatoryPO(userLogin, salesmanId, userRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, salesman.getDistributor().getId()), "salesman not accessible");

        SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
        SimpleDate toDate = getMandatoryIsoDate(_toDate);
        BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
        BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

        Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

        String distributorId = salesman.getDistributor().getId();
        
        List<I_VisitPhoto> photos = visitRepository.getVisitPhotoBySalesman(userLogin.getClientId(), distributorId,
                salesman.getId(), period);

        if (photos == null || photos.isEmpty()) {
            return ListDto.emptyList();
        }

        List<VisitPhotoDto> dtos = new ArrayList<>(photos.size());
        for (I_VisitPhoto photo : photos) {
            dtos.add(new VisitPhotoDto(photo));
        }

        return new ListDto<VisitPhotoDto>(dtos);
    }
    
    @Override
    public InputStream getVisitPhotosZip(UserLogin userLogin, String salesmanId, String _fromDate, String _toDate) {
        I_User salesman = getMandatoryPO(userLogin, salesmanId, userRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, salesman.getDistributor().getId()), "salesman not accessible");

        SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
        SimpleDate toDate = getMandatoryIsoDate(_toDate);
        BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
        BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

        Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

        String distributorId = salesman.getDistributor().getId();
        
        List<I_VisitPhoto> visitPhotos = visitRepository.getVisitPhotoBySalesman(userLogin.getClientId(), distributorId,
                salesman.getId(), period);

        java.io.File zipFile;
        try {
            zipFile = java.io.File.createTempFile("photos_" + System.currentTimeMillis(), ".zip");
            FileOutputStream zipFOS = new FileOutputStream(zipFile);
            ZipOutputStream zipZOS = new ZipOutputStream(zipFOS);
            
            if (visitPhotos != null && !visitPhotos.isEmpty()) {
                HashSet<String> fileNames = new HashSet<>();
                
                for (I_VisitPhoto visitPhoto : visitPhotos) {
                    I_File photoFile = fileRepository.getFileById(userLogin.getClientId(),
                            visitPhoto.getPhoto());
                    
                    String[] temp = photoFile.getFileName().split("\\.");

                    String customerName = visitPhoto.getCustomer().getName().replace(", ", "_").replace(',', '_').replace(' ', '_');
                    
                    String fileNameBase = visitPhoto.getCreatedTime().getIsoDate() + "_" + customerName;
                    String fileName = fileNameBase;
                    int i = 1;
                    while (fileNames.contains(fileName)) {
                        fileName = fileNameBase + "_" + (++i);
                    }
                    
                    ZipEntry zipEntry;
                    if (temp.length > 1) {
                        String extension = temp[temp.length - 1];
                        zipEntry = new ZipEntry(fileName +  "." + extension);
                    } else {
                        zipEntry = new ZipEntry(fileName);
                    }
                    
                    zipZOS.putNextEntry(zipEntry);
                    
                    IOUtils.copy(photoFile.getInputStream(), zipZOS);
                    
                    photoFile.getInputStream().close();
                }
            }
            
            zipZOS.closeEntry();
            zipZOS.close();
            
            return new FileInputStream(zipFile);
        } catch (IOException e) {
            logger.error("Error when create zip photos", e);
            throw new UnsupportedOperationException("Error when create zip photos", e);
        }
    }

}
