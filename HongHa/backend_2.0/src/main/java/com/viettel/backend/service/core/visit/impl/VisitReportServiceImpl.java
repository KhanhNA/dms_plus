package com.viettel.backend.service.core.visit.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.visit.DistributorVisitResultDto;
import com.viettel.backend.dto.visit.SalesmanVisitResultDto;
import com.viettel.backend.dto.visit.VisitResultDailyDto;
import com.viettel.backend.dto.visit.VisitResultDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.visit.VisitReportService;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.VisitSummaryRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.data.I_VisitSummary;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN, Role.ADVANCE_SUPERVISOR })
@Service
public class VisitReportServiceImpl extends AbstractService implements VisitReportService {

	@Autowired
	private VisitSummaryRepository visitSummaryRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Override
	public ListDto<VisitResultDailyDto> getVisitResultDaily(UserLogin userLogin, String regionId, String distributorId,
			String _fromDate, String _toDate) {
		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		Set<String> accessibleDistributorIds = new HashSet<>();
		if (Role.ADVANCE_SUPERVISOR.equalsIgnoreCase(userLogin.getRole())) {
			accessibleDistributorIds = getIdSet(getDistributorsByRegion(userLogin, regionId));
		} else {
			accessibleDistributorIds = getIdSet(getAccessibleDistributors(userLogin));
		}

		if (CollectionUtils.isEmpty(accessibleDistributorIds)) {
			return ListDto.emptyList();
		}

		Set<String> distributorIds;
		if (!"all".equalsIgnoreCase(distributorId)) {
			I_Distributor distributor = getMandatoryPO(userLogin, distributorId, distributorRepository);
			BusinessAssert.contain(accessibleDistributorIds, distributor.getId());
			distributorIds = Collections.singleton(distributor.getId());
		} else {
			distributorIds = accessibleDistributorIds;
		}

		Map<String, I_VisitSummary> visitSummaryByDay = visitSummaryRepository
				.getVisitSummaryDaily(userLogin.getClientId(), distributorIds, period);

		List<VisitResultDailyDto> dtos = new LinkedList<VisitResultDailyDto>();
		SimpleDate tempDate = period.getFromDate();
		while (tempDate.compareTo(period.getToDate()) < 0) {
			String isoDate = tempDate.getIsoDate();
			I_VisitSummary visitSummary = visitSummaryByDay.get(isoDate);

			VisitResultDto visitResultDto;
			if (visitSummary == null) {
				visitResultDto = new VisitResultDto();
			} else {
				visitResultDto = new VisitResultDto(visitSummary);
			}
			dtos.add(new VisitResultDailyDto(isoDate, visitResultDto));

			tempDate = DateTimeUtils.addDays(tempDate, 1);
		}

		return new ListDto<VisitResultDailyDto>(dtos);
	}

	@Override
	public ListDto<DistributorVisitResultDto> getDistributorVisitResult(UserLogin userLogin, String _fromDate,
			String _toDate) {
		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		Set<String> distributorIds = getIdSet(distributors);

		Map<String, I_VisitSummary> visitSummaryByDistributor = visitSummaryRepository
				.getVisitSummaryByDistributor(userLogin.getClientId(), distributorIds, period);

		List<DistributorVisitResultDto> dtos = new ArrayList<DistributorVisitResultDto>(distributors.size());
		for (I_Distributor distributor : distributors) {
			I_VisitSummary visitSummary = visitSummaryByDistributor.get(distributor.getId());

			VisitResultDto visitResultDto;
			if (visitSummary == null) {
				visitResultDto = new VisitResultDto();
			} else {
				visitResultDto = new VisitResultDto(visitSummary);
			}
			dtos.add(new DistributorVisitResultDto(distributor, visitResultDto));
		}

		return new ListDto<DistributorVisitResultDto>(dtos);
	}

	@Override
	public ListDto<SalesmanVisitResultDto> getSalesmanVisitResult(UserLogin userLogin, String _distributorId,
			String _fromDate, String _toDate) {
		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");

		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		Set<String> distributorIds = Collections.emptySet();
		if ("all".equalsIgnoreCase(_distributorId)) {
			distributorIds = getIdSet(getAccessibleDistributors(userLogin));
		} else {
			I_Distributor distributor = getMandatoryDistributor(userLogin, _distributorId);
			distributorIds = Collections.singleton(distributor.getId());
		}

		Map<String, I_VisitSummary> visitSummaryBySalesman = visitSummaryRepository
				.getVisitSummaryBySalesman(userLogin.getClientId(), distributorIds, period);

		List<I_User> salesmen = userRepository.getSalesmen(userLogin.getClientId(), distributorIds);

		List<SalesmanVisitResultDto> dtos = new ArrayList<SalesmanVisitResultDto>(salesmen.size());
		for (I_User salesman : salesmen) {
			I_VisitSummary visitSummary = visitSummaryBySalesman.get(salesman.getId());

			VisitResultDto visitResultDto;
			if (visitSummary == null) {
				visitResultDto = new VisitResultDto();
			} else {
				visitResultDto = new VisitResultDto(visitSummary);
			}
			dtos.add(new SalesmanVisitResultDto(salesman, visitResultDto));
		}

		return new ListDto<SalesmanVisitResultDto>(dtos);
	}

}
