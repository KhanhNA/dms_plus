package com.viettel.backend.service.exchangereturn.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.exchangereturn.ExchangeReturnCreateDto;
import com.viettel.backend.dto.exchangereturn.ExchangeReturnCreateDto.ExchangeReturnDetailCreateDto;
import com.viettel.backend.dto.exchangereturn.ExchangeReturnDto;
import com.viettel.backend.dto.exchangereturn.ExchangeReturnSimpleDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.CustomerEmbed;
import com.viettel.backend.service.core.pdto.ProductQuantity;
import com.viettel.backend.service.core.pdto.UserEmbed;
import com.viettel.backend.service.exchangereturn.ExchangeReturnCreatingService;
import com.viettel.backend.service.exchangereturn.pdto.ExchangeReturn;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.ExchangeReturnRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.data.I_ExchangeReturn;
import com.viettel.repository.common.domain.data.I_ExchangeReturnHeader;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.SALESMAN })
@RequireModule(Module.EXCHANGE_RETURN)
@Service
public class ExchangeReturnCreatingServiceImpl extends AbstractService implements ExchangeReturnCreatingService {

    @Autowired
    private ExchangeReturnRepository exchangeReturnRepository;

    @Autowired
    private DistributorRepository distributorRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public ListDto<ExchangeReturnSimpleDto> getExchangeProductToday(UserLogin userLogin) {
        return getExchangeReturnToday(userLogin, true);
    }

    @Override
    public ExchangeReturnDto getExchangeProduct(UserLogin userLogin, String id) {
        return getDetail(userLogin, true, id);
    }
    
    @Override
    public IdDto createExchangeProduct(UserLogin userLogin, ExchangeReturnCreateDto createDto) {
        return createExchangeReturn(userLogin, true, createDto);
    }

    @Override
    public ListDto<ExchangeReturnSimpleDto> getReturnProductToday(UserLogin userLogin) {
        return getExchangeReturnToday(userLogin, false);
    }

    @Override
    public ExchangeReturnDto getReturnProduct(UserLogin userLogin, String id) {
        return getDetail(userLogin, false, id);
    }
    
    @Override
    public IdDto createReturnProduct(UserLogin userLogin, ExchangeReturnCreateDto createDto) {
        return createExchangeReturn(userLogin, false, createDto);
    }

    // PRIVATE
    private ListDto<ExchangeReturnSimpleDto> getExchangeReturnToday(UserLogin userLogin, boolean exchange) {
        I_User salesman = getCurrentUser(userLogin);
        String distributorId = salesman.getDistributor().getId();
        Collection<String> salesmanIds = Collections.singleton(salesman.getId());
        
        Period todayPeriod = DateTimeUtils.getPeriodToday();

        List<I_ExchangeReturnHeader> exchangeReturns = exchangeReturnRepository.getListByCreatedBys(userLogin.getClientId(),
                exchange, distributorId, salesmanIds, todayPeriod, null);

        if (exchangeReturns == null || exchangeReturns.isEmpty()) {
            return ListDto.emptyList();
        }
        
        List<ExchangeReturnSimpleDto> dtos = new ArrayList<>(exchangeReturns.size());
        for (I_ExchangeReturnHeader exchangeReturn : exchangeReturns) {
            dtos.add(new ExchangeReturnSimpleDto(exchangeReturn));
        }

        return new ListDto<>(dtos);
    }
    
    private ExchangeReturnDto getDetail(UserLogin userLogin, boolean exchange, String id) {
        I_ExchangeReturn exchangeReturn = getMandatoryPO(userLogin, id, exchangeReturnRepository);
        
        BusinessAssert.equals(exchangeReturn.isExchange(), exchange);
        BusinessAssert.equals(exchangeReturn.getCreatedBy().getId(), userLogin.getUserId());
        
        I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);
        
        return new ExchangeReturnDto(exchangeReturn, productPhotoFactory);
    }

    private IdDto createExchangeReturn(UserLogin userLogin, boolean exchange, ExchangeReturnCreateDto createDto) {
        BusinessAssert.notNull(createDto);
        
        I_Distributor distributor;
        if (createDto.getDistributorId() != null) {
            distributor = getMandatoryPO(userLogin, createDto.getDistributorId(), distributorRepository);
        } else {
            distributor = getDefaultDistributor(userLogin);
            BusinessAssert.notNull(distributor, "Distributor is required");
        }

        I_Customer customer = getMandatoryPO(userLogin, createDto.getCustomerId(), customerRepository);
        BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId(), customer),
                "distributor or customer is not accessible");

        I_User currentUser = getCurrentUser(userLogin);

        Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), null);
        List<I_ProductQuantity> productQuantities = new ArrayList<>(createDto.getDetails().size());
        BigDecimal quantity = BigDecimal.ZERO;
        for (ExchangeReturnDetailCreateDto detail : createDto.getDetails()) {
            String productId = detail.getProductId();
            I_Product product = productMap.get(productId);
            BusinessAssert.notNull(product);

            ProductQuantity productQuantity = new ProductQuantity(product);
            productQuantity.setQuantity(detail.getQuantity());

            productQuantities.add(productQuantity);
            
            quantity = quantity.add(detail.getQuantity());
        }

        ExchangeReturn exchangeReturn = new ExchangeReturn();
        initiatePO(userLogin.getClientId(), exchangeReturn);
        exchangeReturn.setExchange(exchange);
        exchangeReturn.setCreatedTime(DateTimeUtils.getCurrentTime());
        exchangeReturn.setDistributor(new CategoryEmbed(distributor));
        exchangeReturn.setCustomer(new CustomerEmbed(customer));
        exchangeReturn.setCreatedBy(new UserEmbed(currentUser));
        exchangeReturn.setQuantity(quantity);
        exchangeReturn.setDetails(productQuantities);

        return new IdDto(exchangeReturnRepository.save(userLogin.getClientId(), exchangeReturn));
    }

}
