package com.viettel.backend.service.exchangereturn.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractExportService;
import com.viettel.backend.service.exchangereturn.ExchangeReturnExportService;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.ExchangeReturnRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductExchangeReturnQuantity;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN, Role.ADVANCE_SUPERVISOR })
@RequireModule(Module.EXCHANGE_RETURN)
@Service
public class ExchangeReturnExportServiceImpl extends AbstractExportService implements ExchangeReturnExportService {

	@Autowired
	private ExchangeReturnRepository exchangeReturnRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Override
	public InputStream export(UserLogin userLogin, String distributorId, String _fromDate, String _toDate,
			String lang) {
		lang = getLang(lang);

		Collection<String> distributorIds = null;
		if (distributorId == null) {
			List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
			distributorIds = getIdSet(distributors);
		} else if (distributorId.startsWith("all_")) {
			distributorId = distributorId.substring(distributorId.indexOf("_") + 1);
			I_Region region = regionRepository.getById(userLogin.getClientId(), distributorId);
			distributorIds = userRepository.getDistributorForRegionRecursively(userLogin.getClientId(), region,
					new ArrayList<>());
		} else {
			I_Distributor distributor = getMandatoryPO(userLogin, distributorId, distributorRepository);
			BusinessAssert.isTrue(checkAccessible(userLogin, distributor.getId()), "distributor not accessible");
			distributorIds = Collections.singleton(distributor.getId());
		}

		SimpleDate fromDate = getMandatoryIsoDate(_fromDate);
		SimpleDate toDate = getMandatoryIsoDate(_toDate);
		BusinessAssert.isTrue(fromDate.compareTo(toDate) <= 0, "fromDate > toDate");

		// Constraint of duration between from - to date is no greater than 1
		// months
		BusinessAssert.isTrue(DateTimeUtils.addMonths(fromDate, 1).compareTo(toDate) >= 0, "greater than 1 month");
		Period period = new Period(fromDate, DateTimeUtils.addDays(toDate, 1));

		List<I_ProductExchangeReturnQuantity> productExchangeReturnQuantities = exchangeReturnRepository
				.getProductExchangeReturnQuantities(userLogin.getClientId(), distributorIds, period);
		// HashMap<String, BigDecimal> productExchangeMap = new HashMap<>();
		// HashMap<String, BigDecimal> productReturnMap = new HashMap<>();
		// for (I_ProductExchangeReturnQuantity productExchangeReturnQuantity :
		// productExchangeReturnQuantities) {
		// productExchangeMap.put(productExchangeReturnQuantity.getProductId(),
		// productExchangeReturnQuantity.getExchangeQuantity());
		// productReturnMap.put(productExchangeReturnQuantity.getProductId(),
		// productExchangeReturnQuantity.getReturnQuantity());
		// }
		List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
		List<I_Distributor> distributors = distributorRepository.getListByIds(userLogin.getClientId(), distributorIds);

		HashMap<String, HashMap<String, List<I_ProductExchangeReturnQuantity>>> mapByDistributor = new HashMap<>();
		for (I_Distributor distributor : distributors) {
			String keyByDistributor = distributor.getName().trim().toLowerCase();
			HashMap<String, List<I_ProductExchangeReturnQuantity>> mapByProduct = new HashMap<>();
			for (I_ProductExchangeReturnQuantity productExchangeReturnQuantityByProduct : productExchangeReturnQuantities) {
				if (keyByDistributor
						.equals(productExchangeReturnQuantityByProduct.getDistributor().trim().toLowerCase())) {
					String keyByProduct = keyByDistributor + "_"
							+ productExchangeReturnQuantityByProduct.getProductId();

					List<I_ProductExchangeReturnQuantity> listProductExchangeReturnQuantity = new ArrayList<>();
					listProductExchangeReturnQuantity.add(productExchangeReturnQuantityByProduct);
					if (mapByProduct.containsKey(keyByProduct)) {
						listProductExchangeReturnQuantity.addAll(mapByProduct.get(keyByProduct));
					}
					mapByProduct.put(keyByProduct, listProductExchangeReturnQuantity);
				}
			}
			if (mapByDistributor.containsKey(keyByDistributor)) {
				mapByProduct.putAll(mapByDistributor.get(keyByDistributor));
			}
			mapByDistributor.put(keyByDistributor, mapByProduct);
		}

		XSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet(translate(lang, "exchange.return"));

			XSSFRow row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, 7));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "product.list"));

			row = sheet.createRow(3);
			createCell(row, 0, getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "distributor"));
			createCell(row, 1, getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "product.name"));
			createCell(row, 2, getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "product.code"));
			createCell(row, 3, getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "customer"));
			createCell(row, 4, getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "exchange"));
			createCell(row, 5, getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "return"));
			createCell(row, 6, getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "time"));
			createCell(row, 7, getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "salesman"));

			int rownum = 3;
			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			for (I_Distributor distributor : distributors) {
				String keyByDistributor = distributor.getName().trim().toLowerCase();
				row = sheet.createRow(++rownum);
				createCell(row, 0, textCellStyle, distributor.getName());
				createCell(row, 1, textCellStyle, "");
				createCell(row, 2, textCellStyle, "");
				createCell(row, 3, textCellStyle, "");
				createCell(row, 4, textCellStyle, "");
				createCell(row, 5, textCellStyle, "");
				createCell(row, 6, textCellStyle, "");
				createCell(row, 7, textCellStyle, "");
				HashMap<String, List<I_ProductExchangeReturnQuantity>> mapByProduct = mapByDistributor
						.get(keyByDistributor);
				for (I_Product product : products) {
					row = sheet.createRow(++rownum);
					createCell(row, 0, textCellStyle, "");
					createCell(row, 1, textCellStyle, product.getName());
					createCell(row, 2, textCellStyle, product.getCode());
					createCell(row, 3, textCellStyle, "");
					createCell(row, 4, textCellStyle, "");
					createCell(row, 5, textCellStyle, "");
					createCell(row, 6, textCellStyle, "");
					createCell(row, 7, textCellStyle, "");
					String keyByProduct = keyByDistributor + "_" + product.getId();
					List<I_ProductExchangeReturnQuantity> setProductExchangeReturnQuantity = mapByProduct
							.get(keyByProduct);
					if (setProductExchangeReturnQuantity != null) {
						for (I_ProductExchangeReturnQuantity productExchangeReturnQuantity : setProductExchangeReturnQuantity) {
							row = sheet.createRow(++rownum);
							createCell(row, 0, textCellStyle, "");
							createCell(row, 1, textCellStyle, "");
							createCell(row, 2, textCellStyle, "");
							String customer = productExchangeReturnQuantity.getCustomer();
							customer = StringUtils.isNullOrEmpty(customer) ? "" : customer;
							createCell(row, 3, textCellStyle, customer);
							BigDecimal exchangeQuatity = productExchangeReturnQuantity.getExchangeQuantity();
							exchangeQuatity = exchangeQuatity == null ? BigDecimal.ZERO : exchangeQuatity;
							createCell(row, 4, numberCellStyle, exchangeQuatity.intValue());
							BigDecimal returnQuantity = productExchangeReturnQuantity.getReturnQuantity();
							returnQuantity = returnQuantity == null ? BigDecimal.ZERO : returnQuantity;
							createCell(row, 5, numberCellStyle, returnQuantity.intValue());
							String time = productExchangeReturnQuantity.getTime();
							time = StringUtils.isNullOrEmpty(time) ? "" : time;
							createCell(row, 6, textCellStyle, time);
							String salesman = productExchangeReturnQuantity.getCreatedBy();
							salesman = StringUtils.isNullOrEmpty(salesman) ? "" : salesman;
							createCell(row, 7, textCellStyle, salesman);
						}
					}
				}
			}

			File outTempFile = File.createTempFile("ExchangeReturn" + System.currentTimeMillis(), "tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new FileInputStream(outTempFile);
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

}
