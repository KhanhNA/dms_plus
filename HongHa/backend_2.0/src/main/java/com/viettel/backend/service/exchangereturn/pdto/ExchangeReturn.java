package com.viettel.backend.service.exchangereturn.pdto;

import java.math.BigDecimal;
import java.util.List;

import com.viettel.backend.service.core.pdto.PO;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.customer.I_CustomerEmbed;
import com.viettel.repository.common.domain.data.I_ExchangeReturn;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.SimpleDate;

public class ExchangeReturn extends PO implements I_ExchangeReturn {

    private static final long serialVersionUID = -2813310315322290251L;
    
    public boolean exchange;
    public BigDecimal quantity;
    public I_CustomerEmbed customer;
    public I_UserEmbed createdBy;
    public SimpleDate createdTime;
    public I_CategoryEmbed distributor;
    public List<I_ProductQuantity> details;

    public boolean isExchange() {
        return exchange;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public I_CustomerEmbed getCustomer() {
        return customer;
    }

    public I_UserEmbed getCreatedBy() {
        return createdBy;
    }

    public SimpleDate getCreatedTime() {
        return createdTime;
    }

    public I_CategoryEmbed getDistributor() {
        return distributor;
    }

    public List<I_ProductQuantity> getDetails() {
        return details;
    }

    public void setExchange(boolean exchange) {
        this.exchange = exchange;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public void setCustomer(I_CustomerEmbed customer) {
        this.customer = customer;
    }

    public void setCreatedBy(I_UserEmbed createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        this.createdTime = createdTime;
    }

    public void setDistributor(I_CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public void setDetails(List<I_ProductQuantity> details) {
        this.details = details;
    }

}
