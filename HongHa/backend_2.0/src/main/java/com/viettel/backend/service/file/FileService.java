package com.viettel.backend.service.file;

import java.io.InputStream;

import com.viettel.backend.dto.common.DTOSimple;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.domain.file.I_File;

public interface FileService {

	public IdDto storeFile(UserLogin userLogin, InputStream inputStream, byte[] bytes, String fileName,
			String contentType);

	public FileDto getFile(UserLogin userLogin, String id);

	public IdDto storeImage(UserLogin userLogin, InputStream inputStream, byte[] bytes, String fileName,
			String contentType, String sizeType);

	public FileDto getImage(String id);

	public void delete(UserLogin userLogin, String id);

	public static class FileDto extends DTOSimple {

		private static final long serialVersionUID = -5701994024221232278L;

		private String fileName;
		private String contentType;
		private InputStream inputStream;

		public FileDto(I_File file) {
			super(file);
			this.fileName = file.getFileName();
			this.contentType = file.getContentType();
			this.inputStream = file.getInputStream();
		}

		public String getFileName() {
			return fileName;
		}

		public String getContentType() {
			return contentType;
		}

		public InputStream getInputStream() {
			return inputStream;
		}

	}

}
