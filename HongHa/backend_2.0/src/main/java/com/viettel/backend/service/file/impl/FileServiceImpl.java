package com.viettel.backend.service.file.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.file.FileService;
import com.viettel.backend.util.FileUtils;
import com.viettel.backend.util.ImageUtils;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.domain.file.I_File;

@Service
public class FileServiceImpl extends AbstractService implements FileService {

	@Autowired
	private FileRepository fileRepository;

	@Override
	public IdDto storeFile(UserLogin userLogin, InputStream inputStream, byte[] bytes, String fileName,
			String contentType) {
		BusinessAssert.notNull(inputStream);
		BusinessAssert.notNull(fileName);
		BusinessAssert.notNull(contentType);

		if (FileUtils.isExtensionValid(fileName) && FileUtils.getWhiteListContentType().contains(contentType)
				&& FileUtils.isHeaderValid(bytes)) {
			String fileId = fileRepository.store(userLogin.getClientId(), inputStream, fileName, contentType);
			return new IdDto(fileId);
		} else {
			throw new BusinessException(BusinessExceptionCode.FILE_INVALID, "it's not valid file");
		}
	}

	@Override
	public IdDto storeImage(UserLogin userLogin, InputStream _inputStream, byte[] bytes, String fileName,
			String contentType, String sizeType) {
		BusinessAssert.notNull(_inputStream);
		BusinessAssert.notNull(fileName);
		BusinessAssert.notNull(contentType);

		InputStream inputStream;
		if (sizeType == null) {
			inputStream = _inputStream;
		} else {
			try {
				byte[] bs = IOUtils.toByteArray(_inputStream);

				bs = ImageUtils.resizeImage(bs, sizeType);

				inputStream = new ByteArrayInputStream(bs);
			} catch (IOException e) {
				throw new UnsupportedOperationException("error when write photo", e);
			} catch(NullPointerException e){
				throw new BusinessException(BusinessExceptionCode.FILE_INVALID,"invalid image file");
			}
		}

		return storeFile(userLogin, inputStream, bytes, fileName, contentType);
	}

	@Override
	public FileDto getFile(UserLogin userLogin, String id) {
		BusinessAssert.notNull(id);

		I_File file = fileRepository.getFileById(userLogin.getClientId(), id);

		BusinessAssert.notNull(file);

		return new FileDto(file);
	}

	@Override
	public FileDto getImage(String id) {
		BusinessAssert.notNull(id);

		I_File file = fileRepository.getFileById(null, id);

		BusinessAssert.notNull(file);

		return new FileDto(file);
	}

	@Override
	public void delete(UserLogin userLogin, String id) {
		fileRepository.delete(userLogin.getClientId(), id);
	}

}
