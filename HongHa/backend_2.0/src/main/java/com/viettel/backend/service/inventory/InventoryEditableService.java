package com.viettel.backend.service.inventory;

import java.math.BigDecimal;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.inventory.InventoryCreateDto;
import com.viettel.backend.dto.inventory.InventoryDto;
import com.viettel.backend.dto.inventory.InventoryListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface InventoryEditableService {
    
    // Mot nghin ti
    public static final BigDecimal MAX_QUANTITY_VALUE = new BigDecimal("1000000000000");
    
    public ListDto<InventoryListDto> getInventoryList(UserLogin userLogin, PageSizeRequest pageSizeRequest);
    
    public InventoryDto getById(UserLogin userLogin, String id);
    
    public IdDto create(UserLogin userLogin, InventoryCreateDto dto);

    public IdDto update(UserLogin userLogin, String id, InventoryCreateDto dto);

    public boolean enable(UserLogin userLogin, String id, boolean recheck);

    public boolean delete(UserLogin userLogin, String id);
    
}
