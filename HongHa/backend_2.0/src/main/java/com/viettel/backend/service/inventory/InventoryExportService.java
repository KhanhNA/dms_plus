package com.viettel.backend.service.inventory;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.report.ReportDistributorListIdDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface InventoryExportService {
	/*
	 * way is all -> get all accessible distributors
	 * way is specify -> get distributors in dto
	 */
    public IdDto exportInventoryReportByDistributor(UserLogin userLogin,String regionId, String way, String lang,ReportDistributorListIdDto dto);
    
    public ExportDto exportInventoryReportByProduct(UserLogin userLogin, String productId, String lang);

}
