package com.viettel.backend.service.inventory;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.inventory.InventoryImportResultDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface InventoryImportService {
    
    public byte[] getImportTemplate(UserLogin userLogin, String lang);
    
    public ImportConfirmDto verifyImport(UserLogin userLogin, String time, String fileId);
    
    public InventoryImportResultDto importInventory(UserLogin userLogin, String time, String fileId);

}
