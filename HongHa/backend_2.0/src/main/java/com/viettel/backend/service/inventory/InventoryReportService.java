package com.viettel.backend.service.inventory;

import com.viettel.backend.dto.inventory.InventoryReportByDistributorListDto;
import com.viettel.backend.dto.inventory.InventoryReportByProductListDto;
import com.viettel.backend.dto.report.ReportDistributorListIdDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface InventoryReportService {
	/**
	 * 
	 * @param wayToGetDistributor Can be 'all' or 'specify'. 
	 *  In case of 'specify', list of distributors store in dto.
	 *  In case of 'all' it means all accessible distributors
	 * @param 
	 * @return
	 */
    public InventoryReportByDistributorListDto getInventoryReportByDistributor(UserLogin userLogin, String regionId,String way,ReportDistributorListIdDto dto);
    
    public InventoryReportByProductListDto getInventoryReportByProduct(UserLogin userLogin, String productId);

}
