package com.viettel.backend.service.inventory.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.inventory.InventoryCreateDto;
import com.viettel.backend.dto.inventory.InventoryCreateDto.InventoryCreateDetailDto;
import com.viettel.backend.dto.inventory.InventoryDto;
import com.viettel.backend.dto.inventory.InventoryListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.ProductQuantity;
import com.viettel.backend.service.inventory.InventoryEditableService;
import com.viettel.backend.service.inventory.pdto.Inventory;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.InventoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.inventory.I_InventoryHeader;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

import reactor.core.support.Assert;

@RolePermission(value = { Role.DISTRIBUTOR_ADMIN })
@RequireModule(Module.INVENTORY)
@Service
public class InventoryEditableServiceImpl extends AbstractService implements InventoryEditableService {

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ConfigRepository configRepository;
    
    @Autowired
    private CacheVisitOrderService cacheVisitOrderService;

    @Override
    public ListDto<InventoryListDto> getInventoryList(UserLogin userLogin, PageSizeRequest pageSizeRequest) {

        String distributorId = getDefaultDistributor(userLogin).getId();

        List<I_InventoryHeader> inventories = inventoryRepository.getList(userLogin.getClientId(), distributorId, pageSizeRequest);
        if (CollectionUtils.isEmpty(inventories)) {
            return ListDto.emptyList();
        }
        List<InventoryListDto> dtos = new ArrayList<>(inventories.size());
        for (I_InventoryHeader inventory : inventories) {
            InventoryListDto dto = new InventoryListDto(inventory);
            dtos.add(dto);
        }

        long count = inventoryRepository.count(userLogin.getClientId(), distributorId);

        return new ListDto<>(dtos, count);
    }

    @Override
    public InventoryDto getById(UserLogin userLogin, String _id) {
        I_Inventory domain = getMandatoryPO(userLogin, _id, null, true, inventoryRepository);

        BusinessAssert.isTrue(checkAccessible(userLogin, domain.getDistributor().getId()));

        return new InventoryDto(domain, getProductPhotoFactory(userLogin));
    }

    @Override
    public IdDto create(UserLogin userLogin, InventoryCreateDto dto) {
        BusinessAssert.notNull(dto);
        BusinessAssert.notNull(dto.getTime());
        BusinessAssert.notEmpty(dto.getDetails());

        Inventory domain = new Inventory();
        initiatePO(userLogin.getClientId(), domain);
        domain.setDraft(true);

        I_Distributor distributor = getDefaultDistributor(userLogin);
        domain.setDistributor(new CategoryEmbed(distributor));

        fillInforFromDto(userLogin, domain, dto, distributor.getId());

        return new IdDto(inventoryRepository.save(userLogin.getClientId(), domain));
    }

    private void fillInforFromDto(UserLogin userLogin, Inventory domain, InventoryCreateDto dto,
            String distributorId) {
        BusinessAssert.notNull(dto);
        BusinessAssert.notNull(dto.getTime());
        BusinessAssert.notEmpty(dto.getDetails());

        SimpleDate time = getMandatoryIsoTime(dto.getTime());

        // Get latest inventory record of distributor and compare date time
        I_Inventory latestInventoryRecord = inventoryRepository.getLatestInventory(userLogin.getClientId(),
                distributorId);
        if (latestInventoryRecord != null) {
            // Validate time
            BusinessAssert.isTrue(latestInventoryRecord.getTime().compareTo(time) < 0,
                    BusinessExceptionCode.INVENTORY_TIME_INVALID, "Inventory time invalid");
        }

        domain.setTime(time);

        // Load Product map
        final Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);
        HashSet<String> productIds = new HashSet<>(productMap.keySet());

        // Verify all product contains in map and not duplicate - Create domain
        // details
        List<I_ProductQuantity> details = new ArrayList<>(dto.getDetails().size());
        for (InventoryCreateDetailDto createDetailDto : dto.getDetails()) {
            String productId = createDetailDto.getProductId();

            // PRODUCT STILL ACTIVE
            if (productIds.contains(productId)) {
                productIds.remove(productId);

                I_Product product = productMap.get(productId);

                BusinessAssert.notNull(product, BusinessExceptionCode.PRODUCT_NOT_FOUND, "Product not found");
                BusinessAssert.isTrue(createDetailDto.getQuantity() != null
                        && createDetailDto.getQuantity().compareTo(BigDecimal.ZERO) >= 0
                        && createDetailDto.getQuantity().compareTo(MAX_QUANTITY_VALUE) <= 0);

                ProductQuantity inventoryDetail = new ProductQuantity(product);
                // Quantitty
                inventoryDetail.setQuantity(createDetailDto.getQuantity());

                details.add(inventoryDetail);
            } else {
                if (productMap.containsKey(productId)) {
                    throw new BusinessException(BusinessExceptionCode.INVENTORY_PRODUCT_DUPLICATED,
                            "Duplicated product inventory");
                }
            }
        }

        BusinessAssert.isTrue(productIds.isEmpty(), BusinessExceptionCode.INVENTORY_MISSING_PRODUCT);

        domain.setDetails(details);
    }

    @Override
    public IdDto update(UserLogin userLogin, String _id, InventoryCreateDto dto) {
        Inventory inventory = new Inventory(getMandatoryPO(userLogin, _id, null, true, inventoryRepository));

        BusinessAssert.isTrue(checkAccessible(userLogin, inventory.getDistributor().getId()));
        BusinessAssert.isTrue(inventory.isDraft());

        fillInforFromDto(userLogin, inventory, dto, inventory.getDistributor().getId());

        IdDto idDto = new IdDto(inventoryRepository.save(userLogin.getClientId(), inventory));

        return idDto;
    }

    @Override
    public boolean enable(UserLogin userLogin, String _id, boolean recheck) {
        I_Inventory inventory = getMandatoryPO(userLogin, _id, true, true, inventoryRepository);

        BusinessAssert.isTrue(inventory.isDraft());

        String distributorId = inventory.getDistributor().getId();

        BusinessAssert.isTrue(checkAccessible(userLogin, distributorId));

        if (recheck) {
        	// Inventory date cannot larger than today
        	SimpleDate tomorrow = DateTimeUtils.getTomorrow();
        	BusinessAssert.isTrue(tomorrow.compareTo(inventory.getTime()) > 0,
                    BusinessExceptionCode.INVENTORY_TIME_AFTER_TODAY, "Inventory time larger than today");
        	
            // Get latest inventory record of distributor and compare date time
            I_Inventory latestInventoryRecord = inventoryRepository.getLatestInventory(userLogin.getClientId(),
                    distributorId);
            if (latestInventoryRecord != null) {
                // Validate time
                BusinessAssert.isTrue(latestInventoryRecord.getTime().compareTo(inventory.getTime()) < 0,
                        BusinessExceptionCode.INVENTORY_TIME_INVALID, "Inventory time invalid");
            }
            
            // Validate time with inventory expiration time
            I_Config config = configRepository.getConfig(userLogin.getClientId());
            Assert.notNull(config);
            SimpleDate today = DateTimeUtils.getToday();
            BusinessAssert.isTrue(
                    DateTimeUtils.addDays(inventory.getTime(), config.getNumberDayInventoryExpire()).compareTo(today) >= 0,
                    BusinessExceptionCode.INVENTORY_TIME_EXCEED_EXPIRATION_LIMIT, "Inventory time exceed expiration limit");

            // Load Product map
            final Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);
            HashSet<String> productIds = new HashSet<>(productMap.keySet());
            List<I_ProductQuantity> details = new LinkedList<>();

            for (I_ProductQuantity inventoryDetail : inventory.getDetails()) {
                if (productIds.contains(inventoryDetail.getId())) {
                    productIds.remove(inventoryDetail.getId());
                    details.add(inventoryDetail);
                }
            }

            BusinessAssert.isTrue(productIds.isEmpty(), BusinessExceptionCode.INVENTORY_MISSING_PRODUCT);

            Inventory _inventory = new Inventory(inventory);
            _inventory.setDetails(details);
            inventoryRepository.saveAndEnable(userLogin.getClientId(), _inventory);
        } else {
            inventoryRepository.enable(userLogin.getClientId(), inventory.getId());
        }

        cacheVisitOrderService.clearQuantitySoldByProductFromLastInventoryUpdate(userLogin.getClientId(),
                distributorId);

        return true;
    }

    @Override
    public boolean delete(UserLogin userLogin, String _id) {
        I_Inventory domain = getMandatoryPO(userLogin, _id, true, true, inventoryRepository);

        BusinessAssert.isTrue(domain.isDraft());

        BusinessAssert.isTrue(checkAccessible(userLogin, domain.getDistributor().getId()));

        return inventoryRepository.delete(userLogin.getClientId(), domain.getId());
    }

}
