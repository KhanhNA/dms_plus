package com.viettel.backend.service.inventory.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.ExportDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.inventory.InventoryReportByDistributorListDto;
import com.viettel.backend.dto.inventory.ProductInventoryResultDto;
import com.viettel.backend.dto.report.ReportDistributorListIdDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.common.AbstractExportService;
import com.viettel.backend.service.inventory.InventoryExportService;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.InventoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@RequireModule(Module.INVENTORY)
@Service
public class InventoryExportServiceImpl extends AbstractExportService implements InventoryExportService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private InventoryRepository inventoryRepository;

	@Autowired
	private CacheVisitOrderService cacheVisitOrderService;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private FileRepository fileRepository;

	@Override
	public IdDto exportInventoryReportByDistributor(UserLogin userLogin, String regionId, String way, String lang,
			ReportDistributorListIdDto dto) {
		lang = getLang(lang);
		ExportDto exportDto = null;
		if ("all".equalsIgnoreCase(way)) {
			if (regionId == null) {
				exportDto = exportInventoryReportByAccessibleDistributors(userLogin, lang,
						getAccessibleDistributors(userLogin));
			} else {
				exportDto = exportInventoryReportByAccessibleDistributors(userLogin, lang,
						getDistributorsByRegion(userLogin, regionId));
			}

		} else if ("specify".equalsIgnoreCase(way)) {
			exportDto = exportInventoryReportByAccessibleDistributors(userLogin, lang,
					distributorRepository.getDistributorByIds(userLogin.getClientId(), dto.getDistributorIds()));
		}
		String id = fileRepository.store(userLogin.getClientId(), exportDto.getInputStream(), exportDto.getFileName(),
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		return new IdDto(id);
	}

	private ExportDto exportInventoryReportByAccessibleDistributors(UserLogin userLogin, String lang,
			List<I_Distributor> distributors) {
		Set<String> distributorIds = getIdSet(distributors);
		Map<String, I_Inventory> lastInventoryByccessibleDistributor = inventoryRepository
				.getLatestInventorys(userLogin.getClientId(), distributorIds);

		Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);

		// Get number of sold products
		Map<String, Double> productSoldMap = new HashMap<>();
		for (String distributorId : distributorIds) {
			Map<String, Double> productSoldMapOfDistributor = cacheVisitOrderService
					.getQuantitySoldByProductFromLastInventoryUpdate(userLogin.getClientId(), distributorId);
			for (String productId : productSoldMapOfDistributor.keySet()) {
				if (productSoldMap.containsKey(productId)) {
					double currentSold = productSoldMap.get(productId);
					productSoldMap.put(productId, currentSold + productSoldMapOfDistributor.get(productId));
				} else {
					productSoldMap.put(productId, productSoldMapOfDistributor.get(productId));
				}
			}
		}
		// Get original number of product of inventory
		Map<String, BigDecimal> lastUpdateQuantityMap = new HashMap<>();
		for (I_Inventory inventory : lastInventoryByccessibleDistributor.values()) {
			for (I_ProductQuantity productQuantity : inventory.getDetails()) {
				if (lastUpdateQuantityMap.containsKey(productQuantity.getId())) {
					BigDecimal currentQuantity = lastUpdateQuantityMap.get(productQuantity.getId());
					lastUpdateQuantityMap.put(productQuantity.getId(),
							currentQuantity.add(productQuantity.getQuantity()));
				} else {
					lastUpdateQuantityMap.put(productQuantity.getId(), productQuantity.getQuantity());
				}
			}
		}
		// Build excel
		SimpleDate currentTime = DateTimeUtils.getCurrentTime();

		String na = translate(lang, "n.a");
		BigDecimal totalLastUpdateQuantity = BigDecimal.ZERO;
		BigDecimal totalSoldQuantity = BigDecimal.ZERO;
		BigDecimal totalUnsoldQuantity = BigDecimal.ZERO;
		BigDecimal totalAmount = BigDecimal.ZERO;

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet 1
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "inventory.multiple.distributor"));

			Row row;

			String[] headers = new String[] { "product", "product.code", "product.category", "uom",
					"inventory.last.update.quantity", "inventory.sold.quantity", "inventory.available.quantity",
					"inventory.amount" };

			int lastCol = headers.length - 1;

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "inventory.report.by.distributor"));
			// Created date - Inventory date
			row = sheet.createRow(2);
			sheet.addMergedRegion(new CellRangeAddress(2, 2, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "report.created.time") + ": " + currentTime.format("dd/MM/yyyy HH:mm"));

			// Create table headers
			row = sheet.createRow(4);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			int rownum = 5;

			DecimalFormat df = new DecimalFormat();
			df.setMinimumFractionDigits(3);
			df.setMaximumFractionDigits(3);
			df.setMinimumIntegerDigits(1);
			df.setMaximumIntegerDigits(3);
			df.setGroupingSize(20);

			List<I_Product> products = new ArrayList<>(productMap.values());
			Collections.sort(products, new Comparator<I_Product>() {
				@Override
				public int compare(I_Product o1, I_Product o2) {
					if (o1.getProductCategory().getId().equals(o2.getProductCategory().getId())) {
						return o1.getName().compareTo(o2.getName());
					} else {
						return o1.getProductCategory().getName().compareTo(o2.getProductCategory().getName());
					}
				}
			});

			for (I_Product product : products) {
				row = sheet.createRow(rownum++);

				String productId = product.getId();

				createCell(row, 0, textCellStyle, product.getName());
				createCell(row, 1, textCellStyle, product.getCode());
				createCell(row, 2, textCellStyle, product.getProductCategory().getName());
				createCell(row, 3, textCellStyle, product.getUom().getName());

				if (lastUpdateQuantityMap == null) {
					createCellNumber(row, lastCol - 2, numberCellStyle, null);
					createCellNumber(row, lastCol - 1, numberCellStyle, null);
					createCellNumber(row, lastCol, numberCellStyle, null);
				} else {
					BigDecimal lastUpdateQuantity = lastUpdateQuantityMap.get(productId);
					lastUpdateQuantity = lastUpdateQuantity != null ? lastUpdateQuantity : BigDecimal.ZERO;
					Double soldQuantity = productSoldMap.get(productId);
					if (soldQuantity == null) {
						soldQuantity = 0.0d;
					}
					totalLastUpdateQuantity = totalLastUpdateQuantity.add(lastUpdateQuantity);
					totalSoldQuantity = totalSoldQuantity.add(BigDecimal.valueOf(soldQuantity));
					totalUnsoldQuantity = totalUnsoldQuantity
							.add(lastUpdateQuantity.subtract(BigDecimal.valueOf(soldQuantity)));
					totalAmount = totalAmount.add(
							lastUpdateQuantity.subtract(new BigDecimal(soldQuantity)).multiply(product.getPrice()));

					createCellNumber(row, lastCol - 3, numberCellStyle, lastUpdateQuantity.toString());
					createCellNumber(row, lastCol - 2, numberCellStyle, soldQuantity.toString());
					createCellNumber(row, lastCol - 1, numberCellStyle, lastUpdateQuantity != null
							? lastUpdateQuantity.subtract(new BigDecimal(soldQuantity)).toString() : null);
					createCellNumber(row, lastCol, numberCellStyle, lastUpdateQuantity != null ? lastUpdateQuantity
							.subtract(new BigDecimal(soldQuantity)).multiply(product.getPrice()).toString() : null);
				}
			}

			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);
			int totalRow = rownum++;
			row = sheet.createRow(totalRow);
			createCell(row, 0, footerTextCellStyle, translate(lang, "total"));
			createCellNumber(row, lastCol - 3, footerTextCellStyle, totalLastUpdateQuantity.toString());
			createCellNumber(row, lastCol - 2, footerTextCellStyle, totalSoldQuantity.toString());
			createCellNumber(row, lastCol - 1, footerTextCellStyle, totalUnsoldQuantity.toString());
			createCellNumber(row, lastCol, footerTextCellStyle, totalAmount.toString());

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
			createCell(row, 0, footerTextCellStyle, productMap.size() + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			// Create sheet 2
			SXSSFSheet sheet2 = (SXSSFSheet) workbook.createSheet(translate(lang, "inventory.update"));
			Row row2;
			row2 = sheet2.createRow(1);
			sheet2.addMergedRegion(new CellRangeAddress(1, 1, 0, 6));
			createCell(row2, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "inventory.update"));
			row2 = sheet2.createRow(3);
			createCell(row2, 0, headerCellStyle, translate(lang, "distributor"));
			createCell(row2, 1, headerCellStyle, translate(lang, "time"));
			int rowCount = 4;
			for (I_Distributor distributor : distributors) {
				row2 = sheet2.createRow(rowCount++);
				I_Inventory inventory = lastInventoryByccessibleDistributor.get(distributor.getId());
				createCell(row2, 0, textCellStyle, distributor.getName());
				createCell(row2, 1, textCellStyle, inventory != null && inventory.getTime() != null
						? inventory.getTime().format("dd/MM/yyyy HH:mm") : null);
			}
			sheet2.autoSizeColumn(0);
			sheet2.autoSizeColumn(1);
			// Create detail for each distributor
			int sheetDetailNumber = 0;
			for (I_Inventory inventory : lastInventoryByccessibleDistributor.values()) {
				SXSSFSheet sheetDetail = (SXSSFSheet) workbook
						.createSheet("DIS" + String.format("%3d", sheetDetailNumber + 1));
				sheetDetailNumber++;
				createSheetDetail(workbook, sheetDetail, products, userLogin, inventory.getDistributor().getId(), lang);

			}
			// Build file
			StringBuilder fileName = new StringBuilder();
			fileName.append("InventoryReportDistributors_");
			fileName.append(currentTime.format("yyyyMMddHHmm"));
			fileName.append(".xlsx");

			File outTempFile = File.createTempFile(fileName.toString() + "_" + System.currentTimeMillis(), ".tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new ExportDto(fileName.toString(), new FileInputStream(outTempFile));
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

	private void createSheetDetail(Workbook workbook, Sheet sheetDetail, List<I_Product> products, UserLogin userLogin,
			String distributorId, String lang) {
		I_Distributor distributor = getMandatoryDistributor(userLogin, distributorId);
		distributorId = distributor.getId();

		I_Inventory inventory = inventoryRepository.getLatestInventory(userLogin.getClientId(), distributorId);
		Map<String, BigDecimal> lastUpdateQuantityMap = null;
		if (inventory != null) {
			lastUpdateQuantityMap = new HashMap<>(inventory.getDetails().size());
			for (I_ProductQuantity productQuantity : inventory.getDetails()) {
				lastUpdateQuantityMap.put(productQuantity.getId(), productQuantity.getQuantity());
			}
		}

		Map<String, Double> productSoldMap = cacheVisitOrderService
				.getQuantitySoldByProductFromLastInventoryUpdate(userLogin.getClientId(), distributorId);

		// Current product map
		Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);

		SimpleDate currentTime = DateTimeUtils.getCurrentTime();

		String na = translate(lang, "n.a");
		BigDecimal totalLastUpdateQuantity = BigDecimal.ZERO;
		BigDecimal totalSoldQuantity = BigDecimal.ZERO;
		BigDecimal totalUnsoldQuantity = BigDecimal.ZERO;
		BigDecimal totalAmount = BigDecimal.ZERO;
		String[] headers = new String[] { "product", "product.code", "product.category", "uom",
				"inventory.last.update.quantity", "inventory.sold.quantity", "inventory.available.quantity",
				"inventory.amount" };

		CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
		CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
		CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

		int lastCol = headers.length - 1;

		Row rowDetail;
		rowDetail = sheetDetail.createRow(1);
		sheetDetail.addMergedRegion(new CellRangeAddress(1, 1, 0, lastCol));
		createCell(rowDetail, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
				translate(lang, "inventory.report.by.distributor"));

		rowDetail = sheetDetail.createRow(3);
		sheetDetail.addMergedRegion(new CellRangeAddress(3, 3, 0, lastCol));
		createCell(rowDetail, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
				translate(lang, "distributor") + ": " + inventory.getDistributor().getName());

		rowDetail = sheetDetail.createRow(4);
		sheetDetail.addMergedRegion(new CellRangeAddress(4, 4, 0, lastCol));
		createCell(rowDetail, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
				translate(lang, "inventory.last.update.time") + ": "
						+ (inventory == null ? na : inventory.getTime().format("dd/MM/yyyy HH:mm")));

		// Created date - Inventory date
		rowDetail = sheetDetail.createRow(5);
		sheetDetail.addMergedRegion(new CellRangeAddress(5, 5, 0, lastCol));
		createCell(rowDetail, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
				translate(lang, "report.created.time") + ": " + currentTime.format("dd/MM/yyyy HH:mm"));

		// Create table headers
		rowDetail = sheetDetail.createRow(7);
		int cellnumDetail = 0;

		for (String header : headers) {
			createCell(rowDetail, cellnumDetail++, headerCellStyle, translate(lang, header));
		}

		int rowNumDetail = 8;

		for (I_Product product : products) {
			rowDetail = sheetDetail.createRow(rowNumDetail++);

			String productId = product.getId();

			createCell(rowDetail, 0, textCellStyle, product.getName());
			createCell(rowDetail, 1, textCellStyle, product.getCode());
			createCell(rowDetail, 2, textCellStyle, product.getProductCategory().getName());
			createCell(rowDetail, 3, textCellStyle, product.getUom().getName());

			if (lastUpdateQuantityMap == null) {
				createCellNumber(rowDetail, lastCol - 2, numberCellStyle, null);
				createCellNumber(rowDetail, lastCol - 1, numberCellStyle, null);
				createCellNumber(rowDetail, lastCol, numberCellStyle, null);
			} else {
				BigDecimal lastUpdateQuantity = lastUpdateQuantityMap.get(productId);
				lastUpdateQuantity = lastUpdateQuantity != null ? lastUpdateQuantity : BigDecimal.ZERO;
				Double soldQuantity = productSoldMap.get(productId);
				if (soldQuantity == null) {
					soldQuantity = 0.0d;
				}
				totalLastUpdateQuantity = totalLastUpdateQuantity.add(lastUpdateQuantity);
				totalSoldQuantity = totalSoldQuantity.add(BigDecimal.valueOf(soldQuantity));
				totalUnsoldQuantity = totalUnsoldQuantity
						.add(lastUpdateQuantity.subtract(BigDecimal.valueOf(soldQuantity)));
				totalAmount = totalAmount
						.add(lastUpdateQuantity.subtract(new BigDecimal(soldQuantity)).multiply(product.getPrice()));

				createCellNumber(rowDetail, lastCol - 3, numberCellStyle, lastUpdateQuantity.toString());
				createCellNumber(rowDetail, lastCol - 2, numberCellStyle, soldQuantity.toString());
				createCellNumber(rowDetail, lastCol - 1, numberCellStyle, lastUpdateQuantity != null
						? lastUpdateQuantity.subtract(new BigDecimal(soldQuantity)).toString() : null);
				createCellNumber(rowDetail, lastCol, numberCellStyle, lastUpdateQuantity != null ? lastUpdateQuantity
						.subtract(new BigDecimal(soldQuantity)).multiply(product.getPrice()).toString() : null);
			}
		}

		// Footer
		CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);
		int totalRow = rowNumDetail++;
		rowDetail = sheetDetail.createRow(totalRow);
		createCell(rowDetail, 0, footerTextCellStyle, translate(lang, "total"));
		createCellNumber(rowDetail, lastCol - 3, footerTextCellStyle, totalLastUpdateQuantity.toString());
		createCellNumber(rowDetail, lastCol - 2, footerTextCellStyle, totalSoldQuantity.toString());
		createCellNumber(rowDetail, lastCol - 1, footerTextCellStyle, totalUnsoldQuantity.toString());
		createCellNumber(rowDetail, lastCol, footerTextCellStyle, totalAmount.toString());

		int lastRownum = rowNumDetail++;
		rowDetail = sheetDetail.createRow(lastRownum);
		sheetDetail.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
		createCell(rowDetail, 0, footerTextCellStyle, productMap.size() + " " + translate(lang, "record(s)"));

		for (int i = 0, length = headers.length; i < length; i++) {
			sheetDetail.autoSizeColumn(i);
		}
	}

	@Override
	public ExportDto exportInventoryReportByProduct(UserLogin userLogin, String _productId, String lang) {
		lang = getLang(lang);
		if ("all".equalsIgnoreCase(_productId)) {

		}
		String clientId = userLogin.getClientId();

		I_Product product = getMandatoryPO(userLogin, _productId, productRepository);
		String productId = product.getId();

		// Get list of all distributor that accessible by current user
		List<I_Distributor> accessibleDistributors = getAccessibleDistributors(userLogin);
		Set<String> accessibleDistributorIds = getIdSet(accessibleDistributors);

		Map<String, I_Inventory> lastInventoryByDistributor = inventoryRepository.getLatestInventorys(clientId,
				accessibleDistributorIds);

		SimpleDate currentTime = DateTimeUtils.getCurrentTime();

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "inventory"));

			Row row;

			String[] headers = new String[] { "distributor.name", "distributor.code", "inventory.last.update.time",
					"inventory.last.update.quantity", "inventory.sold.quantity", "inventory.available.quantity",
					"inventory.amount" };

			int lastCol = headers.length - 1;

			// Create headers
			row = sheet.createRow(1);
			sheet.addMergedRegion(new CellRangeAddress(1, 1, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_CENTER),
					translate(lang, "inventory.report.by.product"));

			row = sheet.createRow(3);
			sheet.addMergedRegion(new CellRangeAddress(3, 3, 0, lastCol / 2));
			sheet.addMergedRegion(new CellRangeAddress(3, 3, (lastCol / 2) + 1, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "product.name") + ": " + product.getName());
			createCell(row, (lastCol / 2) + 1, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "product.code") + ": " + product.getCode());

			row = sheet.createRow(4);
			sheet.addMergedRegion(new CellRangeAddress(4, 4, 0, lastCol));
			createCell(row, 0, getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT),
					translate(lang, "report.created.time") + ": " + currentTime.format("dd/MM/yy HH:mm"));

			// Create table headers
			row = sheet.createRow(7);
			int cellnum = 0;

			CellStyle headerCellStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			for (String header : headers) {
				createCell(row, cellnum++, headerCellStyle, translate(lang, header));
			}

			CellStyle textCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);
			CellStyle numberCellStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_RIGHT);

			int rownum = 8;

			DecimalFormat df = new DecimalFormat();
			df.setMinimumFractionDigits(3);
			df.setMaximumFractionDigits(3);
			df.setMinimumIntegerDigits(1);
			df.setMaximumIntegerDigits(3);
			df.setGroupingSize(20);

			for (I_Distributor distributor : accessibleDistributors) {
				row = sheet.createRow(rownum++);

				createCell(row, 0, textCellStyle, distributor.getName());
				createCell(row, 1, textCellStyle, distributor.getCode());

				I_Inventory inventory = lastInventoryByDistributor.get(distributor.getId());

				if (inventory != null) {
					BigDecimal lastUpdateQuantity = getLastUpdateQuantity(inventory, productId);

					Map<String, Double> productSoldMap = cacheVisitOrderService
							.getQuantitySoldByProductFromLastInventoryUpdate(clientId, distributor.getId());

					Double soldQuantity = productSoldMap != null ? productSoldMap.get(productId) : null;
					if (soldQuantity == null) {
						soldQuantity = 0.0d;
					}

					createCell(row, lastCol - 4, numberCellStyle, inventory.getTime().format("dd/MM/yyyy HH:mm"));
					createCellNumber(row, lastCol - 3, numberCellStyle,
							lastUpdateQuantity != null ? lastUpdateQuantity.toString() : null);
					createCellNumber(row, lastCol - 2, numberCellStyle, new BigDecimal(soldQuantity).toString());
					createCellNumber(row, lastCol - 1, numberCellStyle, lastUpdateQuantity != null
							? lastUpdateQuantity.subtract(new BigDecimal(soldQuantity)).toString() : null);
					createCellNumber(row, lastCol, numberCellStyle, lastUpdateQuantity != null ? lastUpdateQuantity
							.subtract(new BigDecimal(soldQuantity)).multiply(product.getPrice()).toString() : null);

				} else {
					createCell(row, lastCol - 4, numberCellStyle, "");
					createCellNumber(row, lastCol - 3, numberCellStyle, null);
					createCellNumber(row, lastCol - 2, numberCellStyle, null);
					createCellNumber(row, lastCol - 1, numberCellStyle, null);
					createCellNumber(row, lastCol, numberCellStyle, null);
				}
			}

			// Footer
			CellStyle footerTextCellStyle = getCellStyle(workbook, true, false, (short) 11, CellStyle.ALIGN_LEFT);

			int lastRownum = rownum++;
			row = sheet.createRow(lastRownum);
			sheet.addMergedRegion(new CellRangeAddress(lastRownum, lastRownum, 0, headers.length - 1));
			createCell(row, 0, footerTextCellStyle, accessibleDistributors.size() + " " + translate(lang, "record(s)"));

			for (int i = 0, length = headers.length; i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			StringBuilder fileName = new StringBuilder();
			fileName.append("InventoryReportProduct_");
			fileName.append(product.getCode()).append("_");
			fileName.append(DateTimeUtils.getCurrentTime().format("yyyyMMddHHmm"));
			fileName.append(".xlsx");

			File outTempFile = File.createTempFile(fileName + "_" + System.currentTimeMillis(), ".tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new ExportDto(fileName.toString(), new FileInputStream(outTempFile));
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}
	}

	private BigDecimal getLastUpdateQuantity(I_Inventory inventory, String productId) {
		for (I_ProductQuantity productQuantity : inventory.getDetails()) {
			if (productQuantity.getId().equals(productId)) {
				return productQuantity.getQuantity();
			}
		}
		return null;
	}

}
