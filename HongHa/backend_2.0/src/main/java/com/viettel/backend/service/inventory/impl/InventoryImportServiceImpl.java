package com.viettel.backend.service.inventory.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportConfirmDto.RowData;
import com.viettel.backend.dto.inventory.InventoryImportResultDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractImportService;
import com.viettel.backend.service.core.pdto.CategoryEmbed;
import com.viettel.backend.service.core.pdto.ProductQuantity;
import com.viettel.backend.service.inventory.InventoryEditableService;
import com.viettel.backend.service.inventory.InventoryImportService;
import com.viettel.backend.service.inventory.pdto.Inventory;
import com.viettel.repository.common.InventoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;

@RolePermission(value = { Role.DISTRIBUTOR_ADMIN })
@RequireModule(Module.INVENTORY)
@Service
public class InventoryImportServiceImpl extends AbstractImportService implements InventoryImportService {

    private static final Logger logger = LoggerFactory.getLogger(InventoryEditableServiceImpl.class);

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public byte[] getImportTemplate(UserLogin userLogin, String lang) {
        lang = getLang(lang);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(translate(lang, "inventory"));
        XSSFRow row = sheet.createRow(0);
        row.createCell(0).setCellValue(translate(lang, "product"));
        row.createCell(1).setCellValue(translate(lang, "product.code") + "*");
        row.createCell(2).setCellValue(translate(lang, "inventory.quantity") + "*");

        // Fetch product list
        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
        int index = 1;
        for (I_Product product : products) {
            row = sheet.createRow(index);
            row.createCell(0).setCellValue(product.getName());
            row.createCell(1).setCellValue(product.getCode());
            index++;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            workbook.write(baos);

            return baos.toByteArray();
        } catch (IOException e) {
            logger.error("error write file", e);
            throw new UnsupportedOperationException(e);
        } finally {
            IOUtils.closeQuietly(workbook);
        }
    }

    @Override
    public ImportConfirmDto verifyImport(UserLogin userLogin, String _time, String fileId) {

        SimpleDate time = getMandatoryIsoTime(_time);

        I_Distributor distributor = getDefaultDistributor(userLogin);

        // Get latest inventory record of distributor and compare date time
        I_Inventory latestInventoryRecord = inventoryRepository.getLatestInventory(userLogin.getClientId(),
                distributor.getId());
        if (latestInventoryRecord != null) {
            // Validate time
            BusinessAssert.isTrue(latestInventoryRecord.getTime().compareTo(time) < 0,
                    BusinessExceptionCode.INVENTORY_TIME_INVALID, "Inventory time invalid");
        }

        // Get all product from DB
        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);

        // Map product with code to lookup easily
        Map<String, I_Product> mapProductCode = new HashMap<>(products.size());
        for (I_Product product : products) {
            mapProductCode.put(product.getCode().trim().toUpperCase(), product);
        }

        // Validate excel file
        I_CellValidator[] validators = getValidators(userLogin, mapProductCode);
        ImportConfirmDto errorResult = getErrorRows(userLogin, fileId, validators);

        if (errorResult.getRowDatas().isEmpty()) {
            // Require to update available stock of all product
            BusinessAssert.isTrue(errorResult.getTotal() > 0 && errorResult.getTotal() == products.size(),
                    BusinessExceptionCode.INVENTORY_MISSING_PRODUCT, "Missing product");
        }

        return errorResult;
    }

    @Override
    public InventoryImportResultDto importInventory(UserLogin userLogin, String _time, String fileId) {
        BusinessAssert.notNull(_time);
        BusinessAssert.notNull(fileId);

        SimpleDate time = getMandatoryIsoTime(_time);

        I_Distributor distributor = getDefaultDistributor(userLogin);

        // Get latest inventory record of distributor and compare date time
        I_Inventory latestInventoryRecord = inventoryRepository.getLatestInventory(userLogin.getClientId(),
                distributor.getId());
        if (latestInventoryRecord != null) {
            // Validate time
            BusinessAssert.isTrue(latestInventoryRecord.getTime().compareTo(time) < 0,
                    BusinessExceptionCode.INVENTORY_TIME_INVALID, "Inventory time invalid");
        }

        // Get all product from DB
        List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);

        // Map product with code to lookup easily
        Map<String, I_Product> mapProductCode = new HashMap<>(products.size());
        for (I_Product product : products) {
            mapProductCode.put(product.getCode().trim().toUpperCase(), product);
        }

        // Validate excel file
        I_CellValidator[] validators = getValidators(userLogin, mapProductCode);
        ImportConfirmDto dto = getValidRows(userLogin, fileId, validators);

        BusinessAssert.equals(dto.getRowDatas().size(), dto.getTotal());

        // Create details from excel file
        List<I_ProductQuantity> details = new ArrayList<>(dto.getRowDatas().size());
        for (RowData row : dto.getRowDatas()) {
            String code = (String) row.getDatas().get(1);
            BigDecimal quantity = (BigDecimal) row.getDatas().get(2);

            ProductQuantity detail = new ProductQuantity(mapProductCode.get(code.trim().toUpperCase()));
            detail.setQuantity(quantity);

            details.add(detail);
        }

        // Require to update available stock of all product
        BusinessAssert.notEmpty(details, BusinessExceptionCode.INVENTORY_MISSING_PRODUCT, "Missing product");
        BusinessAssert.isTrue(details.size() == products.size(), BusinessExceptionCode.INVENTORY_MISSING_PRODUCT,
                "Missing product");

        Inventory _inventory = new Inventory();
        initiatePO(userLogin.getClientId(), _inventory);
        _inventory.setDraft(true);
        _inventory.setDetails(details);
        _inventory.setTime(time);
        _inventory.setDistributor(new CategoryEmbed(distributor));

        I_Inventory inventory = inventoryRepository.save(userLogin.getClientId(), _inventory);

        return new InventoryImportResultDto(inventory, dto.getTotal(), details.size());
    }

    private I_CellValidator[] getValidators(UserLogin userLogin, Map<String, I_Product> mapProductByCode) {
        // An empty set, only check unique with rows inside this file
        HashSet<String> productCode = new LinkedHashSet<>();

        I_CellValidator[] validators = new I_CellValidator[] { new NoValidator(),
                new MultiCellValidator(new StringMandatoryCellValidator(),
                        new ReferenceCellValidator<I_Product>(mapProductByCode),
                        new StringUniqueCellValidator(productCode)),
                new NumberMinMaxCellValidator(BigDecimal.ZERO, InventoryEditableService.MAX_QUANTITY_VALUE, 0) };
        return validators;
    }

}
