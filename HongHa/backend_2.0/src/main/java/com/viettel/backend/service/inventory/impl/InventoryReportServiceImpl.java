package com.viettel.backend.service.inventory.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.inventory.DistributorInventoryResultDto;
import com.viettel.backend.dto.inventory.InventoryListDto;
import com.viettel.backend.dto.inventory.InventoryReportByDistributorListDto;
import com.viettel.backend.dto.inventory.InventoryReportByProductListDto;
import com.viettel.backend.dto.inventory.InventoryResultDto;
import com.viettel.backend.dto.inventory.ProductInventoryResultDto;
import com.viettel.backend.dto.report.ReportDistributorListIdDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.cache.CacheVisitOrderService;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.inventory.InventoryReportService;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.InventoryRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.domain.user.I_User.Role;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN })
@RequireModule(Module.INVENTORY)
@Service
public class InventoryReportServiceImpl extends AbstractService implements InventoryReportService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private InventoryRepository inventoryRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private CacheVisitOrderService cacheVisitOrderService;

	@Override
	public InventoryReportByDistributorListDto getInventoryReportByDistributor(UserLogin userLogin, String regionId,
			String way, ReportDistributorListIdDto distributorIds) {
		String clientId = userLogin.getClientId();
		if ("all".equalsIgnoreCase(way)) {
			if (regionId == null) {
				return getInventoryReportByDistributors(userLogin, getAccessibleDistributors(userLogin));
			} else {
				return getInventoryReportByDistributors(userLogin, getDistributorsByRegion(userLogin, regionId));
			}
		} else if ("specify".equalsIgnoreCase(way)) {
			return getInventoryReportByDistributors(userLogin, distributorRepository
					.getDistributorByIds(userLogin.getClientId(), distributorIds.getDistributorIds()));
		}
		I_Distributor distributor = getMandatoryDistributor(userLogin, way);
		way = distributor.getId();

		I_Inventory inventory = inventoryRepository.getLatestInventory(clientId, way);

		// Current available map
		Map<String, Double> productSoldMap = cacheVisitOrderService
				.getQuantitySoldByProductFromLastInventoryUpdate(clientId, way);

		Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);
		if (CollectionUtils.isEmpty(productMap)) {
			return new InventoryReportByDistributorListDto(distributor, inventory,
					Collections.<ProductInventoryResultDto>emptyList());
		}

		// Put original quantity to map for access easily
		Map<String, BigDecimal> originalQuantityMap;
		if (inventory != null) {
			originalQuantityMap = new HashMap<>(inventory.getDetails().size());
			for (I_ProductQuantity productQuantity : inventory.getDetails()) {
				originalQuantityMap.put(productQuantity.getId(), productQuantity.getQuantity());
			}
		} else {
			originalQuantityMap = Collections.emptyMap();
		}

		List<ProductInventoryResultDto> dtos = new ArrayList<>(productMap.size());

		I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);

		// Does not have any inventory record - return null for all products
		if (inventory == null) {
			InventoryResultDto naInventoryResult = new InventoryResultDto(null, null, null);
			for (Map.Entry<String, I_Product> entry : productMap.entrySet()) {
				ProductInventoryResultDto dto = new ProductInventoryResultDto(entry.getValue(), productPhotoFactory,
						naInventoryResult);
				dtos.add(dto);
			}
		}
		// Already have inventory record
		else {
			for (Map.Entry<String, I_Product> entry : productMap.entrySet()) {
				String productId = entry.getKey();

				BigDecimal original = originalQuantityMap.get(productId);
				Double soldQuantity = productSoldMap != null ? productSoldMap.get(productId) : null;
				if (soldQuantity == null) {
					soldQuantity = 0.0d;
				}

				InventoryResultDto inventoryResult;
				inventoryResult = new InventoryResultDto(original, new BigDecimal(soldQuantity),
						entry.getValue().getPrice());

				ProductInventoryResultDto dto = new ProductInventoryResultDto(entry.getValue(), productPhotoFactory,
						inventoryResult);
				dtos.add(dto);
			}
		}

		return new InventoryReportByDistributorListDto(distributor, inventory, dtos);
	}

	private InventoryReportByDistributorListDto getInventoryReportByDistributors(UserLogin userLogin,
			List<I_Distributor> distributors) {
		Set<String> distributorIds = getIdSet(distributors);
		Map<String, I_Inventory> lastInventoryByccessibleDistributor = inventoryRepository
				.getLatestInventorys(userLogin.getClientId(), distributorIds);

		Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);
		if (CollectionUtils.isEmpty(productMap)) {
			return new InventoryReportByDistributorListDto(distributors, lastInventoryByccessibleDistributor,
					Collections.<ProductInventoryResultDto>emptyList());
		}
		// Get number of sold products
		Map<String, Double> productSoldMap = new HashMap<>();
		for (String distributorId : distributorIds) {
			Map<String, Double> productSoldMapOfDistributor = cacheVisitOrderService
					.getQuantitySoldByProductFromLastInventoryUpdate(userLogin.getClientId(), distributorId);
			for (String productId : productSoldMapOfDistributor.keySet()) {
				if (productSoldMap.containsKey(productId)) {
					double currentSold = productSoldMap.get(productId);
					productSoldMap.put(productId, currentSold + productSoldMapOfDistributor.get(productId));
				} else {
					productSoldMap.put(productId, productSoldMapOfDistributor.get(productId));
				}
			}
		}
		// Get original number of product of inventory
		Map<String, BigDecimal> productOriginalMap = new HashMap<>();// Collections.emptyMap();
		for (I_Inventory inventory : lastInventoryByccessibleDistributor.values()) {
			for (I_ProductQuantity productQuantity : inventory.getDetails()) {
				if (productOriginalMap.containsKey(productQuantity.getId())) {
					BigDecimal currentQuantity = productOriginalMap.get(productQuantity.getId());
					productOriginalMap.put(productQuantity.getId(), currentQuantity.add(productQuantity.getQuantity()));
				} else {
					productOriginalMap.put(productQuantity.getId(), productQuantity.getQuantity());
				}
			}
		}

		List<ProductInventoryResultDto> dtos = new ArrayList<>(productMap.size());
		I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);
		for (Map.Entry<String, I_Product> entry : productMap.entrySet()) {
			String productId = entry.getKey();

			BigDecimal original = productOriginalMap.get(productId);
			Double soldQuantity = productSoldMap != null ? productSoldMap.get(productId) : null;
			if (soldQuantity == null) {
				soldQuantity = 0.0d;
			}

			InventoryResultDto inventoryResult;
			inventoryResult = new InventoryResultDto(original, new BigDecimal(soldQuantity),
					entry.getValue().getPrice());

			ProductInventoryResultDto dto = new ProductInventoryResultDto(entry.getValue(), productPhotoFactory,
					inventoryResult);
			dtos.add(dto);
		}
		return new InventoryReportByDistributorListDto(distributors, lastInventoryByccessibleDistributor, dtos);
	}

	@Override
	public InventoryReportByProductListDto getInventoryReportByProduct(UserLogin userLogin, String productId) {
		if ("all".equalsIgnoreCase(productId)) {
			return getInventoryReportByClientProduct(userLogin);
		}
		String clientId = userLogin.getClientId();

		// Check if product id valid with current client
		Map<String, I_Product> mapProducts = productRepository.getProductMap(clientId, true);
		I_Product product = mapProducts.get(productId);
		BusinessAssert.isTrue(product != null);

		I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);

		// Get list of all distributor that accessible by current user
		List<I_Distributor> accessibleDistributors = getAccessibleDistributors(userLogin);
		if (CollectionUtils.isEmpty(accessibleDistributors)) {
			return new InventoryReportByProductListDto(product, productPhotoFactory,
					Collections.<DistributorInventoryResultDto>emptyList());
		}

		Set<String> accessibleDistributorIds = getIdSet(accessibleDistributors);

		// Fetch list of inventory records
		Map<String, I_Inventory> mapInventoryRecordByDistributors = inventoryRepository.getLatestInventorys(clientId,
				accessibleDistributorIds);

		List<DistributorInventoryResultDto> dtos = new ArrayList<>(accessibleDistributors.size());
		InventoryResultDto naInventoryResult = new InventoryResultDto(null, null, null);

		for (I_Distributor distributor : accessibleDistributors) {

			I_Inventory inventory = mapInventoryRecordByDistributors.get(distributor.getId());

			if (inventory == null) {
				DistributorInventoryResultDto dto = new DistributorInventoryResultDto(distributor, naInventoryResult);
				dtos.add(dto);
			} else {
				BigDecimal original = getOriginalAvailableQuantity(inventory, productId);

				Map<String, Double> productSoldMap = cacheVisitOrderService
						.getQuantitySoldByProductFromLastInventoryUpdate(clientId, distributor.getId());
				Double soldQuantity = productSoldMap != null ? productSoldMap.get(productId) : null;
				if (soldQuantity == null) {
					soldQuantity = 0.0d;
				}

				InventoryResultDto inventoryResult = new InventoryResultDto(original, new BigDecimal(soldQuantity),
						product.getPrice());

				DistributorInventoryResultDto dto = new DistributorInventoryResultDto(inventory, inventoryResult);
				dtos.add(dto);
			}
		}

		return new InventoryReportByProductListDto(product, productPhotoFactory, dtos);
	}

	private InventoryReportByProductListDto getInventoryReportByClientProduct(UserLogin userLogin) {
		I_ProductPhotoFactory productPhotoFactory = getProductPhotoFactory(userLogin);

		// Get list of all distributor that accessible by current user
		List<I_Distributor> accessibleDistributors = getAccessibleDistributors(userLogin);
		if (CollectionUtils.isEmpty(accessibleDistributors)) {
			return new InventoryReportByProductListDto(Collections.emptyList(), Collections.emptyList(),
					productPhotoFactory);
		}

		Set<String> accessibleDistributorIds = getIdSet(accessibleDistributors);

		// Fetch list of inventory records
		Map<String, I_Inventory> lastInventoryByccessibleDistributor = inventoryRepository
				.getLatestInventorys(userLogin.getClientId(), accessibleDistributorIds);
		List<InventoryListDto> lastInventories = new ArrayList<>();
		for (I_Category distributor : accessibleDistributors) {
			if (lastInventoryByccessibleDistributor.containsKey(distributor.getId())) {
				lastInventories.add(new InventoryListDto(lastInventoryByccessibleDistributor.get(distributor.getId())));
			} else {
				lastInventories.add(new InventoryListDto(distributor));
			}
		}

		Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), true);
		if (CollectionUtils.isEmpty(productMap)) {
			return new InventoryReportByProductListDto(lastInventories, Collections.emptyList(), productPhotoFactory);
		}
		// Get number of sold products
		Map<String, Double> productSoldMap = new HashMap<>();// Collections.emptyMap();
		for (String distributorId : accessibleDistributorIds) {
			Map<String, Double> productSoldMapOfDistributor = cacheVisitOrderService
					.getQuantitySoldByProductFromLastInventoryUpdate(userLogin.getClientId(), distributorId);
			for (String productId : productSoldMapOfDistributor.keySet()) {
				if (productSoldMap.containsKey(productId)) {
					double currentSold = productSoldMap.get(productId);
					productSoldMap.put(productId, currentSold + productSoldMapOfDistributor.get(productId));
				} else {
					productSoldMap.put(productId, productSoldMapOfDistributor.get(productId));
				}
			}
		}
		// Get original number of product of inventory
		Map<String, BigDecimal> productOriginalMap = new HashMap<>();// Collections.emptyMap();
		for (I_Inventory inventory : lastInventoryByccessibleDistributor.values()) {
			for (I_ProductQuantity productQuantity : inventory.getDetails()) {
				if (productOriginalMap.containsKey(productQuantity.getId())) {
					BigDecimal currentQuantity = productOriginalMap.get(productQuantity.getId());
					productOriginalMap.put(productQuantity.getId(), currentQuantity.add(productQuantity.getQuantity()));
				} else {
					productOriginalMap.put(productQuantity.getId(), productQuantity.getQuantity());
				}
			}
		}
		List<ProductInventoryResultDto> products = new ArrayList<>();
		for (I_Product p : productMap.values()) {
			BigDecimal inventoryQuantity = productOriginalMap.get(p.getId());
			BigDecimal soldQuantity = productSoldMap.get(p.getId()) != null
					? BigDecimal.valueOf(productSoldMap.get(p.getId())) : BigDecimal.ZERO;
			InventoryResultDto inventoryResult = new InventoryResultDto(inventoryQuantity, soldQuantity, p.getPrice());
			ProductInventoryResultDto product = new ProductInventoryResultDto(p, productPhotoFactory, inventoryResult);
			products.add(product);
		}

		return new InventoryReportByProductListDto(lastInventories, products, productPhotoFactory);

	}

	private BigDecimal getOriginalAvailableQuantity(I_Inventory inventory, String productId) {
		for (I_ProductQuantity productQuantity : inventory.getDetails()) {
			if (productQuantity.getId().equals(productId)) {
				return productQuantity.getQuantity();
			}
		}
		return null;
	}

}
