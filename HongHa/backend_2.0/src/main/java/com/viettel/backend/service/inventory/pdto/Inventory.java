package com.viettel.backend.service.inventory.pdto;

import java.util.List;

import com.viettel.backend.service.core.pdto.PO;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.entity.SimpleDate;

public class Inventory extends PO implements I_Inventory {

    private static final long serialVersionUID = -2837925355099893909L;
    
    public SimpleDate time;
    public I_CategoryEmbed distributor;
    public List<I_ProductQuantity> details;
    
    public Inventory() {
        super();
    }
    
    public Inventory(I_Inventory inventory) {
        super(inventory);
        
        this.time = inventory.getTime();
        this.distributor = inventory.getDistributor();
        this.details = inventory.getDetails();
    }

    public SimpleDate getTime() {
        return time;
    }

    public void setTime(SimpleDate time) {
        this.time = time;
    }

    public I_CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(I_CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public List<I_ProductQuantity> getDetails() {
        return details;
    }

    public void setDetails(List<I_ProductQuantity> details) {
        this.details = details;
    }

}
