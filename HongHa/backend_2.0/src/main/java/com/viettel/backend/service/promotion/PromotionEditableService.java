package com.viettel.backend.service.promotion;

import com.viettel.backend.dto.promotion.PromotionCreateDto;
import com.viettel.backend.dto.promotion.PromotionDto;
import com.viettel.backend.dto.promotion.PromotionListDto;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface PromotionEditableService extends
        CategoryEditableService<PromotionListDto, PromotionDto, PromotionCreateDto> {

}
