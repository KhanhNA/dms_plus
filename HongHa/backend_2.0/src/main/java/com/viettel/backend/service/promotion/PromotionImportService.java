package com.viettel.backend.service.promotion;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface PromotionImportService {

	public byte[] getImportPromotionTemplate(UserLogin userLogin, String lang);

	public ImportConfirmDto verify(UserLogin userLogin, String fileId);

	public ImportResultDto importPromotion(UserLogin userLogin, String fileId);

}
