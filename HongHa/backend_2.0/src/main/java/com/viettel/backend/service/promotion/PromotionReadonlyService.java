package com.viettel.backend.service.promotion;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.promotion.PromotionListDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface PromotionReadonlyService {

    public ListDto<PromotionListDto> getPromotionsAvailableByCustomer(UserLogin userLogin, String _customerId);

}
