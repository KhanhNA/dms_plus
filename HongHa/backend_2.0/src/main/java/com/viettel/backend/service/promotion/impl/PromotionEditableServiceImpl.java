package com.viettel.backend.service.promotion.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.I_ProductPhotoFactory;
import com.viettel.backend.dto.promotion.PromotionCreateDto;
import com.viettel.backend.dto.promotion.PromotionCreateDto.PromotionDetailCreateDto;
import com.viettel.backend.dto.promotion.PromotionDto;
import com.viettel.backend.dto.promotion.PromotionListDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.CategoryComplexEditableServiceImpl;
import com.viettel.backend.service.core.sub.PromotionCalculationService;
import com.viettel.backend.service.promotion.PromotionEditableService;
import com.viettel.backend.service.promotion.pdto.Promotion;
import com.viettel.backend.service.promotion.pdto.PromotionCondition;
import com.viettel.backend.service.promotion.pdto.PromotionDetail;
import com.viettel.backend.service.promotion.pdto.PromotionReward;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.CategoryComplexBasicRepository;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.PromotionRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.promotion.I_Promotion;
import com.viettel.repository.common.domain.promotion.I_PromotionDetail;
import com.viettel.repository.common.domain.promotion.I_PromotionHeader;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;

@RolePermission(value = { Role.ADMIN })
@RequireModule(Module.PROMOTION)
@Service
public class PromotionEditableServiceImpl extends
		CategoryComplexEditableServiceImpl<I_PromotionHeader, I_Promotion, I_Promotion, PromotionListDto, PromotionDto, PromotionCreateDto>
		implements PromotionEditableService {

	@Autowired
	private PromotionRepository promotionRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Override
	protected CategoryComplexBasicRepository<I_PromotionHeader, I_Promotion, I_Promotion> getRepository() {
		return promotionRepository;
	}

	@Override
	protected void beforeSetActive(UserLogin userLogin, I_Promotion domain, boolean active) {
		throw new UnsupportedOperationException();
	}

	@Override
	public PromotionListDto createListSimpleDto(UserLogin userLogin, I_PromotionHeader domain) {
		return new PromotionListDto(domain);
	}

	@Override
	public PromotionDto createListDetailDto(final UserLogin userLogin, I_Promotion domain) {
		final Map<String, I_Product> productMap = productRepository.getProductMap(userLogin.getClientId(), null);
		final Map<String, I_Distributor> distributorMap = domain.isForAllDistributor() ? null
				: getPOMap(distributorRepository.getAll(userLogin.getClientId(), null));
		PromotionDto dto = new PromotionDto(domain, productMap, new I_ProductPhotoFactory() {

			@Override
			public String getPhoto(String productId) {
				I_Product product = productMap.get(productId);
				if (product == null || product.getPhoto() == null) {
					return getConfig(userLogin).getProductPhoto();
				}
				return product.getPhoto();
			}

		}, distributorMap);

		return dto;
	}

	@Override
	public I_Promotion getObjectForCreate(UserLogin userLogin, PromotionCreateDto createdto) {
		BusinessAssert.notNull(createdto);

		Promotion promotion = new Promotion();
		initiatePO(userLogin.getClientId(), promotion);
		promotion.setDraft(true);

		return getObjectForUpdate(userLogin, promotion, createdto);
	}

	@Override
	public I_Promotion getObjectForUpdate(UserLogin userLogin, I_Promotion oldDomain, PromotionCreateDto createdto) {
		BusinessAssert.notNull(createdto);
		Promotion promotion = new Promotion(oldDomain);
		SimpleDate startDate = getMandatoryIsoDate(createdto.getStartDate());
		SimpleDate endDate = getMandatoryIsoDate(createdto.getEndDate());
		BusinessAssert.isTrue(startDate.compareTo(endDate) <= 0, "startDate > endDate");
		if (promotion.isDraft()) {

			BusinessAssert.notNull(createdto.getDescription());
			BusinessAssert.notNull(createdto.getApplyFor());
			BusinessAssert.notNull(createdto.getStartDate());
			BusinessAssert.notNull(createdto.getEndDate());
			BusinessAssert.notNull(createdto.getDetails());
			BusinessAssert.notEmpty(createdto.getDetails());

			promotion = updateCategoryFieldFromDto(userLogin, promotion, createdto);

			promotion.setStartDate(startDate);
			promotion.setDescription(createdto.getDescription());
			promotion.setApplyFor(createdto.getApplyFor());

			List<I_PromotionDetail> details = new ArrayList<I_PromotionDetail>(createdto.getDetails().size());
			int seqNo = 1;
			for (PromotionDetailCreateDto detailDto : createdto.getDetails()) {
				BusinessAssert.notNull(detailDto, "detailt promotion null");
				BusinessAssert.notNull(detailDto.getCondition());
				BusinessAssert.notNull(detailDto.getCondition().getProductId());
				BusinessAssert.notNull(detailDto.getCondition().getQuantity());
				BusinessAssert.notNull(detailDto.getReward());

				boolean rewardValid = false;
				if (detailDto.getReward().getPercentage() != null) {
					rewardValid = true;
				}
				if (!(detailDto.getReward().getProductId() == null
						&& StringUtils.isNullOrEmpty(detailDto.getReward().getProductText(), true))
						|| detailDto.getReward().getQuantity() == null) {
					rewardValid = true;
				}
				BusinessAssert.isTrue(rewardValid);
				BusinessAssert
						.isTrue(PromotionCalculationService.PromotionDetailType.TYPES.contains(detailDto.getType()));

				I_Product conditionProduct = getMandatoryPO(userLogin, detailDto.getCondition().getProductId(),
						productRepository);

				PromotionReward reward = new PromotionReward();
				reward.setPercentage(detailDto.getReward().getPercentage());
				reward.setQuantity(detailDto.getReward().getQuantity());
				if (detailDto.getReward().getProductId() != null) {
					I_Product rewardProduct = getMandatoryPO(userLogin, detailDto.getReward().getProductId(),
							productRepository);
					reward.setProductId(rewardProduct.getId());
				} else {
					reward.setProductText(detailDto.getReward().getProductText());
				}

				PromotionCondition promotionCondition = new PromotionCondition();
				promotionCondition.setProductId(conditionProduct.getId());
				promotionCondition.setQuantity(detailDto.getCondition().getQuantity());

				PromotionDetail detail = new PromotionDetail();
				detail.setSeqNo(seqNo++);
				detail.setType(detailDto.getType());
				detail.setCondition(promotionCondition);
				detail.setReward(reward);

				details.add(detail);
			}
			promotion.setDetails(details);
		}
		promotion.setEndDate(endDate);
		promotion.setForAllDistributor(createdto.isForAllDistributor());
		if (!createdto.isForAllDistributor()) {
			Set<String> distributorIds = createdto.getDistributorIds();
			if (distributorIds != null) {
				BusinessAssert
						.isTrue(distributorRepository.exists(userLogin.getClientId(), false, true, distributorIds));
			}
			promotion.setDistributorIds(distributorIds);
		}

		return promotion;
	}

}
