package com.viettel.backend.service.promotion.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ImportConfirmDto;
import com.viettel.backend.dto.common.ImportConfirmDto.RowData;
import com.viettel.backend.dto.common.ImportResultDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractImportService;
import com.viettel.backend.service.core.sub.PromotionCalculationService.PromotionDetailType;
import com.viettel.backend.service.promotion.PromotionImportService;
import com.viettel.backend.service.promotion.pdto.Promotion;
import com.viettel.backend.service.promotion.pdto.PromotionCondition;
import com.viettel.backend.service.promotion.pdto.PromotionDetail;
import com.viettel.backend.service.promotion.pdto.PromotionReward;
import com.viettel.backend.util.StringUtils;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.PromotionRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.product.I_Product;
import com.viettel.repository.common.domain.promotion.I_Promotion;
import com.viettel.repository.common.domain.promotion.I_PromotionDetail;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

@RolePermission(value = { Role.ADMIN })
@Service
public class PromotionImportServiceImpl extends AbstractImportService implements PromotionImportService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	PromotionRepository promotionRepository;

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private ProductRepository productRepository;

	@Override
	public byte[] getImportPromotionTemplate(UserLogin userLogin, String lang) {
		lang = getLang(lang);

		XSSFWorkbook workbook = new XSSFWorkbook();
		CellStyle style = null;
		DataFormat format = null;

		format = workbook.createDataFormat();
		style = workbook.createCellStyle();
		style.setDataFormat(format.getFormat("@"));

		// Sheet to fill info
		String[] headers = new String[] { "name", "start.date", "end.date", "apply.for", "description", "distributor",
				"detail.promotion.percent", "", "", "", "detail.promotion.gift", "", "", "", "" };
		String[] subHeaders = new String[] { "", "", "", "", "", "", "X", "product", "up.to.quantity",
				"percent.discount", "X", "product.buy", "up.to.quantity", "product.gift", "quantity.gift" };
		XSSFSheet promotionSheet = workbook.createSheet(translate(lang, "promotion.amt"));
		XSSFRow row = promotionSheet.createRow(0);
		int i;
		for (i = 0; i < headers.length; i++) {
			row.createCell(i)
					.setCellValue(translate(lang, headers[i]) + (StringUtils.isNullOrEmpty(subHeaders[i]) ? "*" : ""));
		}
		row = promotionSheet.createRow(1);
		for (i = 0; i < subHeaders.length; i++) {
			row.createCell(i).setCellValue(translate(lang, subHeaders[i]));
		}
		for (i = 0; i < headers.length; i++) {
			promotionSheet.autoSizeColumn(i);
		}
		promotionSheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
		promotionSheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
		promotionSheet.addMergedRegion(new CellRangeAddress(0, 1, 2, 2));
		promotionSheet.addMergedRegion(new CellRangeAddress(0, 1, 3, 3));
		promotionSheet.addMergedRegion(new CellRangeAddress(0, 1, 4, 4));
		promotionSheet.addMergedRegion(new CellRangeAddress(0, 1, 5, 5));
		promotionSheet.addMergedRegion(new CellRangeAddress(0, 0, 6, 9));
		promotionSheet.addMergedRegion(new CellRangeAddress(0, 0, 10, 14));

		promotionSheet.setDefaultColumnStyle(1, style);
		promotionSheet.setDefaultColumnStyle(2, style);

		// Sheet Distributor
		List<I_Distributor> distributors = distributorRepository.getAll(userLogin.getClientId(), null);
		int index = 0;
		XSSFSheet distributorSheet = workbook.createSheet(translate(lang, "distributor"));
		row = distributorSheet.createRow(index++);
		row.createCell(0).setCellValue(translate(lang, "name"));
		row.createCell(1).setCellValue(translate(lang, "code"));
		row = distributorSheet.createRow(index++);
		row.createCell(0).setCellValue("ALL");
		row.createCell(1).setCellValue("ALL");
		for (I_Distributor distributor : distributors) {
			row = distributorSheet.createRow(index++);
			row.createCell(0).setCellValue(distributor.getName());
			row.createCell(1).setCellValue(distributor.getCode());
		}
		distributorSheet.autoSizeColumn(0);
		distributorSheet.autoSizeColumn(1);

		// Sheet Product
		List<I_Product> products = productRepository.getAll(userLogin.getClientId(), null);
		index = 0;
		XSSFSheet productSheet = workbook.createSheet(translate(lang, "product"));
		row = productSheet.createRow(index++);
		row.createCell(0).setCellValue(translate(lang, "name"));
		row.createCell(1).setCellValue(translate(lang, "code"));
		for (I_Product product : products) {
			row = productSheet.createRow(index++);
			row.createCell(0).setCellValue(product.getName());
			row.createCell(1).setCellValue(product.getCode());
		}
		productSheet.autoSizeColumn(0);
		productSheet.autoSizeColumn(1);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			workbook.write(baos);

			return baos.toByteArray();
		} catch (IOException e) {
			logger.error("error write file", e);
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
		}
	}

	@Override
	public ImportConfirmDto verify(UserLogin userLogin, String fileId) {
		this.setLamda(1);// Xét thêm để tăng 1 dòng so với hàm gốc ban đầu để
		return getErrorRows(userLogin, fileId, getCellValidators(userLogin.getClientId()));
	}

	@Override
	public ImportResultDto importPromotion(UserLogin userLogin, String fileId) {
		this.setLamda(1);
		ImportConfirmDto dto = getValidRows(userLogin, fileId, getCellValidators(userLogin.getClientId()));
		Map<String, List<RowData>> maps = new HashMap<>();
		List<String> listPromotionName = new ArrayList<>();
		for (RowData rowData : dto.getRowDatas()) {
			String key = rowData.getDataTexts().get(0).toString() + rowData.getDataTexts().get(1).toString()
					+ rowData.getDataTexts().get(2).toString();
			// + rowData.getDataTexts().get(3).toString() +
			// rowData.getDataTexts().get(4).toString() +
			// rowData.getDataTexts().get(5).toString();
			if (maps.isEmpty() || !maps.containsKey(key)) {
				maps.put(key, new ArrayList<RowData>());
			}
			if (!listPromotionName.contains(rowData.getDataTexts().get(0).toString())) {
				listPromotionName.add(rowData.getDataTexts().get(0).toString());
			}
			maps.get(key).add(rowData);
		}
		if (maps.keySet().size() != listPromotionName.size()) {
			throw new BusinessException("import.promotion.name.duplicated",
					"List promotion in excel has duplicated name");
		}

		List<I_Promotion> promotionToInsert = new ArrayList<I_Promotion>(maps.size());
		List<Map<String, I_Category>> listProduct = getListMapProduct(userLogin.getClientId());
		List<Map<String, I_Category>> listDistributor = getListMapDistributor(userLogin.getClientId());

		for (String key : maps.keySet()) {
			RowData firstRowData = maps.get(key).get(0);
			String nameOfPromotion = firstRowData.getDataTexts().get(0);
			BusinessAssert.notTrue(
					promotionRepository.checkNameExist(userLogin.getClientId(), null, nameOfPromotion, null),
					BusinessExceptionCode.NAME_USED,
					String.format("Record with [name=%s] already exist", nameOfPromotion));

			Promotion promotion = new Promotion();
			initiatePO(userLogin.getClientId(), promotion);
			promotion.setDraft(true);
			promotion.setName(firstRowData.getDataTexts().get(0));
			promotion.setStartDate(new SimpleDate(DateTimeUtils.getDate(firstRowData.getDataTexts().get(1))));
			promotion.setEndDate(new SimpleDate(DateTimeUtils.getDate(firstRowData.getDataTexts().get(2))));
			promotion.setApplyFor(firstRowData.getDataTexts().get(3));
			promotion.setDescription(firstRowData.getDataTexts().get(4));
			if ("ALL".equals(firstRowData.getDataTexts().get(5).toUpperCase())) {
				promotion.setForAllDistributor(true);
			} else {
				promotion.setForAllDistributor(false);
				String[] distributors = firstRowData.getDataTexts().get(5).split(",");
				Set<String> distributorSet = new HashSet<>();

				for (String distributor : distributors) {
					I_Category dis = getICategory(listDistributor, distributor.trim());
					distributorSet.add(dis.getId());
				}
				promotion.setDistributorIds(distributorSet);
			}
			List<I_PromotionDetail> listPromotionDetails = new ArrayList<>();
			int seqNo = 1;
			for (RowData rowData : maps.get(key)) {
				try {
					PromotionDetail promotionDetail;
					PromotionCondition promotionCondition;
					PromotionReward promotionReward;
					if (!StringUtils.isNullOrEmpty(rowData.getDataTexts().get(6))) {
						promotionDetail = new PromotionDetail();
						promotionDetail.setSeqNo(seqNo++);
						promotionDetail.setType(PromotionDetailType.C_PRODUCT_QTY_R_PERCENTAGE_AMT);

						promotionCondition = new PromotionCondition();
						I_Category product = getICategory(listProduct, rowData.getDataTexts().get(7));
						if (product != null) {
							promotionCondition.setProductId(product.getId());
						}
						promotionCondition.setQuantity(new BigDecimal(rowData.getDataTexts().get(8)));
						promotionDetail.setCondition(promotionCondition);

						promotionReward = new PromotionReward();
						promotionReward.setPercentage(new BigDecimal(rowData.getDataTexts().get(9)));
						promotionDetail.setReward(promotionReward);

						listPromotionDetails.add(promotionDetail);
					}
					if (!StringUtils.isNullOrEmpty(rowData.getDataTexts().get(10))) {
						promotionDetail = new PromotionDetail();
						promotionDetail.setSeqNo(seqNo++);
						promotionDetail.setType(PromotionDetailType.C_PRODUCT_QTY_R_PRODUCT_QTY);

						promotionCondition = new PromotionCondition();
						I_Category product = getICategory(listProduct, rowData.getDataTexts().get(11));
						if (product != null) {
							promotionCondition.setProductId(product.getId());
						}
						promotionCondition.setQuantity(new BigDecimal(rowData.getDataTexts().get(12)));
						promotionDetail.setCondition(promotionCondition);

						promotionReward = new PromotionReward();
						product = getICategory(listProduct, rowData.getDataTexts().get(13));
						promotionReward.setProductId(product.getId());
						promotionReward.setQuantity(new BigDecimal(rowData.getDataTexts().get(14)));
						promotionDetail.setReward(promotionReward);

						listPromotionDetails.add(promotionDetail);
					}
				} catch (Exception e) {
					logger.error("Exception", e);
				}
			}
			promotion.setDetails(listPromotionDetails);
			promotionToInsert.add(promotion);
		}
		if (!promotionToInsert.isEmpty()) {
			promotionRepository.insertBatch(userLogin.getClientId(), promotionToInsert);
		}
		// return new ImportResultDto(dto.getTotal(), promotionToInsert.size());
		return new ImportResultDto(promotionToInsert.size(), promotionToInsert.size());
	}

	private I_Category getICategory(List<Map<String, I_Category>> list, String value) {
		I_Category iCategory = null;
		for (Map<String, I_Category> map : list) {
			iCategory = map.get(value.toUpperCase());
			if (iCategory != null) {
				break;
			}
		}
		return iCategory;
	}

	private I_CellValidator[] getCellValidators(String clientId) {

		I_CellValidator checkFromToDateCellValidator = new I_CellValidator() {

			@Override
			public CheckResult check(XSSFCell cell, RowData rowData, int index) {
				try {
					XSSFCell fromDateCell = cell.getRow().getCell(1);
					String fromDateString = getStringCellValue(fromDateCell);
					Date fromDate = DateTimeUtils.getDate(fromDateString);
					XSSFCell toDateCell = cell.getRow().getCell(2);
					String toDateString = getStringCellValue(toDateCell);
					Date toDate = DateTimeUtils.getDate(toDateString);

					return new CheckResult(toDateString, toDateString, fromDate.compareTo(toDate) <= 0);
				} catch (Exception e) {
					logger.error("Exception",e);
					return new CheckResult(false);
				}
			}
		};

		I_CellValidator checkPromotionCellValidator = new I_CellValidator() {

			@Override
			public CheckResult check(XSSFCell cell, RowData rowData, int index) {
				try {
					XSSFCell tickPercentCell = cell.getRow().getCell(6);
					String tickPercentCellvalue = getStringCellValue(tickPercentCell);
					XSSFCell tickGiftCell = cell.getRow().getCell(10);
					String tickGiftCellValue = getStringCellValue(tickGiftCell);
					if (StringUtils.isNullOrEmpty(tickPercentCellvalue)
							&& StringUtils.isNullOrEmpty(tickGiftCellValue)) {
						return new CheckResult(false);
					}
					if (index == 6 && !StringUtils.isNullOrEmpty(tickPercentCellvalue)) {
						return new CheckResult(tickPercentCellvalue, tickPercentCellvalue, true);
					}
					if (index == 10 && !StringUtils.isNullOrEmpty(tickGiftCellValue)) {
						return new CheckResult(tickGiftCellValue, tickGiftCellValue, true);
					}
					return new CheckResult(true);
				} catch (Exception e) {
					logger.error("Exception",e);
					return new CheckResult(false);
				}
			}
		};

		I_CellValidator checkPromotionPercentCellValidator = new I_CellValidator() {

			@Override
			public CheckResult check(XSSFCell cell, RowData rowData, int index) {
				try {
					String tickPercentCellvalue = getStringCellValue(cell);
					if (!StringUtils.isNullOrEmpty(tickPercentCellvalue)) {
						XSSFCell productCell = cell.getRow().getCell(7);
						XSSFCell quantityCell = cell.getRow().getCell(8);
						XSSFCell discountCell = cell.getRow().getCell(9);
						MultiCellValidator multiCellValidator = new MultiCellValidator(
								new StringMandatoryCellValidator(),
								new MultiReferenceCellValidation<I_Category>(getListMapProduct(clientId)));
						CheckResult result = multiCellValidator.check(productCell, rowData, index);
						if (result.isValid()) {
							multiCellValidator = new MultiCellValidator(new StringMandatoryCellValidator(),
									new NumberMandatoryCellValidator(0));
							result = multiCellValidator.check(quantityCell, rowData, index);
							if (result.isValid()) {
								multiCellValidator = new MultiCellValidator(new StringMandatoryCellValidator(),
										new NumberMinMaxCellValidator(new BigDecimal(0), new BigDecimal(100), 0));
								result = multiCellValidator.check(discountCell, rowData, index);
								if (result.isValid()) {
									return new CheckResult(tickPercentCellvalue, tickPercentCellvalue, true);
								} else {
									return new CheckResult(tickPercentCellvalue, tickPercentCellvalue, false);
								}
							} else {
								return new CheckResult(tickPercentCellvalue, tickPercentCellvalue, false);
							}
						} else {
							return new CheckResult(tickPercentCellvalue, tickPercentCellvalue, false);
						}
					} else {
						return new CheckResult(true);
					}
				} catch (Exception e) {
					logger.error("Exception",e);
					return new CheckResult(false);
				}
			}
		};

		I_CellValidator checkPromotionGiftCellValidator = new I_CellValidator() {

			@Override
			public CheckResult check(XSSFCell cell, RowData rowData, int index) {
				try {
					String tickGiftCellValue = getStringCellValue(cell);
					if (!StringUtils.isNullOrEmpty(tickGiftCellValue)) {
						XSSFCell productBuyCell = cell.getRow().getCell(11);
						XSSFCell quantityBuyCell = cell.getRow().getCell(12);
						XSSFCell productGiftCell = cell.getRow().getCell(13);
						XSSFCell quantityGiftCell = cell.getRow().getCell(14);
						MultiCellValidator multiCellValidator = new MultiCellValidator(
								new StringMandatoryCellValidator(),
								new MultiReferenceCellValidation<I_Category>(getListMapProduct(clientId)));
						CheckResult result = multiCellValidator.check(productBuyCell, rowData, index);
						if (result.isValid()) {
							multiCellValidator = new MultiCellValidator(new StringMandatoryCellValidator(),
									new NumberMandatoryCellValidator(0));
							result = multiCellValidator.check(quantityBuyCell, rowData, index);
							if (result.isValid()) {
								multiCellValidator = new MultiCellValidator(new StringMandatoryCellValidator(),
										new MultiReferenceCellValidation<I_Category>(getListMapProduct(clientId)));
								result = multiCellValidator.check(productGiftCell, rowData, index);
								if (result.isValid()) {
									multiCellValidator = new MultiCellValidator(new StringMandatoryCellValidator(),
											new NumberMandatoryCellValidator(0));
									result = multiCellValidator.check(quantityGiftCell, rowData, index);
									if (result.isValid()) {
										return new CheckResult(tickGiftCellValue, tickGiftCellValue, true);
									} else {
										return new CheckResult(tickGiftCellValue, tickGiftCellValue, false);
									}
								} else {
									return new CheckResult(tickGiftCellValue, tickGiftCellValue, false);
								}
							} else {
								return new CheckResult(tickGiftCellValue, tickGiftCellValue, false);
							}
						} else {
							return new CheckResult(tickGiftCellValue, tickGiftCellValue, false);
						}
					} else {
						return new CheckResult(true);
					}
				} catch (Exception e) {
					logger.error("Exception",e);
					return new CheckResult(false);
				}
			}
		};

		return new I_CellValidator[] { new StringMandatoryCellValidator(),
				new MultiCellValidator(new StringMandatoryCellValidator(), new DateCellValidator()),
				new MultiCellValidator(new StringMandatoryCellValidator(), new DateCellValidator(),
						checkFromToDateCellValidator),
				new StringMandatoryCellValidator(), new StringMandatoryCellValidator(),
				new MultiCellValidator(new StringMandatoryCellValidator(),
						new MultiReferenceCellValidation<I_Category>(getListMapDistributor(clientId))),
				new MultiCellValidator(checkPromotionCellValidator, checkPromotionPercentCellValidator),
				new NoValidator(), new NoValidator(), new NoValidator(),
				new MultiCellValidator(checkPromotionCellValidator, checkPromotionGiftCellValidator), new NoValidator(),
				new NoValidator(), new NoValidator(), new NoValidator() };
	}

	private List<Map<String, I_Category>> getListMapProduct(String clientId) {
		List<I_Product> products = productRepository.getAll(clientId, null);
		HashMap<String, I_Category> productNameMap = new HashMap<String, I_Category>();
		HashMap<String, I_Category> productCodeMap = new HashMap<String, I_Category>();
		if (products != null) {
			for (I_Category product : products) {
				productNameMap.put(product.getName().toUpperCase(), product);
				productCodeMap.put(product.getCode().toUpperCase(), product);
			}
		}
		List<Map<String, I_Category>> listProduct = new ArrayList<>();
		listProduct.add(productNameMap);
		listProduct.add(productCodeMap);

		return listProduct;
	}

	private List<Map<String, I_Category>> getListMapDistributor(String clientId) {
		List<I_Distributor> distributors = distributorRepository.getAll(clientId, null);
		HashMap<String, I_Category> distributorNameMap = new HashMap<String, I_Category>();
		HashMap<String, I_Category> distributorCodeMap = new HashMap<String, I_Category>();
		if (distributors != null) {
			for (I_Category distributor : distributors) {
				distributorNameMap.put(distributor.getName().toUpperCase(), distributor);
				distributorCodeMap.put(distributor.getCode().toUpperCase(), distributor);
			}
		}
		List<Map<String, I_Category>> listDistributor = new ArrayList<>();
		listDistributor.add(distributorNameMap);
		listDistributor.add(distributorCodeMap);
		return listDistributor;
	}
}
