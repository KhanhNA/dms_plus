package com.viettel.backend.service.promotion.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.promotion.PromotionListDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.promotion.PromotionReadonlyService;
import com.viettel.repository.common.PromotionRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.promotion.I_PromotionHeader;

@Service
@RequireModule(Module.PROMOTION)
public class PromotionReadonlyServiceImpl extends AbstractService implements PromotionReadonlyService {

    @Autowired
    private PromotionRepository promotionRepository;

    @Override
    public ListDto<PromotionListDto> getPromotionsAvailableByCustomer(UserLogin userLogin, String _customerId) {
        Set<String> distributorIds = getIdSet(getAccessibleDistributors(userLogin));

        List<I_PromotionHeader> promotions = promotionRepository
                .getPromotionHeadersAvailableToday(userLogin.getClientId(), distributorIds);
        List<PromotionListDto> dtos = new ArrayList<PromotionListDto>(promotions.size());
        for (I_PromotionHeader promotion : promotions) {
            dtos.add(new PromotionListDto(promotion));
        }

        return new ListDto<PromotionListDto>(dtos);
    }

}
