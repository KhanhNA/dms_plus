package com.viettel.backend.service.promotion.pdto;

import java.util.List;
import java.util.Set;

import com.viettel.backend.service.core.pdto.Category;
import com.viettel.repository.common.domain.promotion.I_Promotion;
import com.viettel.repository.common.domain.promotion.I_PromotionDetail;
import com.viettel.repository.common.entity.SimpleDate;

public class Promotion extends Category implements I_Promotion {

    private static final long serialVersionUID = -7211708735557057276L;

    private SimpleDate startDate;
    private SimpleDate endDate;
    private String description;
    private String applyFor;
    private List<I_PromotionDetail> details;
    private boolean forAllDistributor;
    private Set<String> distributorIds;

    public Promotion() {
        super();
    }

    public Promotion(I_Promotion promotion) {
        super(promotion);

        this.startDate = promotion.getStartDate();
        this.endDate = promotion.getEndDate();
        this.description = promotion.getDescription();
        this.applyFor = promotion.getApplyFor();
        this.details = promotion.getDetails();
        this.forAllDistributor = promotion.isForAllDistributor();
        this.distributorIds = promotion.getDistributorIds();
    }

    public SimpleDate getStartDate() {
        return startDate;
    }

    public void setStartDate(SimpleDate startDate) {
        this.startDate = startDate;
    }

    public SimpleDate getEndDate() {
        return endDate;
    }

    public void setEndDate(SimpleDate endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getApplyFor() {
        return applyFor;
    }

    public void setApplyFor(String applyFor) {
        this.applyFor = applyFor;
    }

    public List<I_PromotionDetail> getDetails() {
        return details;
    }

    public void setDetails(List<I_PromotionDetail> details) {
        this.details = details;
    }

    public boolean isForAllDistributor() {
        return forAllDistributor;
    }

    public void setForAllDistributor(boolean forAllDistributor) {
        this.forAllDistributor = forAllDistributor;
    }

    public Set<String> getDistributorIds() {
        return this.distributorIds;
    }

    public void setDistributorIds(Set<String> distributorIds) {
        this.distributorIds = distributorIds;
    }

}
