package com.viettel.backend.service.promotion.pdto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.viettel.repository.common.domain.promotion.I_PromotionCondition;

public class PromotionCondition implements Serializable, I_PromotionCondition {

    private static final long serialVersionUID = -2264520138560472088L;
    
    private String productId;
    private BigDecimal quantity;
    
    public PromotionCondition() {
        super();
    }
    
    public PromotionCondition(I_PromotionCondition promotionCondition) {
        super();
        
        this.productId = promotionCondition.getProductId();
        this.quantity = promotionCondition.getQuantity();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

}
