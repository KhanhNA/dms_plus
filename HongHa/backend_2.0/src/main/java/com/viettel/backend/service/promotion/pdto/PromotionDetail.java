package com.viettel.backend.service.promotion.pdto;

import com.viettel.repository.common.domain.promotion.I_PromotionCondition;
import com.viettel.repository.common.domain.promotion.I_PromotionDetail;
import com.viettel.repository.common.domain.promotion.I_PromotionReward;

public class PromotionDetail implements I_PromotionDetail {

    private static final long serialVersionUID = -622159997536592734L;
    
    private int seqNo;
    private int type;
    private I_PromotionCondition condition;
    private I_PromotionReward reward;

    public PromotionDetail() {
        super();
    }
    
    public PromotionDetail(I_PromotionDetail promotionDetail) {
        super();
        
        this.seqNo = promotionDetail.getSeqNo();
        this.type = promotionDetail.getType();
        this.condition = promotionDetail.getCondition();
        this.reward = promotionDetail.getReward();
    }
    
    public int getSeqNo() {
        return seqNo;
    }
    
    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }
    
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public I_PromotionCondition getCondition() {
        return condition;
    }

    public void setCondition(I_PromotionCondition condition) {
        this.condition = condition;
    }

    public I_PromotionReward getReward() {
        return reward;
    }

    public void setReward(I_PromotionReward reward) {
        this.reward = reward;
    }

}
