package com.viettel.backend.service.promotion.pdto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.viettel.repository.common.domain.promotion.I_PromotionReward;

public class PromotionReward implements Serializable, I_PromotionReward {

    private static final long serialVersionUID = -5008696393035849587L;
    
    private BigDecimal percentage;
    private String productId;
    private String productText;
    private BigDecimal quantity;
    
    public PromotionReward() {
        super();
    }
    
    public PromotionReward(I_PromotionReward promotionReward) {
        super();
        
        this.percentage = promotionReward.getPercentage();
        this.productId = promotionReward.getProductId();
        this.productText = promotionReward.getProductText();
        this.quantity = promotionReward.getQuantity();
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductText() {
        return productText;
    }

    public void setProductText(String productText) {
        this.productText = productText;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

}
