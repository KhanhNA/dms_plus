package com.viettel.backend.service.survey;

import com.viettel.backend.dto.survey.SurveyCreateDto;
import com.viettel.backend.dto.survey.SurveyDto;
import com.viettel.backend.dto.survey.SurveyListDto;
import com.viettel.backend.service.core.common.CategoryEditableService;

public interface SurveyEditableService extends CategoryEditableService<SurveyListDto, SurveyDto, SurveyCreateDto> {

}
