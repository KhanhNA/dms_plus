package com.viettel.backend.service.survey;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.survey.SurveyDto;
import com.viettel.backend.oauth2.core.UserLogin;

public interface SurveyReadonlyService {

    public ListDto<SurveyDto> getSurveysAvailableByCustomer(UserLogin userLogin, String _customerId);

}
