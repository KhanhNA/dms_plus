package com.viettel.backend.service.survey;

import java.io.InputStream;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.survey.SurveyListDto;
import com.viettel.backend.dto.survey.SurveyResultDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface SurveyReportService {
    
    public ListDto<SurveyListDto> getSurveys(UserLogin userLogin, String search, PageSizeRequest pageSizeRequest);
    
    public SurveyResultDto getSurveyReport(UserLogin userLogin, String surveyId);

    public InputStream exportSurveyReport(UserLogin userLogin, String surveyId, String lang);

}
