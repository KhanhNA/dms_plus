package com.viettel.backend.service.survey.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.survey.SurveyCreateDto;
import com.viettel.backend.dto.survey.SurveyDto;
import com.viettel.backend.dto.survey.SurveyListDto;
import com.viettel.backend.dto.survey.SurveyCreateDto.SurveyQuestionCreateDto;
import com.viettel.backend.exeption.BusinessAssert;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.CategoryComplexEditableServiceImpl;
import com.viettel.backend.service.survey.SurveyEditableService;
import com.viettel.backend.service.survey.pdto.Survey;
import com.viettel.backend.service.survey.pdto.SurveyOption;
import com.viettel.backend.service.survey.pdto.SurveyQuestion;
import com.viettel.repository.common.CategoryComplexBasicRepository;
import com.viettel.repository.common.SurveyRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyHeader;
import com.viettel.repository.common.domain.survey.I_SurveyOption;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.SimpleDate;

@RolePermission(value={ Role.ADMIN })
@RequireModule(Module.SURVEY)
@Service
public class SurveyEditableServiceImpl extends
        CategoryComplexEditableServiceImpl<I_SurveyHeader, I_Survey, I_Survey, SurveyListDto, SurveyDto, SurveyCreateDto> implements
        SurveyEditableService {

    @Autowired
    private SurveyRepository surveyRepository;
    
    @Override
    protected CategoryComplexBasicRepository<I_SurveyHeader, I_Survey, I_Survey> getRepository() {
        return surveyRepository;
    }
    
    @Override
    protected void beforeSetActive(UserLogin userLogin, I_Survey domain, boolean active) {
        throw new UnsupportedOperationException();
    }

    @Override
    public SurveyListDto createListSimpleDto(UserLogin userLogin, I_SurveyHeader domain) {
        return new SurveyListDto(domain);
    }

    @Override
    public SurveyDto createListDetailDto(UserLogin userLogin, I_Survey domain) {
        return new SurveyDto(domain);
    }
    
    @Override
    public I_Survey getObjectForCreate(UserLogin userLogin, SurveyCreateDto createdto) {
        BusinessAssert.notNull(createdto);

        Survey survey = new Survey();
        initiatePO(userLogin.getClientId(), survey);
        survey.setDraft(true);

        return getObjectForUpdate(userLogin, survey, createdto);
    }
    
    @Override
    public I_Survey getObjectForUpdate(UserLogin userLogin, I_Survey oldDomain, SurveyCreateDto createdto) {
        BusinessAssert.notNull(createdto);
        BusinessAssert.notNull(createdto.getStartDate());
        BusinessAssert.notNull(createdto.getEndDate());
        BusinessAssert.notNull(createdto.getQuestions());
        BusinessAssert.notEmpty(createdto.getQuestions());
        
        // Not allow update non draft record
        BusinessAssert.isTrue(oldDomain.isDraft(), "Modify non-draft record are not allowed");
        
        Survey survey = new Survey(oldDomain);
        survey = updateCategoryFieldFromDto(userLogin, survey, createdto);
        
        SimpleDate startDate = getMandatoryIsoDate(createdto.getStartDate());
        SimpleDate endDate = getMandatoryIsoDate(createdto.getEndDate());
        BusinessAssert.isTrue(startDate.compareTo(endDate) <= 0, "startDate > endDate");

        survey.setStartDate(startDate);
        survey.setEndDate(endDate);
        survey.setRequired(true);

        int questionSeqNo = 1;
        int optionSeqNo = 1;
        
        List<SurveyQuestion> questions = new ArrayList<SurveyQuestion>(createdto.getQuestions().size());
        for (SurveyQuestionCreateDto questionDto : createdto.getQuestions()) {
            SurveyQuestion question = new SurveyQuestion();
            question.setSeqNo(questionSeqNo++);
            question.setName(questionDto.getName());
            question.setMultipleChoice(questionDto.isMultipleChoice());
            question.setRequired(questionDto.isRequired());

            List<CategoryCreateDto> optionDtos = questionDto.getOptions();
            BusinessAssert.notNull(optionDtos);
            BusinessAssert.notEmpty(optionDtos);
            
            List<SurveyOption> options = new ArrayList<SurveyOption>(optionDtos.size());
            for (CategoryCreateDto optionDto : optionDtos) {
                SurveyOption option = new SurveyOption();
                option.setSeqNo(optionSeqNo++);
                option.setName(optionDto.getName());
                options.add(option);
            }

            question.setOptions(new ArrayList<I_SurveyOption>(options));
            
            questions.add(question);
        }

        survey.setQuestions(new ArrayList<I_SurveyQuestion>(questions));
        
        return survey;
    }
    
}
