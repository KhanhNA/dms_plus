package com.viettel.backend.service.survey.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.survey.SurveyDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.core.common.AbstractService;
import com.viettel.backend.service.survey.SurveyReadonlyService;
import com.viettel.repository.common.SurveyRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.survey.I_Survey;

@Service
@RequireModule(Module.SURVEY)
public class SurveyReadonlyServiceImpl extends AbstractService implements SurveyReadonlyService {

    @Autowired
    private SurveyRepository surveyRepository;
    
    @Override
    public ListDto<SurveyDto> getSurveysAvailableByCustomer(UserLogin userLogin, String _customerId) {
        List<I_Survey> surveys = surveyRepository.getSurveysAvailable(userLogin.getClientId());
        List<SurveyDto> dtos = new ArrayList<SurveyDto>(surveys.size());
        for (I_Survey survey : surveys) {
            dtos.add(new SurveyDto(survey));
        }
        
        return new ListDto<SurveyDto>(dtos);
    }
    
}
