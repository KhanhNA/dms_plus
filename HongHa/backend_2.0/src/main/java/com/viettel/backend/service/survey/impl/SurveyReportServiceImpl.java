package com.viettel.backend.service.survey.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.viettel.backend.dto.common.ListDto;
import com.viettel.backend.dto.survey.SurveyListDto;
import com.viettel.backend.dto.survey.SurveyResultDto;
import com.viettel.backend.dto.survey.SurveyResultDto.SurveyOptionResultDto;
import com.viettel.backend.dto.survey.SurveyResultDto.SurveyQuestionResultDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.aspect.RequireModule;
import com.viettel.backend.service.aspect.RolePermission;
import com.viettel.backend.service.core.common.AbstractExportService;
import com.viettel.backend.service.survey.SurveyReportService;
import com.viettel.repository.common.SurveyAnswerRepository;
import com.viettel.repository.common.SurveyRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.data.I_SurveyAnswer;
import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyHeader;
import com.viettel.repository.common.domain.survey.I_SurveyOption;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;

@RolePermission(value = { Role.ADMIN, Role.OBSERVER, Role.SUPERVISOR, Role.DISTRIBUTOR_ADMIN, Role.ADVANCE_SUPERVISOR })
@RequireModule(Module.SURVEY)
@Service
public class SurveyReportServiceImpl extends AbstractExportService implements SurveyReportService {

	@Autowired
	private SurveyRepository surveyRepository;

	@Autowired
	private SurveyAnswerRepository surveyAnswerRepository;

	@Override
	public ListDto<SurveyListDto> getSurveys(UserLogin userLogin, String search, PageSizeRequest pageSizeRequest) {
		List<I_SurveyHeader> surveys = surveyRepository.getSurveysStarted(userLogin.getClientId(), search,
				pageSizeRequest);
		if (CollectionUtils.isEmpty(surveys) && pageSizeRequest.getPageNumber() == 0) {
			return ListDto.emptyList();
		}

		List<SurveyListDto> dtos = new ArrayList<SurveyListDto>(surveys.size());
		for (I_SurveyHeader survey : surveys) {
			dtos.add(new SurveyListDto(survey));
		}

		long size = Long.valueOf(dtos.size());
		if (pageSizeRequest != null) {
			if (pageSizeRequest.getPageNumber() > 0 || pageSizeRequest.getPageSize() == size) {
				size = surveyRepository.countSurveysStarted(userLogin.getClientId(), search);
			}
		}

		return new ListDto<SurveyListDto>(dtos, size);
	}

	@Override
	public SurveyResultDto getSurveyReport(UserLogin userLogin, String surveyId) {
		I_Survey survey = getMandatoryPO(userLogin, surveyId, surveyRepository);

		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		Set<String> distributorIds = getIdSet(distributors);

		Map<Integer, Integer> resultByOptionId = surveyAnswerRepository.getOptionCountMap(userLogin.getClientId(),
				survey.getId(), distributorIds);

		List<SurveyQuestionResultDto> questionResults = new LinkedList<>();
		if (survey.getQuestions() != null) {
			for (I_SurveyQuestion question : survey.getQuestions()) {

				if (question.getOptions() != null) {
					List<SurveyOptionResultDto> optionResults = new LinkedList<>();
					for (I_SurveyOption option : question.getOptions()) {
						Integer result = resultByOptionId.get(option.getSeqNo());
						result = result == null ? 0 : result;

						SurveyOptionResultDto optionResultDto = new SurveyOptionResultDto(option, result);

						optionResults.add(optionResultDto);
					}

					questionResults.add(new SurveyQuestionResultDto(question, optionResults));
				}

			}
		}

		return new SurveyResultDto(survey, questionResults);
	}

	@Override
	public InputStream exportSurveyReport(UserLogin userLogin, String surveyId, String lang) {
		lang = getLang(lang);

		I_Survey survey = getMandatoryPO(userLogin, surveyId, surveyRepository);

		List<I_Distributor> distributors = getAccessibleDistributors(userLogin);
		Set<String> distributorIds = getIdSet(distributors);

		SXSSFWorkbook workbook = null;
		OutputStream outputStream = null;
		try {
			workbook = new SXSSFWorkbook();
			// Create sheet
			SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(translate(lang, "survey"));

			Row row;

			CellStyle headerStyle = getCellStyle(workbook, true, true, (short) 11, CellStyle.ALIGN_CENTER);
			CellStyle contentStyle = getCellStyle(workbook, false, true, (short) 11, CellStyle.ALIGN_LEFT);

			row = sheet.createRow(0);
			createCell(row, 0, headerStyle, translate(lang, "time"));
			createCell(row, 1, headerStyle, translate(lang, "customer"));
			createCell(row, 2, headerStyle, translate(lang, "salesman"));
			if (survey.getQuestions() != null) {
				int index = 3;
				for (I_SurveyQuestion question : survey.getQuestions()) {
					createCell(row, index, headerStyle, question.getName());
					index++;
				}
			}

			int rownum = 1;

			for (String distributorId : distributorIds) {
				long count = surveyAnswerRepository.countSurveyAnswerByDistributors(userLogin.getClientId(),
						survey.getId(), Collections.singleton(distributorId));

				int nbRecordByQuery = 2000;
				int nbQuery = ((int) count / nbRecordByQuery) + ((count % nbRecordByQuery) == 0 ? 0 : 1);

				for (int page = 1; page <= nbQuery; page++) {
					PageSizeRequest pageSizeRequest = new PageSizeRequest(page - 1, nbRecordByQuery);

					List<I_SurveyAnswer> surveyAnswers = surveyAnswerRepository.getSurveyAnswerByDistributors(
							userLogin.getClientId(), survey.getId(), Collections.singleton(distributorId),
							pageSizeRequest);

					for (int i = 0, length = surveyAnswers.size(); i < length; i++) {
						I_SurveyAnswer surveyAnswer = surveyAnswers.get(i);

						row = sheet.createRow(rownum++);

						createCell(row, 0, contentStyle, surveyAnswer.getCreatedTime().format("dd/MM/yyyy HH:mm"));
						createCell(row, 1, contentStyle, surveyAnswer.getCustomer().getName());
						createCell(row, 2, contentStyle, surveyAnswer.getCreatedBy().getFullname());

						if (survey.getQuestions() != null) {
							int index = 3;

							for (I_SurveyQuestion question : survey.getQuestions()) {
								StringBuilder optionsDisplay = new StringBuilder();
								boolean isFirst = true;
								if (question.getOptions() != null) {
									for (I_SurveyOption option : question.getOptions()) {
										if (surveyAnswer.getOptions() != null
												&& surveyAnswer.getOptions().contains(option.getSeqNo())) {
											if (!isFirst) {
												optionsDisplay.append(", ");
											}
											isFirst = false;

											optionsDisplay.append(option.getName());
										}
									}
								}

								createCell(row, index, contentStyle, optionsDisplay.toString());

								index++;
							}
						}
					}
				}
			}

			for (int i = 0, length = 3 + survey.getQuestions().size(); i < length; i++) {
				sheet.autoSizeColumn(i);
			}

			File outTempFile = File.createTempFile("SurveyAnswer" + "_" + System.currentTimeMillis(), "tmp");
			outputStream = new FileOutputStream(outTempFile);
			workbook.write(outputStream);

			return new FileInputStream(outTempFile);
		} catch (IOException e) {
			throw new UnsupportedOperationException(e);
		} finally {
			IOUtils.closeQuietly(workbook);
			IOUtils.closeQuietly(outputStream);
		}

	}

}
