package com.viettel.backend.service.survey.pdto;

import java.util.List;

import com.viettel.backend.service.core.pdto.Category;
import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;
import com.viettel.repository.common.entity.SimpleDate;

public class Survey extends Category implements I_Survey {

    private static final long serialVersionUID = -864670411951756277L;
    
    private SimpleDate startDate;
    private SimpleDate endDate;
    private boolean isRequired;
    private List<I_SurveyQuestion> questions;

    public Survey() {
        super();
    }
    
    public Survey(I_Survey survey) {
        super(survey);
        
        this.startDate = survey.getStartDate();
        this.endDate = survey.getEndDate();
        this.isRequired = survey.isRequired();
        this.questions = survey.getQuestions();
    }
    
    public SimpleDate getStartDate() {
        return startDate;
    }

    public void setStartDate(SimpleDate startDate) {
        this.startDate = startDate;
    }

    public SimpleDate getEndDate() {
        return endDate;
    }

    public void setEndDate(SimpleDate endDate) {
        this.endDate = endDate;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean isRequired) {
        this.isRequired = isRequired;
    }

    public List<I_SurveyQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<I_SurveyQuestion> questions) {
        this.questions = questions;
    }

}
