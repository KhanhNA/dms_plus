package com.viettel.backend.service.survey.pdto;

import java.io.Serializable;

import com.viettel.repository.common.domain.survey.I_SurveyOption;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;

public class SurveyOption implements Serializable, I_SurveyOption {

    private static final long serialVersionUID = 4431192905615371851L;

    private int seqNo;
    private String name;

    public SurveyOption() {
        super();
    }

    public SurveyOption(I_SurveyQuestion surveyQuestion) {
        super();

        this.seqNo = surveyQuestion.getSeqNo();
        this.name = surveyQuestion.getName();
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
