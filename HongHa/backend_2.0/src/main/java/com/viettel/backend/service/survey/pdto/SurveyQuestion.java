package com.viettel.backend.service.survey.pdto;

import java.io.Serializable;
import java.util.List;

import com.viettel.repository.common.domain.survey.I_SurveyOption;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;

public class SurveyQuestion implements Serializable, I_SurveyQuestion {

    private static final long serialVersionUID = 4431192905615371851L;

    private int seqNo;
    private String name;
    private boolean multipleChoice;
    private boolean required;
    private List<I_SurveyOption> options;

    public SurveyQuestion() {
        super();
    }

    public SurveyQuestion(I_SurveyQuestion surveyQuestion) {
        super();

        this.seqNo = surveyQuestion.getSeqNo();
        this.name = surveyQuestion.getName();
        this.multipleChoice = surveyQuestion.isMultipleChoice();
        this.required = surveyQuestion.isRequired();
        this.options = surveyQuestion.getOptions();
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMultipleChoice() {
        return multipleChoice;
    }

    public void setMultipleChoice(boolean multipleChoice) {
        this.multipleChoice = multipleChoice;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<I_SurveyOption> getOptions() {
        return this.options;
    }

    public void setOptions(List<I_SurveyOption> options) {
        this.options = options;
    }

}
