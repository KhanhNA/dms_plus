package com.viettel.backend.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.exeption.BusinessExceptionCode;
import com.viettel.backend.util.entity.FileTypeDetect;

public class FileUtils {
	private static List<String> whiteListExtension = Arrays.asList(".xls", ".xlsx", ".png", ".jpg", ".jpeg");
	private static List<String> whiteListContentType = Arrays.asList("application/vnd.ms-excel",
			"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "image/png", "image/jpg",
			"image/jpeg");

	public static List<String> getWhiteListContentType() {
		return whiteListContentType;
	}

	public static boolean isExtensionValid(String fileName) {
		for (String extension : whiteListExtension) {
			if (fileName.toLowerCase().endsWith(extension)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isHeaderValid(byte[] bytes) {
		int lengthOffset = 560;
		try {
			if (bytes.length < lengthOffset) {
				return false;
			}
			byte[] header = new byte[lengthOffset];
			InputStream is = new ByteArrayInputStream(bytes);
			is.read(header, 0, lengthOffset);
			FileTypeDetect fileType = FileDetect.GetFileType(header);
			return fileType != null;
		} catch (IOException e) {
			throw new BusinessException(BusinessExceptionCode.FILE_INVALID, "it's not valid file", e);
		}
	}

	// private static InputStream Clone(InputStream inputStream) throws
	// IOException {
	// ByteArrayOutputStream baos = new ByteArrayOutputStream();
	// byte[] buffer = new byte[2048];
	// int len;
	// while ((len = inputStream.read(buffer)) > -1) {
	// baos.write(buffer, 0, len);
	// }
	// InputStream is = new ByteArrayInputStream(baos.toByteArray());
	// return is;
	// }

	public static class FileDetect {
		private static FileTypeDetect xls = new FileTypeDetect(new byte[] { (byte) 0x09, (byte) 0x08, (byte) 0x10,
				(byte) 0x00, (byte) 0x00, (byte) 0x06, (byte) 0x05, (byte) 0x00 }, 512, "xls", "application/excel");
		private static FileTypeDetect xlsx = new FileTypeDetect(
				new byte[] { (byte) 0x50, (byte) 0x4B, (byte) 0x03, (byte) 0x04, (byte) 0x14, (byte) 0x00 }, "xlsx",
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		private static FileTypeDetect jpg = new FileTypeDetect(new byte[] { (byte) 0xFF, (byte) 0xD8, (byte) 0xFF },
				"jpg", "image/jpeg");
		private static FileTypeDetect png = new FileTypeDetect(new byte[] { (byte) 0x89, (byte) 0x50, (byte) 0x4E,
				(byte) 0x47, (byte) 0x0D, (byte) 0x0A, (byte) 0x1A, (byte) 0x0A }, "png", "image/png");

		private static List<FileTypeDetect> whiteListMimeTypes = Arrays.asList(xls, xlsx, jpg, png);

		public static FileTypeDetect GetFileType(byte[] fileHeader) {
			for (FileTypeDetect type : whiteListMimeTypes) {
				int matchingCount = 0;
				for (int i = 0; i < type.getHeader().length; i++) {
					if (type.getHeader()[i] != fileHeader[i + type.getHeaderOffset()]) {
						matchingCount = 0;
						break;
					} else {
						matchingCount++;
					}
				}
				if (matchingCount == type.getHeader().length) {
					return type;
				}
			}
			return null;
		}

	}
}