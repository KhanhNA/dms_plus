package com.viettel.backend.util.entity;

public class FileTypeDetect {
	private byte[] header;
	private int headerOffset;
	private String extension;
	private String mime;

	public byte[] getHeader() {
		return header;
	}

	public void setHeader(byte[] header) {
		this.header = header;
	}

	public int getHeaderOffset() {
		return headerOffset;
	}

	public void setHeaderOffset(int headerOffset) {
		this.headerOffset = headerOffset;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getMime() {
		return mime;
	}

	public void setMime(String mime) {
		this.mime = mime;
	}

	public FileTypeDetect(byte[] header, String extension, String mime) {
		this.header = header;
		this.extension = extension;
		this.mime = mime;
		this.headerOffset = 0;
	}

	public FileTypeDetect(byte[] header, int headerOffset, String extension, String mime) {
		this.header = header;
		this.extension = extension;
		this.mime = mime;
		this.headerOffset = headerOffset;
	}

	@Override
	public boolean equals(Object obj) {
		if (!this.equals(obj))
			return false;

		if (!(obj instanceof FileTypeDetect))
			return false;

		FileTypeDetect otherType = (FileTypeDetect) obj;

		if (this.header != otherType.header)
			return false;
		if (this.headerOffset != otherType.headerOffset)
			return false;
		if (this.extension != otherType.extension)
			return false;
		if (this.mime != otherType.mime)
			return false;

		return true;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	@Override
	public String toString() {
		return extension;
	}
}
