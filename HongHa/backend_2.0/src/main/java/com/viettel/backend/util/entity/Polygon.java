package com.viettel.backend.util.entity;

import java.io.Serializable;
import java.util.List;

import com.viettel.repository.common.entity.Location;

public class Polygon implements Serializable {

    private static final long serialVersionUID = -2818582836630293122L;
    
    private List<Location> coordinates;
    
    public List<Location> getCoordinates() {
        return coordinates;
    }
    
    public void setCoordinates(List<Location> coordinates) {
        this.coordinates = coordinates;
    }
}
