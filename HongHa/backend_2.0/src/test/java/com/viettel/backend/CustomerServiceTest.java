package com.viettel.backend;

import static com.viettel.backend.TestUtil.assertHasBusinessException;
import static com.viettel.backend.TestUtil.createCategoryDto;
import static com.viettel.backend.TestUtil.createUser;
import static com.viettel.backend.TestUtil.getUserLoginForClientTest;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.viettel.backend.config.ServletInitializer;
import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.customer.CustomerCreateDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.distributor.DistributorCreateDto;
import com.viettel.backend.dto.distributor.DistributorDto;
import com.viettel.backend.dto.user.UserDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.config.ClientService;
import com.viettel.backend.service.core.customer.CustomerEditableService;
import com.viettel.backend.service.core.customer.AreaEditableService;
import com.viettel.backend.service.core.customer.AreaReadonlyService;
import com.viettel.backend.service.core.customer.CustomerTypeEditableService;
import com.viettel.backend.service.core.customer.CustomerTypeReadonlyService;
import com.viettel.backend.service.core.distributor.DistributorEditableService;
import com.viettel.backend.service.core.user.UserEditableService;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.MasterDataRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { ServletInitializer.class })
public class CustomerServiceTest {

    @Autowired
    private CommonRepository commonRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private MasterDataRepository masterDataRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private UserEditableService editableUserService;

    @Autowired
    private DistributorEditableService editableDistributorService;

    @Autowired
    private CustomerTypeEditableService editableCustomerTypeService;

    @Autowired
    private CustomerTypeReadonlyService customerTypeService;

    @Autowired
    private AreaEditableService editableAreaService;

    @Autowired
    private AreaReadonlyService areaService;

    @Autowired
    private CustomerEditableService editableCustomerService;

    private UserLogin userLogin;
    private DistributorDto distributorDto;

    @Before
    public void setUp() {
        this.userLogin = getUserLoginForClientTest(commonRepository, clientRepository, userRepository, configRepository,
                clientService, masterDataRepository);
        UserDto supervisor = createUser(editableUserService, userLogin, "sup", "Supervisor", Role.SUPERVISOR, null);

        DistributorCreateDto distributorCreateDto = new DistributorCreateDto();
        distributorCreateDto.setName("Distributor");
        distributorCreateDto.setSupervisorId(supervisor.getId());
        this.distributorDto = createCategoryDto(editableDistributorService, userLogin, distributorCreateDto);
    }

    @After
    public void end() {
        masterDataRepository.deleteAllDatas(this.userLogin.getClientId());
    }

    private CategorySimpleDto getCustomerType() {
        String name = "Customer Type";

        // TEST CREATE
        final CategoryCreateDto categoryCreateDto = new CategoryCreateDto();
        categoryCreateDto.setName(name + " 1");

        IdDto idDto = editableCustomerTypeService.create(userLogin, categoryCreateDto);
        assertNotNull(idDto);

        CategoryDto categoryDto = editableCustomerTypeService.getById(userLogin, idDto.getId());
        assertNotNull(categoryDto);
        assertThat(categoryDto.getId(), is(idDto.getId()));
        assertThat(categoryDto.getName(), is(categoryCreateDto.getName()));
        assertHasBusinessException(() -> editableCustomerTypeService.create(userLogin, categoryCreateDto));

        // TEST UPDATE
        categoryCreateDto.setName(name + " 2");
        assertThat(editableCustomerTypeService.update(userLogin, idDto.getId(), categoryCreateDto).getId(),
                is(idDto.getId()));
        categoryDto = editableCustomerTypeService.getById(userLogin, idDto.getId());
        assertThat(categoryDto.getId(), is(idDto.getId()));
        assertThat(categoryDto.getName(), is(categoryCreateDto.getName()));

        // TEST DELETE
        editableCustomerTypeService.delete(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableCustomerTypeService.getById(userLogin, idDto.getId()));

        // ENABLE
        categoryCreateDto.setName(name + " 1");
        idDto.setId(editableCustomerTypeService.create(userLogin, categoryCreateDto).getId());
        editableCustomerTypeService.enable(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableCustomerTypeService.delete(userLogin, idDto.getId()));

        categoryCreateDto.setName(name + " 2");
        idDto.setId(editableCustomerTypeService.createAndEnable(userLogin, categoryCreateDto).getId());
        assertHasBusinessException(() -> editableCustomerTypeService.delete(userLogin, idDto.getId()));

        // GET LIST
        assertThat(editableCustomerTypeService.getList(userLogin, null, true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(2));
        assertThat(editableCustomerTypeService
                .getList(userLogin, categoryCreateDto.getName(), true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(1));

        // READONLY
        assertThat(customerTypeService.getAll(userLogin, null).getList().size(), is(2));
        CategorySimpleDto categorySimpleDto = customerTypeService.getById(userLogin, idDto.getId());

        assertNotNull(categorySimpleDto);

        return categorySimpleDto;
    }

    private CategorySimpleDto getArea() {
        String name = "Area";

        // TEST CREATE
        final CategoryCreateDto categoryCreateDto = new CategoryCreateDto();
        categoryCreateDto.setName(name + " 1");
        assertHasBusinessException(() -> editableAreaService.create(userLogin, categoryCreateDto));

        categoryCreateDto.setDistributorId(this.distributorDto.getId());
        IdDto idDto = editableAreaService.create(userLogin, categoryCreateDto);
        assertNotNull(idDto);

        CategoryDto categoryDto = editableAreaService.getById(userLogin, idDto.getId());
        assertNotNull(categoryDto);
        assertThat(categoryDto.getId(), is(idDto.getId()));
        assertThat(categoryDto.getName(), is(categoryCreateDto.getName()));
        assertNotNull(categoryDto.getDistributor());
        assertThat(categoryDto.getDistributor().getId(), is(distributorDto.getId()));
        assertThat(categoryDto.getDistributor().getName(), is(distributorDto.getName()));
        assertThat(categoryDto.getDistributor().getCode(), is(distributorDto.getCode()));
        assertHasBusinessException(() -> editableAreaService.create(userLogin, categoryCreateDto));

        // TEST UPDATE
        categoryCreateDto.setName(name + " 2");
        assertThat(editableAreaService.update(userLogin, idDto.getId(), categoryCreateDto).getId(), is(idDto.getId()));
        categoryDto = editableAreaService.getById(userLogin, idDto.getId());
        assertThat(categoryDto.getId(), is(idDto.getId()));
        assertThat(categoryDto.getName(), is(categoryCreateDto.getName()));
        assertNotNull(categoryDto.getDistributor());
        assertThat(categoryDto.getDistributor().getId(), is(distributorDto.getId()));
        assertThat(categoryDto.getDistributor().getName(), is(distributorDto.getName()));
        assertThat(categoryDto.getDistributor().getCode(), is(distributorDto.getCode()));

        // TEST DELETE
        editableAreaService.delete(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableAreaService.getById(userLogin, idDto.getId()));

        // ENABLE
        categoryCreateDto.setName(name + " 1");
        categoryCreateDto.setDistributorId(this.distributorDto.getId());
        idDto.setId(editableAreaService.create(userLogin, categoryCreateDto).getId());
        editableAreaService.enable(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableAreaService.delete(userLogin, idDto.getId()));

        categoryCreateDto.setName(name + " 2");
        categoryCreateDto.setDistributorId(this.distributorDto.getId());
        idDto.setId(editableAreaService.createAndEnable(userLogin, categoryCreateDto).getId());
        assertHasBusinessException(() -> editableAreaService.delete(userLogin, idDto.getId()));

        // GET LIST
        assertThat(editableAreaService.getList(userLogin, null, true, false, null, new PageSizeRequest(0, 10)).getList()
                .size(), is(2));
        assertThat(editableAreaService
                .getList(userLogin, categoryCreateDto.getName(), true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(1));

        // READONLY
        assertThat(areaService.getAll(userLogin, distributorDto.getId()).getList().size(), is(2));
        CategorySimpleDto categorySimpleDto = areaService.getById(userLogin, idDto.getId());

        assertNotNull(categorySimpleDto);

        return categorySimpleDto;
    }

    @Test
    public void customerService() {
        CategorySimpleDto customerType = getCustomerType();
        CategorySimpleDto area = getArea();

        I_Config config = configRepository.getConfig(userLogin.getClientId());

        String name = "Customer";

        // TEST CREATE
        final CustomerCreateDto customerCreateDto = new CustomerCreateDto();
        customerCreateDto.setName(name + " 1");
        assertHasBusinessException(() -> editableCustomerService.create(userLogin, customerCreateDto));

        customerCreateDto.setDistributorId(this.distributorDto.getId());
        assertHasBusinessException(() -> editableCustomerService.create(userLogin, customerCreateDto));

        customerCreateDto.setAreaId(area.getId());
        assertHasBusinessException(() -> editableCustomerService.create(userLogin, customerCreateDto));

        customerCreateDto.setCustomerTypeId(customerType.getId());
        assertHasBusinessException(() -> editableCustomerService.create(userLogin, customerCreateDto));

        customerCreateDto.setMobile("999999");
        assertHasBusinessException(() -> editableCustomerService.create(userLogin, customerCreateDto));

        customerCreateDto.setLocation(config.getLocation());
        IdDto idDto = editableCustomerService.create(userLogin, customerCreateDto);
        assertNotNull(idDto);

        CustomerDto customerDto = editableCustomerService.getById(userLogin, idDto.getId());
        assertNotNull(customerDto);
        assertThat(customerDto.getId(), is(idDto.getId()));
        assertThat(customerDto.getName(), is(customerCreateDto.getName()));
        assertThat(customerDto.getMobile(), is(customerCreateDto.getMobile()));
        assertThat(customerDto.getLocation(), is(customerCreateDto.getLocation()));
        assertNotNull(customerDto.getCode());
        assertNotNull(customerDto.getDistributor());
        assertThat(customerDto.getDistributor().getId(), is(distributorDto.getId()));
        assertThat(customerDto.getDistributor().getName(), is(distributorDto.getName()));
        assertThat(customerDto.getDistributor().getCode(), is(distributorDto.getCode()));
        assertNotNull(customerDto.getArea());
        assertThat(customerDto.getArea().getId(), is(area.getId()));
        assertThat(customerDto.getArea().getName(), is(area.getName()));
        assertNotNull(customerDto.getCustomerType());
        assertThat(customerDto.getCustomerType().getId(), is(customerType.getId()));
        assertThat(customerDto.getCustomerType().getName(), is(customerType.getName()));

        assertHasBusinessException(() -> editableCustomerService.create(userLogin, customerCreateDto));

        // TEST UPDATE
        customerCreateDto.setName(name + " 2");
        assertThat(editableCustomerService.update(userLogin, idDto.getId(), customerCreateDto).getId(),
                is(idDto.getId()));
        customerDto = editableCustomerService.getById(userLogin, idDto.getId());
        assertNotNull(customerDto);
        assertThat(customerDto.getId(), is(idDto.getId()));
        assertThat(customerDto.getName(), is(customerCreateDto.getName()));
        assertThat(customerDto.getMobile(), is(customerCreateDto.getMobile()));
        assertThat(customerDto.getLocation(), is(customerCreateDto.getLocation()));
        assertNotNull(customerDto.getCode());
        assertNotNull(customerDto.getDistributor());
        assertThat(customerDto.getDistributor().getId(), is(distributorDto.getId()));
        assertThat(customerDto.getDistributor().getName(), is(distributorDto.getName()));
        assertThat(customerDto.getDistributor().getCode(), is(distributorDto.getCode()));
        assertNotNull(customerDto.getArea());
        assertThat(customerDto.getArea().getId(), is(area.getId()));
        assertThat(customerDto.getArea().getName(), is(area.getName()));
        assertNotNull(customerDto.getCustomerType());
        assertThat(customerDto.getCustomerType().getId(), is(customerType.getId()));
        assertThat(customerDto.getCustomerType().getName(), is(customerType.getName()));

        // TEST DELETE
        editableCustomerService.delete(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableCustomerService.getById(userLogin, idDto.getId()));

        // ENABLE
        customerCreateDto.setName(name + " 1");
        customerCreateDto.setDistributorId(this.distributorDto.getId());
        customerCreateDto.setAreaId(area.getId());
        customerCreateDto.setCustomerTypeId(customerType.getId());
        customerCreateDto.setMobile("999999");
        customerCreateDto.setLocation(config.getLocation());
        idDto.setId(editableCustomerService.create(userLogin, customerCreateDto).getId());
        editableCustomerService.enable(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableCustomerService.delete(userLogin, idDto.getId()));

        customerCreateDto.setName(name + " 2");
        customerCreateDto.setDistributorId(this.distributorDto.getId());
        customerCreateDto.setAreaId(area.getId());
        customerCreateDto.setCustomerTypeId(customerType.getId());
        customerCreateDto.setMobile("999999");
        customerCreateDto.setLocation(config.getLocation());
        idDto.setId(editableCustomerService.createAndEnable(userLogin, customerCreateDto).getId());
        assertHasBusinessException(() -> editableCustomerService.delete(userLogin, idDto.getId()));

        // GET LIST
        assertThat(editableCustomerService.getList(userLogin, null, true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(2));
        assertThat(editableCustomerService
                .getList(userLogin, customerCreateDto.getName(), true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(1));
    }

}
