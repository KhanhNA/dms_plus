package com.viettel.backend;

import static com.viettel.backend.TestUtil.createCategoryDto;
import static com.viettel.backend.TestUtil.createUser;
import static com.viettel.backend.TestUtil.getUserLoginForClientTest;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.viettel.backend.config.ServletInitializer;
import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.customer.CustomerCreateDto;
import com.viettel.backend.dto.customer.CustomerDto;
import com.viettel.backend.dto.distributor.DistributorCreateDto;
import com.viettel.backend.dto.distributor.DistributorDto;
import com.viettel.backend.dto.order.OrderCreateDto;
import com.viettel.backend.dto.order.OrderCreateDto.OrderDetailCreatedDto;
import com.viettel.backend.dto.product.ProductCreateDto;
import com.viettel.backend.dto.product.ProductDto;
import com.viettel.backend.dto.order.OrderDto;
import com.viettel.backend.dto.schedule.CustomerScheduleCreateDto;
import com.viettel.backend.dto.schedule.RouteCreateDto;
import com.viettel.backend.dto.schedule.RouteDto;
import com.viettel.backend.dto.schedule.ScheduleDto;
import com.viettel.backend.dto.schedule.ScheduleItemDto;
import com.viettel.backend.dto.user.UserDto;
import com.viettel.backend.dto.visit.VisitEndDto;
import com.viettel.backend.dto.visit.VisitInfoDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.config.ClientService;
import com.viettel.backend.service.core.customer.CustomerEditableService;
import com.viettel.backend.service.core.customer.AreaEditableService;
import com.viettel.backend.service.core.customer.CustomerTypeEditableService;
import com.viettel.backend.service.core.distributor.DistributorEditableService;
import com.viettel.backend.service.core.order.OrderCreatingService;
import com.viettel.backend.service.core.product.ProductCategoryEditableService;
import com.viettel.backend.service.core.product.ProductEditableService;
import com.viettel.backend.service.core.product.UOMEditableService;
import com.viettel.backend.service.core.schedule.CustomerScheduleService;
import com.viettel.backend.service.core.schedule.RouteEditableService;
import com.viettel.backend.service.core.user.UserEditableService;
import com.viettel.backend.service.core.visit.VisitCreatingService;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.MasterDataRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.domain.user.I_User.Role;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { ServletInitializer.class })
public class OrderServiceTest {

    @Autowired
    private CommonRepository commonRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private MasterDataRepository masterDataRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private UserEditableService editableUserService;

    @Autowired
    private DistributorEditableService editableDistributorService;

    @Autowired
    private AreaEditableService editableAreaService;

    @Autowired
    private CustomerTypeEditableService editableCustomerTypeService;

    @Autowired
    private CustomerEditableService editableCustomerService;

    @Autowired
    private RouteEditableService editableRouteService;

    @Autowired
    private CustomerScheduleService customerScheduleService;

    @Autowired
    private UOMEditableService editableUOMService;

    @Autowired
    private ProductCategoryEditableService editableProductCategoryService;

    @Autowired
    private ProductEditableService editableProductService;

    @Autowired
    private OrderCreatingService orderCreatingService;

    @Autowired
    private VisitCreatingService visitService;

    private UserLogin userLogin;

    private UserDto supervisorDto;

    private DistributorDto distributorDto;

    private UserDto salesmanDto;

    private CategoryDto areaDto;

    private CategoryDto customerTypeDto;

    private CustomerDto customerDto;

    private List<ProductDto> productDtos;

    @Before
    public void setUp() {
        this.userLogin = getUserLoginForClientTest(commonRepository, clientRepository, userRepository, configRepository,
                clientService, masterDataRepository);
        this.supervisorDto = createUser(editableUserService, userLogin, "sup", "Supervisor", Role.SUPERVISOR, null);

        DistributorCreateDto distributorCreateDto = new DistributorCreateDto();
        distributorCreateDto.setName("Distributor");
        distributorCreateDto.setSupervisorId(this.supervisorDto.getId());
        this.distributorDto = createCategoryDto(editableDistributorService, userLogin, distributorCreateDto);

        this.salesmanDto = createUser(editableUserService, userLogin, "sm", "Salesman", Role.SALESMAN,
                this.distributorDto.getId());

        CategoryCreateDto areaCreateDto = new CategoryCreateDto();
        areaCreateDto.setName("Area");
        areaCreateDto.setDistributorId(this.distributorDto.getId());
        this.areaDto = createCategoryDto(editableAreaService, userLogin, areaCreateDto);

        CategoryCreateDto customerTypeCreateDto = new CategoryCreateDto();
        customerTypeCreateDto.setName("Customer Type");
        this.customerTypeDto = createCategoryDto(editableCustomerTypeService, userLogin, customerTypeCreateDto);

        I_Config config = configRepository.getConfig(userLogin.getClientId());

        CustomerCreateDto customerCreateDto = new CustomerCreateDto();
        customerCreateDto.setName("Customer");
        customerCreateDto.setDistributorId(this.distributorDto.getId());
        customerCreateDto.setAreaId(this.areaDto.getId());
        customerCreateDto.setCustomerTypeId(this.customerTypeDto.getId());
        customerCreateDto.setMobile("999999");
        customerCreateDto.setLocation(config.getLocation());
        this.customerDto = createCategoryDto(editableCustomerService, userLogin, customerCreateDto);

        RouteCreateDto routeCreateDto = new RouteCreateDto();
        routeCreateDto.setName("Route");
        routeCreateDto.setDistributorId(this.distributorDto.getId());
        routeCreateDto.setSalesmanId(this.salesmanDto.getId());
        RouteDto routeDto = createCategoryDto(editableRouteService, userLogin, routeCreateDto);

        ScheduleDto scheduleDto = new ScheduleDto();
        ScheduleItemDto scheduleItemDto = new ScheduleItemDto();
        scheduleItemDto.setMonday(true);
        scheduleItemDto.setTuesday(true);
        scheduleItemDto.setWednesday(true);
        scheduleItemDto.setThursday(true);
        scheduleItemDto.setFriday(true);
        scheduleItemDto.setSaturday(true);
        scheduleItemDto.setSunday(true);
        scheduleDto.setItem(scheduleItemDto);
        scheduleDto.setRouteId(routeDto.getId());

        CustomerScheduleCreateDto customerScheduleCreateDto = new CustomerScheduleCreateDto();
        customerScheduleCreateDto.setId(this.customerDto.getId());
        customerScheduleCreateDto.setSchedule(scheduleDto);
        customerScheduleService.saveCustomerSchedule(userLogin, customerScheduleCreateDto);

        CategoryCreateDto uomCreateDto = new CategoryCreateDto();
        uomCreateDto.setName("UOM");
        uomCreateDto.setCode("UOM");
        CategoryDto uomDto = createCategoryDto(editableUOMService, userLogin, uomCreateDto);

        CategoryCreateDto productCategoryCreateDto = new CategoryCreateDto();
        productCategoryCreateDto.setName("Product Type");
        CategoryDto productCategoryDto = createCategoryDto(editableProductCategoryService, userLogin,
                productCategoryCreateDto);

        int numberProduct = 10;
        this.productDtos = new ArrayList<>(numberProduct);
        for (int i = 1; i <= numberProduct; i++) {
            ProductCreateDto productCreateDto = new ProductCreateDto();
            productCreateDto.setName("Product " + i);
            productCreateDto.setCode("P" + i);
            productCreateDto.setPrice(BigDecimal.ONE);
            productCreateDto.setProductivity(BigDecimal.ONE);
            productCreateDto.setPhoto(config.getProductPhoto());
            productCreateDto.setUomId(uomDto.getId());
            productCreateDto.setProductCategoryId(productCategoryDto.getId());

            this.productDtos.add(createCategoryDto(editableProductService, userLogin, productCreateDto));
        }
    }

    @After
    public void end() {
        masterDataRepository.deleteAllDatas(this.userLogin.getClientId());
    }

    @Test
    public void orderCreating() {
        OrderCreateDto orderCreateDto = new OrderCreateDto();
        orderCreateDto.setDistributorId(this.distributorDto.getId());
        orderCreateDto.setSalesmanId(this.salesmanDto.getId());
        orderCreateDto.setCustomerId(this.customerDto.getId());
        orderCreateDto.setDeliveryType(I_Order.DELIVERY_TYPE_IMMEDIATE);
        List<OrderDetailCreatedDto> details = new ArrayList<>(this.productDtos.size());
        for (ProductDto productDto : productDtos) {
            OrderDetailCreatedDto detail = new OrderDetailCreatedDto();
            detail.setProductId(productDto.getId());
            detail.setQuantity(BigDecimal.ONE);
            details.add(detail);
        }
        orderCreateDto.setDetails(details);

        UserLogin salesmanUserLogin = new UserLogin(userLogin.getClientId(), userLogin.getClientCode(),
                userLogin.getClientName(), salesmanDto.getId(), salesmanDto.getUsername(), salesmanDto.getRole(),
                Module.ALL);

        IdDto orderIdDto = orderCreatingService.createOrder(salesmanUserLogin, orderCreateDto);

        OrderDto orderDto = orderCreatingService.getOrderById(salesmanUserLogin, orderIdDto.getId());

        assertNotNull(orderDto);
        assertThat(orderDto.getGrandTotal(), is(BigDecimal.ONE.multiply(new BigDecimal(this.productDtos.size()))));
    }

    @Test
    public void visitCreating() {
        I_Config config = configRepository.getConfig(userLogin.getClientId());

        UserLogin salesmanUserLogin = new UserLogin(userLogin.getClientId(), userLogin.getClientCode(),
                userLogin.getClientName(), salesmanDto.getId(), salesmanDto.getUsername(), salesmanDto.getRole(),
                Module.ALL);

        IdDto visitDto = visitService.startVisit(salesmanUserLogin, customerDto.getId(), config.getLocation());

        VisitEndDto visitEndDto = new VisitEndDto();

        VisitInfoDto visitInfoDto = visitService.endVisit(salesmanUserLogin, visitDto.getId(), visitEndDto);

        assertNotNull(visitInfoDto);
        assertThat(visitInfoDto.getLocationStatus(), is(I_Visit.LOCATION_STATUS_LOCATED));
        assertThat(visitInfoDto.getSalesman().getId(), is(this.salesmanDto.getId()));
        assertThat(visitInfoDto.getCustomer().getId(), is(this.customerDto.getId()));
    }

}
