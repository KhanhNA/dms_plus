package com.viettel.backend;

import static com.viettel.backend.TestUtil.assertHasBusinessException;
import static com.viettel.backend.TestUtil.getUserLoginForClientTest;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.viettel.backend.config.ServletInitializer;
import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.CategorySimpleDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.product.ProductCreateDto;
import com.viettel.backend.dto.product.ProductDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.config.ClientService;
import com.viettel.backend.service.core.product.ProductCategoryEditableService;
import com.viettel.backend.service.core.product.ProductEditableService;
import com.viettel.backend.service.core.product.UOMEditableService;
import com.viettel.backend.service.core.product.ProductCategoryReadonlyService;
import com.viettel.backend.service.core.product.UOMReadonlyService;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.MasterDataRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.entity.PageSizeRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { ServletInitializer.class })
public class ProductServiceTest {

    @Autowired
    private CommonRepository commonRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private MasterDataRepository masterDataRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private UOMEditableService editableUOMService;

    @Autowired
    private UOMReadonlyService uomService;

    @Autowired
    private ProductCategoryEditableService editableProductCategoryService;

    @Autowired
    private ProductCategoryReadonlyService productCategoryService;

    @Autowired
    private ProductEditableService editableProductService;

    private UserLogin userLogin;

    @Before
    public void setUp() {
        this.userLogin = getUserLoginForClientTest(commonRepository, clientRepository, userRepository,
                configRepository, clientService, masterDataRepository);
    }

    @After
    public void end() {
        masterDataRepository.deleteAllDatas(this.userLogin.getClientId());
    }

    private CategorySimpleDto getUOM() {
        String name = "UOM";

        // TEST CREATE
        final CategoryCreateDto categoryCreateDto = new CategoryCreateDto();
        categoryCreateDto.setName(name + " 1");
        assertHasBusinessException(() -> editableUOMService.create(userLogin, categoryCreateDto));

        categoryCreateDto.setCode(name + "1");
        IdDto idDto = editableUOMService.create(userLogin, categoryCreateDto);
        assertNotNull(idDto);

        CategoryDto categoryDto = editableUOMService.getById(userLogin, idDto.getId());
        assertNotNull(categoryDto);
        assertThat(categoryDto.getId(), is(idDto.getId()));
        assertThat(categoryDto.getName(), is(categoryCreateDto.getName()));
        assertThat(categoryDto.getCode(), is(categoryCreateDto.getCode()));
        assertHasBusinessException(() -> editableUOMService.create(userLogin, categoryCreateDto));

        // TEST UPDATE
        categoryCreateDto.setName(name + " 2");
        assertThat(editableUOMService.update(userLogin, idDto.getId(), categoryCreateDto).getId(), is(idDto.getId()));
        categoryDto = editableUOMService.getById(userLogin, idDto.getId());
        assertThat(categoryDto.getId(), is(idDto.getId()));
        assertThat(categoryDto.getName(), is(categoryCreateDto.getName()));
        assertThat(categoryDto.getCode(), is(categoryCreateDto.getCode()));

        // TEST DELETE
        editableUOMService.delete(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableUOMService.getById(userLogin, idDto.getId()));

        // ENABLE
        categoryCreateDto.setName(name + " 1");
        categoryCreateDto.setCode(name + "1");
        idDto.setId(editableUOMService.create(userLogin, categoryCreateDto).getId());
        editableUOMService.enable(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableUOMService.delete(userLogin, idDto.getId()));

        categoryCreateDto.setName(name + " 2");
        categoryCreateDto.setCode(name + "2");
        idDto.setId(editableUOMService.createAndEnable(userLogin, categoryCreateDto).getId());
        assertHasBusinessException(() -> editableUOMService.delete(userLogin, idDto.getId()));

        // GET LIST
        assertThat(editableUOMService.getList(userLogin, null, true, false, null, new PageSizeRequest(0, 10)).getList()
                .size(), is(2));
        assertThat(editableUOMService
                .getList(userLogin, categoryCreateDto.getName(), true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(1));

        // READONLY
        assertThat(uomService.getAll(userLogin, null).getList().size(), is(2));
        CategorySimpleDto categorySimpleDto = uomService.getById(userLogin, idDto.getId());

        assertNotNull(categorySimpleDto);

        return categorySimpleDto;
    }

    private CategorySimpleDto getProductCategory() {
        String name = "Product Category";

        // TEST CREATE
        final CategoryCreateDto categoryCreateDto = new CategoryCreateDto();
        categoryCreateDto.setName(name + " 1");

        IdDto idDto = editableProductCategoryService.create(userLogin, categoryCreateDto);
        assertNotNull(idDto);

        CategoryDto categoryDto = editableProductCategoryService.getById(userLogin, idDto.getId());
        assertNotNull(categoryDto);
        assertThat(categoryDto.getId(), is(idDto.getId()));
        assertThat(categoryDto.getName(), is(categoryCreateDto.getName()));
        assertHasBusinessException(() -> editableProductCategoryService.create(userLogin, categoryCreateDto));

        // TEST UPDATE
        categoryCreateDto.setName(name + " 2");
        assertThat(editableProductCategoryService.update(userLogin, idDto.getId(), categoryCreateDto).getId(),
                is(idDto.getId()));
        categoryDto = editableProductCategoryService.getById(userLogin, idDto.getId());
        assertThat(categoryDto.getId(), is(idDto.getId()));
        assertThat(categoryDto.getName(), is(categoryCreateDto.getName()));

        // TEST DELETE
        editableProductCategoryService.delete(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableProductCategoryService.getById(userLogin, idDto.getId()));

        // ENABLE
        categoryCreateDto.setName(name + " 1");
        idDto.setId(editableProductCategoryService.create(userLogin, categoryCreateDto).getId());
        editableProductCategoryService.enable(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableProductCategoryService.delete(userLogin, idDto.getId()));

        categoryCreateDto.setName(name + " 2");
        idDto.setId(editableProductCategoryService.createAndEnable(userLogin, categoryCreateDto).getId());
        assertHasBusinessException(() -> editableProductCategoryService.delete(userLogin, idDto.getId()));

        // GET LIST
        assertThat(editableProductCategoryService
                .getList(userLogin, null, true, false, null, new PageSizeRequest(0, 10)).getList().size(), is(2));
        assertThat(editableProductCategoryService
                .getList(userLogin, categoryCreateDto.getName(), true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(1));

        // READONLY
        assertThat(productCategoryService.getAll(userLogin, null).getList().size(), is(2));
        CategorySimpleDto categorySimpleDto = productCategoryService.getById(userLogin, idDto.getId());

        assertNotNull(categorySimpleDto);

        return categorySimpleDto;
    }

    @Test
    public void productService() {
        CategorySimpleDto uom = getUOM();
        CategorySimpleDto productCategory = getProductCategory();

        I_Config config = configRepository.getConfig(userLogin.getClientId());

        String name = "Product";

        // TEST CREATE
        final ProductCreateDto productCreateDto = new ProductCreateDto();
        productCreateDto.setName(name + " 1");
        assertHasBusinessException(() -> editableProductService.create(userLogin, productCreateDto));

        productCreateDto.setCode(name + "1");
        assertHasBusinessException(() -> editableProductService.create(userLogin, productCreateDto));

        productCreateDto.setPrice(BigDecimal.ONE);
        assertHasBusinessException(() -> editableProductService.create(userLogin, productCreateDto));

        productCreateDto.setProductivity(BigDecimal.ONE);
        assertHasBusinessException(() -> editableProductService.create(userLogin, productCreateDto));

        productCreateDto.setPhoto(config.getProductPhoto());
        assertHasBusinessException(() -> editableProductService.create(userLogin, productCreateDto));

        productCreateDto.setUomId(uom.getId());
        assertHasBusinessException(() -> editableProductService.create(userLogin, productCreateDto));

        productCreateDto.setProductCategoryId(productCategory.getId());

        IdDto idDto = editableProductService.create(userLogin, productCreateDto);
        assertNotNull(idDto);

        ProductDto productDto = editableProductService.getById(userLogin, idDto.getId());
        assertNotNull(productDto);
        assertThat(productDto.getId(), is(idDto.getId()));
        assertThat(productDto.getName(), is(productCreateDto.getName()));
        assertThat(productDto.getCode(), is(productCreateDto.getCode().toUpperCase()));
        assertThat(productDto.getPrice(), is(productCreateDto.getPrice()));
        assertThat(productDto.getProductivity(), is(productCreateDto.getProductivity()));
        assertThat(productDto.getPhoto(), is(productCreateDto.getPhoto()));
        assertNotNull(productDto.getUom());
        assertThat(productDto.getUom().getId(), is(uom.getId()));
        assertThat(productDto.getUom().getName(), is(uom.getName()));
        assertThat(productDto.getUom().getCode(), is(uom.getCode()));
        assertNotNull(productDto.getProductCategory());
        assertThat(productDto.getProductCategory().getId(), is(productCategory.getId()));
        assertThat(productDto.getProductCategory().getName(), is(productCategory.getName()));

        assertHasBusinessException(() -> editableProductService.create(userLogin, productCreateDto));

        // TEST UPDATE
        productCreateDto.setName(name + " 2");
        assertThat(editableProductService.update(userLogin, idDto.getId(), productCreateDto).getId(),
                is(idDto.getId()));
        productDto = editableProductService.getById(userLogin, idDto.getId());
        assertNotNull(productDto);
        assertThat(productDto.getId(), is(idDto.getId()));
        assertThat(productDto.getName(), is(productCreateDto.getName()));
        assertThat(productDto.getCode(), is(productCreateDto.getCode().toUpperCase()));
        assertThat(productDto.getPrice(), is(productCreateDto.getPrice()));
        assertThat(productDto.getProductivity(), is(productCreateDto.getProductivity()));
        assertThat(productDto.getPhoto(), is(productCreateDto.getPhoto()));
        assertNotNull(productDto.getUom());
        assertThat(productDto.getUom().getId(), is(uom.getId()));
        assertThat(productDto.getUom().getName(), is(uom.getName()));
        assertThat(productDto.getUom().getCode(), is(uom.getCode()));
        assertNotNull(productDto.getProductCategory());
        assertThat(productDto.getProductCategory().getId(), is(productCategory.getId()));
        assertThat(productDto.getProductCategory().getName(), is(productCategory.getName()));

        // TEST DELETE
        editableProductService.delete(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableProductService.getById(userLogin, idDto.getId()));

        // ENABLE
        productCreateDto.setName(name + " 1");
        productCreateDto.setCode(name + "1");
        productCreateDto.setPrice(BigDecimal.ONE);
        productCreateDto.setProductivity(BigDecimal.ONE);
        productCreateDto.setPhoto(config.getProductPhoto());
        productCreateDto.setUomId(uom.getId());
        productCreateDto.setProductCategoryId(productCategory.getId());
        idDto.setId(editableProductService.create(userLogin, productCreateDto).getId());
        editableProductService.enable(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableProductService.delete(userLogin, idDto.getId()));

        productCreateDto.setName(name + " 2");
        productCreateDto.setCode(name + "2");
        productCreateDto.setPrice(BigDecimal.ONE);
        productCreateDto.setProductivity(BigDecimal.ONE);
        productCreateDto.setPhoto(config.getProductPhoto());
        productCreateDto.setUomId(uom.getId());
        productCreateDto.setProductCategoryId(productCategory.getId());
        idDto.setId(editableProductService.createAndEnable(userLogin, productCreateDto).getId());
        assertHasBusinessException(() -> editableProductService.delete(userLogin, idDto.getId()));

        // GET LIST
        assertThat(editableProductService.getList(userLogin, null, true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(2));
        assertThat(editableProductService
                .getList(userLogin, productCreateDto.getName(), true, false, null, new PageSizeRequest(0, 10)).getList()
                .size(), is(1));
    }

}
