package com.viettel.backend;

import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import com.viettel.backend.dto.common.CategoryCreateDto;
import com.viettel.backend.dto.common.CategoryDto;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.config.CalendarConfigDto;
import com.viettel.backend.dto.config.ClientConfigDto;
import com.viettel.backend.dto.config.ClientCreateDto;
import com.viettel.backend.dto.user.UserCreateDto;
import com.viettel.backend.dto.user.UserDto;
import com.viettel.backend.exeption.BusinessException;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.common.CategoryEditableService;
import com.viettel.backend.service.core.config.ClientService;
import com.viettel.backend.service.core.user.UserEditableService;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.MasterDataRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig.Module;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.exception.IdFormatException;

public class TestUtil {

    public static UserLogin getUserLoginForClientTest(CommonRepository commonRepository,
            ClientRepository clientRepository, UserRepository userRepository, ConfigRepository configRepository,
            ClientService clientService, MasterDataRepository masterDataRepository) {
        String rootId = commonRepository.getRootId();

        I_Category clientTest = null;

        List<I_Category> clients = clientRepository.getList(rootId, false, true, null, "test", "T", null);

        for (I_Category client : clients) {
            if ("test".equalsIgnoreCase(client.getName()) || "T".equalsIgnoreCase(client.getCode())) {
                if ("test".equalsIgnoreCase(client.getName()) && "T".equalsIgnoreCase(client.getCode())) {
                    clientTest = client;
                    break;
                } else {
                    throw new UnsupportedOperationException();
                }
            }
        }

        if (clientTest == null) {
            I_User master = userRepository.getAll(rootId).get(0);
            UserLogin masterUserLogin = new UserLogin(rootId, rootId, rootId, master.getId(), master.getUsername(),
                    master.getRole(), Module.ALL);

            I_Config systemConfig = configRepository.getConfig(rootId);

            ClientCreateDto clientCreateDto = new ClientCreateDto();
            clientCreateDto.setName("Test");
            clientCreateDto.setCode("T");

            ClientConfigDto clientConfigDto = new ClientConfigDto();
            clientConfigDto.setVisitDistanceKPI(0.3);
            clientConfigDto.setVisitDurationKPI(5 * 60 * 1000);
            clientConfigDto.setCanEditCustomerLocation(true);
            clientConfigDto.setLocation(systemConfig.getLocation());
            clientConfigDto.setModules(Module.ALL);
            clientConfigDto.setNumberDayInventoryExpire(31);
            clientCreateDto.setClientConfig(clientConfigDto);

            CalendarConfigDto calendarConfigDto = new CalendarConfigDto();
            calendarConfigDto.setWorkingDays(Arrays.asList(Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY,
                    Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY));
            clientCreateDto.setCalendarConfig(calendarConfigDto);

            IdDto idDto = clientService.create(masterUserLogin, clientCreateDto);

            clientTest = clientRepository.getById(rootId, idDto.getId());
        } else {
            masterDataRepository.deleteAllDatas(clientTest.getId());
        }

        I_User defaultAdmin = userRepository.getDefaultAdmin(clientTest.getId());

        return new UserLogin(clientTest.getId(), clientTest.getCode(), clientTest.getName(), defaultAdmin.getId(),
                defaultAdmin.getUsername(), defaultAdmin.getRole(), Module.ALL);
    }

    public static UserDto createUser(UserEditableService editableUserService, UserLogin userLogin, String username,
            String fullname, String role, String distributorId) {
        UserCreateDto userCreateDto = new UserCreateDto();
        userCreateDto.setUsername(username);
        userCreateDto.setFullname(fullname);
        userCreateDto.setRole(role);
        userCreateDto.setDistributorId(distributorId);
        IdDto idDto = editableUserService.create(userLogin, userCreateDto);
        editableUserService.enable(userLogin, idDto.getId());
        return editableUserService.getById(userLogin, idDto.getId());
    }

    public static <LD extends CategoryDto, DD extends LD, CD extends CategoryCreateDto> DD createCategoryDto(
            CategoryEditableService<LD, DD, CD> editableService, UserLogin userLogin, CD createDto) {
        IdDto idDto = editableService.create(userLogin, createDto);
        editableService.enable(userLogin, idDto.getId());
        return editableService.getById(userLogin, idDto.getId());
    }

    public static void assertHasBusinessException(Runnable runnable) {
        try {
            runnable.run();
            fail("no exception throw");
        } catch (Exception ex) {
            if (!(ex instanceof BusinessException)) {
                fail("unknown exception");
            }
        }
    }
    
    public static void assertHasIdFormatException(Runnable runnable) {
        try {
            runnable.run();
            fail("no exception throw");
        } catch (Exception ex) {
            if (!(ex instanceof IdFormatException)) {
                fail("unknown exception");
            }
        }
    }


}
