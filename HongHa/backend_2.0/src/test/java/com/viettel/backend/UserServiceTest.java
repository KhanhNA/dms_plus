package com.viettel.backend;

import static com.viettel.backend.TestUtil.assertHasBusinessException;
import static com.viettel.backend.TestUtil.assertHasIdFormatException;
import static com.viettel.backend.TestUtil.getUserLoginForClientTest;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.viettel.backend.config.ServletInitializer;
import com.viettel.backend.dto.common.IdDto;
import com.viettel.backend.dto.distributor.DistributorCreateDto;
import com.viettel.backend.dto.distributor.DistributorDto;
import com.viettel.backend.dto.user.UserCreateDto;
import com.viettel.backend.dto.user.UserDto;
import com.viettel.backend.oauth2.core.UserLogin;
import com.viettel.backend.service.core.config.ClientService;
import com.viettel.backend.service.core.distributor.DistributorEditableService;
import com.viettel.backend.service.core.user.UserEditableService;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.CommonRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.MasterDataRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { ServletInitializer.class })
public class UserServiceTest {

    @Autowired
    private CommonRepository commonRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Autowired
    private MasterDataRepository masterDataRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private UserEditableService editableUserService;

    @Autowired
    private DistributorEditableService editableDistributorService;

    private UserLogin userLogin;

    @Before
    public void setUp() {
        this.userLogin = getUserLoginForClientTest(commonRepository, clientRepository, userRepository, configRepository,
                clientService, masterDataRepository);
    }

    @After
    public void end() {
        masterDataRepository.deleteAllDatas(this.userLogin.getClientId());
    }

    @Test
    public void userService() {
        // TEST CREATE
        final UserCreateDto userCreateDto = new UserCreateDto();
        userCreateDto.setUsername("admin2");
        assertHasBusinessException(() -> editableUserService.create(userLogin, userCreateDto));
        userCreateDto.setFullname("Admin 2");
        assertHasBusinessException(() -> editableUserService.create(userLogin, userCreateDto));
        userCreateDto.setRole(Role.ADMIN);

        IdDto idDto = editableUserService.create(userLogin, userCreateDto);
        assertNotNull(idDto);
        UserDto userDto = editableUserService.getById(userLogin, idDto.getId());

        assertNotNull(userDto);
        assertThat(userDto.getId(), is(idDto.getId()));
        assertThat(userDto.getFullname(), is(userCreateDto.getFullname()));
        assertThat(userDto.getUsername(), is(userCreateDto.getUsername()));
        assertHasBusinessException(() -> editableUserService.create(userLogin, userCreateDto));

        // TEST UPDATE
        userCreateDto.setUsername("admin3");
        userCreateDto.setFullname("Admin 3");
        assertThat(editableUserService.update(userLogin, idDto.getId(), userCreateDto).getId(), is(idDto.getId()));
        userDto = editableUserService.getById(userLogin, idDto.getId());
        assertNotNull(userDto);
        assertThat(userDto.getId(), is(idDto.getId()));
        assertThat(userDto.getFullname(), is(userCreateDto.getFullname()));
        assertThat(userDto.getUsername(), is(userCreateDto.getUsername()));

        // TEST DELETE
        editableUserService.delete(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableUserService.getById(userLogin, idDto.getId()));

        // ENABLE
        userCreateDto.setUsername("admin2");
        userCreateDto.setFullname("Admin 2");
        userCreateDto.setRole(Role.ADMIN);
        userCreateDto.setDistributorId(null);
        idDto.setId(editableUserService.create(userLogin, userCreateDto).getId());
        editableUserService.enable(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableUserService.update(userLogin, idDto.getId(), userCreateDto));
        assertHasBusinessException(() -> editableUserService.delete(userLogin, idDto.getId()));

        userCreateDto.setUsername("admin3");
        userCreateDto.setFullname("Admin 3");
        userCreateDto.setRole(Role.ADMIN);
        userCreateDto.setDistributorId(null);
        idDto.setId(editableUserService.createAndEnable(userLogin, userCreateDto).getId());
        assertHasBusinessException(() -> editableUserService.update(userLogin, idDto.getId(), userCreateDto));
        assertHasBusinessException(() -> editableUserService.delete(userLogin, idDto.getId()));

        // GET LIST
        assertThat(editableUserService.getList(userLogin, null, true, false, null, null, new PageSizeRequest(0, 10))
                .getList().size(), is(2));
        assertThat(editableUserService
                .getList(userLogin, userCreateDto.getFullname(), true, false, null, null, new PageSizeRequest(0, 10))
                .getList().size(), is(1));

        assertHasBusinessException(() -> editableUserService.setActive(userLogin, idDto.getId(), true));
        editableUserService.setActive(userLogin, idDto.getId(), false);
        assertThat(editableUserService.getList(userLogin, null, null, null, null, null, new PageSizeRequest(0, 10))
                .getList().size(), is(2));
        assertThat(editableUserService.getList(userLogin, null, true, null, null, null, new PageSizeRequest(0, 10))
                .getList().size(), is(1));

        // DISTRIBUTOR ADMIN
        userCreateDto.setUsername("dis");
        userCreateDto.setFullname("Distributor");
        userCreateDto.setRole(Role.DISTRIBUTOR_ADMIN);
        userCreateDto.setDistributorId(null);
        assertHasBusinessException(() -> editableUserService.create(userLogin, userCreateDto));
        userCreateDto.setDistributorId("wrongId");
        assertHasIdFormatException(() -> editableUserService.create(userLogin, userCreateDto));

        // SALESMAN
        userCreateDto.setUsername("sm");
        userCreateDto.setFullname("Salesman");
        userCreateDto.setRole(Role.SALESMAN);
        userCreateDto.setDistributorId(null);
        assertHasBusinessException(() -> editableUserService.create(userLogin, userCreateDto));
        userCreateDto.setDistributorId("wrongId");
        assertHasIdFormatException(() -> editableUserService.create(userLogin, userCreateDto));
    }

    @Test
    public void distributorService() {
        // CREATE ADMIN
        final UserCreateDto adminCreateDto = new UserCreateDto();
        adminCreateDto.setUsername("adminx");
        adminCreateDto.setFullname("Admin X");
        adminCreateDto.setRole(Role.ADMIN);
        IdDto adminIdDto = editableUserService.createAndEnable(userLogin, adminCreateDto);

        // CREATE SUPERVISOR
        final UserCreateDto supervisorCreateDto = new UserCreateDto();
        supervisorCreateDto.setUsername("sup");
        supervisorCreateDto.setFullname("Sup 2");
        supervisorCreateDto.setRole(Role.SUPERVISOR);
        IdDto supervisorIdDto = editableUserService.createAndEnable(userLogin, supervisorCreateDto);

        // TEST CREATE
        final DistributorCreateDto distributorCreateDto = new DistributorCreateDto();
        distributorCreateDto.setName("Distributor 1");
        assertHasBusinessException(() -> editableDistributorService.create(userLogin, distributorCreateDto));
        distributorCreateDto.setSupervisorId(adminIdDto.getId());
        assertHasBusinessException(() -> editableDistributorService.create(userLogin, distributorCreateDto));
        distributorCreateDto.setSupervisorId(supervisorIdDto.getId());
        IdDto idDto = editableDistributorService.create(userLogin, distributorCreateDto);
        assertNotNull(idDto);
        DistributorDto distributorDto = editableDistributorService.getById(userLogin, idDto.getId());

        assertNotNull(distributorDto);
        assertNotNull(distributorDto.getCode());
        assertThat(distributorDto.getId(), is(idDto.getId()));
        assertThat(distributorDto.getName(), is(distributorCreateDto.getName()));
        assertNotNull(distributorDto.getSupervisor());
        assertThat(distributorDto.getSupervisor().getId(), is(supervisorIdDto.getId()));
        assertHasBusinessException(() -> editableDistributorService.create(userLogin, distributorCreateDto));

        // TEST UPDATE
        distributorCreateDto.setName("Distributor 2");
        assertThat(editableDistributorService.update(userLogin, idDto.getId(), distributorCreateDto).getId(),
                is(idDto.getId()));
        distributorDto = editableDistributorService.getById(userLogin, idDto.getId());
        assertNotNull(distributorDto);
        assertThat(distributorDto.getId(), is(idDto.getId()));
        assertThat(distributorDto.getName(), is(distributorCreateDto.getName()));

        // TEST DELETE
        editableDistributorService.delete(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableDistributorService.getById(userLogin, idDto.getId()));

        // ENABLE
        distributorCreateDto.setName("Distributor 1");
        distributorCreateDto.setSupervisorId(supervisorIdDto.getId());
        idDto.setId(editableDistributorService.create(userLogin, distributorCreateDto).getId());
        editableDistributorService.enable(userLogin, idDto.getId());
        assertHasBusinessException(() -> editableDistributorService.delete(userLogin, idDto.getId()));

        distributorCreateDto.setName("Distributor 2");
        distributorCreateDto.setSupervisorId(supervisorIdDto.getId());
        idDto.setId(editableDistributorService.createAndEnable(userLogin, distributorCreateDto).getId());
        assertHasBusinessException(() -> editableDistributorService.delete(userLogin, idDto.getId()));

        // GET LIST
        assertThat(editableDistributorService.getList(userLogin, null, true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(2));
        assertThat(editableDistributorService
                .getList(userLogin, distributorCreateDto.getName(), true, false, null, new PageSizeRequest(0, 10))
                .getList().size(), is(1));
    }

}
