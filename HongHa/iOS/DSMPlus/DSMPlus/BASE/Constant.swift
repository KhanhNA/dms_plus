//
//  Constant.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/3/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

//mark: link url
//let urlSever = "http://cloud.dmsone.vn:6969/"
let urlSever = "http://192.168.1.34:8080/"

struct BASEURL {
    
    enum GET: String {
        
        case api_image = "api/image/"
        case oauth_userinfo = "oauth/userinfo"
        case dashboard = "api/salesman/dashboard"
        case dashboard_by_customer = "api/salesman/dashboard/by-customer"
        case dashboard_by_customerID = "api/salesman/dashboard/by-customer/detail?customerId="
        case dashboard_by_day = "api/salesman/dashboard/by-day"
        case dashboard_by_day_detail = "api/salesman/dashboard/by-day/detail?date="
        case visit_customer = "api/salesman/customer/for-visit?today=true"
        case visit_customerAll = "api/salesman/customer/for-visit"
        case visit_customerSumary = "api/salesman/customer/{0}/summary"
        case visit_customer_end = "api/salesman/visit/today?customerId={0}"
        case visit_customer_survey = "api/salesman/survey/available?customerId={0}"
        case order_list = "api/salesman/order/today"
        case order_list_product = "api/salesman/product/for-order"
        case order_promotion = "api/salesman/promotion/available"
        case order_info = "api/salesman/order/"
        case product_list = "api/salesman/product"
        case exchange_product = "api/salesman/exchange-product/today"
        case return_product = "api/salesman/return-product/today"
        case customer_register = "api/salesman/customer/register?page={0}&size={1}&q={2}"
        case exchange_product_detail = "api/salesman/exchange-product/"
        case return_product_detail = "api/salesman/return-product/"
        case customer_type = "api/salesman/customertype/all"
        case customer_area = "api/salesman/area/all"
        case list_supervisor = "api/supervisor/check-in?page={0}&size={1}"
        case check_in = "api/supervisor/check-in/{0}"
        func url() -> String {
            
            return "\(urlSever)\(self.rawValue)"
        }
        
    }
    
    enum POST: String {
        case oauth_token = "oauth/token?"
        case post_imag = ""
        case order_calculate = "api/salesman/order/calculate"
        case start_visit_customer = "api/salesman/visit/start?customerId={0}"
        case order_unplanned = "api/salesman/order/unplanned"
        case upload_image = "api/image"
        case return_product = "api/salesman/return-product"
        case exchange_product = "api/salesman/exchange-product"
        case create_new_customer = "api/salesman/customer/register"
        case check_in = "api/supervisor/check-in"
        case close_customer = "api/salesman/visit/close?customerId={0}"
        func url() -> String {
            return "\(urlSever)\(self.rawValue)"
        }
    }
    enum PUT: String {
        
        case visit_end = "api/salesman/visit/{0}/end"
        case update_mobile_customer = "api/salesman/customer/{0}/mobile"
        case change_password = "oauth/password"
        case update_location_customer = "api/salesman/customer/{0}/location"
        func url() -> String {
            return "\(urlSever)\(self.rawValue)"
        }
    }
    
    enum DELETE: String {
        case oauth_revoke = "oauth/revoke/"
        func url() -> String {
            return "\(urlSever)\(self.rawValue)"
        }
    }
}

//mark: Tham số luôn tồn tại trong suốt quá trình sử dụng app
var entityLogin: EntityLogin?
var entityUserinfo: EntityUserinfo?
var page_size: Int64 = 15
var ConfigSizeImage: CGFloat = 0.2
class Constant: NSObject {
    
}
