//
//  BaseEntity.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/3/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class BaseEntity: Mappable {
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    deinit {
        print("\(self) deinit")
    }
}

class DateTransformCustom: TransformType {
    
    public typealias Object = Date
    public typealias JSON = String
    
    public init() {}
    
    open func transformFromJSON(_ value: Any?) -> Date? {
        if let timeInt = value as? Double {
            return Date(timeIntervalSince1970: TimeInterval(timeInt))
        }
        
        if let timeStr = value as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            var dateObj = dateFormatter.date(from: timeStr)
            if dateObj == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd"
                dateObj = dateFormatter.date(from: timeStr)
            }
            return dateObj
        }
        
        return nil
    }
    
    open func transformToJSON(_ value: Date?) -> String? {
        if let date = value {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
            
            return dateFormatter.string(from: date)
        }
        return nil
    }
}
