//
//  Extension.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import Foundation

//mark: Check thiết bị là iphone hay ipad
extension UIDevice {
    class var isIphone: Bool {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return true
        // It's an iPhone
        default:
            return false
        }
    }
}

//mark: Màu của hệ thống.
extension UIColor {
    
    class var theme: UIColor {
        return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    
    class var tabbar: UIColor {
        return #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
    }
    
    class var navibar: UIColor {
        return #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
    }
    
    class var text: UIColor {
        return #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
    }
    
    class var success: UIColor {
        return #colorLiteral(red: 0.1215686275, green: 0.6941176471, blue: 0.5411764706, alpha: 1)
    }
    
    class var warning: UIColor {
        return #colorLiteral(red: 1, green: 0.5254901961, blue: 0, alpha: 1)
    }
    
    class var error: UIColor {
        return #colorLiteral(red: 1, green: 0.3568627451, blue: 0.2549019608, alpha: 1)
    }
    
    class var info: UIColor {
        return #colorLiteral(red: 0.2941176471, green: 0.4196078431, blue: 0.4784313725, alpha: 1)
    }
}
//mark: xoá khoảng trắng 2 đầu ký tự
extension String {
    func trimSpace () -> String {
        return  self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
    
    func toNumber() -> NSNumber? {
        let numberFormatter: NumberFormatter = {
            let formattedNumber = NumberFormatter()
            formattedNumber.locale = Locale(identifier: "vi_VN")
            formattedNumber.numberStyle = .decimal
            formattedNumber.maximumFractionDigits = 0
            return formattedNumber
        }()
        if let decimal = numberFormatter.number(from: self) {
            return decimal
        }
        return nil
    }
    
    func toDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        var dateObj = dateFormatter.date(from: self)
        if dateObj == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateObj = dateFormatter.date(from: self)
        }
        return dateObj
    }
    
    func toJson() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            if let json = json {
                return json
            }
        }
        return nil
    }
}

//mark: get hình ảnh là 1 dải màu
extension UIImage {
    class func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x:0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        //        UIGraphicsBeginImageContextWithOptions(rect, false, 0)
        color.setFill()
        UIRectFill(rect)
        //        let context = UIGraphicsGetCurrentContext()// Context()
        //        context.setFillcolor(color.cgColor)
        //        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    public convenience init(icon: String, size: CGSize, textColor: UIColor = UIColor.black, backgroundColor: UIColor = UIColor.clear) {
        
        let sizeScale = size//CGSize(width: size.width * UIScreen.main.scale, height: size.height * UIScreen.main.scale)
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = NSTextAlignment.center
        
        let fontAspectRatio: CGFloat = 1.28571429
        let fontSize = min(sizeScale.width / fontAspectRatio, sizeScale.height)
        let font = FontFamily.MaterialIcons.Regular.font(size: fontSize)
        let attributes = [NSAttributedString.Key.font: font!, NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.backgroundColor: backgroundColor, NSAttributedString.Key.paragraphStyle: paragraph]
        
        let attributedString = NSAttributedString(string: icon, attributes: attributes)
        UIGraphicsBeginImageContextWithOptions(sizeScale, false, 0.0)
        attributedString.draw(in: CGRect(x: 0, y: (sizeScale.height - fontSize) * 0.5, width: sizeScale.width, height: fontSize))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if let image = image {
            self.init(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)
        } else {
            self.init()
        }
    }
}

//mark: set UITabBarItem theo text
public extension UITabBarItem {
    func setIcon(_ icon: String) {
        image = UIImage(icon: icon, size: CGSize(width: 30, height: 30))
        selectedImage = UIImage(icon: icon, size: CGSize(width: 30, height: 30))
    }
}

public extension UIBarButtonItem {
    
    var icon: String? {
        set {
            let font = FontFamily.MaterialIcons.Regular.font(size: 23)
            setTitleTextAttributes([NSAttributedString.Key.font: font!], for: .normal)
            title = newValue
        }
        get {
            return title
        }
    }
}

//mark: get image và lưu cache ảnh.
import Kingfisher
extension UIImageView {
    func setImage_Url(_ string: String? = nil, placeholder: String? = "ic_user") -> Void {
        
        var url = URL(string: "\(BASEURL.GET.api_image.url())")
        
        if let string = string {
            url = URL(string: "\(BASEURL.GET.api_image.url())\(string)")
        }
        
        self.kf.setImage(with: url, placeholder: UIImage(named: placeholder!))
    }
}

//mark: chuyển đổi date thành string
enum DateFormat: String {
    case HHmmDDMMYYYY = "HH:mm dd/MM/yyyy"
    case DDMMYYYY = "dd/MM/yyyy"
    case YYYYMMDD = "yyyy-MM-dd"
    case HH_mm_DD_MM_YYYY = "HH_mm_dd_MM_yyyy"
}
extension Date {
    func toString(dateFormat: DateFormat? = .HHmmDDMMYYYY) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat?.rawValue
        return dateFormatter.string(from: self)
    }
}

//mark: Chuyển NSNumber thành kiểu string kiểu 1.000.000
extension NSNumber {
    func toDecimalString(_ isShow: Bool? = false) -> String {
        
        var string = ""
        var number = self
        if isShow == true && number.int64Value >= 1000000 {
            string = "M"
            number = NSNumber(value: Int64(number.int64Value/1000000))
        }
        
        let numberFormatter: NumberFormatter = {
            let formattedNumber = NumberFormatter()
            formattedNumber.locale = Locale(identifier: "vi_VN")
            formattedNumber.numberStyle = .decimal
            formattedNumber.maximumFractionDigits = 0
            return formattedNumber
        }()
        if let decimal = numberFormatter.string(from: number) {
            return "\(decimal)\(string)"
        }
        return ""
    }
}

//mark: NotificationName.
extension Notification.Name {
    static let addOder = Notification.Name("addOder")
}

import SVPullToRefresh
import DZNEmptyDataSet
extension UIScrollView {
    func infiniteScroll(_ actionHandler: @escaping ()->Swift.Void) {
        self.emptyDataSetSource = self
        self.emptyDataSetDelegate = self
        self.addInfiniteScrolling {
            isShowSVProgressHUD = false
            actionHandler()
        }
//        self.infiniteScrollingView.activityIndicatorView().color = UIColor.navibar
    }
    
    func stopAnimating() {
        isShowSVProgressHUD = true
        if let infiniteScrollingView = self.infiniteScrollingView {
            infiniteScrollingView.stopAnimating()
        }
        if let pullToRefreshView = self.pullToRefreshView {
            pullToRefreshView.stopAnimating()
        }
    }
    
    func pullToRefresh(_ actionHandler: @escaping ()->Swift.Void) {
        self.emptyDataSetSource = self
        self.emptyDataSetDelegate = self
        self.addPullToRefresh(actionHandler: {
            isShowSVProgressHUD = false
            actionHandler()
        })
//        self.pullToRefreshView.activityIndicatorView().color = UIColor.navibar
        self.pullToRefreshView.arrowColor = UIColor.navibar
        self.pullToRefreshView.textColor = UIColor.text
        self.pullToRefreshView.setTitle("Kéo để làm mới...".localized(), forState: 0)
        self.pullToRefreshView.setTitle("Thả để làm mới...".localized(), forState: 1)
        self.pullToRefreshView.setTitle("Đang tải...".localized(), forState: 2)
        self.triggerPullToRefresh()
    }
}

extension UIScrollView: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.text, NSAttributedString.Key.font: FontFamily.MaterialIcons.Regular.font(size: 30)]
        
        let partOne = NSMutableAttributedString(string: "do_not_disturb", attributes: attributes)
        return partOne
    }
    
    public func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.text, NSAttributedString.Key.font: FontFamily.NotoSans.Bold.font(size: 18)]
        let partOne = NSMutableAttributedString(string: "Không có dữ liệu".localized(), attributes: attributes)
        return partOne
    }
    
    public func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    public func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
}

public extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(vc: self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(vc: nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(vc: tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: pvc)
            } else {
                return vc
            }
        }
    }
}

import MMDrawerController
public extension UIViewController {
    func dismissToRootViewController() {
        let appDelegate  = UIApplication.shared.delegate as? AppDelegate
        if (appDelegate?.window?.visibleViewController as? MMDrawerController) != nil {
            
            print("MMDrawerController")
            
            return
        }
        
        print("visibleViewController")
        
        appDelegate?.window?.visibleViewController?.dismiss(animated: false) {
            self.dismissToRootViewController()
        }
    }
    
    func showLocationSetting(_ titleCanel: String? = "" ,_ completionHandler: @escaping (Bool?) -> Swift.Void) {
        let alertController = UIAlertController (title: "Thông báo".localized(), message: "DMS cần quyền truy cập vị trí của bạn, Vui lòng bật truy cập vị trí.".localized(), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Cài đặt".localized(), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        if success == true {
                            completionHandler(true)
                        }
                    })
                } else if #available(iOS 9.0, *) {
                    UIApplication.shared.openURL(URL(string: "prefs:root=LOCATION_SERVICES")!)
                    completionHandler(true)
                } else {
                    UIAlertView(title: "Thông báo".localized(), message: "Anh/chị vui lòng truy cập vào Cài đăt -> Quyền riêng tư -> Dịch vụ định vị, để bật định vị và cấp quyền truy cập cho ứng dụng".localized(), delegate: nil, cancelButtonTitle: "OK").show()
                }
            }
        }
        alertController.addAction(settingsAction)
        
        if let titleCanel = titleCanel {
            if titleCanel.isEmpty == false {
                let cancelAction = UIAlertAction(title: titleCanel, style: .cancel) { (_) -> Void in
                    completionHandler(false)
                }
                alertController.addAction(cancelAction)
            }
        }
        present(alertController, animated: true, completion: nil)
    }
}

extension UIImage {
    func resizeWith(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWith(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}

