//
//  DHToast.swift
//  FreeMusic
//
//  Created by Tom Jony on 8/8/16.
//  Copyright © 2016 Tom Jony. All rights reserved.
//

import UIKit
import ISMessages

class Toast: NSObject {
    class func show(_ string: String) {
        ISMessages.showCardAlert(withTitle: "Thông báo".localized(),
                                 message: string,
                                 duration: 3,
                                 hideOnSwipe: true,
                                 hideOnTap: true,
                                 alertType: ISAlertType.info,
                                 alertPosition: ISAlertPosition.top,
                                 didHide: nil)
    }
    
    class func showSuccess(_ string: String) {
        ISMessages.showCardAlert(withTitle: "Thành Công".localized(),
                                 message: string,
                                 duration: 3,
                                 hideOnSwipe: true,
                                 hideOnTap: true,
                                 alertType: ISAlertType.success,
                                 alertPosition: ISAlertPosition.top,
                                 didHide: nil)
    }
    
    class func showErr(_ string: String) {
        ISMessages.showCardAlert(withTitle: "Lỗi".localized(),
                                 message: string,
                                 duration: 3,
                                 hideOnSwipe: true,
                                 hideOnTap: true,
                                 alertType: ISAlertType.error,
                                 alertPosition: ISAlertPosition.top,
                                 didHide: nil)
    }
}
