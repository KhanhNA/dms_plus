//
//  UIBaseLabel.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/3/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UIBaseLabel: UILabel {
    
    @IBInspectable var sizeFont: CGFloat = 14 {
        didSet {
            if isBold {
                self.font = FontFamily.NotoSans.Bold.font(size: self.sizeFont)
            } else {
                self.font = FontFamily.NotoSans.Regular.font(size: self.sizeFont)
            }
        }
    }
    
    @IBInspectable var isBold: Bool = false {
        didSet {
            if isBold {
                self.font = FontFamily.NotoSans.Bold.font(size: self.sizeFont)
            } else {
                self.font = FontFamily.NotoSans.Regular.font(size: self.sizeFont)
            }
        }
    }
    
    @IBInspectable var colorText: UIColor = UIColor.text {
        didSet {
           self.textColor = colorText
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func setUp() {
        //self.font = FontFamily.NotoSans.Italic.font(size: nil)
    }

}
