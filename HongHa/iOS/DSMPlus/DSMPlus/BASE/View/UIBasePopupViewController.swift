//
//  UIBasePopupViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

class UIBasePopupViewController: UIBaseViewController, PopupContentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // PopupContentViewController Protocol
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        let height = UIScreen.main.bounds.size.height
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width - 40, height: max(300, height - 150))
    }

}

class UIBasePopupNaviViewController: UINaviBaseViewController, PopupContentViewController {
    // PopupContentViewController Protocol
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        let height = UIScreen.main.bounds.size.height
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width - 40, height: max(300, height - 150))
    }
}
