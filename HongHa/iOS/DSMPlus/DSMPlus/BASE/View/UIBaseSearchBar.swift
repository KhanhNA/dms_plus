//
//  UIBaseSearchBar.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/8/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UIBaseSearchBar: UISearchBar {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    init() {
        super.init(frame: CGRect.zero)
    }
    
    convenience init(placeholder: String) {
        self.init()
        setup()
    }
    
    private func setup() {
        self.barStyle = UIBarStyle.default
        self.keyboardAppearance = UIKeyboardAppearance.default
        self.barTintColor = UIColor.navibar
        self.tintColor = UIColor.white
        var textView = self.findTextField(self)
        if textView == nil {
            textView = self.value(forKey: "searchField") as? UITextField
        }
        textView?.tintColor = UIColor.navibar
        textView?.textColor = UIColor.text
        
        self.placeholder = "Tìm kiếm".localized()
        
        self.setValue("Huỷ".localized(), forKey: "_cancelButtonText")
    }
    
    fileprivate func findTextField(_ view: UIView) -> UITextField? {
        for subView in view.subviews {
            if let textView = subView as? UITextField {
                return textView
            } else if subView.subviews.count > 0 {
                return self.findTextField(subView)
            }
        }
        return nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
