//
//  UIBaseTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/16/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UIBaseTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
