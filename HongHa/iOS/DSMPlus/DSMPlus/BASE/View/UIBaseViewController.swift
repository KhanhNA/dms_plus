//
//  UIBaseViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/3/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class UIBaseViewController: UIViewController {
    
    let notificationName = Notification.Name("NotificationIdentifier")
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.setNaviColor(UIColor.navibar)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.view.backgroundColor = UIColor.theme
        let barItem = UIBarButtonItem()
        barItem.image = nil
        barItem.icon = "arrow_back"
        self.navigationItem.backBarButtonItem = barItem
        
        UINavigationBar.appearance().backIndicatorImage = UIImage()
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage()
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -20, vertical: 0), for: UIBarMetrics.default)
        
        self.navigationItem.rightBarButtonItems = naviMenuRight()
        naviMenuLeft()
        
        if let mm_drawerController = self.mm_drawerController {
            mm_drawerController.setGestureCompletionBlock { (drawerController, gestureRecognizer) in
                self.hidenKeyboard()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setLanguage()
    }
    
    func setNaviColor(_ color: UIColor) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.imageWithColor(color), for: UIBarMetrics.default)
    }
    
    func hidenKeyboard() {
        view.endEditing(true)
        hidenKeyboardSeachBar(self.view)
    }
    
    internal func hidenKeyboardSeachBar(_ vi: UIView) {
        for v in vi.subviews {
            if let seachBar = v as? UISearchBar {
                seachBar.resignFirstResponder()
            } else {
                hidenKeyboardSeachBar(v)
            }
        }
    }
    
    func setLanguage() {
        
    }
    
    func naviMenuRight() -> [UIBarButtonItem]? {
        return nil
    }
    
    func naviMenuLeft() {
        if let navigationController =  self.navigationController {
            if navigationController.viewControllers.count > 1 {
                return
            }
            let barButtonItem = UIBarButtonItem(title: "",
                                                style: UIBarButtonItem.Style.plain,
                                                target: self,
                                                action: #selector(UIBaseViewController.openMenuLeft))
            barButtonItem.icon = "menu"
            
            self.navigationItem.leftBarButtonItem = barButtonItem
        }
    }
    
     @objc func openMenuLeft() {
        self.mm_drawerController.toggle(.left, animated: true) {
            b in
            self.hidenKeyboard()
        }
    }
}

extension UIBaseViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
    }
}
