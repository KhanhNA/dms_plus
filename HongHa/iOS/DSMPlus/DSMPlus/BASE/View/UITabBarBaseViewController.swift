//
//  UITabBarBaseViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UITabBarBaseViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.imageWithColor(UIColor.navibar), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tabBar.barTintColor = UIColor.navibar
        self.tabBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        naviMenuLeft()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func naviMenuLeft() {
        if let navigationController =  self.navigationController {
            if navigationController.viewControllers.count > 1 {
                return
            }
            let barButtonItem = UIBarButtonItem(title: "",
                                                style: UIBarButtonItem.Style.plain,
                                                target: self,
                                                action: #selector(UIBaseViewController.openMenuLeft))
            barButtonItem.icon = "menu"
            
            self.navigationItem.leftBarButtonItem = barButtonItem
        }
    }
}
