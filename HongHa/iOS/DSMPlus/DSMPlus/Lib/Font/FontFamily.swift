//
//  Font.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/3/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

struct FontFamily {
    enum MaterialIcons: String {
        case Regular = "MaterialIcons-Regular"
        
        func font(size: CGFloat? = nil) -> UIFont? {
            if let size = size {
                if UIDevice.isIphone {
                    return UIFont(name:self.rawValue, size:size)
                }
                return UIFont(name:self.rawValue, size:size + 2)
            }
            var sizeFont: CGFloat = 18 //iPad
            if UIDevice.isIphone {
                sizeFont = 16
            }
            return UIFont(name:self.rawValue, size:sizeFont)
        }
    }
    enum NotoSans: String {
        case Regular = "NotoSans"
        case Bold = "NotoSans-Bold"
        case Italic = "NotoSans-Italic"
        case BoldItalic = "NotoSans-BoldItalic"
        
        func font(size: CGFloat? = nil) -> UIFont? {
            if let size = size {
                if UIDevice.isIphone {
                    return UIFont(name:self.rawValue, size:size)
                }
                return UIFont(name:self.rawValue, size:size + 2)
            }
            var sizeFont: CGFloat = 18 //iPad
            if UIDevice.isIphone {
                sizeFont = 16
            }
            return UIFont(name:self.rawValue, size:sizeFont)
        }
    }
}
