//
//  Networking.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/3/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import SVProgressHUD
import MMDrawerController

struct EntityResponse {
    var error: Error?
    var data: [String: Any?]?
}

var isShowSVProgressHUD = true

class Networking: NSObject {
    
    enum NSMethod: String {
        case NULL = ""
        case GET = "GET"
        case POST = "POST"
        case PUT = "PUT"
        case DELETE = "DELETE"
    }
    
    class func GETUrl(url: String,
                      paramater: [String: Any]? = nil,
                      isShowProgress: Bool? = isShowSVProgressHUD,
                      completionHandler: @escaping (EntityResponse?) -> Swift.Void) {
        var stringURL = url
        if let paramater = paramater {
            stringURL = "\(url)?\(paramater.stringFromHttpParameters())"
        }
        isShowSVProgressHUD = isShowProgress!
        send(url: stringURL,
             paramater: nil,
             method: .GET,
             completionHandler: completionHandler)
        
    }
    
    class func POSTUrl(url: String,
                       paramater: Data? = nil,
                       isShowProgress: Bool? = isShowSVProgressHUD,
                       completionHandler: @escaping (EntityResponse?) -> Swift.Void) {
        isShowSVProgressHUD = isShowProgress!
        send(url: url,
             paramater: paramater,
             method: .POST,
             completionHandler: completionHandler)
    }
    
    class func PUTUrl(url: String,
                      paramater: Data? = nil,
                      isShowProgress: Bool? = isShowSVProgressHUD,
                      completionHandler: @escaping (EntityResponse?) -> Swift.Void) {
        isShowSVProgressHUD = isShowProgress!
        send(url: url,
             paramater: paramater,
             method: .PUT,
             completionHandler: completionHandler)
    }
    
    class func DELETE(_ url: String,
                      paramater: Data? = nil,
                      isShowProgress: Bool? = isShowSVProgressHUD,
                      completionHandler: @escaping (EntityResponse?) -> Swift.Void) {
        isShowSVProgressHUD = isShowProgress!
        send(url: url,
             paramater: paramater,
             method: .DELETE,
             completionHandler: completionHandler)
    }
    
    fileprivate class func send(url: String,
                                paramater: Data? = nil,
                                method: NSMethod,
                                completionHandler: @escaping (EntityResponse?) -> Swift.Void) {
        if isShowSVProgressHUD {
            SVProgressHUD.show()
            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.setDefaultStyle(.light)
            SVProgressHUD.setForegroundColor(UIColor.navibar)
            SVProgressHUD.setBackgroundColor(UIColor.white)
        }
        
        let stringURL = url
        
        var request = URLRequest(url: URL(string: stringURL)!)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = paramater
        
        if let entityLogin = entityLogin {
            if let token_type = entityLogin.token_type,
                let access_token = entityLogin.access_token {
                request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: "Authorization")
            }
        }
        
        let session = URLSession.shared
        session.dataTask(with: request) {data, response, err in
            DispatchQueue.main.async() {
                SVProgressHUD.dismiss()
            }
            if let err = err {
                print(err.localizedDescription)
                DispatchQueue.main.async() {
                    Networking.showErrNetworking(err: err)
                    completionHandler(EntityResponse(error: err, data: nil))
                }
                return
            }
            
            if let data = data {
                print("data: \(NSString(data: data, encoding: String.Encoding.utf8.rawValue))")
                
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                DispatchQueue.main.async() {
                    if let json = json {
                        if troubleshooting(json) == true {
                            return
                        }
                        completionHandler(EntityResponse(error: nil, data: json))
                    } else {
                        completionHandler(nil)
                    }
                }
                return
            } else {
                DispatchQueue.main.async() {
                    completionHandler(nil)
                }
            }
            
            }.resume()
    }
    
    fileprivate class func troubleshooting(_ json: [String: Any]?) -> Bool {
        if let json = json {
            if (json["error"]) != nil {
                if (json["error_description"] as? String) != nil {
                    dismissViewController()
                    return true
                }
                return false
            }
        }
        return false
    }
    
    fileprivate class func dismissViewController() {
        entityLogin = nil
        entityUserinfo = nil
        let appDelegate  = UIApplication.shared.delegate as? AppDelegate
        if (appDelegate?.window?.visibleViewController as? LoginViewController) != nil {
            Toast.showErr("Phiên đăng nhập đã hết hạn. Vui lòng đăng nhập lại hệ thống".localized())
            return
        }
        appDelegate?.window?.visibleViewController?.dismiss(animated: false, completion: {
            dismissViewController()
        })
    }
    
    class func showErrNetworking(err: Error) {
        Toast.showErr(err.localizedDescription)
    }
}

extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            if let key = key as? String {
                let percentEscapedKey = key.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
                
                if let value = value as? String {
                    let percentEscapedValue = value.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
                    return "\(percentEscapedKey!)=\(percentEscapedValue!)"
                }
                return "\(percentEscapedKey!)=\(value)"
            }
            return ""
        }
        return parameterArray.joined(separator: "&")
    }
    
}
