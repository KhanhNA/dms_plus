//
//  ProgressUILabel.h
//  OfficeOneVNA
//
//  Created by Điển Đỗ Hữu on 11/6/14.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ProgressUILabel : UILabel{
    
}

@property (nonatomic, strong) UIColor *backgroundProgressColor;
@property (nonatomic, strong) UIColor *foregroundColor;

@property (nonatomic, strong) NSString *stLabel;

@property (nonatomic,readwrite) CGFloat progress;

@end
