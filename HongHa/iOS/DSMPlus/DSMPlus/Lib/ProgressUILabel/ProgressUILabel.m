//
//  ProgressUILabel.m
//  OfficeOneVNA
//
//  Created by Điển Đỗ Hữu on 11/6/14.
//
//

#import "ProgressUILabel.h"

@implementation ProgressUILabel

- (instancetype)init{
    self = [super init];
    if (self) {
//        if (IPhone) {
//            self.contentMode = UIViewContentModeRedraw;
//            UIColor *backgroundColor = [UIColor colorWithRed:10.0/255.0 green:70.0/255.0 blue:135.0/255.0 alpha:1.0];
//            self.layer.borderColor = backgroundColor.CGColor;
//            self.layer.borderWidth = 0.3;
//        }
        self.clipsToBounds = true;
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set up environment.
    CGSize size = [self bounds].size;
    if (_backgroundProgressColor == nil) {
        _backgroundProgressColor = [UIColor colorWithRed:10.0/255.0 green:70.0/255.0 blue:135.0/255.0 alpha:1.0];
    }
    if (_foregroundColor == nil) {
        _foregroundColor = [UIColor whiteColor];
    }
    UIFont *font = self.font;
    
    // Prepare progress as a string.
    if (_stLabel == nil) {
        _stLabel = @"";
    }
    NSString *progress = [NSString stringWithFormat:@"%d%% %@", (int)round([self progress] * 100),_stLabel];
    NSMutableDictionary *attributes = [@{ NSFontAttributeName : font } mutableCopy];
    CGSize textSize = [progress sizeWithAttributes:attributes];
    CGFloat progressX = ceil([self progress] * size.width);
    CGPoint textPoint = CGPointMake(ceil((size.width - textSize.width) / 2.0), ceil((size.height - textSize.height) / 2.0));
    
    // Draw background + foreground text
    [_backgroundProgressColor setFill];
    CGContextFillRect(context, CGRectMake(0, 0 , self.frame.size.width,  self.frame.size.height));
    attributes[NSForegroundColorAttributeName] = _foregroundColor;
    [progress drawAtPoint:textPoint withAttributes:attributes];
    
    // Clip the drawing that follows to the remaining progress' frame.
    CGContextSaveGState(context);
    CGRect remainingProgressRect = CGRectMake(progressX, 0.0, size.width - progressX, size.height);
    CGContextAddRect(context, remainingProgressRect);
    CGContextClip(context);
    
    // Draw again with inverted colors.
    [_foregroundColor setFill];
    CGContextFillRect(context, [self bounds]);
    attributes[NSForegroundColorAttributeName] = _backgroundProgressColor;
    [progress drawAtPoint:textPoint withAttributes:attributes];
    CGContextRestoreGState(context);
}

- (void)setStLabel:(NSString *)stLabel{
    _stLabel = stLabel;
    [self setNeedsDisplay];
    [self layoutIfNeeded];
}

- (void)setProgress:(CGFloat)progress {
    _progress = fminf(1.0, fmaxf(progress, 0.0));
    [self setNeedsDisplay];
    [self layoutIfNeeded];
}

@end
