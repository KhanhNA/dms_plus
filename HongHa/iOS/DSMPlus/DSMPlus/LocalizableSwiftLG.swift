//
//  LocalizableSwiftLG.swift
//  DSMPlus
//
//  Created by Tom Jony on 1/3/17.
//  Copyright © 2017 Viettel ICT. All rights reserved.
//

import UIKit

class LocalizableSwiftLG: NSObject {
    public class func valueKey() {
        let path = Bundle.main.path(forResource: "Localizable",
                                    ofType: "strings",
                                    inDirectory: nil,
                                    forLocalization: "en")
        let dic = NSDictionary(contentsOfFile: path!)
        
        let orderedKeysen = dic?.keysSortedByValue(using: #selector(NSString.caseInsensitiveCompare(_:)))
        
        let pathvn = Bundle.main.path(forResource: "Localizable",
                                    ofType: "strings",
                                    inDirectory: nil,
                                    forLocalization: "vi")
        let dicvn = NSDictionary(contentsOfFile: pathvn!)
        
        let orderedKeys = dicvn?.keysSortedByValue(using: #selector(NSString.caseInsensitiveCompare(_:)))
        
        var i = 0
        
        for item in orderedKeys! {
            let key = orderedKeysen?[i]
//            print(key)
            print("\"\(item as! String)\" = \"\(dic?.object(forKey: key ?? "") as! String)\";")
//            print("\"item\(i)\" = \"\(item as! String)\";")
            i = i + 1
        }
    }
}
