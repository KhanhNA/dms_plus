//
//  EntityChangeDelivery.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/19/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityChangeDelivery: BaseEntity {
    
    var id: String?
    var distributor: EntityDistributor?
    var customer: VisitCustomerEntity?
    var createdBy: EnityCreatedBy?
    var createdTime: Date?
    var quantity: NSNumber?
    var details: [[String: Any]]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        distributor <- map["distributor"]
        customer <- map["customer"]
        createdBy <- map["createdBy"]
        createdTime <- (map["createdTime"], DateTransformCustom())
        quantity <- map["quantity"]
        details <- map["details"]
    }
}
