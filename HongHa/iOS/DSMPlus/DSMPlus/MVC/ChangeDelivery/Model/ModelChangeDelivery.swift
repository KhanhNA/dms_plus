//
//  ModelChangeDelivery.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/19/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelChangeDelivery: BaseModel {
    
    var entityChangeDeliverys = [EntityChangeDelivery]()
    var entityChangeDelivery: EntityChangeDelivery?
    
    func GET(url: BASEURL.GET?, completion: @escaping ([EntityChangeDelivery]?) -> Swift.Void ) {
        if let url = url {
            Networking.GETUrl(url: url.url(), paramater: nil) {response in
                if let json = response?.data {
                    if let list = json["list"] as? [[String: Any]] {
                        self.entityChangeDeliverys.removeAll()
                        for item in list {
                            let entityChangeDelivery = EntityChangeDelivery(JSON: item)
                            self.entityChangeDeliverys.append(entityChangeDelivery!)
                        }
                    }
                }
                completion(self.entityChangeDeliverys)
            }
        }
    }
    
    func GET(_ url: String?, completion: @escaping (EntityChangeDelivery?) -> Swift.Void ) {
        if let url = url {
            Networking.GETUrl(url: url, paramater: nil) {response in
                if let json = response?.data {
                    self.entityChangeDelivery = EntityChangeDelivery(JSON: json)
                    completion(self.entityChangeDelivery)
                    return
                }
                completion(self.entityChangeDelivery)
            }
        }
    }
    
}
