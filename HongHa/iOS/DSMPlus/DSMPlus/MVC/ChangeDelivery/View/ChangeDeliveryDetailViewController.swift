//
//  ChangeDeliveryDetailViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ChangeDeliveryDetailViewController: UIBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var modelChangeDelivery = ModelChangeDelivery()
    var url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Chi tiết".localized()
        
        let cell = UINib(nibName: "CellOderTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "CellOderTableViewCell")
        let cell1 = UINib(nibName: "ChangeDeliveryTableViewCell", bundle: nil)
        tableView.register(cell1, forCellReuseIdentifier: "ChangeDeliveryTableViewCell")
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.pullToRefresh {
            self.modelChangeDelivery.GET(self.url, completion: { entityChangeDelivery in
                self.tableView.reloadData()
                self.tableView.stopAnimating()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension ChangeDeliveryDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 45
        }
        return 96
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if let details = modelChangeDelivery.entityChangeDelivery?.details {
                return details.count
            }
        } else if section == 1 {
            return 1
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellOderTableViewCell") as? CellOderTableViewCell {
                let json = modelChangeDelivery.entityChangeDelivery?.details?[indexPath.row]
                let entityOderSelect = EntityOderSelect(json: json!)
                cell.setData(entityOderSelect: entityOderSelect)
                return cell
            }
        } else if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeDeliveryTableViewCell") as? ChangeDeliveryTableViewCell {
                cell.setData(modelChangeDelivery.entityChangeDelivery?.quantity)
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
}
