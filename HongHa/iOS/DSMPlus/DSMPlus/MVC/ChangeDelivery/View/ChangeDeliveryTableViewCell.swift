//
//  TableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ChangeDeliveryTableViewCell: UIBaseTableViewCell {
    
    @IBOutlet weak var lbSumMoney: UIBaseLabel!
    @IBOutlet weak var lbSumMoneyN0: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lbSumMoney.textColor = #colorLiteral(red: 0.4235294118, green: 0.4823529412, blue: 0.5411764706, alpha: 1)
        lbSumMoneyN0.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        lbSumMoney.text = "Tổng số".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ sumMoney: NSNumber?) {
        if let sumMoney = sumMoney {
            lbSumMoneyN0.text = sumMoney.toDecimalString()
        } else {
            lbSumMoneyN0.text = ""
        }
    }
}
