//
//  ChangeDeliveryViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/19/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ChangeDeliveryViewController: UIBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UIBaseSearchBar!
    var array: [EntityChangeDelivery]?
    var url: BASEURL.GET!
    var modelChangeDelivery = ModelChangeDelivery()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if url == .exchange_product {
            self.title = "Đổi Hàng".localized()
        } else if url == .return_product {
            self.title = "Trả Hàng".localized()
        }
        
        let cell = UINib(nibName: "OderListTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "OderListTableViewCell")
        tableView.tableFooterView = UIView()
        
        tableView.pullToRefresh {
            self.modelChangeDelivery.GET(url: self.url) { (entityChangeDeliverys) in
                if entityChangeDeliverys != nil {
                    self.array = entityChangeDeliverys
                    self.tableView.reloadData()
                    self.tableView.stopAnimating()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func naviMenuRight() -> [UIBarButtonItem]? {
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(ChangeDeliveryViewController.addShoppingCart))
        barButtonItem.icon = "add_circle_outline"
        
        return [barButtonItem]
    }
}

extension ChangeDeliveryViewController {
    @objc func addShoppingCart() {
        let story = UIStoryboard(name: "OderList", bundle: nil)
        if let selectCustomerViewController = story.instantiateViewController(withIdentifier: "SelectCustomerViewController") as? SelectCustomerViewController {
            selectCustomerViewController.url = url
            self.navigationController?.pushViewController(selectCustomerViewController, animated: true)
        }
    }
}
extension ChangeDeliveryViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = self.array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "OderListTableViewCell") as? OderListTableViewCell {
            cell.setData((self.array?[indexPath.row])!, url: url)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entityListOrder = self.array?[indexPath.row]
        let changeDeliveryDetailViewController = ChangeDeliveryDetailViewController(nibName: "ChangeDeliveryDetailViewController", bundle: nil)
        if url == .exchange_product {
            if let id = entityListOrder?.id {
                changeDeliveryDetailViewController.url = "\(BASEURL.GET.exchange_product_detail.url())\(id)"
            }
        } else if url == .return_product {
            if let id = entityListOrder?.id {
                changeDeliveryDetailViewController.url = "\(BASEURL.GET.return_product_detail.url())\(id)"
            }
        }
        self.navigationController?.pushViewController(changeDeliveryDetailViewController, animated: true)
    }
}

extension ChangeDeliveryViewController {
    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let search = searchBar.text {
            if search.isEmpty {
                self.array = modelChangeDelivery.entityChangeDeliverys
                tableView.reloadData()
                return
            }
            self.array = modelChangeDelivery.entityChangeDeliverys.filter({ (entity) -> Bool in
                return (entity.customer?.name!.uppercased().contains(search.uppercased()))!
            })
            tableView.reloadData()
        } else {
            self.array = modelChangeDelivery.entityChangeDeliverys
            tableView.reloadData()
        }
    }
}
