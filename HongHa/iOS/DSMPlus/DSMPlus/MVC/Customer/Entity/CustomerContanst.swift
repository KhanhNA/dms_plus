//
//  CustomerContanst.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/17/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class CustomerContanst: NSObject {

    static var KEY_FEEDBACK = "KEY_FEEDBACK"
    static var KEY_ORDER = "KEY_ORDER"
    static var IMAGE_PATH = "IMAGE_PATH"
    static var LASR_DATE = "LASR_DATE"
}
