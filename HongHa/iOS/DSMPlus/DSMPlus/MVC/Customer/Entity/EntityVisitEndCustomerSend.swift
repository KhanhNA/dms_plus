//
//  EntityVisitEndCustomerSend.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class EntityVisitEndCustomerSend: BaseEntitySend {
    var exhibitionRatings: [String]?
    var feedbacks: [String]?
    var order: EntityCalculator?
    var photo: String?
    var surveyAnswers: [SurveyEntitySend]?

}

class LocationSend: BaseEntitySend {
    var latitude: Double?
    var longitude: Double?
}

class LocationVisitSend: BaseEntitySend {
    var location: LocationSend?
    
}

class SurveyEntitySend: BaseEntitySend {

    var options: [String]?
    var surveyId: String?
    
}
class MobileSen: BaseEntitySend {

    var content: String?
}
class CloseCustomerSend: BaseEntitySend {

    var location: LocationSend?
    var closingPhoto: String?
}
