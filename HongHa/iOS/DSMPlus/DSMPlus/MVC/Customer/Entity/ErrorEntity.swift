//
//  ErrorEntity.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/30/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class ErrorEntity: BaseEntity {

    var error_type: String?
    var code: Int?
    var error_message: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        error_type <- map["error_type"]
        code <- map["code"]
        error_message <- map["error_message"]
    }
}
