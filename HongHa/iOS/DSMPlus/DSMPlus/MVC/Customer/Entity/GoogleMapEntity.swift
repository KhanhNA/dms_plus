//
//  GoogleMapEntity.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/14/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class GoogleMapEntity: BaseEntity {
    var distance: Distance?
    var status: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        distance <- map["distance"]
        status <- map["status"]
    }
}
class Distance: BaseEntity {
    
    var text: String?
    var value: Double?
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        text <- map["text"]
        value <- map["value"]
    }
    
}
