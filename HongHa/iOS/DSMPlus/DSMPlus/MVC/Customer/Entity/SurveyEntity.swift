//
//  SurveyEntity.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class SurveyEntity: BaseEntity {
    ///keyyyyy
    public static var KEY_SURVEY = "KEY_SURVEY"
    var id: String?
    var draft: Bool?
    var active: Bool?
    var name: String?
    var code: String?
    var distributor: String?
    var startDate: Date?
    var endDate: Date?
    var required: Bool?
    var dateStatus: Int64?
    var questions: [Questions]?
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        draft <- map["draft"]
        active <- map["active"]
        name <- map["name"]
        code <- map["code"]
        distributor <- map["distributor"]
        startDate <- (map["startDate"], DateTransformCustom())
        endDate <- (map["endDate"], DateTransformCustom())
        required <- map["required"]
        dateStatus <- map["dateStatus"]
        questions <- map["questions"]
    }
    
}
class Questions: BaseEntity {
    
    var id: String?
    var name: String?
    var multipleChoice: Bool?
    var required: Bool?
    var options: [Options]?
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        multipleChoice <- map["multipleChoice"]
        required <- map["required"]
        options <- map["options"]
    }
}
class Options: BaseEntity {
    
    var id: String?
    var name: String?
    var select: Bool = false
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        name <- map["name"]
        id <- map["id"]
    }
}

class SurveyAnswers: BaseEntity {

    var surveyId: String?
    var options: [String]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        surveyId <- map["surveyId"]
        options <- map["options"]
    }
}

