//
//  VisitCustomerEntity.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class VisitCustomerEntity: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    var area: ClassArea?
    var customerType: ClassCustomerType?
    var planned: Bool?
    var visitStatus: Int64?
    var seqNo: Int?
    var visitInfo: VisitInfo?
    var location: ClassLocation?
    var phone: String?
    var mobile: String?
    var contact: String?
    var draft: Bool?
    var active: Bool?
    var status: Int64?
    var createdBy: CreatedBy?
    var distributor: Distributor?
    var createdTime: Date?
    var salesman: String?
    var email: String?
    var photo: String?
    var productivityLastMonth: Int64?
    var productivityThisMonth: Int64?
    var nbOrderThisMonth: Int64?
    var revenueLastThreeMonth: [RevenueLastThreeMonth]?
    var lastFiveOrders: [LastFiveOrders]?
    var feedbacks: [String]?
    var startTime: Date?
    var surveyAnswers: [SurveyAnswers]?
    var surveys: [SurveyEntity]?
    var withVisit: Bool?
    var grandTotal: NSNumber?
    var approveStatus: OrderStatus?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
        area <- map["area"]
        customerType <- map["customerType"]
        planned <- map["planned"]
        visitStatus <- map["visitStatus"]
        seqNo <- map["seqNo"]
        visitInfo <- map["visitInfo"]
        location <- map["location"]
        phone <- map["phone"]
        mobile <- map["mobile"]
        contact <- map["contact"]
        draft <- map["draft"]
        active <- map["active"]
        status <- map["status"]
        createdBy <- map["createdBy"]
        distributor <- map["distributor"]
        createdTime <- (map["createdTime"], DateTransformCustom())
        salesman <- map["salesman"]
        email <- map["email"]
        photo <- map["photo"]
        productivityLastMonth <- map["productivityLastMonth"]
        productivityThisMonth <- map["productivityThisMonth"]
        nbOrderThisMonth <- map["nbOrderThisMonth"]
        revenueLastThreeMonth <- map["revenueLastThreeMonth"]
        lastFiveOrders <- map["lastFiveOrders"]
        feedbacks <- map["feedbacks"]
        startTime <- (map["startTime"], DateTransformCustom())
        surveyAnswers <- map["surveyAnswers"]
        surveys <- map["surveys"]
        withVisit <- map["withVisit"]
        grandTotal <- map["grandTotal"]
        approveStatus <- map["approveStatus"]
    }
    
}
class ClassLocation: BaseEntity {
    
    var latitude: Double?
    var longitude: Double?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
class ClassArea: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
    }
    
}

class ClassCustomerType: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
    }
    
}

class Distributor: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
        
    }
}
class CreatedBy: BaseEntity {
    
    var id: String?
    var username: String?
    var usernameFull: String?
    var fullname: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        id <- map["id"]
        username <- map["username"]
        usernameFull <- map["usernameFull"]
        fullname <- map["fullname"]
    }
    
}
class RevenueLastThreeMonth: BaseEntity {
    
    var revenue: NSNumber?
    var month: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        revenue <- map["revenue"]
        month <- map["month"]
    }
    
}

class LastFiveOrders: BaseEntity {
    
    var date: Date?
    var total: NSNumber?
    var skuNumber: Int64?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        date <- (map["date"], DateTransformCustom())
        total <- map["total"]
        skuNumber <- map["skuNumber"]
    }
}
class VisitInfo: BaseEntity {
    
    var id: String?
    var hasOrder: Bool?
    var closed: Bool?
    var photo: String?
    var startTime: Date?
    var endTime: Date?
    var duration: Int64?
    var errorDuration: Bool?
    var locationStatus: Int64?
    var distance: Double?
    var grandTotal: NSNumber?
    var approveStatus: OrderStatus?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        
        id <- map["id"]
        hasOrder <- map["hasOrder"]
        closed <- map["closed"]
        photo <- map["photo"]
        startTime <- (map["startTime"], DateTransformCustom())
        endTime <- (map["endTime"], DateTransformCustom())
        duration <- map["duration"]
        errorDuration <- map["errorDuration"]
        locationStatus <- map["locationStatus"]
        distance <- map["distance"]
        grandTotal <- map["grandTotal"]
        approveStatus <- map["approveStatus"]
    }
}

