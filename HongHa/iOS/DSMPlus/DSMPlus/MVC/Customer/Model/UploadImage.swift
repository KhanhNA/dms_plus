//
//  UploadImage.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/19/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import SVProgressHUD

class UploadImage: NSObject {
    
    class func UploadRequest(image: UIImageView?, name: String?, completion: @escaping (String?) -> Swift.Void) {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setForegroundColor(UIColor.navibar)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        
        if let url = URL(string: BASEURL.POST.upload_image.url()) {
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = "POST"
            
            let boundary = generateBoundaryString()
            
            //define the multipart request type
            
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            if let entityLogin = entityLogin {
                if let token_type = entityLogin.token_type,
                    let access_token = entityLogin.access_token {
                    request.setValue("\(token_type) \(access_token)", forHTTPHeaderField: "Authorization")
                }
            }
            if image?.image == nil {
                Toast.show("Vui lòng chọn ảnh".localized())
                completion(nil)
            }
            
            let image_data = (image?.image!)!.jpegData(compressionQuality: ConfigSizeImage)
            if image_data == nil {
                Toast.show("Vui lòng chọn ảnh".localized())
                completion(nil)
            }
            let body = NSMutableData()
            let date = Date()
            let cal = Calendar.current
            let hour = cal.component(.hour, from: date)
            let minites = cal.component(.minute, from: date)
            let day = cal.component(.day, from: date)
            let month = cal.component(.month, from: date)
            let year = cal.component(.year, from: date)
            let fname = "\(name)_\(hour)_\(minites)_\(day)_\(month)_\(year).png"
            let mimetype = "image/png"
            
            //define the data post parameter
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"test\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("hi\r\n".data(using: String.Encoding.utf8)!)
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(image_data!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
            
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            
            request.httpBody = body as Data
            let session = URLSession.shared
            
            let task = session.dataTask(with: request as URLRequest) {
                (
                data, response, error) in
                DispatchQueue.main.async() {
                    SVProgressHUD.dismiss()
                }
                guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                    print("error")
                    return
                }
                
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print(dataString ?? "image nil")
                let json = try? JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                DispatchQueue.main.async() {
                    if let json = json {
                        if let id = json["id"] as? String {
                            completion(id)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    class func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}
