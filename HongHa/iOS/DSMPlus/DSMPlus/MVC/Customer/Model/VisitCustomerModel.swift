//
//  VisitCustomerModel.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import CoreLocation

class VisitCustomerModel: BaseModel {
    
    func getListVisitCustomer (completion: @escaping ([VisitCustomerEntity]?) -> Swift.Void) {
        
        let url = "\(BASEURL.GET.visit_customer.url())"
        Networking.GETUrl(url: url, paramater: nil) { response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    var listCustomers = [VisitCustomerEntity]()
                    for item in list {
                        let entityCustomer = VisitCustomerEntity(JSON: item)
                        listCustomers.append(entityCustomer!)
                    }
                    completion(listCustomers)
                    return
                }
            }
            completion(nil)
        }
        
    }
    
    func getListVisitCustomerAll (completion: @escaping ([VisitCustomerEntity]?) -> Swift.Void) {
        
        let url = "\(BASEURL.GET.visit_customerAll.url())"
        Networking.GETUrl(url: url, paramater: nil) { response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    var listCustomers = [VisitCustomerEntity]()
                    for item in list {
                        let entityCustomer = VisitCustomerEntity(JSON: item)
                        listCustomers.append(entityCustomer!)
                    }
                    completion(listCustomers)
                    return
                }
            }
            completion(nil)
        }
        
    }
    var visitCustomerEntity: VisitCustomerEntity?
    func getDetailCustomer ( cusid: String?, completion: @escaping (VisitCustomerEntity?) -> Swift.Void) {
        
        var url = "\(BASEURL.GET.visit_customerSumary.url())"
        url = url.replacingOccurrences(of: "{0}", with: cusid!)
        print(url)
        Networking.GETUrl(url: url, paramater: nil) { response in
            if let json = response?.data {
                if  let listCustomers = VisitCustomerEntity(JSON: json) {
                    self.visitCustomerEntity = listCustomers
                    completion(listCustomers)
                }
                return
            }
        }
    }
    
    var error_message: String?
    func endVisit ( cusid: String?, put: EntityVisitEndCustomerSend?, completion: @escaping (VisitCustomerEntity?) -> Swift.Void) {
        var url = "\(BASEURL.PUT.visit_end.url())"
        url = url.replacingOccurrences(of: "{0}", with: cusid!)
        Networking.PUTUrl(url: url, paramater: put?.toJSON()) { response in
            if let json = response?.data {
                if let meta = json["meta"] as? [String: Any] {
                    let error_entity = ErrorEntity(JSON: meta)
                    if error_entity?.code == 400 {
                        self.error_message = error_entity?.error_message
                        completion(nil)
                        return
                    }
                }
                if  let listCustomers = VisitCustomerEntity(JSON: json) {
                    self.visitCustomerEntity = listCustomers
                    completion(listCustomers)
                    return
                }
            }
        }
    }
    
    func getCutomerCustomerVisit (cusid: String?, completion: @escaping (VisitCustomerEntity?) -> Swift.Void) {
        var url = "\(BASEURL.GET.visit_customer_end.url())"
        url = url.replacingOccurrences(of: "{0}", with: cusid!)
        print(url)
        Networking.GETUrl(url: url, paramater: nil) { response in
            if let json = response?.data {
                if  let listCustomers = VisitCustomerEntity(JSON: json) {
                    self.visitCustomerEntity = listCustomers
                    completion(listCustomers)
                }
                return
            }
        }
    }
    
    func calculateDistance( originLat: Double, originLong: Double, destLat: Double, destLong: Double, completion: @escaping (CLLocationDistance) -> Swift.Void) {
        
        let newLocation = CLLocation(latitude: originLat, longitude: originLong)
        let oldLocation = CLLocation(latitude: destLat, longitude: destLong)
        let meters = newLocation.distance(from: oldLocation)
        completion(meters)
    }
    
    func startVisit (cusid: String?, latitude: Double?, longitude: Double?, completion: @escaping (String?) -> Swift.Void) {
        var url = "\(BASEURL.POST.start_visit_customer.url())"
        url = url.replacingOccurrences(of: "{0}", with: cusid!)
        let locationVisitSend = LocationVisitSend()
        
        if let lat = latitude,
            let lng = longitude {
            let localtion = LocationSend()
            localtion.latitude = lat
            localtion.longitude = lng
            locationVisitSend.location = localtion
        }
        Networking.POSTUrl(url: url, paramater: locationVisitSend.toJSON()) { response in
            if let json = response?.data {
                if  let id = json["id"] as? String {
                    print("id \(id)")
                    completion(id)
                }
                return
            }
        }
        
    }
    let usdfSurvey = UserDefaults.standard
    func getListSurvey(cusId: String?, completion: @escaping ([SurveyEntity]?) -> Swift.Void) {
        
        var url = "\(BASEURL.GET.visit_customer_survey.url())"
        url = url.replacingOccurrences(of: "{0}", with: cusId!)
        Networking.GETUrl(url: url, paramater: nil) {
            respone in
            if let json = respone?.data {
                
                if let ls = json["list"] as? [[String: Any]] {
                    
                    var lsSurvey = [SurveyEntity]()
                    var keysurvey = [String?]()
                    for item in ls {
                        let survey = SurveyEntity(JSON: item)
                        lsSurvey.append(survey!)
                        keysurvey.append(survey?.id)
                    }
                    self.usdfSurvey.set(keysurvey, forKey: SurveyEntity.KEY_SURVEY)
                    self.usdfSurvey.synchronize()
                    completion(lsSurvey)
                }
            }
            return
        }
    }
    
    func updateMobileCustomer(cusId: String?, mobile: String?, completion: @escaping (String) -> Swift.Void ) {
        
        var url = "\(BASEURL.PUT.update_mobile_customer.url())"
        url = url.replacingOccurrences(of: "{0}", with: cusId!)
        let send = MobileSen()
        send.content = mobile
        Networking.PUTUrl(url: url, paramater: send.toJSON()) {repone in
            if let rp = repone?.data {
                if let code = rp["code"] as? Int {
                    if code == 200 {
                        completion("YES")
                        return
                    }
                }
            }
            completion("NO")
        }
    }
    func closeCustomer(cusId: String?, param: CloseCustomerSend?, completion: @escaping(String?) -> Swift.Void) {
        
        var url = "\(BASEURL.POST.close_customer.url())"
        if let id = cusId {
            
            url = url.replacingOccurrences(of: "{0}", with: id)
            print(url)
            Networking.POSTUrl(url: url, paramater: param?.toJSON(), isShowProgress: true, completionHandler: {
                repone in
                if let rp = repone?.data {
                    
                    if rp["id"] != nil {
                        if let idEN = rp["id"] as? String {
                            completion(idEN)
                            return
                        }
                    } else {
                        Toast.showErr("Có lỗi xảy ra trong quá trình xử lý".localized())
                        completion(nil)
                        return
                    }
                }
                
            })
        } else {
            
            Toast.showErr("Có lỗi xảy ra trong quá trình xử lý".localized())
            return
        }
    }
    func updateLocationCustomer(cusId: String?, param: LocationSend?, completion: @escaping(Int) -> Swift.Void) {
    
        var url = "\(BASEURL.PUT.update_location_customer.url())"
        if let id = cusId {
        
            url = url.replacingOccurrences(of: "{0}", with: id)
            
        } else {
        
            Toast.showErr("Không xác định khách hàng".localized())
            return
        }
        if param == nil {
        
            Toast.showErr("Anh / chị chưa chọn vị trí khách hàng".localized())
            return
        }
        print(url)
        Networking.PUTUrl(url: url, paramater: param?.toJSON(), isShowProgress: true, completionHandler: {
        
            respone in
            if let json = respone?.data {
            
                if let code = json["code"] as? Int {
                
                    completion(code)
                    return
                }
            }
            completion(0)
            return
        })
    }
}
