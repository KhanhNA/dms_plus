//
//  ListProductViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ListProductViewController: UIBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UIBaseSearchBar!
    var array: [EntityProduct]?
    var entityProducts: [EntityProduct]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Danh sách sản phẩm".localized()
        
        let cell = UINib(nibName: "SelectProductTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "SelectProductTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        tableView.pullToRefresh { 
            self.getList()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getList() {
        
    }
}

extension ListProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = self.array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SelectProductTableViewCell") as? SelectProductTableViewCell {
            cell.setData(entityProduct:(self.array?[indexPath.row])!)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ListProductViewController {
    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let search = searchBar.text {
            if search.isEmpty {
                self.array = entityProducts
                tableView.reloadData()
                return
            }
            self.array = entityProducts?.filter({ (entity) -> Bool in
                return ((entity.name?.uppercased().contains(search.uppercased()))!
                    || (entity.code?.uppercased().contains(search.uppercased()))!)
            })
            tableView.reloadData()
        } else {
            self.array = entityProducts
            tableView.reloadData()
        }
    }
}
