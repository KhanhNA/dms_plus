//
//  SelectCustomerTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/8/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class SelectCustomerTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var lbName: UIBaseLabel!
    @IBOutlet weak var lbMobile: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(entityListOrder: VisitCustomerEntity) {
        lbName.text = entityListOrder.name
        lbMobile.text = entityListOrder.mobile
    }
}
