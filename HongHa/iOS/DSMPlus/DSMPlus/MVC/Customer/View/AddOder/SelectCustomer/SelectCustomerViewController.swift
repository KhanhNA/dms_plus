//
//  SelectCustomerViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/8/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class SelectCustomerViewController: UIBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var array: [VisitCustomerEntity]?
    let modelListCustomerOder = ModelListCustomerOder()
    var url: BASEURL.GET!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Chọn khách hàng".localized()
        
        let cell = UINib(nibName: "SelectCustomerTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "SelectCustomerTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.pullToRefresh {
            self.modelListCustomerOder.getList { (visitCustomerEntitys) in
                if visitCustomerEntitys != nil {
                    self.array = visitCustomerEntitys
                    self.tableView.reloadData()
                    self.tableView.stopAnimating()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension SelectCustomerViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = self.array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCustomerTableViewCell") as? SelectCustomerTableViewCell {
            cell.setData(entityListOrder: (self.array?[indexPath.row])!)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let tabBarBaseViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductTabarViewController") as? ProductTabarViewController {
            
            tabBarBaseViewController.url = url
            tabBarBaseViewController.modelSelectProductOrder.visitCustomerEntity = self.array?[indexPath.row]
            
            if entityUserinfo?.vanSales == true {
                if self.url != nil {
                    if self.url == .exchange_product ||
                        self.url == .return_product {
                        self.present(tabBarBaseViewController,
                                     animated: true, completion: nil)
                    }
                }
                
                let alert = UIAlertController(title: "Chọn hình thức bán hàng".localized(),
                                              message: nil,
                                              preferredStyle: .actionSheet)
                
                let alertExit = UIAlertAction(title: "Pre-Sale".localized(), style: .default) { (alertAction) in
                    self.present(tabBarBaseViewController, animated: true) {
                        if tabBarBaseViewController.entityCalculator == nil {
                            tabBarBaseViewController.entityCalculator = EntityCalculator()
                        }
                        tabBarBaseViewController.entityCalculator?.vanSales = false
                    }
                }
                
                let alertSend = UIAlertAction(title: "Van-Sale".localized(), style: .default) { (alertAction) in
                    self.present(tabBarBaseViewController, animated: true) {
                        if tabBarBaseViewController.entityCalculator == nil {
                            tabBarBaseViewController.entityCalculator = EntityCalculator()
                        }
                        tabBarBaseViewController.entityCalculator?.vanSales = true
                    }
                }
                
                let alertCancel = UIAlertAction(title: "Huỷ".localized(), style: .cancel)
                
                alert.addAction(alertExit)
                alert.addAction(alertSend)
                alert.addAction(alertCancel)
                
                self.present(alert, animated: true)
            } else {
                self.present(tabBarBaseViewController,
                             animated: true, completion: nil)
            }
            
        }
    }
}

extension SelectCustomerViewController {
    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let search = searchBar.text {
            if search.isEmpty {
                self.array = modelListCustomerOder.visitCustomerEntitys
                tableView.reloadData()
                return
            }
            self.array = modelListCustomerOder.visitCustomerEntitys.filter({ (entity) -> Bool in
                return (entity.name!.uppercased().contains(search.uppercased()) || (entity.mobile?.contains(search))!)
            })
            tableView.reloadData()
        } else {
            self.array = modelListCustomerOder.visitCustomerEntitys
            tableView.reloadData()
        }
    }
}
