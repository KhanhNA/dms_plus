//
//  CellOderTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class CellOderTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var codeProduct: UIBaseLabel!
    @IBOutlet weak var nameProduct: UIBaseLabel!
    @IBOutlet weak var unitProduct: UIBaseLabel!
    @IBOutlet weak var slProduct: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        nameProduct.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        codeProduct.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        unitProduct.textColor = #colorLiteral(red: 0.4235294118, green: 0.4823529412, blue: 0.5411764706, alpha: 1)
        slProduct.textColor = UIColor.navibar
        
        imageProduct.layer.cornerRadius = 8
        imageProduct.layer.borderWidth = 0.5
        imageProduct.layer.borderColor = codeProduct.textColor.cgColor
        
        self.textLabel?.font = FontFamily.MaterialIcons.Regular.font(size: 106)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(entityOderSelect: EntityOderSelect) {
        imageProduct.setImage_Url(entityOderSelect.entityProduct?.photo)
        codeProduct.text = entityOderSelect.entityProduct?.name
        nameProduct.text = entityOderSelect.entityProduct?.code
        
        if let price = entityOderSelect.entityProduct?.price,
            let name = entityOderSelect.entityProduct?.uom?.name {
            unitProduct.text = "\(price.toDecimalString())/\(name)"
        } else {
            unitProduct.text = ""
        }
        
        if let quantity = entityOderSelect.quantity {
            slProduct.text = NSNumber(value: quantity).toDecimalString()
        } else {
            slProduct.text = ""
        }
    }
    
}
