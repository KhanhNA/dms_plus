//
//  ChoseDateTimeViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

protocol ChoseDateTimeViewControllerDelegate: class {
    func didChose(date: Date)
}

class ChoseDateTimeViewController: UIBasePopupViewController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btChon: UIButton!
    
    weak var delegate: ChoseDateTimeViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.minimumDate = Date()
        btChon.setTitle("Chọn".localized(), for: .normal)
        
        btChon.setTitleColor(UIColor.white, for: .normal)
        btChon.backgroundColor = UIColor.navibar
        btChon.layer.cornerRadius = 5
        btChon.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width - 40, height: 350)
    }
    
    @IBAction func actionChose(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.didChose(date: datePicker.date)
        }
    }
}
