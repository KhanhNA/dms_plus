//
//  DeliveryFormViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

protocol DeliveryFormViewControllerDelegate: class {
    func didSelect(at: Int)
}

class DeliveryFormViewController: UIBasePopupViewController {
    
    @IBOutlet weak var lbTitle: UIBaseLabel!
    @IBOutlet weak var viewN0: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: DeliveryFormViewControllerDelegate?
    
    var array: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbTitle.text = "Hình thức giao hàng".localized()
        self.lbTitle.textColor = UIColor.navibar
        viewN0.layer.cornerRadius = 1.5
        viewN0.backgroundColor = UIColor.navibar
        
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        let height = UIScreen.main.bounds.size.height
        let width = UIScreen.main.bounds.size.width
        if let array = array {
            return CGSize(width: width - 40, height: min(CGFloat(array.count * 45 + 70), height - 150))
        }
        return CGSize(width: width - 40, height: height - 150)
    }
}

extension DeliveryFormViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CELLAAA") {
            cell.textLabel?.font = FontFamily.NotoSans.Regular.font()
            cell.textLabel?.text = array?[indexPath.row]
            return cell
        }
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "CELLAAA")
        
        cell.textLabel?.font = FontFamily.NotoSans.Regular.font()
        cell.textLabel?.text = array?[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = delegate {
            delegate.didSelect(at: indexPath.row)
        }
    }
}
