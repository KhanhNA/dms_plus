//
//  DeliveryViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/14/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

class DeliveryViewController: UIBaseViewController {

    @IBOutlet weak var layoutHeight: NSLayoutConstraint!
    @IBOutlet weak var lb1: UIBaseLabel!
    @IBOutlet weak var bt1: UIButton!
    @IBOutlet weak var lbv1: UILabel!
    
    @IBOutlet weak var lb2: UIBaseLabel!
    @IBOutlet weak var bt2: UIButton!
    @IBOutlet weak var lbv2: UILabel!
    
    @IBOutlet weak var lb3: UIBaseLabel!
    @IBOutlet weak var tv3: UITextView!
    
    weak var popupController: PopupController?
    
    var productTabarViewController: ProductTabarViewController? {
        return self.tabBarController as? ProductTabarViewController
    }
    
    fileprivate var arrayDeliveryFrom: [String] {
        return ["Giao hàng ngay".localized(),
                "Giao hàng trong ngày".localized(),
                "Giao hàng ngày".localized()]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Giao hàng".localized()
        didSelect(at: 0)
        self.bt2.setTitle("", for: .normal)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func naviMenuRight() -> [UIBarButtonItem]? {
        let navigate_next = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(DeliveryViewController.navigateNext))
        navigate_next.icon = "arrow_forward"
        
        return [navigate_next]
    }
    
    override func setLanguage() {
        self.lb1.text = "Hình thức giao hàng".localized()
        self.lb2.text = "Thời gian giao hàng".localized()
        self.lb3.text = "Ghi chú".localized()
        
        self.lbv1.layer.borderWidth = 0.5
        self.lbv1.layer.borderColor = #colorLiteral(red: 0.2991197109, green: 0.3839037418, blue: 0.4443931282, alpha: 1).cgColor
        self.lbv1.layer.cornerRadius = 8
        
        self.lbv2.layer.borderWidth = 0.5
        self.lbv2.layer.borderColor = self.lbv1.layer.borderColor
        self.lbv2.layer.cornerRadius = 8
        
        self.tv3.layer.borderWidth = 0.5
        self.tv3.layer.borderColor = self.lbv1.layer.borderColor
        self.tv3.layer.cornerRadius = 8
        
        tv3.text = ""
    }
    
    @IBAction func actionDeliveryFrom(_ sender: UIButton) {
        let deliveryFormViewController = DeliveryFormViewController(nibName: "DeliveryFormViewController", bundle: nil)
        deliveryFormViewController.array = arrayDeliveryFrom
        deliveryFormViewController.delegate = self
        popupController = PopupController.create(self).show(deliveryFormViewController)
    }
    
    @IBAction func actionChoseTime(_ sender: UIButton) {
        let choseDateTimeViewController = ChoseDateTimeViewController(nibName: "ChoseDateTimeViewController", bundle: nil)
        choseDateTimeViewController.delegate = self
        popupController = PopupController.create(self).show(choseDateTimeViewController)
    }
    
    @objc func navigateNext() {
        let infoOderViewController = InfoOderViewController(nibName: "InfoOderViewController", bundle: nil)
        self.navigationController?.pushViewController(infoOderViewController, animated: true)
    }
}

extension DeliveryViewController: DeliveryFormViewControllerDelegate {
    func didSelect(at: Int) {
        self.bt1.setTitle(arrayDeliveryFrom[at], for: .normal)
        
        if at != 2 {
            self.bt2.setTitle("", for: .normal)
            self.bt2.isEnabled = false
            self.lbv2.backgroundColor = #colorLiteral(red: 0.9376165271, green: 0.9376165271, blue: 0.9376165271, alpha: 1)
            productTabarViewController?.entityCalculator?.deliveryTime = nil
        } else {
            self.bt2.isEnabled = true
            self.lbv2.backgroundColor = UIColor.clear
            let date = Date()
            productTabarViewController?.entityCalculator?.deliveryTime = date
            self.bt2.setTitle(date.toString(dateFormat: .DDMMYYYY), for: .normal)
            productTabarViewController?.entityCalculator?.deliveryType = DeliveryType(rawValue: at)!
        }
        popupController?.dismiss()
    }
}

extension DeliveryViewController: ChoseDateTimeViewControllerDelegate {
    func didChose(date: Date) {
        productTabarViewController?.entityCalculator?.deliveryTime = date
        productTabarViewController?.entityCalculator?.deliveryType = .ANOTHER_DAY
        self.bt2.setTitle(date.toString(dateFormat: .DDMMYYYY), for: .normal)
        popupController?.dismiss()
    }
}
