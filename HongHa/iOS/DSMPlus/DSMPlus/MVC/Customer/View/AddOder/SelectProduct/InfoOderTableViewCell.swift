//
//  InfoOderTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/16/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

protocol InfoOderTableViewCellDelegate: NSObjectProtocol {
    func actionDiscount(_ number: NSNumber?)
}

class InfoOderTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var lbSumMoney: UIBaseLabel!
    @IBOutlet weak var lbSumMoneyN0: UIBaseLabel!
    @IBOutlet weak var lbPromotion: UIBaseLabel!
    @IBOutlet weak var lbPromotionN0: UIBaseLabel!
    @IBOutlet weak var lbDiscount: UIBaseLabel!
    @IBOutlet weak var lbDiscountN0: UIBaseLabel!
    @IBOutlet weak var btDiscount: UIButton!
    @IBOutlet weak var lbPay: UIBaseLabel!
    @IBOutlet weak var lbPayN0: UIBaseLabel!
    
    weak var delegate: InfoOderTableViewCellDelegate?
    
    var entityCalculator: EntityCalculator?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lbSumMoney.textColor = #colorLiteral(red: 0.4235294118, green: 0.4823529412, blue: 0.5411764706, alpha: 1)
        lbPromotion.textColor = lbSumMoney.textColor
        lbDiscount.textColor = lbSumMoney.textColor
        lbPay.textColor = lbSumMoney.textColor
        
        btDiscount.layer.borderWidth = 0.5
        btDiscount.layer.borderColor = #colorLiteral(red: 0.4235294118, green: 0.4823529412, blue: 0.5411764706, alpha: 1).cgColor
        btDiscount.layer.cornerRadius = 5
        btDiscount.clipsToBounds = true
        
        lbSumMoneyN0.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        lbPromotionN0.textColor = lbSumMoneyN0.textColor
        lbDiscountN0.textColor = lbSumMoneyN0.textColor
        lbPayN0.textColor = lbSumMoneyN0.textColor
        
        lbSumMoney.text = "Tổng tiền".localized()
        lbPromotion.text = "Khuyến mại".localized()
        lbDiscount.text = "Chiết khấu".localized()
        lbPay.text = "Thanh toán".localized()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ entityCalculator: EntityCalculator?) {
        lbSumMoneyN0.text = entityCalculator?.subTotal?.toDecimalString()
        lbPromotionN0.text = entityCalculator?.sumPromotion?.toDecimalString()
        lbDiscountN0.text = entityCalculator?.discountAmt?.toDecimalString()
        
        let sumMoneyN0 = entityCalculator?.subTotal ?? NSNumber(value: 0)
        let sumPromotion = entityCalculator?.sumPromotion ?? NSNumber(value: 0)
        let discountAmt = entityCalculator?.discountAmt ?? NSNumber(value: 0)
        lbPayN0.text = NSNumber(value: sumMoneyN0.int64Value - sumPromotion.int64Value - discountAmt.int64Value).toDecimalString()
    }
    
    @IBAction func actionDiscount(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.actionDiscount(lbDiscountN0.text?.toNumber())
        }
    }
}
