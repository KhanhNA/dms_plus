//
//  InfoOderViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController
import MMDrawerController

class InfoOderViewController: UIBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var productTabarViewController: ProductTabarViewController? {
        return self.tabBarController as? ProductTabarViewController
    }
    
    var entityCalculator: EntityCalculator?
    
    var modelInfoOder = ModelInfoOder()
    var idOder: String?
    var isEndVisit: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Thông tin đơn hàng".localized()
        
        let cell = UINib(nibName: "CellOderTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "CellOderTableViewCell")
        let cell1 = UINib(nibName: "InfoOderTableViewCell", bundle: nil)
        tableView.register(cell1, forCellReuseIdentifier: "InfoOderTableViewCell")
        tableView.allowsMultipleSelectionDuringEditing = false
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        
        entityCalculator = productTabarViewController?.entityCalculator
        
        if entityCalculator == nil {
            //xem chi tiết đơn hàng call Api
            getInfoOder()
        } else {
            entityCalculator?.discountAmt = nil
        }
        
        if let check =  isEndVisit {
            if check == true {
                let close = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(InfoOderViewController.dismisViewControler))
                close.icon = "clear"
                self.navigationItem.leftBarButtonItem = close
            }
        }
        
    }
    override func naviMenuRight() -> [UIBarButtonItem]? {
        if idOder != nil {
            return nil
        }
        let barItem = UIBarButtonItem(title: nil, style: UIBarButtonItem.Style.plain, target: self, action: #selector(InfoOderViewController.sendInfoOder))
        
        if productTabarViewController?.url == BASEURL.GET.visit_customerAll {
            barItem.icon = "send"
        } else if productTabarViewController?.delegateProduct != nil {
            barItem.icon = "save"
        } else {
            barItem.icon = "send"
        }
        return [barItem]
    }
    @objc func dismisViewControler() {
        
        self.dismiss(animated: true, completion: nil)
    }
    @objc func sendInfoOder() {
        let alert = UIAlertController(title: "Xác nhận đơn hàng".localized(),
                                      message: "Chắc chắn muốn đặt đơn hàng theo thông tin đơn hàng đã chọn?".localized(),
                                      preferredStyle: .alert)
        
        let alertExit = UIAlertAction(title: "Huỷ".localized(), style: .cancel)
        
        let alertSend = UIAlertAction(title: "Xác nhận".localized(), style: .destructive) { (alertAction) in
            
            if self.productTabarViewController?.url == BASEURL.GET.visit_customerAll {
                self.sendOder()
            } else  if let delegate = self.productTabarViewController?.delegateProduct {
                delegate.saveOder(self.productTabarViewController!, self.entityCalculator!)
            } else {
                self.sendOder()
            }
        }
        
        alert.addAction(alertExit)
        alert.addAction(alertSend)
        
        self.present(alert, animated: true)
    }
    
    fileprivate func sendOder() {
        self.modelInfoOder.POST(entityCalculator: self.entityCalculator!) {
            b in
            if b == true {
                Toast.showSuccess("Đặt đơn hàng thành công".localized())
                
                if self.productTabarViewController?.url == BASEURL.GET.visit_customerAll {
                    if let delegate = self.productTabarViewController?.delegateProduct {
                        delegate.saveOder(self.productTabarViewController!, self.entityCalculator!)
                    }
                    return
                } else {
                    let storyBoard = UIStoryboard(name: "OderList", bundle: nil)
                    let navi = self.productTabarViewController?.presentingViewController as? MMDrawerController
                    navi?.centerViewController = storyBoard.instantiateViewController(withIdentifier: "UINaviBaseViewController")
                    
                    self.productTabarViewController?.dismiss(animated: true, completion: {
                        
                    })
                }
            } else {
                Toast.showSuccess("Đặt đơn hàng không thành công".localized())
            }
        }
    }
}

extension InfoOderViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 150
        }
        return 96
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if let details = entityCalculator?.details {
                return details.count
            }
        } else if section == 1 {
            if let count = entityCalculator?.details?.count {
                if count > 0 {
                    return 1
                }
            }
            
        } else if section == 2 {
            if let promotions = entityCalculator?.promotions {
                return promotions.count
            }
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let promotions = entityCalculator?.promotions {
            if promotions.count > 0 {
                return 3
            }
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // Create label with section title
        let lb = UIBaseLabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.backgroundColor = UIColor.clear
        lb.textColor = UIColor.white
        lb.font = FontFamily.NotoSans.Bold.font(size: 14)
        lb.text = "Khuyến mại".localized()
        // Create header view and add label as a subview
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.navibar
        viewHeader.addSubview(lb)
        
        let topRowHorizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lb]-0-|",
                                                                         options: [],
                                                                         metrics: nil,
                                                                         views: ["lb":lb])
        viewHeader.addConstraints(topRowHorizontalConstraints)
        
        let topRowV = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[lb]-0-|",
            options: [],
            metrics: nil,
            views: ["lb":lb])
        viewHeader.addConstraints(topRowV)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellOderTableViewCell") as? CellOderTableViewCell {
                cell.setData(entityOderSelect: (entityCalculator?.details?[indexPath.row])!)
                return cell
            }
        } else if indexPath.section == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "InfoOderTableViewCell") as? InfoOderTableViewCell {
                cell.setData(entityCalculator)
                if idOder != nil {
                    cell.btDiscount.layer.borderColor = UIColor.clear.cgColor
                }
                cell.delegate = self
                return cell
            }
        } else if indexPath.section == 2 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "CellOderTableViewCell") as? CellOderTableViewCell {
                cell.setData(entityOderSelect: (entityCalculator?.promotions?[indexPath.row])!)
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
}

extension InfoOderViewController: InfoOderTableViewCellDelegate {
    func actionDiscount(_ number: NSNumber?) {
        hidenKeyboard()
        if idOder != nil {
            return
        }
        let inputNumberViewController = InputNumberViewController(nibName: "InputNumberViewController", bundle: nil)
        inputNumberViewController.delegate = self
        inputNumberViewController.popupController = PopupController.create(self.tabBarController!).show(inputNumberViewController)
        let sumMoneyN0 = entityCalculator?.subTotal ?? NSNumber(value: 0)
        let sumPromotion = entityCalculator?.sumPromotion ?? NSNumber(value: 0)
        inputNumberViewController.maxvalue = sumMoneyN0.int64Value - sumPromotion.int64Value
        if let number = number {
            inputNumberViewController.numberStr = number
        }
    }
}

extension InfoOderViewController: InputNumberViewControllerDelegate {
    func doneInput(inputNumber: InputNumberViewController, number: NSNumber) {
        entityCalculator?.discountAmt = number
        tableView.reloadData()
    }
}

extension InfoOderViewController {
    fileprivate func getInfoOder() {
        tableView.pullToRefresh {
            ModelInfoOder.GET(idOder: self.idOder!) { (entityCalculator) in
                self.entityCalculator = entityCalculator
                self.tableView.reloadData()
                self.tableView.stopAnimating()
            }
        }
    }
}
