//
//  ListSelectedOrderViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

class ListSelectedOrderViewController: UIBaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var entityOderSelect: EntityOderSelect?
    
    var productTabarViewController: ProductTabarViewController? {
        return self.tabBarController as? ProductTabarViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Chi tiết đơn hàng".localized()
        
        if productTabarViewController?.url == .exchange_product {
            self.title = "Đổi hàng".localized()
        } else if productTabarViewController?.url == .return_product {
            self.title = "Trả hàng".localized()
        }
        
        let cell = UINib(nibName: "CellOderTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "CellOderTableViewCell")
        tableView.allowsMultipleSelectionDuringEditing = false
//        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        productTabarViewController?.tabBar.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func naviMenuLeft() {
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(SelectProductOrderViewController.closeViewController))
        barButtonItem.icon = "close"
        
        self.navigationItem.leftBarButtonItem = barButtonItem
    }
    
    func closeViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func naviMenuRight() -> [UIBarButtonItem]? {
        
        if productTabarViewController?.url == .exchange_product ||
           productTabarViewController?.url == .return_product {
            let send = UIBarButtonItem(title: "",
                                       style: UIBarButtonItem.Style.plain,
                                       target: productTabarViewController,
                                       action: #selector(ProductTabarViewController.send))
            send.icon = "send"
            
            return [send]
        }
        
        let navigate_next = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: productTabarViewController,
                                            action: #selector(ProductTabarViewController.navigateNext))
        navigate_next.icon = "arrow_forward"
        
        if NSModule.PROMOTION.isShow {
            let loyalty = UIBarButtonItem(title: "",
                                          style: UIBarButtonItem.Style.plain,
                                          target: productTabarViewController,
                                          action: #selector(ProductTabarViewController.showPromotion))
            loyalty.icon = "loyalty"
            
            return [navigate_next, loyalty]
        }
        
        return [navigate_next]
    }
}

extension ListSelectedOrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = productTabarViewController?.array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9564377666, blue: 0.9564377666, alpha: 0.95)
        
        let lbSum = UIBaseLabel()
        let lbSum0 = UIBaseLabel()
        
        lbSum.textColor = UIColor.text
        lbSum0.textColor = UIColor.navibar
        lbSum0.isBold = true
        lbSum.text = "Tổng tiền:".localized()
        
        v.addSubview(lbSum)
        v.addSubview(lbSum0)
        
        lbSum0.translatesAutoresizingMaskIntoConstraints = false
        lbSum.translatesAutoresizingMaskIntoConstraints = false
        
        let views = ["lbSum0": lbSum0, "lbSum": lbSum]
        
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[lbSum]-8-|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: views)
        
        v.addConstraints(horizontalConstraints)
        
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[lbSum]-0-|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: views)
        v.addConstraints(verticalConstraints)
        
        let horizontalConstraints1 = NSLayoutConstraint.constraints(withVisualFormat: "H:[lbSum0]-8-|",
                                                                   options: [],
                                                                   metrics: nil,
                                                                   views: views)
        
        v.addConstraints(horizontalConstraints1)
        
        let verticalConstraints1 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[lbSum0]-0-|",
                                                                 options: [],
                                                                 metrics: nil,
                                                                 views: views)
        v.addConstraints(verticalConstraints1)
        
        productTabarViewController?.sumMoney()
        
        lbSum0.text = productTabarViewController?.entityCalculator?.subTotal?.toDecimalString()
        
        return v
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CellOderTableViewCell") as? CellOderTableViewCell {
            cell.setData(entityOderSelect: (productTabarViewController?.array?[indexPath.row])!)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .normal, title: "Sửa".localized()) { action, index in
            self.entityOderSelect = self.productTabarViewController?.array?[indexPath.row]
            let inputNumberViewController = InputNumberViewController(nibName: "InputNumberViewController", bundle: nil)
            inputNumberViewController.delegate = self
            inputNumberViewController.popupController = PopupController.create(self.tabBarController!).show(inputNumberViewController)
            inputNumberViewController.numberStr = NSNumber(value: (self.entityOderSelect?.quantity)!)
            self.tableView.reloadData()
        }
        let delete = UITableViewRowAction(style: .destructive, title: "Xoá".localized()) { action, index in
            self.entityOderSelect = self.productTabarViewController?.array?[indexPath.row]
            self.productTabarViewController?.removeOders(entityOderSelect: self.entityOderSelect)
            self.entityOderSelect = nil
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.tableView.reloadData()
        }
        return [delete, edit]
    }
    
    internal func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension ListSelectedOrderViewController: InputNumberViewControllerDelegate {
    func doneInput(inputNumber: InputNumberViewController, number: NSNumber) {
        if number.int64Value == 0 {
            entityOderSelect = nil
            return
        }
        entityOderSelect?.quantity = number.int64Value
        productTabarViewController?.addOrUpdateOder(entityOderSelect: entityOderSelect)
        entityOderSelect = nil
        tableView.reloadData()
    }
}
