//
//  ProductTabarViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController
import MMDrawerController

protocol ProductTabarViewControllerDelegate: NSObjectProtocol {
    func saveOder(_ productTabarViewController: ProductTabarViewController, _ entityCalculator: EntityCalculator)
}

class ProductTabarViewController: UITabBarBaseViewController {
    
    let modelSelectProductOrder = ModelSelectProductOrder()
    
    var array: [EntityOderSelect]?
    
    var entityCalculator: EntityCalculator?
    
    weak var delegateProduct: ProductTabarViewControllerDelegate?
    
    var url: BASEURL.GET!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if entityCalculator == nil {
            entityCalculator = EntityCalculator()
        } else {
            array = entityCalculator?.details
            setBage()
        }
        if let barItem = self.tabBar.items?[0] {
            barItem.title = "Chọn hàng".localized()
            barItem.setIcon("view_list")
        }
        
        if let barItem = self.tabBar.items?[1] {
            barItem.title = "Chi tiết đơn hàng".localized()
            
            if self.url != nil {
                if self.url == .exchange_product {
                    barItem.title = "Đổi hàng".localized()
                } else if self.url == .return_product {
                    barItem.title = "Trả hàng".localized()
                }
            }
            barItem.setIcon("shopping_basket")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setBage() {
        if let barItem = self.tabBar.items?[1] {
            if let array = array {
                if array.count == 0 {
                    barItem.badgeValue = nil
                    return
                }
                barItem.badgeValue = "\(array.count)"
            }
        }
    }
    
    @objc func navigateNext() {
        if array == nil || array?.count == 0 {
            Toast.showErr("Chưa chọn sản phẩm vào đơn hàng".localized())
            return
        }
        self.sumMoney()
        
        entityCalculator?.customerId = self.modelSelectProductOrder.visitCustomerEntity?.id
        entityCalculator?.details = array
        
        if NSModule.PROMOTION.isShow == false {
            //không có module khuyến mại
            //Bỏ qua màn hình km
            self.pushVC()
            return
        }
        modelSelectProductOrder.postList(promotion: entityCalculator!) {
            entityCalculatePromotions in
            
            if self.entityCalculator?.promotions != nil {
                self.entityCalculator?.promotions?.removeAll()
            }
            
            if let entity = entityCalculatePromotions {
                if entity.count > 0 {
                    //add vào mảng thông tin
                    for item in entity {
                        if let details = item.details {
                            for detail in details {
                                if let reward = detail.reward {
                                    if let product = reward.product {
                                        if self.entityCalculator?.promotions == nil {
                                            self.entityCalculator?.promotions = [EntityOderSelect]()
                                        }
                                        let entityOderSelect = EntityOderSelect()
                                        entityOderSelect.productId = product.id
                                        entityOderSelect.quantity = reward.quantity?.int64Value
                                        entityOderSelect.entityProduct = product
                                        self.entityCalculator?.promotions?.append(entityOderSelect)
                                    } else if let amount = reward.amount {
                                        if let sumPromotion = self.entityCalculator?.sumPromotion {
                                            self.entityCalculator?.sumPromotion = NSNumber(value:sumPromotion.int64Value + amount.int64Value)
                                        } else {
                                            self.entityCalculator?.sumPromotion = amount
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    //vào hiển thị thông tin màn hình km
                    let promotionVC = UIPromotionViewController(nibName: "UIPromotionViewController", bundle: nil)
                    promotionVC.entityCalculatePromotions = entity
                    
                    if let navi = self.viewControllers?[self.selectedIndex] as? UINaviBaseViewController {
                        navi.pushViewController(promotionVC, animated: true)
                        self.tabBar.isHidden = true
                    }
                    return
                }
            }
            
            //Bỏ qua màn hình km
            self.pushVC()
        }
    }
    
    ///bỏ qua màn hình khuyến mại
    fileprivate func pushVC() {
        //bỏ qua màn hình khuyến mại
        if self.entityCalculator?.vanSales == true {
            //bỏ qua màn hình giao hàng
            let infoOderViewController = InfoOderViewController(nibName: "InfoOderViewController", bundle: nil)
            if let navi = self.viewControllers?[self.selectedIndex] as? UINaviBaseViewController {
                navi.pushViewController(infoOderViewController, animated: true)
                self.tabBar.isHidden = true
            }
        } else {
            let deliveryViewController = DeliveryViewController(nibName: "DeliveryViewController", bundle: nil)
            if let navi = self.viewControllers?[self.selectedIndex] as? UINaviBaseViewController {
                navi.pushViewController(deliveryViewController, animated: true)
                self.tabBar.isHidden = true
            }
        }
    }
    @objc  
    func showPromotion() {
        let promotionViewController = PromotionViewController(nibName: "PromotionViewController", bundle: nil)
        promotionViewController.isNaviMenuLeft = false
        let navi = UIBasePopupNaviViewController()
        navi.viewControllers = [promotionViewController]
        let _ = PopupController.create(self).show(navi)
    }
    
    @objc func send() {
        if array == nil || array?.count == 0 {
            Toast.showErr("Chưa chọn sản phẩm".localized())
            return
        }
        self.sumMoney()
        let alert = UIAlertController(title: "Xác nhận đơn hàng".localized(),
                                      message: nil,
                                      preferredStyle: .alert)
        
        let alertExit = UIAlertAction(title: "Huỷ".localized(), style: .cancel)
        
        let alertSend = UIAlertAction(title: "Xác nhận".localized(), style: .destructive) { (alertAction) in
            let entityChageDelivery = EntityChageDelivery()
            entityChageDelivery.customerId = self.modelSelectProductOrder.visitCustomerEntity?.id
            entityChageDelivery.details = self.array
            
            for item in self.array! {
                if let quantity = item.quantity {
                    if entityChageDelivery.total == nil {
                        entityChageDelivery.total = quantity
                    } else {
                        entityChageDelivery.total = entityChageDelivery.total! + quantity
                    }
                }
            }
            
            if self.url == .exchange_product {
                self.modelSelectProductOrder.postExchange(entityChageDelivery, completion: { (b) in
                    if b == true {
                        Toast.showSuccess("Đổi hàng thành công".localized())
                        
                        let storyBoard = UIStoryboard(name: "ChangeDelivery", bundle: nil)
                        let changeDeliveryViewController = storyBoard.instantiateViewController(withIdentifier: "ChangeDeliveryViewController") as? ChangeDeliveryViewController
                        changeDeliveryViewController?.url = BASEURL.GET.exchange_product
                        let navi =  UINaviBaseViewController()
                        navi.viewControllers = [changeDeliveryViewController!]
                        
                        let drawerController = self.presentingViewController as? MMDrawerController
                        drawerController?.centerViewController = navi
                        
                        self.dismiss(animated: true, completion: {
                            
                        })
                    } else {
                        Toast.showErr("Đổi hàng không thành công".localized())
                    }
                })
            } else if self.url == .return_product {
                self.modelSelectProductOrder.postReturn(entityChageDelivery, completion: { (b) in
                    if b == true {
                        Toast.showSuccess("Trả hàng thành công".localized())
                        let storyBoard = UIStoryboard(name: "ChangeDelivery", bundle: nil)
                        let changeDeliveryViewController = storyBoard.instantiateViewController(withIdentifier: "ChangeDeliveryViewController") as? ChangeDeliveryViewController
                        changeDeliveryViewController?.url = BASEURL.GET.return_product
                        let navi =  UINaviBaseViewController()
                        navi.viewControllers = [changeDeliveryViewController!]
                        
                        let drawerController = self.presentingViewController as? MMDrawerController
                        drawerController?.centerViewController = navi
                        
                        self.dismiss(animated: true, completion: {
                            
                        })
                    } else {
                        Toast.showErr("Trả hàng không thành công".localized())
                    }
                })
            }
        }
        
        alert.addAction(alertExit)
        alert.addAction(alertSend)
        
        self.present(alert, animated: true)
    }
}

//mark: func tính tổng tiền
extension ProductTabarViewController {
    func sumMoney() {
        var sum: Int64 = 0
        if let array = self.array {
            for entityOderSelect in array {
                if let quantity = entityOderSelect.quantity,
                    let price = entityOderSelect.entityProduct?.price {
                    sum = sum + quantity * price.int64Value
                }
            }
        }
        self.entityCalculator?.subTotal = NSNumber(value: sum)
    }
}

//mark: func đặt hàng
extension ProductTabarViewController {
    func removeOders(entityOderSelect: EntityOderSelect?) {
        if let entityOderSelect = entityOderSelect {
            if let array = array {
                var idx = 0
                for u in array {
                    if entityOderSelect.productId == u.productId {
                        self.array?.remove(at: idx)
                        setBage()
                        return
                    }
                    idx = idx + 1
                }
            }
        }
        
    }
    
    func addOrUpdateOder(entityOderSelect: EntityOderSelect?) {
        if let entityOderSelect = entityOderSelect {
            if array == nil {
                array = [EntityOderSelect]()
            }
            if let array = array {
                for u in array {
                    if entityOderSelect.productId == u.productId {
                        //update
                        u.quantity = entityOderSelect.quantity
                        return
                    }
                }
            }
            array?.append(entityOderSelect)
        }
        setBage()
    }
}
