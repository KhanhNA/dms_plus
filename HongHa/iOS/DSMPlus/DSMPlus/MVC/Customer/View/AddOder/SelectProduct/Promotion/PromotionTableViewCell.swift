//
//  PromotionTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class PromotionTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var lbName: UIBaseLabel!
    @IBOutlet weak var lbTime: UIBaseLabel!
    @IBOutlet weak var lbTimeValue: UIBaseLabel!
    @IBOutlet weak var lbApplyFor: UIBaseLabel!
    @IBOutlet weak var lbApplyForValue: UIBaseLabel!
    @IBOutlet weak var lbDescription: UIBaseLabel!
    @IBOutlet weak var lbDescriptionValue: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lbName.textColor = UIColor.navibar
        lbTime.text = "Thời gian:".localized()
        lbTime.textColor = #colorLiteral(red: 0.4078193307, green: 0.4078193307, blue: 0.4078193307, alpha: 1)
        lbApplyFor.text = "Đối tượng:".localized()
        lbApplyFor.textColor = lbTime.textColor
        lbDescription.text = "Cơ cấu:".localized()
        lbDescription.textColor = lbTime.textColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ entity: EntityPromotion) {
        lbName.text = entity.name
        
        var strStartDate = ""
        if let startDate = entity.startDate {
            strStartDate = "\("Từ ngày".localized()) \(startDate.toString(dateFormat: .DDMMYYYY)!) "
        }
        
        var strendDate = ""
        if let endDate = entity.endDate {
            strendDate = "\("đến ngày".localized()) \(endDate.toString(dateFormat: .DDMMYYYY)!)"
        }
        
        lbTimeValue.text =  "\(strStartDate)\(strendDate)"
        lbApplyForValue.text = entity.applyFor
        lbDescriptionValue.text = entity.description
        
        self.layoutIfNeeded()
    }
    
}
