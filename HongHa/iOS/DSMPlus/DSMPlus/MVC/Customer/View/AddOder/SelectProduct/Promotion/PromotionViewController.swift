//
//  PromotionViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class PromotionViewController: UIBasePopupViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let modelProduct = ModelProduct()
    var isNaviMenuLeft = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Chương trình khuyến mại".localized()
        
        let cell = UINib(nibName: "PromotionTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "PromotionTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.pullToRefresh { 
            self.modelProduct.getList { (entityPromotions) in
                self.tableView.reloadData()
                self.tableView.stopAnimating()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func naviMenuLeft() {
        if isNaviMenuLeft == false {
            return
        }
        super.naviMenuLeft()
    }

}

extension PromotionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.modelProduct.entityPromotions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PromotionTableViewCell") as? PromotionTableViewCell {
            cell.setData(self.modelProduct.entityPromotions[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
}
