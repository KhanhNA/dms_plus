//
//  UIPromotionTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/14/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UIPromotionTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var lbCode: UIBaseLabel!
    @IBOutlet weak var lbdis: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lbCode.textColor = #colorLiteral(red: 0.4078193307, green: 0.4078193307, blue: 0.4078193307, alpha: 1)
        lbdis.textColor = UIColor.black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ entityListPromotion: EntityListPromotion) {
        lbCode.text = entityListPromotion.conditionProductName
        lbdis.text = ""
        if let reward = entityListPromotion.reward {
            if let amount = reward.amount {
                //hiển thị giá.
                lbdis.text = amount.toDecimalString()
            } else if let product = reward.product,
                let quantity = reward.quantity {
                lbdis.text = "\(quantity.toDecimalString()) \(product.uom?.name ?? "") x \(product.name ?? "")"
            }
        }
    }
    
}
