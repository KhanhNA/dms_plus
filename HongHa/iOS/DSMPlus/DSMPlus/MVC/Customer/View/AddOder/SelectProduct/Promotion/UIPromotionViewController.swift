//
//  UIPromotionViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/14/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UIPromotionViewController: UIBaseViewController {
    
    var entityCalculatePromotions: [EntityCalculatePromotion]?
    
    var productTabarViewController: ProductTabarViewController? {
        return self.tabBarController as? ProductTabarViewController
    }
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Khuyến mại".localized()
        
        let cell = UINib(nibName: "UIPromotionTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "UIPromotionTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func naviMenuRight() -> [UIBarButtonItem]? {
        let navigate_next = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(UIPromotionViewController.navigateNext))
        navigate_next.icon = "arrow_forward"
        
        return [navigate_next]
    }
    
    @objc func navigateNext() {
        
        if productTabarViewController?.entityCalculator?.vanSales == true {
            //bỏ qua màn hình khuyến mại
            let infoOderViewController = InfoOderViewController(nibName: "InfoOderViewController", bundle: nil)
            self.navigationController?.pushViewController(infoOderViewController, animated: true)
            return
        }
        
        let deliveryViewController = DeliveryViewController(nibName: "DeliveryViewController", bundle: nil)
        self.navigationController?.pushViewController(deliveryViewController, animated: true)
    }
    
}

extension UIPromotionViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let array = entityCalculatePromotions {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = entityCalculatePromotions {
            return array[section].details?.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let array = entityCalculatePromotions {
            
            // Create label with section title
            let lb = UIBaseLabel()
            lb.translatesAutoresizingMaskIntoConstraints = false
            lb.backgroundColor = UIColor.clear
            lb.textColor = UIColor.navibar
            lb.numberOfLines = 0
            lb.font = FontFamily.NotoSans.Bold.font(size: 14)
            
            lb.text = array[section].name
            // Create header view and add label as a subview
            let viewHeader = UIView()
            viewHeader.backgroundColor = #colorLiteral(red: 0.9441131949, green: 0.9441131949, blue: 0.9441131949, alpha: 1)
            viewHeader.addSubview(lb)
            
            let topRowHorizontalConstraints = NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-10-[lb]-0-|",
                options: [],
                metrics: nil,
                views: ["lb":lb])
            viewHeader.addConstraints(topRowHorizontalConstraints)
            
            let topRowV = NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-0-[lb]-0-|",
                options: [],
                metrics: nil,
                views: ["lb":lb])
            viewHeader.addConstraints(topRowV)
            return viewHeader
            
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "UIPromotionTableViewCell") as? UIPromotionTableViewCell {
            cell.setData((entityCalculatePromotions?[indexPath.section].details?[indexPath.row])!)
            return cell
        }
        return UITableViewCell()
    }
}
