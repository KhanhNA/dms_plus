//
//  SelectProductOrderViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

class SelectProductOrderViewController: ListProductViewController {
    
    weak var entityProduct: EntityProduct?
    
    var productTabarViewController: ProductTabarViewController? {
        return self.tabBarController as? ProductTabarViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Chọn hàng".localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        productTabarViewController?.tabBar.isHidden = false
    }
    
    override func getList() {
        self.productTabarViewController?.modelSelectProductOrder.getList { (entityProducts) in
            if entityProducts != nil {
                self.entityProducts = entityProducts
                self.array = entityProducts
                self.tableView.reloadData()
                self.tableView.stopAnimating()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func naviMenuLeft() {
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(SelectProductOrderViewController.closeViewController))
        barButtonItem.icon = "close"
        
        self.navigationItem.leftBarButtonItem = barButtonItem
    }
    
    @objc func closeViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func naviMenuRight() -> [UIBarButtonItem]? {
        if productTabarViewController?.url == .exchange_product ||
            productTabarViewController?.url == .return_product {
            let send = UIBarButtonItem(title: "",
                                       style: UIBarButtonItem.Style.plain,
                                       target: productTabarViewController,
                                       action: #selector(ProductTabarViewController.send))
            send.icon = "send"
            
            return [send]
        }
        let navigate_next = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: productTabarViewController,
                                            action: #selector(ProductTabarViewController.navigateNext))
        navigate_next.icon = "arrow_forward"
        
        if NSModule.PROMOTION.isShow {
            let loyalty = UIBarButtonItem(title: "",
                                          style: UIBarButtonItem.Style.plain,
                                          target: productTabarViewController,
                                          action: #selector(ProductTabarViewController.showPromotion))
            loyalty.icon = "loyalty"
            
            return [navigate_next, loyalty]
        }
        return [navigate_next]
    }
    
}

extension SelectProductOrderViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        hidenKeyboard()
        
        entityProduct = self.array?[indexPath.row]
        let inputNumberViewController = InputNumberViewController(nibName: "InputNumberViewController", bundle: nil)
        inputNumberViewController.delegate = self
        inputNumberViewController.popupController = PopupController.create(self.tabBarController!).show(inputNumberViewController)
    }
}

extension SelectProductOrderViewController: InputNumberViewControllerDelegate {
    func doneInput(inputNumber: InputNumberViewController, number: NSNumber) {
        if number.int64Value == 0 {
            entityProduct = nil
            return
        }
        let entity = EntityOderSelect()
        entity.productId = entityProduct?.id
        entity.quantity = number.int64Value
        entity.entityProduct = entityProduct
        productTabarViewController?.addOrUpdateOder(entityOderSelect: entity)
        entityProduct = nil
    }
}

