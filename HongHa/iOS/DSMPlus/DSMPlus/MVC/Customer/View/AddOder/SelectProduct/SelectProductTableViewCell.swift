//
//  SelectProductTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class SelectProductTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var codeProduct: UIBaseLabel!
    @IBOutlet weak var nameProduct: UIBaseLabel!
    @IBOutlet weak var unitProduct: UIBaseLabel!
    
    @IBOutlet weak var lbAvailable: UIBaseLabel!
    @IBOutlet weak var lbAvailable0: UIBaseLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameProduct.textColor = UIColor.navibar
        codeProduct.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        unitProduct.textColor = #colorLiteral(red: 0.4235294118, green: 0.4823529412, blue: 0.5411764706, alpha: 1)
        
        imageProduct.layer.cornerRadius = 8
        imageProduct.layer.borderWidth = 0.5
        imageProduct.layer.borderColor = codeProduct.textColor.cgColor
        
        if NSModule.DISTRIBUTOR_PRICE_LIST.isShow {
            lbAvailable.isHidden = false
            lbAvailable.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
            lbAvailable.font = FontFamily.MaterialIcons.Regular.font()
            lbAvailable.text = "event_available"
            
            lbAvailable0.textColor = #colorLiteral(red: 0.4235294118, green: 0.4823529412, blue: 0.5411764706, alpha: 1)
            lbAvailable0.isHidden = false
        } else {
            lbAvailable.isHidden = true
            lbAvailable0.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(entityProduct: EntityProduct) {
        imageProduct.setImage_Url(entityProduct.photo)
        codeProduct.text = entityProduct.name
        nameProduct.text = entityProduct.code
        
        if let price = entityProduct.price,
            let name = entityProduct.uom?.name {
            unitProduct.text = "\(price.toDecimalString())/\(name)"
        } else {
            unitProduct.text = ""
        }
        if let availableQuantity = entityProduct.availableQuantity {
            lbAvailable0.text = availableQuantity.toDecimalString()
        } else {
             lbAvailable0.text = "N/A".localized()
        }
       
    }

}
