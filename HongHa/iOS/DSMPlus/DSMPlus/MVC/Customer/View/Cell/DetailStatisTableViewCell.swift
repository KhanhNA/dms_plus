//
//  DetailStatisTableViewCell.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DetailStatisTableViewCell: UIBaseTableViewCell {
    @IBOutlet weak var lblValue: UIBaseLabel!

    @IBOutlet weak var lblTitle: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblValue.textColor = UIColor.navibar
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValue( name: String?, value: String?) {
        lblTitle.text = name
        lblValue.text = value
    }
}
