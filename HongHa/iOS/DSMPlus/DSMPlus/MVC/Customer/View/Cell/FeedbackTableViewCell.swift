//
//  FeedbackTableViewCell.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class FeedbackTableViewCell: UIBaseTableViewCell {
    
    @IBOutlet weak var viewcontent: UIView!
    @IBOutlet weak var lblTime: UIBaseLabel!
    @IBOutlet weak var lblContent: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewcontent.layer.cornerRadius = 10
        lblContent.sizeToFit()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setValue( content: String?, time: String?) {
        
        lblContent.text = content
        lblContent.textColor = UIColor.navibar
        if time != nil {
            lblTime.text = time
        } else {
            
            lblTime.isHidden = true
        }
    }
    
}
