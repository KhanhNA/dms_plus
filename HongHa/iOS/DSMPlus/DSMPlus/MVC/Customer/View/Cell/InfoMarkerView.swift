//
//  InfoMarkerView.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/28/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class InfoMarkerView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSDT: UILabel!
    @IBOutlet weak var btnViewInfo: UIButton!
    @IBAction func actionViewInfo(_ sender: Any) {
        print("xem chi tiet")
    }
    @IBOutlet weak var btnVisit: UIButton!
    @IBAction func actionVisit(_ sender: Any) {
        print("Ghe tham")
    }

}
