//
//  QuestionSurveyTableViewCell.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class QuestionSurveyTableViewCell: UIBaseTableViewCell {
    @IBOutlet weak var lblCheck: UILabel!
    
    @IBOutlet weak var lblQuestion: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadQuestion(questions: Options, multi: Bool, arrayCheck: [String?], surveyId: String?, answers: [SurveyAnswers]?) {
        
        lblCheck.textColor = UIColor.navibar
        if multi {
            lblCheck.text = "check_box_outline_blank"
        } else {
            lblCheck.text = "radio_button_unchecked"
        }
        
        if answers != nil {
            for answer in answers! {
                
                for question in answer.options! {
                    
                    if questions.id == question {
                        
                        if multi {
                            lblCheck.text = "check_box"
                        } else {
                            lblCheck.text = "radio_button_checked"
                        }
                    }
                }
            }
        } else {
            
            if questions.select {
                
                if multi {
                    lblCheck.text = "check_box"
                } else {
                    lblCheck.text = "radio_button_checked"
                }
                
            } else {
                
                if multi {
                    lblCheck.text = "check_box_outline_blank"
                } else {
                    lblCheck.text = "radio_button_unchecked"
                }
                
            }
        }
        lblQuestion.text = questions.name
        lblQuestion.textColor = UIColor.navibar
    }
}
