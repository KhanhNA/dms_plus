//
//  SurveyTableViewCell.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class SurveyTableViewCell: UIBaseTableViewCell {
    
    @IBOutlet weak var lblNameSurvey: UIBaseLabel!
    
    @IBOutlet weak var lblTime: UIBaseLabel!
    
    @IBOutlet weak var lblCountQuestion: UIBaseLabel!

    @IBOutlet weak var lblStatus: UIBaseLabel!
    @IBOutlet weak var iconNote: UILabel!
    @IBOutlet weak var iconStatus: UILabel!
    
    @IBOutlet weak var iconTimer: UILabel!
    let saveSurvey = UserDefaults.standard
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setData(name: String, time: String, count: Int?, status: Int64?, surveyId: String?) {
        
        lblNameSurvey.text = name
        lblNameSurvey.textColor = UIColor.navibar
        lblStatus.textColor = UIColor.navibar
        iconNote.textColor = UIColor.navibar
        iconTimer.textColor = UIColor.navibar
        iconStatus.textColor = UIColor.navibar
        lblTime.text = time
        if let total = count {
            lblCountQuestion.text = "\(total)"
        } else {
            lblCountQuestion.text = "0"
        }
        
        if let data = saveSurvey.value(forKey: surveyId!) {
            
            let arr = data as? [String]
            if (arr?.count)! > 0 {
                lblStatus.text = "Đang trả lời".localized()
            }
            
        } else {
            
            if let stt = status {
                if stt == 1 {
                    lblStatus.text = "Chưa trả lời".localized()
                } else {
                    lblStatus.text = "Đã trả lời".localized()
                }
                
            }
        }
    }
    
    func dataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
}
