//
//  VisitCustomerTableViewCell.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class VisitCustomerTableViewCell: UIBaseTableViewCell {
    
    @IBOutlet weak var lblStatus: UIBaseLabel!
    @IBOutlet weak var lblSDT: UIBaseLabel!
    @IBOutlet weak var lbDate: UIBaseLabel!
    @IBOutlet weak var lbName: UIBaseLabel!
    @IBOutlet weak var lbColor: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setData(entity: VisitCustomerEntity?) {
        lblStatus.text = ""
        if let name = entity?.name {
            lbName.text = name
            
        }
        
        if let mobile = entity?.mobile {
            lblSDT.text = mobile
        }
        if entity?.visitStatus == 1 {
            lblStatus.text = "Chưa ghé thăm".localized()
            lblStatus.colorText = UIColor.navibar
            lbName.textColor = UIColor.navibar
            lbColor.textColor = UIColor.navibar
            lbDate.textColor = UIColor.navibar
            lblSDT.textColor = UIColor.navibar
        } else {
            lblStatus.text = "Đã ghé thăm".localized()
            lblStatus.colorText = UIColor.blue
            lbName.textColor = UIColor.text
            lbColor.textColor = UIColor.text
            lbDate.textColor = UIColor.text
            lblSDT.textColor = UIColor.text
        }
    }
    func setDataAll(entity: VisitCustomerEntity?) {
        lblStatus.text = ""
        lblStatus.colorText = UIColor.navibar
        lbName.textColor = UIColor.navibar
        lbColor.textColor = UIColor.navibar
        lbDate.textColor = UIColor.navibar
        lblSDT.textColor = UIColor.navibar
        if let name = entity?.name {
            lbName.text = name
            
        }
        
        if let mobile = entity?.mobile {
            lblSDT.text = mobile
        }
    }
    
}
