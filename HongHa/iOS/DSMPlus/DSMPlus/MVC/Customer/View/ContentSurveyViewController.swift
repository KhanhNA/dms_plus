//
//  ContentSurveyViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ContentSurveyViewController: UIBaseViewController {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var viewcontent: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    var arrayChoice = [String]()
    let saveSurvey = UserDefaults.standard
    var arraySelect = [String?]()
    var navi: NaviContentSurveyViewController? {
        
        return self.navigationController as? NaviContentSurveyViewController
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let buttonClose = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ContentSurveyViewController.dismis))
        buttonClose.icon  = "clear"
        self.navigationItem.leftBarButtonItem = buttonClose
        if navi?.isEndVisit == nil || navi?.isEndVisit == false {
            
            let buttonacept = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ContentSurveyViewController.accept))
            buttonacept.icon = "check"
            self.navigationItem.rightBarButtonItem = buttonacept
        }
        
        self.navigationItem.title = navi?.survey?.name
        
        if let arselect = saveSurvey.value(forKey: (navi?.survey?.id)!) as? [String?] {
            
            arraySelect = arselect
        }
        
        let nib = UINib(nibName: "QuestionSurveyTableViewCell", bundle: nil)
        tableview.register(nib, forCellReuseIdentifier: "cell")
        tableview.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func dismis() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func accept() {
        
        var options = [String]()
        for question in (navi?.survey?.questions)! {
            
            for option in question.options! {
                
                if option.select {
                    
                    options.append(option.id!)
                    
                }
            }
        }
        
        if options.count > 0 {
            saveSurvey.removeSuite(named: (navi?.survey?.id)!)
            saveSurvey.set(options, forKey: (navi?.survey?.id)!)
            saveSurvey.synchronize()
        } else {
            saveSurvey.removeSuite(named: (navi?.survey?.id)!)
        }
        self.dismiss(animated: true, completion: nil)
    }
    override func naviMenuLeft() {
        
    }
}
extension ContentSurveyViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (navi!.survey?.questions?.count)!
    }
    
    func tableView(_
        tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (navi!.survey?.questions?[section].options!.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // Create label with section title
        let lb = UIBaseLabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.backgroundColor = UIColor.clear
        lb.textColor = UIColor.white
        lb.font = FontFamily.NotoSans.Bold.font(size: 15)
        if let content = navi?.survey?.questions?[section].name {
            lb.text = "\(section + 1). \(content)"
        }
        lb.numberOfLines = 0
        // Create header view and add label as a subview
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.navibar
        viewHeader.addSubview(lb)
        
        let topRowHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[lb]-0-|",
            options: [],
            metrics: nil,
            views: ["lb":lb])
        viewHeader.addConstraints(topRowHorizontalConstraints)
        
        let topRowV = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[lb]-0-|",
            options: [],
            metrics: nil,
            views: ["lb":lb])
        viewHeader.addConstraints(topRowV)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? QuestionSurveyTableViewCell {
            
            cell.loadQuestion(questions: (navi?.survey?.questions?[indexPath.section].options?[indexPath.row])!, multi: (navi?.survey?.questions?[indexPath.section].multipleChoice)!, arrayCheck: arraySelect, surveyId: navi?.survey?.id, answers: navi?.listAnswer)
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let en = navi?.survey?.questions {
            
            let aaa = en[indexPath.section]
            if aaa.multipleChoice == true {
                let bbb = aaa.options?[indexPath.row]
                bbb?.select = !(bbb?.select)!
            } else {
                for item in aaa.options! {
                    item.select = false
                }
                let bbb = aaa.options?[indexPath.row]
                bbb?.select = true
            }
            tableView.reloadSections([indexPath.section], with: UITableView.RowAnimation.fade)
        }
    }
}
