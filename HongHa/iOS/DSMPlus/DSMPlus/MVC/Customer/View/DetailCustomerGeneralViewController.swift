//
//  DetailCustomerViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DetailCustomerGeneralViewController: UIBaseViewController {
    
    @IBOutlet weak var lblName: UIBaseLabel!
    
    @IBOutlet weak var lblLoai: UIBaseLabel!
    
    @IBOutlet weak var lblDiaChi: UIBaseLabel!
    
    @IBOutlet weak var lblMobile: UITextField!
    
    @IBOutlet weak var lblPhone: UITextField!
    
    @IBOutlet weak var lblEmail: UIBaseLabel!
    
    @IBOutlet weak var lblContac: UIBaseLabel!
    @IBOutlet weak var btnEditPhone1: UIButton!
    
    @IBOutlet weak var btnEditPhone2: UIButton!
    
    @IBOutlet weak var btnEditMobile1: UIButton!
    
    @IBOutlet weak var btnEditMobile2: UIButton!
    
    var tabBarDetailCustomerViewController: UITabBarDetailCustomerViewController? {
        return self.tabBarController as? UITabBarDetailCustomerViewController
    }
    @IBOutlet weak var txtMobile: UITextField!
    
    @IBOutlet weak var viewcontact: UIView!
    @IBOutlet weak var viewemail: UIView!
    @IBOutlet weak var viewmobile: UIView!
    @IBOutlet weak var viewphone: UIView!
    @IBOutlet weak var viewaddress: UIView!
    @IBOutlet weak var viewtype: UIView!
    @IBOutlet weak var viewname: UIView!
    
    @IBOutlet weak var heightContantsName: NSLayoutConstraint!
    @IBOutlet weak var heightContantsType: NSLayoutConstraint!
    @IBOutlet weak var heightContantsAddress: NSLayoutConstraint!
    @IBOutlet weak var heightContantsPhone: NSLayoutConstraint!
    @IBOutlet weak var heightContantsMobile: NSLayoutConstraint!
    @IBOutlet weak var heightContantsEmail: NSLayoutConstraint!
    @IBOutlet weak var heightContantsContact: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(DetailCustomerGeneralViewController.dismis))
        barButtonItem.icon = "clear"
        
        self.navigationItem.leftBarButtonItem = barButtonItem
        self.navigationItem.title = "TỔNG HỢP".localized()
        
        btnEditPhone1.setTitleColor(UIColor.navibar, for: .normal)
        btnEditPhone2.setTitleColor(UIColor.navibar, for: .normal)
        btnEditMobile1.setTitleColor(UIColor.navibar, for: .normal)
        btnEditMobile2.setTitleColor(UIColor.navibar, for: .normal)
        
        setInfo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismis() {
        self.dismiss(animated: true, completion:nil)
    }
    
    override func naviMenuLeft() {
        
    }
    
    func setInfo () {
        
        if let customer = tabBarDetailCustomerViewController?.entity {
            
            tabBarDetailCustomerViewController?.model.getDetailCustomer(cusid: customer.id, completion: {
                entity in
                if let info = entity {
                    self.lblName.text = info.name
                    if info.status != nil {
                        self.lblLoai.text =  info.customerType?.name
                    }
                    if info.email == nil || info.email == "" {
                        
                        self.heightContantsEmail.constant = 0
                        self.viewemail.isHidden = true
                        
                    } else {
                        
                        self.lblEmail.text = info.email
                        
                    }
                    // self.lblPhone.text = info.phone
                    self.lblMobile.text = info.mobile
                    self.lblContac.text = info.contact
                    self.lblDiaChi.text = info.name
                    self.heightContantsPhone.constant = 0
                    self.viewphone.isHidden = true
                }
                
            })
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var boolEditmobile2: Bool = false
    @IBAction func actionEditMobile1(_ sender: Any) {
        
        if boolEditmobile2 {
            
            if (txtMobile.text?.isEmpty)! {
                Toast.showErr("Bạn cần nhập số điện thoại".localized())
                return
            }
            
            tabBarDetailCustomerViewController?.model.updateMobileCustomer(cusId: tabBarDetailCustomerViewController?.entity?.id, mobile: txtMobile.text, completion: {
                
                respone in
                print(respone)
                if respone == "YES" {
                    
                    Toast.showSuccess("Cập nhập số điện thoại khách hàng thành công".localized())
                    self.btnEditMobile1.setTitle("mode_edit", for: .normal)
                    self.btnEditMobile2.setTitle("stay_current_portrait", for: .normal)
                    self.lblMobile.isEnabled = false
                    self.lblMobile.borderStyle = UITextField.BorderStyle.none
                    self.boolEditmobile2 = false
                    
                } else {
                    
                    Toast.showErr("Cập nhập số điện thoại không thành công".localized())
                }
            })
        } else {
            
            btnEditMobile1.setTitle("done", for: .normal)
            btnEditMobile2.setTitle("clear", for: .normal)
            lblMobile.isEnabled = true
            lblMobile.becomeFirstResponder()
            lblMobile.borderStyle = UITextField.BorderStyle.roundedRect
            boolEditmobile2 = true
        }
    }
    var boolEditmobile: Bool = false
    @IBAction func actionEditPhone1(_ sender: Any) {
        
        if boolEditmobile {
            
            btnEditPhone1.setTitle("mode_edit", for: .normal)
            btnEditPhone2.setTitle("local_phone", for: .normal)
            lblPhone.isEnabled = false
            lblPhone.borderStyle = UITextField.BorderStyle.none
            boolEditmobile = false
            
        } else {
            
            btnEditPhone1.setTitle("done", for: .normal)
            btnEditPhone2.setTitle("clear", for: .normal)
            lblPhone.isEnabled = true
            lblPhone.becomeFirstResponder()
            lblPhone.borderStyle = UITextField.BorderStyle.roundedRect
            boolEditmobile = true
        }
        
    }
    @IBAction func cancelEditphone2(_ sender: Any) {
        
        if boolEditmobile {
            
            btnEditPhone1.setTitle("mode_edit", for: .normal)
            btnEditPhone2.setTitle("local_phone", for: .normal)
            lblPhone.isEnabled = false
            lblPhone.borderStyle = UITextField.BorderStyle.none
            boolEditmobile = false
            
        }
    }
    @IBAction func cancelEditMobile2(_ sender: Any) {
        
        if boolEditmobile2 {
            
            btnEditMobile1.setTitle("mode_edit", for: .normal)
            btnEditMobile2.setTitle("stay_current_portrait", for: .normal)
            lblMobile.isEnabled = false
            lblMobile.borderStyle = UITextField.BorderStyle.none
            boolEditmobile2 = false
            
        }
    }
    
}
