//
//  DetailCustomerLocaltionViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import GoogleMaps
import SVProgressHUD
import CoreLocation

class DetailCustomerLocaltionViewController: UIBaseViewController {
    
    var tabBarDetailCustomerViewController: UITabBarDetailCustomerViewController? {
        return self.tabBarController as? UITabBarDetailCustomerViewController
    }
    
    @IBOutlet weak var maps: GMSMapView!
    var isLoadMap = false
    var isAvailable = true
    var marker: GMSMarker?
    var localsend = LocationSend()
    var myLocal = LocationSend()
    var camera: GMSCameraPosition?
    var locationManager = LocationManager.sharedInstance
    var zoom: Float = 14
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(DetailCustomerLocaltionViewController.dismis))
        barButtonItem.icon = "clear"
        
        self.navigationItem.leftBarButtonItem = barButtonItem
        
        self.navigationItem.title = "VỊ TRÍ".localized()
        checkLocation()
        maps.settings.myLocationButton = true
        maps.isMyLocationEnabled = true
        maps.delegate = self
    }
    
    @objc func editFunc() {
        
        tabBarDetailCustomerViewController?.model.updateLocationCustomer(cusId: tabBarDetailCustomerViewController?.model.visitCustomerEntity?.id, param: localsend, completion: {
            
            respone in
            print(respone)
            if respone == 200 {
                
                Toast.showSuccess("Câp nhập vị trí khách hàng thành công".localized())
                self.maps.clear()
                if let lat = self.localsend.latitude,
                    let lng = self.localsend.longitude {
                    
                    self.camera = GMSCameraPosition.camera(withLatitude:  lat,
                                                           longitude: lng,
                                                           zoom: self.zoom)
                    self.maps.camera =  self.camera!
                    self.marker?.position = (self.camera?.target)!
                    self.marker?.snippet = self.tabBarDetailCustomerViewController?.model.visitCustomerEntity?.name
                    self.marker?.appearAnimation = .pop
                    self.marker?.map = self.maps
                    self.maps.selectedMarker = self.marker
                }
            } else {
                
                Toast.showErr("Có lỗi xảy ra trong quá trình xử lý".localized())
            }
        })
    }
    func checkLocation() {
        locationManager.showVerboseMessage = true
        locationManager.autoUpdate = true
        locationManager.startUpdatingLocationWithCompletionHandler { (latitude, longitude, status, verboseMessage, error) -> () in
            if status != "Allowed access" {
                self.showLocationSetting {_ in 
                    self.checkLocation()
                }
            } else if latitude != 0 && longitude != 0 {
                self.myLocal.latitude = latitude
                self.myLocal.longitude = longitude
            }
        }
    }
    @objc func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionZoomOut(_ sender: Any) {
        if zoom == 1 {
            
            return
        }
        zoom -= 1
        maps.animate(toZoom: zoom)
    }
    
    @IBAction func actionZoomin(_ sender: Any) {
        if zoom == 20 {
            
            return
        }
        zoom += 1
        maps.animate(toZoom: zoom)
    }
}
extension DetailCustomerLocaltionViewController: GMSMapViewDelegate {
    
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        if !isLoadMap {
            
            let entity = tabBarDetailCustomerViewController?.model.visitCustomerEntity
            if let lat = entity?.location?.latitude,
                let lng = entity?.location?.longitude {
                
                camera = GMSCameraPosition.camera(withLatitude:  lat,
                                                  longitude: lng,
                                                  zoom: zoom)
                maps.camera =  camera!
                let markerDefault = GMSMarker()
                markerDefault.position = (camera?.target)!
                markerDefault.snippet = entity?.name
                markerDefault.appearAnimation = .pop
                markerDefault.map = maps
                maps.selectedMarker = markerDefault
                isLoadMap = true
            }
            
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if !(entityUserinfo?.canEditCustomerLocation)! {
            
            return
        }
        if marker == nil {
            
            marker = GMSMarker()
            
            let barButtonItem = UIBarButtonItem(title: "",
                                                style: UIBarButtonItem.Style.plain,
                                                target: self,
                                                action: #selector(DetailCustomerLocaltionViewController.editFunc))
            barButtonItem.icon = "done"
            self.navigationItem.rightBarButtonItem = barButtonItem
            
        }
        if isAvailable {
            self.localsend.latitude = coordinate.latitude
            self.localsend.longitude = coordinate.longitude
            getAddresLocaltion()
        } else {
            camera = GMSCameraPosition.camera(withLatitude:  coordinate.latitude,
                                              longitude: coordinate.longitude,
                                              zoom: zoom)
            self.maps.camera = camera!
            self.marker?.position = (camera?.target)!
            self.marker?.snippet = "Vị trí khách hàng"
            self.marker?.appearAnimation = .pop
            self.marker?.icon = UIImage(named: "blue_location")
            self.marker?.map = self.maps
            self.localsend.latitude = coordinate.latitude
            self.localsend.longitude = coordinate.longitude
        }
    }
    
    func getAddresLocaltion() {
        SVProgressHUD.show(withStatus: "Đang xác định vị trí".localized())
        if let latitude = localsend.latitude,
            let longitude = localsend.longitude {
            
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: latitude, longitude: longitude)
            var address = ""
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) -> Void in
                if placemarks != nil {
                    if let  placeMark = placemarks?[0] {
                        if let locationName = placeMark.addressDictionary!["Name"] as? String {
                            address = address.appending(locationName)
                            address = address.appending(" - ")
                        }
                        if let street = placeMark.addressDictionary!["Thoroughfare"] as? String {
                            address = address.appending(street)
                            address = address.appending(" - ")
                        }
                        if let state = placeMark.addressDictionary!["State"] as? String {
                            address = address.appending(state)
                            address = address.appending(" - ")
                        }
                        if let city = placeMark.addressDictionary!["City"] as? String {
                            address = address.appending(city)
                            address = address.appending(" - ")
                        }
                        if let country = placeMark.addressDictionary!["Country"] as? String {
                            address = address.appending(country)
                        }
                        
                        self.camera = GMSCameraPosition.camera(withLatitude: latitude,
                                                               longitude: longitude,
                                                               zoom: self.zoom)
                        self.maps.camera = self.camera!
                        self.marker?.position = (self.camera?.target)!
                        self.marker?.snippet = address
                        self.marker?.icon = UIImage(named: "blue_location")
                        self.marker?.appearAnimation = .pop
                        self.marker?.map = self.maps
                        SVProgressHUD.dismiss()
                    } else {
                        SVProgressHUD.dismiss()
                        Toast.showErr("Lỗi không xác định được địa chỉ".localized())
                        self.isAvailable = false
                    }
                } else {
                    SVProgressHUD.dismiss()
                    Toast.showErr("Lỗi không xác định được địa chỉ".localized())
                    self.isAvailable = false
                }
            }
        } else {
            SVProgressHUD.dismiss()
            Toast.showErr("Lỗi không xác định được vị trí".localized())
            self.isAvailable = false
        }
    }
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if !(entityUserinfo?.canEditCustomerLocation)! {
            
            return true
        }
        if marker == nil {
            
            marker = GMSMarker()
            
            let barButtonItem = UIBarButtonItem(title: "",
                                                style: UIBarButtonItem.Style.plain,
                                                target: self,
                                                action: #selector(DetailCustomerLocaltionViewController.editFunc))
            barButtonItem.icon = "done"
            self.navigationItem.rightBarButtonItem = barButtonItem
        }
        if let lat = myLocal.latitude,
            let lng = myLocal.longitude {
            
            camera = GMSCameraPosition.camera(withLatitude:  lat,
                                              longitude: lng,
                                              zoom: zoom)
            maps.camera =  camera!
            marker?.position = (camera?.target)!
            marker?.snippet = "Vị trí của tôi".localized()
            self.marker?.icon = UIImage(named: "blue_location")
            marker?.appearAnimation = .pop
            marker?.map = maps
            maps.selectedMarker = marker
            self.localsend.latitude = lat
            self.localsend.longitude = lng
        }
        return true
    }
}
