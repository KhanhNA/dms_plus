//
//  DetailCustomerStatisticsViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DetailCustomerStatisticsViewController: UIBaseViewController {
    @IBOutlet weak var tblThreemonth: UITableView!
    
    @IBOutlet weak var tblfive: UITableView!
    
    var arrayThree = [RevenueLastThreeMonth]()
    var arrayFive = [LastFiveOrders]()
    
    @IBOutlet weak var lblValueSL2TT: UIBaseLabel!
    @IBOutlet weak var lblTitleSanluong2thangtruoc: UIBaseLabel!
    
    @IBOutlet weak var lblTitleSLTHT: UIBaseLabel!
    @IBOutlet weak var lblValueSLTHT: UIBaseLabel!
    
    @IBOutlet weak var lblTitleDonDH: UIBaseLabel!
    
    @IBOutlet weak var lblValueDonDH: UIBaseLabel!
    
    @IBOutlet weak var lblDoanhThu2Thang: UIBaseLabel!
    
    @IBOutlet weak var lblDanhSach5DonHang: UIBaseLabel!
    var tabBarDetailCustomerViewController: UITabBarDetailCustomerViewController? {
        return self.tabBarController as? UITabBarDetailCustomerViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(DetailCustomerStatisticsViewController.dismis))
        barButtonItem.icon = "clear"
        
        self.navigationItem.leftBarButtonItem = barButtonItem
        
        self.navigationItem.title = "THỐNG KÊ".localized()
        
        let cell = UINib(nibName: "DetailStatisTableViewCell", bundle: nil)
        tblfive.register(cell, forCellReuseIdentifier: "cell")
        tblfive.tableFooterView = UIView()
        
        tblThreemonth.register(cell, forCellReuseIdentifier: "cell")
        tblThreemonth.tableFooterView = UIView()
        
        setLanguage()
        
        if let entity = tabBarDetailCustomerViewController?.model.visitCustomerEntity {
            
            if let productivityLastMonth = entity.productivityLastMonth {
                lblValueSL2TT.text = "\(productivityLastMonth)"
            } else {
                
                lblValueSL2TT.text = "0"
                
            }
            if let productivityThisMonth = entity.productivityThisMonth {
                lblValueSLTHT.text = "\(productivityThisMonth)"
            } else {
                
                lblValueSLTHT.text = "0"
                
            }
            
            if let nbOrderThisMonth = entity.nbOrderThisMonth {
                lblValueDonDH.text = "\(nbOrderThisMonth)"
            } else {
                
                lblValueDonDH.text = "0"
            }
            
            arrayThree = entity.revenueLastThreeMonth!
            arrayFive = entity.lastFiveOrders!
            
            self.tblfive.reloadData()
            self.tblThreemonth.reloadData()
            
        }
    }
    override func setLanguage() {
        lblTitleDonDH.text = "Đơn đặt hàng trong tháng".localized()
        lblTitleSLTHT.text = "Sản lượng tháng hiện tại".localized()
        lblTitleSanluong2thangtruoc.text = "Sản lượng 2 tháng trước".localized()
        lblDoanhThu2Thang.text = "Doanh thu 03 tháng gần đây".localized()
        lblDanhSach5DonHang.text = "Danh sách 5 đơn hàng gần nhất".localized()
        lblValueDonDH.textColor = UIColor.navibar
        lblValueSL2TT.textColor = UIColor.navibar
        lblValueSLTHT.textColor = UIColor.navibar
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func naviMenuLeft() {
        
    }
    
}

extension DetailCustomerStatisticsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblThreemonth {
            
            return arrayThree.count
            
        } else if tableView == tblfive {
            
            return arrayFive.count
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if  tableView ==  tblThreemonth {
            if let cell: DetailStatisTableViewCell = self.tblThreemonth.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? DetailStatisTableViewCell {
                let entity = arrayThree[indexPath.row]
                
                if let month = entity.month,
                    let revenue = entity.revenue {
                    cell.setValue(name: month, value: revenue.toDecimalString() )
                }
                return cell
            }
        } else if tableView == tblfive {
            if let cell: DetailStatisTableViewCell = self.tblfive.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? DetailStatisTableViewCell {
                let entity = arrayFive[indexPath.row]
                
                if let date = entity.date,
                    let total = entity.total {
                    cell.setValue(name: date.toString(), value: total.toDecimalString() )
                }
                
                return cell
            }
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let viewcontroler = VisitCustomerInfoViewController(nibName: "VisitCustomerInfoViewController", bundle: nil)
        //
        //        let entity = array?[indexPath.row]
        //        viewcontroler.info = entity
        //        self.showDetailViewController(viewcontroler, sender: nil)
    }
    
}

