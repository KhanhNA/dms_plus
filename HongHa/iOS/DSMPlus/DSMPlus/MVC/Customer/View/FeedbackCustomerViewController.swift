//
//  FeedbackCustomerViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class FeedbackCustomerViewController: UIBaseViewController {
    
    @IBOutlet weak var markerHeightConstraint: NSLayoutConstraint!
    var entity: VisitCustomerEntity?
    
    @IBOutlet weak var markerView: UIView!
    var array = [String]()
    
    @IBOutlet weak var lblCount: UIBaseLabel!
    @IBOutlet weak var viewInput: UIView!
    
    @IBOutlet weak var heightViewInput: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var txtMess: UITextField!
    var dateServer = ""
    let userdefau = UserDefaults.standard
    var navi: NaviFeedbackCustomerViewController? {
        
        return self.navigationController as? NaviFeedbackCustomerViewController
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Ý KIẾN KHÁCH HÀNG".localized()
        self.txtMess.placeholder = "Nhập ý kiến".localized()
        btnSend.tintColor = UIColor.navibar
        let nib = UINib(nibName: "FeedbackTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cell")
        
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(FeedbackCustomerViewController.dismis))
        barButtonItem.icon = "clear"
        
        self.navigationItem.leftBarButtonItem = barButtonItem
        
        if let endVisit = navi?.entity?.withVisit {
            
            if endVisit {
                
                viewInput.isHidden = true
                heightViewInput.constant = 0
                if let fb = navi?.entity?.feedbacks, let time = navi?.entity?.startTime {
                    array = fb
                    tableView.reloadData()
                    updateContentInsetForTableView(self.tableView, animated: false)
                    dateServer = time.toString()!
                    
                }
                
            }
            
        } else {
            
            let barRighAccep = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(FeedbackCustomerViewController.accept))
            barRighAccep.icon = "done"
            self.navigationItem.rightBarButtonItem = barRighAccep
            
            if let arrayFB = userdefau.value(forKey: CustomerContanst.KEY_FEEDBACK) as? [String] {
                array = arrayFB
                tableView.reloadData()
                updateContentInsetForTableView(self.tableView, animated: false)
            }
        }
    }
    @objc func dismis () {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func accept() {
        if array.count > 0 {
            userdefau.set(array, forKey: CustomerContanst.KEY_FEEDBACK)
            if userdefau.value(forKey: CustomerContanst.LASR_DATE) == nil {
                userdefau.set(setDate(), forKey: CustomerContanst.LASR_DATE)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateContentInsetForTableView(self.tableView, animated: false)
    }
    
    override func naviMenuLeft() {
        
    }
    
    @IBAction func actionSend(_ sender: Any) {
        send()
    }
    
    func send() {
        
        if txtMess.text?.isEmpty == false {
            array.append(txtMess.text!)
            txtMess.text = ""
            lblCount.text = "0/500"
            userdefau.removeObject(forKey: CustomerContanst.LASR_DATE)
            tableView.reloadData()
            updateContentInsetForTableView(self.tableView, animated: false)
        } else {
            Toast.showErr("Anh/chị vui lòng nhập nội dung ý kiến".localized())
        }
    }
}
extension FeedbackCustomerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: FeedbackTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? FeedbackTableViewCell {
            let content = array[indexPath.row]
            var dateformat = ""
            if indexPath.row == array.count - 1 {
                dateformat = setDate()
                if dateServer != "" {
                    
                    dateformat = dateServer
                }
                if userdefau.value(forKey: CustomerContanst.LASR_DATE) != nil {
                    
                    let stDate = userdefau.value(forKey: CustomerContanst.LASR_DATE) as! String
                    dateformat = stDate
                }
            }
            cell.setValue(content: content, time: dateformat)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func updateContentInsetForTableView(_ tableView: UITableView, animated: Bool) {
        
        let lastRow = self.tableView(tableView, numberOfRowsInSection: 0)
        let lastIndex = lastRow > 0 ? lastRow - 1 : 0
        let lastIndexPath = NSIndexPath(item: lastIndex, section: 0)
        let lastCellFrame = tableView.rectForRow(at: lastIndexPath as IndexPath)
        let topInset = max(self.tableView.frame.height - lastCellFrame.origin.y - lastCellFrame.height, 0)
        var contentInset = tableView.contentInset
        contentInset.top = topInset
        UIView.animate(withDuration: animated ? 0.25 : 0.0, delay: 0.0, options: UIView.AnimationOptions.beginFromCurrentState, animations: {
            self.markerHeightConstraint.constant = abs(topInset)
            tableView.contentInset = contentInset
            self.markerView.setNeedsLayout()
            self.markerView.layoutIfNeeded()
            
        }, completion: { (finished: Bool) in
            
        })
        
    }
    func setDate() -> String {
        
        let date = Date()
        let cal = Calendar.current
        let hour = cal.component(.hour, from: date)
        let minites = cal.component(.minute, from: date)
        let day = cal.component(.day, from: date)
        let month = cal.component(.month, from: date)
        let year = cal.component(.year, from: date)
        return "\(hour):\(minites) \(day)/\(month)/\(year)"
    }
    
}
extension FeedbackCustomerViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        send()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let number = range.location - range.length + 1
        
        if number > 500 {
            Toast.showErr("Anh/ chị không được nhập quá 500 ký tự".localized())
            return false
        }
        self.lblCount.text = "\(number)/500"
        return true
    }
    
}
