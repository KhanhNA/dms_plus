//
//  InputMessView.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/30/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class InputMessView: UIView {
    
    fileprivate var txtMes: UITextField?
    fileprivate var bt: UIButton?
    
    override func draw(_ rect: CGRect) {
        
        guard self.txtMes == nil else {
            return
        }
        
        guard self.bt == nil else {
            return
        }
        
        self.txtMes = UITextField()
        self.txtMes?.placeholder = "Nhap noi dung".localized()
        self.txtMes?.translatesAutoresizingMaskIntoConstraints = false
        
        self.bt = UIButton(type: UIButton.ButtonType.custom)
        self.bt?.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.txtMes!)
        self.addSubview(self.bt!)
        
        let views = ["txtMes": self.txtMes ?? UITextField(),
                     "bt": self.bt ?? UIButton(type: UIButton.ButtonType.custom)]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let txtVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[txtMes]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += txtVerticalConstraints
        
        let txtHConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-8-[txtMes]-8-[bt(45)]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += txtHConstraints
        
        let btVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-8-[bt]-8-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += btVerticalConstraints
        
        NSLayoutConstraint.activate(allConstraints)
        
    }
    
}
