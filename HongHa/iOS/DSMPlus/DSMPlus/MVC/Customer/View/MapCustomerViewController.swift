//
//  MapCustomerViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapCustomerViewController: UIBaseViewController {
    
    @IBOutlet weak var maps: GMSMapView!
    var navi: UINavibarMapCustomerViewController? {
        
        return self.navigationController as? UINavibarMapCustomerViewController
    }
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var btnChangeMap: UIButton!
    @IBOutlet weak var btnDirect: UIButton!
    var london: GMSMarker?
    var londonView: UIImageView?
    var cameraPo: GMSCameraPosition?
    var zoom = 14
    var markerTapped: Bool = false
    var infoWindow: InfoMarkerView?
    var tappedMarker = GMSMarker()
    var info: VisitCustomerEntity?
    var isRender = false
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        self.navigationItem.title = "BẢN ĐỒ KHÁCH HÀNG".localized()
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(MapCustomerViewController.dismis))
        barButtonItem.icon = "clear"
        
        self.navigationItem.leftBarButtonItem = barButtonItem
        btnChangeMap.tintColor = UIColor.navibar
        btnDirect.tintColor = UIColor.navibar
        loadDefault()
        // loadMap()
    }
    override func naviMenuLeft() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadDefault() {
        
        maps.isMyLocationEnabled = true
        maps.settings.myLocationButton = true
        maps.settings.allowScrollGesturesDuringRotateOrZoom = true
        maps.settings.zoomGestures = true
        maps.settings.compassButton = true
        maps.settings.scrollGestures = true
        maps.delegate = self
        maps.setMinZoom(1, maxZoom: 15)
    }
    @objc func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    func loadMap() {
        
        if navi?.list != nil &&  (navi?.list?.count)! > 0 {
            
            var i = 0
            for item in (navi?.list!)! {
                if let lat = item.location?.latitude,
                    let lng = item.location?.longitude {
                    
                    if i == 0 {
                        cameraPo = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: Float(zoom))
                        maps.camera = cameraPo!
                    }
                    let position = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                    let marker = GMSMarker()
                    marker.position = position
                    marker.title = item.name
                    if let endVisit = item.visitStatus {
                        
                        if endVisit == 2 {
                            
                            marker.icon = UIImage(named: "blue_location")
                        }
                    }
                    marker.tracksViewChanges = true
                    marker.isFlat = true
                    marker.zIndex = Int32(i)
                    marker.appearAnimation = .pop
                    marker.map = maps
                    i += 1
                }
            }
            
        }
    }
    @IBAction func zoom_out(_ sender: Any) {
        if zoom == 1 {
            
            return
        }
        zoom -= 1
        maps.animate(toZoom: Float(zoom))
    }
    @IBAction func zoom_in(_ sender: Any) {
        if zoom == 20 {
            
            return
        }
        zoom += 1
        maps.animate(toZoom: Float(zoom))
    }
    @IBAction func actionDirect(_ sender: Any){
        if let en = self.navi?.list {
            if let entity = en[btnDirect.tag] as VisitCustomerEntity? {
                
                if  let latitude =  entity.location?.latitude,
                    let longitude: CLLocationDegrees =  entity.location?.longitude {
                    
                    if UIApplication.shared.openURL(NSURL(string:"comgooglemaps://")! as URL) {
                        
                        UIApplication.shared.openURL(NSURL(string:
                            "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")! as URL)
                        
                    } else {
                        
                        let url = NSURL(string: "http://maps.apple.com/?saddr=&daddr=\(latitude),\(longitude)")
                        UIApplication.shared.openURL(url! as URL)
                    }
                }
            }
        }
    }
    @IBAction func actionChangeMap(_ sender: Any) {
        switch maps.mapType {
        case .normal:
            maps.mapType = .hybrid
            break
        case .hybrid:
            maps.mapType = .normal
        default:
            maps.mapType = .normal
        }
    }
    
}
extension MapCustomerViewController: CLLocationManagerDelegate {
    
    //Location Manager delegates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
    }
}
extension MapCustomerViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        return UIView()
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let location = marker.position
        tappedMarker = marker
        infoWindow?.removeFromSuperview()
        btnDirect.isHidden = false
        if self.infoWindow == nil {
            self.infoWindow = Bundle.main.loadNibNamed("InfoMarkerView", owner: self, options: nil)?.first! as? InfoMarkerView
        }
        let index = marker.zIndex
        btnDirect.tag = Int(index)
        if let en = self.navi?.list {
            if let entity = en[Int(index)] as VisitCustomerEntity? {
                info = entity
                infoWindow?.lblName.text = entity.name
                infoWindow?.lblSDT.text = entity.mobile
                infoWindow?.btnViewInfo.setTitle("Chi tiết".localized(), for: .normal)
                infoWindow?.btnVisit.setTitle("Ghé thăm".localized(), for: .normal)
                infoWindow?.btnVisit.addTarget(self, action: #selector(actionVisit(_:)), for: .touchUpInside)
                infoWindow?.btnViewInfo.addTarget(self, action: #selector(actionDetail(_:)), for: .touchUpInside)
            }
        }
        infoWindow?.center = mapView.projection.point(for: location)
        infoWindow?.center.y -= 60
        infoWindow?.center.x += 35
        self.view.addSubview(infoWindow!)
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        infoWindow?.center = mapView.projection.point(for: tappedMarker.position)
        infoWindow?.center.y -= 60
        infoWindow?.center.x += 35
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow?.removeFromSuperview()
        btnDirect.isHidden = true
    }
    @objc func actionVisit(_ sender: UIButton!) {
        if info?.visitStatus == 2 {
            let viewcontroler = VisitCustomerActionsViewController(nibName: "VisitCustomerActionsViewController", bundle: nil)
            viewcontroler.entity = info
            viewcontroler.isVisit = false
            viewcontroler.isStart = true
            let navi = UINaviBaseViewController()
            navi.viewControllers = [viewcontroler]
            
            self.present(navi, animated: true, completion: nil)
            
        } else {
            let viewcontroler = VisitCustomerInfoViewController(nibName: "VisitCustomerInfoViewController", bundle: nil)
            viewcontroler.info = info
            self.showDetailViewController(viewcontroler, sender: nil)
        }
        
    }
    @objc func actionDetail(_ sender: UIButton!) {
        let storyboard = UIStoryboard(name: "DetailCus", bundle: nil)
        if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "storyboardCusDetail") as? UITabBarDetailCustomerViewController {
            viewcontroler.entity = self.info
            self.showDetailViewController(viewcontroler, sender: nil)
        }
        
    }
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        print("mapViewDidFinishTileRendering")
        if !isRender {
            self.loadMap()
            isRender = true
        }
    }
}
