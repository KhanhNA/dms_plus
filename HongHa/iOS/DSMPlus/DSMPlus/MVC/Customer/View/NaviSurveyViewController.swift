//
//  NaviSurveyViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class NaviSurveyViewController: UINavigationController {
    
    var cusId: String?
    var listAnswer: [SurveyAnswers]?
    var isEndVisit: Bool?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
