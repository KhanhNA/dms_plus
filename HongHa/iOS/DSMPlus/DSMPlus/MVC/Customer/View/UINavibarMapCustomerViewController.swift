//
//  UINavibarMapCustomerViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UINavibarMapCustomerViewController: UINavigationController {

    var list: [VisitCustomerEntity]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
