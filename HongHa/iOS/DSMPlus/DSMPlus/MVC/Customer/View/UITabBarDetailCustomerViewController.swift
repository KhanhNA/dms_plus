//
//  UITabBarDetailCustomerViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UITabBarDetailCustomerViewController: UITabBarBaseViewController {

    weak var entity: VisitCustomerEntity?
    var model = VisitCustomerModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let barItem = self.tabBar.items?[0]
        barItem?.title = "Tổng hợp".localized()
        barItem?.setIcon("account_balance")
        
        let barItemAll = self.tabBar.items?[1]
        barItemAll?.title = "Thống kê".localized()
        barItemAll?.setIcon("show_chart")
        
        let barItemLocaltion = self.tabBar.items?[2]
        barItemLocaltion?.title = "Vị trí".localized()
        barItemLocaltion?.setIcon("location_on")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
