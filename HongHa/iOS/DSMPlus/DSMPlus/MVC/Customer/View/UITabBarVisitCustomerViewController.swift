//
//  UITabBarVisitCustomerViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UITabBarVisitCustomerViewController: UITabBarBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barItem = self.tabBar.items?[0]
        barItem?.title = "Hôm nay".localized()
        barItem?.setIcon("today")
        
        let barItemAll = self.tabBar.items?[1]
        barItemAll?.title = "Tất cả".localized()
        barItemAll?.setIcon("filter_list")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
