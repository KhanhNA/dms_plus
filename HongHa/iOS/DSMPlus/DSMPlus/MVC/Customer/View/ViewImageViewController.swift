//
//  ViewImageViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ImagePicker

class ViewImageViewController: UIBaseViewController {
    
    var image: UIImage?
    var name: String?
    var id_image: String?
    var isClose: Bool?
    var idCustomer: VisitCustomerEntity?
    var localSen: LocationSend?
    
    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var activityAnimation: UIActivityIndicatorView!
    let save = UserDefaults.standard
    var model = VisitCustomerModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "XEM HÌNH".localized()
        activityAnimation.startAnimating()
        if isClose == nil {
            if let id_img = id_image {
                imageview.setImage_Url(id_img, placeholder: "")
            } else if let img = image {
                imageview.image = img
                createButtonNavi()
            } else if let url = save.value(forKey: CustomerContanst.IMAGE_PATH) as? String {
                let image = UIImage(contentsOfFile: url)
                imageview.image = image
                createButtonNavi()
            }
        } else {
            
            let barRighAccep = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ViewImageViewController.closeCustomer))
            barRighAccep.icon = "done"
            self.navigationItem.rightBarButtonItem = barRighAccep
            
            imageview.image = image
        }
    }
    
    func createButtonNavi() {
        
        let barRighiChup = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ViewImageViewController.takeCamera))
        barRighiChup.icon = "photo_camera"
        
        let barRighAccep = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ViewImageViewController.accept))
        barRighAccep.icon = "done"
        self.navigationItem.rightBarButtonItems = [barRighAccep, barRighiChup]
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    @objc func closeCustomer() {
        
        UploadImage.UploadRequest(image: imageview, name: "Close_Customer", completion: {
            
            respone in
            print("id photo: \(respone)")
            if respone != nil {
                
                let send = CloseCustomerSend()
                send.closingPhoto = respone
                send.location = self.localSen
                
                print(send.toString() ?? "nil")
                
                self.model.closeCustomer(cusId: self.idCustomer?.id, param: send, completion: {
                    respone in
                    print(respone ?? "id nul loi close")
                    if respone != nil {
                        
                        let viewcontroler = VisitCustomerActionsViewController(nibName: "VisitCustomerActionsViewController", bundle: nil)
                        self.idCustomer?.visitStatus = 2
                        viewcontroler.entity = self.idCustomer
                        viewcontroler.isVisit = false
                        viewcontroler.isStart = false
                        viewcontroler.isClose = true
                        self.navigationController?.pushViewController(viewcontroler, animated: true)
                    }
                })
            }
        })
    }
    @objc func takeCamera() {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        
        present(imagePickerController, animated: true) {
            imagePickerController.galleryView.alpha = 0
        }
    }
    @objc func accept() {
        
        saveImageDocumentDirectory(image: imageview.image)
        let _ =  self.navigationController?.popViewController(animated: true)
        
    }
    
    func saveImageDocumentDirectory(image: UIImage?) {
        let fileManager = FileManager.default
        if let name = name {
            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("\(name).png")
            print(paths)
            save.set(paths, forKey: CustomerContanst.IMAGE_PATH)
            let imageData = image!.jpegData(compressionQuality: ConfigSizeImage)
            fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        }
    }
}
extension ViewImageViewController: ImagePickerDelegate {
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        self.imageview.image = images[0]
    }
}
