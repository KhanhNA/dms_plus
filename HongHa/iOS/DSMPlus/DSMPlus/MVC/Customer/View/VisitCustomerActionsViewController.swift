//
//  VisitCustomerActionsViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ImagePicker
import CoreLocation

protocol VisitCustomerActionsViewControllerDelegate: NSObjectProtocol {
    func reloadTable()
}

class VisitCustomerActionsViewController: UIBaseViewController {
    
    @IBOutlet weak var lblMobile: UIBaseLabel!
    @IBOutlet weak var lblName: UIBaseLabel!
    @IBOutlet weak var btnXemChiTiet: UIButton!
    @IBOutlet weak var btnKhaoSat: UIButton!
    @IBOutlet weak var lblOrder: UIBaseLabel!
    @IBOutlet weak var lblFeedback: UIBaseLabel!
    @IBOutlet weak var lblCamera: UIBaseLabel!
    @IBOutlet weak var lblSurvey: UIBaseLabel!
    @IBOutlet weak var iconOder: UILabel!
    @IBOutlet weak var iconFeedback: UILabel!
    @IBOutlet weak var iconCamera: UILabel!
    @IBOutlet weak var iconSurvey: UILabel!
    @IBOutlet weak var viewline: UIView!
    @IBOutlet weak var viewKhaosat: UIView!
    
    @IBOutlet weak var lbNumberOder: UIBaseLabel!
    @IBOutlet weak var lbNumberFeedback: UIBaseLabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbNumberSurvey: UIBaseLabel!
    
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var btnChupHinh: UIButton!
    @IBOutlet weak var btnYKien: UIButton!
    @IBOutlet weak var btnDonDH: UIButton!
    @IBOutlet weak var lblAdd: UIBaseLabel!
    var entity: VisitCustomerEntity?
    var info: VisitCustomerEntity?
    let save = UserDefaults.standard
    let model = VisitCustomerModel()
    var idVisit: String?
    var isVisit: Bool!
    var isStart: Bool = false
    var isClose: Bool = false
    weak var delegate: VisitCustomerActionsViewControllerDelegate?
    var entityOrder: EntityCalculator? = nil
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.tabBarController?.tabBar.isHidden = true
        setInfo()
        if save.value(forKey: "IMAGE_PATH") != nil {
            lblCamera.text = "XEM HÌNH".localized()
        }
        if entity?.visitStatus == 2 {
            getCustomerVisit()
        }
        
        if entity?.visitStatus == 0 {
            if CLLocationManager.locationServicesEnabled() {
                if let location = locationManager.location {
                    self.startVisit(id: (self.entity?.id)!, latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                }
            } else {
                self.startVisit(id: (self.entity?.id)!, latitude: nil, longitude: nil)
            }
        }
    }
    func getCustomerVisit() {
        btnFinish.isHidden = true
        model.getCutomerCustomerVisit(cusid: entity?.id, completion: { respone in
            if let data = respone {
                self.info = data
                self.entity?.photo = self.info?.photo
                self.entity?.visitStatus = self.info?.visitStatus
                if (self.info?.photo) != nil {
                    self.lblCamera.text = "XEM HÌNH".localized()
                } else {
                    self.lblCamera.text = "KHÔNG CÓ HÌNH".localized()
                }
                self.setDataNumber()
            }
        })
    }
    func startVisit (id: String, latitude: Double?, longitude: Double?) {
        model.startVisit(cusid: id, latitude: latitude, longitude: longitude, completion: { respone in
            if respone != nil {
                self.idVisit = respone
            }
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        setDataNumber()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setInfo() {
        
        lblOrder.text = "ĐƠN ĐẶT HÀNG".localized()
        lblOrder.colorText = UIColor.navibar
        lblCamera.text = "CHỤP HÌNH".localized()
        lblCamera.colorText = UIColor.navibar
        lblSurvey.text = "KHẢO SÁT".localized()
        lblSurvey.colorText = UIColor.navibar
        lblFeedback.text = "Ý KIẾN".localized()
        lblFeedback.colorText = UIColor.navibar
        btnXemChiTiet.setTitle("XEM CHI TIẾT".localized(), for: .normal)
        iconOder.textColor = UIColor.white
        iconCamera.textColor = UIColor.white
        iconSurvey.textColor = UIColor.white
        iconFeedback.textColor = UIColor.white
        viewline.backgroundColor = UIColor.navibar
        
        iconOder.backgroundColor = UIColor.navibar
        iconCamera.backgroundColor = UIColor.navibar
        iconSurvey.backgroundColor = UIColor.navibar
        iconFeedback.backgroundColor = UIColor.navibar
        
        iconOder.layer.cornerRadius = iconOder.bounds.size.width/2
        iconCamera.layer.cornerRadius = iconCamera.bounds.size.width/2
        iconSurvey.layer.cornerRadius = iconSurvey.bounds.size.width/2
        iconFeedback.layer.cornerRadius = iconFeedback.bounds.size.width/2
        
        if let name = entity?.contact,
            let sdt = entity?.mobile,
            let add = entity?.name {
            lblName.text = name
            lblMobile.text = sdt
            lblAdd.text = add
        }
        if !NSModule.SURVEY.isShow {
            viewKhaosat.isHidden = true
        }
        
        lbNumberOder.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        lbNumberOder.text = nil
        lbNumberFeedback.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        lbNumberFeedback.text = nil
        lbNumberSurvey.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        lbNumberSurvey.text = nil
        imageView.image = nil
    }
    @IBAction func actionDismis(_ sender: Any) {
        if self.info?.withVisit != nil {
            if isStart {
                delegate?.reloadTable()
                self.navigationController?.navigationBar.isHidden = false
                let _ = self.navigationController?.popViewController(animated: true)
                return
            } else {
                self.navigationController?.navigationBar.isHidden = false
                let _ = self.navigationController?.popToRootViewController(animated: true)
                return
            }
        }
        if let stt = entity?.visitStatus {
            if stt == 0 || isVisit {
                UIAlertView(title: "Thông báo".localized(), message: "Bạn vẫn đang ghé thăm khách hàng này, phải kết thúc ghé thăm trước khi thoát".localized(), delegate: self, cancelButtonTitle: "OK").show()
                return
            } else {
                if isStart {
                    delegate?.reloadTable()
                    self.navigationController?.dismiss(animated: true, completion: nil)
                    return
                } else {
                    if isClose {
                        self.navigationController?.dismissToRootViewController()
                        return
                    } else {
                        self.navigationController?.dismissToRootViewController()
                        return
                    }
                }
            }
        }
    }
    
    @IBAction func actionFinish(_ sender: Any) {
        let alertController = UIAlertController (title: "Thống báo".localized(), message: "Bạn muốn kết thúc ghé thăm?".localized(), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Đồng ý".localized(), style: .default) { (_) -> Void in
            
            let sendEntity = EntityVisitEndCustomerSend()
            sendEntity.exhibitionRatings = nil
            sendEntity.feedbacks = nil
            sendEntity.order = nil
            sendEntity.photo = nil
            sendEntity.surveyAnswers = nil
            // upload image truoc
            if let url = self.save.value(forKey: CustomerContanst.IMAGE_PATH) {
                if let url = url as? String {
                    let image    = UIImage(contentsOfFile: url)
                    let imageView = UIImageView(image: image)
                    self.upImage(imageView, sendEntity, url)
                }
            } else {
                if let sv = self.save.value(forKey: SurveyEntity.KEY_SURVEY) as? [String] {
                    sendEntity.surveyAnswers = [SurveyEntitySend]()
                    for survey in sv {
                        let op = SurveyEntitySend()
                        op.options = []
                        op.surveyId = survey
                        if let svChoice = self.save.value(forKey: survey) as? [String] {
                            for select in svChoice {
                                op.options?.append(select)
                            }
                            sendEntity.surveyAnswers?.append(op)
                        }
                        self.save.removeObject(forKey: survey)
                    }
                    if sendEntity.surveyAnswers?.count == 0 {
                        sendEntity.surveyAnswers = nil
                    }
                }
                
                if let data = self.save.value(forKey: CustomerContanst.KEY_ORDER) as? String {
                    
                    if let json = data.toJson() {
                        let entityOder = EntityCalculator(json: json)
                        
                        sendEntity.order = entityOder
                    }
                }
                
                if let arrayFB = self.save.value(forKey: CustomerContanst.KEY_FEEDBACK) as? [String] {
                    
                    sendEntity.feedbacks = arrayFB
                    self.save.removeObject(forKey: CustomerContanst.KEY_FEEDBACK)
                }
                
                self.endVisit(sendEntity: sendEntity)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Huỷ bỏ".localized(), style: .default, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    fileprivate func upImage(_ imageView: UIImageView, _ sendEntity: EntityVisitEndCustomerSend, _ url: String) {
        UploadImage.UploadRequest(image: imageView, name: self.entity?.name, completion: { respone in
            if let id_photo = respone {
                sendEntity.photo = id_photo
                let fileManager = FileManager.default
                do {
                    try fileManager.removeItem(atPath: url)
                } catch {
                }
                if let sv = self.save.value(forKey: SurveyEntity.KEY_SURVEY) as? [String] {
                    sendEntity.surveyAnswers = [SurveyEntitySend]()
                    for survey in sv {
                        let op = SurveyEntitySend()
                        op.options = []
                        op.surveyId = survey
                        if let svChoice = self.save.value(forKey: survey) as? [String] {
                            for select in svChoice {
                                op.options?.append(select)
                            }
                            sendEntity.surveyAnswers?.append(op)
                        }
                        self.save.removeObject(forKey: survey)
                    }
                    if sendEntity.surveyAnswers?.count == 0 {
                        sendEntity.surveyAnswers = nil
                    }
                }
                if let arrayFB = self.save.value(forKey: CustomerContanst.KEY_FEEDBACK) as? [String] {
                    
                    sendEntity.feedbacks = arrayFB
                    self.save.removeObject(forKey: CustomerContanst.KEY_FEEDBACK)
                }
                if let data = self.save.value(forKey: CustomerContanst.KEY_ORDER) as? String {
                    
                    if let json = data.toJson() {
                        let entityOder = EntityCalculator(json: json)
                        
                        sendEntity.order = entityOder
                    }
                }
                self.endVisit(sendEntity: sendEntity)
            } else {
                let alertPhoto = UIAlertController(title: "Thông báo".localized(), message: "Đã xảy ra lỗi trong quá trình upload ảnh. Bạn có muốn tiếp tục hoàn thành ghé thăm".localized(), preferredStyle: .alert)
                let continue_photo = UIAlertAction(title: "Tiếp tục".localized(), style: .default) { (_) -> Void in
                    sendEntity.photo = nil
                }
                alertPhoto.addAction(continue_photo)
                let cancel_photo = UIAlertAction(title: "Huỷ bỏ".localized(), style: .default) { (_) -> Void in }
                alertPhoto.addAction(cancel_photo)
            }
        })
    }
    
    func endVisit(sendEntity: EntityVisitEndCustomerSend?) {
        
        if (self.entity?.id) != nil {
            self.model.endVisit(cusid: self.idVisit, put: sendEntity, completion: { respone in
                if let en = respone {
                    if en.withVisit != nil {
                        self.entity?.visitStatus = en.visitStatus
                        self.save.removeObject(forKey: CustomerContanst.IMAGE_PATH)
                        self.save.removeSuite(named: CustomerContanst.IMAGE_PATH)
                        self.save.removeSuite(named: CustomerContanst.KEY_ORDER)
                        self.save.removeObject(forKey: CustomerContanst.KEY_ORDER)
                        self.save.removeObject(forKey: CustomerContanst.LASR_DATE)
                        self.save.removeSuite(named: CustomerContanst.LASR_DATE)
                        self.btnFinish.isHidden = true
                        self.isVisit = false
                        self.entity?.photo = en.photo
                        self.entity?.withVisit = en.withVisit
                        self.info = en
                        Toast.showSuccess("Ghé thăm thành công".localized())
                        self.setDataNumber()
                    }
                } else {
                    if let mes = self.model.error_message {
                        if mes == "visit.ended" {
                            Toast.showErr("Chương trình ghé thăm đã hoàn tất".localized())
                            self.getCustomerVisit()
                            self.save.removeObject(forKey: CustomerContanst.IMAGE_PATH)
                            self.save.removeSuite(named: CustomerContanst.IMAGE_PATH)
                            self.save.removeSuite(named: CustomerContanst.KEY_ORDER)
                            self.save.removeObject(forKey: CustomerContanst.KEY_ORDER)
                        } else {
                            Toast.showErr(mes)
                        }
                    }
                }
            })
        }
    }
    
    @IBAction func actionDetail(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "DetailCus", bundle: nil)
        if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "storyboardCusDetail") as? UITabBarDetailCustomerViewController {
            viewcontroler.entity = entity
            self.showDetailViewController(viewcontroler, sender: nil)
        }
    }
    @IBAction func actionDonDH(_ sender: Any) {
        
        if (self.info?.withVisit) != nil {
            self.navigationController?.navigationBar.isHidden = false
            let infoOderViewController = InfoOderViewController(nibName: "InfoOderViewController", bundle: nil)
            infoOderViewController.idOder = self.info?.id
            infoOderViewController.isEndVisit = true
            let navi = UINaviBaseViewController()
            navi.viewControllers = [infoOderViewController]
            self.showDetailViewController(navi, sender: nil)
            return
        }
        
        if let data = self.save.value(forKey: CustomerContanst.KEY_ORDER) as? String {
            
            if let json = data.toJson() {
                let entityOder = EntityCalculator(json: json)
                let orderStoryboard = UIStoryboard(name: "OderList", bundle: nil)
                if let tabbar = orderStoryboard.instantiateViewController(withIdentifier: "ProductTabarViewController") as? ProductTabarViewController {
                    
                    tabbar.url = .visit_customer
                    tabbar.delegateProduct = self
                    tabbar.entityCalculator = entityOder
                    tabbar.modelSelectProductOrder.visitCustomerEntity = entity
                    self.present(tabbar, animated: true, completion: nil)
                }
            }
            
            return
        }
        let storyboarOder = UIStoryboard(name: "OderList", bundle: nil)
        if let tabBarBaseViewController = storyboarOder.instantiateViewController(withIdentifier: "ProductTabarViewController") as? ProductTabarViewController {
            
            tabBarBaseViewController.url = .visit_customer
            tabBarBaseViewController.delegateProduct = self
            tabBarBaseViewController.modelSelectProductOrder.visitCustomerEntity = entity
            
            if entityUserinfo?.vanSales == true {
                
                let alert = UIAlertController(title: "Chọn hình thức bán hàng".localized(),
                                              message: nil,
                                              preferredStyle: .actionSheet)
                
                let alertExit = UIAlertAction(title: "Pre-Sale".localized(), style: .default) { (alertAction) in
                    self.present(tabBarBaseViewController, animated: true) {
                        if tabBarBaseViewController.entityCalculator == nil {
                            tabBarBaseViewController.entityCalculator = EntityCalculator()
                        }
                        tabBarBaseViewController.entityCalculator?.vanSales = false
                    }
                }
                
                let alertSend = UIAlertAction(title: "Van-Sale".localized(), style: .default) { (alertAction) in
                    self.present(tabBarBaseViewController, animated: true) {
                        if tabBarBaseViewController.entityCalculator == nil {
                            tabBarBaseViewController.entityCalculator = EntityCalculator()
                        }
                        tabBarBaseViewController.entityCalculator?.vanSales = true
                    }                }
                
                let alertCancel = UIAlertAction(title: "Huỷ".localized(), style: .cancel)
                
                alert.addAction(alertExit)
                alert.addAction(alertSend)
                alert.addAction(alertCancel)
                
                self.present(alert, animated: true)
            } else {
                self.present(tabBarBaseViewController,
                             animated: true, completion: nil)
            }
            
        }
    }
    @IBAction func actionYKien(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Customer", bundle: nil)
        if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "navifeedback") as? NaviFeedbackCustomerViewController {
            viewcontroler.entity = info
            if let en = info {
                
                if  en.withVisit! {
                    
                    viewcontroler.entity = info
                }
            }
            self.showDetailViewController(viewcontroler, sender: nil)
        }
        
    }
    @IBAction func actionChupHinh(_ sender: Any) {
        if let end = self.info?.withVisit {
            if end {
                
                if (self.entity?.photo) != nil {
                    
                    let storyboard = UIStoryboard(name: "Customer", bundle: nil)
                    if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "viewimagestoryboard") as? ViewImageViewController {
                        viewcontroler.id_image = self.entity?.photo
                        self.navigationController?.pushViewController(viewcontroler, animated: true)
                    }
                    return
                } else {
                    
                    UIAlertView(title: "Thông báo".localized(), message: "Không có ảnh khi ghé thăm".localized(), delegate: nil, cancelButtonTitle: "OK").show()
                    return
                }
                
            }
        }
        if let vsstt = self.entity?.visitStatus,
            let endvisit = self.entity?.withVisit {
            
            if vsstt == 2 || endvisit {
                
                if (self.entity?.photo) != nil {
                    
                    let storyboard = UIStoryboard(name: "Customer", bundle: nil)
                    if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "viewimagestoryboard") as? ViewImageViewController {
                        viewcontroler.id_image = self.entity?.photo
                        self.navigationController?.pushViewController(viewcontroler, animated: true)
                    }
                } else {
                    
                    UIAlertView(title: "Thông báo".localized(), message: "Không có ảnh khi ghé thăm".localized(), delegate: nil, cancelButtonTitle: "OK").show()
                    return
                }
            }
        }
        
        if let url = save.value(forKey: CustomerContanst.IMAGE_PATH) {
            print(url)
            let storyboard = UIStoryboard(name: "Customer", bundle: nil)
            if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "viewimagestoryboard") as? ViewImageViewController {
                if let name = entity?.name {
                    viewcontroler.name = name.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with: "").replacingOccurrences(of: " ", with: "_")
                }
                self.navigationController?.pushViewController(viewcontroler, animated: true)
            }
        } else {
            
            let imagePickerController = ImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.imageLimit = 1
            present(imagePickerController, animated: true) { 
                imagePickerController.galleryView.alpha = 0
            }
        }
    }
    @IBAction func actionKhaoSat(_ sender: Any) {
        let storyboar = UIStoryboard(name: "Customer", bundle: nil)
        if let viewcontroler = storyboar.instantiateViewController(withIdentifier: "navisurveylist") as? NaviSurveyViewController {
            viewcontroler.cusId = entity?.id
            viewcontroler.isEndVisit = info?.withVisit
            if let answers = info?.surveyAnswers {
                
                if answers.count > 0 {
                    
                    viewcontroler.listAnswer = answers
                }
                
            }
            self.present(viewcontroler, animated: true, completion: nil)
        }
    }
}
extension VisitCustomerActionsViewController: ImagePickerDelegate {
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Customer", bundle: nil)
        if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "viewimagestoryboard") as?ViewImageViewController {
            viewcontroler.image = images[0]
            if let name = entity?.name {
                viewcontroler.name = name.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: ",", with: "").replacingOccurrences(of: " ", with: "_")
            }
            self.navigationController?.pushViewController(viewcontroler, animated: true)
        }
    }
}
extension VisitCustomerActionsViewController: ProductTabarViewControllerDelegate {
    func saveOder(_ productTabarViewController: ProductTabarViewController, _ entityCalculator: EntityCalculator) {
        if self.save.value(forKey: CustomerContanst.KEY_ORDER) != nil {
            self.save.removeObject(forKey: CustomerContanst.KEY_ORDER)
            self.save.removeSuite(named: CustomerContanst.KEY_ORDER)
        }
        
        if let json = entityCalculator.toString() {
            self.save.setValue(json, forKey: CustomerContanst.KEY_ORDER)
            self.save.synchronize()
        }
        productTabarViewController.dismiss(animated: true, completion: nil)
    }
}

extension VisitCustomerActionsViewController {
    func setDataNumber() {
        lbNumberOder.text = nil
        lbNumberFeedback.text = nil
        lbNumberSurvey.text = nil
        imageView.image = nil
        
        var sumOder: NSNumber?
        var numberFB: Int?
        var numbersurveyAnswers: Int?
        var idImage: String?
        var approveStatus: OrderStatus = .WAITING
        
        if let info = self.info {
            if let withVisit = info.withVisit {
                if withVisit {
                    if let approveStatu = info.approveStatus {
                        sumOder = NSNumber(value: -1.0)
                        approveStatus = approveStatu
                        if approveStatus == .ACCEPTED {
                            sumOder = info.grandTotal
                        }
                    }
                } else {
                    sumOder = info.grandTotal
                }
            }
            numberFB = info.feedbacks?.count
            numbersurveyAnswers = info.surveyAnswers?.count
            idImage = info.photo
        }
        
        if let info = self.entity {
            if sumOder == nil {
                if entity?.visitStatus == 2 {
                    if let approveStatu = info.visitInfo?.approveStatus {
                        sumOder = NSNumber(value: -1.0)
                        approveStatus = approveStatu
                        if approveStatus == .ACCEPTED {
                            sumOder = info.visitInfo?.grandTotal
                        }
                    }
                } else {
                    sumOder = info.visitInfo?.grandTotal
                }
                
                if numbersurveyAnswers == nil {
                    numbersurveyAnswers = info.surveyAnswers?.count
                }
                if numberFB == nil {
                    numberFB = info.feedbacks?.count
                }
                if idImage == nil {
                    idImage = info.photo
                }
            }
        }
        if sumOder == nil {
            if let data = self.save.value(forKey: CustomerContanst.KEY_ORDER) as? String {
                if let json = data.toJson() {
                    let entityOder = EntityCalculator(json: json)
                    
                    let sumMoneyN0 = entityOder.subTotal ?? NSNumber(value: 0)
                    let sumPromotion = entityOder.sumPromotion ?? NSNumber(value: 0)
                    let discountAmt = entityOder.discountAmt ?? NSNumber(value: 0)
                    sumOder = NSNumber(value: sumMoneyN0.int64Value - sumPromotion.int64Value - discountAmt.int64Value)
                }
            }
        }
        
        if let sumNumberOder = sumOder {
            if sumNumberOder.doubleValue != -1 {
                if sumNumberOder.int64Value > 0 {
                    self.lbNumberOder.text = sumNumberOder.toDecimalString()
                }
            } else {
                switch approveStatus {
                case .ACCEPTED:
                    self.lbNumberOder.text = "Đã duyệt".localized()
                    self.lbNumberOder.textColor = UIColor.success
                    break
                case .REJECTED:
                    self.lbNumberOder.text = "Từ chối".localized()
                    self.lbNumberOder.textColor = UIColor.error
                    break
                case .WAITING:
                    self.lbNumberOder.text = "Chờ duyệt".localized()
                    self.lbNumberOder.textColor = UIColor.info
                    break
                }
            }
        }
        if let url = save.value(forKey: CustomerContanst.IMAGE_PATH) as? String {
            let image = UIImage(contentsOfFile: url)
            imageView.image = image
            self.lblCamera.text = "XEM HÌNH".localized()
        } else if let idImg = idImage {
            imageView.setImage_Url(idImg, placeholder: "")
        }
        
        if let arrayFB = self.save.value(forKey: CustomerContanst.KEY_FEEDBACK) as? [String] {
            numberFB = arrayFB.count
        }
        
        if let count = numberFB {
            if count > 0 {
                lbNumberFeedback.text = "\(count)"
            }
        }
        
        if let sv = self.save.value(forKey: SurveyEntity.KEY_SURVEY) as? [String] {
            var surveyAnswers = [SurveyEntitySend]()
            for survey in sv {
                let op = SurveyEntitySend()
                op.options = []
                op.surveyId = survey
                if let svChoice = self.save.value(forKey: survey) as? [String] {
                    for select in svChoice {
                        op.options?.append(select)
                    }
                    surveyAnswers.append(op)
                }
            }
            numbersurveyAnswers = surveyAnswers.count
        }
        if let count = numbersurveyAnswers {
            if count > 0 {
                lbNumberSurvey.text = "\(count)"
            }
        }
    }
}
