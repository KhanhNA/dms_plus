//
//  VisitCustomerAllViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class VisitCustomerAllViewController: UIBaseViewController {
    
    @IBOutlet weak var uisearch: UIBaseSearchBar!
    @IBOutlet weak var tableView: UITableView!
    var array: [VisitCustomerEntity]?
    var model = VisitCustomerModel()
    var arrayall: [VisitCustomerEntity]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "TẤT CẢ".localized()
        let cell = UINib(nibName: "VisitCustomerTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        GetData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetData() {
        model.getListVisitCustomerAll(completion: {
            arrayItem in
            if let arrayItem = arrayItem {
                self.array = arrayItem
                self.arrayall = arrayItem
                self.tableView.reloadData()
            }
        })
    }
    
    func showAllMap() {
        let customer = UIStoryboard(name: "Customer", bundle: nil)
        let centerViewController = customer.instantiateViewController(withIdentifier: "mapcustomer") as? UINavibarMapCustomerViewController
        centerViewController?.list = arrayall
        self.mm_drawerController.centerViewController = centerViewController
    }
    
}
extension VisitCustomerAllViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: VisitCustomerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? VisitCustomerTableViewCell {
            let entity = array?[indexPath.row]
            cell.setDataAll(entity: entity)
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let entity = array?[indexPath.row]
        
        let storyboarOder = UIStoryboard(name: "OderList", bundle: nil)
        if let tabBarBaseViewController = storyboarOder.instantiateViewController(withIdentifier: "ProductTabarViewController") as? ProductTabarViewController {
            
            tabBarBaseViewController.url = BASEURL.GET.visit_customerAll
            tabBarBaseViewController.delegateProduct = self
            tabBarBaseViewController.modelSelectProductOrder.visitCustomerEntity = entity
            
            if entityUserinfo?.vanSales == true {
                
                let alert = UIAlertController(title: "Chọn hình thức bán hàng".localized(),
                                              message: nil,
                                              preferredStyle: .actionSheet)
                
                let alertExit = UIAlertAction(title: "Pre-Sale".localized(), style: .default) { (alertAction) in
                    self.present(tabBarBaseViewController, animated: true) {
                        if tabBarBaseViewController.entityCalculator == nil {
                            tabBarBaseViewController.entityCalculator = EntityCalculator()
                        }
                        tabBarBaseViewController.entityCalculator?.vanSales = false
                    }
                }
                
                let alertSend = UIAlertAction(title: "Van-Sale".localized(), style: .default) { (alertAction) in
                    self.present(tabBarBaseViewController, animated: true) {
                        if tabBarBaseViewController.entityCalculator == nil {
                            tabBarBaseViewController.entityCalculator = EntityCalculator()
                        }
                        tabBarBaseViewController.entityCalculator?.vanSales = true
                    }
                }
                
                let alertCancel = UIAlertAction(title: "Huỷ".localized(), style: .cancel)
                
                alert.addAction(alertExit)
                alert.addAction(alertSend)
                alert.addAction(alertCancel)
                
                self.present(alert, animated: true)
            } else {
                self.present(tabBarBaseViewController,
                             animated: true, completion: nil)
            }
        }
    }
}
extension VisitCustomerAllViewController {
    
    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let search = searchBar.text {
            if search.isEmpty {
                self.array = self.arrayall
                tableView.reloadData()
                return
            }
            self.array = self.arrayall?.filter({ (entity) -> Bool in
                return (entity.name?.uppercased().contains(search.uppercased()))!
            })
            tableView.reloadData()
        } else {
            self.array = self.arrayall
            tableView.reloadData()
        }
    }
}
extension VisitCustomerAllViewController: ProductTabarViewControllerDelegate {
    
    func saveOder(_ productTabarViewController: ProductTabarViewController, _ entityCalculator: EntityCalculator) {
        productTabarViewController.dismiss(animated: true, completion: nil)
        self.GetData()
    }
    
}
