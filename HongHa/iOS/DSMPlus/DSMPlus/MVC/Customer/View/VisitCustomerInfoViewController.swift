//
//  VisitCustomerInfoViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import SVProgressHUD
import ImagePicker

enum NSLocationType: NSInteger {
    case NOTON = 0
    case NOLocation = 1
    case LongDistance = 2
    case OK = -1
}

class VisitCustomerInfoViewController: UIBaseViewController {
    
    var info: VisitCustomerEntity?
    
    @IBOutlet weak var imageCustomer: UIImageView!
    
    @IBOutlet weak var lblName: UIBaseLabel!
    
    @IBOutlet weak var btnXemChiTiet: UIButton!
    
    @IBOutlet weak var lblSDT: UIBaseLabel!
    @IBOutlet weak var lblAddress: UIBaseLabel!
    
    @IBOutlet weak var lblLock: UILabel!
    
    @IBOutlet weak var lblStart: UIBaseLabel!
    
    @IBOutlet weak var lblActionStart: UIBaseLabel!
    
    @IBOutlet weak var lblStatus: UIBaseLabel!
    @IBOutlet weak var viewline: UIView!
    
    var locationManager = LocationManager.sharedInstance
    var locationMe: LocationSend?
    var locationType: NSLocationType = .NOTON
    
    var model = VisitCustomerModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNaviColor(UIColor.clear)
        self.tabBarController?.tabBar.isHidden = true
        setInfo()
        setLanguage()
        checkLocation()
    }
    
    func checkLocation() {
        locationManager.showVerboseMessage = true
        locationManager.autoUpdate = true
        locationManager.startUpdatingLocationWithCompletionHandler { (latitude, longitude, status, verboseMessage, error) -> () in
            if status != "Allowed access" {
                self.locationType = .NOTON
            } else if latitude != 0 && longitude != 0 {
                if self.locationMe == nil {
                    self.locationMe = LocationSend()
                }
                self.locationMe?.latitude = latitude
                self.locationMe?.longitude = longitude
                self.locationType = .OK
            } else {
                self.locationType = .NOLocation
            }
        }
    }
    
    func setInfo() {
        
        if let entiy = info {
            lblName.text = entiy.contact
            lblSDT.text = entiy.mobile
            lblAddress.text = entiy.name
        }
    }
    
    override func setLanguage() {
        lblStatus.text = "KHÁCH HÀNG ĐÓNG CỬA".localized()
        lblStatus.colorText = UIColor.navibar
        btnXemChiTiet.setTitle("XEM CHI TIẾT".localized(), for: .normal)
        lblStart.text = "BẮT ĐẦU GHÉ THĂM".localized()
        lblStart.textColor = UIColor.navibar
        viewline.backgroundColor = UIColor.navibar
        lblActionStart.colorText = UIColor.navibar
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func actionDetail(_ sender: Any) {
        let storyboard = UIStoryboard(name: "DetailCus", bundle: nil)
        if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "storyboardCusDetail") as? UITabBarDetailCustomerViewController {
            viewcontroler.entity = info
            self.showDetailViewController(viewcontroler, sender: nil)
        }
        
    }
    
    @IBAction func actionStartVisit(_ sender: Any) {
        calculateDistanceLocation { (locationType) in
            switch locationType {
            case .NOTON:
                self.checkLocation()
                break
            case .NOLocation:
                let aler = UIAlertController(title: "Cảnh báo".localized(), message: "Không xác định được vị trí hiện tại của bạn. Bạn vẫn muốn tiếp tục thao tác?".localized(), preferredStyle: .alert)
                let actionContinue = UIAlertAction(title: "Tiếp tục".localized(), style: .default ) { (_) -> Void in
                    self.startVisit(id: (self.info?.id)!, latitude: self.locationMe?.latitude, longitude: self.locationMe?.longitude)
                }
                aler.addAction(actionContinue)
                let actionCance = UIAlertAction(title: "Huỷ bỏ".localized(), style: .default, handler: nil)
                aler.addAction(actionCance)
                self.present(aler, animated: true, completion: nil)
                break
            case .LongDistance:
                let aler = UIAlertController(title: "Cảnh báo".localized(), message: "Khoảng cách quá xa".localized(), preferredStyle: .alert)
                let actionContinue = UIAlertAction(title: "Tiếp tục".localized(), style: .default ) { (_) -> Void in
                    self.startVisit(id: (self.info?.id)!, latitude: self.locationMe?.latitude, longitude: self.locationMe?.longitude)
                }
                aler.addAction(actionContinue)
                let actionCance = UIAlertAction(title: "Huỷ bỏ".localized(), style: .default, handler: nil)
                aler.addAction(actionCance)
                self.present(aler, animated: true, completion: nil)
                break
            case .OK:
                self.startVisit(id: (self.info?.id)!, latitude: self.locationMe?.latitude, longitude: self.locationMe?.longitude)
                break
            }
        }
    }
    
    fileprivate func calculateDistanceLocation(_ completion: @escaping (NSLocationType) -> Swift.Void) {
        self.checkLocation()
        switch self.locationType {
        case .NOTON:
            self.showLocationSetting("Huỷ".localized()) {b in
                if b == true {
                    self.checkLocation()
                } else {
                    completion(.NOLocation)
                }
            }
            break
        case .NOLocation:
            return completion(.NOLocation)
        case .LongDistance:
            return completion(.LongDistance)
        case .OK:
            if let latitudeMe = locationMe?.latitude,
                let longitudeMe = locationMe?.longitude,
                let latitudeInfo = info?.location?.latitude,
                let longitudeInfo = info?.location?.longitude {
                model.calculateDistance(originLat: latitudeMe,
                                        originLong: longitudeMe,
                                        destLat: latitudeInfo,
                                        destLong: longitudeInfo) {respone in
                                            if let far = entityUserinfo?.visitDistanceKPI {
                                                if far > (respone / 1000) {
                                                    return completion(.OK)
                                                }
                                            }
                                            return completion(.LongDistance)
                }
            } else {
                return completion(.LongDistance)
            }
            break
        }
    }
    
    func startVisit (id: String, latitude: Double?, longitude: Double?) {
        model.startVisit(cusid: id, latitude: latitude, longitude: longitude, completion: {
            respone in
            if respone != nil {
                let viewcontroler = VisitCustomerActionsViewController(nibName: "VisitCustomerActionsViewController", bundle: nil)
                viewcontroler.entity = self.info
                viewcontroler.isVisit = true
                viewcontroler.idVisit = respone
                self.navigationController?.pushViewController(viewcontroler, animated: true)
            }
        })
    }
    
    @IBAction func actionCloseCustomer(_ sender: Any) {
        calculateDistanceLocation { (locationType) in
            switch locationType {
            case .NOTON:
                self.checkLocation()
                break
            case .NOLocation:
                let aler = UIAlertController(title: "Cảnh báo".localized(), message: "Không xác định được vị trí hiện tại của bạn. Bạn vẫn muốn tiếp tục thao tác?".localized(), preferredStyle: .alert)
                let actionContinue = UIAlertAction(title: "Tiếp tục".localized(), style: .default ) { (_) -> Void in
                    self.actionClose()
                }
                aler.addAction(actionContinue)
                let actionCance = UIAlertAction(title: "Huỷ bỏ".localized(), style: .default, handler: nil)
                aler.addAction(actionCance)
                self.present(aler, animated: true, completion: nil)
                break
            case .LongDistance:
                let aler = UIAlertController(title: "Cảnh báo".localized(), message: "Khoảng cách quá xa".localized(), preferredStyle: .alert)
                let actionContinue = UIAlertAction(title: "Tiếp tục".localized(), style: .default ) { (_) -> Void in
                    self.actionClose()
                }
                aler.addAction(actionContinue)
                let actionCance = UIAlertAction(title: "Huỷ bỏ".localized(), style: .default, handler: nil)
                aler.addAction(actionCance)
                self.present(aler, animated: true, completion: nil)
                break
            case .OK:
                self.actionClose()
                break
            }
        }
    }
    
    func actionClose() {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        present(imagePickerController, animated: true) {
            imagePickerController.galleryView.alpha = 0
        }
    }
}

extension VisitCustomerInfoViewController: ImagePickerDelegate {
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "Customer", bundle: nil)
        if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "viewimagestoryboard") as? ViewImageViewController {
            viewcontroler.image = images[0]
            viewcontroler.isClose = true
            let loc = LocationSend()
            loc.latitude = self.locationMe?.latitude
            loc.longitude = self.locationMe?.longitude
            viewcontroler.localSen = loc
            viewcontroler.idCustomer = self.info
            self.navigationController?.pushViewController(viewcontroler, animated: true)
            
        }
    }
}
