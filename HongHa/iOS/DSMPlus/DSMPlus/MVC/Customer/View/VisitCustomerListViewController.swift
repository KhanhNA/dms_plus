//
//  VisitCustomerListViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class VisitCustomerListViewController: UIBaseViewController {
    
    @IBOutlet weak var uisearchBar: UIBaseSearchBar!
    @IBOutlet weak var tableVIew: UITableView!
    let model = VisitCustomerModel()
    var array: [VisitCustomerEntity]?
    var arrayall: [VisitCustomerEntity]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "HÔM NAY".localized()
        let cell = UINib(nibName: "VisitCustomerTableViewCell", bundle: nil)
        tableVIew.register(cell, forCellReuseIdentifier: "cell")
        tableVIew.tableFooterView = UIView()
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(VisitCustomerListViewController.showAllMap))
        barButtonItem.icon = "map"
        
        self.navigationItem.rightBarButtonItem = barButtonItem
        
        tableVIew.pullToRefresh {
            self.GetData()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.setNaviColor(UIColor.navibar)
    }
    
    func GetData() {
        model.getListVisitCustomer(completion: {
            arrayItem in
            if let arrayItem = arrayItem {
                //comment
//                self.array = arrayItem.sorted(by: {$0.0.visitStatus! < $0.1.visitStatus!})
//                self.arrayall = arrayItem.sorted(by: {$0.0.visitStatus! < $0.1.visitStatus!})
//                self.tableVIew.reloadData()
//                self.tableVIew.stopAnimating()
            }
        })
        
    }
    @objc func showAllMap() {
        
        let customer = UIStoryboard(name: "Customer", bundle: nil)
        let centerViewController = customer.instantiateViewController(withIdentifier: "mapcustomer") as? UINavibarMapCustomerViewController
        centerViewController?.list = arrayall
        self.showDetailViewController(centerViewController!, sender: nil)
        
    }
}

extension VisitCustomerListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: VisitCustomerTableViewCell = tableVIew.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? VisitCustomerTableViewCell {
            let entity = array?[indexPath.row]
            if entity?.visitStatus == 0 {
                model.startVisit(cusid: entity?.id, latitude: nil, longitude: nil, completion: {
                    respone in
                    if respone != nil {
                        let viewcontroler = VisitCustomerActionsViewController(nibName: "VisitCustomerActionsViewController", bundle: nil)
                        viewcontroler.entity = entity
                        viewcontroler.isVisit = false
                        viewcontroler.isStart = true
                        viewcontroler.delegate = self
                        viewcontroler.idVisit = respone
                        self.navigationController?.pushViewController(viewcontroler, animated: true)
                    }
                })
            }
            cell.setData(entity: entity)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entity = array?[indexPath.row]
        StartVisit(entity: entity)
    }
    func StartVisit (entity: VisitCustomerEntity?) {
        if entity?.visitStatus == 2 {
            let viewcontroler = VisitCustomerActionsViewController(nibName: "VisitCustomerActionsViewController", bundle: nil)
            viewcontroler.entity = entity
            viewcontroler.isVisit = false
            viewcontroler.isStart = true
            viewcontroler.delegate = self
            self.navigationController?.pushViewController(viewcontroler, animated: true)
            
        } else {
            let viewcontroler = VisitCustomerInfoViewController(nibName: "VisitCustomerInfoViewController", bundle: nil)
            viewcontroler.info = entity
            self.navigationController?.pushViewController(viewcontroler, animated: true)
        }
    }
}
extension VisitCustomerListViewController {
    
    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let search = searchBar.text {
            if search.isEmpty {
                self.array = self.arrayall
                tableVIew.reloadData()
                return
            }
            self.array = self.arrayall?.filter({ (entity) -> Bool in
                return (entity.name?.uppercased().contains(search.uppercased()))!
            })
            tableVIew.reloadData()
        } else {
            self.array = self.arrayall
            tableVIew.reloadData()
        }
    }
}
extension VisitCustomerListViewController: VisitCustomerActionsViewControllerDelegate {
    func reloadTable() {
        GetData()
    }
}
