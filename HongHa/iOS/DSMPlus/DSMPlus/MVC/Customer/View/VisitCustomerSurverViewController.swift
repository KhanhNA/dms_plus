//
//  VisitCustomerSurverViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/15/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class VisitCustomerSurverViewController: UIBaseViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    var array: [SurveyEntity]?
    
    var model = VisitCustomerModel()
    
    var cusId: String?
    var navi: NaviSurveyViewController? {
        
        return self.navigationController as? NaviSurveyViewController
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let close = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(VisitCustomerSurverViewController.dismis))
        close.icon = "clear"
        self.navigationItem.leftBarButtonItem = close
        
        self.navigationItem.title = "DANH SÁCH KHẢO SÁT".localized()
        
        let nib = UINib(nibName: "SurveyTableViewCell", bundle: nil)
        tableview.register(nib, forCellReuseIdentifier: "cell")
        tableview.tableFooterView = UIView()
        if let id = navi?.cusId {
            model.getListSurvey(cusId: id, completion: {
                respone in
                if let rp = respone {
                    if rp.count > 0 {
                        self.array = rp
                        self.tableview.reloadData()
                    }
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        tableview.reloadData()
    }
    override func naviMenuLeft() {
        
    }
    @objc func dismis() {
        self.dismiss(animated: true, completion: nil)
    }
}
extension VisitCustomerSurverViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: SurveyTableViewCell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? SurveyTableViewCell {
            let entity = array?[indexPath.row]
            var timeStart = ""
            var timeEnd = ""
            if let start = entity?.startDate {
                
                timeStart = start.toString(dateFormat: DateFormat.DDMMYYYY)!
            }
            if let end = entity?.endDate {
                
                timeEnd = end.toString(dateFormat: DateFormat.DDMMYYYY)!
            }
            var status = entity?.dateStatus
            if let ansv = navi?.listAnswer {
                
                for item in ansv {
                    
                    if entity?.id == item.surveyId {
                        
                        status = 0
                    }
                }
            }
            
            cell.setData(name: (entity?.name)!, time: "\(timeStart) - \(timeEnd)", count: entity?.questions?.count, status: status, surveyId: entity?.id)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entity = array?[indexPath.row]
        
        let story = UIStoryboard(name: "Customer", bundle: nil)
        let viewcontroler = story.instantiateViewController(withIdentifier: "navicontentsurvey") as? NaviContentSurveyViewController
        viewcontroler?.survey = entity
        viewcontroler?.isEndVisit = navi?.isEndVisit
        if let answers = navi?.listAnswer {
            
            if answers.count > 0 {
                
                viewcontroler?.listAnswer = answers
            }
        }
        self.present(viewcontroler!, animated: true, completion: nil)
        
    }
    
}
