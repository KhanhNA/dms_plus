//
//  CustomerNewSend.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/21/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class CustomerNewSend: BaseEntitySend {

    var address: String?
    var areaId: String?
    var code: String?
    var contact: String?
    var customerTypeId: String?
    var description: String?
    var email: String?
    var location: LocationSend?
    var mobile: String?
    var name: String?
    var phone: String?
    var photos: String?
}
