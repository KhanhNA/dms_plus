//
//  EntityType.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/21/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityType: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
    }
}
