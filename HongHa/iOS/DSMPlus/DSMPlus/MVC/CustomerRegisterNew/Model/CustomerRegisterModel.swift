//
//  CustomerRegisterModel.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class CustomerRegisterModel: BaseModel {
    
    var array = [VisitCustomerEntity]()
    var totalCount: Int = 0
    
    func getListCustomerRegister(page: Int64?, size: Int64?, q: String?, completion: @escaping ([VisitCustomerEntity]?) -> Swift.Void ) {
        
        var url = "\(BASEURL.GET.customer_register.url())"
        var key = ""
        var _page: Int64 = 1
        var _size = page_size
        if let txt = q {
            
            key = txt
        }
        if let p = page {
            
            _page = p
        }
        if let s = size {
            
            _size = s
        }
        
        url = url.replacingOccurrences(of: "{0}", with: "\(_page)")
        url = url.replacingOccurrences(of: "{1}", with: "\(_size)")
        url = url.replacingOccurrences(of: "{2}", with: key)
        
        Networking.GETUrl(url: url, paramater: nil, isShowProgress: true, completionHandler: {
            respone in
            
            if let json = respone?.data {
                
                if page == nil || page == 1 {
                    self.array.removeAll()
                }
                
                if let count = json["count"] as? Int {
                    self.totalCount = count
                }
                
                if let list = json["list"] as? [[String: Any]] {
                    
                    for item in list {
                        let entity = VisitCustomerEntity(JSON: item)
                        self.array.append(entity!)
                    }
                    
                    completion(self.array)
                    return
                }
            }
        })
        
    }
    func getCustomerType(completion: @escaping([EntityType]?) -> Swift.Void) {
        
        let url = "\(BASEURL.GET.customer_type.url())"
        Networking.GETUrl(url: url, completionHandler: { respone in
            if let json = respone?.data {
                
                var arayItem = [EntityType]()
                if let list = json["list"] as? [[String: Any]] {
                    
                    for item in list {
                        
                        let entity = EntityType(JSON: item)
                        arayItem.append(entity!)
                    }
                }
                completion(arayItem)
                return
            }
        })
    }
    func getCustomerArea(completion: @escaping([EntityType]?) -> Swift.Void) {
        let url = "\(BASEURL.GET.customer_area.url())"
        Networking.GETUrl(url: url) { respone in
            if let json = respone?.data {
                
                var arayItem = [EntityType]()
                if let list = json["list"] as? [[String: Any]] {
                    
                    for item in list {
                        
                        let entity = EntityType(JSON: item)
                        arayItem.append(entity!)
                    }
                }
                completion(arayItem)
                return
            }
        }
    }
    
    func createCustomer(param: CustomerNewSend?, completion: @escaping(String?) -> Swift.Void) {
        let url = "\(BASEURL.POST.create_new_customer.url())"
        Networking.POSTUrl(url: url, paramater: param?.toJSON(), isShowProgress: true) { respone in
            if let json = respone?.data {
                completion(json["id"] as? String)
            }
            return
        }
    }
}
