//
//  CustomerAddNewViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

class CustomerAddNewViewController: UIBaseViewController {
    
    @IBOutlet weak var lblName: UIBaseLabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblType: UIBaseLabel!
    @IBOutlet weak var txtTpe: UITextField!
    @IBOutlet weak var lblAddress: UIBaseLabel!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var lblPhone: UIBaseLabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lblNote: UIBaseLabel!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var lblCoDinh: UIBaseLabel!
    @IBOutlet weak var txtCoDinh: UITextField!
    @IBOutlet weak var lblEmail: UIBaseLabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblContact: UIBaseLabel!
    @IBOutlet weak var txtContact: UITextField!
    
    weak var popupController: PopupController?
    var tabBarCustomerAddNewViewController: TabBarCustomerAddNewViewController? {
        return self.tabBarController as? TabBarCustomerAddNewViewController
    }
    var areaId: String?
    var customerTypeId: String?
    var arrayCustomerType: [EntityType]?
    var arrayCustomerArea: [EntityType]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "THÔNG TIN KHÁCH HÀNG".localized()
        let butonback = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(CustomerAddNewViewController.dismis))
        butonback.icon = "keyboard_backspace"
        self.navigationItem.leftBarButtonItem = butonback
        txtNote.layer.borderColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0).cgColor
        txtNote.layer.borderWidth = 1.0
        txtNote.layer.cornerRadius = 5
        loadCustomerType()
        loadCustomerArea()
    }
    override func setLanguage() {
        lblName.text = "Tên khách hàng".localized()
        lblType.text = "Loại khách hàng".localized()
        lblAddress.text = "Khu vực".localized()
        lblPhone.text = "Di động".localized()
        lblCoDinh.text = "Cố định".localized()
        lblEmail.text = "Email".localized()
        lblContact.text = "Liên hệ".localized()
        lblNote.text = "Ghi chú".localized()
    }
    override func naviMenuRight() -> [UIBarButtonItem]? {
        let saveButton = UIBarButtonItem(title: "",
                                         style: .plain,
                                         target: tabBarCustomerAddNewViewController,
                                         action: #selector(TabBarCustomerAddNewViewController.sendData))
        saveButton.icon = "check"
        
        return [saveButton]
    }
    
    func loadCustomerType() {
        
        tabBarCustomerAddNewViewController?.model.getCustomerType { respone in
            
            if let list = respone {
                
                self.arrayCustomerType = list
                let itemDefault = list[0]
                self.txtTpe.text = itemDefault.name
                self.customerTypeId = itemDefault.id
                
            }
        }
        
    }
    func loadCustomerArea() {
        
        tabBarCustomerAddNewViewController?.model.getCustomerArea { respone in
            
            if let list = respone {
                
                self.arrayCustomerArea = list
                let itemDefault = list[0]
                self.txtAddress.text = itemDefault.name
                self.areaId = itemDefault.id
            }
        }
        
    }
    
    override func naviMenuLeft() {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    func accept() {
        
    }
    @IBAction func actionSelectCustomerType(_ sender: Any) {
        
        let selectCustomerTye = SelectCustomerTypeViewController(nibName: "SelectCustomerTypeViewController", bundle: nil)
        selectCustomerTye.array = self.arrayCustomerType
        selectCustomerTye.delegate = self
        popupController = PopupController.create(self).show(selectCustomerTye)
    }
    @IBAction func actionSelectAddress(_ sender: Any) {
        let selectLocation = SelectCustomerLocationViewController(nibName: "SelectCustomerLocationViewController", bundle: nil)
        selectLocation.array = arrayCustomerArea
        selectLocation.delegate = self
        popupController = PopupController.create(self).show(selectLocation)
    }
}

extension CustomerAddNewViewController: SelectCustomerTypeViewControllerDelegate {
    
    func didselect(item: EntityType?) {
        
        if let itemSelect = item {
            
            self.txtTpe.text = itemSelect.name
            self.customerTypeId = itemSelect.id
        }
        popupController?.dismiss()
        
    }
    func dismisPopup() {
        popupController?.dismiss()
    }
    
}

extension CustomerAddNewViewController: SelectCustomerLocationViewControllerDelegate {
    
    func dismisPop() {
        popupController?.dismiss()
    }
    func didSelect(item: EntityType?) {
        if let entity = item {
            
            self.txtAddress.text = entity.name
            self.areaId = entity.id
        }
        popupController?.dismiss()
    }
}
