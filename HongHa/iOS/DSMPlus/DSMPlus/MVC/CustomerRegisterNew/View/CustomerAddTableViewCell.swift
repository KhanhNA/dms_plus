//
//  CustomerAddTableViewCell.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class CustomerAddTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblPhone: UIBaseLabel!
    @IBOutlet weak var lblTimeCreate: UIBaseLabel!
    @IBOutlet weak var lblStatus: UIBaseLabel!
    @IBOutlet weak var lblRadio: UILabel!
    @IBOutlet weak var lblName: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setData(entity: VisitCustomerEntity?) {
        
        if let info = entity {
            lblName.text = info.name
            //lblTimeCreate.text = info.createdTime
            lblTimeCreate.text = info.createdTime?.toString()
            lblPhone.text = info.mobile
            
            if info.status == 0 {
                
                lblName.textColor = UIColor.navibar
                lblRadio.textColor = UIColor.navibar
                lblStatus.text = "Chờ phê duyệt".localized()
                
            } else if info.status == 1 {
                
                lblName.textColor = UIColor.text
                lblRadio.textColor = UIColor.text
                lblStatus.textColor = UIColor.navibar
                lblStatus.text = "Đã phê duyệt".localized()
            } else {
                
                lblName.textColor = UIColor.navibar
                lblRadio.textColor = UIColor.navibar
                lblStatus.text = "Bị từ chối".localized()
            }
        }
        
    }
    
}
