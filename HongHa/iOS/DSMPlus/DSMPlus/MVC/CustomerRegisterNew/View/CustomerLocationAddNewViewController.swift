//
//  CustomerLocationAddNewViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import SVProgressHUD

class CustomerLocationAddNewViewController: UIBaseViewController {
    
    @IBOutlet weak var maps: GMSMapView!
    var tabBarCustomerAddNewViewController: TabBarCustomerAddNewViewController? {
        return self.tabBarController as? TabBarCustomerAddNewViewController
    }
    var maker: GMSMarker?
    var localsend = LocationSend()
    var zoom: Float = 13
    var isAvailable = true
    var isDidSelectLocation = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "VỊ TRÍ KHÁCH HÀNG".localized()
        let butonback = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(CustomerLocationAddNewViewController.dismis))
        butonback.icon = "keyboard_backspace"
        self.navigationItem.leftBarButtonItem = butonback
        
        self.maps.delegate = self
        self.maps.isMyLocationEnabled = true
        self.maps.settings.myLocationButton = true
        self.maps.setMinZoom(1, maxZoom: 15)
        self.maps.animate(toZoom: Float(self.zoom))
        self.loadMaps()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarCustomerAddNewViewController?.checkLocation()
    }
    
    override func naviMenuLeft() {
        
    }
    
    override func naviMenuRight() -> [UIBarButtonItem]? {
        let saveButton = UIBarButtonItem(title: "",
                                         style: .plain,
                                         target: tabBarCustomerAddNewViewController,
                                         action: #selector(TabBarCustomerAddNewViewController.sendData))
        saveButton.icon = "check"
        
        return [saveButton]
    }
    
    @objc func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadMaps() {
        getAddresLocaltion()
    }
    
    @IBAction func actionZoom(_ sender: Any) {
        zoom -= 1
        maps.animate(toZoom: zoom)
    }
    @IBAction func actionZoomIn(_ sender: Any) {
        zoom += 1
        maps.animate(toZoom: zoom)
    }
}
extension CustomerLocationAddNewViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if maker == nil {
            maker = GMSMarker()
        }
        isDidSelectLocation = true
        let camera = GMSCameraPosition.camera(withLatitude:  coordinate.latitude,
                                              longitude: coordinate.longitude,
                                              zoom: 14)
        self.maps.camera = camera
        self.maker?.position = camera.target
        self.maker?.snippet = "VỊ TRÍ KHÁCH HÀNG".localized()
        self.maker?.appearAnimation = .pop
        self.maker?.map = self.maps
        self.localsend.latitude = coordinate.latitude
        self.localsend.longitude = coordinate.longitude
        if isAvailable {
            getAddresLocaltion()
        }
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        isDidSelectLocation = false
        if isAvailable {
            getAddresLocaltion()
        }
        if let latitude = localsend.latitude,
            let longitude = localsend.longitude {
            let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                                  longitude: longitude,
                                                  zoom: 14)
            self.maps.camera = camera
            self.maker?.position = camera.target
            self.maker?.snippet = "VỊ TRÍ KHÁCH HÀNG".localized()
            self.maker?.appearAnimation = .pop
            self.maker?.map = self.maps
        }
        return true
    }
    
    func getAddresLocaltion() {
        if let latitude = localsend.latitude,
            let longitude = localsend.longitude {
            
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: latitude, longitude: longitude)
            var address = ""
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) -> Void in
                if placemarks != nil {
                    if let  placeMark = placemarks?[0] {
                        if let locationName = placeMark.addressDictionary!["Name"] as? String {
                            address = address.appending(locationName)
                            address = address.appending(" - ")
                        }
                        if let street = placeMark.addressDictionary!["Thoroughfare"] as? String {
                            address = address.appending(street)
                            address = address.appending(" - ")
                        }
                        if let state = placeMark.addressDictionary!["State"] as? String {
                            address = address.appending(state)
                            address = address.appending(" - ")
                        }
                        if let city = placeMark.addressDictionary!["City"] as? String {
                            address = address.appending(city)
                            address = address.appending(" - ")
                        }
                        if let country = placeMark.addressDictionary!["Country"] as? String {
                            address = address.appending(country)
                        }
                        
                        let camera = GMSCameraPosition.camera(withLatitude: latitude,
                                                              longitude: longitude,
                                                              zoom: 14)
                        self.maps.camera = camera
                        self.maker?.position = camera.target
                        self.maker?.snippet = address
                        self.maker?.appearAnimation = .pop
                        self.maker?.map = self.maps
                    } else {
                        self.isAvailable = false
                    }
                } else {
                    self.isAvailable = false
                }
            }
        } else {
            Toast.showErr("Lỗi không xác định được vị trí".localized())
            self.isAvailable = false
        }
    }
}
