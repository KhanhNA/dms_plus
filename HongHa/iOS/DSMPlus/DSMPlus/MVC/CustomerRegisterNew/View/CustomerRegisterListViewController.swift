//
//  CustomerRegisterListViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class CustomerRegisterListViewController: UIBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchbar: UIBaseSearchBar!
    var model = CustomerRegisterModel()
    var page: Int64 = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "KHÁCH HÀNG ĐĂNG KÝ MỚI".localized()
        let butonAdd = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(CustomerRegisterListViewController.addNew))
        butonAdd.icon = "add_circle_outline"
        self.navigationItem.rightBarButtonItem = butonAdd
        let nib = UINib(nibName: "CustomerAddTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        tableView.pullToRefresh {
            self.page = 1
            self.getList()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getList() {
        
        var _page: Int64? = nil
        let _size: Int64? = nil
        var _q: String? = nil
        if self.page > 1 {
            
            if model.array.count < model.totalCount {
                
                _page = self.page
            } else {
                
                tableView.stopAnimating()
                return
            }
        }
        if self.searchbar.text?.isEmpty == false {
            
            _q = self.searchbar.text?.trimSpace()
        }
        model.getListCustomerRegister(page: _page, size: _size, q: _q, completion: { respone in
            if respone != nil {
                self.tableView.reloadData()
                self.page += 1
            }
            self.tableView.stopAnimating()
        })
    }
    
    @objc func addNew() {
        let storyboard = UIStoryboard(name: "CustomerRegister", bundle: nil)
        if let viewcontroler = storyboard.instantiateViewController(withIdentifier: "tabarnavicustomer") as?TabBarCustomerAddNewViewController {
            viewcontroler.delegateTab = self
            self.showDetailViewController(viewcontroler, sender: nil)
        }
    }
    
}
extension CustomerRegisterListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.array.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: CustomerAddTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? CustomerAddTableViewCell {
            let entity = model.array[indexPath.row]
            cell.setData(entity: entity)
            return cell
            
        }
        return UITableViewCell()
    }
}
extension CustomerRegisterListViewController: TabBarCustomerAddNewViewControllerDelegate {
    
    func reloadTable() {
        
        self.tableView.pullToRefresh {
            self.model.array.removeAll()
            self.page = 1
            self.getList()
            self.tableView.reloadData()
        }
        Toast.showSuccess("Thêm mới khách hàng thành công".localized())
        
    }
}
extension CustomerRegisterListViewController {
    
    override func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        page = 1
        getList()
        hidenKeyboard()
    }
    override func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchbar.text = ""
        page = 1
        getList()
        hidenKeyboard()
    }
}
