//
//  SelectCustomerLocationViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/21/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

protocol SelectCustomerLocationViewControllerDelegate: NSObjectProtocol {
    func dismisPop()
    func didSelect(item: EntityType?)
}
class SelectCustomerLocationViewController: UIBasePopupViewController {
    
    @IBOutlet weak var btnDismis: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var array: [EntityType]?
    weak var delegate: SelectCustomerLocationViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        lblTitle.text = "Chọn khu vực khách hàng".localized()
        btnDismis.backgroundColor = UIColor.navibar
        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableview.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionDismis(_ sender: Any) {
        delegate?.dismisPop()
    }
    
}
extension SelectCustomerLocationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let list = array {
            
            return list.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = array?[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect(item: array?[indexPath.row])
    }
}
