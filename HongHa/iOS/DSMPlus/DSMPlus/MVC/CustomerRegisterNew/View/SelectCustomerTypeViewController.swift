//
//  SelectCustomerTypeViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/21/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

protocol SelectCustomerTypeViewControllerDelegate: NSObjectProtocol {
    func dismisPopup()
    func didselect(item: EntityType?)
}

class SelectCustomerTypeViewController: UIBasePopupViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDismis: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var array: [EntityType]?
    weak var delegate: SelectCustomerTypeViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tableView.tableFooterView = UIView()
        btnDismis.backgroundColor = UIColor.navibar
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.reloadData()
        lblTitle.text = "Chọn loại khách hàng".localized()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionDismis(_ sender: Any) {
        
        delegate?.dismisPopup()
    }
    
}
extension SelectCustomerTypeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let list = array {
            return list.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = array?[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didselect(item: array?[indexPath.row])
    }
}
