//
//  TabBarCustomerAddNewViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import CoreLocation

protocol TabBarCustomerAddNewViewControllerDelegate: NSObjectProtocol {
    func reloadTable()
}

class TabBarCustomerAddNewViewController: UITabBarBaseViewController {
    
    var entitySend = CustomerNewSend()
    var model = CustomerRegisterModel()
    var customerAddNewViewController: CustomerAddNewViewController?
    var customerLocationAddNewViewController: CustomerLocationAddNewViewController?
    var locationManager = LocationManager.sharedInstance
    weak var delegateTab: TabBarCustomerAddNewViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let barItem = self.tabBar.items?[0]
        barItem?.title = "Thông tin".localized()
        barItem?.setIcon("info_outline")
        
        let barItemAll = self.tabBar.items?[1]
        barItemAll?.title = "Vị trí".localized()
        barItemAll?.setIcon("location_on")
        
        let navi = self.viewControllers?[0] as? UINaviBaseViewController
        customerAddNewViewController = navi?.viewControllers[0] as? CustomerAddNewViewController
        
        let navi1 = self.viewControllers?[1] as? UINaviBaseViewController
        customerLocationAddNewViewController = navi1?.viewControllers[0] as? CustomerLocationAddNewViewController
        checkLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkLocation() {
        locationManager.showVerboseMessage = true
        locationManager.autoUpdate = true
        locationManager.startUpdatingLocationWithCompletionHandler { (latitude, longitude, status, verboseMessage, error) -> () in
            if self.customerLocationAddNewViewController?.isDidSelectLocation == true {
                return
            }
            if status != "Allowed access" {
                self.showLocationSetting {_ in 
                    self.checkLocation()
                }
            } else if latitude != 0 && longitude != 0 {
                self.customerLocationAddNewViewController?.localsend.latitude = latitude
                self.customerLocationAddNewViewController?.localsend.longitude = longitude
            }
        }
    }
    
    @objc func sendData() {
        checkLocation()
        // name\
        let name = customerAddNewViewController?.txtName.text
        if (name?.isEmpty)! == false {
            entitySend.name = name
        } else {
            Toast.showErr("Bạn chưa nhập tên khách hàng".localized())
            customerAddNewViewController?.txtName.layer.borderColor = UIColor.red.cgColor
            return
        }
        entitySend.customerTypeId = customerAddNewViewController?.customerTypeId
        entitySend.areaId = customerAddNewViewController?.areaId
        let mobie = customerAddNewViewController?.txtPhone.text
        if (mobie?.isEmpty) == false {
            entitySend.mobile = mobie
        } else {
            Toast.showErr("Bạn chưa nhập số điện thoại khách hàng".localized())
            customerAddNewViewController?.txtPhone.layer.borderColor = UIColor.red.cgColor
            return
        }
        if let phone = customerAddNewViewController?.txtCoDinh.text {
            
            entitySend.phone = phone
        } else {
            
            entitySend.phone = nil
        }
        if let email = customerAddNewViewController?.txtEmail.text {
            
            entitySend.email = email
        } else {
            
            entitySend.email = nil
        }
        if let contact = customerAddNewViewController?.txtContact.text {
            
            entitySend.contact = contact
        } else {
            
            entitySend.contact = nil
        }
        if let note = customerAddNewViewController?.txtNote.text {
            
            entitySend.description = note
        } else {
            
            entitySend.description = nil
        }
        let location = LocationSend()
        if let lat = customerLocationAddNewViewController?.localsend.latitude,
            let lng = customerLocationAddNewViewController?.localsend.longitude {
            location.latitude = lat
            location.longitude = lng
        } else {
            Toast.showErr("Anh/chị vui lòng chọn vị trí khách hàng".localized())
            return
        }
        entitySend.location = location
        model.createCustomer(param: entitySend, completion: {
            
            respone in
            if let id = respone {
                
                if id.isEmpty == false {
                    self.delegateTab?.reloadTable()
                    self.customerLocationAddNewViewController?.dismis()
                }
            } else {
                
                Toast.showErr("Có lỗi xảy ra trong quá trình xử lý".localized())
            }
        })
    }
}
