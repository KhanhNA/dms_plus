//
//  EntityDBNgayBan.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/26/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityDBNgayBan: BaseEntity {
    var date: Date?
    var sales: Sales?
    var entityListOrders: [EntityListOrder]?
    var typeLoad: NSTypeLoad = .notLoad
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        date <- (map["date"], DateTransformCustom())
        sales <- map["salesResult"]
    }
}

class Sales: BaseEntity {
    var revenue: NSNumber?
    var subRevenue: NSNumber?
    var productivity: NSNumber?
    var nbOrder: NSNumber?
    var nbDistributor: NSNumber?
    var nbSalesman: NSNumber?
    var nbCustomer: NSNumber?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        revenue <- map["revenue"]
        subRevenue <- map["subRevenue"]
        productivity <- map["productivity"]
        nbOrder <- map["nbOrder"]
        nbDistributor <- map["customerType"]
        nbSalesman <- map["nbSalesman"]
        nbCustomer <- map["nbCustomer"]
    }
}
