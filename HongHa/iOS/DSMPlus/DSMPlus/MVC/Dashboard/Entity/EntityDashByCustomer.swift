//
//  EntityDashByCustomer.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/6/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

enum NSTypeLoad: NSInteger {
    case notLoad = -1
    case loaded = 1
    case loadHiden = 0
}

class EntityDashByCustomer: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    var area: ByCustomer?
    var customerType: ByCustomer?
    var salesResult: SalesResult?
    
    var entityListOrders: [EntityListOrder]?
    
    var typeLoad: NSTypeLoad = .notLoad
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
        area <- map["area"]
        customerType <- map["customerType"]
        salesResult <- map["salesResult"]
    }
}
class ByCustomer: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
    }
}
class SalesResult: BaseEntity {
    
    var revenue: NSNumber?
    var productivity: Int64?
    var nbOrder: Int64?
    var nbDistributor: Int64?
    var nbSalesman: Int64?
    var nbCustomer: Int64?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        revenue <- map["revenue"]
        productivity <- map["productivity"]
        nbOrder <- map["nbOrder"]
        nbDistributor <- map["nbDistributor"]
        nbSalesman <- map["nbSalesman"]
        nbCustomer <- map["nbCustomer"]
    }
    
}
