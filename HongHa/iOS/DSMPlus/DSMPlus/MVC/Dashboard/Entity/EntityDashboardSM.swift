//
//  EntityDashboardSM.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class DashboardBase: BaseEntity {
    
    var plan: NSNumber?
    var actual: NSNumber?
    var hasPlan: Bool?
    var percentage: Double?
    var remaining: Int64?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        plan <- map["plan"]
        actual <- map["actual"]
        percentage <- map["percentage"]
        remaining <- map["remaining"]
        hasPlan <- map["hasPlan"]
    }
}

class EntityDashboardSM: BaseEntity {
    
    var revenue: DashboardBase?
    var productivity: DashboardBase?
    var visit: DashboardBase?
    var day: DashboardBase?
    var subRevenue: DashboardBase?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        revenue <- map["revenue"]
        productivity <- map["productivity"]
        visit <- map["visit"]
        day <- map["day"]
        subRevenue <- map["subRevenue"]
    }
    
}
