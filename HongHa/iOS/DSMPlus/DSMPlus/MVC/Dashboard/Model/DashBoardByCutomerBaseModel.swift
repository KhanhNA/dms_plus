//
//  DashBoardByCutomerBaseModel.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/6/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DashBoardByCutomerBaseModel: BaseModel {

    func getDashboardByCustomer (completion: @escaping ([EntityDashByCustomer]?) -> Swift.Void) {
        
        let url = "\(BASEURL.GET.dashboard_by_customer.url())"
        Networking.GETUrl(url: url, paramater: nil) { response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    var entityDashByCustomers = [EntityDashByCustomer]()
                    for item in list {
                        let entityDashByCustomer = EntityDashByCustomer(JSON: item)
                        entityDashByCustomers.append(entityDashByCustomer!)
                    }
                    completion(entityDashByCustomers)
                    return
                }
            }
            completion(nil)
        }
    }
    
    class func getList(_ customerId: String, completion: @escaping ([EntityListOrder]?) -> Swift.Void) {
        let url = "\(BASEURL.GET.dashboard_by_customerID.url())\(customerId)"
        Networking.GETUrl(url: url, paramater: nil) {response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    var entityListOrders = [EntityListOrder]()
                    for item in list {
                        let entityListOrder = EntityListOrder(JSON: item)
                        entityListOrders.append(entityListOrder!)
                    }
                    completion(entityListOrders)
                    return
                }
            }
            completion(nil)
        }
    }

}
