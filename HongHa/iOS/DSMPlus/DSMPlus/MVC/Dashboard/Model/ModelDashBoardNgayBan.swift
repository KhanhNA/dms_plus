//
//  ModelDashBoardNgayBan.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/26/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelDashBoardNgayBan: BaseModel {
    class func getDashboardNgayBan(completion: @escaping ([EntityDBNgayBan]?) -> Swift.Void) {
        
        let url = "\(BASEURL.GET.dashboard_by_day.url())"
        Networking.GETUrl(url: url, paramater: nil) { response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    var entityDBNgayBans = [EntityDBNgayBan]()
                    for item in list {
                        let entityDBNgayBan = EntityDBNgayBan(JSON: item)
                        entityDBNgayBans.append(entityDBNgayBan!)
                    }
                    completion(entityDBNgayBans)
                    return
                }
            }
            completion(nil)
        }
    }
    
    class func getDashboardNgayBanDetail(_ entityDBNgayBan: EntityDBNgayBan, completion: @escaping ([EntityListOrder]?) -> Swift.Void) {
        
        if let date = entityDBNgayBan.date?.toString(dateFormat: .YYYYMMDD) {
            let url = "\(BASEURL.GET.dashboard_by_day_detail.url())\(date)"
            Networking.GETUrl(url: url, paramater: nil) { response in
                if let json = response?.data {
                    if let list = json["list"] as? [[String: Any]] {
                        var entityListOrders = [EntityListOrder]()
                        for item in list {
                            let entityListOrder = EntityListOrder(JSON: item)
                            entityListOrders.append(entityListOrder!)
                        }
                        completion(entityListOrders)
                        return
                    }
                }
                completion(nil)
            }
        }
    }
}
