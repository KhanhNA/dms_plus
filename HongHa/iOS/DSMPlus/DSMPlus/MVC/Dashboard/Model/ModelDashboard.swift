//
//  ModelDashboard.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelDashboard: BaseModel {
    
    var entityDashboardSM: EntityDashboardSM?
    
    func getDashboardSM(completion: @escaping (EntityDashboardSM?) -> Swift.Void) {
        
        let url = "\(BASEURL.GET.dashboard.url())"
        Networking.GETUrl(url: url, paramater: nil) { response in
            if let json = response?.data {
                self.entityDashboardSM = EntityDashboardSM(JSON: json)
                completion(self.entityDashboardSM)
            }
        }
    }
}
