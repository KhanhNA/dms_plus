//
//  DBByCustomerCollectionViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/21/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DBByCustomerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var circleLayerView: CircleLayerView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setData(_ dashboardBase: DashboardBase, _ string: String) {
        circleLayerView.title.text = string
        circleLayerView.number.text = dashboardBase.actual?.toDecimalString(true)
        circleLayerView.progress = CGFloat(dashboardBase.percentage ?? 0)
    }
}
