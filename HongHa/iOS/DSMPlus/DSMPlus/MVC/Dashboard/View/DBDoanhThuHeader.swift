//
//  DBDoanhThuHeader.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/23/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DBDoanhThuHeader: UITableViewHeaderFooterView {
    
    var lbJust = UIBaseLabel()
    var lbName = UIBaseLabel()
    var lbMobile = UIBaseLabel()
    var lbUpDown = UIBaseLabel()
    var viewF = UIView()
    
    var button = UIButton(type: UIButton.ButtonType.custom)
    
    var tableView: UITableView?
    
    fileprivate var entityDashByCustomer: EntityDashByCustomer?
    fileprivate var entityDBNgayBan: EntityDBNgayBan?
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        lbJust.textColor = UIColor.text
        lbName.textColor = UIColor.text
        lbMobile.textColor = UIColor.navibar
        lbUpDown.textColor = lbJust.textColor
        viewF.backgroundColor = #colorLiteral(red: 0.7233663201, green: 0.7233663201, blue: 0.7233663201, alpha: 0.3004786532)
        button.backgroundColor = UIColor.clear
        
        lbJust.font = FontFamily.MaterialIcons.Regular.font(size: 21)
        lbUpDown.font = lbJust.font
        
        lbJust.text = "adjust"
        
        lbJust.translatesAutoresizingMaskIntoConstraints = false
        lbName.translatesAutoresizingMaskIntoConstraints = false
        lbMobile.translatesAutoresizingMaskIntoConstraints = false
        lbUpDown.translatesAutoresizingMaskIntoConstraints = false
        viewF.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(lbJust)
        self.addSubview(lbName)
        self.addSubview(lbMobile)
        self.addSubview(lbUpDown)
        self.addSubview(viewF)
        self.addSubview(button)
        
        button.addTarget(self, action: #selector(DBDoanhThuHeader.openData), for: UIControl.Event.touchUpInside)
        
        let views = ["lbJust": lbJust,
                     "lbName": lbName,
                     "lbMobile": lbMobile,
                     "lbUpDown": lbUpDown,
                     "viewF": viewF]
        
        var allConstraints = [NSLayoutConstraint]()
        
        let iconVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-8-[lbJust(20)]",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += iconVerticalConstraints
        
        let iconHConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-8-[lbJust(20)]-5-[lbName]-2-[lbUpDown(20)]-5-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += iconHConstraints
        
        let lbmobileHConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-8-[lbJust(20)]-5-[lbMobile]-2-[lbUpDown]-5-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += lbmobileHConstraints
        
        let vConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-8-[lbName]-8-[lbMobile]-8-[viewF(1)]-2-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += vConstraints
        
        let lbUpDownvConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-8-[lbUpDown]-8-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += lbUpDownvConstraints
        
        let viewFHConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[viewF]-0-|",
            options: [],
            metrics: nil,
            views: views)
        allConstraints += viewFHConstraints
        
        let buttonHConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[button]-0-|",
            options: [],
            metrics: nil,
            views: ["button": button])
        allConstraints += buttonHConstraints
        
        let buttonVConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[button]-0-|",
            options: [],
            metrics: nil,
            views: ["button": button])
        allConstraints += buttonVConstraints
        
        NSLayoutConstraint.activate(allConstraints)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @objc func openData() {
        if entityDashByCustomer?.typeLoad == .notLoad ||
           entityDBNgayBan?.typeLoad == .notLoad {
            //call on internet
            if let entityDashByCustomer = self.entityDashByCustomer {
                if let id = entityDashByCustomer.id {
                    DashBoardByCutomerBaseModel.getList(id) { entityListOrders in
                        self.entityDashByCustomer?.entityListOrders  = entityListOrders
                        self.entityDashByCustomer?.typeLoad = .loaded
                        self.tableView?.reloadData()
                    }
                }
            } else if let entityDBNgayBan = self.entityDBNgayBan {
                ModelDashBoardNgayBan.getDashboardNgayBanDetail(entityDBNgayBan) { (entityListOrders) in
                    self.entityDBNgayBan?.entityListOrders  = entityListOrders
                    self.entityDBNgayBan?.typeLoad = .loaded
                    self.tableView?.reloadData()
                }
            }
        } else if entityDashByCustomer?.typeLoad == .loaded ||
                  entityDBNgayBan?.typeLoad == .loaded {
            self.entityDashByCustomer?.typeLoad = .loadHiden
            self.entityDBNgayBan?.typeLoad = .loadHiden
            self.tableView?.reloadData()
        } else if entityDashByCustomer?.typeLoad == .loadHiden ||
                  entityDBNgayBan?.typeLoad == .loadHiden {
            self.entityDashByCustomer?.typeLoad = .loaded
            self.entityDBNgayBan?.typeLoad = .loaded
            self.tableView?.reloadData()
        }
    }
    
    func setData(_ entityDashByCustomer: EntityDashByCustomer) {
        self.entityDashByCustomer = entityDashByCustomer
        lbName.text = entityDashByCustomer.name
        lbMobile.text = entityDashByCustomer.salesResult?.revenue?.toDecimalString()
        
        if entityDashByCustomer.typeLoad == .loaded {
            lbUpDown.text = "arrow_drop_down"
        } else {
            lbUpDown.text = "arrow_drop_up"
        }
    }
    
    func setData(_ entityDBNgayBan: EntityDBNgayBan) {
        self.entityDBNgayBan = entityDBNgayBan
        lbName.text = entityDBNgayBan.date?.toString(dateFormat: .DDMMYYYY)
        lbMobile.text = entityDBNgayBan.sales?.revenue?.toDecimalString()
        
        if entityDBNgayBan.typeLoad == .loaded {
            lbUpDown.text = "arrow_drop_down"
        } else {
            lbUpDown.text = "arrow_drop_up"
        }
    }
}
