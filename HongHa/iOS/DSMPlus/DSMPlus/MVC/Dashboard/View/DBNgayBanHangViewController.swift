//
//  DBNgayBanHangViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/6/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DBNgayBanHangViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var array: [EntityDBNgayBan] = [EntityDBNgayBan]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Kết quả theo ngày".localized()
        
        let nib = UINib(nibName: "OderListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "OderListTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.pullToRefresh {
            self.getData()
        }
    }

    func getData() {
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        ModelDashBoardNgayBan.getDashboardNgayBan(completion: { arrayItem in
            if let arrayItem = arrayItem {
                self.array = arrayItem
            }
            self.tableView.reloadData()
            self.tableView.stopAnimating()
        })
    }
}

//mark: - UITableViewDataSource, UITableViewDelegate
extension DBNgayBanHangViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let entity = array[section]
        
        if entity.typeLoad == .loaded {
            if let entityListOrders = entity.entityListOrders {
                return entityListOrders.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var sectionHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "DBDoanhThuHeader") as? DBDoanhThuHeader
        
        if sectionHeader == nil {
            sectionHeader = DBDoanhThuHeader(reuseIdentifier: "DBDoanhThuHeader")
        }
        let entity = array[section]
        sectionHeader?.setData(entity)
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "OderListTableViewCell", for: indexPath) as? OderListTableViewCell {
            let entity = array[indexPath.section]
            if let entityListOrders = entity.entityListOrders {
                let e = entityListOrders[indexPath.row]
                cell.setDataDB(e, indexPath.row + 1)
            }
            return cell
        }
        return UITableViewCell()
    }
}

import DZNEmptyDataSet
extension DBNgayBanHangViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        if array.count > 0 {
            return NSMutableAttributedString(string: "")
        }
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.text, NSAttributedString.Key.font: FontFamily.MaterialIcons.Regular.font(size: 30)]
        
        let partOne = NSMutableAttributedString(string: "do_not_disturb", attributes: attributes)
        return partOne
    }
    
    public func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        if array.count > 0 {
            return NSMutableAttributedString(string: "")
        }
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.text, NSAttributedString.Key.font: FontFamily.NotoSans.Bold.font(size: 18)]
        let partOne = NSMutableAttributedString(string: "Không có dữ liệu".localized(), attributes: attributes)
        return partOne
    }
    
    public func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    public func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
}
