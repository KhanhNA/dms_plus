//
//  DashboardDoanhThuViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/6/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DashboardDoanhThuViewController: UIBaseViewController {
    
    @IBOutlet weak var tableData: UITableView!
    let model = DashBoardByCutomerBaseModel()
    var array: [EntityDashByCustomer] = [EntityDashByCustomer]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Doanh thu".localized()
        
        let nib = UINib(nibName: "OderListTableViewCell", bundle: nil)
        tableData.register(nib, forCellReuseIdentifier: "OderListTableViewCell")
        tableData.tableFooterView = UIView()
        tableData.pullToRefresh {
            self.GetData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetData() {
        self.tableData.emptyDataSetSource = self
        self.tableData.emptyDataSetDelegate = self
        
        model.getDashboardByCustomer(completion: { arrayItem in
            if let arrayItem = arrayItem {
                self.array = arrayItem
            }
            self.tableData.reloadData()
            self.tableData.stopAnimating()
        })
        
    }
}

//mark: - UITableViewDataSource, UITableViewDelegate
extension DashboardDoanhThuViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let entity = array[section]
        
        if entity.typeLoad == .loaded {
            if let entityListOrders = entity.entityListOrders {
                return entityListOrders.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var sectionHeader = tableView.dequeueReusableHeaderFooterView(withIdentifier: "DBDoanhThuHeader") as? DBDoanhThuHeader
        
        if sectionHeader == nil {
            sectionHeader = DBDoanhThuHeader(reuseIdentifier: "DBDoanhThuHeader")
        }
        let entity = array[section]
        sectionHeader?.setData(entity)
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableData.dequeueReusableCell(withIdentifier: "OderListTableViewCell", for: indexPath) as? OderListTableViewCell {
            let entity = array[indexPath.section]
            if let entityListOrders = entity.entityListOrders {
                let e = entityListOrders[indexPath.row]
                cell.setDataDB(e)
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entity = array[indexPath.section]
        if let entityListOrders = entity.entityListOrders {
            let e = entityListOrders[indexPath.row]
            let infoOderViewController = InfoOderViewController(nibName: "InfoOderViewController", bundle: nil)
            infoOderViewController.idOder = e.id
            self.navigationController?.pushViewController(infoOderViewController, animated: true)
        }
    }
}

import DZNEmptyDataSet
extension DashboardDoanhThuViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        if array.count > 0 {
            return NSMutableAttributedString(string: "")
        }
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.text, NSAttributedString.Key.font: FontFamily.MaterialIcons.Regular.font(size: 30)]
        
        let partOne = NSMutableAttributedString(string: "do_not_disturb", attributes: attributes)
        return partOne
    }
    
    public func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        if array.count > 0 {
            return NSMutableAttributedString(string: "")
        }
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.text, NSAttributedString.Key.font: FontFamily.NotoSans.Bold.font(size: 18)]
        let partOne = NSMutableAttributedString(string: "Không có dữ liệu".localized(), attributes: attributes)
        return partOne
    }
    
    public func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    public func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView!) -> Bool {
        return false
    }
}
