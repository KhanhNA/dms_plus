//
//  DashboardUIBaseViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DashboardUIBaseViewController: UIBaseViewController {
    
    let modelDashboard = ModelDashboard()
    @IBOutlet weak var circleLayerView: CircleLayerView!
    @IBOutlet weak var circleLayerView2: CircleLayerView!
    @IBOutlet weak var dbValuesView: DBValuesView!
    @IBOutlet weak var dbValuesView2: DBValuesView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Thống kê chung".localized()
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.modelDashboard.getDashboardSM() { entityDB  in
            
            if let visit = self.modelDashboard.entityDashboardSM?.visit {
                self.circleLayerView.setData(visit, "Lượt Ghé Thăm".localized())
            }
            if let subRevenue = self.modelDashboard.entityDashboardSM?.subRevenue {
                self.circleLayerView2.setData(subRevenue, "Doanh Thu Trước CK".localized())
            }
            if let revenue = self.modelDashboard.entityDashboardSM?.revenue {
                self.dbValuesView.setData(revenue, "Doanh Thu".localized())
            }
            if let day = self.modelDashboard.entityDashboardSM?.day {
                self.dbValuesView2.setData(day, "Ngày Bán Hàng".localized())
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
