//
//  CircleLayerView.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class CircleLayerView: UIView {
    
    let π: CGFloat = CGFloat(M_PI)
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var title: UIBaseLabel!
    @IBOutlet weak var number: UIBaseLabel!
    @IBOutlet weak var progresslb: UIBaseLabel!
    
    var circleLayer: CAShapeLayer!
    
    @IBInspectable var progress: CGFloat = 50 {
        didSet {
            self.setNeedsDisplay()
            self.layoutIfNeeded()
        }
    }
    
    @IBInspectable var progressColor: UIColor = UIColor.navibar
    @IBInspectable var counterColor: UIColor = #colorLiteral(red: 0.9215686275, green: 0.937254902, blue: 0.9568627451, alpha: 1)
    
    func setData(_ dashboardBase: DashboardBase, _ string: String) {
        self.title.text = string
        self.number.text = dashboardBase.actual?.toDecimalString(true)
        self.progress = CGFloat(dashboardBase.percentage ?? 0)
    }
    
    override func draw(_ rect: CGRect) {
        
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        view.backgroundColor = UIColor.navibar
        
        //        super.draw(rect)
        // 1
        let center = CGPoint(x:bounds.width/2, y: bounds.height/2)
        
        // 2
        let radius: CGFloat = max(bounds.width, bounds.height)
        
        // 3
        let arcWidth: CGFloat = UIDevice.isIphone ? 15 : 30
        
        // 4
        let startAngle: CGFloat = 3 * π / 4
        let endAngle: CGFloat = π / 4
        
        // 5
        let path = UIBezierPath(arcCenter: center,
                                radius: radius/2 - arcWidth/2,
                                startAngle: startAngle,
                                endAngle: endAngle,
                                clockwise: true)
        
        // 6
        path.lineWidth = arcWidth
        path.lineCapStyle = .round
        counterColor.setStroke()
        path.stroke()
        
        //1 - first calculate the difference between the two angles
        //ensuring it is positive
        let angleDifference: CGFloat = 2 * π - startAngle + endAngle
        
        //then calculate the arc for each single glass
        let arcLengthPerGlass = angleDifference / 100
        
        //then multiply out by the actual glasses drunk
        var progress1 = progress
        if progress1 < 0 {
            progress1 = 0
        } else if progress1 > 100 {
            progress1 = 100
        }
        let outlineEndAngle = arcLengthPerGlass * progress1 + startAngle
        
        let path1 = UIBezierPath(arcCenter: center,
                                 radius: radius/2 - arcWidth/2,
                                 startAngle: startAngle,
                                 endAngle: outlineEndAngle,
                                 clockwise: true)
        
        // 6
        path1.lineWidth = arcWidth
        path1.lineCapStyle = .round
        progressColor.setStroke()
        path1.stroke()
        
        progresslb.text = String(format: "%.0f %%", progress)
    }
    
}
