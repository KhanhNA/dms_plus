//
//  DBValuesView.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/21/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class DBValuesView: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var title: UIBaseLabel!
    @IBOutlet weak var number: UIBaseLabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func setData(_ dashboardBase: DashboardBase, _ string: String) {
        self.title.text = string
        
        if let actual = dashboardBase.actual,
            let plan = dashboardBase.plan {
            self.number.text = "\(actual.toDecimalString(true))/\(plan.toDecimalString(true))"
        } else {
            self.number.text = ""
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        view.backgroundColor = UIColor.navibar
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        
        title.textColor = #colorLiteral(red: 0.3568627451, green: 0.3882352941, blue: 0.4196078431, alpha: 1)
        number.textColor = #colorLiteral(red: 0.1960784314, green: 0.2352941176, blue: 0.2745098039, alpha: 1)
        
        imageView.image = imageView.image!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.navibar

    }
}
