//
//  EntityCheckInSend.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/23/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class EntityCheckInSend: BaseEntitySend {
    
    var location: LocationSend?
    var note: String?
    var photos: [String]?
}
