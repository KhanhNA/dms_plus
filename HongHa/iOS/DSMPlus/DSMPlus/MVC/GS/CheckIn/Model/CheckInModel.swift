//
//  CheckInModel.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/23/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class CheckInModel: BaseModel {
    
    func checkIn(param: EntityCheckInSend?, completion: @escaping(Int64) -> Swift.Void) {
        
        let url = "\(BASEURL.POST.check_in.url())"
        Networking.POSTUrl(url: url, paramater: param?.toJSON(), isShowProgress: true, completionHandler: {
            
            respone in
            if let json = respone?.data {
                
                if let code = json["code"] as? Int64 {
                    completion(code)
                } else {
                    completion(0)
                }
                return
            }
        })
    }
    func getDetailCheckIn(id: String, completion: @escaping(SupervisorBaseEntity?) -> Swift.Void) {
        
        var url = "\(BASEURL.GET.check_in.url())"
        url = url.replacingOccurrences(of: "{0}", with: id)
        Networking.GETUrl(url: url, completionHandler: {
            repone in
            if let json = repone?.data {
                
                if let entity = SupervisorBaseEntity(JSON: json) {
                    completion(entity)
                }
                return
            }
        })
    }

}
