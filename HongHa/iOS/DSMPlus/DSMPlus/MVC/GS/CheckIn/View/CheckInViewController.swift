//
//  CheckInViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/23/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class CheckInViewController: UIBaseViewController {
    
    @IBOutlet weak var txtMes: UITextField!
    @IBOutlet weak var maps: GMSMapView!
    //    var locationManager = CLLocationManager()
    var locationEntity = LocationSend()
    var zoom = 13
    var isRending = false
    var tabbar: TabBarCheckInViewController? {
        
        return self.tabBarController as? TabBarCheckInViewController
    }
    var locationManager = LocationManager.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "VỊ TRÍ".localized()
        self.txtMes.placeholder = "Nhập ý kiến".localized()
        maps.settings.myLocationButton = true
        maps.isMyLocationEnabled = true
        maps.delegate = self
        checkLocation()
    }
    @objc func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    func checkLocation() {
        locationManager.showVerboseMessage = true
        locationManager.autoUpdate = true
        locationManager.startUpdatingLocationWithCompletionHandler { (latitude, longitude, status, verboseMessage, error) -> () in
            if status != "Allowed access" {
                self.showLocationSetting {_ in 
                    self.checkLocation()
                }
            } else if latitude != 0 && longitude != 0 {
                self.locationEntity.latitude = latitude
                self.locationEntity.longitude = longitude
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func naviMenuRight() -> [UIBarButtonItem]? {
        if tabbar?.isDetail != nil && (tabbar?.isDetail)! {
            
            return []
        }
        let sendButton = UIBarButtonItem(title: "", style: .plain, target: tabbar, action: #selector(TabBarCheckInViewController.sendData))
        sendButton.icon = "send"
        return [sendButton]
    }
    @IBAction func actionZoomIn(_ sender: Any) {
        if zoom == 20 {
            
            return
        }
        zoom += 1
        maps.animate(toZoom: Float(self.zoom))
    }
    @IBAction func actionZoomOut(_ sender: Any) {
        if zoom == 1 {
            
            return
        }
        zoom -= 1
        maps.animate(toZoom: Float(self.zoom))
    }
}
extension CheckInViewController: GMSMapViewDelegate {
    
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        if !isRending {
            if tabbar?.isDetail != nil && (tabbar?.isDetail)! {
                
                let buttonBack = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(CheckInViewController.dismis))
                buttonBack.icon = "keyboard_backspace"
                self.navigationItem.leftBarButtonItem = buttonBack
                isRending = true
                
            } else {
                
                if let lat = locationEntity.latitude,
                    let lng = locationEntity.longitude {
                    locationEntity.latitude = lat
                    locationEntity.longitude = lng
                    let camre = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: Float(zoom))
                    maps.camera = camre
                    let marker =  GMSMarker()
                    marker.position = camre.target
                    marker.snippet = "Vị trí hiện tại".localized()
                    marker.map = maps
                    isRending = true
                    
                }
            }
        }
    }
}
