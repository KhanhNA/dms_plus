//
//  ImageCollectionViewCell.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/23/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var activityLoading: UIActivityIndicatorView!
    @IBOutlet weak var imgaview: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activityLoading.startAnimating()
    }

}
