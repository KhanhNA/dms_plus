//
//  ListImageViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/23/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ImagePicker
import PopupController

class ListImageViewController: UIBaseViewController {
    
    weak var popupController: PopupController?
    @IBOutlet weak var collectionView: UICollectionView!
    var array: [UIImage]?
    var arrayIdPhoto: [String]?
    var tabbar: TabBarCheckInViewController? {
        
        return self.tabBarController as? TabBarCheckInViewController
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "HÌNH ẢNH".localized()
        let nib = UINib(nibName: "ImageCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "cell")
        if (tabbar?.isDetail)! {
            
            let back = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ListImageViewController.dismis))
            back.icon = "keyboard_backspace"
            self.navigationItem.leftBarButtonItem = back
            if let arraphoto = tabbar?.output?.photos {
                if arraphoto.count > 0 {
                    
                    self.arrayIdPhoto = arraphoto
                    self.collectionView.reloadData()
                    
                }
            }
        }
    }
    @objc func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func naviMenuRight() -> [UIBarButtonItem]? {
        if (tabbar?.isDetail)! {
            
            return []
        }
        let buttSend = UIBarButtonItem(title: "", style: .plain, target: tabbar, action: #selector(TabBarCheckInViewController.sendData))
        buttSend.icon = "send"
        let buttonImage = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ListImageViewController.takeCamera))
        buttonImage.icon = "photo_camera"
        return [buttSend, buttonImage]
    }
    
    @objc func takeCamera() {
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 10
        present(imagePickerController, animated: true, completion: nil)
    }
}
extension ListImageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width/3 - 20, height: width/3 - 20)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let arrayIdPhoto = self.arrayIdPhoto {
            
            return arrayIdPhoto.count
        }
        if let imgs = self.array {
            return imgs.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? ImageCollectionViewCell {
            if let arrayIdPhoto = self.arrayIdPhoto {
                cell.imgaview.setImage_Url(arrayIdPhoto[indexPath.row], placeholder: "")
            } else if let imgs = self.array {
                cell.imgaview.image = imgs[indexPath.row]
            }
            
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let viewImg = PopupViewImageViewController(nibName: "PopupViewImageViewController", bundle: nil)
        if self.arrayIdPhoto != nil {
            let img = self.arrayIdPhoto?[indexPath.row]
            viewImg.idImg = img
            
        } else {
            
            let imag = array?[indexPath.row]
            viewImg.image = imag
            
        }
        viewImg.delegate = self
        popupController = PopupController.create(self).show(viewImg)
    }
}
extension ListImageViewController: ImagePickerDelegate {
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        self.array = images
        self.collectionView.reloadData()
    }
    
}
extension ListImageViewController: PopupViewImageViewControllerDelegate {
    
    func popDismis() {
        popupController?.dismiss()
    }
}
