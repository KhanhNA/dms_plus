//
//  PopupViewImageViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/23/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
protocol PopupViewImageViewControllerDelegate: NSObjectProtocol {
    func popDismis()
}

class PopupViewImageViewController: UIBasePopupViewController {
    
    @IBOutlet weak var imageview: UIImageView!
    var image: UIImage?
    weak var delegate: PopupViewImageViewControllerDelegate?
    var idImg: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let im = idImg {
            imageview.setImage_Url(im, placeholder: "")
        } else {
            imageview.image = image
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionClose(_ sender: Any) {
        delegate?.popDismis()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
