//
//  TabBarCheckInViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/23/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import GoogleMaps

class TabBarCheckInViewController: UITabBarBaseViewController {
    
    var checkInViewController: CheckInViewController?
    var listImageViewController: ListImageViewController?
    var model = CheckInModel()
    var arrayId = [String]()
    var countImage = 0
    var isDetail: Bool = false
    var output: SupervisorBaseEntity?
    var input: SupervisorBaseEntity?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let local = self.tabBar.items?[0]
        local?.title = "Vị trí".localized()
        local?.setIcon("location_on")
        
        let image = self.tabBar.items?[1]
        image?.title = "Chụp hình".localized()
        image?.setIcon("linked_camera")
        
        let naviCheckIn = self.viewControllers?[0] as? UINaviBaseViewController
        checkInViewController = naviCheckIn?.viewControllers[0] as? CheckInViewController
        
        let naviImg = self.viewControllers?[1] as? UINaviBaseViewController
        listImageViewController = naviImg?.viewControllers[0] as? ListImageViewController
        if isDetail {
            
            getDetailCheckIn()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getDetailCheckIn() {
        
        if let info = input {
            
            model.getDetailCheckIn(id: info.id!, completion: { respone in
                if let data = respone {
                    self.output = data
                    self.checkInViewController?.txtMes.text = data.note
                    self.checkInViewController?.txtMes.isEnabled = false
                    if let lat =  data.location?.latitude,
                        let lng = data.location?.longitude {
                        let camre = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 13)
                        self.checkInViewController?.maps.camera = camre
                        let marker =  GMSMarker()
                        marker.position = camre.target
                        marker.snippet = "Vị trí check in".localized()
                        marker.map = self.checkInViewController?.maps
                    }
                }
            })
            
        }
        
    }
    @objc func sendData() {
        
        if (checkInViewController?.txtMes.text?.isEmpty)! {
            
            Toast.showErr("Anh/ chị chưa nhập nội dung check in".localized())
            return
        }
        
        if (listImageViewController?.array) != nil {
            if (listImageViewController?.array?.count)! > 0 {
                
                upImage()
            }
        } else {
            done()
        }
    }
    
    func upImage() {
        
        if self.countImage >= (self.listImageViewController?.array?.count)! {
            self.done()
            return
        }
        
        let dataImg = (listImageViewController?.array?[countImage])!.jpegData(compressionQuality: 1)
        let length =  NSData(data: dataImg!).length / 1024
        let imageView = UIImageView()
        if length < 200 {
            
            imageView.image = listImageViewController?.array?[countImage]
            
        } else {
            imageView.image = listImageViewController?.array?[countImage].resizeWith(percentage: ConfigSizeImage)
            
        }
        UploadImage.UploadRequest(image: imageView, name: "GSBH_", completion: { respone in
            self.arrayId.append(respone!)
            self.countImage += 1
            self.upImage()
        })
    }
    fileprivate func done() {
        let sendEntity = EntityCheckInSend()
        sendEntity.note = checkInViewController?.txtMes.text
        sendEntity.photos = self.arrayId
        sendEntity.location = checkInViewController?.locationEntity
        self.model.checkIn(param: sendEntity, completion: {
            
            repone in
            if repone == 200 {
                
                Toast.showSuccess("Check in thành công".localized())
            } else {
                
                Toast.showErr("Check in không thành công".localized())
                
            }
        })
    }
}

