//
//  SupervisorBaseEntity.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/26/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class SupervisorBaseEntity: BaseEntity {
    
    var id: String?
    var createdBy: CreatedBy?
    var createdTime: Date?
    var note: String?
    var nbPhoto: Int64?
    var location: EntityLocation?
    var province: Province?
    var photos: [String]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        id <- map["id"]
        createdBy <- map["createdBy"]
        createdTime <- (map["createdTime"], DateTransformCustom())
        note <- map["note"]
        nbPhoto <- map["nbPhoto"]
        location <- map["location"]
        province <- map["province"]
        photos <- map["photos"]
    }
}
class Province: BaseEntity {
    var id: String?
    var name: String?
    var code: String?
    required init?(map: Map) {
        super.init(map: map)
    }
    override func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
    }
}
