//
//  SupervisorModel.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/26/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class SupervisorModel: BaseModel {
    
    var arrayList = [SupervisorBaseEntity]()
    var total: Int = 0
    
    func getListHistoryCheckIn(page: Int64?, size: Int64?, completion: @escaping([SupervisorBaseEntity]?) -> Swift.Void) {
        
        if page == nil || page == 1 {
            
            arrayList.removeAll()
        }
        var url = "\(BASEURL.GET.list_supervisor.url())"
        var _page = 1
        if let p = page {
            
            _page = Int(p)
        }
        var _size = page_size
        if let s = size {
            
            _size = s
        }
        
        url = url.replacingOccurrences(of: "{0}", with: "\(_page)")
        url = url.replacingOccurrences(of: "{1}", with: "\(_size)")
        print(url)
        Networking.GETUrl(url: url, completionHandler: {
            repone in
            if let json = repone?.data {
                if json["list"] != nil {
                    if let count = json["count"] as? Int {
                        self.total = count
                    }
                    
                    if let list = json["list"] as? [[String: Any]] {
                        for item in list {
                            
                            let entity = SupervisorBaseEntity(JSON: item)
                            self.arrayList.append(entity!)
                        }
                        completion(self.arrayList)
                        return
                    }
                }
            }
            
        })
    }
}
