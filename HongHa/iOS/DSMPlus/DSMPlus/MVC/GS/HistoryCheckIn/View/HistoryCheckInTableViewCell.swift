//
//  HistoryCheckInTableViewCell.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/26/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class HistoryCheckInTableViewCell: UITableViewCell {

    @IBOutlet weak var lblContent: UIBaseLabel!
    @IBOutlet weak var lblTime: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(entity: SupervisorBaseEntity?) {
    
        if let info = entity {
        
            lblTime.text = info.createdTime?.toString()
            lblContent.text = info.note
        } else {
        
            lblTime.text = ""
            lblContent.text = ""
        }
    }
}
