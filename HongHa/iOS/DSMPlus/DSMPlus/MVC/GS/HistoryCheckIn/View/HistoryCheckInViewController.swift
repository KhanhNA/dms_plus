//
//  HistoryCheckInViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/26/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class HistoryCheckInViewController: UIBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var model = SupervisorModel()
    var page: Int64 = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "HistoryCheckInTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        tableView.pullToRefresh {
            self.model.getListHistoryCheckIn(page: 1, size: nil, completion: {
                respone in
                if respone != nil {
                    
                    self.tableView.reloadData()
                    self.page += 1
                }
                self.tableView.stopAnimating()
            })
        }
        tableView.addInfiniteScrolling {
            
            var _page: Int64? = self.page
            let _size: Int64? = page_size
            if self.page > 1 {
                if self.model.arrayList.count < self.model.total {
                    
                    _page = self.page
                    
                } else {
                    
                    self.tableView.infiniteScrollingView.stopAnimating()
                    return
                }
            }
            self.model.getListHistoryCheckIn(page: _page, size: _size, completion: {
                respone in
                if respone != nil {
                    self.tableView.reloadData()
                    self.page += 1
                }
                self.tableView.infiniteScrollingView.stopAnimating()
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension HistoryCheckInViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.arrayList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? HistoryCheckInTableViewCell {
            let entity = self.model.arrayList[indexPath.row]
            cell.setData(entity: entity)
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let entity = model.arrayList[indexPath.row]
        let story = UIStoryboard(name: "CheckIn", bundle: nil)
        let checkIn = story.instantiateViewController(withIdentifier: "tabbarCheckIn") as? TabBarCheckInViewController
        checkIn?.isDetail = true
        checkIn?.input = entity
        self.showDetailViewController(checkIn!, sender: nil)
    }
}
