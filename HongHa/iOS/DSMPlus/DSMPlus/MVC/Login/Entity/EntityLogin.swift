//
//  EntityLogin.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityLogin: BaseEntity {
    var access_token: String?
    var token_type: String?
    var refresh_token: String?
    var expires_in: Int?
    var scope: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        access_token <- map["access_token"]
        token_type <- map["token_type"]
        refresh_token <- map["refresh_token"]
        expires_in <- map["expires_in"]
        scope <- map["scope"]
    }

}
