//
//  EntityLogin.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityLoginSend: Mappable {
    var grant_type = "password"
    var client_id = "dmsplus"
    var client_secret = "secret"
    var username = ""
    var password = ""
    
    required init?() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        grant_type <- map["grant_type"]
        client_id <- map["client_id"]
        client_secret <- map["client_secret"]
        username <- map["username"]
        password <- map["password"]
    }
}
class EntityChangePass: BaseEntitySend {
    var newPassword: String?
    var oldPassword: String?
}
