//
//  EntityUserinfo.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

enum NSRoleCode: String {
    case NULL = ""
    case SM = "SM"
    case SUP = "SUP"
}

class EntityUserinfo: BaseEntity {

    var id: String?
    var username: String?
    var fullname: String?
    var roleCode: NSRoleCode?
    var clientCode: String?
    var clientName: String?
    var clientAddress: String?
    var clientTaxCode: String?
    var clientTel: String?
    var vanSales: Bool?
    var languages: [String]?
    var productPhoto: String?
    var numberDayOrderPendingExpire: Int?
    var orderDateType: String?
    var visitDistanceKPI: Double?
    var visitDurationKPI: Int64?
    var canEditCustomerLocation: Bool?
    var productivityUnit: String?
    var modules: [String]?
    var numberWeekOfFrequency: Int?
    var dateFormat: String?
    var firstDayOfWeek: Int?
    var minimalDaysInFirstWeek: Int?
    var location: EntityLocation?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        username <- map["username"]
        fullname <- map["fullname"]
        roleCode <- map["roleCode"]
        clientCode <- map["clientCode"]
        clientName <- map["clientName"]
        clientAddress <- map["clientAddress"]
        clientTaxCode <- map["clientTaxCode"]
        clientTel <- map["clientTel"]
        vanSales <- map["vanSales"]
        languages <- map["languages"]
        productPhoto <- map["productPhoto"]
        numberDayOrderPendingExpire <- map["numberDayOrderPendingExpire"]
        orderDateType <- map["orderDateType"]
        visitDistanceKPI <- map["visitDistanceKPI"]
        visitDurationKPI <- map["visitDurationKPI"]
        canEditCustomerLocation <- map["canEditCustomerLocation"]
        productivityUnit <- map["productivityUnit"]
        modules <- map["modules"]
        numberWeekOfFrequency <- map["numberWeekOfFrequency"]
        dateFormat <- map["dateFormat"]
        firstDayOfWeek <- map["firstDayOfWeek"]
        minimalDaysInFirstWeek <- map["minimalDaysInFirstWeek"]
        location <- map["location"]
    }
}
