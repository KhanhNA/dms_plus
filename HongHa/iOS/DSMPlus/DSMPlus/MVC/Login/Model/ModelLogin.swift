//
//  ModelLogin.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelLogin: BaseModel {
    class func postOauthToken(entityLoginSend: EntityLoginSend, completion: @escaping (EntityLogin?, EntityUserinfo?) -> Swift.Void) {
        
        entityLogin = nil
        entityUserinfo = nil
        
        let url = "\(BASEURL.POST.oauth_token.url())\(entityLoginSend.toJSON().stringFromHttpParameters())"
        Networking.POSTUrl(url: url) { response in
            if let json = response?.data {
                entityLogin = EntityLogin(JSON: json)
            } else if (response?.error) != nil {
                return
            }
            ModelLogin.getOauthUserinfo(completion: { (entityUserinfo) in
                if let entityUserinfo = entityUserinfo {
                    if entityUserinfo.roleCode != nil {
                        completion(entityLogin, entityUserinfo)
                        return
                    }
                    completion(nil, nil)
                }
                completion(nil, nil)
            })
        }
    }
    
    class func getOauthUserinfo(completion: @escaping (EntityUserinfo?) -> Swift.Void) {
        let url = "\(BASEURL.GET.oauth_userinfo.url())"
        Networking.GETUrl(url: url, paramater: nil) {response in
            if let json = response?.data {
                entityUserinfo = EntityUserinfo(JSON: json)
            }
            completion(entityUserinfo)
        }
    }
    
    class func validateLogin(username: String?, password: String?) -> Bool {
    
        if username == nil {
           Toast.showErr("Tên đăng nhập không được để trống".localized())
            return false
        }
        if username?.isEmpty == true {
            Toast.showErr("Tên đăng nhập không được để trống".localized())
            return false
        }
        if password == nil {
            Toast.showErr("Mật khẩu không được để trống".localized())
            return false
        }
        if password?.isEmpty == true {
            Toast.showErr("Mật khẩu không được để trống".localized())
            return false
        }
        
        return true
    }
    
    class func changePassword(oldPassword: String, newPassword: String, completion: @escaping(Int64) -> Swift.Void) {
    
        let url = "\(BASEURL.PUT.change_password.url())"
        let send = EntityChangePass()
        send.newPassword = newPassword
        send.oldPassword = oldPassword
        Networking.PUTUrl(url: url, paramater: send.toJSON(), isShowProgress: true, completionHandler: {
        
            respone in
            if let json = respone?.data {
            
                if json["code"] != nil {
                    
                    if let code = json["code"] as? Int64 {
                    
                       completion(code)
                    }
                
                } else {
                
                    completion(0)
                }
                return
            }
        })
        
    }
    
}
