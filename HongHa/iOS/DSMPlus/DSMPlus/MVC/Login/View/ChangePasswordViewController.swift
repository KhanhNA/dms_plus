//
//  ChangePasswordViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/6/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIBaseViewController {
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var txtReNewPassWord: UITextField!
    @IBOutlet weak var txtNewPassWord: UITextField!
    @IBOutlet weak var txtOldPassWord: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "ĐỔI MẬT KHẨU".localized()
        txtOldPassWord.placeholder = "Nhập mật khẩu cũ".localized()
        txtNewPassWord.placeholder = "Nhập mật khẩu mới".localized()
        txtReNewPassWord.placeholder = "Nhập lại mật khẩu mới".localized()
        btnAccept.setTitle("Xác nhận".localized(), for: .normal)
        btnAccept.layer.cornerRadius = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionChangePassWord(_ sender: Any) {
        
        if (txtOldPassWord.text?.isEmpty)! {
            
            Toast.showErr("Anh/chị chưa nhập mật khẩu cũ".localized())
            txtOldPassWord.becomeFirstResponder()
            return
        }
        if (txtNewPassWord.text?.isEmpty)! {
            
            Toast.showErr("Anh/chị chưa nhập mật khẩu mới".localized())
            txtNewPassWord.becomeFirstResponder()
            return
        }
        if (txtReNewPassWord.text?.isEmpty)! {
            
            Toast.showErr("Anh/chị chưa nhập lại mật khẩu mới".localized())
            txtReNewPassWord.becomeFirstResponder()
            return
        }
        if !(txtReNewPassWord.text?.trimSpace().isEqualToString(find: (txtNewPassWord.text?.trimSpace())!))! {
            
            Toast.showErr("Mật khẩu mới nhập lại không đúng".localized())
            txtReNewPassWord.becomeFirstResponder()
            return
        }
        ModelLogin.changePassword(oldPassword: txtOldPassWord.text!, newPassword: txtNewPassWord.text!, completion: {
            
            respone in
            if respone == 200 {
                
                Toast.showSuccess("Đổi mật khẩu thành công".localized())
                self.txtNewPassWord.text = ""
                self.txtOldPassWord.text = ""
                self.txtReNewPassWord.text = ""
            } else {
                
                Toast.showErr("Đổi mật khẩu không thành công".localized())
            }
        })
        
    }
}
extension String {
    
    func isEqualToString(find: String) -> Bool {
        
        return String(format: self) == find
        
    }
}
