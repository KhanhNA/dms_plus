//
//  ViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/3/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import Foundation
import MMDrawerController

class LoginViewController: UIBaseViewController {
    
    var USER_NAME: String = "USER_NAME"
    
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogin.layer.cornerRadius = 5
        btnLogin.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.4724361796)
        self.view.backgroundColor = UIColor.navibar
        
        if let us = defaults.string(forKey: USER_NAME) {
            txtUserName.text = us
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func setLanguage() {
        super.setLanguage()
        txtUserName.placeholder = "Nhập tên đăng nhập".localized()
        txtPassword.placeholder = "Nhập mật khẩu".localized()
        btnLogin.setTitle("Đăng nhập".localized(), for: .normal)
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        login()
    }
    
    func login() {
        if ModelLogin.validateLogin(username: txtUserName.text, password: txtPassword.text) {
            
            let entityLoginSend = EntityLoginSend()
            entityLoginSend?.username = txtUserName.text!.trimSpace()
            entityLoginSend?.password = txtPassword.text!.trimSpace()
            
            ModelLogin.postOauthToken(entityLoginSend: entityLoginSend!) { (entityLogin, entityUserinfo) in
                if entityLogin?.toJSONString() != nil {
                    let menuLeft = UIStoryboard(name: "MenuLeft", bundle: nil)
                    let viewMenuLeft = menuLeft.instantiateViewController(withIdentifier: "MenuLeftViewController")
                    
                    self.defaults.set(self.txtUserName.text, forKey: self.USER_NAME)
                    
                    if let roleCode = entityUserinfo?.roleCode {
                        switch roleCode {
                        case .SM:
                            let dashboard = UIStoryboard(name: "Dashboard", bundle: nil)
                            let viewDashboard = dashboard.instantiateViewController(withIdentifier: "UINaviBaseViewController")
                            
                            let drawerController = MMDrawerController(center: viewDashboard,
                                                                      leftDrawerViewController: viewMenuLeft)
                            drawerController?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all
                            drawerController?.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
                            
                            self.present(drawerController!,
                                         animated: true,
                                         completion: nil)
                            break
                        case .SUP:
                            if NSModule.CHECK_IN.isShow == false {
                                Toast.showErr("Tên đăng nhập hoặc mật khẩu không hợp lệ".localized())
                                return
                            }
                            let storyGS = UIStoryboard(name: "CheckIn", bundle: nil)
                            let viewcontroler = storyGS.instantiateViewController(withIdentifier: "tabbarCheckIn")
                            let drawerControler = MMDrawerController(center: viewcontroler, leftDrawerViewController: viewMenuLeft)
                            drawerControler?.openDrawerGestureModeMask = MMOpenDrawerGestureMode.all
                            drawerControler?.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
                            self.present(drawerControler!, animated: true, completion: nil)
                            
                            break
                            
                        default:
                            break
                        }
                    }
                    
                } else {
                    Toast.showErr("Tên đăng nhập hoặc mật khẩu không hợp lệ".localized())
                }
                
            }
            
        }
    }
}
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        login()
        return true
    }
}

