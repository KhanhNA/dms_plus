//
//  EntityMenu.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

enum EntityMenuKey: Int {
    case KETQUABANHANG = 1
    case GHETHAM = 2
    case DATHANG = 3
    case DOIHANG = 4
    case TRAHANG = 5
    case SANPHAM = 6
    case KHACHHANGMOI = 7
    case CHUONGTRINHKM = 8
    case DOIMK = 9
    case CAIDAT = 10
    case THOAT = 11
    case CHECKIN = 12
    case LICHSUCHECKIN = 13
}

class EntityMenuParents: NSObject {
    var textName: String?
    var entityMenus = [EntityMenu]()
}

class EntityMenu: NSObject {
    var textName = ""
    var strImage = ""
    var isSelect = false
    var entityMenuKey: EntityMenuKey?
}

