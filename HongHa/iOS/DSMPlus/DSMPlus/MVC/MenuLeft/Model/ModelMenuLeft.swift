//
//  ModelMenuLeft.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

enum NSModule: String {
    case INVENTORY = "INVENTORY"
    case PROMOTION = "PROMOTION"
    case SURVEY = "SURVEY"
    case CHECK_IN = "CHECK_IN"
    case EXCHANGE_RETURN = "EXCHANGE_RETURN"
    case DISTRIBUTOR_PRICE_LIST = "DISTRIBUTOR_PRICE_LIST"
    case NULL = ""
    
    var isShow: Bool {
        if let modules = entityUserinfo?.modules {
            for item in modules {
                if item == self.rawValue {
                    return true
                }
            }
        }
        return false
    }
}

class ModelMenuLeft: NSObject {
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(languageChangeNotification),
                                               name: NSNotification.Name(rawValue: LCLLanguageChangeNotification),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func languageChangeNotification() {
        self._arraySM = nil
        self._arraySUP = nil
    }
    
    var arrayMenu: [EntityMenuParents] {
        if let roleCode = entityUserinfo?.roleCode {
            switch roleCode {
            case .SM:
                return self.arraySM
            case .SUP:
                return self.arraySUP
            default:
                break
            }
        }
        return [EntityMenuParents]()
    }
    
    //mark: menu trái dành cho nhân viên bán hàng
    private var _arraySM: [EntityMenuParents]?
    private var arraySM: [EntityMenuParents] {
        if let _arraySM = self._arraySM {
            return _arraySM
        }
        
        self._arraySM = [EntityMenuParents]()
        
        //menu 1
        let entityMenuParents = EntityMenuParents()
        let entityMenu1 = EntityMenu()
        entityMenu1.textName = "Kết quả bán hàng".localized()
        entityMenu1.strImage = "dashboard"
        entityMenu1.isSelect = true
        entityMenu1.entityMenuKey = .KETQUABANHANG
        entityMenuParents.entityMenus.append(entityMenu1)
        
        //        if NSModule.CHECK_IN.isShow {
        let entityMenu12 = EntityMenu()
        entityMenu12.textName = "Ghé thăm".localized()
        entityMenu12.strImage = "group"
        entityMenu12.entityMenuKey = .GHETHAM
        entityMenuParents.entityMenus.append(entityMenu12)
        //        }
        
        let entityMenu13 = EntityMenu()
        entityMenu13.textName = "Đặt hàng".localized()
        entityMenu13.strImage = "event_note"
        entityMenu13.entityMenuKey = .DATHANG
        entityMenuParents.entityMenus.append(entityMenu13)
        if NSModule.EXCHANGE_RETURN.isShow {
            let entityMenu14 = EntityMenu()
            entityMenu14.textName = "Đổi hàng".localized()
            entityMenu14.strImage = "find_replace"
            entityMenu14.entityMenuKey = .DOIHANG
            entityMenuParents.entityMenus.append(entityMenu14)
            
            let entityMenu15 = EntityMenu()
            entityMenu15.textName = "Trả hàng".localized()
            entityMenu15.strImage = "local_shipping"
            entityMenu15.entityMenuKey = .TRAHANG
            entityMenuParents.entityMenus.append(entityMenu15)
        }
        self._arraySM?.append(entityMenuParents)
        
        //menu 2
        let entityMenuParents1 = EntityMenuParents()
        entityMenuParents1.textName = "Hỗ trợ bán hàng".localized()
        let entityMenu21 = EntityMenu()
        entityMenu21.textName = "Danh sách sản phẩm".localized()
        entityMenu21.strImage = "widgets"
        entityMenu21.entityMenuKey = .SANPHAM
        entityMenuParents1.entityMenus.append(entityMenu21)
        
        let entityMenu22 = EntityMenu()
        entityMenu22.textName = "Khách hàng đăng ký mới".localized()
        entityMenu22.strImage = "person_add"
        entityMenu22.entityMenuKey = .KHACHHANGMOI
        entityMenuParents1.entityMenus.append(entityMenu22)
        
        if NSModule.PROMOTION.isShow {
            let entityMenu23 = EntityMenu()
            entityMenu23.textName = "Chương trình khuyến mại".localized()
            entityMenu23.strImage = "local_offer"
            entityMenu23.entityMenuKey = .CHUONGTRINHKM
            entityMenuParents1.entityMenus.append(entityMenu23)
        }
        self._arraySM?.append(entityMenuParents1)
        
        //menu 3
        let entityMenuParents2 = EntityMenuParents()
        entityMenuParents2.textName = "Hệ thống".localized()
        let entityMenu31 = EntityMenu()
        entityMenu31.textName = "Đổi mật khẩu".localized()
        entityMenu31.strImage = "lock_outline"
        entityMenu31.entityMenuKey = .DOIMK
        entityMenuParents2.entityMenus.append(entityMenu31)
        
        let entityMenu32 = EntityMenu()
        entityMenu32.textName = "Cài đặt".localized()
        entityMenu32.strImage = "settings"
        entityMenu32.entityMenuKey = .CAIDAT
        entityMenuParents2.entityMenus.append(entityMenu32)
        
        let entityMenu33 = EntityMenu()
        entityMenu33.textName = "Thoát".localized()
        entityMenu33.strImage = "exit_to_app"
        entityMenu33.entityMenuKey = .THOAT
        entityMenuParents2.entityMenus.append(entityMenu33)
        
        self._arraySM?.append(entityMenuParents2)
        
        return self._arraySM!
    }
    
    //mark: menu trái dành cho Giám sát
    private var _arraySUP: [EntityMenuParents]?
    private var arraySUP: [EntityMenuParents] {
        
        if let _arraySUP = self._arraySUP {
            return _arraySUP
        }
        self._arraySUP = [EntityMenuParents]()
        let entityMenuParents1 = EntityMenuParents()
        if NSModule.CHECK_IN.isShow {
            //menu 1
            let entityMenu21 = EntityMenu()
            entityMenu21.textName = "Check in".localized()
            entityMenu21.strImage = "pin_drop"
            entityMenu21.isSelect = true
            entityMenu21.entityMenuKey = .CHECKIN
            entityMenuParents1.entityMenus.append(entityMenu21)
        }
        
        let entityMenu22 = EntityMenu()
        entityMenu22.textName = "Lịch sử check in".localized()
        entityMenu22.strImage = "history"
        entityMenu22.entityMenuKey = .LICHSUCHECKIN
        entityMenuParents1.entityMenus.append(entityMenu22)
        
        self._arraySUP?.append(entityMenuParents1)
        
        //menu 2
        let entityMenuParents2 = EntityMenuParents()
        entityMenuParents2.textName = "Hệ thống".localized()
        let entityMenu31 = EntityMenu()
        entityMenu31.textName = "Đổi mật khẩu".localized()
        entityMenu31.strImage = "lock_outline"
        entityMenu31.entityMenuKey = .DOIMK
        entityMenuParents2.entityMenus.append(entityMenu31)
        
        let entityMenu32 = EntityMenu()
        entityMenu32.textName = "Cài đặt".localized()
        entityMenu32.strImage = "settings"
        entityMenu32.entityMenuKey = .CAIDAT
        entityMenuParents2.entityMenus.append(entityMenu32)
        
        let entityMenu33 = EntityMenu()
        entityMenu33.textName = "Thoát".localized()
        entityMenu33.strImage = "exit_to_app"
        entityMenu33.entityMenuKey = .THOAT
        entityMenuParents2.entityMenus.append(entityMenu33)
        
        self._arraySUP?.append(entityMenuParents2)
        
        return self._arraySUP!
        
    }
    
    func setAllDisSelect() {
        for entityMenuParents in arrayMenu {
            for entityMenu in entityMenuParents.entityMenus {
                entityMenu.isSelect = false
            }
        }
    }
    
    func logout() {
        if let access_token = entityLogin?.access_token {
            let url = "\(BASEURL.DELETE.oauth_revoke.url())\(access_token)"
            Networking.DELETE(url, completionHandler: { (response) in
                
            })
        }
        
    }
    
}
