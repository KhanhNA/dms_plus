//
//  MenuLeftTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import Font_Awesome_Swift

class MenuLeftTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var imageMenu: UIImageView!
    @IBOutlet weak var lbNameMenu: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setMenu(entityMenu: EntityMenu) {
        imageMenu.image = UIImage(icon: entityMenu.strImage, size: CGSize(width: 150, height: 150))
        lbNameMenu.text = entityMenu.textName
        if entityMenu.isSelect {
            self.backgroundColor = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 0.3776133363)
        } else {
            self.backgroundColor = UIColor.clear
        }
    }
    
}
