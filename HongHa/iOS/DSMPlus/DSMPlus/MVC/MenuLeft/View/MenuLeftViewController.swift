//
//  MenuLeftViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import MMDrawerController

class MenuLeftViewController: UIBaseViewController {
    
    @IBOutlet weak var infoUserView: UIInfoUserView!
    @IBOutlet weak var tableView: UITableView!
    
    let modelMenuLeft = ModelMenuLeft()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "MenuLeftTableViewCell", bundle: nil),
                           forCellReuseIdentifier: "MenuLeftTableViewCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
        infoUserView.setUpView(stImage: "", txtName: entityUserinfo?.clientName, txtDiaChi: entityUserinfo?.fullname)
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
}

extension MenuLeftViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return modelMenuLeft.arrayMenu.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelMenuLeft.arrayMenu[section].entityMenus.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if modelMenuLeft.arrayMenu[section].textName != nil {
            return 40
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        // Create label with section title
        let lb = UIBaseLabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.backgroundColor = UIColor.clear
        lb.textColor = UIColor.white
        lb.font = FontFamily.NotoSans.Bold.font(size: 14)
        lb.text = modelMenuLeft.arrayMenu[section].textName
        // Create header view and add label as a subview
        let viewHeader = UIView()
        viewHeader.backgroundColor = UIColor.navibar
        viewHeader.addSubview(lb)
        
        let topRowHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[lb]-0-|",
            options: [],
            metrics: nil,
            views: ["lb":lb])
        viewHeader.addConstraints(topRowHorizontalConstraints)
        
        let topRowV = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[lb]-0-|",
            options: [],
            metrics: nil,
            views: ["lb":lb])
        viewHeader.addConstraints(topRowV)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MenuLeftTableViewCell") as? MenuLeftTableViewCell {
            let entityMenu = modelMenuLeft.arrayMenu[indexPath.section].entityMenus[indexPath.row]
            cell.setMenu(entityMenu: entityMenu)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        modelMenuLeft.setAllDisSelect()
        let entityMenu = modelMenuLeft.arrayMenu[indexPath.section].entityMenus[indexPath.row]
        entityMenu.isSelect = true
        
        weak var centerViewController: UIViewController?
        if let entityMenuKey = entityMenu.entityMenuKey {
            switch entityMenuKey {
            case .KETQUABANHANG:
                let dashboard = UIStoryboard(name: "Dashboard", bundle: nil)
                centerViewController = dashboard.instantiateViewController(withIdentifier: "UINaviBaseViewController")
                break
            case .GHETHAM:
                let customer = UIStoryboard(name: "Customer", bundle: nil)
                centerViewController = customer.instantiateViewController(withIdentifier: "tabarnaviCustomer")
                break
            case .DATHANG:
                let storyBoard = UIStoryboard(name: "OderList", bundle: nil)
                centerViewController = storyBoard.instantiateViewController(withIdentifier: "UINaviBaseViewController")
                break
            case .SANPHAM:
                let storyBoard = UIStoryboard(name: "Product", bundle: nil)
                centerViewController = storyBoard.instantiateViewController(withIdentifier: "naviProductList")
                break
            case .KHACHHANGMOI:
                let storyboard = UIStoryboard(name: "CustomerRegister", bundle: nil)
                centerViewController = storyboard.instantiateViewController(withIdentifier: "navicustomerregister")
                break
            case .DOIHANG:
                let storyBoard = UIStoryboard(name: "ChangeDelivery", bundle: nil)
                let changeDeliveryViewController = storyBoard.instantiateViewController(withIdentifier: "ChangeDeliveryViewController") as? ChangeDeliveryViewController
                changeDeliveryViewController?.url = BASEURL.GET.exchange_product
                let navi =  UINaviBaseViewController()
                navi.viewControllers = [changeDeliveryViewController!]
                centerViewController = navi
                break
                
            case .TRAHANG:
                let storyBoard = UIStoryboard(name: "ChangeDelivery", bundle: nil)
                let changeDeliveryViewController = storyBoard.instantiateViewController(withIdentifier: "ChangeDeliveryViewController") as? ChangeDeliveryViewController
                changeDeliveryViewController?.url = BASEURL.GET.return_product
                let navi =  UINaviBaseViewController()
                navi.viewControllers = [changeDeliveryViewController!]
                centerViewController = navi
                break
                
            case .CHUONGTRINHKM:
                let promotionViewController = PromotionViewController(nibName: "PromotionViewController", bundle: nil)
                let navi = UINaviBaseViewController()
                navi.viewControllers = [promotionViewController]
                centerViewController = navi
                break
            case .DOIMK:
                let storyboardLogin = UIStoryboard(name: "Login", bundle: nil)
                let viewcontrolerChangePass = storyboardLogin.instantiateViewController(withIdentifier: "changepassword")
                centerViewController = viewcontrolerChangePass
                break
            case .CHECKIN:
                let storyboard = UIStoryboard(name: "CheckIn", bundle: nil)
                let checkInControler = storyboard.instantiateViewController(withIdentifier: "tabbarCheckIn")
                centerViewController = checkInControler
                break
            case .LICHSUCHECKIN:
                let hisStoryboard = UIStoryboard(name: "HistorySup", bundle: nil)
                let hisViewControler = hisStoryboard.instantiateViewController(withIdentifier: "hisStoryboard")
                centerViewController = hisViewControler
                break
            case .CAIDAT:
                let story = UIStoryboard(name: "Setting", bundle: nil)
                centerViewController = story.instantiateViewController(withIdentifier: "UINaviBaseViewController")
                break
            case .THOAT:
                logout()
                return
            default:
                break
            }
        }
        
        if let centerViewController = centerViewController {
            self.mm_drawerController.centerViewController = centerViewController
            self.mm_drawerController.closeDrawer(animated: true, completion: nil)
        }
        tableView.reloadData()
    }
    
    fileprivate func logout() {
        let alert = UIAlertController(title: "Xác nhận thoát".localized(),
                                      message: "Chắc chắn muốn thoát?".localized(),
                                      preferredStyle: .alert)
        
        let alertExit = UIAlertAction(title: "Huỷ".localized(), style: .cancel)
        
        let alert1 = UIAlertAction(title: "Xác nhận".localized(), style: .destructive) { (alertAction) in
            self.modelMenuLeft.logout()
            entityLogin = nil
            entityUserinfo = nil
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(alertExit)
        alert.addAction(alert1)
        
        self.present(alert, animated: true)
    }
}
