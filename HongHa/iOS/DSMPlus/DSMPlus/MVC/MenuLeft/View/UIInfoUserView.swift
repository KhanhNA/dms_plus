//
//  UIInfoUserView.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/5/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UIInfoUserView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbName: UIBaseLabel!
    @IBOutlet weak var lbDiaChi: UIBaseLabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = imageView.bounds.size.height / 2
    }
    
    func setUpView(stImage: String?, txtName: String?, txtDiaChi: String?) {
        self.backgroundColor = UIColor.navibar
        self.lbName.textColor = UIColor.white
        self.lbDiaChi.textColor = self.lbName.textColor
        if let stImage = stImage {
            //hiển thị hình ảnh.
            imageView.setImage_Url(stImage)
        } else {
            imageView.image = UIImage(named: "ic_user")
        }
        if let txtName = txtName {
            lbName.text = txtName
        } else {
            lbName.text = ""
        }
        if let txtDiaChi = txtDiaChi {
            lbDiaChi.text = txtDiaChi
        } else {
            lbDiaChi.text = ""
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
