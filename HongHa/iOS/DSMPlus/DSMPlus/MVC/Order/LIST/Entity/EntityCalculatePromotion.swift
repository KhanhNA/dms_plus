//
//  EntityCalculatePromotion.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/14/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityCalculatePromotion: BaseEntity {
    var id: String?
    var name: String?
    var code: String?
    var details: [EntityListPromotion]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
        details <- map["details"]
    }
}

class EntityListPromotion: BaseEntity {
    var id: String?
    var conditionProductName: String?
    var code: String?
    var reward: EntityReward?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        conditionProductName <- map["conditionProductName"]
        code <- map["code"]
        reward <- map["reward"]
    }
}

class EntityReward: BaseEntity {
    var quantity: NSNumber?
    var product: EntityProduct?
    var amount: NSNumber?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        quantity <- map["quantity"]
        product <- map["product"]
        amount <- map["amount"]
    }
}
