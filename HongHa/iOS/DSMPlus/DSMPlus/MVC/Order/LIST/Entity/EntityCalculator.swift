//
//  EntityCalcuate.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/14/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

enum DeliveryType: Int {
    case IMMEDIATELY = 0
    case SAME_DAY = 1
    case ANOTHER_DAY = 2
}

class EntityCalculator: BaseEntitySend {
    var customerId: String?
    var deliveryTime: Date?
    var discountAmt: NSNumber?
    var deliveryType: DeliveryType = .IMMEDIATELY
    var orderForDP: Bool = false
    var vanSales: Bool = false
    var details: [EntityOderSelect]?
    
    var promotions: [EntityOderSelect]?
    
    var sumPromotion: NSNumber?
    var subTotal: NSNumber?
    
    override init() {
        
    }
    
    init(json: [String: Any]) {
        
        if let customerId = json["customerId"] as? String {
            self.customerId = customerId
        }
        
        if let deliveryType = json["deliveryType"] as? Int {
            if let de = DeliveryType(rawValue: deliveryType) {
                self.deliveryType = de
            }
        }
        
        if let orderForDP = json["orderForDP"] as? Bool {
            self.orderForDP = orderForDP
        }
        
        if let vanSales = json["vanSales"] as? Bool {
            self.vanSales = vanSales
        }
        
        if let subTotal = json["subTotal"] as? Int64 {
            self.subTotal = NSNumber(value: subTotal)
        }
        
        if let promotionAmt = json["promotionAmt"] as? Int64 {
            self.sumPromotion = NSNumber(value: promotionAmt)
        }
        
        if let deliveryTime = json["deliveryTime"] as? String {
            self.deliveryTime = deliveryTime.toDate()
        }
        
        if let discountAmt = json["discountAmt"] as? Int64 {
            self.discountAmt = NSNumber(value: discountAmt)
        }
        
        if let details = json["details"] as? [[String: Any]] {
            if self.details == nil {
                self.details = [EntityOderSelect]()
            }
            for item in details {
                let entityOderSelect = EntityOderSelect(json: item)
                self.details?.append(entityOderSelect)
            }
        }
        
        if let sumPromotion = json["sumPromotion"] as? Int64 {
            self.sumPromotion = NSNumber(value: sumPromotion)
        }
        
        //promotions
        if let promotions = json["promotions"] as? [[String: Any]] {
            for promotion in promotions {
                if let details = promotion["details"] as? [[String: Any]] {
                    for detail in details {
                        if let reward = detail["reward"] as? [String: Any] {
                            if (reward["product"] as? [String: Any]) != nil {
                                if self.promotions == nil {
                                    self.promotions = [EntityOderSelect]()
                                }
                                let entityOderSelect = EntityOderSelect(json: reward)
                                self.promotions?.append(entityOderSelect)
                            }
                        }
                    }
                }
            }
        }
    }
}

class EntityChageDelivery: BaseEntitySend {
    var customerId: String?
    var details: [EntityOderSelect]?
    var salesmanId: String? {
        return entityUserinfo?.id
    }
    var total: Int64?
    var distributorId: String?
    
}
