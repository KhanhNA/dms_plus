//
//  EntityListOrder.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

enum OrderStatus: Int {
    case WAITING = 0
    case ACCEPTED = 1
    case REJECTED = 2
}

class EntityListOrder: BaseEntity {
    
    var id: String?
    var customer: VisitCustomerEntity?
    var createdBy: EnityCreatedBy?
    var createdTime: Date?
    var code: String?
    var approveStatus: OrderStatus?
    var deliveryType: Int?
    var deliveryTime: String?
    var comment: String?
    var subTotal: Int64?
    var promotionAmt: Int?
    var discountPercentage: String?
    var discountAmt: Int64?
    var grandTotal: Int64?
    var quantity: Int64?
    var productivity: Int64?
    var vanSales: Bool?
    var withVisit: Bool?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        customer <- map["customer"]
        createdBy <- map["createdBy"]
        createdTime <- (map["createdTime"], DateTransformCustom())
        code <- map["code"]
        approveStatus <- map["approveStatus"]
        deliveryType <- map["deliveryType"]
        deliveryTime <- map["deliveryTime"]
        comment <- map["comment"]
        subTotal <- map["subTotal"]
        promotionAmt <- map["promotionAmt"]
        discountPercentage <- map["discountPercentage"]
        discountAmt <- map["discountAmt"]
        grandTotal <- map["grandTotal"]
        quantity <- map["quantity"]
        productivity <- map["productivity"]
        vanSales <- map["vanSales"]
        withVisit <- map["withVisit"]
    }
}

class EntityDistributor: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
    }
}

class EnityCreatedBy: BaseEntity {
    var id: String?
    var username: String?
    var usernameFull: String?
    var fullname: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        username <- map["username"]
        usernameFull <- map["usernameFull"]
        fullname <- map["fullname"]
    }
}
