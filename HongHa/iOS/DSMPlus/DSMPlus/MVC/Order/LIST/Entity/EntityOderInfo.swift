//
//  EntityOderInfo.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/19/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityOderInfo: BaseEntity {
    var id: String?
    var customer: VisitCustomerEntity?
    var createdTime: Date?
    var code: String?
    var subTotal: NSNumber?
    var promotionAmt: NSNumber?
    var discountAmt: NSNumber?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        customer <- map["customer"]
        createdTime <- (map["createdTime"], DateTransformCustom())
        code <- map["code"]
    }
}
