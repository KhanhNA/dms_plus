//
//  EntityOderSelect.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class EntityOderSelect: BaseEntitySend {
    var quantity: Int64?
    var productId: String?
    var entityProduct: EntityProduct?
    
    override init() {
        
    }
    
    init(json: [String: Any]) {
        if let quantity = json["quantity"] as? Int64 {
            self.quantity = quantity
        }
        
        if let product = json["product"] as? [String: Any] {
            entityProduct = EntityProduct(JSON: product)
        } else if let entityProduct = json["entityProduct"] as? [String: Any] {
            self.entityProduct =  EntityProduct(JSON: entityProduct)
        }
        
        if let productId = json["productId"] as? String {
            self.productId = productId
        }
    }
}
