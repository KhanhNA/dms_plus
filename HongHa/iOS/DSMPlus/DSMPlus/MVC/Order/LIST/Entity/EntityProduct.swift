//
//  EntityListProduct.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityProduct: BaseEntity {
    
    var id: String?
    var name: String?
    var code: String?
    var photo: String?
    var uom: Uom?
    var productCategory: ProductCategory?
    var price: NSNumber?
    var productivity: Double?
    var description: String?
    var seqNo: Int64?
    var subTotal: Int64?
    var availableQuantity: NSNumber?
    var count: Int64?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
        photo <- map["photo"]
        uom <- map["uom"]
        description <- map["description"]
        productCategory <- map["productCategory"]
        price <- map["price"]
        productivity <- map["productivity"]
        seqNo <- map["seqNo"]
        subTotal <- map["subTotal"]
        availableQuantity <- map["availableQuantity"]
        count <- map["count"]
    }
}

class Uom: BaseEntity {
    var id: String?
    var name: String?
    var code: String?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
    }
}

class ProductCategory: BaseEntity {
    var id: String?
    var name: String?
    var code: String?
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
    }
}
