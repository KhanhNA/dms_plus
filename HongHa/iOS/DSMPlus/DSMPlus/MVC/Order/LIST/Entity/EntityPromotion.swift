//
//  EntityPromotion.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EntityPromotion: BaseEntity {
    
    var id: String?
    var draft: Bool?
    var active: Bool?
    var name: String?
    var code: String?
    var distributor: String?
    var startDate: Date?
    var endDate: Date?
    var applyFor: String?
    var description: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        id <- map["id"]
        name <- map["name"]
        code <- map["code"]
        draft <- map["draft"]
        active <- map["active"]
        distributor <- map["distributor"]
        startDate <- (map["startDate"], DateTransformCustom())
        endDate <- (map["endDate"], DateTransformCustom())
        applyFor <- map["applyFor"]
        description <- map["description"]
    }
}
