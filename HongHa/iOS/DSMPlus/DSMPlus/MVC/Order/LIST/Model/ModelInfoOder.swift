//
//  ModelInfoOder.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/17/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelInfoOder: BaseModel {
    func POST(entityCalculator: EntityCalculator, completion: @escaping (Bool?) -> Swift.Void) {
        
        let url = BASEURL.POST.order_unplanned.url()
        
        Networking.POSTUrl(url: url, paramater: entityCalculator.toJSON()) { response in
            if let json = response?.data {
                if json["id"] != nil {
                    completion(true)
                    return
                }
            }
            completion(false)
        }
    }
    
    //lấy thông tin chi tiết đơn hàng
    class func GET(idOder: String, completion: @escaping (EntityCalculator?) -> Swift.Void) {
        let url = "\(BASEURL.GET.order_info.url())\(idOder)"
        
        Networking.GETUrl(url: url) { response in
            if let json = response?.data {
                let entityCalculator = EntityCalculator(json: json)
                completion(entityCalculator)
                return
            }
            completion(nil)
        }
    }
}
