//
//  ModelListCustomerOder.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/8/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelListCustomerOder: BaseModel {
    var visitCustomerEntitys = [VisitCustomerEntity]()
    
    func getList(completion: @escaping ([VisitCustomerEntity]?) -> Swift.Void) {
        let url = "\(BASEURL.GET.visit_customerAll.url())"
        Networking.GETUrl(url: url, paramater: nil) {
            response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    self.visitCustomerEntitys.removeAll()
                    for item in list {
                        let visitCustomerEntity = VisitCustomerEntity(JSON: item)
                        self.visitCustomerEntitys.append(visitCustomerEntity!)
                    }
                }
            }
            completion(self.visitCustomerEntitys)
        }
    }
}
