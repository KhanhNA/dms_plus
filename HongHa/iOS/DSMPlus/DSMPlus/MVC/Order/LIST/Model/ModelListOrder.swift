//
//  ModelListOrder.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelListOrder: BaseModel {
    
    var entityListOrders = [EntityListOrder]()
    
    func getList(completion: @escaping ([EntityListOrder]?) -> Swift.Void) {
        let url = "\(BASEURL.GET.order_list.url())"
        Networking.GETUrl(url: url, paramater: nil) {response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    self.entityListOrders.removeAll()
                    for item in list {
                        let entityListOrder = EntityListOrder(JSON: item)
                        self.entityListOrders.append(entityListOrder!)
                    }
                }
            }
            completion(self.entityListOrders)
        }
    }
}
