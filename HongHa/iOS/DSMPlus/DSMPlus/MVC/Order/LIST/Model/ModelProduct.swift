//
//  ModelProduct.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/13/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelProduct: BaseModel {
    var entityPromotions = [EntityPromotion]()
    
    func getList(completion: @escaping ([EntityPromotion]?) -> Swift.Void) {
        
        let url = BASEURL.GET.order_promotion.url()
        Networking.GETUrl(url: url) { response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    self.entityPromotions.removeAll()
                    for item in list {
                        let entityPromotion = EntityPromotion(JSON: item)
                        self.entityPromotions.append(entityPromotion!)
                    }
                }
            }
            completion(self.entityPromotions)
        }
    }
}
