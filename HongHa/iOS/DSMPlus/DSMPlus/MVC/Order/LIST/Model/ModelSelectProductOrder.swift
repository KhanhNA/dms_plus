//
//  ModelSelectProductOrder.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ModelSelectProductOrder: BaseModel {
    weak var visitCustomerEntity: VisitCustomerEntity?
    
    var entityProducts = [EntityProduct]()
    
    func getList(completion: @escaping ([EntityProduct]?) -> Swift.Void) {
        
        let url = BASEURL.GET.order_list_product.url()
        let paramater = ["customerId":visitCustomerEntity?.id]
        Networking.GETUrl(url: url, paramater: paramater) { response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    self.entityProducts.removeAll()
                    for item in list {
                        let entityProduct = EntityProduct(JSON: item)
                        self.entityProducts.append(entityProduct!)
                    }
                }
            }
            completion(self.entityProducts.sorted(by: { (entity1, entity2) -> Bool in
                return entity1.seqNo! < entity2.seqNo!
            }))
        }
    }
    
    func postList(promotion: EntityCalculator, completion: @escaping ([EntityCalculatePromotion]?) -> Swift.Void) {
        
        var entityCalculatePromotions = [EntityCalculatePromotion]()
        
        let url = BASEURL.POST.order_calculate.url()
        Networking.POSTUrl(url: url, paramater: promotion.toJSON()) { response in
            if let json = response?.data {
                if let list = json["list"] as? [[String: Any]] {
                    for item in list {
                        let entityCalculatePromotion = EntityCalculatePromotion(JSON: item)
                        entityCalculatePromotions.append(entityCalculatePromotion!)
                    }
                }
            }
            completion(entityCalculatePromotions)
        }
    }
    
    //mark: đổi hàng và trả hàng
    func postReturn(_ entityChageDelivery: EntityChageDelivery, completion: @escaping (Bool?) -> Swift.Void) {
        let url = BASEURL.POST.return_product.url()
        Networking.POSTUrl(url: url, paramater: entityChageDelivery.toJSON()) { response in
            if let json = response?.data {
                if (json["id"] as? String) != nil {
                    completion(true)
                    return
                }
            }
            completion(false)
        }
    }
    
    func postExchange(_ entityChageDelivery: EntityChageDelivery, completion: @escaping (Bool?) -> Swift.Void) {
        let url = BASEURL.POST.exchange_product.url()
        Networking.POSTUrl(url: url, paramater: entityChageDelivery.toJSON()) { response in
            if let json = response?.data {
                if (json["id"] as? String) != nil {
                    completion(true)
                    return
                }
            }
            completion(false)
        }
    }
}
