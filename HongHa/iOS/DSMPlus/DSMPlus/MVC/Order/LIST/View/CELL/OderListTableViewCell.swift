//
//  OderListTableViewCell.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class OderListTableViewCell: UIBaseTableViewCell {

    @IBOutlet weak var imgOder: UIImageView!
    @IBOutlet weak var lbSubTotal: UIBaseLabel!
    @IBOutlet weak var lbDate: UIBaseLabel!
    @IBOutlet weak var lbAdress: UIBaseLabel!
    @IBOutlet weak var lbStatus: UIBaseLabel!
    @IBOutlet weak var lbNumber: UIBaseLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lbSubTotal.textColor = UIColor.navibar
        lbDate.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        lbAdress.textColor = #colorLiteral(red: 0.4235294118, green: 0.4823529412, blue: 0.5411764706, alpha: 1)
        
        lbNumber.isHidden = true
        lbNumber.textColor = UIColor.navibar
        lbNumber.text = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setDataDB(_ entityListOrder: EntityListOrder) {
        if entityListOrder.withVisit == true {
            imgOder.image = UIImage(icon: "location_on", size: CGSize(width: 30, height: 30))
        } else {
            imgOder.image = UIImage(icon: "phone", size: CGSize(width: 30, height: 30))
        }
        
        if let grandTotal = entityListOrder.grandTotal {
            lbSubTotal.text = NSNumber(value: grandTotal).toDecimalString()
        } else {
            lbSubTotal.text = "0"
        }
        
        if let createdTime = entityListOrder.createdTime {
            lbDate.text = createdTime.toString()
        } else {
            lbDate.text = " "
        }
        
        lbStatus.text = " "
        
        lbAdress.text = ""
    }
    
    func setDataDB(_ entityListOrder: EntityListOrder, _ number: NSInteger) {
        if entityListOrder.withVisit == true {
            imgOder.image = UIImage(icon: "location_on", size: CGSize(width: 30, height: 30))
        } else {
            imgOder.image = UIImage(icon: "phone", size: CGSize(width: 30, height: 30))
        }
        
        if let grandTotal = entityListOrder.grandTotal {
            lbSubTotal.text = NSNumber(value: grandTotal).toDecimalString()
        } else {
            lbSubTotal.text = "0"
        }
        
        if let createdTime = entityListOrder.createdTime {
            lbDate.text = createdTime.toString()
        } else {
            lbDate.text = " "
        }
        
        if let quantity = entityListOrder.quantity {
            lbStatus.text = "\(quantity)"
        } else {
            lbStatus.text = " "
        }
        
        lbAdress.text = entityListOrder.customer?.name
        
        lbNumber.isHidden = false
        lbNumber.text = "\(number)."
    }
    
    func setData(entityListOrder: EntityListOrder) {
        if entityListOrder.withVisit == true {
            imgOder.image = UIImage(icon: "location_on", size: CGSize(width: 30, height: 30))
        } else {
            imgOder.image = UIImage(icon: "phone", size: CGSize(width: 30, height: 30))
        }
        
        if let grandTotal = entityListOrder.grandTotal {
             lbSubTotal.text = NSNumber(value: grandTotal).toDecimalString()
        } else {
            lbSubTotal.text = "0"
        }
       
        if let createdTime = entityListOrder.createdTime {
            lbDate.text = createdTime.toString()
        } else {
            lbDate.text = " "
        }
        
        if let approveStatus =  entityListOrder.approveStatus {
            switch approveStatus {
            case .ACCEPTED:
                lbStatus.text = "Đã duyệt".localized()
                lbStatus.textColor = UIColor.success
                break
            case .REJECTED:
                lbStatus.text = "Từ chối".localized()
                lbStatus.textColor = UIColor.error
                break
            case .WAITING:
                lbStatus.text = "Chờ duyệt".localized()
                lbStatus.textColor = UIColor.info
                break
            }
        } else {
            lbStatus.text = " "
        }
        
        lbAdress.text = entityListOrder.customer?.name
    }
    
    func setData(_ entityChangeDelivery: EntityChangeDelivery, url: BASEURL.GET) {
        
        imgOder.image = nil
        
        if url == .exchange_product {
            imgOder.image = UIImage(icon: "find_replace", size: CGSize(width: 30, height: 30))
        } else if url == .return_product {
            imgOder.image = UIImage(icon: "local_shipping", size: CGSize(width: 30, height: 30))
        }
        
        if let grandTotal = entityChangeDelivery.quantity {
            lbSubTotal.text = grandTotal.toDecimalString()
        } else {
            lbSubTotal.text = "0"
        }
        
        if let createdTime = entityChangeDelivery.createdTime {
            lbDate.text = createdTime.toString()
        } else {
            lbDate.text = " "
        }
        
        lbStatus.text = ""
        
        lbAdress.text = entityChangeDelivery.customer?.name
    }
    
}
