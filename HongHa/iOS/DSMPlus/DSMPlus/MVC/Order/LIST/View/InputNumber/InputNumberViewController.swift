//
//  InputNumberViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import PopupController

protocol InputNumberViewControllerDelegate: NSObjectProtocol {
    func doneInput(inputNumber: InputNumberViewController, number: NSNumber)
}

class InputNumberViewController: UIBaseViewController, PopupContentViewController {
    
    @IBOutlet weak var lbtitle: UIBaseLabel!
    @IBOutlet weak var viewU: UIView!
    @IBOutlet weak var lbNumber: UIBaseLabel!
    @IBOutlet weak var viewNumber: UIView!
    
    weak var delegate: InputNumberViewControllerDelegate?
    weak var popupController: PopupController?
    
    var maxvalue: Int64 = 999999
    var numberStr: NSNumber = 0 {
         didSet {
            lbNumber.text = numberStr.toDecimalString()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lbtitle.text = "Nhập số lượng".localized()
        self.lbtitle.textColor = #colorLiteral(red: 0.2352941176, green: 0.3098039216, blue: 0.368627451, alpha: 1)
        viewU.layer.cornerRadius = 1.5
        viewU.backgroundColor = UIColor.navibar
        
        viewNumber.layer.cornerRadius = 5
        viewNumber.layer.borderColor = self.lbtitle.textColor.cgColor
        viewNumber.layer.borderWidth = 0.5
        
        self.view.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // PopupContentViewController Protocol
    func sizeForPopup(_ popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        let height = UIScreen.main.bounds.size.height
        let width = UIScreen.main.bounds.size.width
        return CGSize(width: width - 40, height: max(300, height - 150))
    }

    @IBAction func actionNumber(_ sender: UIButton) {
        let i = Int(numberStr.int64Value) * 10 + sender.tag
        if i > maxvalue {
            return
        }
        numberStr = NSNumber(value: i)
    }
    
    @IBAction func actionDelete(_ sender: UIButton) {
        numberStr = NSNumber(value: numberStr.intValue/10)
    }
    
    @IBAction func actionDone(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.doneInput(inputNumber: self, number: self.numberStr)
        }
        popupController?.dismiss()
    }
    
}
