//
//  UINumberButton.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/12/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class UINumberButton: UIButton {

    override var isHighlighted: Bool {
        didSet {
            switch isHighlighted {
            case true:
                backgroundColor = UIColor.navibar
            case false:
                backgroundColor = UIColor.white
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .selected)
    }
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
 
}
