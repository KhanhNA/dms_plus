//
//  OderListViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/7/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class OderListViewController: UIBaseViewController {
    
    let modelListOrder = ModelListOrder()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UIBaseSearchBar!
    var array: [EntityListOrder]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Danh sách đơn hàng".localized()
        
        let cell = UINib(nibName: "OderListTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "OderListTableViewCell")
        tableView.tableFooterView = UIView()
        tableView.pullToRefresh { 
            self.modelListOrder.getList { (entityListOrders) in
                if entityListOrders != nil {
                    self.array = entityListOrders
                    self.tableView.reloadData()
                    self.tableView.stopAnimating()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func naviMenuRight() -> [UIBarButtonItem]? {
        let barButtonItem = UIBarButtonItem(title: "",
                                            style: UIBarButtonItem.Style.plain,
                                            target: self,
                                            action: #selector(OderListViewController.addShoppingCart))
        barButtonItem.icon = "add_circle_outline"
        return [barButtonItem]
    }
}

extension OderListViewController {
    @objc func addShoppingCart() {
        let selectCustomerViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectCustomerViewController") as? SelectCustomerViewController
        self.navigationController?.pushViewController(selectCustomerViewController!, animated: true)
    }
}
extension OderListViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = self.array {
            return array.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "OderListTableViewCell") as? OderListTableViewCell {
            cell.setData(entityListOrder: (self.array?[indexPath.row])!)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entityListOrder = self.array?[indexPath.row]
        let infoOderViewController = InfoOderViewController(nibName: "InfoOderViewController", bundle: nil)
        infoOderViewController.idOder = entityListOrder?.id
        self.navigationController?.pushViewController(infoOderViewController, animated: true)
    }
}

extension OderListViewController {
    override func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let search = searchBar.text {
            if search.isEmpty {
                self.array = modelListOrder.entityListOrders
                tableView.reloadData()
                return
            }
            self.array = modelListOrder.entityListOrders.filter({ (entity) -> Bool in
                return (entity.customer?.name!.uppercased().contains(search.uppercased()))!
            })
            tableView.reloadData()
        } else {
            self.array = modelListOrder.entityListOrders
            tableView.reloadData()
        }
    }
}
