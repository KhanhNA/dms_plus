//
//  EnitySendKey.swift
//  DSMPlus
//
//  Created by Tom Jony on 12/28/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import ObjectMapper

class EnitySendKey: Mappable {
    var q: String = ""
    var page: Int64 = 1
    var size: Int64 = page_size
    
    required init() { }
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        q <- map["q"]
        page <- map["page"]
        size <- map["size"]
    }
}
