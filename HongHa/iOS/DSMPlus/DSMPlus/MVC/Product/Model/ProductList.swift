//
//  ProductList.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/19/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ProductList: BaseModel {
    var array = [EntityProduct]()
    var totalCount: Int = 0
    var entityKey = EnitySendKey()
    
    func getListProduct(_ completion: @escaping ([EntityProduct]?) -> Swift.Void) {
        let url = "\(BASEURL.GET.product_list.url())"
        
        Networking.GETUrl(url: url, paramater: entityKey.toJSON()) { respone in
            if let json = respone?.data {
                if let count = json["count"] as? Int {
                    self.totalCount = count
                }
                if let list = json["list"] as? [[String: Any ]] {
                    
                    if self.entityKey.page == 1 {
                        self.array.removeAll()
                    }
                    self.entityKey.page = self.entityKey.page + 1
                    
                    for item in list {
                        let entity = EntityProduct(JSON: item)
                        
                        self.array.append(entity!)
                    }
                    completion(self.array)
                }
                return
            }
        }
    }
    
}
