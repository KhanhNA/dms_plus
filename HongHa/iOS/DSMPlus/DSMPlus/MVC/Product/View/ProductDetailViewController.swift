//
//  ProductDetailViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/20/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIBaseViewController {
    
    @IBOutlet weak var lblGhiChu: UIBaseLabel!
    @IBOutlet weak var titleGhiChu: UIBaseLabel!
    @IBOutlet weak var lblDonGia: UIBaseLabel!
    @IBOutlet weak var titleDonGia: UIBaseLabel!
    @IBOutlet weak var lblMaSP: UIBaseLabel!
    @IBOutlet weak var titleMaSP: UIBaseLabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lblNameProduct: UIBaseLabel!
    var navi: NaviProductDetailViewController? {
    
        return self.navigationController as? NaviProductDetailViewController
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let buttonBack = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(ProductDetailViewController.dismis))
        buttonBack.icon = "keyboard_backspace"
        self.navigationItem.leftBarButtonItem = buttonBack
        self.navigationItem.title = "CHI TIẾT SẢN PHẨM".localized()
        setData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func naviMenuLeft() {
        
    }
    
    @objc func dismis() {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func setData() {
        titleMaSP.text = "Mã sản phẩm".localized()
        titleDonGia.text = "Đơn giá".localized()
        titleGhiChu.text = "Ghi chú".localized()
        if let info = navi?.product {
        
            lblMaSP.text = info.code
            if let price = info.price,
                let type = info.uom?.name {
            lblDonGia.text = "\(price)/\(type)"
            }
            lblGhiChu.text = info.description
            image.setImage_Url(info.photo, placeholder: "")
            lblNameProduct.text = info.name
        }
    }
    
}
