//
//  ProductListViewController.swift
//  DSMPlus
//
//  Created by Luong Hoang on 12/19/16.
//  Copyright © 2016 Viettel ICT. All rights reserved.
//

import UIKit
import SVPullToRefresh

class ProductListViewController: UIBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var uisearchbar: UIBaseSearchBar!
    let model = ProductList()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "SelectProductTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "SelectProductTableViewCell")
        tableView.tableFooterView = UIView()
        self.navigationItem.title = "DANH SÁCH SẢN PHẨM".localized()
        
        tableView.pullToRefresh {
            self.model.entityKey.page = 1
            if let text = self.uisearchbar.text {
                self.model.entityKey.q = text
            }
            self.getList()
        }
        
        tableView.infiniteScroll {
            if self.model.array.count < self.model.totalCount {
                if let text = self.uisearchbar.text {
                    self.model.entityKey.q = text
                }
                self.getList()
            } else {
                self.tableView.stopAnimating()
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getList() {
        model.getListProduct() { respone in
            self.tableView.reloadData()
            self.tableView.stopAnimating()
        }
    }
}
extension ProductListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 96
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: SelectProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SelectProductTableViewCell", for: indexPath) as? SelectProductTableViewCell {
            
            cell.setData(entityProduct:(model.array[indexPath.row]))
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.array.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Product", bundle: nil)
        if let viewcontoler = storyboard.instantiateViewController(withIdentifier: "naviproduct") as? NaviProductDetailViewController {
            viewcontoler.product = self.model.array[indexPath.row]
            self.showDetailViewController(viewcontoler, sender: nil)
        }
    }
    
}
extension ProductListViewController {
    
    override func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if (searchBar.text?.isEmpty)! == false {
            self.model.entityKey.page = 1
            if let text = self.uisearchbar.text {
                self.model.entityKey.q = text
            }
            self.getList()
        }
        
    }
    override func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hidenKeyboard()
        uisearchbar.text = ""
        uisearchbar.showsCancelButton = false
        self.model.entityKey.page = 1
        if let text = self.uisearchbar.text {
            self.model.entityKey.q = text
        }
        self.getList()
    }
}
