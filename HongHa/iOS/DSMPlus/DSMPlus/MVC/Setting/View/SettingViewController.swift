//
//  SettingViewController.swift
//  DSMPlus
//
//  Created by Tom Jony on 1/3/17.
//  Copyright © 2017 Viettel ICT. All rights reserved.
//

import UIKit

class SettingViewController: UITableViewController {
    @IBOutlet weak var lbNgonNgu: UIBaseLabel!
    @IBOutlet weak var lbNgonNgu0: UIBaseLabel!
    let model = ModelSetting()
    let availableLanguages = Localize.availableLanguages()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.imageWithColor(UIColor.navibar), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.view.backgroundColor = UIColor.theme
        self.tableView.tableFooterView = UIView()
        naviMenuLeft()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setLanguage()
    }
    
    func naviMenuLeft() {
        if let navigationController =  self.navigationController {
            if navigationController.viewControllers.count > 1 {
                return
            }
            let barButtonItem = UIBarButtonItem(title: "",
                                                style: UIBarButtonItem.Style.plain,
                                                target: self,
                                                action: #selector(SettingViewController.openMenuLeft))
            barButtonItem.icon = "menu"
            
            self.navigationItem.leftBarButtonItem = barButtonItem
        }
    }
    
    @objc func openMenuLeft() {
        self.mm_drawerController.toggle(.left, animated: true) { b in
            
        }
    }
    
    fileprivate func setLanguage() {
        self.title = "Cài đặt".localized()
        self.lbNgonNgu.text = "Ngôn ngữ".localized()
        self.lbNgonNgu0.text = Localize.displayNameForLanguage(Localize.currentLanguage().localized())
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row + 1
        switch row {
        case 0:
            //màu
            
            break
        case 1:
            //ngôn ngữ
            let alert = UIAlertController(title: "Thay đổi ngôn ngữ".localized(),
                                          message: nil,
                                          preferredStyle: .actionSheet)
            
            let alertExit = UIAlertAction(title: "Huỷ".localized(), style: .cancel)
            
            for language in availableLanguages {
                let displayName = Localize.displayNameForLanguage(language)
                if displayName.isEmpty == false {
                    let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
                        (alert: UIAlertAction!) -> Void in
                        Localize.setCurrentLanguage(language)
                        self.setLanguage()
                    })
                    alert.addAction(languageAction)
                }
            }
            
            alert.addAction(alertExit)
            
            self.present(alert, animated: true)
            break
        default:
            //
            
            break
        }
    }
}
