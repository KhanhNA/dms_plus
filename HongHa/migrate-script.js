conn = new Mongo();
db = conn.getDB("dmsoishi");

//CLIENT
print('Start Fix Client');
db.getCollection('Client').find({}).forEach(function(client) {
	client.code = client.name.toUpperCase();

	db.getCollection('Client').save(client);
});
print('Finish Fix Client');

//DISTRICT
print('Start District -> Area');
db.getCollection('District').find({}).forEach(function(district) {
	var area = {
	    "_id" : district._id,
	    "_class" : "com.viettel.backend.domain.Area",
	    "name" : district.name,
	    "distributor" : {
	        "_id" : ObjectId("556fb5a0d4c664d670b994d1"),
	        "name" : "MKTG Office - Ho Chi Minh",
	        "code" : "DIS0001"
	    },
	    "search" : district.search,
	    "clientId" : ObjectId("55594f793004abc87cbbc7df"),
	    "draft" : false,
	    "active" : true
	};
	db.getCollection('Area').save(area);
});
db.getCollection('District').drop();
print('Finish District -> Area');

//CALENDAR CONFIG
print('Start Fix Calendar Config');
db.getCollection('CalendarConfig').find({}).forEach(function(calendarConfig) {
	delete calendarConfig.everyYearHolidays;
	delete calendarConfig.exceptionHolidays;
	delete calendarConfig.exceptionWorkingDays;

	db.getCollection('CalendarConfig').save(calendarConfig);
});
print('Finish Fix Calendar Config');

//CONFIG
print('Start ClientConfig + SalesConfig -> Config');
db.getCollection('ClientConfig').find({ clientId: ObjectId("000000000000000000000000") }).forEach(function(clientConfig) {
	var config = {
	    	"_class" : "com.viettel.backend.domain.Config",
	    	"dateFormat" : "dd/MM/yyyy",
	    	"productPhoto" : clientConfig.defaultProductPhoto,
		    "location" : {
		        "latitude" : 21.0420893468122152,
		        "longitude" : 105.8209669589996338
		    },
	    	"firstDayOfWeek" : 2,
	    	"minimalDaysInFirstWeek" : 1,
	    	"complexSchedule" : false,
	    	"numberWeekOfFrequency" : 0,
	    	"numberDayOrderPendingExpire" : 32,
	    	"orderDateType" : "CREATED_DATE",
	    	"visitDurationKPI" : NumberLong(0),
	    	"visitDistanceKPI" : 0.0000000000000000,
	    	"canEditCustomerLocation" : false,
	    	"clientId" : ObjectId("000000000000000000000000"),
	    	"draft" : false,
	    	"active" : true
	};

	db.getCollection('Config').save(config);
});
db.getCollection('ClientConfig').drop();
db.getCollection('SalesConfig').find({}).forEach(function(salesConfig) {
	var config = {
		    "_class" : "com.viettel.backend.domain.Config",
		    "location" : {
		        "latitude" : 20.8512321892209691,
		        "longitude" : 106.6882538795471191
		    },
		    "firstDayOfWeek" : 0,
		    "minimalDaysInFirstWeek" : 0,
		    "complexSchedule" : false,
		    "numberWeekOfFrequency" : 0,
		    "numberDayOrderPendingExpire" : 0,
		    "visitDurationKPI" : salesConfig.visitDurationKPI,
		    "visitDistanceKPI" : salesConfig.visitDistanceKPI,
		    "canEditCustomerLocation" : salesConfig.canEditCustomerLocation,
		    "clientId" : salesConfig.clientId,
		    "draft" : false,
		    "active" : true
		};
	db.getCollection('Config').save(config);
});
db.getCollection('SalesConfig').drop();
print('Finish ClientConfig + SalesConfig -> Config');

//COUNTER
print('Start counters -> Counter');
db.getCollection('counters').find({}).forEach(function(counters) {
	var Counter = {
	    "_id" : counters._id,
	    "_class" : "com.viettel.backend.domain.Counter",
	    "seq" : counters.seq
	};

	db.getCollection('Counter').save(Counter);
});

db.getCollection('counters').drop();
print('Finish counters -> Counter');

// ROUTE
print('Start Insert Route');
db.getCollection('User').find({ roles: { $all: [ 'SM' ] } }).forEach(function(salesman) {
	var route = {
		    "_id" : salesman._id,
		    "_class" : "com.viettel.backend.domain.Route",
		    "salesman" : {
		        "_id" : salesman._id,
		        "username" : salesman.username,
		        "fullname" : salesman.fullname
		    },
		    "name" : "RouteOf " + salesman.fullname,
		    "distributor" : {
		        "_id" : ObjectId("556fb5a0d4c664d670b994d1"),
		        "name" : "MKTG Office - Ho Chi Minh",
		        "code" : "DIS0001"
		    },
		    "search" : "\\QRouteOf\\E",
		    "clientId" : ObjectId("55594f793004abc87cbbc7df"),
		    "draft" : false,
		    "active" : true
		};
	db.getCollection('Route').save(route);
});
print('Finish Insert Route');

// CUSTOMER
print('Start Fix Customer');
db.getCollection('Customer').find({}).forEach(function(customer) {
	customer.joinDP = false;
	customer.createdBy = {
		"_id" : customer.createdBy._id,
		"username" : customer.createdBy.username,
		"fullname" : customer.createdBy.fullname
	};
	customer.customerType = {
		"_id" : customer.customerType._id,
		"name" : customer.customerType.name
	};

	if (customer.district != null) {
		customer.area = {
			"_id" : customer.district._id,
			"name" : customer.district.name
		};
		delete customer.district;
	}
	
	customer.distributor = {
		"_id" : customer.distributor._id,
		"name" : customer.distributor.name,
		"code" : customer.distributor.code
	};

	if (customer.schedule != null && customer.schedule.items != null && customer.schedule.items.length > 0 
		&& customer.schedule.items[0].days != null && customer.schedule.items[0].days.length > 0) {
		var schedule = {
			"routeId" : customer.schedule.salesmanId
		};

		var item = {
			"monday" : customer.schedule.items[0].days.indexOf(2) > -1,
            "tuesday" : customer.schedule.items[0].days.indexOf(3) > -1,
            "wednesday" : customer.schedule.items[0].days.indexOf(4) > -1,
            "thursday" : customer.schedule.items[0].days.indexOf(5) > -1,
            "friday" : customer.schedule.items[0].days.indexOf(6) > -1,
            "saturday" : customer.schedule.items[0].days.indexOf(7) > -1,
            "sunday" : customer.schedule.items[0].days.indexOf(1) > -1
		};

		schedule.item = item;
		schedule.items = [ item ];

		customer.schedule = schedule;
	}
	db.getCollection('Customer').save(customer);
});
print('Finish Fix Customer');

//PRODUCT
print('Start Fix Product');
db.getCollection('Product').find({}).forEach(function(product) {
	product.productivity = 1;

	delete product.output;

	product.uom = {
		"_id" : product.uom._id,
		"name" : product.uom.name,
		"code" : product.uom.code
	};

	product.productCategory = {
		"_id" : product.productCategory._id,
		"name" : product.productCategory.name
	};

	db.getCollection('Product').save(product);
});
print('Finish Fix Product');

//USER
print('Start Fix User');
db.getCollection('User').find().forEach(function(user) {
	if (user.roles != null) {
		if (user.roles[0] == 'SUPER') {
			db.getCollection('User').remove({ "_id" : user._id });
		} else {
			user.usernameFull = user.username;
			user.role = user.roles[0];
			delete user.roles;
			
			if (user.role == 'SC') {
				user.distributor = {
	        		"_id" : ObjectId("556fb5a0d4c664d670b994d1"),
	        		"name" : "MKTG Office - Ho Chi Minh",
	        		"code" : "DIS0001"
	    		};
			} else {
				if (user.distributor != null) {
					user.distributor = {
						"_id" : user.distributor._id,
						"name" : user.distributor.name,
						"code" : user.distributor.code
					};
				}
			}

			if (user.storeChecker != null) {
				user.storeChecker = {
					"_id" : user.storeChecker._id,
					"username" : user.storeChecker.username,
					"fullname" : user.storeChecker.fullname
				};
			}

			user.defaultAdmin = false;
			db.getCollection('User').save(user);
		}
	}
});
print('Finish Fix User');

// VISIT AND ORDER
print('Start Fix VisitAndOrder');
db.getCollection('VisitAndOrder').find({}).forEach(function(data) {
	if (data.distributor != null) {
		data.distributor = {
			"_id" : data.distributor._id,
			"name" : data.distributor.name,
			"code" : data.distributor.code
		};
	}

	if (data.customer != null) {
		var customerType = db.getCollection('CustomerType').find({ "_id": data.customer._id });
		data.customer = {
			"_id" : data.customer._id,
			"name" : data.customer.name,
        	"code" : data.customer.code,
        	"joinDP" : false,
        	"area" : {
            	"_id" : data.customer.district._id,
            	"name" : data.customer.district.name
        	},
        	"customerType" : {
        		"_id" : customerType._id,
        		"name" : customerType.name
    		}
		}
	}

	if (data.salesman != null) {
		data.createdBy = {
			"_id" : data.salesman._id,
			"username" : data.salesman.username,
			"fullname" : data.salesman.fullname
		}

		delete data.salesman
	}

	if (data.details != null) {
		var quantity = 0;
		for (var i = 0; i < data.details.length; i++) {
			var detail = data.details[i];

			quantity = parseInt(quantity, 10) + parseInt(detail.quantity, 10);

			var productCategory = db.getCollection('ProductCategory').find({ "_id": detail.product._id });
			var product = {
                "_id" : detail.product._id,
                "productCategory" : {
                    "_id" : productCategory._id,
                    "name" : productCategory.name
                },
                "uom" : {
                    "_id" : detail.product.uom._id,
                    "name" : detail.product.uom.name,
                    "code" : detail.product.uom.code
                },
                "price" : detail.product.price,
                "productivity" : "1",
                "name" : detail.product.name,
                "code" : detail.product.code
            }
            data.details[i].product = product;
		}
		data.quantity = parseInt(quantity, 10);
		data.productivity = parseInt(quantity, 10);
		data.orderForDP = false;
	}

	if (data.isVisit) {
		data.planned = true;
	}

	db.getCollection('VisitAndOrder').save(data);
});
print('Finish Fix VisitAndOrder');

// CREATE INDEX
print('Start Create Index');
db.getCollectionNames().forEach(function(collection) {
   indexes = db[collection].getIndexes();
   if (collection == 'Area'
       || collection == 'CalendarConfig'
       || collection == 'Config'
       || collection == 'Customer'
       || collection == 'CustomerType'
       || collection == 'DeliveryMan'
       || collection == 'Distributor'
       || collection == 'Product'
       || collection == 'ProductCategory'
       || collection == 'Promotion'
       || collection == 'Route'
       || collection == 'Survey'
       || collection == 'Target'
       || collection == 'UOM'
       || collection == 'User'
       || collection == 'VisitAndOrder') {
       		print("Add clientId index to " + collection);
       		db[collection].createIndex( { "clientId" : 1 } );
   }

   if (collection == 'Area'
       || collection == 'Customer'
       || collection == 'DeliveryMan'
       || collection == 'Route'
       || collection == 'VisitAndOrder') {
       		print("Add distributor._id index to " + collection);
       		db[collection].createIndex( { "distributor._id" : 1 } );
   }

   if (collection == 'VisitAndOrder') {
       		print("Add startTime.value index to " + collection);
       		db[collection].createIndex( { "startTime.value" : -1 } );
   }

   if (collection == 'VisitAndOrder') {
          print("Add approveTime.value index to " + collection);
          db[collection].createIndex( { "approveTime.value" : -1 } );
   }

});
print('Finish Create Index');
