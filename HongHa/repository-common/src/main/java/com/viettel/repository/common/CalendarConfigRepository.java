package com.viettel.repository.common;

import com.viettel.repository.common.domain.category.I_CalendarConfig;

public interface CalendarConfigRepository {

    public I_CalendarConfig getCalendarConfig(String clientId);

    public I_CalendarConfig save(String clientId, I_CalendarConfig domain);

}
