package com.viettel.repository.common;

import com.viettel.repository.common.domain.common.I_Category;

public interface CategoryBasicRepository<DOMAIN extends I_Category>
        extends CategoryComplexBasicRepository<DOMAIN, DOMAIN, DOMAIN> {

}
