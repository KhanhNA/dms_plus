package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface CategoryComplexBasicRepository<LIST extends I_Category, DETAIL extends I_Category, WRITE extends I_Category>
        extends POBasicRepository<DETAIL, WRITE> {

    // DRAFT = FALSE & ACTIVE = TRUE
    public List<LIST> getAll(String clientId, String distributorId);

    // ALL STATUS
    public List<LIST> getList(String clientId, Boolean draft, Boolean active, Collection<String> distributorIds,
            String searchName, String searchCode, PageSizeRequest pageSizeRequest);

    public long count(String clientId, Boolean draft, Boolean active, Collection<String> distributorIds,
            String searchName, String searchCode);

    public boolean checkNameExist(String clientId, String id, String name, String distributorId);

    public boolean checkCodeExist(String clientId, String id, String code, String distributorId);

    public boolean checkDistributorUsed(String clientId, String distributorId);

}
