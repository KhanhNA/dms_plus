package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.checkin.I_CheckIn;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

public interface CheckInRepository extends POBasicRepository<I_CheckIn, I_CheckIn> {

    public List<I_CheckIn> getCheckIns(String clientId, Collection<String> createdByIds, Period period,
            PageSizeRequest pageSizeRequest);

    public long countCheckIns(String clientId, Collection<String> createdByIds, Period period);

}
