package com.viettel.repository.common;

import com.viettel.repository.common.annotation.ClientRootInclude;
import com.viettel.repository.common.domain.common.I_Category;

@ClientRootInclude
public interface ClientRepository extends CategoryBasicRepository<I_Category> {

}
