package com.viettel.repository.common;

import java.util.List;

import com.viettel.repository.common.entity.SimpleDate;

public interface CodeGeneratorRepository {

    public String getDistributorCode(String clientId);
    
    public String getRegionCode(String clientId);

    public String getOrderCode(String clientId, SimpleDate createdDate);

    public String getCustomerCode(String clientId);
    
    public List<String> getCustomerCodeBatch(String clientId, int size);
}
