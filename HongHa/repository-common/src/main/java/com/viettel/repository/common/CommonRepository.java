package com.viettel.repository.common;

public interface CommonRepository {

    public String getRootId();
    
    public void resetCache();
    
}
