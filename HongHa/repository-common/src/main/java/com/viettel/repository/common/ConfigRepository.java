package com.viettel.repository.common;

import com.viettel.repository.common.domain.category.I_ClientConfig;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_SystemConfig;

public interface ConfigRepository {

    public I_Config getConfig(String clientId);

    public I_ClientConfig save(String clientId, I_ClientConfig clientConfig);
    
    public I_SystemConfig save(I_SystemConfig systemConfig);

}
