package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;

public interface CustomerPendingRepository extends POGetterRepository<I_Customer> {

	public boolean checkCustomerTypeUsed(String clientId, String customerTypeId);

	public boolean checkAreaUsed(String clientId, String areaId);

	public boolean checkDistributorUsed(String clientId, String distributorId);

	public List<I_Customer> getCustomersByCreatedUsers(String clientId, String distributorId,
			Collection<String> userIds, Integer status, String searchName, String searchCode,
			PageSizeRequest pageSizeRequest);

	public long countCustomersByCreatedUsers(String clientId, String distributorId, Collection<String> userIds,
			Integer status, String searchName, String searchCode);

	public List<I_Customer> getPendingCustomersByDistributors(String clientId, Collection<String> distributorIds,
			PageSizeRequest pageSizeRequest);

	public long countPendingCustomersByDistributors(String clientId, Collection<String> distributorIds);

	public boolean approve(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime, String id);

	public boolean approveMulti(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime,
			Collection<String> ids) throws Exception;

	public boolean reject(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime, String id);

	public boolean rejectMulti(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime,
			Collection<String> ids);

}
