package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Schedule;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;

public interface CustomerRepository extends CategoryBasicRepository<I_Customer> {

    // CHECK
    public boolean checkCustomerTypeUsed(String clientId, String customerTypeId);

    public boolean checkAreaUsed(String clientId, String areaId);

    /**
     * @param clientId mandatory
     * @param distributorId mandatory
     * @param searchByRoute true if you want to search by routeId
     * @param routeId use when searchByRoute is true (null if you want to search customer don't have route)
     * @param dayOfWeek day of week
     * @param searchName search if customer name contains this value
     * @param searchCode search if customer code contains this value
     * @return list of customer with schedule
     */
    public List<I_Customer> getList(String clientId, String distributorId, boolean searchByRoute,
            String routeId, Integer dayOfWeek, String searchName, String searchCode, PageSizeRequest pageSizeRequest);

    /**
     * @param clientId mandatory
     * @param distributorId mandatory
     * @param searchByRoute true if you want to search by routeId
     * @param routeId use when searchByRoute is true (null if you want to search customer don't have route)
     * @param dayOfWeek day of week
     * @param searchName search if customer name contains this value
     * @param searchCode search if customer code contains this value
     * @return count of customer match this criteria
     */
    public long count(String clientId, String distributorId, boolean searchByRoute, String routeId,
            Integer dayOfWeek, String searchName, String searchCode);

    /**
     * @param clientId mandatory
     * @param distributorId mandatory
     * @param routeIds mandatory
     * @param date null if you want to get all customer of any date
     * @return list of customer with schedule
     */
    public List<I_Customer> getCustomersByRoutes(String clientId, String distributorId,
            Collection<String> routeIds, SimpleDate date);
    
    /**
     * Check if this customer is belong to these list of route
     * @param clientId mandatory
     * @param customerId mandatory
     * @param routeIds mandatory
     * @return true if this customer is belong to these list of route
     */
    public boolean checkCustomerInRoute(String clientId, String customerId, Collection<String> routeIds);
    
    public boolean checkRouteUsed(String clientId, String distributorId, String routeId);
    
    public boolean updateSchedule(String clientId, String id, I_Schedule schedule);
    
    public boolean updatePhone(String clientId, String id, String phone);
    
    public boolean updateMobile(String clientId, String id, String mobile);
    
    public boolean updateLocation(String clientId, String id, Location location);

}
