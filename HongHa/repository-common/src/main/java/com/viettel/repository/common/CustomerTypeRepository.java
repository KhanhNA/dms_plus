package com.viettel.repository.common;

import com.viettel.repository.common.domain.common.I_Category;

public interface CustomerTypeRepository extends CategoryBasicRepository<I_Category> {

}
