package com.viettel.repository.common;

import java.math.BigDecimal;
import java.util.Map;

public interface DistributorPriceListRepository {

    /**
     * Get special price for product of distributor
     * @param clientId mandatory
     * @param distributorId mandatory
     * @return a map of price by productId
     */
    public Map<String, BigDecimal> getPriceList(String clientId, String distributorId);

    public void savePriceList(String clientId, String distributorId, Map<String, BigDecimal> priceList);

}
