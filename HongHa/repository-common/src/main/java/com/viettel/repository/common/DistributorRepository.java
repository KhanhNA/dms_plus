package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.category.I_Distributor;

public interface DistributorRepository extends CategoryBasicRepository<I_Distributor> {

    public List<I_Distributor> getDistributorsBySupervisors(String clientId, Collection<String> supervisorIds);
    
    public List<I_Distributor> getDistributorNotAssignedToRegion(String clientId);
    
    // CHECK
    public boolean checkSupervisorUsed(String clientId, String supervisorId);

}
