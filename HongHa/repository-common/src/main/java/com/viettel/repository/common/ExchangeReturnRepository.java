package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.data.I_ExchangeReturn;
import com.viettel.repository.common.domain.data.I_ExchangeReturnHeader;
import com.viettel.repository.common.domain.product.I_ProductExchangeReturnQuantity;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

public interface ExchangeReturnRepository extends POBasicRepository<I_ExchangeReturn, I_ExchangeReturn> {

    public List<I_ExchangeReturnHeader> getListByCreatedBys(String clientId, boolean exchange, String distributorId,
            Collection<String> createdByIds, Period period, PageSizeRequest pageSizeRequest);

    public long countByCreatedBys(String clientId, boolean exchange, String distributorId,
            Collection<String> createdByIds, Period period);

    public List<I_ProductExchangeReturnQuantity> getProductExchangeReturnQuantities(String clientId,
            Collection<String> distributorIds, Period period);

}
