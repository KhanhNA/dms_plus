package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.data.I_Feedback;
import com.viettel.repository.common.domain.data.I_FeedbackHeader;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

public interface FeedbackRepository extends POGetterRepository<I_Feedback> {

    public List<I_FeedbackHeader> getFeedbackByDistributors(String clientId, Collection<String> distributorIds,
            Boolean readed, PageSizeRequest pageSizeRequest);

    public long countFeedbackByDistributors(String clientId, Collection<String> distributorIds, Boolean readed);

    public List<I_Feedback> getFeedbackByDistributors(String clientId, Collection<String> distributorIds, Period period,
            PageSizeRequest pageSizeRequest);

    public long countFeedbackByDistributors(String clientId, Collection<String> distributorIds, Period period);
    
    public List<I_Feedback> getLastFeedbackByCustomer(String clientId, String distributorId, String customerId,
            int size);

    public void markAsRead(String clientId, String id);

}
