package com.viettel.repository.common;

import java.io.InputStream;
import java.util.Collection;

import com.viettel.repository.common.annotation.ClientRootInclude;
import com.viettel.repository.common.domain.file.I_File;

@ClientRootInclude
public interface FileRepository {

    public I_File getFileById(String clientId, String fileId);

    public String store(String clientId, InputStream inputStream, String fileName, String contentType);

    public void delete(String clientId, String fileId);

    public void delete(String clientId, Collection<String> fileIds);

    public void markAsUsed(String clientId, String fileId);

    public void markAsUsed(String clientId, Collection<String> fileIds);

    public boolean exists(String clientId, String fileId);

    public boolean existsAll(String clientId, Collection<String> fileIds);

}
