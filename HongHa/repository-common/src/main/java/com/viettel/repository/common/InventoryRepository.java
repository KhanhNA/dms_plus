package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.inventory.I_InventoryHeader;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface InventoryRepository extends POBasicRepository<I_Inventory, I_Inventory> {

    public List<I_InventoryHeader> getList(String clientId, String distributorId, PageSizeRequest pageSizeRequest);

    public long count(String clientId, String distributorId);

    public I_Inventory getLatestInventory(String clientId, String distributorId);

    public Map<String, I_Inventory> getLatestInventorys(String clientId, Collection<String> distributorIds);

}
