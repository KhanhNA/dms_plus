package com.viettel.repository.common;

public interface MasterDataRepository {
    
    public void deleteAllDatas(String clientId);
    
    public void deleteDatasWithoutCategory(String clientId);
    
}
