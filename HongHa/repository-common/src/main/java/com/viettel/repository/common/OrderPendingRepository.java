package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;

public interface OrderPendingRepository extends POGetterRepository<I_Order> {

    /**
     * @return only pending orders
     */
    public List<I_OrderHeader> getPendingOrdersByDistributors(String clientId, Collection<String> distributorIds,
            PageSizeRequest pageSizeRequest);

    /**
     * @return count only pending orders
     */
    public long countPendingOrdersByDistributors(String clientId, Collection<String> distributorIds);

    /**
     * @return all order status (pending, approved, rejected)
     */
    public List<I_OrderHeader> getOrdersCreatedToday(String clientId, String distributorId, String createdUserId,
            Collection<String> customerIds);
    
    public boolean approve(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime, String id);
    
    public boolean reject(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime, String id);
    
    public I_Order create(String clientId, I_Order order);
    
}
