package com.viettel.repository.common;

import java.util.List;

import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

public interface OrderRepository {

    public List<I_OrderHeader> getOrderHeadersByDistributor(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType, PageSizeRequest pageSizeRequest,
            boolean sortAsc);

    public List<I_Order> getOrdersByDistributor(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType, PageSizeRequest pageSizeRequest,
            boolean sortAsc);

    public long countOrdersByDistributor(String clientId, String distributorId, String createdUserId, String customerId,
            Period period, OrderDateType orderDateType);

    public I_Order getOrderByCode(String clientId, String code);

}
