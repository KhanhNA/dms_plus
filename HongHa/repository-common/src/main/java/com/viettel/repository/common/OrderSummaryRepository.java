package com.viettel.repository.common;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.data.I_OrderSummary;
import com.viettel.repository.common.entity.SimpleDate.Period;

public interface OrderSummaryRepository {

    /**
     * @param clientId
     *            mandatory
     * @param distributorIds
     *            mandatory
     * @param period
     *            mandatory
     * @return a map of order summary by isoDate
     */
    public Map<String, I_OrderSummary> getOrderSummaryDaily(String clientId, Collection<String> distributorIds,
            Period period, OrderDateType orderDateType);

    /**
     * get quantity sold by product id
     * 
     * @param clientId
     *            mandatory
     * @param distributorId
     *            mandatory
     * @param createdUserId
     *            optional
     * @param customerId
     *            optional
     * @param period
     *            mandatory
     * @param orderDateType
     *            mandatory
     */
    public Map<String, I_OrderSummary> getOrderSummaryDaily(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType);

    /**
     * @param clientId
     *            mandatory
     * @param distributorId
     *            mandatory
     * @param period
     *            mandatory
     * @return a map of order summary by salesmanId
     */
    public Map<String, I_OrderSummary> getOrderSummaryBySalesman(String clientId, String distributorId, Period period,
            OrderDateType orderDateType);

    /**
     * @param clientId
     *            mandatory
     * @param distributorIds
     *            mandatory
     * @param period
     *            mandatory
     * @return a map of order summary by productId
     */
    public Map<String, I_OrderSummary> getOrderSummaryByProduct(String clientId, Collection<String> distributorIds,
            String productCategoryId, Period period, OrderDateType orderDateType);

    /**
     * @param clientId
     *            mandatory
     * @param distributorIds
     *            mandatory
     * @param period
     *            mandatory
     * @return a map of order summary by distributorId
     */
    public Map<String, I_OrderSummary> getOrderSummaryByDistributor(String clientId, Collection<String> distributorIds,
            Period period, OrderDateType orderDateType);

    /**
     * get quantity sold by product id
     * 
     * @param clientId
     *            mandatory
     * @param distributorId
     *            mandatory
     * @param createdUserId
     *            optional
     * @param customerId
     *            optional
     * @param period
     *            mandatory
     * @param orderDateType
     *            mandatory
     */
    public Map<String, BigDecimal> getProductSoldMap(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType);

    public I_OrderSummary getOrderSummary(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType);

}
