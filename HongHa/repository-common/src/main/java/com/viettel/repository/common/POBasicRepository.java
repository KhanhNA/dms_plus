package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.common.I_PO;

public interface POBasicRepository<DETAIL extends I_PO, WRITE extends I_PO> extends POGetterRepository<DETAIL> {

   public DETAIL save(String clientId, WRITE domain);

    public DETAIL saveAndEnable(String clientId, WRITE domain);

    public boolean enable(String clientId, String id);

    public boolean deactivate(String clientId, String id);

    public boolean activate(String clientId, String id);

    public boolean delete(String clientId, String id);

    public void insertBatch(String clientId, Collection<WRITE> domains);

    // ALL STATUS
    public DETAIL getById(String clientId, Boolean draft, Boolean active, String id);
    
    public List<DETAIL> getListByIds(String clientId, Boolean draft, Boolean active, Collection<String> ids);
    
    public boolean exists(String clientId, Boolean draft, Boolean active, String id);

    public boolean exists(String clientId, Boolean draft, Boolean active, Collection<String> ids);

}
