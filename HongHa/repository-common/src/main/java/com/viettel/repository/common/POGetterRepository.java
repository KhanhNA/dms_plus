package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.common.I_PO;

public interface POGetterRepository<D extends I_PO> {

    // DRAFT = FALSE & ACTIVE = TRUE
    public D getById(String clientId, String id);
    
    public List<D> getListByIds(String clientId, Collection<String> ids);
    
}
