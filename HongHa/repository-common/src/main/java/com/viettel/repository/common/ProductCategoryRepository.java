package com.viettel.repository.common;

import com.viettel.repository.common.domain.common.I_Category;

public interface ProductCategoryRepository extends CategoryBasicRepository<I_Category> {

}
