package com.viettel.repository.common;

import java.util.Set;

public interface ProductImportantRepository {
	
	public void saveProductImportant(String clientId, Set<String> productIds);
	
	public Set<String> getProductImportant(String clientId);

}
