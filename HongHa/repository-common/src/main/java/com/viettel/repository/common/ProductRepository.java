package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.viettel.repository.common.domain.product.I_Product;

public interface ProductRepository extends CategoryBasicRepository<I_Product> {

    /**
     * @return product map of all product not draft include active true and
     *         false
     */
    public Map<String, I_Product> getProductMap(String clientId, Boolean active);

    public List<I_Product> getProductsByCategories(String clientId, Collection<String> productCategoryIds);

    // CHECK
    public boolean checkUOMUsed(String clientId, String uomId);

    public boolean checkProductCategoryUsed(String clientId, String productCategoryId);

}
