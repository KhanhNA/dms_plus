package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.promotion.I_Promotion;
import com.viettel.repository.common.domain.promotion.I_PromotionHeader;

public interface PromotionRepository
        extends CategoryComplexBasicRepository<I_PromotionHeader, I_Promotion, I_Promotion> {

    public List<I_PromotionHeader> getPromotionHeadersAvailableToday(String clientId, Collection<String> distributorIds);
    
    public List<I_Promotion> getPromotionsAvailableToday(String clientId, Collection<String> distributorIds);

}
