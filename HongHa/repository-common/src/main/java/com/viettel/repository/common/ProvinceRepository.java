package com.viettel.repository.common;

import com.viettel.repository.common.annotation.ClientRootInclude;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.entity.Location;

@ClientRootInclude
public interface ProvinceRepository extends CategoryBasicRepository<I_Category> {

    public I_Category getProvinceByLocation(Location location);

}
