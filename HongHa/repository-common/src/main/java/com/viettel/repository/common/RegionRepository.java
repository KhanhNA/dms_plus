package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;

public interface RegionRepository extends CategoryBasicRepository<I_Region> {
		
		I_Region getRootNode(String clientId);
		
		// MANAGER
	    public List<I_CategoryEmbed> getDistributorsOfRegion(String clientId, String id);

	    public boolean setDistributorsOfManager(String clientId, String id, Collection<String> distributorIds);
	    
	    
}
