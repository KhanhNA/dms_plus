package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.user.I_UserEmbed;

public interface RouteRepository extends CategoryBasicRepository<I_Route> {
    
    public List<I_Route> getAll(String clientId, Collection<String> distributorIds);

    public boolean setSalesman(String clientId, String id, I_UserEmbed salesman);

    public List<I_Route> getRoutesBySalesmen(String clientId, String distributorId, Collection<String> salesmanIds);

    // CHECK
    public boolean checkSalesmanUsed(String clientId, String distributorId, String salesmanId);

}
