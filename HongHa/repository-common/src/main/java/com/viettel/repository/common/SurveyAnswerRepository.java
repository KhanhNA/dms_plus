package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.viettel.repository.common.domain.data.I_SurveyAnswer;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface SurveyAnswerRepository {

    public List<I_SurveyAnswer> getSurveyAnswerByDistributors(String clientId, String surveyId,
            Collection<String> distributorIds, PageSizeRequest pageSizeRequest);

    public long countSurveyAnswerByDistributors(String clientId, String surveyId, Collection<String> distributorIds);

    public Map<Integer, Integer> getOptionCountMap(String clientId, String surveyId, Collection<String> distributorIds);

}
