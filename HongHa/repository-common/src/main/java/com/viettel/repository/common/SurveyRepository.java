package com.viettel.repository.common;

import java.util.List;

import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyHeader;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface SurveyRepository extends CategoryComplexBasicRepository<I_SurveyHeader, I_Survey, I_Survey> {

    public List<I_Survey> getSurveysAvailable(String clientId);

    public List<I_SurveyHeader> getSurveysStarted(String clientId, String search, PageSizeRequest pageSizeRequest);

    public long countSurveysStarted(String clientId, String search);

}
