package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.category.I_Target;

public interface TargetRepository extends POBasicRepository<I_Target, I_Target> {

    public List<I_Target> getTargetsBySalesmen(String clientId, Collection<String> salesmanIds, int month, int year,
            boolean sortBySalesmanName);

    public boolean existsTargetBySalesman(String clientId, String salesmanId, int month, int year);

    public I_Target getTargetBySalesman(String clientId, String salesmanId, int month, int year);
    
}
