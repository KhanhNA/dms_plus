package com.viettel.repository.common;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.entity.PageSizeRequest;

public interface UserRepository extends POBasicRepository<I_User, I_User> {

	public List<I_User> getAll(String clientId);

	public I_User getDefaultAdmin(String clientId);

	/**
	 * @param clientId
	 *            mandatory
	 * @param role
	 *            mandatory
	 */
	public List<I_User> getUserByRole(String clientId, String role);

	public List<I_User> getUserByRole(String clientId, String role, Collection<String> distributorIds);

	// MANAGER
	public List<I_Distributor> getDistributorsOfManager(String clientId, String id);

	public boolean setDistributorsOfManager(String clientId, String id, Collection<String> accessibleDistributorIds);

	// ADVANCE SUPPERVISOR
	public List<String> getDistributorForRegionRecursively(String clientId, I_Region region,
			List<String> distributorIds);

	public List<I_Region> getRegionsOfManager(String clientId, String id);

	public boolean setRegionsOfManager(String clientId, String id, Collection<String> regionIds);

	// DISTRIBUTOR ADMIN
	/**
	 * @param clientId
	 *            mandatory
	 * @param distributorIds
	 *            mandatory
	 */
	public List<I_User> getDistributorAdmins(String clientId, Collection<String> distributorIds);

	// SALEMAN
	/**
	 * @param clientId
	 *            mandatory
	 * @param distributorIds
	 *            mandatory
	 */
	public List<I_User> getSalesmen(String clientId, Collection<String> distributorIds);

	public boolean updateVanSalesStatus(String clientId, Collection<String> ids, boolean status);

	// ALL
	public List<I_User> getList(String clientId, Boolean draft, Boolean active, String role, String distributorId,
			String searchFullname, String searchUsername, PageSizeRequest pageSizeRequest, boolean sortByFullname);

	public long count(String clientId, Boolean draft, Boolean active, String role, String distributorId,
			String searchFullname, String searchUsername);

	public boolean checkUsernameExist(String clientId, String id, String username);

	public boolean checkFullnameExist(String clientId, String id, String fullname);

	// AUTHENTICATE
	public I_User findByUsernameFull(String usernameFull);

	public boolean changePassword(String clientId, String id, String password);

}
