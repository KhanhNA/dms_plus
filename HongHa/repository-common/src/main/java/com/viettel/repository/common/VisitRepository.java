package com.viettel.repository.common;

import java.util.List;

import com.viettel.repository.common.domain.data.I_VisitHeader;
import com.viettel.repository.common.domain.data.I_VisitPhoto;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

public interface VisitRepository {

    /**
     * @param clientId mandatory
     * @param distributorId mandatory
     * @param salesmanId optional
     * @param customerId optional
     * @param period optional but will have an default period (3 last month)
     * @param pageable optional
     * @param sortAsc
     * @return
     */
    public List<I_VisitHeader> getVisitByDistributor(String clientId, String distributorId, String salesmanId,
            String customerId, Period period, PageSizeRequest pageable, boolean sortAsc);
    
    /**
     * @param clientId mandatory
     * @param distributorId mandatory
     * @param salesmanId optional
     * @param customerId optional
     * @param period optional but will have an default period (3 last month)
     */
    public long countVisitedsByDistributor(String clientId, String distributorId, String salesmanId,
            String customerId, Period period);
    
    /**
     * @param clientId mandatory
     * @param distributorId mandatory
     * @param salesmanId mandatory
     * @param period mandatory
     */
    public List<I_VisitPhoto> getVisitPhotoBySalesman(String clientId, String distributorId, String salesmanId,
            Period period);
    
    
}
