package com.viettel.repository.common;

import java.util.Collection;
import java.util.Map;

import com.viettel.repository.common.domain.data.I_VisitSummary;
import com.viettel.repository.common.entity.SimpleDate.Period;

public interface VisitSummaryRepository {

    /**
     * @param clientId
     *            mandatory
     * @param distributorIds
     *            mandatory
     * @param period
     *            mandatory
     * @return a map of visit summary by isoDate
     */
    public Map<String, I_VisitSummary> getVisitSummaryDaily(String clientId, Collection<String> distributorIds,
            Period period);

    /**
     * @param clientId
     *            mandatory
     * @param distributorId
     *            mandatory
     * @param period
     *            mandatory
     * @return a map of visit summary by salesmanId
     */
    public Map<String, I_VisitSummary> getVisitSummaryBySalesman(String clientId, String distributorId, Period period);

    /**
     * @param clientId
     *            mandatory
     * @param distributorIds
     *            mandatory
     * @param period
     *            mandatory
     * @return a map of visit summary by distributorId
     */
    public Map<String, I_VisitSummary> getVisitSummaryByDistributor(String clientId, Collection<String> distributorIds,
            Period period);
    
    public I_VisitSummary getVisitSummary(String clientId, String distributorId, String createdUserId,
            String customerId, Period period);

}
