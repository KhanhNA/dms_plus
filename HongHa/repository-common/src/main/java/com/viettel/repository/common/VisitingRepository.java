package com.viettel.repository.common;

import java.util.Collection;
import java.util.Map;

import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.domain.data.I_VisitHeader;

public interface VisitingRepository extends POGetterRepository<I_Visit> {

    /**
     * Get today visit of customer If customer have more than 1 one visit today
     * the function return the first visited
     */
    public I_VisitHeader getVisitHeaderByCustomerToday(String clientId, String distributorId, String customerId);
    
    /**
     * Get today visit of customer If customer have more than 1 one visit today
     * the function return the first visited
     */
    public I_Visit getVisitByCustomerToday(String clientId, String distributorId, String customerId);

    /**
     * Get today visit map by customerId If customer have more than 1 one visit
     * today the function return the first visited
     */
    public Map<String, I_VisitHeader> getVisitHeaderByCustomerTodayMap(String clientId, String distributorId,
            Collection<String> customerIds);

    /**
     * Check if any customer is this list is visiting
     */
    public boolean checkVisiting(String clientId, String distributorId, Collection<String> customerIds);

    public I_Visit save(String clientId, I_Visit visit);
    
}
