package com.viettel.repository.common.domain.category;

import java.util.List;

import com.viettel.repository.common.entity.SimpleDate;

public interface I_CalendarConfig {

    public List<Integer> getWorkingDays();

    public List<SimpleDate> getHolidays();

}
