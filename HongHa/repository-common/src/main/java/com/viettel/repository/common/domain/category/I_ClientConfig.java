package com.viettel.repository.common.domain.category;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.viettel.repository.common.entity.Location;

public interface I_ClientConfig extends Serializable {

	public long getVisitDurationKPI();

	public double getVisitDistanceKPI();

	public boolean isCanEditCustomerLocation();

	public Location getLocation();

	public Set<String> getModules();

	public int getNumberDayInventoryExpire();

	public String getProductivityUnit();

	public int getLimitAccount();

	public static class Module {

		public static final String CHECK_IN = "CHECK_IN";
		public static final String PROMOTION = "PROMOTION";
		public static final String SURVEY = "SURVEY";
		public static final String EXCHANGE_RETURN = "EXCHANGE_RETURN";
		public static final String INVENTORY = "INVENTORY";

		public static final Set<String> ALL = Collections
				.unmodifiableSet(new HashSet<>(Arrays.asList(CHECK_IN, PROMOTION, SURVEY, EXCHANGE_RETURN, INVENTORY)));

	}

}
