package com.viettel.repository.common.domain.category;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_DataOfRegion;
import com.viettel.repository.common.domain.user.I_UserEmbed;

public interface I_Distributor extends I_Category ,I_DataOfRegion{

    public I_UserEmbed getSupervisor();
    
}
