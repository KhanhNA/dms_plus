package com.viettel.repository.common.domain.category;

import java.io.Serializable;

import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

public interface I_SystemConfig extends Serializable {
    
    public static enum OrderDateType {
        CREATED_DATE, APPROVED_DATE
    }
    
    public String getDateFormat();

    public String getProductPhoto();
    
    public Location getLocation();

    public int getFirstDayOfWeek();

    public int getMinimalDaysInFirstWeek();

    public int getNumberWeekOfFrequency();

    public OrderDateType getOrderDateType();
    
    public int getNumberDayOrderPendingExpire();
    
    public default int getWeekIndex(SimpleDate date) {
        if (this.getNumberWeekOfFrequency() > 1) {
            int firstDayOfWeek = this.getFirstDayOfWeek();
            int minimalDaysInFirstWeek = this.getMinimalDaysInFirstWeek();
            // Tuan lam viec cua ngay hom nay tinh tu dau nam
            int weekOfToday = DateTimeUtils.getWeekOfYear(date, firstDayOfWeek, minimalDaysInFirstWeek);
            int weeekOfFrequency = this.getNumberWeekOfFrequency();
            // Thu tu cua tuan theo week duration
            int weekIndex = (weekOfToday % weeekOfFrequency) + 1;
            return weekIndex;
        } else {
            return 0;
        }
    }
    
}
