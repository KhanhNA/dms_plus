package com.viettel.repository.common.domain.category;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.common.I_DataOfDistributor;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.domain.user.I_UserEmbed;

public interface I_Target extends I_PO, I_DataOfDistributor {
    
    public I_UserEmbed getSalesman();

    public int getMonth();

    public int getYear();

    public BigDecimal getRevenue();
    
    public BigDecimal getSubRevenue();

    public BigDecimal getQuantity();

    public BigDecimal getProductivity();

    public int getNbOrder();

    public BigDecimal getRevenueByOrder();

    public BigDecimal getSkuByOrder();

    public BigDecimal getProductivityByOrder();

    public int getNewCustomer();

}
