package com.viettel.repository.common.domain.checkin;

import java.util.List;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_CheckIn extends I_PO {

    public I_UserEmbed getCreatedBy();

    public SimpleDate getCreatedTime();

    public String getNote();
    
    public Location getLocation();

    public I_CategoryEmbed getProvince();
    
    public List<String> getPhotos();
    
    public default int getNbPhotos() {
        return this.getPhotos() == null ? 0 : this.getPhotos().size();
    }
    
}
