package com.viettel.repository.common.domain.common;

import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_Approvable {
    
    public static final int APPROVE_STATUS_PENDING = 0;
    public static final int APPROVE_STATUS_APPROVED = 1;
    public static final int APPROVE_STATUS_REJECTED = 2;
    
    public int getApproveStatus();
    
    public I_UserEmbed getCreatedBy();

    public SimpleDate getCreatedTime();

    public I_UserEmbed getApprovedBy();

    public SimpleDate getApprovedTime();

}
