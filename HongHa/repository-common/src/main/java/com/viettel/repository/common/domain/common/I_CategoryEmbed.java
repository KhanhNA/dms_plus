package com.viettel.repository.common.domain.common;

public interface I_CategoryEmbed extends I_POEmbed {

    public String getName();

    public String getCode();

}
