package com.viettel.repository.common.domain.common;

import com.viettel.repository.common.domain.customer.I_CustomerEmbed;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_DataOfCustomer extends I_DataOfDistributor {
    
    public I_CustomerEmbed getCustomer();
    
    public I_UserEmbed getCreatedBy();
    
    public SimpleDate getCreatedTime();

}
