package com.viettel.repository.common.domain.common;

public interface I_PO extends I_POEmbed {

    public String getClientId();
    
    public String getId();
    
    public boolean isDraft();
    
    public boolean isActive();
    
}
