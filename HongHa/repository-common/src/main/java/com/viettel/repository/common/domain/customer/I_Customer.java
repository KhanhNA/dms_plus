package com.viettel.repository.common.domain.customer;

import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.common.I_Approvable;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_Customer extends I_Category, I_CustomerEmbed, I_Approvable {

	public String getContact();

	public String getMobile();

	public String getPhone();

	public String getEmail();

	public I_CategoryEmbed getCustomerType();

	public I_CategoryEmbed getArea();

	public Location getLocation();

	public I_Schedule getSchedule();

	public SimpleDate getLastModifiedTime();

	public default boolean checkScheduleDate(I_Config config, int dayOfWeek, int weekIndex) {
		if (this.getSchedule() == null) {
			return false;
		}

		I_ScheduleItem item = this.getSchedule().getItem();
		if (item == null) {
			return false;
		}

		if (config.getNumberWeekOfFrequency() > 1) {
			if (item.getDays().contains(dayOfWeek) && item.getWeeks().contains(weekIndex)) {
				return true;
			}
		} else {
			if (item.getDays().contains(dayOfWeek)) {
				return true;
			}
		}

		return false;
	}

}
