package com.viettel.repository.common.domain.customer;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;

public interface I_CustomerEmbed extends I_CategoryEmbed {

    public I_CategoryEmbed getArea();
    
    public I_CategoryEmbed getCustomerType();
    
}
