package com.viettel.repository.common.domain.customer;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.user.I_UserEmbed;

public interface I_Route extends I_Category {

    public I_UserEmbed getSalesman();
    
}
