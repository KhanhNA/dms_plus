package com.viettel.repository.common.domain.customer;

import java.io.Serializable;

public interface I_Schedule extends Serializable {

    public String getRouteId();
    
    public I_ScheduleItem getItem();

}
