package com.viettel.repository.common.domain.customer;

import java.io.Serializable;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public interface I_ScheduleItem extends Serializable {

    public boolean isMonday();

    public boolean isTuesday();

    public boolean isWednesday();

    public boolean isThursday();

    public boolean isFriday();

    public boolean isSaturday();

    public boolean isSunday();
    
    public List<Integer> getWeeks();
    
    public default List<Integer> getDays() {
        List<Integer> days = new LinkedList<Integer>();
        if (isMonday()) {
            days.add(Calendar.MONDAY);
        }
        
        if (isTuesday()) {
            days.add(Calendar.TUESDAY);
        }
        
        if (isWednesday()) {
            days.add(Calendar.WEDNESDAY);
        }
        
        if (isThursday()) {
            days.add(Calendar.THURSDAY);
        }
        
        if (isFriday()) {
            days.add(Calendar.FRIDAY);
        }
        
        if (isSaturday()) {
            days.add(Calendar.SATURDAY);
        }
        
        if (isSunday()) {
            days.add(Calendar.SUNDAY);
        };
        return days;
    }
    
}
