package com.viettel.repository.common.domain.data;

import java.util.List;

import com.viettel.repository.common.domain.product.I_ProductQuantity;

public interface I_ExchangeReturn extends I_ExchangeReturnHeader {

    public List<I_ProductQuantity> getDetails();
    
}
