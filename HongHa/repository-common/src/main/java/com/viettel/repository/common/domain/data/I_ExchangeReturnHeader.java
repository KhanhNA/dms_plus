package com.viettel.repository.common.domain.data;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.common.I_DataOfCustomer;
import com.viettel.repository.common.domain.common.I_PO;

public interface I_ExchangeReturnHeader extends I_PO, I_DataOfCustomer {
    
    public boolean isExchange();
    
    public BigDecimal getQuantity();

}
