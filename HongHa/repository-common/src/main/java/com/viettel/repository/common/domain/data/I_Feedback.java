package com.viettel.repository.common.domain.data;

import java.util.List;

public interface I_Feedback extends I_FeedbackHeader {

    public List<String> getFeedbacks();

}
