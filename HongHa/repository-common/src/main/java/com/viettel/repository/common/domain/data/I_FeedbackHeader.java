package com.viettel.repository.common.domain.data;

import com.viettel.repository.common.domain.common.I_DataOfCustomer;
import com.viettel.repository.common.domain.common.I_PO;

public interface I_FeedbackHeader extends I_PO, I_DataOfCustomer {

    public boolean isReaded();
    
    public String getFirstMessage();
    
}
