package com.viettel.repository.common.domain.data;

import java.util.List;

public interface I_Order extends I_OrderWithDetail {

    public List<I_OrderPromotion> getPromotions();
    
}
