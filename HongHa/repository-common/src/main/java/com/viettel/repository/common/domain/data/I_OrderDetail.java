package com.viettel.repository.common.domain.data;

import java.io.Serializable;
import java.math.BigDecimal;

public interface I_OrderDetail extends Serializable {

    public I_OrderProduct getProduct();

    public BigDecimal getQuantity();
    
    public default BigDecimal getAmount() {
        if (getProduct() == null || getQuantity() == null || getQuantity().signum() <= 0
                || getProduct().getPrice() == null || getProduct().getPrice().signum() <= 0)
            return BigDecimal.ZERO;

        return getProduct().getPrice().multiply(this.getQuantity());
    }

    public default BigDecimal getProductivity() {
        if (getProduct() == null || getQuantity() == null || getQuantity().signum() <= 0
                || getProduct().getProductivity() == null || getProduct().getProductivity().signum() <= 0)
            return BigDecimal.ZERO;

        return getProduct().getProductivity().multiply(getQuantity());
    }


}
