package com.viettel.repository.common.domain.data;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.common.I_Approvable;
import com.viettel.repository.common.domain.common.I_DataOfCustomer;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_OrderHeader extends I_PO, I_DataOfCustomer, I_Approvable {

    public static final int DELIVERY_TYPE_IMMEDIATE = 0;
    public static final int DELIVERY_TYPE_IN_DAY = 1;
    public static final int DELIVERY_TYPE_ANOTHER_DAY = 2;

    public boolean isWithVisit();

    public String getCode();

    public int getDeliveryType();

    public SimpleDate getDeliveryTime();

    public String getComment();
    

    /** Gia tri don hang truoc khuyen mai */
    public BigDecimal getSubTotal();

    /** Gia tri khuyen mai */
    public BigDecimal getPromotionAmt();

    /** Manual Discount Percentage (null if amount is fixed) */
    public BigDecimal getDiscountPercentage();

    /**
     * Manual Discount Amount discountAmt = fixed or = (subTotal - promotionAmt)
     * discount
     */
    public BigDecimal getDiscountAmt();

    /**
     * Gia tri cuoi cung cua don hang = subTotal - promotionAmt - discountAmt
     */
    public BigDecimal getGrandTotal();

    public BigDecimal getQuantity();

    public BigDecimal getProductivity();
    
    public int getNbSKU();

    public boolean isVanSales();

}
