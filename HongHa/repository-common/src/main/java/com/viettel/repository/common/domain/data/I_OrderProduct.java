package com.viettel.repository.common.domain.data;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.product.I_ProductEmbed;

public interface I_OrderProduct extends I_ProductEmbed {

    public BigDecimal getPrice();

    public BigDecimal getProductivity();

}
