package com.viettel.repository.common.domain.data;

import java.util.List;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;

public interface I_OrderPromotion extends I_CategoryEmbed {

    public List<I_OrderPromotionDetail> getDetails();
    
}
