package com.viettel.repository.common.domain.data;

import java.io.Serializable;

public interface I_OrderPromotionDetail extends Serializable {

    public int getSeqNo();
    
    public String getConditionProductName();
    
    public I_OrderPromotionReward getReward();
    
}
