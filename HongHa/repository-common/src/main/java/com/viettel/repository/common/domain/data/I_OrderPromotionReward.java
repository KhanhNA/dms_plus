package com.viettel.repository.common.domain.data;

import java.io.Serializable;
import java.math.BigDecimal;

import com.viettel.repository.common.domain.product.I_ProductEmbed;

public interface I_OrderPromotionReward extends Serializable {

    // PRODUCT
    public I_ProductEmbed getProduct();
    public String getProductText();
    public BigDecimal getQuantity();

    // MONEY
    public BigDecimal getAmount();

}
