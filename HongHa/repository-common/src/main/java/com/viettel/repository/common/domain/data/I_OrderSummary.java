package com.viettel.repository.common.domain.data;

import java.io.Serializable;
import java.math.BigDecimal;

public interface I_OrderSummary extends Serializable {

    public BigDecimal getRevenue();
    
    public BigDecimal getSubRevenue();

    public BigDecimal getProductivity();
    
    public int getNbSKU();

    public int getNbOrder();
    
    public int getNbOrderWithoutVisit();

    public int getNbSalesman();

    public int getNbCustomer();

}
