package com.viettel.repository.common.domain.data;

import java.util.List;

public interface I_OrderWithDetail extends I_OrderHeader {
    
    public List<I_OrderDetail> getDetails();

}
