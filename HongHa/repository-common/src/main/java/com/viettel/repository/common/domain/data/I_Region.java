package com.viettel.repository.common.domain.data;

import java.util.List;
import java.util.Set;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;

public interface I_Region extends I_Category {
	
	public I_CategoryEmbed getParent();
	public List<I_CategoryEmbed> getChildren();
	public Set<I_CategoryEmbed> getDistributors();
	public boolean isRoot();
	public boolean isLeaf();

	
}
