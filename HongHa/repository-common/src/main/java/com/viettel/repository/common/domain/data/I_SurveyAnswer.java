package com.viettel.repository.common.domain.data;

import com.viettel.repository.common.domain.common.I_DataOfCustomer;

public interface I_SurveyAnswer extends I_DataOfCustomer, I_SurveyAnswerContent {
    
}
