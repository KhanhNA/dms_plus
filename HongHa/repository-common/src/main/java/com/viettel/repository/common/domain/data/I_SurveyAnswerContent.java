package com.viettel.repository.common.domain.data;

import java.io.Serializable;
import java.util.Set;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;

public interface I_SurveyAnswerContent extends Serializable {

    public I_CategoryEmbed getSurvey();
    
    public Set<Integer> getOptions();
    
}
