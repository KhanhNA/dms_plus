package com.viettel.repository.common.domain.data;

import java.util.List;

public interface I_Visit extends I_VisitHeader, I_Order, I_Feedback {

    public List<I_SurveyAnswerContent> getSurveyAnswers();
    
}
