package com.viettel.repository.common.domain.data;

import java.util.List;

import com.viettel.repository.common.domain.common.I_DataOfCustomer;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_VisitHeader extends I_PO, I_DataOfCustomer, I_OrderHeader, I_VisitPhoto {

    public static final int VISIT_STATUS_VISITING = 0;
    public static final int VISIT_STATUS_VISITED = 1;

    public static final int LOCATION_STATUS_LOCATED = 0;
    public static final int LOCATION_STATUS_TOO_FAR = 1;
    public static final int LOCATION_STATUS_UNLOCATED = 2;
    public static final int LOCATION_STATUS_CUSTOMER_UNLOCATED = 3;

    public default SimpleDate getStartTime() {
        return getCreatedTime();
    }

    public SimpleDate getEndTime();

    public default I_UserEmbed getSalesman() {
        return getCreatedBy();
    }

    public boolean isHasOrder();
    
    public int getVisitStatus();

    public boolean isClosed();

    public String getPhoto();

    public Location getLocation();

    public Location getCustomerLocation();

    public int getLocationStatus();

    public Double getDistance();

    public long getDuration();

    public boolean isErrorDuration();
    
    public List<String> getFeedbacks();

}
