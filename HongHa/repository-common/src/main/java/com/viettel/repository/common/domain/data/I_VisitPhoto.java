package com.viettel.repository.common.domain.data;

import com.viettel.repository.common.domain.common.I_DataOfCustomer;
import com.viettel.repository.common.domain.common.I_PO;

public interface I_VisitPhoto extends I_PO, I_DataOfCustomer {

    public boolean isClosed();
    
    public String getPhoto();
    
}
