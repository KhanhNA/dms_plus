package com.viettel.repository.common.domain.data;

import java.io.Serializable;

public interface I_VisitSummary extends Serializable {

    public int getNbVisit();
    
    public int getNbVisitWithOrder();

    public int getNbVisitErrorDuration();

    public int getNbVisitErrorPosition();
    
    public int getNbSalesman();
    
    public int getNbCustomer();
    
}
