package com.viettel.repository.common.domain.file;

import java.io.InputStream;

public interface I_File extends I_FileMetadata {
    
    public InputStream getInputStream();

}
