package com.viettel.repository.common.domain.file;

import com.viettel.repository.common.domain.common.I_PO;

public interface I_FileMetadata extends I_PO {

    public String getFileName();

    public String getContentType();

}
