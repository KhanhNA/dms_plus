package com.viettel.repository.common.domain.inventory;

import java.util.List;

import com.viettel.repository.common.domain.product.I_ProductQuantity;

public interface I_Inventory extends I_InventoryHeader {

    public List<I_ProductQuantity> getDetails();
    
}
