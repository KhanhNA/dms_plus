package com.viettel.repository.common.domain.inventory;

import com.viettel.repository.common.domain.common.I_DataOfDistributor;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_InventoryHeader extends I_PO, I_DataOfDistributor {
    
    public SimpleDate getTime();

}
