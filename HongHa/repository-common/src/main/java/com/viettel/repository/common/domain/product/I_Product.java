package com.viettel.repository.common.domain.product;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.common.I_Category;

public interface I_Product extends I_Category, I_ProductEmbed {

    public BigDecimal getPrice();

    public BigDecimal getProductivity();
    
    public String getPhoto();

    public String getDescription();

}
