package com.viettel.repository.common.domain.product;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;

public interface I_ProductEmbed extends I_CategoryEmbed {

    public I_CategoryEmbed getProductCategory();
    
    public I_CategoryEmbed getUom();
    
}
