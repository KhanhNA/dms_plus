package com.viettel.repository.common.domain.product;

import java.io.Serializable;
import java.math.BigDecimal;

public interface I_ProductExchangeReturnQuantity extends Serializable {

	public String getThisId();

	public String getProductId();

	public BigDecimal getExchangeQuantity();

	public BigDecimal getReturnQuantity();

	public String getCustomer();

	public String getTime();

	public String getCreatedBy();

	public String getDistributor();

}
