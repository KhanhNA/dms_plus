package com.viettel.repository.common.domain.product;

import java.math.BigDecimal;

public interface I_ProductQuantity extends I_ProductEmbed {

    public BigDecimal getQuantity();
    
}
