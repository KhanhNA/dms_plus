package com.viettel.repository.common.domain.promotion;

import java.util.List;
import java.util.Set;

public interface I_Promotion extends I_PromotionHeader {

    public List<I_PromotionDetail> getDetails();
    
    public boolean isForAllDistributor();
    
    public Set<String> getDistributorIds();
    
}
