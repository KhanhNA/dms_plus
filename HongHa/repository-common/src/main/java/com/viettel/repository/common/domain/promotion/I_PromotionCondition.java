package com.viettel.repository.common.domain.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

public interface I_PromotionCondition extends Serializable {

    public String getProductId();
    
    public BigDecimal getQuantity();
    
}
