package com.viettel.repository.common.domain.promotion;

import java.io.Serializable;

public interface I_PromotionDetail extends Serializable {
    
    public int getSeqNo();

    public int getType();
    
    public I_PromotionCondition getCondition();
    
    public I_PromotionReward getReward();
    
}
