package com.viettel.repository.common.domain.promotion;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_PromotionHeader extends I_Category {
    
    public SimpleDate getStartDate();
    
    public SimpleDate getEndDate();
    
    public String getDescription();
    
    public String getApplyFor();

}
