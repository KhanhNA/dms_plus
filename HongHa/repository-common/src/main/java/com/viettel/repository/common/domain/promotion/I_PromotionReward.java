package com.viettel.repository.common.domain.promotion;

import java.io.Serializable;
import java.math.BigDecimal;

public interface I_PromotionReward extends Serializable {

    public BigDecimal getPercentage();
    
    public String getProductId();
    
    public String getProductText();
    
    public BigDecimal getQuantity();
    
}
