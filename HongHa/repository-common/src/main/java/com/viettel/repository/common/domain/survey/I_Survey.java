package com.viettel.repository.common.domain.survey;

import java.util.List;

public interface I_Survey extends I_SurveyHeader{

    public List<I_SurveyQuestion> getQuestions();
    
}
