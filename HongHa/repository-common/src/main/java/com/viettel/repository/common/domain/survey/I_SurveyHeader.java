package com.viettel.repository.common.domain.survey;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.entity.SimpleDate;

public interface I_SurveyHeader extends I_Category {
    
    public SimpleDate getStartDate();
    
    public SimpleDate getEndDate();
    
    public boolean isRequired();

}
