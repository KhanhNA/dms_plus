package com.viettel.repository.common.domain.survey;

import java.io.Serializable;

public interface I_SurveyOption extends Serializable {
    
    public int getSeqNo();
    
    public String getName();
    
}
