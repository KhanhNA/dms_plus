package com.viettel.repository.common.domain.survey;

import java.io.Serializable;
import java.util.List;

public interface I_SurveyQuestion extends Serializable {
    
    public int getSeqNo();
    
    public String getName();

    public boolean isMultipleChoice();
    
    public boolean isRequired();
    
    public List<I_SurveyOption> getOptions();
    
}
