package com.viettel.repository.common.domain.user;

import com.viettel.repository.common.domain.common.I_DataOfDistributor;
import com.viettel.repository.common.domain.common.I_PO;

public interface I_User extends I_PO, I_UserEmbed, I_DataOfDistributor {

	public boolean isDefaultAdmin();

	public String getUsernameFull();

	public String getPassword();

	public String getRole();

	public boolean isVanSales();

	public static class Role {

		public static final String SUPER_ADMIN = "SUPER";
		public static final String SUPPORTER = "SUPPORTER";

		public static final String ADMIN = "AD";
		public static final String OBSERVER = "OBS";
		public static final String ADVANCE_SUPERVISOR = "A_SUP";
		public static final String SUPERVISOR = "SUP";
		public static final String SALESMAN = "SM";
		public static final String DISTRIBUTOR_ADMIN = "DIS";

	}

}
