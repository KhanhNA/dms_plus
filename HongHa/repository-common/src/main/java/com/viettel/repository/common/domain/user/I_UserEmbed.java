package com.viettel.repository.common.domain.user;

import com.viettel.repository.common.domain.common.I_POEmbed;

public interface I_UserEmbed extends I_POEmbed {

    public String getUsername();
    
    public String getFullname();
    
}
