package com.viettel.repository.common.entity;

import java.io.Serializable;

public class PageSizeRequest implements Serializable {

    private static final long serialVersionUID = -3716276826407276065L;

    private int pageNumber;
    private int pageSize;

    public PageSizeRequest(int pageNumber, int pageSize) {
        super();
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
