package com.viettel.repository.common.exception;

public class IdFormatException extends RuntimeException {

    private static final long serialVersionUID = -328200147156014784L;
    
    private String id;

    public IdFormatException() {
        super();
    }

    public IdFormatException(String id) {
        super();

        this.id = id;
    }

    public IdFormatException(String id, Throwable throwable) {
        super(throwable);

        this.id = id;
    }
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

}
