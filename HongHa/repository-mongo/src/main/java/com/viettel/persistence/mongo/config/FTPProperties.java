package com.viettel.persistence.mongo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Application properties.
 * 
 * @author trungkh
 */
@ConfigurationProperties("ftp")
public class FTPProperties {

    private String host;
    private int port;
    private String username;
    private String password;
    private String folder;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }
    
    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

}
