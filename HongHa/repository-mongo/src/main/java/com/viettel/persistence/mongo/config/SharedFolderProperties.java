package com.viettel.persistence.mongo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Application properties.
 * 
 * @author trungkh
 */
@ConfigurationProperties("shared.folder")
public class SharedFolderProperties {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
