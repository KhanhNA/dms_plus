package com.viettel.persistence.mongo.domain;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.persistence.mongo.domain.embed.CustomerEmbed;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.common.I_DataOfCustomer;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.entity.SimpleDate;

public class A_VisitAndOrder extends PO implements I_DataOfCustomer {

    private static final long serialVersionUID = -4486423788270854798L;

    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor._id";
    public static final String COLUMNNAME_CUSTOMER_ID = "customer._id";
    public static final String COLUMNNAME_CUSTOMER_NAME = "customer._id";
    public static final String COLUMNNAME_CREATED_BY_ID = "createdBy._id";
    public static final String COLUMNNAME_START_TIME_VALUE = "startTime.value";
    public static final String COLUMNNAME_CREATED_TIME_VALUE = COLUMNNAME_START_TIME_VALUE;

    private CategoryEmbed distributor;
    private CustomerEmbed customer;
    private UserEmbed createdBy;
    private SimpleDate startTime;
    
    public A_VisitAndOrder() {
        super();
    }
    
    public A_VisitAndOrder(I_Order order) {
        super(order);
        
        this.distributor = order.getDistributor() == null ? null : new CategoryEmbed(order.getDistributor());
        this.customer = order.getCustomer() == null ? null : new CustomerEmbed(order.getCustomer());
        this.createdBy = order.getCreatedBy() == null ? null : new UserEmbed(order.getCreatedBy());
        this.startTime = order.getCreatedTime();
    }
    
    public CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public CustomerEmbed getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEmbed customer) {
        this.customer = customer;
    }

    public UserEmbed getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserEmbed createdBy) {
        this.createdBy = createdBy;
    }

    protected SimpleDate getStartTime() {
        return startTime;
    }

    protected void setStartTime(SimpleDate startTime) {
        this.startTime = startTime;
    }
    
    @Override
    public SimpleDate getCreatedTime() {
        return getStartTime();
    }
    
    protected void setCreatedTime(SimpleDate createdTime) {
        this.startTime = createdTime;
    }

}
