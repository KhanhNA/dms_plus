package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.common.I_Category;

@Document(collection = "Area")
public class Area extends Category {

    private static final long serialVersionUID = -1350241576145500083L;
    
    public Area() {
        super();
    }
    
    public Area(I_Category category) {
        super(category);
    }

}
