package com.viettel.persistence.mongo.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.category.I_CalendarConfig;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "CalendarConfig")
public class CalendarConfig extends PO implements I_CalendarConfig {

    private static final long serialVersionUID = 7763979551635207290L;

    private List<Integer> workingDays;
    private List<SimpleDate> holidays;

    public CalendarConfig() {
        super();
    }
    
    public CalendarConfig(I_CalendarConfig calendarConfig) {
        super();
        
        this.workingDays = calendarConfig.getWorkingDays();
        this.holidays = calendarConfig.getHolidays();
    }
    
    public List<Integer> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<Integer> workingDays) {
        this.workingDays = workingDays;
    }

    public List<SimpleDate> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<SimpleDate> holidays) {
        this.holidays = holidays;
    }

}
