package com.viettel.persistence.mongo.domain;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.util.StringUtils;

public abstract class Category extends PO implements I_Category {

    private static final long serialVersionUID = -975777589819587341L;
    
    public static final String COLUMNNAME_NAME = "name";
    public static final String COLUMNNAME_SEARCH_NAME = "searchName";
    public static final String COLUMNNAME_CODE = "code";
    public static final String COLUMNNAME_DISTRIBUTOR = "distributor";
    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor._id";
    public static final String COLUMNNAME_DISTRIBUTOR_NAME = "distributor.name";
    public static final String COLUMNNAME_DISTRIBUTOR_CODE = "distributor.code";
    
    private String name;
    @SuppressWarnings("unused")
    private String searchName;
    private String code;
    private CategoryEmbed distributor;

    public Category() {
        super();

        this.name = null;
        this.searchName = null;
        this.code = null;
        this.distributor = null;
    }

    public Category(I_Category category) {
        super(category);

        this.name = category.getName();
        this.searchName = StringUtils.getSearchableString(category.getName());
        this.code = category.getCode();
        this.distributor = category.getDistributor() == null ? null : new CategoryEmbed(category.getDistributor());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.searchName = StringUtils.getSearchableString(name);
    }
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public I_CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public boolean isCanChangeName() {
        return false;
    }
    
    public boolean isCanChangeCode() {
        return false;
    }
    
    public void setSearchName(String str){
    	this.searchName = str;
    }
    
}
