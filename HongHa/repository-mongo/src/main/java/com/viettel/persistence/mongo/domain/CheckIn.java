package com.viettel.persistence.mongo.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.checkin.I_CheckIn;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "CheckIn")
public class CheckIn extends PO implements I_CheckIn {

    private static final long serialVersionUID = -7954522145457220542L;

    public static final String COLUMNNAME_CREATED_BY_ID = "createdBy.id";
    public static final String COLUMNNAME_CREATED_TIME_VALUE = "createdTime.value";

    private UserEmbed createdBy;
    private SimpleDate createdTime;

    private String note;
    private List<String> photos;
    private Location location;
    private CategoryEmbed province;

    public CheckIn() {
        super();
    }

    public CheckIn(I_CheckIn checkIn) {
        super(checkIn);

        this.createdBy = checkIn.getCreatedBy() == null ? null : new UserEmbed(checkIn.getCreatedBy());
        this.createdTime = checkIn.getCreatedTime();

        this.note = checkIn.getNote();
        this.photos = checkIn.getPhotos();
        this.location = checkIn.getLocation();
        this.province = checkIn.getProvince() == null ? null : new CategoryEmbed(checkIn.getProvince());
    }

    public UserEmbed getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserEmbed createdBy) {
        this.createdBy = createdBy;
    }

    public SimpleDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        this.createdTime = createdTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public CategoryEmbed getProvince() {
        return province;
    }

    public void setProvince(CategoryEmbed province) {
        this.province = province;
    }

}
