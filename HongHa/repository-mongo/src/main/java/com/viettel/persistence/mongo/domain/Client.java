package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.common.I_Category;

@Document(collection = "Client")
public class Client extends Category {

    private static final long serialVersionUID = 4538417826817640859L;
    
    public Client() {
        super();
    }
    
    public Client(I_Category category) {
        super(category);
    }

}
