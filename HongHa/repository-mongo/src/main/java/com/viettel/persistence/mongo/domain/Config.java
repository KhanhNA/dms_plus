package com.viettel.persistence.mongo.domain;

import java.util.Set;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.category.I_ClientConfig;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_SystemConfig;
import com.viettel.repository.common.entity.Location;

@Document(collection = "Config")
public class Config extends PO implements I_Config {

    private static final long serialVersionUID = 5202363354970843896L;

    // DEFAULT
    private String dateFormat;
    private String productPhoto;
    private Location location;
    private int limitAccount;

    // CALENDAR
    private int firstDayOfWeek;
    private int minimalDaysInFirstWeek;

    // SCHEDULE
    private int numberWeekOfFrequency;

    // NUMBER DAY ORDER PENDING EXPIRE
    private int numberDayOrderPendingExpire;
    // ORDER DATE TYPE
    private OrderDateType orderDateType;

    private long visitDurationKPI;
    private double visitDistanceKPI;
    private boolean canEditCustomerLocation;

    private Set<String> modules;
    private int numberDayInventoryExpire;
    
    private String productivityUnit;

    public Config() {
        super();
    }

    public Config(I_ClientConfig config) {
        super();

        fillClientConfigData(config);
    }

    public Config(I_SystemConfig config) {
        super();

        fillSystemConfigData(config);
    }

    @Transient
    public void fillClientConfigData(I_ClientConfig config) {
        this.location = config.getLocation();

        this.visitDurationKPI = config.getVisitDurationKPI();
        this.visitDistanceKPI = config.getVisitDistanceKPI();
        this.canEditCustomerLocation = config.isCanEditCustomerLocation();

        this.modules = config.getModules();
        this.numberDayInventoryExpire = config.getNumberDayInventoryExpire();
        
        this.productivityUnit = config.getProductivityUnit();
        this.limitAccount = config.getLimitAccount();
    }

    @Transient
    public void fillSystemConfigData(I_SystemConfig config) {
        this.dateFormat = config.getDateFormat();
        this.productPhoto = config.getProductPhoto();
        this.location = config.getLocation();

        this.firstDayOfWeek = config.getFirstDayOfWeek();
        this.minimalDaysInFirstWeek = config.getMinimalDaysInFirstWeek();

        this.numberWeekOfFrequency = config.getNumberWeekOfFrequency();

        this.numberDayOrderPendingExpire = config.getNumberDayOrderPendingExpire();
        this.orderDateType = config.getOrderDateType();
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getProductPhoto() {
        return productPhoto;
    }

    public void setProductPhoto(String productPhoto) {
        this.productPhoto = productPhoto;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    public void setFirstDayOfWeek(int firstDayOfWeek) {
        this.firstDayOfWeek = firstDayOfWeek;
    }

    public int getMinimalDaysInFirstWeek() {
        return minimalDaysInFirstWeek;
    }

    public void setMinimalDaysInFirstWeek(int minimalDaysInFirstWeek) {
        this.minimalDaysInFirstWeek = minimalDaysInFirstWeek;
    }

    public int getNumberWeekOfFrequency() {
        return numberWeekOfFrequency;
    }

    public void setNumberWeekOfFrequency(int numberWeekOfFrequency) {
        this.numberWeekOfFrequency = numberWeekOfFrequency;
    }

    public int getNumberDayOrderPendingExpire() {
        return numberDayOrderPendingExpire;
    }

    public void setNumberDayOrderPendingExpire(int numberDayOrderPendingExpire) {
        this.numberDayOrderPendingExpire = numberDayOrderPendingExpire;
    }

    public OrderDateType getOrderDateType() {
        return orderDateType;
    }

    public void setOrderDateType(OrderDateType orderDateType) {
        this.orderDateType = orderDateType;
    }

    public long getVisitDurationKPI() {
        return visitDurationKPI;
    }

    public void setVisitDurationKPI(long visitDurationKPI) {
        this.visitDurationKPI = visitDurationKPI;
    }

    public double getVisitDistanceKPI() {
        return visitDistanceKPI;
    }

    public void setVisitDistanceKPI(double visitDistanceKPI) {
        this.visitDistanceKPI = visitDistanceKPI;
    }

    public boolean isCanEditCustomerLocation() {
        return canEditCustomerLocation;
    }

    public void setCanEditCustomerLocation(boolean canEditCustomerLocation) {
        this.canEditCustomerLocation = canEditCustomerLocation;
    }

    public Set<String> getModules() {
        return modules;
    }

    public void setModules(Set<String> modules) {
        this.modules = modules;
    }

    public int getNumberDayInventoryExpire() {
        return numberDayInventoryExpire;
    }

    public void setNumberDayInventoryExpire(int numberDayInventoryExpire) {
        this.numberDayInventoryExpire = numberDayInventoryExpire;
    }
    
    public String getProductivityUnit() {
        return productivityUnit;
    }
    
    public void setProductivityUnit(String productivityUnit) {
        this.productivityUnit = productivityUnit;
    }

	@Override
	public int getLimitAccount() {
		return limitAccount;
	}

}
