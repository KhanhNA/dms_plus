package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.persistence.mongo.domain.embed.Schedule;
import com.viettel.persistence.mongo.domain.embed.ScheduleItem;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "Customer")
public class Customer extends Category implements POApprovable, I_Customer {

	private static final long serialVersionUID = 6488644531172649264L;

	public static final String COLUMNNAME_CONTACT = "contact";
	public static final String COLUMNNAME_MOBILE = "mobile";
	public static final String COLUMNNAME_PHONE = "phone";
	public static final String COLUMNNAME_EMAIL = "email";
	public static final String COLUMNNAME_CUSTOMER_TYPE_ID = "customerType._id";
	public static final String COLUMNNAME_AREA_ID = "area._id";
	public static final String COLUMNNAME_PHOTOS = "photos";
	public static final String COLUMNNAME_LOCATION = "location";
	public static final String COLUMNNAME_CREATED_BY_ID = "createdBy._id";
	public static final String COLUMNNAME_CREATED_TIME_VALUE = "createdTime.value";
	public static final String COLUMNNAME_SCHEDULE = "schedule";
	public static final String COLUMNNAME_SCHEDULE_ROUTE_ID = "schedule." + Schedule.COLUMNNAME_ROUTE_ID;
	public static final String COLUMNNAME_SCHEDULE_ITEM = "schedule." + Schedule.COLUMNNAME_ITEM;
	public static final String COLUMNNAME_SCHEDULE_ITEM_WEEKS = "schedule." + Schedule.COLUMNNAME_ITEM + "."
			+ ScheduleItem.COLUMNNAME_WEEKS;
	public static final String COLUMNNAME_APPROVED_TIME = "approvedTime";
	public static final String COLUMNNAME_APPROVED_TIME_YEAR = "approvedTime.year";
	public static final String COLUMNNAME_APPROVED_TIME_MONTH = "approvedTime.month";
	public static final String COLUMNNAME_APPROVED_TIME_DATE = "approvedTime.date";
	public static final String COLUMNNAME_APPROVED_TIME_HOUR = "approvedTime.hour";
	public static final String COLUMNNAME_APPROVED_TIME_MINUTE = "approvedTime.minute";
	public static final String COLUMNNAME_APPROVED_TIME_SECOND = "approvedTime.second";
	public static final String COLUMNNAME_APPROVED_TIME_VALUE = "approvedTime.value";
	public static final String COLUMNNAME_APPROVED_TIME_ISODATE = "approvedTime.isoDate";
	public static final String COLUMNNAME_APPROVED_BY = "approvedBy";
	public static final String COLUMNNAME_APPROVED_BY_ID = "approvedBy._id";
	public static final String COLUMNNAME_APPROVED_BY_USERNAME = "approvedBy.username";
	public static final String COLUMNNAME_APPROVED_BY_FULLNAME = "approvedBy.fullname";
	public static final String COLUMNNAME_LAST_MODIFIED_TIME = "lastModifiedTime";

	private String contact;
	private String mobile;
	private String phone;
	private String email;
	private Location location;
	private SimpleDate createdTime;
	private int approveStatus;
	private UserEmbed approvedBy;
	private SimpleDate approvedTime;

	private UserEmbed createdBy;
	private CategoryEmbed customerType;
	private CategoryEmbed area;
	private Schedule schedule;

	public Customer() {
		super();
	}

	public Customer(I_Customer customer) {
		super(customer);

		this.contact = customer.getContact();
		this.mobile = customer.getMobile();
		this.phone = customer.getPhone();
		this.email = customer.getEmail();
		this.location = customer.getLocation();
		this.createdTime = customer.getCreatedTime();
		this.approveStatus = customer.getApproveStatus();
		this.approvedBy = customer.getApprovedBy() == null ? null : new UserEmbed(customer.getApprovedBy());
		this.approvedTime = customer.getApprovedTime();
		this.createdBy = customer.getCreatedBy() == null ? null : new UserEmbed(customer.getCreatedBy());
		this.customerType = customer.getCustomerType() == null ? null : new CategoryEmbed(customer.getCustomerType());
		this.area = customer.getArea() == null ? null : new CategoryEmbed(customer.getArea());
		this.schedule = customer.getSchedule() == null ? null : new Schedule(customer.getSchedule());
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CategoryEmbed getCustomerType() {
		return customerType;
	}

	public void setCustomerType(CategoryEmbed customerType) {
		this.customerType = customerType;
	}

	public CategoryEmbed getArea() {
		return area;
	}

	public void setArea(CategoryEmbed area) {
		this.area = area;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public UserEmbed getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserEmbed createdBy) {
		this.createdBy = createdBy;
	}

	public SimpleDate getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(SimpleDate createdTime) {
		this.createdTime = createdTime;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public int getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(int approveStatus) {
		this.approveStatus = approveStatus;
	}

	public UserEmbed getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(UserEmbed approvedBy) {
		this.approvedBy = approvedBy;
	}

	public SimpleDate getApprovedTime() {
		return approvedTime;
	}

	public void setApprovedTime(SimpleDate approvedTime) {
		this.approvedTime = approvedTime;
	}

}
