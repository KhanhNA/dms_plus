package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.common.I_Category;

@Document(collection = "CustomerType")
public class CustomerType extends Category {

    private static final long serialVersionUID = 6488644531172649264L;
    
    public CustomerType() {
        super();
    }
    
    public CustomerType(I_Category category) {
        super(category);
    }
    
}
