package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.util.StringUtils;

@Document(collection = "Distributor")
public class Distributor extends Category implements I_Distributor {

    private static final long serialVersionUID = 8782304292353101761L;

    public static final String COLUMNNAME_SUPERVISOR_ID = "supervisor._id";
    public static final String COLUMNNAME_REGION_ID = "region._id";
    
    private UserEmbed supervisor;
    private CategoryEmbed region;

    public Distributor() {
        super();
    }
    
    public Distributor(I_Distributor distributor) {
        super(distributor);
        setSearchName(StringUtils.getSearchableString(distributor.getName() +" "+ distributor.getCode() ));
        this.supervisor = distributor.getSupervisor() == null ? null : new UserEmbed(distributor.getSupervisor());
        this.region = distributor.getRegion() == null ? null : new CategoryEmbed(distributor.getRegion());
    }
    
    public Distributor(I_CategoryEmbed distributor){
    	super();
    	this.setId(PO.convertId(distributor.getId()));
    	this.setName(distributor.getName());
    	this.setCode(distributor.getCode());
    }
    
    public UserEmbed getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(UserEmbed supervisor) {
        this.supervisor = supervisor;
    }

	public CategoryEmbed getRegion() {
		return region;
	}

	public void setRegion(CategoryEmbed region) {
		this.region = region;
	}
    
}
