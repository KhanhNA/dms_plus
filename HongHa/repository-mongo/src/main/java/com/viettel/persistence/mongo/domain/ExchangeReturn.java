package com.viettel.persistence.mongo.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.persistence.mongo.domain.embed.CustomerEmbed;
import com.viettel.persistence.mongo.domain.embed.ProductQuantity;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.data.I_ExchangeReturn;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "ExchangeReturn")
public class ExchangeReturn extends PO implements I_ExchangeReturn {

    private static final long serialVersionUID = -2660284870905603354L;

    public static final String COLUMNNAME_EXCHANGE = "exchange";

    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor._id";
    public static final String COLUMNNAME_DISTRIBUTOR_CODE = "distributor.code";
    public static final String COLUMNNAME_DISTRIBUTOR_NAME = "distributor.name";

    public static final String COLUMNNAME_CUSTOMER_ID = "customer._id";
    public static final String COLUMNNAME_CUSTOMER_CODE = "customer.code";
    public static final String COLUMNNAME_CUSTOMER_NAME = "customer.name";
    public static final String COLUMNNAME_CUSTOMER_AREA_ID = "customer.area._id";

    public static final String COLUMNNAME_CREATED_BY_ID = "createdBy._id";

    public static final String COLUMNNAME_CREATED_TIME_VALUE = "createdTime.value";

    private boolean exchange;

    private CategoryEmbed distributor;
    private UserEmbed createdBy;
    private CustomerEmbed customer;
    private SimpleDate createdTime;
    private BigDecimal quantity;
    private List<ProductQuantity> details;
    
    public ExchangeReturn() {
        super();
    }
    
    public ExchangeReturn(I_ExchangeReturn er) {
        super(er);
        
        this.exchange = er.isExchange();
        this.distributor = er.getDistributor() == null ? null : new CategoryEmbed(er.getDistributor());
        this.createdBy = er.getCreatedBy() == null ? null : new UserEmbed(er.getCreatedBy());
        this.customer = er.getCustomer() == null ? null : new CustomerEmbed(er.getCustomer());
        this.createdTime = er.getCreatedTime();
        this.quantity = er.getQuantity();
        
        if (er.getDetails() != null) {
            this.details = new ArrayList<>(er.getDetails().size());
            for (I_ProductQuantity pq : er.getDetails()) {
                this.details.add(new ProductQuantity(pq));
            }
        }
    }

    public ExchangeReturn(boolean exchange, CategoryEmbed distributor, UserEmbed createdBy, CustomerEmbed customer,
            SimpleDate createdTime, BigDecimal quantity, List<ProductQuantity> details) {
        super();
        
    }

    public boolean isExchange() {
        return exchange;
    }

    public void setExchange(boolean exchange) {
        this.exchange = exchange;
    }

    public CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public UserEmbed getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserEmbed createdBy) {
        this.createdBy = createdBy;
    }

    public CustomerEmbed getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEmbed customer) {
        this.customer = customer;
    }

    public SimpleDate getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(SimpleDate createdTime) {
        this.createdTime = createdTime;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }
    
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
    
    public List<I_ProductQuantity> getDetails() {
        return PO.convertList(I_ProductQuantity.class, details);
    }

    public void setDetails(List<ProductQuantity> details) {
        this.details = details;
    }

}
