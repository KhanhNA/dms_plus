package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.file.I_FileMetadata;

@Document(collection = "FileMetadata")
public class FileMetadata extends PO implements I_FileMetadata {

    private static final long serialVersionUID = -5980553510127811402L;

    private String fileName;
    private String contentType;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

}
