package com.viettel.persistence.mongo.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.persistence.mongo.domain.embed.ProductQuantity;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_ProductQuantity;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "Inventory")
public class Inventory extends PO implements I_Inventory {

    private static final long serialVersionUID = 6729304115985124711L;
    
    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor._id";
    public static final String COLUMNNAME_TIME_VALUE = "time.value";
    
    private CategoryEmbed distributor;
    private SimpleDate time;
    private List<ProductQuantity> details;
    
    public Inventory() {
        super();
    }
    
    public Inventory(I_Inventory inventory) {
        super(inventory);
        
        this.distributor = inventory.getDistributor() == null ? null : new CategoryEmbed(inventory.getDistributor());
        this.time = inventory.getTime();

        if (inventory.getDetails() != null) {
            this.details = new ArrayList<>(inventory.getDetails().size());
            for (I_ProductQuantity pq : inventory.getDetails()) {
                this.details.add(new ProductQuantity(pq));
            }
        }
    }

    public CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(CategoryEmbed distributor) {
        this.distributor = distributor;
    }

    public SimpleDate getTime() {
        return time;
    }

    public void setTime(SimpleDate time) {
        this.time = time;
    }

	public List<I_ProductQuantity> getDetails() {
		return PO.convertList(I_ProductQuantity.class, details);
	}

	public void setDetails(List<ProductQuantity> details) {
		this.details = details;
	}

}
