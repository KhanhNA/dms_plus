package com.viettel.persistence.mongo.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.OrderDetail;
import com.viettel.persistence.mongo.domain.embed.OrderPromotion;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderDetail;
import com.viettel.repository.common.domain.data.I_OrderPromotion;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "VisitAndOrder")
public class Order extends A_VisitAndOrder implements POApprovable, I_Order {

    private static final long serialVersionUID = -1522058115974076896L;

    public static final int DELIVERY_TYPE_IMMEDIATE = 0;
    public static final int DELIVERY_TYPE_IN_DAY = 1;
    public static final int DELIVERY_TYPE_ANOTHER_DAY = 2;

    public static final String COLUMNNAME_IS_VISIT = "isVisit";
    public static final String COLUMNNAME_IS_ORDER = "isOrder";

    public static final String COLUMNNAME_CODE = "code";

    public static final String COLUMNNAME_CREATED_TIME_VALUE = COLUMNNAME_START_TIME_VALUE;
    public static final String COLUMNNAME_DELIVERY_TYPE = "deliveryType";
    public static final String COLUMNNAME_DELIVERY_DATE = "deliveryDate";

    public static final String COLUMNNAME_APPROVE_TIME_VALUE = "approveTime.value";
    public static final String COLUMNNAME_APPROVE_USER = "approveUser";

    public static final String COLUMNNAME_SUB_TOTAL = "subTotal";
    public static final String COLUMNNAME_PROMOTION_AMT = "promotionAmt";
    public static final String COLUMNNAME_DISCOUNT_AMT = "discountAmt";

    public static final String COLUMNNAME_GRAND_TOTAL = "grandTotal";

    private boolean isVisit;
    private boolean isOrder;
    private String code;
    
    private int approveStatus;
    private SimpleDate approvedTime;
    private UserEmbed approvedBy;

    private int deliveryType;
    private SimpleDate deliveryTime;
    
    /** Thong tin khuyen mai khac */
    private String comment;

    /** Gia tri don hang truoc khuyen mai */
    private BigDecimal subTotal;

    /** Gia tri khuyen mai */
    private BigDecimal promotionAmt;

    /** Manual Discount Percentage (null if amount is fixed) */
    private BigDecimal discountPercentage;
    /**
     * Manual Discount Amount discountAmt = fixed or = (subTotal - promotionAmt)
     * * discount
     */
    private BigDecimal discountAmt;

    /**
     * Gia tri cuoi cung cua don hang = subTotal - promotionAmt - discountAmt
     */
    private BigDecimal grandTotal;

    private BigDecimal quantity;
    private BigDecimal productivity;

    private List<OrderDetail> details;
    private List<OrderPromotion> promotions;

    private boolean vanSales;
    
    public Order() {
        super();

        setVisit(false);
        setOrder(true);
    }
    
    public Order(I_Order order) {
        super(order);
        
        setOrder(true);
        
        this.isVisit = order.isWithVisit();
        this.code = order.getCode();
        this.approveStatus = order.getApproveStatus();
        this.approvedTime = order.getApprovedTime();
        this.approvedBy = order.getApprovedBy() == null ? null : new UserEmbed(order.getApprovedBy());
        this.deliveryType = order.getDeliveryType();
        this.deliveryTime = order.getDeliveryTime();
        this.comment = order.getComment();
        this.subTotal = order.getSubTotal();
        this.promotionAmt = order.getPromotionAmt();
        this.discountPercentage = order.getDiscountPercentage();
        this.discountAmt = order.getDiscountAmt();
        this.grandTotal = order.getGrandTotal();
        this.quantity = order.getQuantity();
        this.productivity = order.getProductivity();
        this.vanSales = order.isVanSales();
              
        if (order.getDetails() != null) {
            this.details = new ArrayList<>(order.getDetails().size());
            for (I_OrderDetail orderDetail : order.getDetails()) {
                this.details.add(new OrderDetail(orderDetail));
            }
        }

        if (order.getPromotions() != null) {
            this.promotions = new ArrayList<>(order.getPromotions().size());
            for (I_OrderPromotion orderPromotion : order.getPromotions()) {
                this.promotions.add(new OrderPromotion(orderPromotion));
            }
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }

    public SimpleDate getApprovedTime() {
        return approvedTime;
    }

    public void setApprovedTime(SimpleDate approvedTime) {
        this.approvedTime = approvedTime;
    }

    public UserEmbed getApprovedBy() {
        return approvedBy;
    }
    
    public void setApprovedBy(UserEmbed approvedBy) {
        this.approvedBy = approvedBy;
    }

    public int getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(int deliveryType) {
        this.deliveryType = deliveryType;
    }

    public SimpleDate getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(SimpleDate deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getPromotionAmt() {
        return promotionAmt;
    }

    public void setPromotionAmt(BigDecimal promotionAmt) {
        this.promotionAmt = promotionAmt;
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public BigDecimal getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(BigDecimal discountAmt) {
        this.discountAmt = discountAmt;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public List<I_OrderDetail> getDetails() {
        return PO.convertList(I_OrderDetail.class, details);
    }

    public void setDetails(List<OrderDetail> details) {
        this.details = details;
    }

    public List<I_OrderPromotion> getPromotions() {
        return PO.convertList(I_OrderPromotion.class, promotions);
    }

    public void setPromotions(List<OrderPromotion> promotions) {
        this.promotions = promotions;
    }

    public SimpleDate getCreatedTime() {
        return getStartTime();
    }

    public void setCreatedTime(SimpleDate createdTime) {
        setStartTime(new SimpleDate(createdTime));
    }

    // PROTECTED
    public boolean isVisit() {
        return isVisit;
    }
    
    @Override
    public boolean isWithVisit() {
        return isVisit();
    }

    protected void setVisit(boolean isVisit) {
        this.isVisit = isVisit;
    }

    public boolean isOrder() {
        return isOrder;
    }

    protected void setOrder(boolean isOrder) {
        this.isOrder = isOrder;
    }

    public boolean isVanSales() {
        return vanSales;
    }

    public void setVanSales(boolean vanSales) {
        this.vanSales = vanSales;
    }

    @Override
    public int getNbSKU() {
        return getDetails() == null ? 0 : getDetails().size();
    }

}
