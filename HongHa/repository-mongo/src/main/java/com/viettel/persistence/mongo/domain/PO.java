package com.viettel.persistence.mongo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.exception.IdFormatException;

public abstract class PO implements Serializable, I_PO {

	private static final long serialVersionUID = 3629393676420041333L;

	public static final ObjectId ROOT_ID = ObjectId.createFromLegacyFormat(0, 0, 0);

	public static final String COLUMNNAME_ID = "id";
	public static final String COLUMNNAME_CLIENT_ID = "clientId";
	public static final String COLUMNNAME_DRAFT = "draft";
	public static final String COLUMNNAME_ACTIVE = "active";
	public static final String COLUMNNAME_LAST_MODIFIED_TIME = "lastModifiedTime";
	public static final String COLUMNNAME_LAST_MODIFIED_USER = "lastModifiedUser";

	@Id
	private ObjectId id;
	private ObjectId clientId;
	private boolean draft;
	private boolean active;
	private SimpleDate lastModifiedTime;
	private UserEmbed lastModifiedUser;

	public PO() {
		this.id = null;
		this.clientId = null;
		this.draft = false;
		this.active = true;
	}

	public PO(I_PO po) {
		Assert.notNull(po);

		this.id = PO.convertId(po.getId());
		this.clientId = PO.convertId(po.getClientId());
		this.draft = po.isDraft();
		this.active = po.isActive();
	}

	public String getId() {
		return PO.convertId(this.id);
	}

	public ObjectId _getId() {
		return this.id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getClientId() {
		return PO.convertId(this.clientId);
	}

	public ObjectId _getClientId() {
		return this.clientId;
	}

	public void setClientId(ObjectId clientId) {
		this.clientId = clientId;
	}

	public boolean isDraft() {
		return draft;
	}

	public void setDraft(boolean draft) {
		this.draft = draft;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public SimpleDate getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(SimpleDate lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public UserEmbed getLastModifiedUser() {
		return lastModifiedUser;
	}

	public void setLastModifiedUser(UserEmbed lastModifiedUser) {
		this.lastModifiedUser = lastModifiedUser;
	}

	/******************** OBJECT'S METHODS ********************/
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj.getClass().equals(this.getClass())) {
			PO po = (PO) obj;
			if (po.getId() != null && this.getId() != null) {
				return po.getId().equals(this.getId());
			}

			return super.equals(obj);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}

	@Override
	public String toString() {
		return new StringBuilder(this.getClass().getSimpleName()).append("=").append(getId()).toString();
	}

	/******************** STATIC METHODS ********************/
	public static ObjectId convertId(String id) {
		if (id != null) {
			try {
				return new ObjectId(id);
			} catch (IllegalArgumentException ex) {
				throw new IdFormatException(id, ex);
			}
		}

		return null;
	}

	public static String convertId(ObjectId id) {
		return id == null ? null : id.toString();
	}

	public static Set<ObjectId> convertIds(Collection<String> _ids) {
		if (_ids == null) {
			return null;
		}

		if (_ids.isEmpty()) {
			return Collections.emptySet();
		}

		Set<ObjectId> ids = new HashSet<>();
		for (String _id : _ids) {
			ids.add(convertId(_id));
		}

		return ids;
	}

	public static Set<String> convertIdsToSet(Collection<String> _ids) {
		if (_ids == null) {
			return null;
		}

		if (_ids.isEmpty()) {
			return Collections.emptySet();
		}

		Set<String> ids = new HashSet<>();
		for (String _id : _ids) {
			ids.add(_id);
		}

		return ids;
	}

	public static Set<String> convertObjectIds(Collection<ObjectId> _ids) {
		if (_ids == null) {
			return null;
		}

		if (_ids.isEmpty()) {
			return Collections.emptySet();
		}

		Set<String> ids = new HashSet<>();
		for (ObjectId _id : _ids) {
			ids.add(_id.toString());
		}

		return ids;
	}

	public static <T> List<T> convertList(Class<T> clazz, List<? extends T> list) {
		return list == null ? null : new ArrayList<>(list);
	}

}
