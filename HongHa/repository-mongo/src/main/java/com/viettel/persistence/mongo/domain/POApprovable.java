package com.viettel.persistence.mongo.domain;

import com.viettel.repository.common.domain.common.I_Approvable;

public interface POApprovable extends I_Approvable {
    
    public static final String COLUMNNAME_APPROVE_STATUS = "approveStatus";
    
}