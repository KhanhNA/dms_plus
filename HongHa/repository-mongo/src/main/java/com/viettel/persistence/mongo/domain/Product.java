package com.viettel.persistence.mongo.domain;

import java.math.BigDecimal;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.repository.common.domain.product.I_Product;

@Document(collection = "Product")
public class Product extends Category implements I_Product {

    private static final long serialVersionUID = -4897491701019327268L;

    public static final String COLUMNNAME_PRODUCT_CATEGORY_ID = "productCategory._id";
    public static final String COLUMNNAME_DESCRIPTION = "description";
    public static final String COLUMNNAME_CODE = "code";
    public static final String COLUMNNAME_PRICE = "price";
    public static final String COLUMNNAME_UOM_CODE = "uom.code";
    public static final String COLUMNNAME_UOM_NAME = "uom.name";
    public static final String COLUMNNAME_UOM_ID = "uom._id";
    public static final String COLUMNNAME_PHOTO = "photo";

    private BigDecimal price;
    private BigDecimal productivity;
    private String photo;
    private String description;

    private CategoryEmbed uom;
    private CategoryEmbed productCategory;

    public Product() {
        super();
    }

    public Product(I_Product product) {
        super(product);

        this.price = product.getPrice();
        this.productivity = product.getProductivity();
        this.photo = product.getPhoto();
        this.description = product.getDescription();

        this.uom = product.getUom() == null ? null : new CategoryEmbed(product.getUom());
        this.productCategory = product.getProductCategory() == null ? null
                : new CategoryEmbed(product.getProductCategory());
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public CategoryEmbed getUom() {
        return uom;
    }

    public void setUom(CategoryEmbed uom) {
        this.uom = uom;
    }

    public CategoryEmbed getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(CategoryEmbed productCategory) {
        this.productCategory = productCategory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
