package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.common.I_Category;

@Document(collection = "ProductCategory")
public class ProductCategory extends Category {

    private static final long serialVersionUID = 6488644531172649264L;
    
    public ProductCategory() {
        super();
    }
    
    public ProductCategory(I_Category category) {
        super(category);
    }
}
