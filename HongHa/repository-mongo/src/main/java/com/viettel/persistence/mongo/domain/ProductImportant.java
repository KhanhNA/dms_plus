package com.viettel.persistence.mongo.domain;

import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ProductImportant")
public class ProductImportant extends PO {

	private static final long serialVersionUID = -247938481884662645L;

	private Set<ObjectId> productIds;

	public Set<ObjectId> getProductIds() {
		return productIds;
	}

	public void setProductIds(Set<ObjectId> productIds) {
		this.productIds = productIds;
	}

}
