package com.viettel.persistence.mongo.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.PromotionDetail;
import com.viettel.repository.common.domain.promotion.I_Promotion;
import com.viettel.repository.common.domain.promotion.I_PromotionDetail;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "Promotion")
public class Promotion extends Category implements I_Promotion {

    private static final long serialVersionUID = 5767023932450786132L;

    public static final String COLUMNNAME_START_DATE_VALUE = "startDate.value";
    public static final String COLUMNNAME_END_DATE_VALUE = "endDate.value";
    public static final String COLUMNNAME_FOR_ALL_DISTRIBUTOR = "forAllDistributor";
    public static final String COLUMNNAME_DISTRIBUTOR_IDS = "distributorIds";

    private SimpleDate startDate;
    private SimpleDate endDate;
    private String description;
    private String applyFor;
    private List<PromotionDetail> details;
    private boolean forAllDistributor;
    private Set<ObjectId> distributorIds;

    public Promotion() {
        super();
    }

    public Promotion(I_Promotion promotion) {
        super(promotion);

        this.startDate = promotion.getStartDate();
        this.endDate = promotion.getEndDate();
        this.description = promotion.getDescription();
        this.applyFor = promotion.getApplyFor();

        if (promotion.getDetails() != null) {
            this.details = new ArrayList<>(promotion.getDetails().size());
            for (I_PromotionDetail promotionDetail : promotion.getDetails()) {
                this.details.add(new PromotionDetail(promotionDetail));
            }
        }
        this.forAllDistributor = promotion.isForAllDistributor();
        this.distributorIds = promotion.getDistributorIds() == null ? null
                : new HashSet<>(PO.convertIds(promotion.getDistributorIds()));
    }

    public SimpleDate getStartDate() {
        return startDate;
    }

    public void setStartDate(SimpleDate startDate) {
        this.startDate = startDate;
    }

    public SimpleDate getEndDate() {
        return endDate;
    }

    public void setEndDate(SimpleDate endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getApplyFor() {
        return applyFor;
    }

    public void setApplyFor(String applyFor) {
        this.applyFor = applyFor;
    }

    public List<I_PromotionDetail> getDetails() {
        return PO.convertList(I_PromotionDetail.class, details);
    }

    public void setDetails(List<PromotionDetail> details) {
        this.details = details;
    }

    public boolean isForAllDistributor() {
        return forAllDistributor;
    }

    public void setForAllDistributor(boolean forAllDistributor) {
        this.forAllDistributor = forAllDistributor;
    }

    public Set<String> getDistributorIds() {
        return this.distributorIds == null ? null : new HashSet<>(PO.convertObjectIds(this.distributorIds));
    }

    public void setDistributorIds(Set<ObjectId> distributorIds) {
        this.distributorIds = distributorIds;
    }

}
