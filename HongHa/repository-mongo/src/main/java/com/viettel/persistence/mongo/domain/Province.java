package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.common.I_Category;

@Document(collection = "Province")
public class Province extends Category {

    private static final long serialVersionUID = -5412291803462967601L;
    
    public Province() {
        super();
    }
    
    public Province(I_Category category) {
        super(category);
    }
    
}
