package com.viettel.persistence.mongo.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;

@Document(collection = "Region")
public class Region extends Category implements I_Region {

	private static final long serialVersionUID = 1955872322569277880L;
	public static final String COLUMNNAME_ROOT = "root";

	private I_CategoryEmbed parent;
	private List<I_CategoryEmbed> children;
	private Set<I_CategoryEmbed> distributors;
	private boolean root;
	private boolean leaf;

	public Region() {
		super();
	}

	public Region(I_Region region) {
		super(region);
		this.leaf = region.isLeaf();
		this.root = region.isRoot();

		parent = region.getParent() == null ? null : new CategoryEmbed(region.getParent());
		
		if (region.getDistributors() != null) {
			this.distributors = new HashSet<>();
			for (I_CategoryEmbed c : region.getDistributors()) {
				this.distributors.add(new CategoryEmbed(c));
			}
		}

		if (region.getChildren() != null) {
			this.children = new ArrayList<>(region.getChildren().size());
			for (I_CategoryEmbed c : region.getChildren()) {
				this.children.add(new CategoryEmbed(c));
			}
		}
	}

	@Override
	public I_CategoryEmbed getParent() {
		return parent;
	}

	@Override
	public List<I_CategoryEmbed> getChildren() {
		return children;
	}

	@Override
	public boolean isRoot() {
		return root;
	}

	@Override
	public boolean isLeaf() {
		return leaf;
	}

	public void setParent(I_CategoryEmbed parent) {
		this.parent = parent;
	}

	public void setChildren(List<I_CategoryEmbed> children) {
		this.children = children;
	}


	public void setRoot(boolean root) {
		this.root = root;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	@Override
	public Set<I_CategoryEmbed> getDistributors() {
		return distributors;
	}

	public void setDistributors(Set<I_CategoryEmbed> distributors) {
		this.distributors = distributors;
	}
}
