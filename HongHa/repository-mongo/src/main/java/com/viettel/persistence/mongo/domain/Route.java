package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.customer.I_Route;

@Document(collection = "Route")
public class Route extends Category implements I_Route {

    private static final long serialVersionUID = -6365294573313110385L;
    
    public static final String COLUMNNAME_SALESMAN = "salesman";
    public static final String COLUMNNAME_SALESMAN_ID = "salesman._id";

    private UserEmbed salesman;
    
    public Route() {
        super();
    }
    
    public Route(I_Route route) {
        super(route);
        
        this.salesman = route.getSalesman() == null ? null : new UserEmbed(route.getSalesman());
    }

    public UserEmbed getSalesman() {
        return salesman;
    }

    public void setSalesman(UserEmbed salesman) {
        this.salesman = salesman;
    }
    
    public boolean isCanChangeName() {
        return true;
    };

}
