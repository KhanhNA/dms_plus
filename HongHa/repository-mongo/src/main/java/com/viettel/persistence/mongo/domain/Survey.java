package com.viettel.persistence.mongo.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.SurveyQuestion;
import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "Survey")
public class Survey extends Category implements I_Survey {

    private static final long serialVersionUID = 3227127774937212395L;

    public static final String COLUMNNAME_START_DATE_VALUE = "startDate.value";
    public static final String COLUMNNAME_END_DATE_VALUE = "endDate.value";
    public static final String COLUMNNAME_REQUIRED = "required";

    private SimpleDate startDate;
    private SimpleDate endDate;
    private boolean required;

    private List<SurveyQuestion> questions;
    
    public Survey() {
        super();
    }
    
    public Survey(I_Survey survey) {
        super(survey);
        
        this.startDate = survey.getStartDate();
        this.endDate = survey.getEndDate();
        this.required = survey.isRequired();
        
        if (survey.getQuestions() != null) {
            this.questions = new ArrayList<>(survey.getQuestions().size());
            for (I_SurveyQuestion surveyQuestion : survey.getQuestions()) {
                this.questions.add(new SurveyQuestion(surveyQuestion));
            }
        }
    }

    public SimpleDate getStartDate() {
        return startDate;
    }

    public void setStartDate(SimpleDate startDate) {
        this.startDate = startDate;
    }

    public SimpleDate getEndDate() {
        return endDate;
    }

    public void setEndDate(SimpleDate endDate) {
        this.endDate = endDate;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<I_SurveyQuestion> getQuestions() {
        return PO.convertList(I_SurveyQuestion.class, questions);
    }

    public void setQuestions(List<SurveyQuestion> questions) {
        this.questions = questions;
    }

}
