package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.common.I_Category;

@Document(collection = "UOM")
public class UOM extends Category {

    private static final long serialVersionUID = 6488644531172649264L;
    
    public UOM() {
        super();
    }
    
    public UOM(I_Category category) {
        super(category);
    }
    
}
