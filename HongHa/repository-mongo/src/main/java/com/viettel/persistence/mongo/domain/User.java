package com.viettel.persistence.mongo.domain;

import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.util.StringUtils;

@Document(collection = "User")
public class User extends PO implements I_User {

    private static final long serialVersionUID = -2980948476333281024L;

    public static final String COLUMNNAME_DEFAULT_ADMIN = "defaultAdmin";
    public static final String COLUMNNAME_USERNAME_FULL = "usernameFull";
    public static final String COLUMNNAME_USERNAME = "username";
    public static final String COLUMNNAME_FULLNAME = "fullname";
    public static final String COLUMNNAME_SEARCH_FULLNAME = "searchFullname";
    public static final String COLUMNNAME_ROLE = "role";
    public static final String COLUMNNAME_DISTRIBUTOR = "distributor";
    public static final String COLUMNNAME_DISTRIBUTOR_ID = "distributor._id";
    public static final String COLUMNNAME_REGION_ID = "region._id";
    public static final String COLUMNNAME_VAN_SALES = "vanSales";

    private boolean defaultAdmin;
    
    private String usernameFull;
    private String username;
    private String fullname;
    @SuppressWarnings("unused")
    private String searchFullname;
    private String password;
    private String role;
    private boolean vanSales;
    
    private CategoryEmbed distributor;
    
    private Set<ObjectId> distributorIds;
    private Set<ObjectId> regionIds;
    
    public User() {
        super();
    }
    
    public User(I_User user) {
        super(user);
        
        this.defaultAdmin = user.isDefaultAdmin();
        this.usernameFull = user.getUsernameFull();
        this.username = user.getUsername();
        this.fullname = user.getFullname();
        this.searchFullname = StringUtils.getSearchableString(user.getFullname());
        this.password = user.getPassword();
        this.role = user.getRole();
        this.vanSales = user.isVanSales();
        this.distributor = user.getDistributor() == null ? null : new CategoryEmbed(user.getDistributor());
    }
    
    public boolean isDefaultAdmin() {
        return defaultAdmin;
    }
    
    public void setDefaultAdmin(boolean defaultAdmin) {
        this.defaultAdmin = defaultAdmin;
    }
    
    public String getUsernameFull() {
        return usernameFull;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
        this.searchFullname = StringUtils.getSearchableString(fullname);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    public boolean isVanSales() {
        return vanSales;
    }
    
    public void setVanSales(boolean vanSales) {
        this.vanSales = vanSales;
    }

    public CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(CategoryEmbed distributor) {
        this.distributor = distributor;
    }
    
    public Set<ObjectId> getDistributorIds() {
        return distributorIds;
    }
    
    public void setDistributorIds(Set<ObjectId> distributorIds) {
        this.distributorIds = distributorIds;
    }

	public Set<ObjectId> getRegionIds() {
		return regionIds;
	}

	public void setRegionIds(Set<ObjectId> regionIds) {
		this.regionIds = regionIds;
	}
    
}
