package com.viettel.persistence.mongo.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.persistence.mongo.domain.embed.OrderDetail;
import com.viettel.persistence.mongo.domain.embed.OrderPromotion;
import com.viettel.persistence.mongo.domain.embed.SurveyAnswerContent;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.repository.common.domain.data.I_SurveyAnswerContent;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "VisitAndOrder")
public class Visit extends Order implements I_Visit {

    private static final long serialVersionUID = 7248716614988497636L;

    public static final String COLUMNNAME_IS_VISIT = "isVisit";
    public static final String COLUMNNAME_VISIT_STATUS = "visitStatus";
    public static final String COLUMNNAME_CLOSED = "closed";

    public static final String COLUMNNAME_IS_FEEDBACK = "isFeedback";
    public static final String COLUMNNAME_FEEDBACKS = "feedbacks";
    public static final String COLUMNNAME_IS_FEEDBACKS_READED = "feedbacksReaded";

    public static final String COLUMNNAME_SURVEY_ANSWERS = "surveyAnswers";

    public static final String COLUMNNAME_SALESMAN_ID = COLUMNNAME_CREATED_BY_ID;

    public static final int VISIT_STATUS_VISITING = 0;
    public static final int VISIT_STATUS_VISITED = 1;

    public static final int LOCATION_STATUS_LOCATED = 0;
    public static final int LOCATION_STATUS_TOO_FAR = 1;
    public static final int LOCATION_STATUS_UNLOCATED = 2;
    public static final int LOCATION_STATUS_CUSTOMER_UNLOCATED = 3;

    public static final String COLUMNNAME_PHOTO = "photo";

    public Visit() {
        super();

        setVisit(true);
        setOrder(false);
    }

    public Visit(I_Visit visit) {
        super(visit);

        setVisit(true);
        setOrder(visit.isHasOrder());

        this.endTime = visit.getEndTime();
        this.visitStatus = visit.getVisitStatus();
        this.closed = visit.isClosed();
        this.photo = visit.getPhoto();
        this.location = visit.getLocation();
        this.customerLocation = visit.getCustomerLocation();
        this.locationStatus = visit.getLocationStatus();
        this.distance = visit.getDistance();
        this.duration = visit.getDuration();
        this.errorDuration = visit.isErrorDuration();
        this.isFeedback = visit.getFeedbacks() != null && !visit.getFeedbacks().isEmpty();
        this.feedbacksReaded = this.isFeedback ? visit.isReaded() : false;
        this.feedbacks = this.isFeedback ? visit.getFeedbacks() : null;

        if (visit.getSurveyAnswers() != null) {
            this.surveyAnswers = new ArrayList<>(visit.getSurveyAnswers().size());
            for (I_SurveyAnswerContent surveyAnswer : visit.getSurveyAnswers()) {
                this.surveyAnswers.add(new SurveyAnswerContent(surveyAnswer));
            }
        }
    }

    private SimpleDate endTime;

    private int visitStatus;
    private boolean closed;

    private String photo;

    private Location location;
    private Location customerLocation;
    private int locationStatus;
    private Double distance;

    private long duration;
    private boolean errorDuration;

    // FEEDBACK
    private boolean isFeedback;
    private boolean feedbacksReaded;
    private List<String> feedbacks;

    // SURVEY ANSWER
    private List<SurveyAnswerContent> surveyAnswers;

    public int getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(int visitStatus) {
        this.visitStatus = visitStatus;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getCustomerLocation() {
        return customerLocation;
    }

    public void setCustomerLocation(Location customerLocation) {
        this.customerLocation = customerLocation;
    }

    public int getLocationStatus() {
        return locationStatus;
    }

    public void setLocationStatus(int locationStatus) {
        this.locationStatus = locationStatus;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public boolean isFeedback() {
        return isFeedback;
    }

    public void setFeedback(boolean isFeedback) {
        this.isFeedback = isFeedback;
    }

    public boolean isFeedbacksReaded() {
        return feedbacksReaded;
    }

    public void setFeedbacksReaded(boolean feedbacksReaded) {
        this.feedbacksReaded = feedbacksReaded;
    }

    public List<String> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<String> feedbacks) {
        this.feedbacks = feedbacks;
        this.isFeedback = true;
    }

    public List<I_SurveyAnswerContent> getSurveyAnswers() {
        return PO.convertList(I_SurveyAnswerContent.class, surveyAnswers);
    }

    public void setSurveyAnswers(List<SurveyAnswerContent> surveyAnswers) {
        this.surveyAnswers = surveyAnswers;
    }

    /**
     * Increase method's visibility to public
     */
    @Override
    public SimpleDate getStartTime() {
        return super.getStartTime();
    }

    /**
     * Increase method's visibility to public
     */
    @Override
    public void setStartTime(SimpleDate startTime) {
        super.setStartTime(startTime);
    }

    public SimpleDate getEndTime() {
        return endTime;
    }

    public void setEndTime(SimpleDate endTime) {
        this.endTime = endTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isErrorDuration() {
        return errorDuration;
    }

    public void setErrorDuration(boolean errorDuration) {
        this.errorDuration = errorDuration;
    }

    @Override
    public void setDetails(List<OrderDetail> details) {
        setOrder(true);
        super.setDetails(details);
    }

    @Override
    public void setPromotions(List<OrderPromotion> promotionResults) {
        setOrder(true);
        super.setPromotions(promotionResults);
    }

    /**
     * Set Order's creation time
     * 
     * @deprecated For Visit, please use {@link #setStartTime(SimpleDate)} and
     *             {@link #setEndTime(SimpleDate)}
     */
    @Override
    public void setCreatedTime(SimpleDate createdTime) {
        throw new UnsupportedOperationException("use startTime and endTime");
    }

    @Override
    public UserEmbed getSalesman() {
        return getCreatedBy();
    }

    public void setSalesman(UserEmbed salesman) {
        setCreatedBy(salesman);
    }

    @Override
    public boolean isHasOrder() {
        return isOrder();
    }

    @Override
    public boolean isReaded() {
        return isFeedbacksReaded();
    }

    @Override
    public String getFirstMessage() {
        return getFeedbacks() == null || getFeedbacks().isEmpty() ? null : getFeedbacks().get(0);
    }

}
