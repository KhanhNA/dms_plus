package com.viettel.persistence.mongo.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.viettel.repository.common.domain.data.I_VisitPhoto;
import com.viettel.repository.common.entity.SimpleDate;

@Document(collection = "VisitAndOrder")
public class VisitPhoto extends A_VisitAndOrder implements I_VisitPhoto {

    private static final long serialVersionUID = -3282388189852006751L;

    private boolean closed;
    private String photo;

    public boolean isClosed() {
        return closed;
    }

    public String getPhoto() {
        return photo;
    }
    
    public SimpleDate getCreatedTime() {
        return super.getStartTime();
    }

}
