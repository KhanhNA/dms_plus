package com.viettel.persistence.mongo.domain.embed;

import com.viettel.repository.common.domain.customer.I_CustomerEmbed;

public class CustomerEmbed extends CategoryEmbed implements I_CustomerEmbed {

    private static final long serialVersionUID = -3158993502257758137L;

    private CategoryEmbed area;
    private CategoryEmbed customerType;

    public CustomerEmbed() {
        super();
    }

    public CustomerEmbed(I_CustomerEmbed customer) {
        super(customer);

        this.area = customer.getArea() == null ? null : new CategoryEmbed(customer.getArea());
        this.customerType = customer.getCustomerType() == null ? null : new CategoryEmbed(customer.getCustomerType());
    }
    
    public CategoryEmbed getArea() {
        return area;
    }

    public void setArea(CategoryEmbed area) {
        this.area = area;
    }

    public CategoryEmbed getCustomerType() {
        return customerType;
    }

    public void setCustomerType(CategoryEmbed customerType) {
        this.customerType = customerType;
    }

}
