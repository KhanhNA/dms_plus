package com.viettel.persistence.mongo.domain.embed;

import java.math.BigDecimal;
import java.util.Map;

import org.bson.types.ObjectId;

import com.viettel.persistence.mongo.domain.Product;
import com.viettel.repository.common.domain.data.I_OrderProduct;

public class OrderProduct extends ProductEmbed implements I_OrderProduct {

    private static final long serialVersionUID = -5848974644014161066L;

    private BigDecimal price;
    private BigDecimal productivity;

    public OrderProduct() {
        super();
    }

    public OrderProduct(Product product, Map<ObjectId, BigDecimal> priceList) {
        super(product);

        BigDecimal price = null;
        if (priceList != null) {
            price = priceList.get(product.getId());
        }
        price = price == null ? product.getPrice() : price;
        this.price = price;
        
        this.productivity = product.getProductivity();
    }
    
    public OrderProduct(I_OrderProduct orderProduct) {
        super(orderProduct);
        
        this.price = orderProduct.getPrice();
        this.productivity = orderProduct.getProductivity();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public void setProductivity(BigDecimal productivity) {
        this.productivity = productivity;
    }

}
