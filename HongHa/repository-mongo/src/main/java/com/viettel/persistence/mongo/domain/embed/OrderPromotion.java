package com.viettel.persistence.mongo.domain.embed;

import java.util.ArrayList;
import java.util.List;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.domain.data.I_OrderPromotion;
import com.viettel.repository.common.domain.data.I_OrderPromotionDetail;

public class OrderPromotion extends CategoryEmbed implements I_OrderPromotion {

    private static final long serialVersionUID = 6844459029280345505L;
    
    private List<OrderPromotionDetail> details;
    
    public OrderPromotion() {
        super();
    }
    
    public OrderPromotion(I_OrderPromotion orderPromotion) {
        super(orderPromotion);
        
        if (orderPromotion.getDetails() != null) {
            this.details = new ArrayList<>(orderPromotion.getDetails().size());
            for (I_OrderPromotionDetail orderPromotionDetail : orderPromotion.getDetails()) {
                this.details.add(new OrderPromotionDetail(orderPromotionDetail));
            }
        }
    }

    @Override
    public List<I_OrderPromotionDetail> getDetails() {
        return PO.convertList(I_OrderPromotionDetail.class, details);
    }

    public void setDetails(List<OrderPromotionDetail> details) {
        this.details = details;
    }

}
