package com.viettel.persistence.mongo.domain.embed;

import com.viettel.repository.common.domain.data.I_OrderPromotionDetail;

public class OrderPromotionDetail implements I_OrderPromotionDetail {

    private static final long serialVersionUID = 6620807032420284594L;

    private int seqNo;
    private String conditionProductName;
    private OrderPromotionReward reward;

    public OrderPromotionDetail() {
        super();
    }

    public OrderPromotionDetail(I_OrderPromotionDetail orderPromotionDetail) {
        super();

        this.seqNo = orderPromotionDetail.getSeqNo();
        this.conditionProductName = orderPromotionDetail.getConditionProductName();
        this.reward = orderPromotionDetail.getReward() == null ? null
                : new OrderPromotionReward(orderPromotionDetail.getReward());
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    @Override
    public OrderPromotionReward getReward() {
        return reward;
    }

    public void setReward(OrderPromotionReward reward) {
        this.reward = reward;
    }

    public String getConditionProductName() {
        return conditionProductName;
    }

    public void setConditionProductName(String conditionProductName) {
        this.conditionProductName = conditionProductName;
    }

}
