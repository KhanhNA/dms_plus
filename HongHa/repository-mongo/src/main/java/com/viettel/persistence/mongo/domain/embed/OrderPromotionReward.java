package com.viettel.persistence.mongo.domain.embed;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.data.I_OrderPromotionReward;

public class OrderPromotionReward implements I_OrderPromotionReward {

    private static final long serialVersionUID = -8108962349175417341L;

    private BigDecimal quantity;
    private ProductEmbed product;
    private String productText;
    private BigDecimal amount;

    public OrderPromotionReward() {
        super();
    }

    public OrderPromotionReward(I_OrderPromotionReward orderPromotionReward) {
        super();

        this.quantity = orderPromotionReward.getQuantity();
        this.product = orderPromotionReward.getProduct() == null ? null
                : new ProductEmbed(orderPromotionReward.getProduct());
        this.productText = orderPromotionReward.getProductText();
        this.amount = orderPromotionReward.getAmount();

    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public ProductEmbed getProduct() {
        return product;
    }

    public void setProduct(ProductEmbed product) {
        this.product = product;
    }

    public String getProductText() {
        return productText;
    }

    public void setProductText(String productText) {
        this.productText = productText;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
