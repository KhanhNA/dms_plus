package com.viettel.persistence.mongo.domain.embed;

import java.io.Serializable;

import org.bson.types.ObjectId;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.domain.common.I_POEmbed;

public class POEmbed implements Serializable, I_POEmbed {
    
    private static final long serialVersionUID = -6744009390560348781L;
    
    public static final String COLUMNNAME_ID = "id";
    
    private ObjectId id;
    
    public POEmbed() {
        this.id = null;
    }
    
    public POEmbed(I_POEmbed po) {
        Assert.notNull(po);
        this.id = PO.convertId(po.getId());
    }

    public String getId() {
        return PO.convertId(this.id);
    }
    
    public ObjectId _getId() {
        return id;
    }
    
    public void setId(ObjectId id) {
        this.id = id;
    }
    
}
