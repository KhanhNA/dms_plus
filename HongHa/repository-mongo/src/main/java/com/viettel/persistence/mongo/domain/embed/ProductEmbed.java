package com.viettel.persistence.mongo.domain.embed;

import com.viettel.repository.common.domain.product.I_ProductEmbed;

public class ProductEmbed extends CategoryEmbed implements I_ProductEmbed {

    private static final long serialVersionUID = -3188876010666458486L;

    private CategoryEmbed productCategory;
    private CategoryEmbed uom;

    public ProductEmbed() {
        super();
    }

    public ProductEmbed(I_ProductEmbed pe) {
        super(pe);
        
        this.productCategory = pe.getProductCategory() == null ? null : new CategoryEmbed(pe.getProductCategory());
        this.uom = pe.getUom() == null ? null : new CategoryEmbed(pe.getUom());
    }
    
    public CategoryEmbed getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(CategoryEmbed productCategory) {
        this.productCategory = productCategory;
    }

    public CategoryEmbed getUom() {
        return uom;
    }

    public void setUom(CategoryEmbed uom) {
        this.uom = uom;
    }

}
