package com.viettel.persistence.mongo.domain.embed;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.product.I_ProductQuantity;

public class ProductQuantity extends ProductEmbed implements I_ProductQuantity {

    private static final long serialVersionUID = 6620807032420284594L;

    private BigDecimal quantity;

    public ProductQuantity() {
        super();
    }
    
    public ProductQuantity(I_ProductQuantity productQuantity) {
        super(productQuantity);
        
        this.quantity = productQuantity.getQuantity();
    }
    
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

}
