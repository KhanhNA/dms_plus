package com.viettel.persistence.mongo.domain.embed;

import java.math.BigDecimal;

import org.bson.types.ObjectId;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.domain.promotion.I_PromotionCondition;

public class PromotionCondition implements I_PromotionCondition {

    private static final long serialVersionUID = 3830540425679484882L;
    
    private ObjectId productId;
    private BigDecimal quantity;
    
    public PromotionCondition() {
        super();
    }
    
    public PromotionCondition(I_PromotionCondition promotionCondition) {
        super();
        
        this.productId = PO.convertId(promotionCondition.getProductId());
        this.quantity = promotionCondition.getQuantity();
    }

    public String getProductId() {
        return PO.convertId(productId);
    }
    
    public void setProductId(ObjectId productId) {
        this.productId = productId;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

}
