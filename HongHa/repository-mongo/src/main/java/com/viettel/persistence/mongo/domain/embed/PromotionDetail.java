package com.viettel.persistence.mongo.domain.embed;

import com.viettel.repository.common.domain.promotion.I_PromotionDetail;

public class PromotionDetail implements I_PromotionDetail {

    private static final long serialVersionUID = -4712090697721809249L;

    private int seqNo;
    private int type;
    private PromotionCondition condition;
    private PromotionReward reward;

    public PromotionDetail() {
        super();
    }

    public PromotionDetail(I_PromotionDetail promotionDetail) {
        super();

        this.seqNo = promotionDetail.getSeqNo();
        this.type = promotionDetail.getType();
        this.condition = promotionDetail.getCondition() == null ? null
                : new PromotionCondition(promotionDetail.getCondition());
        this.reward = promotionDetail.getReward() == null ? null : new PromotionReward(promotionDetail.getReward());
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public PromotionCondition getCondition() {
        return condition;
    }

    public void setCondition(PromotionCondition condition) {
        this.condition = condition;
    }

    public PromotionReward getReward() {
        return reward;
    }

    public void setReward(PromotionReward reward) {
        this.reward = reward;
    }

}
