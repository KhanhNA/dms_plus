package com.viettel.persistence.mongo.domain.embed;

import java.math.BigDecimal;

import org.bson.types.ObjectId;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.domain.promotion.I_PromotionReward;

public class PromotionReward implements I_PromotionReward {

    private static final long serialVersionUID = -8477427361033661699L;

    private BigDecimal percentage;
    private BigDecimal quantity;
    private ObjectId productId;
    private String productText;

    public PromotionReward() {
        super();
    }

    public PromotionReward(I_PromotionReward promotionReward) {
        super();

        this.percentage = promotionReward.getPercentage();
        this.quantity = promotionReward.getQuantity();
        this.productId = PO.convertId(promotionReward.getProductId());
        this.productText = promotionReward.getProductText();
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getProductId() {
        return PO.convertId(productId);
    }

    public void setProductId(ObjectId productId) {
        this.productId = productId;
    }

    public String getProductText() {
        return productText;
    }

    public void setProductText(String productText) {
        this.productText = productText;
    }

}
