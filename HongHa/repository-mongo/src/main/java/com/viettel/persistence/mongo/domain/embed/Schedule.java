package com.viettel.persistence.mongo.domain.embed;

import org.bson.types.ObjectId;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.domain.customer.I_Schedule;

public class Schedule implements I_Schedule {

    private static final long serialVersionUID = -7721585729963009430L;
    
    public static final String COLUMNNAME_ROUTE_ID = "routeId";
    public static final String COLUMNNAME_ITEM = "item";

    private ObjectId routeId;
    private ScheduleItem item;

    public Schedule() {
        super();
    }
    
    public Schedule(I_Schedule schedule) {
        super();
        
        this.routeId = PO.convertId(schedule.getRouteId());
        this.item = schedule.getItem() == null ? null : new ScheduleItem(schedule.getItem());
    }
    
    public String getRouteId() {
        return PO.convertId(routeId);
    }

    public void setRouteId(ObjectId routeId) {
        this.routeId = routeId;
    }

    public ScheduleItem getItem() {
        return item;
    }

    public void setItem(ScheduleItem item) {
        this.item = item;
    }

}
