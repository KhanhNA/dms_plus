package com.viettel.persistence.mongo.domain.embed;

import java.util.Set;

import org.springframework.data.annotation.Transient;

import com.viettel.persistence.mongo.domain.Visit;
import com.viettel.repository.common.domain.data.I_SurveyAnswerContent;

public class SurveyAnswerContent implements I_SurveyAnswerContent {

    private static final long serialVersionUID = 3227127774937212395L;

    public static final String COLUMNNAME_SURVEY_ID = "survey._id";
    public static final String COLUMNNAME_OPTIONS = "options";

    private CategoryEmbed survey;
    private Set<Integer> options;

    public SurveyAnswerContent() {
        super();
    }

    public SurveyAnswerContent(I_SurveyAnswerContent surveyAnswerContent) {
        super();

        this.survey = surveyAnswerContent.getSurvey() == null ? null
                : new CategoryEmbed(surveyAnswerContent.getSurvey());
        this.options = surveyAnswerContent.getOptions();
    }

    @Transient
    private Visit visit;

    public CategoryEmbed getSurvey() {
        return survey;
    }

    public void setSurvey(CategoryEmbed survey) {
        this.survey = survey;
    }

    public Set<Integer> getOptions() {
        return options;
    }

    public void setOptions(Set<Integer> options) {
        this.options = options;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }

}
