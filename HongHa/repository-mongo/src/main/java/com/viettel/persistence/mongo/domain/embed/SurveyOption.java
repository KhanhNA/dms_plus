package com.viettel.persistence.mongo.domain.embed;

import com.viettel.repository.common.domain.survey.I_SurveyOption;

public class SurveyOption implements I_SurveyOption {
    
    private static final long serialVersionUID = 3227127774937212395L;

    private int seqNo;
    private String name;
    
    public SurveyOption() {
        super();
    }
    
    public SurveyOption(I_SurveyOption surveyOption) {
        super();
        
        this.seqNo = surveyOption.getSeqNo();
        this.name = surveyOption.getName();
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
