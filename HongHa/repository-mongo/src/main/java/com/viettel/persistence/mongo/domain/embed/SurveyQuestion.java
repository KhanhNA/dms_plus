package com.viettel.persistence.mongo.domain.embed;

import java.util.ArrayList;
import java.util.List;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.domain.survey.I_SurveyOption;
import com.viettel.repository.common.domain.survey.I_SurveyQuestion;

public class SurveyQuestion implements I_SurveyQuestion {

    private static final long serialVersionUID = 3227127774937212395L;

    private int seqNo;
    private String name;
    private boolean multipleChoice;
    private boolean required;
    private List<SurveyOption> options;
    
    public SurveyQuestion() {
        super();
    }
    
    public SurveyQuestion(I_SurveyQuestion surveyQuestion) {
        super();
        
        this.seqNo = surveyQuestion.getSeqNo();
        this.name = surveyQuestion.getName();
        this.multipleChoice = surveyQuestion.isMultipleChoice();
        this.required = surveyQuestion.isRequired();

        if (surveyQuestion.getOptions() != null) {
            this.options = new ArrayList<>(surveyQuestion.getOptions().size());
            for (I_SurveyOption surveyOption : surveyQuestion.getOptions()) {
                this.options.add(new SurveyOption(surveyOption));
            }
        }
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMultipleChoice() {
        return multipleChoice;
    }

    public void setMultipleChoice(boolean multipleChoice) {
        this.multipleChoice = multipleChoice;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<I_SurveyOption> getOptions() {
        return PO.convertList(I_SurveyOption.class, options);
    }

    public void setOptions(List<SurveyOption> options) {
        this.options = options;
    }

}
