package com.viettel.persistence.mongo.domain.entity;

import java.io.InputStream;

import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.FileMetadata;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.domain.file.I_File;

public class File extends FileMetadata implements I_File {

    private static final long serialVersionUID = -7672615606528455236L;

    public File(FileMetadata fileMetadata, InputStream inputStream) {
        Assert.notNull(fileMetadata);
        Assert.notNull(inputStream);
        Assert.notNull(fileMetadata.getId());
        Assert.notNull(fileMetadata.getClientId());
        Assert.notNull(fileMetadata.getFileName());
        Assert.notNull(fileMetadata.getContentType());
        
        this.setId(PO.convertId(fileMetadata.getId()));
        this.setClientId(PO.convertId(fileMetadata.getClientId()));
        this.setFileName(fileMetadata.getFileName());
        this.setContentType(fileMetadata.getContentType());
        this.setInputStream(inputStream);
    }
    
    private InputStream inputStream;

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

}