package com.viettel.persistence.mongo.domain.entity;

import java.math.BigDecimal;

import com.viettel.repository.common.domain.data.I_OrderSummary;

public class OrderSummary implements I_OrderSummary {

    private static final long serialVersionUID = 2570116283554046264L;

    private BigDecimal revenue;
    private BigDecimal subRevenue;
    private BigDecimal quantity;
    private BigDecimal productivity;
    private int nbSKU;
    private int nbOrder;
    private int nbOrderWithoutVisit;
    private int nbSalesman;
    private int nbCustomer;

    public OrderSummary() {
        this.revenue = BigDecimal.ZERO;
        this.productivity = BigDecimal.ZERO;
        this.nbSKU = 0;
        this.nbOrder = 0;
        this.nbOrderWithoutVisit = 0;
        this.nbSalesman = 0;
        this.nbCustomer = 0;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }
    
    public BigDecimal getRevenue() {
        return revenue;
    }
    
    public BigDecimal getSubRevenue() {
        return subRevenue;
    }

    public BigDecimal getProductivity() {
        return productivity;
    }

    public int getNbSKU() {
        return nbSKU;
    }

    public int getNbOrder() {
        return nbOrder;
    }

    public int getNbOrderWithoutVisit() {
        return nbOrderWithoutVisit;
    }

    public int getNbSalesman() {
        return nbSalesman;
    }

    public void setNbSalesman(int nbSalesman) {
        this.nbSalesman = nbSalesman;
    }

    public int getNbCustomer() {
        return nbCustomer;
    }

    public void setNbCustomer(int nbCustomer) {
        this.nbCustomer = nbCustomer;
    }

}
