package com.viettel.persistence.mongo.domain.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import org.bson.types.ObjectId;

public class ProductExchangeReturnQuantity implements Serializable {

	private static final long serialVersionUID = -920078846211803915L;

	private ExchangeReturnKey id;
	private ExchangeReturnValue value;

	public ExchangeReturnKey getId() {
		return id;
	}

	public ExchangeReturnValue getValue() {
		return value;
	}

	public static class ExchangeReturnKey implements Serializable {
		private static final long serialVersionUID = -6856031234942801874L;

		private ObjectId thisId;
		private ObjectId productId;

		public ObjectId getThisId() {
			return thisId;
		}

		public ObjectId getProductId() {
			return productId;
		}
	}

	public static class ExchangeReturnValue implements Serializable {

		private static final long serialVersionUID = -6856037723942801874L;

		private BigDecimal exchangeQuantity;
		private BigDecimal returnQuantity;
		private String customer;
		private String time;
		private String createdBy;
		private String distributor;

		public BigDecimal getExchangeQuantity() {
			return exchangeQuantity;
		}

		public BigDecimal getReturnQuantity() {
			return returnQuantity;
		}

		public String getCustomer() {
			return customer;
		}

		public String getTime() {
			return time;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public String getDistributor() {
			return distributor;
		}

	}

}
