package com.viettel.persistence.mongo.domain.entity;

import com.viettel.repository.common.domain.data.I_VisitSummary;

public class VisitSummary implements I_VisitSummary {

    private static final long serialVersionUID = -8065567259352213544L;

    private int nbVisit;
    private int nbVisitWithOrder;
    private int nbVisitErrorDuration;
    private int nbVisitErrorPosition;

    private int nbSalesman;
    private int nbCustomer;

    public VisitSummary() {
        this.nbVisit = 0;
        this.nbVisitWithOrder = 0;
        this.nbVisitErrorDuration = 0;
        this.nbVisitErrorPosition = 0;

        this.nbSalesman = 0;
        this.nbCustomer = 0;
    }

    public int getNbVisit() {
        return nbVisit;
    }

    public int getNbVisitWithOrder() {
        return nbVisitWithOrder;
    }

    public int getNbVisitErrorDuration() {
        return nbVisitErrorDuration;
    }

    public int getNbVisitErrorPosition() {
        return nbVisitErrorPosition;
    }

    public int getNbSalesman() {
        return nbSalesman;
    }

    public void setNbSalesman(int nbSalesman) {
        this.nbSalesman = nbSalesman;
    }

    public int getNbCustomer() {
        return nbCustomer;
    }

    public void setNbCustomer(int nbCustomer) {
        this.nbCustomer = nbCustomer;
    }

}