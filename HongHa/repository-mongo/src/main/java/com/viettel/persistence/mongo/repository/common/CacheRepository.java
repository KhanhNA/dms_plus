package com.viettel.persistence.mongo.repository.common;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.CalendarConfig;
import com.viettel.persistence.mongo.domain.Client;
import com.viettel.persistence.mongo.domain.Config;
import com.viettel.repository.common.cache.RedisCache;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.product.I_Product;

@Repository
public class CacheRepository {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String CACHE_CLIENT = "CACHE_CLIENT";
	private static final String CACHE_CONFIG = "CONFIG";
	private static final String CACHE_CALENDAR_CONFIG = "CALENDAR_CONFIG";
	private static final String CACHE_PRODUCT_MAP = "PRODUCT_MAP";
	private static final String CACHE_PRODUCT_ACTIVE_LIST = "PRODUCT_ACTIVE_LIST";
	private static final String CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR = "CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR";
	private static final String CACHE_PRODUCT_IMPORTANT_LIST = "CACHE_PRODUCT_IMPORTANT_LIST";

	@Autowired
	private RedisCache redisCache;

	public void clearAll() {
		redisCache.getValueCache(CACHE_CLIENT, String.class, Client.class).clear();
		redisCache.getValueCache(CACHE_CONFIG, String.class, Config.class).clear();
		redisCache.getValueCache(CACHE_PRODUCT_MAP, String.class, Map.class).clear();
		redisCache.getValueCache(CACHE_CALENDAR_CONFIG, String.class, CalendarConfig.class).clear();
		redisCache.getHashCache(CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR, String.class, String.class, I_Inventory.class)
				.clear();
	}

	public void clearByClient(String id) {
		Assert.notNull(id);

		redisCache.getValueCache(CACHE_CONFIG, String.class, Config.class).delete(id.toString());
		redisCache.getValueCache(CACHE_PRODUCT_MAP, String.class, Map.class).delete(id.toString());
		redisCache.getValueCache(CACHE_CALENDAR_CONFIG, String.class, CalendarConfig.class).delete(id.toString());
		redisCache.getHashCache(CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR, String.class, String.class, I_Inventory.class)
				.delete(id.toString());
	}

	public Client getClientCache(String id) {
		Assert.notNull(id);

		try {
			return redisCache.getValueCache(CACHE_CLIENT, String.class, Client.class).get(id.toString()).get();
		} catch (Exception e) {
			logger.error("Cannot get Client from cache", e);
			return null;
		}
	}

	public void setClientCache(String id, Client client) {
		Assert.notNull(id);
		Assert.notNull(client);

		try {
			redisCache.getValueCache(CACHE_CLIENT, String.class, Client.class).get(id.toString()).set(client);
		} catch (Exception e) {
			logger.error("Cannot set Client to cache", e);
		}
	}

	public void clearClientCache(String id) {
		Assert.notNull(id);

		try {
			redisCache.getValueCache(CACHE_CLIENT, String.class, Client.class).delete(id.toString());
		} catch (Exception e) {
			logger.error("Cannot set Client to cache", e);
		}
	}

	// CONFIG
	public Config getConfigCache(String clientId) {
		Assert.notNull(clientId);

		try {
			return redisCache.getValueCache(CACHE_CONFIG, String.class, Config.class).get(clientId).get();
		} catch (Exception e) {
			logger.error("Cannot get Config from cache", e);
			return null;
		}
	}

	public void setConfigCache(String clientId, Config config) {
		Assert.notNull(clientId);

		try {
			redisCache.getValueCache(CACHE_CONFIG, String.class, Config.class).get(clientId).set(config);
		} catch (Exception e) {
			logger.error("Cannot set Config to cache", e);
		}
	}

	// PRODUCT_MAP
	@SuppressWarnings("unchecked")
	public Map<String, I_Product> getProductMapCache(String clientId) {
		Assert.notNull(clientId);

		try {
			return redisCache.getValueCache(CACHE_PRODUCT_MAP, String.class, Map.class).get(clientId).get();
		} catch (Exception e) {
			logger.error("Cannot get productMap from cache", e);
			return null;
		}
	}

	public void setProductMapCache(String clientId, Map<String, I_Product> productMap) {
		Assert.notNull(clientId);

		try {
			redisCache.getValueCache(CACHE_PRODUCT_MAP, String.class, Map.class).get(clientId).set(productMap);
		} catch (Exception e) {
			logger.error("Cannot set productMap to cache", e);
		}
	}

	// PRODUCT_ACTIVE_MAP
	@SuppressWarnings("unchecked")
	public List<I_Product> getProductActiveListCache(String clientId) {
		Assert.notNull(clientId);

		try {
			return redisCache.getValueCache(CACHE_PRODUCT_ACTIVE_LIST, String.class, List.class).get(clientId).get();
		} catch (Exception e) {
			logger.error("Cannot get productList from cache", e);
			return null;
		}
	}

	public void setProductActiveListCache(String clientId, List<I_Product> products) {
		Assert.notNull(clientId);

		try {
			redisCache.getValueCache(CACHE_PRODUCT_ACTIVE_LIST, String.class, List.class).get(clientId).set(products);
		} catch (Exception e) {
			logger.error("Cannot set productList to cache", e);
		}
	}

	// PRODUCT_IMPORTANT
	@SuppressWarnings("unchecked")
	public List<I_Product> getProductImportantListCache(String clientId) {
		Assert.notNull(clientId);

		try {
			return redisCache.getValueCache(CACHE_PRODUCT_IMPORTANT_LIST, String.class, List.class).get(clientId).get();
		} catch (Exception e) {
			logger.error("Cannot get productList from cache", e);
			return null;
		}
	}

	public void setProductImportantListCache(String clientId, List<I_Product> products) {
		Assert.notNull(clientId);

		try {
			redisCache.getValueCache(CACHE_PRODUCT_IMPORTANT_LIST, String.class, List.class).get(clientId).set(products);
		} catch (Exception e) {
			logger.error("Cannot set productList to cache", e);
		}
	}

	
	// CALENDAR CONFIG
	public CalendarConfig getCalendarConfigCache(String clientId) {
		Assert.notNull(clientId);

		try {
			return redisCache.getValueCache(CACHE_CALENDAR_CONFIG, String.class, CalendarConfig.class)
					.get(clientId.toString()).get();
		} catch (Exception e) {
			logger.error("Cannot get CalendarConfig from cache", e);
			return null;
		}
	}

	public void setCalendarConfigCache(String clientId, CalendarConfig calendarConfig) {
		Assert.notNull(clientId);

		try {
			redisCache.getValueCache(CACHE_CALENDAR_CONFIG, String.class, CalendarConfig.class).get(clientId.toString())
					.set(calendarConfig);
		} catch (Exception e) {
			logger.error("Cannot set CalendarConfig to cache", e);
		}
	}

	// LASTEST INVENTORY BY DISTRIBUTOR
	public I_Inventory getLatestInventory(String clientId, String distributorId) {
		Assert.notNull(clientId);
		Assert.notNull(distributorId);

		try {
			return redisCache
					.getHashCache(CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR, String.class, String.class, I_Inventory.class)
					.get(clientId.toString()).get(distributorId);
		} catch (Exception e) {
			logger.error("Cannot get CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR", e);
			return null;
		}
	}

	public Map<String, I_Inventory> getLatestInventorys(String clientId) {
		Assert.notNull(clientId);

		try {
			return redisCache
					.getHashCache(CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR, String.class, String.class, I_Inventory.class)
					.get(clientId.toString()).entries();
		} catch (Exception e) {
			logger.error("Cannot get CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR", e);
			return null;
		}
	}

	public void setLatestInventory(String clientId, String distributorId, I_Inventory inventory) {
		Assert.notNull(clientId);
		Assert.notNull(distributorId);
		Assert.notNull(inventory);

		try {
			redisCache
					.getHashCache(CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR, String.class, String.class, I_Inventory.class)
					.get(clientId.toString()).put(distributorId, inventory);
		} catch (Exception e) {
			logger.error("Cannot set CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR", e);
		}
	}

	public void clearLatestInventory(String clientId, String distributorId) {
		Assert.notNull(clientId);
		Assert.notNull(distributorId);

		try {
			redisCache
					.getHashCache(CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR, String.class, String.class, I_Inventory.class)
					.get(clientId.toString()).delete(distributorId);
		} catch (Exception e) {
			logger.error("Cannot get CACHE_LASTEST_INVENTORY_BY_DISTRIBUTOR", e);
		}
	}

}
