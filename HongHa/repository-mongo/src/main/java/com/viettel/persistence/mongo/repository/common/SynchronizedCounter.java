package com.viettel.persistence.mongo.repository.common;

public class SynchronizedCounter {

    private long c = 0;

    public synchronized void increment(long c) {
        this.c += c;
    }

    public synchronized void decrement(long c) {
        this.c -= c;;
    }

    public synchronized long value() {
        return c;
    }
    
}
