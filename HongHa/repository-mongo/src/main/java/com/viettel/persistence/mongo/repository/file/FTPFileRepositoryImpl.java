package com.viettel.persistence.mongo.repository.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.config.FTPProperties;
import com.viettel.persistence.mongo.domain.FileMetadata;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.entity.File;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.domain.file.I_File;
import com.viettel.repository.common.util.DateTimeUtils;

public class FTPFileRepositoryImpl implements FileRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private FileMetadataRepository fileMetadataRepository;
    
    @Autowired
    private FTPProperties ftpProperties;
    
    @Override
    public I_File getFileById(String clientId, String fileId) {
        FileMetadata fileMetadata = fileMetadataRepository.getById(clientId, fileId);

        if (fileMetadata == null) {
            return null;
        }

        return new File(fileMetadata, readFile(fileId));
    }

    @Override
    public String store(String clientId, InputStream inputStream, String fileName, String contentType) {
        ObjectId fileId = writeFile(inputStream);
        
        Assert.notNull(fileId);
        
        FileMetadata fileMetadata = new FileMetadata();
        fileMetadata.setId(fileId);
        fileMetadata.setClientId(PO.convertId(clientId));
        fileMetadata.setFileName(fileName);
        fileMetadata.setContentType(contentType);
        
        fileMetadataRepository.save(clientId, fileMetadata);
        
        return fileMetadata.getId();
    }

    @Override
    public void delete(String clientId, String fileId) {
        fileMetadataRepository.delete(clientId, fileId);
        deleteFile(Collections.singleton(fileId));
    }

    @Override
    public void delete(String clientId, Collection<String> fileIds) {
        fileMetadataRepository.delete(clientId, fileIds);
        deleteFile(fileIds);
    }

    @Override
    public void markAsUsed(String clientId, String fileId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void markAsUsed(String clientId, Collection<String> fileIds) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean exists(String clientId, String fileId) {
        return fileMetadataRepository.exists(clientId, fileId);
    }

    @Override
    public boolean existsAll(String clientId, Collection<String> fileIds) {
        return fileMetadataRepository.exists(clientId, fileIds);
    }
    
    // #S PRIVATE FUNCTION
    private InputStream readFile(String id) {
        String server = ftpProperties.getHost();
        int port = ftpProperties.getPort();
        String user = ftpProperties.getUsername();
        String pass = ftpProperties.getPassword();
        String preFolder = ftpProperties.getFolder();
 
        logger.error("XXX Start " + DateTimeUtils.getCurrentTime().format("HH:mm:ss"));
        
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            
            logger.error("XXX Login " + DateTimeUtils.getCurrentTime().format("HH:mm:ss"));
 
            java.io.File tempFile = java.io.File.createTempFile("ftp_" + System.currentTimeMillis(), "tmp");
            FileOutputStream tempFileOutputStream = new FileOutputStream(tempFile);
            
            String remoteFile = preFolder + id.toString();
            boolean success = ftpClient.retrieveFile(remoteFile, tempFileOutputStream);
            tempFileOutputStream.close();
            
            logger.error("XXX Write To Temp " + DateTimeUtils.getCurrentTime().format("HH:mm:ss"));
 
            if (!success) {
                logger.error("cannot get file from ftp");
                throw new UnsupportedOperationException();
            }
            
            logger.error("XXX End " + DateTimeUtils.getCurrentTime().format("HH:mm:ss"));
            
            return new FileInputStream(tempFile);
        } catch (IOException ex) {
            logger.error("cannot get file from ftp", ex);
            throw new UnsupportedOperationException();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                logger.debug("cannot disconected and logout", ex);
            }
        }
    }

    private ObjectId writeFile(InputStream inputStream) {
        ObjectId id = new ObjectId();
        
        String server = ftpProperties.getHost();
        int port = ftpProperties.getPort();
        String user = ftpProperties.getUsername();
        String pass = ftpProperties.getPassword();
        String preFolder = ftpProperties.getFolder();
 
        FTPClient ftpClient = new FTPClient();
        try {
 
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
 
            String remoteFile = preFolder + id.toString();
            boolean success = ftpClient.storeFile(remoteFile, inputStream);
            
            if (!success) {
                logger.error("cannot upload file to ftp");
                throw new UnsupportedOperationException();
            }
            
            return id;
        } catch (IOException ex) {
            logger.error("cannot get file from ftp", ex);
            throw new UnsupportedOperationException();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                logger.debug("cannot disconected and logout", ex);
            }
        }
    }
    
    private void deleteFile(Collection<String> ids) {
        if (ids  == null || ids.isEmpty()) {
            return;
        }
        
        String server = ftpProperties.getHost();
        int port = ftpProperties.getPort();
        String user = ftpProperties.getUsername();
        String pass = ftpProperties.getPassword();
        String preFolder = ftpProperties.getFolder();
 
        FTPClient ftpClient = new FTPClient();
        try {
 
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
 
            for (String id : ids) {
                String remoteFile = preFolder + id.toString();
                boolean success = ftpClient.deleteFile(remoteFile);
                
                if (!success) {
                    logger.error("cannot delete file to ftp");
                }
            }
        } catch (IOException ex) {
            logger.error("cannot get file from ftp", ex);
            throw new UnsupportedOperationException();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                logger.debug("cannot disconected and logout", ex);
            }
        }
    }

    // #E
    
}
