package com.viettel.persistence.mongo.repository.file;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.viettel.persistence.mongo.domain.FileMetadata;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.repository.impl.BasicRepositoryImpl;
import com.viettel.repository.common.POGetterRepository;
import com.viettel.repository.common.annotation.ClientRootInclude;

@ClientRootInclude
@Component
public class FileMetadataRepository extends BasicRepositoryImpl<FileMetadata, FileMetadata, FileMetadata>
        implements POGetterRepository<FileMetadata> {

    @Autowired
    private MongoTemplate mongoTemplate;
    
    @Override
    protected FileMetadata convertToDomain(FileMetadata write) {
        return write;
    }
    
    @Override
    public FileMetadata getById(String clientId, String id) {
        return mongoTemplate.findById(PO.convertId(id), FileMetadata.class);
    }
    
    public boolean delete(String clientId, Collection<String> ids) {
        return _delete(clientId, ids);
    }

    public boolean exists(String clientId, String id) {
        return exists(clientId, null, null, id);
    }

    public boolean exists(String clientId, Collection<String> ids) {
        return exists(clientId, null, null, ids);
    }

}
