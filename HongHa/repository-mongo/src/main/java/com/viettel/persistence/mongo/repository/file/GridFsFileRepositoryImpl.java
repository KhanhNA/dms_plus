package com.viettel.persistence.mongo.repository.file;

import java.io.InputStream;
import java.util.Collection;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;
import com.viettel.persistence.mongo.domain.FileMetadata;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.entity.File;
import com.viettel.repository.common.FileRepository;

@Repository
public class GridFsFileRepositoryImpl implements FileRepository {

    @Autowired
    private GridFsTemplate gridFsTemplate;

    @Autowired
    private FileMetadataRepository fileMetadataRepository;

    @Override
    public File getFileById(String clientId, String fileId) {
        FileMetadata fileMetadata = fileMetadataRepository.getById(clientId, fileId);

        if (fileMetadata == null) {
            return null;
        }

        GridFSDBFile dbFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(PO.convertId(fileId))));
        Assert.notNull(dbFile);

        return new File(fileMetadata, dbFile.getInputStream());
    }

    @Override
    public String store(String clientId, InputStream inputStream, String fileName, String contentType) {
        GridFSFile file = gridFsTemplate.store(inputStream, fileName, contentType, null);
        
        Assert.notNull(file);
        
        FileMetadata fileMetadata = new FileMetadata();
        fileMetadata.setId((ObjectId) file.getId());
        fileMetadata.setClientId(PO.convertId(clientId));
        fileMetadata.setFileName(fileName);
        fileMetadata.setContentType(contentType);
        
        fileMetadataRepository.save(clientId, fileMetadata);
        
        return fileMetadata.getId();
    }

    @Override
    public void delete(String clientId, String fileId) {
        fileMetadataRepository.delete(clientId, fileId);
        gridFsTemplate.delete(new Query(Criteria.where("_id").is(PO.convertId(fileId))));
    }

    @Override
    public void delete(String clientId, Collection<String> fileIds) {
        fileMetadataRepository.delete(clientId, fileIds);
        gridFsTemplate.delete(new Query(Criteria.where("_id").in(PO.convertIds(fileIds))));
    }

    @Override
    public void markAsUsed(String clientId, String fileId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void markAsUsed(String clientId, Collection<String> fileIds) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean exists(String clientId, String fileId) {
        return fileMetadataRepository.exists(clientId, fileId);
    }

    @Override
    public boolean existsAll(String clientId, Collection<String> fileIds) {
        return fileMetadataRepository.exists(clientId, fileIds);
    }

}
