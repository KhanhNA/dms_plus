package com.viettel.persistence.mongo.repository.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.Collections;

import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.config.SharedFolderProperties;
import com.viettel.persistence.mongo.domain.FileMetadata;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.entity.File;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.domain.file.I_File;

public class SharedFolderFileRepositoryImpl implements FileRepository {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private FileMetadataRepository fileMetadataRepository;
    
    @Autowired
    private SharedFolderProperties sharedFolderProperties;
    
    @Override
    public I_File getFileById(String clientId, String fileId) {
        FileMetadata fileMetadata = fileMetadataRepository.getById(clientId, fileId);

        if (fileMetadata == null) {
            return null;
        }

        return new File(fileMetadata, readFile(fileId));
    }

    @Override
    public String store(String clientId, InputStream inputStream, String fileName, String contentType) {
        ObjectId fileId = writeFile(inputStream);
        
        Assert.notNull(fileId);
        
        FileMetadata fileMetadata = new FileMetadata();
        fileMetadata.setId((ObjectId) fileId);
        fileMetadata.setClientId(PO.convertId(clientId));
        fileMetadata.setFileName(fileName);
        fileMetadata.setContentType(contentType);
        
        fileMetadataRepository.save(clientId, fileMetadata);
        
        return fileMetadata.getId();
    }

    @Override
    public void delete(String clientId, String fileId) {
        fileMetadataRepository.delete(clientId, fileId);
        deleteFile(Collections.singleton(fileId));
    }

    @Override
    public void delete(String clientId, Collection<String> fileIds) {
        fileMetadataRepository.delete(clientId, fileIds);
        deleteFile(fileIds);
    }

    @Override
    public void markAsUsed(String clientId, String fileId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void markAsUsed(String clientId, Collection<String> fileIds) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean exists(String clientId, String fileId) {
        return fileMetadataRepository.exists(clientId, fileId);
    }

    @Override
    public boolean existsAll(String clientId, Collection<String> fileIds) {
        return fileMetadataRepository.exists(clientId, fileIds);
    }
    
    // PRIVATE
    private InputStream readFile(String id) {
        try {
            String fileName = sharedFolderProperties.getPath() + id.toString();
            
            FileInputStream fileInputStream = new FileInputStream(fileName);
            
            return fileInputStream;
        } catch (Exception ex) {
            logger.error("cannot read file: " + id.toString(), ex);
            throw new UnsupportedOperationException();
        }
    }

    private ObjectId writeFile(InputStream inputStream) {
        try {
            ObjectId id = new ObjectId();
            
            String fileName = sharedFolderProperties.getPath() + id.toString();
            
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            
            IOUtils.copy(inputStream, fileOutputStream);
            
            inputStream.close();
            fileOutputStream.close();
            
            return id;
        } catch (Exception ex) {
            logger.error("cannot write file ", ex);
            throw new UnsupportedOperationException();
        }
        
    }
    
    private void deleteFile(Collection<String> ids) {
        if (ids  == null || ids.isEmpty()) {
            return;
        }
        
        for (String id : ids) {
            String fileName = sharedFolderProperties.getPath() + id.toString();
            java.io.File file = new java.io.File(fileName);
            try {
                file.delete();
            } catch (Exception ex) {
                logger.error("cannot delete file: " + id.toString(), ex);
            }
        }
    }

}
