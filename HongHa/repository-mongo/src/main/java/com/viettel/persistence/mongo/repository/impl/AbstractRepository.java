package com.viettel.persistence.mongo.repository.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.Assert;

import com.mongodb.WriteResult;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.annotation.ClientRootInclude;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.util.DateTimeUtils;

public abstract class AbstractRepository<D extends PO> {

	@Autowired
	private MongoTemplate dataTemplate;

	private Class<D> domainClazz;

	@SuppressWarnings("unchecked")
	public AbstractRepository() {
		Class<?> superClazz = getClass();
		Type superType = superClazz.getGenericSuperclass();
		while (!(superType instanceof ParameterizedType)) {
			superClazz = superClazz.getSuperclass();
			superType = superClazz.getGenericSuperclass();
		}

		int paraIndex = 0;
		ParameterizedType genericSuperclass = (ParameterizedType) superType;
		this.domainClazz = (Class<D>) genericSuperclass.getActualTypeArguments()[paraIndex++];
	}

	public Class<D> getDomainClazz() {
		return domainClazz;
	}

	protected final D _getById(String clientId, Boolean draft, Boolean active, String id) {
		Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(PO.convertId(id));
		Query mainQuery = initQuery(clientId, draft, active, criteria, null, null);
		return dataTemplate.findOne(mainQuery, domainClazz);
	}

	protected final List<D> _getListByIds(String clientId, Boolean draft, Boolean active, Collection<String> ids) {
		Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(PO.convertIds(ids));
		return _getList(clientId, draft, active, criteria, null, null);
	}

	protected final D _getFirst(String clientId, Boolean draft, Boolean active, Criteria criteria, Sort sort) {
		Query mainQuery = initQuery(clientId, draft, active, criteria, null, sort);
		return dataTemplate.findOne(mainQuery, domainClazz);
	}

	protected final List<D> _getList(String clientId, Boolean draft, Boolean active, Criteria criteria,
			Pageable pageable, Sort sort) {
		Query mainQuery = initQuery(clientId, draft, active, criteria, pageable, sort);
		return dataTemplate.find(mainQuery, domainClazz);
	}

	protected final Long _count(String clientId, Boolean draft, Boolean active, Criteria criteria) {
		Query mainQuery = initQuery(clientId, draft, active, criteria, null, null);
		return dataTemplate.count(mainQuery, domainClazz);
	}

	protected final boolean _exists(String clientId, Boolean draft, Boolean active, String id) {
		Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(PO.convertId(id));
		return this._exists(clientId, draft, active, criteria);
	}

	protected final boolean _exists(String clientId, Boolean draft, Boolean active, Criteria criteria) {
		Query mainQuery = initQuery(clientId, draft, active, criteria, null, null);
		return dataTemplate.exists(mainQuery, domainClazz);
	}

	protected final D _save(String clientId, D domain) {
		Assert.notNull(domain);
		Assert.notNull(domain.getClientId());
		Assert.notNull(clientId);
		if (!domain.getClientId().equals(clientId)) {
			throw new IllegalArgumentException("client id not valid");
		}
		// Hieunt47
		domain.setLastModifiedTime(DateTimeUtils.getCurrentTime());
		dataTemplate.save(domain);

		onChange(clientId);

		return domain;
	}

	protected final boolean _delete(String clientId, String id) {
		Assert.notNull(id);

		Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).is(PO.convertId(id));
		Query query = initQuery(clientId, true, null, criteria, null, null);
		WriteResult result = dataTemplate.remove(query, domainClazz);

		onChange(clientId);

		return result.getN() == 1;
	}

	protected final boolean _delete(String clientId, Collection<String> ids) {
		Assert.notEmpty(ids);

		Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(PO.convertIds(ids));
		Query query = initQuery(clientId, true, null, criteria, null, null);
		WriteResult result = dataTemplate.remove(query, domainClazz);

		onChange(clientId);

		return result.getN() == ids.size();
	}

	protected final void _insertBatch(String clientId, Collection<D> domains) {
		Assert.notEmpty(domains);

		// CHECK
		for (D domain : domains) {
			Assert.notNull(domain);
			Assert.notNull(domain.getClientId());
			Assert.notNull(clientId);
			if (!domain.getClientId().equals(clientId)) {
				throw new IllegalArgumentException("client id not valid");
			}
			// Hieunt47
			domain.setLastModifiedTime(DateTimeUtils.getCurrentTime());
		}

		onChange(clientId);

		dataTemplate.insert(domains, domainClazz);
	}

	protected final int _updateMulti(String clientId, Boolean draft, Boolean active, Collection<String> ids,
			Update update) {
		Assert.notEmpty(ids);
		// Hieunt47
		update.set(PO.COLUMNNAME_LAST_MODIFIED_TIME, DateTimeUtils.getCurrentTime());
		Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(PO.convertIds(ids));
		onChange(clientId);
		return this._updateMulti(clientId, draft, active, criteria, update);
	}

	protected final int _updateMulti(String clientId, Boolean draft, Boolean active, Criteria criteria, Update update) {
		// Hieunt47
		update.set(PO.COLUMNNAME_LAST_MODIFIED_TIME, DateTimeUtils.getCurrentTime());
		Query mainQuery = initQuery(clientId, draft, active, criteria, null, null);
		WriteResult result = dataTemplate.updateMulti(mainQuery, update, domainClazz);
		onChange(clientId);
		return result.getN();
	}

	protected Criteria getDefaultCriteria() {
		return null;
	}

	protected void onChange(String clientId) {
		// DO NOTHING
	}

	/************************************************************************/

	/**************************** PRIVATE METHODS ***************************/
	/************************************************************************/
	protected final Query initQuery(String clientId, Boolean draft, Boolean active, Criteria criteria,
			Pageable pageable, Sort sort) {
		Assert.notNull(clientId);

		Criteria clientFilterCriteria;
		if (isIncludeClientRoot()) {
			clientFilterCriteria = CriteriaUtils.orOperator(
					Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId)),
					Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.ROOT_ID));
		} else {
			clientFilterCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
		}

		Criteria activeCriteria = null;
		if (active != null) {
			activeCriteria = Criteria.where(PO.COLUMNNAME_ACTIVE).is(active);
		}

		Criteria draftCriteria = null;
		if (draft != null) {
			draftCriteria = Criteria.where(PO.COLUMNNAME_DRAFT).is(draft);
		}

		Criteria mainCriteria = CriteriaUtils.andOperator(clientFilterCriteria, activeCriteria, draftCriteria,
				getDefaultCriteria(), criteria);

		Query mainQuery = new Query();
		mainQuery.addCriteria(mainCriteria);

		if (pageable != null) {
			if (sort != null) {
				pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
			}
			mainQuery.with(pageable);
		} else if (sort != null) {
			mainQuery.with(sort);
		}

		return mainQuery;
	}

	private boolean isIncludeClientRoot() {
		return (this.getClass().isAnnotationPresent(ClientRootInclude.class));
	}

	public Pageable getPageable(PageSizeRequest pageSizeRequest) {
		if (pageSizeRequest == null) {
			return null;
		}

		return new PageRequest(pageSizeRequest.getPageNumber(), pageSizeRequest.getPageSize());
	}

}
