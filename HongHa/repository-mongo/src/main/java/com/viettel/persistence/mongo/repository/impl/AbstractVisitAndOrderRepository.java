package com.viettel.persistence.mongo.repository.impl;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.A_VisitAndOrder;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

public class AbstractVisitAndOrderRepository<D extends A_VisitAndOrder> extends AbstractRepository<D> {

    protected final List<D> _getList(String clientId, String distributorId, Period period, Criteria p_criteria,
            Pageable pageable, boolean sortAsc) {
        Assert.notNull(distributorId);

        Criteria criteriaRequired = getCriteriaRequired(distributorId, period);
        Criteria criteria = CriteriaUtils.andOperator(criteriaRequired, p_criteria);

        return super._getList(clientId, null, null, criteria, pageable, getSort(sortAsc));
    }

    protected final Long _count(String clientId, String distributorId, Period period, Criteria p_criteria) {
        Assert.notNull(distributorId);

        Criteria criteriaRequired = getCriteriaRequired(distributorId, period);
        Criteria criteria = CriteriaUtils.andOperator(criteriaRequired, p_criteria);

        return super._count(clientId, null, null, criteria);
    }

    protected final boolean _exists(String clientId, String distributorId, Period period, Criteria p_criteria) {
        Assert.notNull(distributorId);

        Criteria criteriaRequired = getCriteriaRequired(distributorId, period);
        Criteria criteria = CriteriaUtils.andOperator(criteriaRequired, p_criteria);

        return super._exists(clientId, null, null, criteria);
    }

    // PRIVATE
    private Period getDefaultPeriod() {
        SimpleDate threeMonthAgo = DateTimeUtils.addMonths(DateTimeUtils.getToday(), -3);
        return new Period(threeMonthAgo, null);
    }

    private Criteria getCriteriaRequired(String distributorId, Period period) {
        period = period == null ? getDefaultPeriod() : period;

        Criteria distributorCriteria = Criteria.where(A_VisitAndOrder.COLUMNNAME_DISTRIBUTOR_ID)
                .is(PO.convertId(distributorId));
        Criteria startTimeCriteria = CriteriaUtils.getPeriodCriteria(A_VisitAndOrder.COLUMNNAME_START_TIME_VALUE,
                period);

        return CriteriaUtils.andOperator(distributorCriteria, startTimeCriteria);
    }

    private Sort getSort(boolean sortAsc) {
        if (sortAsc) {
            return new Sort(Direction.ASC, A_VisitAndOrder.COLUMNNAME_START_TIME_VALUE);
        } else {
            return new Sort(Direction.DESC, A_VisitAndOrder.COLUMNNAME_START_TIME_VALUE);
        }
    }

}
