package com.viettel.persistence.mongo.repository.impl;

import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.Area;
import com.viettel.repository.common.AreaRepository;
import com.viettel.repository.common.domain.common.I_Category;

@Repository
public class AreaRepositoryImpl extends CategoryRepositoryImpl<Area, I_Category> implements AreaRepository {

    @Override
    protected Area convertToDomain(I_Category write) {
        return new Area(write);
    }

}
