package com.viettel.persistence.mongo.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.POBasicRepository;
import com.viettel.repository.common.POGetterRepository;
import com.viettel.repository.common.domain.common.I_PO;

public abstract class BasicRepositoryImpl<D extends PO, I_DETAIL extends I_PO, I_WRITE extends I_PO>
        extends AbstractRepository<D> implements POBasicRepository<I_DETAIL, I_WRITE>, POGetterRepository<I_DETAIL> {

    protected abstract D convertToDomain(I_WRITE write);

    @SuppressWarnings("unchecked")
    protected final I_DETAIL convertFromDomain(D detail) {
        return (I_DETAIL) detail;
    }

    @SuppressWarnings("unchecked")
    protected final List<I_DETAIL> convertFromDomains(List<D> domains) {
        if (domains == null) {
            return null;
        }
        
        if (domains.isEmpty()) {
            return Collections.emptyList();
        }

        List<I_DETAIL> details = new ArrayList<>(domains.size());
        for (D domain : domains) {
            details.add((I_DETAIL) domain);
        }
        
        return details;
    }

    // DRAFT = FALSE & ACTIVE = TRUE
    @Override
    public I_DETAIL getById(String clientId, String id) {
        return convertFromDomain(_getById(clientId, false, true, id));
    }

    @Override
    public List<I_DETAIL> getListByIds(String clientId, Collection<String> ids) {
        return getListByIds(clientId, false, true, ids);
    }

    // ALL
    @Override
    public I_DETAIL getById(String clientId, Boolean draft, Boolean active, String id) {
        return convertFromDomain(_getById(clientId, draft, active, id));
    }

    @Override
    public List<I_DETAIL> getListByIds(String clientId, Boolean draft, Boolean active, Collection<String> ids) {
        return convertFromDomains(_getListByIds(clientId, draft, active, ids));
    }

    @Override
    public boolean exists(String clientId, Boolean draft, Boolean active, String id) {
        return _exists(clientId, draft, active, id);
    }

    @Override
    public boolean exists(String clientId, Boolean draft, Boolean active, Collection<String> ids) {
        Criteria criteria = Criteria.where(PO.COLUMNNAME_ID).in(PO.convertIds(ids));
        return _exists(clientId, draft, active, criteria);
    }

    @Override
    public I_DETAIL save(String clientId, I_WRITE write) {
        return convertFromDomain(_save(clientId, convertToDomain(write)));
    }

    @Override
    public boolean delete(String clientId, String id) {
        return _delete(clientId, id);
    }

    @Override
    public I_DETAIL saveAndEnable(String clientId, I_WRITE write) {
        D domain = convertToDomain(write);
        domain.setDraft(false);
        return convertFromDomain(_save(clientId, domain));
    }

    @Override
    public boolean enable(String clientId, String id) {
        Update update = new Update();
        update.set(PO.COLUMNNAME_DRAFT, false);

        return super._updateMulti(clientId, null, null, Collections.singleton(id),
                update) == 1;
    }

    @Override
    public boolean deactivate(String clientId, String id) {
        Update update = new Update();
        update.set(PO.COLUMNNAME_ACTIVE, false);

        return super._updateMulti(clientId, null, null, Collections.singleton(id),
                update) == 1;
    }

    @Override
    public boolean activate(String clientId, String id) {
        Update update = new Update();
        update.set(PO.COLUMNNAME_ACTIVE, true);

        return super._updateMulti(clientId, null, null, Collections.singleton(id),
                update) == 1;
    }

    @Override
    public void insertBatch(String clientId, Collection<I_WRITE> writes) {
        if (writes != null && !writes.isEmpty()) {
            List<D> domains = new ArrayList<>(writes.size());
            for (I_WRITE write : writes) {
                domains.add(convertToDomain(write));
            }

            super._insertBatch(clientId, domains);
        }
    }

}
