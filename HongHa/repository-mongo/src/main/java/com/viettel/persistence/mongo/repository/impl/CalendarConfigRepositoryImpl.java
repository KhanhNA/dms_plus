package com.viettel.persistence.mongo.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.CalendarConfig;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.repository.common.CacheRepository;
import com.viettel.repository.common.CalendarConfigRepository;
import com.viettel.repository.common.domain.category.I_CalendarConfig;

@Repository
public class CalendarConfigRepositoryImpl extends AbstractRepository<CalendarConfig> implements
        CalendarConfigRepository {

    @Autowired
    private CacheRepository cacheRepository;

    @Override
    public I_CalendarConfig getCalendarConfig(String clientId) {
        CalendarConfig calendarConfig = cacheRepository.getCalendarConfigCache(clientId);
        if (calendarConfig == null) {
            calendarConfig = _getById(clientId, false, true, clientId);
            if (calendarConfig != null) {
                cacheRepository.setCalendarConfigCache(clientId, calendarConfig);
            }
        }
        return calendarConfig;
    }

    @Override
    public I_CalendarConfig save(String clientId, I_CalendarConfig domain) {
        Assert.notNull(domain);

        cacheRepository.setCalendarConfigCache(clientId, null);
        
        CalendarConfig calendarConfig = new CalendarConfig(domain);
        calendarConfig.setId(PO.convertId(clientId));
        calendarConfig.setClientId(PO.convertId(clientId));
        calendarConfig.setActive(true);
        calendarConfig.setDraft(false);
        
        return _save(clientId, calendarConfig);
    }

    

}
