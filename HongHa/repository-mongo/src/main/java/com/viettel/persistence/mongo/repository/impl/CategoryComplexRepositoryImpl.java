package com.viettel.persistence.mongo.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Category;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.CategoryComplexBasicRepository;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.util.StringUtils;

public abstract class CategoryComplexRepositoryImpl<D extends Category, I_HEADER extends I_Category, I_DETAIL extends I_HEADER>
        extends BasicRepositoryImpl<D, I_DETAIL, I_DETAIL>
        implements CategoryComplexBasicRepository<I_HEADER, I_DETAIL, I_DETAIL> {

    protected Sort getSort() {
        return new Sort(new Order(Direction.DESC, PO.COLUMNNAME_DRAFT), new Order(Category.COLUMNNAME_NAME));
    }

    @SuppressWarnings("unchecked")
    protected final List<I_HEADER> convertFromDomainsToHeaders(List<D> domains) {
        if (domains == null) {
            return null;
        }

        if (domains.isEmpty()) {
            return Collections.emptyList();
        }

        List<I_HEADER> details = new ArrayList<>(domains.size());
        for (D domain : domains) {
            details.add((I_HEADER) domain);
        }

        return details;
    }

    // DRAFT = FALSE & ACTIVE = TRUE
    @Override
    public List<I_HEADER> getAll(String clientId, String distributorId) {
        Criteria distributorCriteria = null;
        if (distributorId != null) {
            distributorCriteria = Criteria.where(Category.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        }

        return convertFromDomainsToHeaders(_getList(clientId, false, true, distributorCriteria, null, null));
    }

    // ALL
    @Override
    public List<I_HEADER> getList(String clientId, Boolean draft, Boolean active, Collection<String> distributorIds,
            String searchName, String searchCode, PageSizeRequest pageSizeRequest) {
        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            if (distributorIds.isEmpty()) {
                return Collections.emptyList();
            }
            distributorCriteria = Criteria.where(Category.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        }

        Criteria searchNameCriteria = CriteriaUtils.getSearchLikeCriteria(Category.COLUMNNAME_SEARCH_NAME, searchName);
        Criteria searchCodeCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCodeCriteria = Criteria.where(Category.COLUMNNAME_CODE).is(searchCode);
        }
        Criteria searchCriteria = CriteriaUtils.orOperator(searchNameCriteria, searchCodeCriteria);

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, searchCriteria);

        return convertFromDomainsToHeaders(
                _getList(clientId, draft, active, criteria, getPageable(pageSizeRequest), getSort()));
    }

    @Override
    public long count(String clientId, Boolean draft, Boolean active, Collection<String> distributorIds,
            String searchName, String searchCode) {
        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            if (distributorIds.isEmpty()) {
                return 0l;
            }
            distributorCriteria = Criteria.where(Category.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        }

        Criteria searchNameCriteria = CriteriaUtils.getSearchLikeCriteria(Category.COLUMNNAME_SEARCH_NAME, searchName);
        Criteria searchCodeCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCodeCriteria = Criteria.where(Category.COLUMNNAME_CODE).is(searchCode);
        }
        Criteria searchCriteria = CriteriaUtils.orOperator(searchNameCriteria, searchCodeCriteria);

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, searchCriteria);

        return _count(clientId, draft, active, criteria);
    }

    @Override
    public boolean checkNameExist(String clientId, String id, String name, String distributorId) {
        Criteria notMe = null;
        if (id != null) {
            notMe = Criteria.where(PO.COLUMNNAME_ID).ne(PO.convertId(id));
        }

        Assert.notNull(name);
        Criteria nameCriteria = CriteriaUtils.getSearchExactCriteria(Category.COLUMNNAME_NAME, name);

        Criteria distributorCriteria = null;
        if (distributorId != null) {
            distributorCriteria = Criteria.where(Category.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        }

        Criteria criteria = CriteriaUtils.andOperator(notMe, nameCriteria, distributorCriteria);

        return super._exists(clientId, null, null, criteria);
    }

    @Override
    public boolean checkCodeExist(String clientId, String id, String code, String distributorId) {
        Criteria notMe = null;
        if (id != null) {
            notMe = Criteria.where(PO.COLUMNNAME_ID).ne(PO.convertId(id));
        }

        Assert.notNull(code);
        Criteria codeCriteria = CriteriaUtils.getSearchExactCriteria(Category.COLUMNNAME_CODE, code);

        Criteria distributorCriteria = null;
        if (distributorId != null) {
            distributorCriteria = Criteria.where(Category.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        }

        Criteria criteria = CriteriaUtils.andOperator(notMe, codeCriteria, distributorCriteria);

        return super._exists(clientId, null, null, criteria);
    }

    @Override
    public boolean checkDistributorUsed(String clientId, String distributorId) {
        Assert.notNull(distributorId);

        Criteria criteria = Criteria.where(Category.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));

        return _checkUsed(clientId, criteria);
    }

    // PROTECTED
    protected boolean _checkUsed(String clientId, Criteria criteria) {
        return super._exists(clientId, true, null, criteria) || super._exists(clientId, false, true, criteria);
    }
    
}
