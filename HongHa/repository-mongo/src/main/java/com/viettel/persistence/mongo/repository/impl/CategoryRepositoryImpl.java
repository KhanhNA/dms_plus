package com.viettel.persistence.mongo.repository.impl;

import com.viettel.persistence.mongo.domain.Category;
import com.viettel.repository.common.CategoryBasicRepository;
import com.viettel.repository.common.domain.common.I_Category;

public abstract class CategoryRepositoryImpl<D extends Category, I extends I_Category>
        extends CategoryComplexRepositoryImpl<D, I, I> implements CategoryBasicRepository<I> {

}
