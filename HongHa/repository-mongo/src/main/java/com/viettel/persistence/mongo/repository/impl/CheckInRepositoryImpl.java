package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.CheckIn;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.CheckInRepository;
import com.viettel.repository.common.domain.checkin.I_CheckIn;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

@Repository
public class CheckInRepositoryImpl extends BasicRepositoryImpl<CheckIn, I_CheckIn, I_CheckIn>
        implements CheckInRepository {

    @Override
    protected CheckIn convertToDomain(I_CheckIn write) {
        return new CheckIn(write);
    }

    @Override
    public List<I_CheckIn> getCheckIns(String clientId, Collection<String> createdByIds, Period period,
            PageSizeRequest pageSizeRequest) {
        Criteria criteria = getCriteria(createdByIds, period);

        Sort sort = new Sort(Direction.DESC, CheckIn.COLUMNNAME_CREATED_TIME_VALUE);

        return convertFromDomains(_getList(clientId, false, true, criteria, getPageable(pageSizeRequest), sort));
    }

    @Override
    public long countCheckIns(String clientId, Collection<String> createdByIds, Period period) {
        Criteria criteria = getCriteria(createdByIds, period);

        return _count(clientId, false, true, criteria);
    }

    // PRIVATE
    private Criteria getCriteria(Collection<String> createdByIds, Period period) {
        Criteria createdCriteria = null;
        if (createdByIds != null) {
            createdCriteria = Criteria.where(CheckIn.COLUMNNAME_CREATED_BY_ID).in(PO.convertIds(createdByIds));
        }

        Criteria periodCriteria = null;
        if (period != null) {
            periodCriteria = CriteriaUtils.getPeriodCriteria(CheckIn.COLUMNNAME_CREATED_TIME_VALUE, period);
        }

        Criteria criteria = CriteriaUtils.andOperator(createdCriteria, periodCriteria);
        return criteria;
    }

}
