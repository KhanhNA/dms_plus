package com.viettel.persistence.mongo.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.Client;
import com.viettel.persistence.mongo.repository.common.CacheRepository;
import com.viettel.repository.common.ClientRepository;
import com.viettel.repository.common.domain.common.I_Category;

@Repository
public class ClientRepositoryImpl extends CategoryRepositoryImpl<Client, I_Category> implements ClientRepository {
	
	@Autowired
	private CacheRepository cacheRepository;

	@Override
	public I_Category getById(String clientId, String id) {
		Client client = cacheRepository.getClientCache(id);
		if (client == null) {
			client = _getById(clientId, false, true, id);
			if (client != null) {
				cacheRepository.setClientCache(id, client);
			}
		}
		return client;
	}
	
	@Override
	public I_Category save(String clientId, I_Category write) {
	    if (write.getId() != null) {
	        cacheRepository.clearClientCache(write.getId());
	    }
	    
	    return super.save(clientId, write);
	}

    @Override
    protected Client convertToDomain(I_Category write) {
        return new Client(write);
    }
	
}
