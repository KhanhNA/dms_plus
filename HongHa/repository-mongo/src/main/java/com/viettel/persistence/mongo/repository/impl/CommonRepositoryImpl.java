package com.viettel.persistence.mongo.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.repository.common.CacheRepository;
import com.viettel.repository.common.CommonRepository;

@Repository
public class CommonRepositoryImpl implements CommonRepository {

    @Autowired
    private CacheRepository cacheRepository;

    @Override
    public String getRootId() {
        return PO.ROOT_ID.toString();
    }

    @Override
    public void resetCache() {
        cacheRepository.clearAll();
    }

}
