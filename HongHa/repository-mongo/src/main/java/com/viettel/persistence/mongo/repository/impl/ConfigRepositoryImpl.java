package com.viettel.persistence.mongo.repository.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Config;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.repository.common.CacheRepository;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.FileRepository;
import com.viettel.repository.common.domain.category.I_ClientConfig;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.category.I_SystemConfig;

@Repository
public class ConfigRepositoryImpl extends AbstractRepository<Config> implements ConfigRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private CacheRepository cacheRepository;

    @Autowired
    private FileRepository fileRepository;

    @Override
    public I_Config getConfig(String clientId) {
        Config config = cacheRepository.getConfigCache(clientId);
        if (config == null) {
            config = _getById(clientId, false, true, clientId);

            if (clientId.equals(PO.ROOT_ID.toString())) {
                if (config.getProductPhoto() == null) {
                    try {
                        ClassPathResource classPathResource = new ClassPathResource("default-product-photo.png");
                        String productPhoto = fileRepository.store(clientId, classPathResource.getInputStream(),
                                "default-product-photo.png", "image/png");

                        config.setProductPhoto(productPhoto);

                        config = _save(PO.ROOT_ID.toString(), config);
                    } catch (IOException e) {
                        logger.error("store default photo error", e);
                        throw new UnsupportedOperationException(e);
                    }
                }
            }

            if (config != null) {
                cacheRepository.setConfigCache(clientId, config);
            }
        }

        if (!clientId.equals(PO.ROOT_ID.toString())) {
            I_Config rootConfig = getConfig(PO.ROOT_ID.toString());

            Config confiMerge = new Config();
            confiMerge.setId(PO.convertId(clientId));
            confiMerge.setClientId(PO.convertId(clientId));
            confiMerge.setActive(true);
            confiMerge.setDraft(false);
            
            if (config == null) {
                confiMerge.fillSystemConfigData(rootConfig);
            } else {
                confiMerge.fillSystemConfigData(rootConfig);
                confiMerge.fillClientConfigData(config);
            }

            return confiMerge;
        } else {
            return config;
        }
    }

    @Override
    public I_ClientConfig save(String clientId, I_ClientConfig clientConfig) {
        Assert.notNull(clientConfig);

        cacheRepository.setConfigCache(clientId, null);

        Config config = new Config(clientConfig);
        config.setId(PO.convertId(clientId));
        config.setClientId(PO.convertId(clientId));
        config.setActive(true);
        config.setDraft(false);

        return _save(clientId, config);
    }

    @Override
    public I_SystemConfig save(I_SystemConfig systemConfig) {
        Assert.notNull(systemConfig);

        cacheRepository.setConfigCache(PO.ROOT_ID.toString(), null);

        Config config = new Config(systemConfig);
        config.setId(PO.ROOT_ID);
        config.setClientId(PO.ROOT_ID);
        config.setActive(true);
        config.setDraft(false);

        return _save(PO.ROOT_ID.toString(), config);
    }

}
