package com.viettel.persistence.mongo.repository.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Category;
import com.viettel.persistence.mongo.domain.Customer;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.CustomerPendingRepository;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.StringUtils;

@Repository
public class CustomerPendingRepositoryImpl extends AbstractRepository<Customer> implements CustomerPendingRepository {

	private static final Criteria IGNORE_REJECTED = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS)
			.in(Arrays.asList(Customer.APPROVE_STATUS_PENDING, Customer.APPROVE_STATUS_APPROVED));

	@Override
	public boolean checkCustomerTypeUsed(String clientId, String customerTypeId) {
		Criteria customerTypeCriteria = Criteria.where(Customer.COLUMNNAME_CUSTOMER_TYPE_ID)
				.is(PO.convertId(customerTypeId));

		Criteria criteria = CriteriaUtils.andOperator(IGNORE_REJECTED, customerTypeCriteria);

		return super._exists(clientId, true, null, criteria) || super._exists(clientId, false, true, criteria);
	}

	@Override
	public boolean checkAreaUsed(String clientId, String areaId) {
		Criteria areaCriteria = Criteria.where(Customer.COLUMNNAME_AREA_ID).is(PO.convertId(areaId));

		Criteria criteria = CriteriaUtils.andOperator(IGNORE_REJECTED, areaCriteria);

		return super._exists(clientId, true, null, criteria) || super._exists(clientId, false, true, criteria);
	}

	@Override
	public boolean checkDistributorUsed(String clientId, String distributorId) {
		Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID)
				.is(PO.convertId(distributorId));

		Criteria criteria = CriteriaUtils.andOperator(IGNORE_REJECTED, distributorCriteria);

		return super._exists(clientId, true, null, criteria) || super._exists(clientId, false, true, criteria);
	}

	@Override
	public List<I_Customer> getCustomersByCreatedUsers(String clientId, String distributorId,
			Collection<String> userIds, Integer status, String searchName, String searchCode,
			PageSizeRequest pageSizeRequest) {
		Assert.notNull(userIds);

		if (userIds.isEmpty()) {
			return Collections.emptyList();
		}

		Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID)
				.is(PO.convertId(distributorId));
		Criteria createdByCriteria = Criteria.where(Customer.COLUMNNAME_CREATED_BY_ID).in(PO.convertIds(userIds));

		Criteria statusCriteria = null;
		if (status != null) {
			statusCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS).is(status);
		}

		Criteria searchNameCriteria = CriteriaUtils.getSearchLikeCriteria(Category.COLUMNNAME_SEARCH_NAME, searchName);
		Criteria searchCodeCriteria = null;
		if (!StringUtils.isEmpty(searchCode)) {
			searchCodeCriteria = Criteria.where(Category.COLUMNNAME_CODE).is(searchCode);
		}
		Criteria searchCriteria = CriteriaUtils.orOperator(searchNameCriteria, searchCodeCriteria);

		Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, createdByCriteria, statusCriteria,
				searchCriteria);

		return PO.convertList(I_Customer.class,
				super._getList(clientId, false, true, criteria, getPageable(pageSizeRequest), null));
	}

	@Override
	public long countCustomersByCreatedUsers(String clientId, String distributorId, Collection<String> userIds,
			Integer status, String searchName, String searchCode) {
		Assert.notNull(userIds);

		if (userIds.isEmpty()) {
			return 0l;
		}

		Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID)
				.is(PO.convertId(distributorId));
		Criteria createdByCriteria = Criteria.where(Customer.COLUMNNAME_CREATED_BY_ID).in(PO.convertIds(userIds));

		Criteria statusCriteria = null;
		if (status != null) {
			statusCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS).is(status);
		}

		Criteria searchNameCriteria = CriteriaUtils.getSearchLikeCriteria(Category.COLUMNNAME_SEARCH_NAME, searchName);
		Criteria searchCodeCriteria = null;
		if (!StringUtils.isEmpty(searchCode)) {
			searchCodeCriteria = Criteria.where(Category.COLUMNNAME_CODE).is(searchCode);
		}
		Criteria searchCriteria = CriteriaUtils.orOperator(searchNameCriteria, searchCodeCriteria);

		Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, createdByCriteria, statusCriteria,
				searchCriteria);

		return super._count(clientId, false, true, criteria);
	}

	@Override
	public List<I_Customer> getPendingCustomersByDistributors(String clientId, Collection<String> distributorIds,
			PageSizeRequest pageSizeRequest) {
		Assert.notNull(distributorIds);

		if (distributorIds.isEmpty()) {
			return Collections.emptyList();
		}

		Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID)
				.in(PO.convertIds(distributorIds));
		Criteria pendingCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS)
				.is(Customer.APPROVE_STATUS_PENDING);

		Sort sort = new Sort(Customer.COLUMNNAME_CREATED_TIME_VALUE);

		List<Customer> customers = super._getList(clientId, false, true,
				CriteriaUtils.andOperator(distributorCriteria, pendingCriteria), getPageable(pageSizeRequest), sort);

		return PO.convertList(I_Customer.class, customers);
	}

	@Override
	public long countPendingCustomersByDistributors(String clientId, Collection<String> distributorIds) {
		Assert.notNull(distributorIds);

		if (distributorIds.isEmpty()) {
			return 0;
		}

		Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID)
				.in(PO.convertIds(distributorIds));
		Criteria pendingCriteria = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS)
				.is(Customer.APPROVE_STATUS_PENDING);

		return super._count(clientId, false, true, CriteriaUtils.andOperator(distributorCriteria, pendingCriteria));
	}

	@Override
	public I_Customer getById(String clientId, String id) {
		return super._getById(clientId, false, true, id);
	}

	@Override
	public List<I_Customer> getListByIds(String clientId, Collection<String> ids) {
		List<Customer> list = _getListByIds(clientId, false, true, ids);
		return PO.convertList(I_Customer.class, list);
	}

	@Override
	public boolean approve(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime, String id) {
		Customer customer = super._getById(clientId, false, true, id);

		if (customer != null) {
			customer.setApprovedBy(new UserEmbed(approvedBy));
			customer.setApprovedTime(approvedTime);
			customer.setApproveStatus(Customer.APPROVE_STATUS_APPROVED);
			super._save(clientId, customer);

			return true;
		}

		return false;
	}

	@Override
	public boolean approveMulti(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime,
			Collection<String> ids) throws Exception {
		if (ids != null && !ids.isEmpty()) {
			Update update = new Update();
			update.set(Customer.COLUMNNAME_APPROVED_BY, new UserEmbed(approvedBy));
			update.set(Customer.COLUMNNAME_APPROVED_TIME, approvedTime);
			update.set(Customer.COLUMNNAME_APPROVE_STATUS, Customer.APPROVE_STATUS_APPROVED);
			super._updateMulti(clientId, false, true, ids, update);
			return true;
		}

		return false;
	}

	@Override
	public boolean reject(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime, String id) {
		Customer customer = super._getById(clientId, false, true, id);

		if (customer != null) {
			customer.setApprovedBy(new UserEmbed(approvedBy));
			customer.setApprovedTime(approvedTime);
			customer.setApproveStatus(Customer.APPROVE_STATUS_REJECTED);
			super._save(clientId, customer);

			return true;
		}

		return false;
	}

	@Override
	public boolean rejectMulti(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime,
			Collection<String> ids) {
		if (ids != null && !ids.isEmpty()) {
			Update update = new Update();
			update.set(Customer.COLUMNNAME_APPROVED_BY, new UserEmbed(approvedBy));
			update.set(Customer.COLUMNNAME_APPROVED_TIME, approvedTime);
			update.set(Customer.COLUMNNAME_APPROVE_STATUS, Customer.APPROVE_STATUS_REJECTED);

			super._updateMulti(clientId, false, true, ids, update);
		}

		return false;
	}

}
