package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Category;
import com.viettel.persistence.mongo.domain.Customer;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.embed.Schedule;
import com.viettel.persistence.mongo.domain.embed.ScheduleItem;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.CustomerPendingRepository;
import com.viettel.repository.common.CustomerRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.customer.I_Customer;
import com.viettel.repository.common.domain.customer.I_Schedule;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.StringUtils;

@Repository
public class CustomerRepositoryImpl extends CategoryRepositoryImpl<Customer, I_Customer> implements CustomerRepository {

    private static final Criteria DEFAULT_CRITERIA = Criteria.where(Customer.COLUMNNAME_APPROVE_STATUS)
            .is(Customer.APPROVE_STATUS_APPROVED);

    @Autowired
    private ConfigRepository configRepository;
    
    @Autowired
    private CustomerPendingRepository customerPendingRepository;
    
    @Override
    protected Criteria getDefaultCriteria() {
        return DEFAULT_CRITERIA;
    }

    @Override
    public boolean checkCustomerTypeUsed(String clientId, String customerTypeId) {
        return customerPendingRepository.checkCustomerTypeUsed(clientId, customerTypeId);
    }

    @Override
    public boolean checkAreaUsed(String clientId, String areaId) {
        return customerPendingRepository.checkAreaUsed(clientId, areaId);
    }
    
    @Override
    public boolean checkDistributorUsed(String clientId, String distributorId) {
        return customerPendingRepository.checkDistributorUsed(clientId, distributorId);
    }

    @Override
    public List<I_Customer> getList(String clientId, String distributorId, boolean searchByRoute, String routeId,
            Integer dayOfWeek, String searchName, String searchCode, PageSizeRequest pageSizeRequest) {
        Assert.notNull(distributorId);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));

        Criteria scheduleNullCriteria = null;
        Criteria routeCriteria = null;
        if (searchByRoute) {
            if (routeId == null) {
                scheduleNullCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE).is(null);
            } else {
                routeCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_ROUTE_ID).is(PO.convertId(routeId));
            }
        }

        Criteria dayOfWeekCriteria = null;
        if (scheduleNullCriteria == null && dayOfWeek != null) {
            dayOfWeekCriteria = Criteria
                    .where(Customer.COLUMNNAME_SCHEDULE_ITEM + "." + ScheduleItem.getDayColumnname(dayOfWeek)).is(true);
        }

        Criteria searchNameCriteria = CriteriaUtils.getSearchLikeCriteria(Category.COLUMNNAME_SEARCH_NAME, searchName);
        Criteria searchCodeCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCodeCriteria = Criteria.where(Category.COLUMNNAME_CODE).is(searchCode);
        }
        Criteria searchCriteria = CriteriaUtils.orOperator(searchNameCriteria, searchCodeCriteria);

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, scheduleNullCriteria, routeCriteria,
                dayOfWeekCriteria, searchCriteria);

        Sort sort = new Sort(Customer.COLUMNNAME_NAME);

        List<Customer> list = _getList(clientId, false, true, criteria, getPageable(pageSizeRequest), sort);

        return PO.convertList(I_Customer.class, list);
    }

    @Override
    public long count(String clientId, String distributorId, boolean searchByRoute, String routeId, Integer dayOfWeek,
            String searchName, String searchCode) {
        Assert.notNull(distributorId);
        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));

        Criteria scheduleNullCriteria = null;
        Criteria routeCriteria = null;
        if (searchByRoute) {
            if (routeId == null) {
                scheduleNullCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE).is(null);
            } else {
                routeCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_ROUTE_ID).is(PO.convertId(routeId));
            }
        }

        Criteria dayOfWeekCriteria = null;
        if (scheduleNullCriteria == null && dayOfWeek != null) {
            dayOfWeekCriteria = Criteria
                    .where(Customer.COLUMNNAME_SCHEDULE_ITEM + "." + ScheduleItem.getDayColumnname(dayOfWeek)).is(true);
        }

        Criteria searchNameCriteria = CriteriaUtils.getSearchLikeCriteria(Category.COLUMNNAME_SEARCH_NAME, searchName);
        Criteria searchCodeCriteria = null;
        if (!StringUtils.isEmpty(searchCode)) {
            searchCodeCriteria = Criteria.where(Category.COLUMNNAME_CODE).is(searchCode);
        }
        Criteria searchCriteria = CriteriaUtils.orOperator(searchNameCriteria, searchCodeCriteria);

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, scheduleNullCriteria, routeCriteria,
                dayOfWeekCriteria, searchCriteria);
        
        return super._count(clientId, false, true, criteria);
    }

    @Override
    public List<I_Customer> getCustomersByRoutes(String clientId, String distributorId, Collection<String> routeIds,
            SimpleDate date) {
        Assert.notNull(distributorId);
        Assert.notNull(routeIds);

        if (routeIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria routeCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_ROUTE_ID).in(PO.convertIds(routeIds));

        Criteria dateCriteria = null;
        if (date != null) {
            I_Config config = configRepository.getConfig(clientId);
            
            int dayOfToday = date.getDayOfWeek();
            Criteria weekCriteria = null;
            if (config.getNumberWeekOfFrequency() > 1) {
                weekCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_ITEM_WEEKS).all(
                        config.getWeekIndex(date));
            }

            dateCriteria = CriteriaUtils.andOperator(
                    Criteria.where(
                            Customer.COLUMNNAME_SCHEDULE_ITEM + "." + ScheduleItem.getDayColumnname(dayOfToday))
                            .is(true), weekCriteria);
        }

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, routeCriteria, dateCriteria);

        List<Customer> list = super._getList(clientId, false, true, criteria, null, null);
        
        return PO.convertList(I_Customer.class, list);
    }

    @Override
    public boolean checkCustomerInRoute(String clientId, String customerId, Collection<String> routeIds) {
        Assert.notNull(customerId);
        Assert.notNull(routeIds);

        if (routeIds.isEmpty()) {
            return false;
        }

        Criteria idCriteria = Criteria.where(Customer.COLUMNNAME_ID).is(PO.convertId(customerId));
        Criteria routeCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_ROUTE_ID).in(PO.convertIds(routeIds));

        return super._exists(clientId, false, true, CriteriaUtils.andOperator(idCriteria, routeCriteria));
    }

    @Override
    public boolean checkRouteUsed(String clientId, String distributorId, String routeId) {
        Assert.notNull(distributorId);
        Assert.notNull(routeId);

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria routeCriteria = Criteria.where(Customer.COLUMNNAME_SCHEDULE_ROUTE_ID).is(PO.convertId(routeId));

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, routeCriteria);
        
        return super._checkUsed(clientId, criteria);
    }

    @Override
    public I_Customer save(String clientId, I_Customer write) {
        Customer newCustomer = convertToDomain(write);
        
        if (write.getId() != null) {
            Customer oldCustomer = super._getById(clientId, false, true, write.getId());
            
            if (oldCustomer != null) {
                newCustomer.setSchedule(oldCustomer.getSchedule());
            }
        }
        
        return convertFromDomain(_save(clientId, newCustomer));
    }
    
    @Override
    public boolean updateSchedule(String clientId, String id, I_Schedule schedule) {
        Customer customer = super._getById(clientId, false, true, id);
        
        if (customer != null) {
            if (schedule == null) {
                customer.setSchedule(null);
            } else {
                customer.setSchedule(new Schedule(schedule));
            }
            super._save(clientId, customer);
            return true;
        }
        
        return false;
    }

    @Override
    public boolean updatePhone(String clientId, String id, String phone) {
        Customer customer = super._getById(clientId, false, true, id);
        
        if (customer != null) {
            customer.setPhone(phone);
            super._save(clientId, customer);
            return true;
        }
        
        return false;
    }

    @Override
    public boolean updateMobile(String clientId, String id, String mobile) {
        Customer customer = super._getById(clientId, false, true, id);
        
        if (customer != null) {
            customer.setMobile(mobile);
            super._save(clientId, customer);
            return true;
        }
        
        return false;
    }

    @Override
    public boolean updateLocation(String clientId, String id, Location location) {
        Customer customer = super._getById(clientId, false, true, id);
        
        if (customer != null) {
            customer.setLocation(location);
            super._save(clientId, customer);
            return true;
        }
        
        return false;
    }

    @Override
    protected Customer convertToDomain(I_Customer write) {
        return new Customer(write);
    }

}
