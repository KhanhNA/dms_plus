package com.viettel.persistence.mongo.repository.impl;

import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.CustomerType;
import com.viettel.repository.common.CustomerTypeRepository;
import com.viettel.repository.common.domain.common.I_Category;

@Repository
public class CustomerTypeRepositoryImpl extends CategoryRepositoryImpl<CustomerType, I_Category>
        implements CustomerTypeRepository {
            
    @Override
    protected CustomerType convertToDomain(I_Category write) {
        return new CustomerType(write);
    }

}
