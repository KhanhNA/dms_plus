package com.viettel.persistence.mongo.repository.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.DistributorPriceList;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.DistributorPriceListRepository;

@Repository
public class DistributorPriceListRepositoryImpl extends AbstractRepository<DistributorPriceList>
        implements DistributorPriceListRepository {

    @Override
    public Map<String, BigDecimal> getPriceList(String clientId, String distributorId) {
        Assert.notNull(distributorId);

        Criteria criteria = Criteria.where(DistributorPriceList.COLUMNNAME_DISTRIBUTOR_ID)
                .is(PO.convertId(distributorId));

        DistributorPriceList distritbutorPriceList = _getFirst(clientId, null, null, criteria, null);

        if (distritbutorPriceList == null || distritbutorPriceList.getPriceList() == null) {
            return Collections.emptyMap();
        }

        return distritbutorPriceList.getPriceList();
    }

    @Override
    public void savePriceList(String clientId, String distributorId, Map<String, BigDecimal> priceList) {
        Assert.notNull(distributorId);

        Criteria criteria = Criteria.where(DistributorPriceList.COLUMNNAME_DISTRIBUTOR_ID)
                .is(PO.convertId(distributorId));

        DistributorPriceList distritbutorPriceList = _getFirst(clientId, null, null, criteria, null);
        if (distritbutorPriceList == null) {
            distritbutorPriceList = new DistributorPriceList();
            distritbutorPriceList.setClientId(PO.convertId(clientId));
            distritbutorPriceList.setDraft(false);
            distritbutorPriceList.setActive(true);
            distritbutorPriceList.setDistributorId(PO.convertId(distributorId));
        }
        distritbutorPriceList.setPriceList(priceList);

        _save(clientId, distritbutorPriceList);
    }

}
