package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Distributor;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.domain.category.I_Distributor;

@Repository
public class DistributorRepositoryImpl extends CategoryRepositoryImpl<Distributor, I_Distributor>
        implements DistributorRepository {

    @Override
    protected Distributor convertToDomain(I_Distributor write) {
        return new Distributor(write);
    }

    @Override
    public List<I_Distributor> getDistributorsBySupervisors(String clientId, Collection<String> supervisorIds) {
        Assert.notNull(supervisorIds);

        if (supervisorIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria supervisorCriteria = Criteria.where(Distributor.COLUMNNAME_SUPERVISOR_ID)
                .in(PO.convertIds(supervisorIds));

        List<Distributor> distributors = super._getList(clientId, false, true, supervisorCriteria, null, null);

        return PO.convertList(I_Distributor.class, distributors);
    }

    // CHECK
    @Override
    public boolean checkSupervisorUsed(String clientId, String supervisorId) {
        Assert.notNull(supervisorId);

        Criteria criteria = Criteria.where(Distributor.COLUMNNAME_SUPERVISOR_ID).is(PO.convertId(supervisorId));

        return super._checkUsed(clientId, criteria);
    }

	@Override
	public List<I_Distributor> getDistributorNotAssignedToRegion(String clientId) {
		Assert.notNull(clientId);
		Criteria criteria = Criteria.where(Distributor.COLUMNNAME_REGION_ID).is(null);
		
		List<Distributor> distributors =  _getList(clientId, false, true, criteria, null, null);
		return PO.convertList(I_Distributor.class,distributors);
	}

}
