package com.viettel.persistence.mongo.repository.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.ExchangeReturn;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.entity.ProductExchangeReturnQuantity;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.ExchangeReturnRepository;
import com.viettel.repository.common.domain.data.I_ExchangeReturn;
import com.viettel.repository.common.domain.data.I_ExchangeReturnHeader;
import com.viettel.repository.common.domain.product.I_ProductExchangeReturnQuantity;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

@Repository
public class ExchangeReturnRepositoryImpl extends
		BasicRepositoryImpl<ExchangeReturn, I_ExchangeReturn, I_ExchangeReturn> implements ExchangeReturnRepository {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<I_ExchangeReturnHeader> getListByCreatedBys(String clientId, boolean exchange, String distributorId,
			Collection<String> createdByIds, Period period, PageSizeRequest pageSizeRequest) {
		Assert.notNull(distributorId);
		Assert.notNull(period);
		Assert.notNull(createdByIds);

		if (createdByIds.isEmpty()) {
			return Collections.emptyList();
		}

		Criteria exchangeCriteria = Criteria.where(ExchangeReturn.COLUMNNAME_EXCHANGE).is(exchange);
		Criteria distributorCriteria = Criteria.where(ExchangeReturn.COLUMNNAME_DISTRIBUTOR_ID)
				.is(PO.convertId(distributorId));
		Criteria createdByCriteria = Criteria.where(ExchangeReturn.COLUMNNAME_CREATED_BY_ID)
				.in(PO.convertIds(createdByIds));
		Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(ExchangeReturn.COLUMNNAME_CREATED_TIME_VALUE, period);

		Criteria criteria = CriteriaUtils.andOperator(exchangeCriteria, distributorCriteria, createdByCriteria,
				periodCriteria);

		Sort sort = new Sort(Direction.DESC, ExchangeReturn.COLUMNNAME_CREATED_TIME_VALUE);

		List<ExchangeReturn> list = _getList(clientId, false, true, criteria, getPageable(pageSizeRequest), sort);

		return PO.convertList(I_ExchangeReturnHeader.class, list);
	}

	@Override
	public long countByCreatedBys(String clientId, boolean exchange, String distributorId,
			Collection<String> createdByIds, Period period) {
		Assert.notNull(distributorId);
		Assert.notNull(period);
		Assert.notNull(createdByIds);

		if (createdByIds.isEmpty()) {
			return 0;
		}

		Criteria exchangeCriteria = Criteria.where(ExchangeReturn.COLUMNNAME_EXCHANGE).is(exchange);
		Criteria distributorCriteria = Criteria.where(ExchangeReturn.COLUMNNAME_DISTRIBUTOR_ID)
				.is(PO.convertId(distributorId));
		Criteria createdByCriteria = Criteria.where(ExchangeReturn.COLUMNNAME_CREATED_BY_ID)
				.in(PO.convertIds(createdByIds));
		Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(ExchangeReturn.COLUMNNAME_CREATED_TIME_VALUE, period);

		Criteria criteria = CriteriaUtils.andOperator(exchangeCriteria, distributorCriteria, createdByCriteria,
				periodCriteria);

		return _count(clientId, false, true, criteria);
	}

	@Override
	public List<I_ProductExchangeReturnQuantity> getProductExchangeReturnQuantities(String clientId,
			Collection<String> distributorIds, Period period) {
		Assert.notNull(distributorIds);
		Assert.notNull(period);

		if (distributorIds.isEmpty()) {
			return Collections.emptyList();
		}

		Criteria ditributorCriteria = Criteria.where(ExchangeReturn.COLUMNNAME_DISTRIBUTOR_ID)
				.in(PO.convertIds(distributorIds));
		Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(ExchangeReturn.COLUMNNAME_CREATED_TIME_VALUE, period);

		Criteria criteria = CriteriaUtils.andOperator(ditributorCriteria, periodCriteria);

		Query query = initQuery(clientId, false, true, criteria, null, null);

		StringBuilder mapFn = new StringBuilder();
		mapFn.append("function () { ");
		mapFn.append("  var thisId = this._id; ");
		mapFn.append("  var customer = ").append(" 	this.customer.code + ' - ' + this.customer.name; ");
		// + ' (' + this.customer.area.name + ')';");
		mapFn.append("  var time = this.createdTime.value; ");
		// .append(" this.createdTime.month + '/' + this.createdTime.date + '/'
		// + this.createdTime.year + ' ' ")
		// .append(" + this.createdTime.hour + ':' + this.createdTime.minute +
		// ':' + this.createdTime.second; ");
		mapFn.append("  var createdBy = this.createdBy.fullname; ");
		// + ' (' + this.createdBy.username + ') '; ");
		mapFn.append(" 	var distributor = this.distributor.name; ");
		mapFn.append("  for(var i = 0; i < this.details.length; i++) { ");
		mapFn.append("    var detail = this.details[i]; ");
		mapFn.append("    if (this.exchange) { ");
		mapFn.append("      emit( { thisId: thisId, productId: detail._id }, ").append(
				"				  { exchangeQuantity: detail.quantity, returnQuantity: '0', customer: customer, time: time, createdBy: createdBy, distributor: distributor } );");
		mapFn.append("    } else { ");
		mapFn.append("      emit( { thisId: thisId, productId: detail._id }, ").append(
				"				  { exchangeQuantity: '0', returnQuantity: detail.quantity, customer: customer, time: time, createdBy: createdBy, distributor: distributor } );");
		mapFn.append("    } ");
		mapFn.append("  } ");
		mapFn.append("} ");

		StringBuilder reduceFn = new StringBuilder();
		reduceFn.append("function(key, values) { ");
		reduceFn.append("  var result = ").append(
				"			   { exchangeQuantity: 0, returnQuantity: 0, customer: \"\", time: \"\" , createdBy: \"\", distributor: \"\" }; ");
		reduceFn.append("  for(var i = 0; i < values.length; i++) { ");
		reduceFn.append("    var value = values[i]; ");
		reduceFn.append("    result.exchangeQuantity += parseFloat(value.exchangeQuantity); ");
		reduceFn.append("    result.returnQuantity += parseFloat(value.returnQuantity); ");
		reduceFn.append("  } ");
		reduceFn.append("  result.exchangeQuantity = result.exchangeQuantity.toString(); ");
		reduceFn.append("  result.returnQuantity = result.returnQuantity.toString(); ");
		reduceFn.append("  result.customer = value.customer; ");
		reduceFn.append("  result.time = value.time.toString(); ");
		reduceFn.append("  result.createdBy = value.createdBy; ");
		reduceFn.append("  result.distributor = value.distributor; ");
		reduceFn.append("  return result; ");
		reduceFn.append("}; ");

		MapReduceResults<ProductExchangeReturnQuantity> mapReduceResults = mongoTemplate.mapReduce(query,
				"ExchangeReturn", mapFn.toString(), reduceFn.toString(), ProductExchangeReturnQuantity.class);

		List<I_ProductExchangeReturnQuantity> list = new ArrayList<>();
		for (Iterator<ProductExchangeReturnQuantity> iter = mapReduceResults.iterator(); iter.hasNext();) {
			final ProductExchangeReturnQuantity perq = iter.next();
			list.add(new I_ProductExchangeReturnQuantity() {

				private static final long serialVersionUID = 1L;

				@Override
				public String getThisId() {
					return perq.getId().getThisId().toString();
				}

				@Override
				public String getProductId() {
					return perq.getId().getProductId().toString();
				}

				@Override
				public BigDecimal getReturnQuantity() {
					return perq.getValue().getReturnQuantity();
				}

				@Override
				public BigDecimal getExchangeQuantity() {
					return perq.getValue().getExchangeQuantity();
				}

				@Override
				public String getCustomer() {
					return perq.getValue().getCustomer();
				}

				@Override
				public String getTime() {
					try {
						SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
						Date date = formatter.parse(perq.getValue().getTime());
						formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
						return formatter.format(date);
					} catch (Exception e) {
						return "";
					}
					// String fullDateTime = perq.getValue().getTime();
					// String year = fullDateTime.substring(0, 3);
					// String month = fullDateTime.substring(4, 5);
					// String date = fullDateTime.substring(6, 7);
					// String hour = fullDateTime.substring(8, 9);
					// String minute = fullDateTime.substring(10, 11);
					// return String.format("%1s/%2s/%3s %4s:%5s", month, date,
					// year, hour, minute);
				}

				@Override
				public String getCreatedBy() {
					return perq.getValue().getCreatedBy();
				}

				@Override
				public String getDistributor() {
					return perq.getValue().getDistributor();
				}
			});
		}

		return list;
	}

	@Override
	protected ExchangeReturn convertToDomain(I_ExchangeReturn write) {
		return new ExchangeReturn(write);
	}

}
