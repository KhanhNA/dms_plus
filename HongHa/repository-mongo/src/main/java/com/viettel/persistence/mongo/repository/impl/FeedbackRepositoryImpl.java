package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.A_VisitAndOrder;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Visit;
import com.viettel.persistence.mongo.repository.common.SynchronizedCounter;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.FeedbackRepository;
import com.viettel.repository.common.domain.data.I_Feedback;
import com.viettel.repository.common.domain.data.I_FeedbackHeader;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

@Repository
public class FeedbackRepositoryImpl extends AbstractRepository<Visit> implements FeedbackRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isFeedback = Criteria.where(Visit.COLUMNNAME_IS_FEEDBACK).is(true);
        return isFeedback;
    }

    @Override
    public List<I_FeedbackHeader> getFeedbackByDistributors(String clientId, Collection<String> distributorIds,
            Boolean readed, PageSizeRequest pageSizeRequest) {
        Assert.notNull(distributorIds);
        if (distributorIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria distributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID)
                .in(PO.convertIds(distributorIds));

        Criteria readStatusCriteria = null;
        if (readed != null) {
            readStatusCriteria = Criteria.where(Visit.COLUMNNAME_IS_FEEDBACKS_READED).is(readed);
        }

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, readStatusCriteria);

        Sort sort = new Sort(Direction.DESC, A_VisitAndOrder.COLUMNNAME_START_TIME_VALUE);
        
        List<Visit> list = super._getList(clientId, null, null, criteria, getPageable(pageSizeRequest), sort);

        return PO.convertList(I_FeedbackHeader.class, list);
    }

    @Override
    public long countFeedbackByDistributors(String clientId, Collection<String> distributorIds, Boolean readed) {
        Assert.notNull(distributorIds);
        if (distributorIds.isEmpty()) {
            return 0l;
        }

        ExecutorService es = Executors.newCachedThreadPool();
        final SynchronizedCounter synchronizedCounter = new SynchronizedCounter();

        for (final String distributorId : distributorIds) {
            es.execute(new Runnable() {

                @Override
                public void run() {
                    Criteria distributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID)
                            .is(PO.convertId(distributorId));
                    Criteria readStatusCriteria = null;
                    if (readed != null) {
                        readStatusCriteria = Criteria.where(Visit.COLUMNNAME_IS_FEEDBACKS_READED).is(readed);
                    }

                    Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, readStatusCriteria);

                    synchronizedCounter.increment(FeedbackRepositoryImpl.this._count(clientId, null, null, criteria));
                }
            });

        }

        try {
            es.shutdown();
            if (es.awaitTermination(30, TimeUnit.MINUTES)) {
                return synchronizedCounter.value();
            } else {
                throw new UnsupportedOperationException();
            }
        } catch (InterruptedException e) {
            logger.error("InterruptedException", e);
            Thread.currentThread().interrupt();
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public List<I_Feedback> getFeedbackByDistributors(String clientId, Collection<String> distributorIds, Period period,
            PageSizeRequest pageSizeRequest) {
        Assert.notNull(distributorIds);
        Assert.notNull(period);
        if (distributorIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria distributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID)
                .in(PO.convertIds(distributorIds));

        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_START_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, periodCriteria);

        List<Visit> list = super._getList(clientId, null, null, criteria, getPageable(pageSizeRequest),
                new Sort(Visit.COLUMNNAME_START_TIME_VALUE));

        return PO.convertList(I_Feedback.class, list);
    }

    @Override
    public long countFeedbackByDistributors(String clientId, Collection<String> distributorIds, Period period) {
        Assert.notNull(distributorIds);
        Assert.notNull(period);
        if (distributorIds.isEmpty()) {
            return 0;
        }

        Criteria distributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID)
                .in(PO.convertIds(distributorIds));

        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_START_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, periodCriteria);

        return _count(clientId, null, null, criteria);
    }

    @Override
    public List<I_Feedback> getLastFeedbackByCustomer(String clientId, String distributorId, String customerId,
            int size) {
        Assert.notNull(distributorId);
        Assert.notNull(customerId);

        Criteria distributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, customerCriteria);

        Pageable pageable = new PageRequest(0, 10);

        List<Visit> list = super._getList(clientId, null, null, criteria, pageable, null);

        return PO.convertList(I_Feedback.class, list);
    }

    @Override
    public void markAsRead(String clientId, String id) {
        Update update = new Update();
        update.set(Visit.COLUMNNAME_IS_FEEDBACKS_READED, true);

        super._updateMulti(clientId, false, true, Collections.singleton(id), update);
    }

    @Override
    public I_Feedback getById(String clientId, String id) {
        return super._getById(clientId, null, null, id);
    }

    @Override
    public List<I_Feedback> getListByIds(String clientId, Collection<String> ids) {
        List<Visit> list = super._getListByIds(clientId, null, null, ids);
        return PO.convertList(I_Feedback.class, list);
    }

}
