package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Inventory;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.repository.common.CacheRepository;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.ConfigRepository;
import com.viettel.repository.common.InventoryRepository;
import com.viettel.repository.common.domain.category.I_Config;
import com.viettel.repository.common.domain.inventory.I_Inventory;
import com.viettel.repository.common.domain.inventory.I_InventoryHeader;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

@Repository
public class InventoryRepositoryImpl extends BasicRepositoryImpl<Inventory, I_Inventory, I_Inventory>
        implements InventoryRepository {

    private static final Sort DEFAULT_SORT = new Sort(new Sort.Order(Direction.DESC, Inventory.COLUMNNAME_DRAFT),
            new Sort.Order(Direction.DESC, Inventory.COLUMNNAME_TIME_VALUE));

    @Autowired
    private CacheRepository cacheRepository;

    @Autowired
    private ConfigRepository configRepository;

    @Override
    protected Inventory convertToDomain(I_Inventory write) {
        return new Inventory(write);
    }

    @Override
    public List<I_InventoryHeader> getList(String clientId, String distributorId, PageSizeRequest pageSizeRequest) {
        Assert.notNull(distributorId);

        Criteria criteria = Criteria.where(Inventory.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));

        List<Inventory> inventories = _getList(clientId, null, true, criteria, getPageable(pageSizeRequest),
                DEFAULT_SORT);

        return PO.convertList(I_InventoryHeader.class, inventories);
    }

    @Override
    public long count(String clientId, String distributorId) {
        Assert.notNull(distributorId);

        Criteria criteria = Criteria.where(Inventory.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));

        return _count(clientId, null, true, criteria);
    }

    @Override
    public I_Inventory getLatestInventory(String clientId, String distributorId) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        // Try to get from cache, Redis will perform expiration time check
        I_Inventory inventory = cacheRepository.getLatestInventory(clientId, distributorId);

        // Get config
        I_Config config = configRepository.getConfig(clientId);
        Assert.notNull(config);

        // Check if inventory expired
        SimpleDate minInventoryTime = DateTimeUtils.addDays(DateTimeUtils.getToday(),
                -(config.getNumberDayInventoryExpire() - 1));
        if (inventory != null) {
            // Return null inventory expired (do not fetch from DB, this's
            // always newest record)
            if (minInventoryTime.compareTo(inventory.getTime()) > 0) {
                return null;
            }
            return inventory;
        }

        // Get latest inventory record from DB
        Criteria criteriaDistributor = Criteria.where(Inventory.COLUMNNAME_DISTRIBUTOR_ID)
                .is(PO.convertId(distributorId));
        // Only select record not expired
        Criteria criteriaTime = Criteria.where(Inventory.COLUMNNAME_TIME_VALUE).gte(minInventoryTime.getValue());

        Criteria criteria = CriteriaUtils.andOperator(criteriaDistributor, criteriaTime);
        inventory = _getFirst(clientId, false, true, criteria, DEFAULT_SORT);

        // Put back to cache
        if (inventory != null) {
            cacheRepository.setLatestInventory(clientId, distributorId, inventory);
        }

        return inventory;
    }

    @Override
    public Map<String, I_Inventory> getLatestInventorys(String clientId, Collection<String> distributorIds) {
        Assert.notNull(clientId);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<String, I_Inventory> lastestInventoryByDistributorFromCache = cacheRepository.getLatestInventorys(clientId);

        // Get config
        I_Config config = configRepository.getConfig(clientId);
        Assert.notNull(config);

        // Check if inventory expired
        SimpleDate minInventoryTime = DateTimeUtils.addDays(DateTimeUtils.getToday(),
                -(config.getNumberDayInventoryExpire() - 1));

        Map<String, I_Inventory> map = new HashMap<>(distributorIds.size());
        for (String distributorId : distributorIds) {
            I_Inventory inventory = lastestInventoryByDistributorFromCache.get(distributorId);
            if (inventory != null) {
                // If record expired, set it to null
                if (minInventoryTime.compareTo(inventory.getTime()) > 0) {
                    inventory = null;
                }
            } else {
                inventory = getLatestInventory(clientId, distributorId);
            }

            if (inventory != null) {
                map.put(distributorId, inventory);
            }
        }

        return map;
    }

    @Override
    public boolean enable(String clientId, String id) {
        Inventory inventory = _getById(clientId, null, true, id);
        if (inventory != null && inventory.getDistributor() != null && inventory.getDistributor().getId() != null) {
            cacheRepository.clearLatestInventory(clientId, inventory.getDistributor().getId());
        }
        return super.enable(clientId, id);
    }
    
    @Override
    public I_Inventory saveAndEnable(String clientId, I_Inventory write) {
        if (write != null && write.getDistributor() != null && write.getDistributor().getId() != null) {
            cacheRepository.clearLatestInventory(clientId, write.getDistributor().getId());
        }
        return super.saveAndEnable(clientId, write);
    }
    
}
