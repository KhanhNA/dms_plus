package com.viettel.persistence.mongo.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Area;
import com.viettel.persistence.mongo.domain.Customer;
import com.viettel.persistence.mongo.domain.CustomerType;
import com.viettel.persistence.mongo.domain.Distributor;
import com.viettel.persistence.mongo.domain.DistributorPriceList;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Product;
import com.viettel.persistence.mongo.domain.ProductCategory;
import com.viettel.persistence.mongo.domain.Promotion;
import com.viettel.persistence.mongo.domain.Route;
import com.viettel.persistence.mongo.domain.Survey;
import com.viettel.persistence.mongo.domain.UOM;
import com.viettel.persistence.mongo.domain.User;
import com.viettel.persistence.mongo.domain.Visit;
import com.viettel.persistence.mongo.repository.common.CacheRepository;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.MasterDataRepository;

@Repository
public class MasterDataRepositoryImpl implements MasterDataRepository {

    @Autowired
    private MongoTemplate dataTemplate;
    
    @Autowired
    private CacheRepository cacheRepository;
    
    @Override
    public void deleteAllDatas(String clientId) {
        Assert.notNull(clientId);
        
        Query userQuery = new Query();
        userQuery.addCriteria(CriteriaUtils.andOperator(Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId)),
                Criteria.where(User.COLUMNNAME_DEFAULT_ADMIN).ne(true)));
        dataTemplate.remove(userQuery, User.class);

        Query clientQuery = new Query();
        clientQuery.addCriteria(Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId)));
        dataTemplate.remove(clientQuery, Distributor.class);
        dataTemplate.remove(clientQuery, UOM.class);
        dataTemplate.remove(clientQuery, ProductCategory.class);
        dataTemplate.remove(clientQuery, Product.class);
        dataTemplate.remove(clientQuery, CustomerType.class);
        dataTemplate.remove(clientQuery, Area.class);
        dataTemplate.remove(clientQuery, Route.class);
        dataTemplate.remove(clientQuery, Customer.class);
        dataTemplate.remove(clientQuery, Promotion.class);
        dataTemplate.remove(clientQuery, Survey.class);
        dataTemplate.remove(clientQuery, DistributorPriceList.class);
        dataTemplate.remove(clientQuery, Visit.class);
        
        cacheRepository.clearByClient(clientId);
    }
    
    @Override
    public void deleteDatasWithoutCategory(String clientId) {
        Assert.notNull(clientId);

        Query clientQuery = new Query();
        clientQuery.addCriteria(Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId)));
        dataTemplate.remove(clientQuery, Visit.class);
    }

}
