package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Customer;
import com.viettel.persistence.mongo.domain.Order;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.embed.UserEmbed;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.OrderPendingRepository;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

@Repository
public class OrderPendingRepositoryImpl extends AbstractRepository<Order> implements OrderPendingRepository {

    private static final Criteria IS_ORDER_CRITERIA = Criteria.where(Order.COLUMNNAME_IS_ORDER).is(true);
    private static final Criteria PENDING_CRITERIA = Criteria.where(Order.COLUMNNAME_APPROVE_STATUS)
            .is(Order.APPROVE_STATUS_PENDING);

    @Override
    protected Criteria getDefaultCriteria() {
        return IS_ORDER_CRITERIA;
    }

    @Override
    public I_Order getById(String clientId, String id) {
        return _getById(clientId, false, true, id);
    }

    @Override
    public List<I_Order> getListByIds(String clientId, Collection<String> ids) {
        List<Order> list = _getListByIds(clientId, false, true, ids);
        return PO.convertList(I_Order.class, list);
    }

    @Override
    public List<I_OrderHeader> getPendingOrdersByDistributors(String clientId, Collection<String> distributorIds,
            PageSizeRequest pageSizeRequest) {
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID)
                .in(PO.convertIds(distributorIds));

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, PENDING_CRITERIA);

        List<Order> list = super._getList(clientId, null, null, criteria, getPageable(pageSizeRequest), null);
        return PO.convertList(I_OrderHeader.class, list);
    }

    @Override
    public long countPendingOrdersByDistributors(String clientId, Collection<String> distributorIds) {
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return 0l;
        }

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID)
                .in(PO.convertIds(distributorIds));

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, PENDING_CRITERIA);

        return super._count(clientId, null, null, criteria);
    }

    @Override
    public List<I_OrderHeader> getOrdersCreatedToday(String clientId, String distributorId, String createdUserId,
            Collection<String> customerIds) {
        Assert.notNull(distributorId);
        Assert.notNull(createdUserId);

        Criteria distributorCriteria = Criteria.where(Customer.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria createdByCriteria = Criteria.where(Order.COLUMNNAME_CREATED_BY_ID).is(PO.convertId(createdUserId));
        Criteria customerCriteria = null;
        if (customerIds != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).in(PO.convertIds(customerIds));
        }
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_START_TIME_VALUE,
                DateTimeUtils.getPeriodToday());

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, createdByCriteria, customerCriteria,
                periodCriteria);

        List<Order> list = super._getList(clientId, null, null, criteria, null, null);
        return PO.convertList(I_OrderHeader.class, list);
    }

    @Override
    public boolean approve(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime, String id) {
        Assert.notNull(id);
        Assert.notNull(approvedBy);
        Assert.notNull(approvedTime);
        
        Order order = super._getById(clientId, false, true, id);
        
        if (order != null && order.getApproveStatus() == Order.APPROVE_STATUS_PENDING) {
            order.setApprovedBy(new UserEmbed(approvedBy));
            order.setApprovedTime(approvedTime);
            order.setApproveStatus(Order.APPROVE_STATUS_APPROVED);
            super._save(clientId, order);
            return true;
        }
        
        return false;
    }

    @Override
    public boolean reject(String clientId, I_UserEmbed approvedBy, SimpleDate approvedTime, String id) {
        Assert.notNull(id);
        Assert.notNull(approvedBy);
        Assert.notNull(approvedTime);
        
        Order order = super._getById(clientId, false, true, id);
        
        if (order != null && order.getApproveStatus() == Order.APPROVE_STATUS_PENDING) {
            order.setApprovedBy(new UserEmbed(approvedBy));
            order.setApprovedTime(approvedTime);
            order.setApproveStatus(Order.APPROVE_STATUS_REJECTED);
            super._save(clientId, order);
            return true;
        }
        
        return false;
    }

    @Override
    public I_Order create(String clientId, I_Order _order) {
        Assert.notNull(_order);
        
        Order order = new Order(_order);
        
        return super._save(clientId, order);
    }

}
