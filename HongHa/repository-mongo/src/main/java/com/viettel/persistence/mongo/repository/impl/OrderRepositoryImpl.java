package com.viettel.persistence.mongo.repository.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Order;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.OrderRepository;
import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.data.I_Order;
import com.viettel.repository.common.domain.data.I_OrderHeader;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

@Repository
public class OrderRepositoryImpl extends AbstractVisitAndOrderRepository<Order> implements OrderRepository {

    private static final Criteria DEFAULT_CRITERIA = CriteriaUtils.andOperator(
            Criteria.where(Order.COLUMNNAME_IS_ORDER).is(true),
            Criteria.where(Order.COLUMNNAME_APPROVE_STATUS).is(Order.APPROVE_STATUS_APPROVED));

    @Override
    protected Criteria getDefaultCriteria() {
        return DEFAULT_CRITERIA;
    }

    @Override
    public List<I_OrderHeader> getOrderHeadersByDistributor(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType, PageSizeRequest pageSizeRequest,
            boolean sortAsc) {
        List<I_Order> list = getOrdersByDistributor(clientId, distributorId, createdUserId, customerId, period,
                orderDateType, pageSizeRequest, sortAsc);

        return PO.convertList(I_OrderHeader.class, list);
    }

    @Override
    public List<I_Order> getOrdersByDistributor(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType, PageSizeRequest pageSizeRequest,
            boolean sortAsc) {
        Assert.notNull(distributorId);
        Assert.notNull(period);
        Assert.notNull(orderDateType);

        Criteria createdUserCriteria = null;
        if (createdUserId != null) {
            createdUserCriteria = Criteria.where(Order.COLUMNNAME_CREATED_BY_ID).is(PO.convertId(createdUserId));
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));
        }

        Criteria criteria = CriteriaUtils.andOperator(createdUserCriteria, customerCriteria);

        List<Order> list = super._getList(clientId, distributorId, period, criteria, getPageable(pageSizeRequest),
                sortAsc);

        return PO.convertList(I_Order.class, list);
    }

    @Override
    public long countOrdersByDistributor(String clientId, String distributorId, String createdUserId, String customerId,
            Period period, OrderDateType orderDateType) {
        Assert.notNull(distributorId);
        Assert.notNull(period);
        Assert.notNull(orderDateType);

        Criteria createdUserCriteria = null;
        if (createdUserId != null) {
            createdUserCriteria = Criteria.where(Order.COLUMNNAME_CREATED_BY_ID).is(PO.convertId(createdUserId));
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));
        }

        Criteria criteria = CriteriaUtils.andOperator(createdUserCriteria, customerCriteria);

        return super._count(clientId, distributorId, period, criteria);
    }

    @Override
    public Order getOrderByCode(String clientId, String code) {
        if (code == null) {
            return null;
        }

        Criteria codeCriteria = Criteria.where(Order.COLUMNNAME_CODE).is(code);

        return _getFirst(clientId, null, null, codeCriteria, null);
    }

    // PRIVATE
    // TODO remove permanent???
    // private Criteria getDateCriteria(Period period, OrderDateType
    // orderDateType) {
    // return
    // CriteriaUtils.getPeriodCriteria(getPeriodColumnName(orderDateType),
    // period);
    // }

    // private String getPeriodColumnName(OrderDateType orderDateType) {
    // if (orderDateType == OrderDateType.CREATED_DATE) {
    // return Order.COLUMNNAME_CREATED_TIME_VALUE;
    // } else {
    // return Order.COLUMNNAME_APPROVE_TIME_VALUE;
    // }
    // }

}
