package com.viettel.persistence.mongo.repository.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Order;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.entity.OrderSummary;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.OrderSummaryRepository;
import com.viettel.repository.common.domain.category.I_SystemConfig.OrderDateType;
import com.viettel.repository.common.domain.data.I_OrderSummary;
import com.viettel.repository.common.entity.SimpleDate.Period;

@Repository
public class OrderSummaryRepositoryImpl implements OrderSummaryRepository {

    private static final Criteria IS_ORDER_CRITERIA = Criteria.where(Order.COLUMNNAME_IS_ORDER).is(true);
    private static final Criteria APPROVED_CRITERIA = Criteria.where(Order.COLUMNNAME_APPROVE_STATUS)
            .is(Order.APPROVE_STATUS_APPROVED);

    //@formatter:off
    private static final String REDUCE_FUNCTION = "function (key, values) {" 
            + "    var result = { "
            + "        revenue: 0, " 
            + "        subRevenue: 0, "
            + "        productivity: 0, " 
            + "        quantity: 0, " 
            + "        nbSKU: 0, " 
            + "        nbOrder: 0, "
            + "        nbOrderWithoutVisit: 0 " 
            + "    };" 
            + "    " 
            + "    for(var i = 0; i < values.length; i++) {"
            + "        var value = values[i];" 
            + "        result.revenue += value.revenue == null ? 0 : parseFloat(value.revenue);"
            + "        result.subRevenue += value.subRevenue == null ? 0 : parseFloat(value.subRevenue);"
            + "        result.productivity += value.productivity == null ? 0 : parseFloat(value.productivity);"
            + "        result.quantity += value.quantity == null ? 0 : parseFloat(value.quantity);"
            + "        result.nbSKU += value.nbSKU == null ? 0 : value.nbSKU;" 
            + "        result.nbOrder += value.nbOrder == null ? 0 : value.nbOrder;"
            + "        result.nbOrderWithoutVisit += value.nbOrderWithoutVisit == null ? 0 : value.nbOrderWithoutVisit;" 
            + "    };" 
            + "    "
            + "    result.revenue = result.revenue.toString();"
            + "    result.subRevenue = result.subRevenue.toString();"
            + "    result.productivity = result.productivity.toString();" 
            + "    result.quantity = result.quantity.toString();"
            + "    " 
            + "    return result;" 
            + "}";

    private static final String EMIT_VALUE = "" 
            + "        { " 
            + "            revenue: this.grandTotal, "
            + "            subRevenue: this.subTotal, "
            + "            productivity: this.productivity, " 
            + "            nbSKU: this.details.length, "
            + "            nbOrder: 1, " 
            + "            nbOrderWithoutVisit: (this.isVisit ? 0 : 1) " 
            + "        }";
    //@formatter:on

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Map<String, I_OrderSummary> getOrderSummaryDaily(String clientId, Collection<String> distributorIds,
            Period period, OrderDateType orderDateType) {
        Assert.notNull(clientId);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return Collections.emptyMap();
        }

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                IS_ORDER_CRITERIA, APPROVED_CRITERIA);

        return _getOrderSummaryDaily(criteria);
    }

    @Override
    public Map<String, I_OrderSummary> getOrderSummaryDaily(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);

        Criteria createdCriteria = null;
        if (createdUserId != null) {
            createdCriteria = Criteria.where(Order.COLUMNNAME_CREATED_BY_ID).is(PO.convertId(createdUserId));
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));
        }

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                createdCriteria, customerCriteria, IS_ORDER_CRITERIA, APPROVED_CRITERIA);

        return _getOrderSummaryDaily(criteria);
    }

    @Override
    public Map<String, I_OrderSummary> getOrderSummaryBySalesman(String clientId, String distributorId, Period period,
            OrderDateType orderDateType) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                IS_ORDER_CRITERIA, APPROVED_CRITERIA);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    ");
        mapFn.append("    emit(");
        mapFn.append("        this.createdBy._id, " + EMIT_VALUE);
        mapFn.append("    );");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultById> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultById.class);

        List<MapReduceResultById> mapReduceResultByIds = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultByIds, mapReduceResults.iterator());

        // AGGRAGATION
        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation project = Aggregation
                .project(Aggregation.bind("createdBy._id", "createdBy._id").and("customer._id", "customer._id"));
        AggregationOperation group = Aggregation.group("createdBy._id").addToSet("customer._id").as("customerIds");
        Aggregation aggregation = Aggregation.newAggregation(match, project, group);

        AggregationResults<AggregationResultById> aggregationResults = mongoTemplate.aggregate(aggregation,
                "VisitAndOrder", AggregationResultById.class);
        List<AggregationResultById> aggregationResultByIds = aggregationResults.getMappedResults();

        HashMap<ObjectId, Integer> nbCustomerByDate = new HashMap<>();
        for (AggregationResultById aggregationResultById : aggregationResultByIds) {
            nbCustomerByDate.put(aggregationResultById.getId(), aggregationResultById.getNbCustomer());
        }

        HashMap<String, I_OrderSummary> map = new HashMap<>();
        for (MapReduceResultById mapReduceResultById : mapReduceResultByIds) {
            OrderSummary orderSummary = mapReduceResultById.getValue();
            orderSummary.setNbCustomer(nbCustomerByDate.get(mapReduceResultById.getId()));

            map.put(mapReduceResultById.getId().toString(), orderSummary);
        }

        return map;
    }

    @Override
    public Map<String, I_OrderSummary> getOrderSummaryByProduct(String clientId, Collection<String> distributorIds,
            String productCategoryId, Period period, OrderDateType orderDateType) {
        Assert.notNull(clientId);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return Collections.emptyMap();
        }

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);

        Criteria productCategoryCriteria = null;
        if (productCategoryId != null) {
            productCategoryCriteria = Criteria.where("details")
                    .elemMatch(Criteria.where("product.productCategory._id").in(PO.convertId(productCategoryId)));
        }

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                IS_ORDER_CRITERIA, APPROVED_CRITERIA, productCategoryCriteria);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    for (var i = 0; i < this.details.length; i++) {");
        mapFn.append("        var detail = this.details[i];");
        mapFn.append("        var revenue = parseFloat(detail.quantity) * parseFloat(detail.product.price);");
        mapFn.append("        revenue = revenue.toString();");
        mapFn.append(
                "        var productivity = parseFloat(detail.quantity) * parseFloat(detail.product.productivity);");
        mapFn.append("        productivity = productivity.toString();");
        mapFn.append("        ");
        mapFn.append("        emit(");
        mapFn.append("            detail.product._id, ");
        mapFn.append("            { ");
        mapFn.append("                revenue: revenue, ");
        mapFn.append("                productivity: productivity, ");
        mapFn.append("                nbSKU: 0, ");
        mapFn.append("                nbOrderWithoutVisit: (this.isVisit ? 0 : 1), ");
        mapFn.append("                nbOrder: 1");
        mapFn.append("            }");
        mapFn.append("        );");
        mapFn.append("    }");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultById> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultById.class);

        List<MapReduceResultById> mapReduceResultByIds = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultByIds, mapReduceResults.iterator());

        HashMap<String, I_OrderSummary> map = new HashMap<>();
        for (MapReduceResultById mapReduceResultById : mapReduceResultByIds) {
            OrderSummary orderSummary = mapReduceResultById.getValue();
            map.put(mapReduceResultById.getId().toString(), orderSummary);
        }

        return map;
    }

    @Override
    public Map<String, I_OrderSummary> getOrderSummaryByDistributor(String clientId, Collection<String> distributorIds,
            Period period, OrderDateType orderDateType) {
        Assert.notNull(clientId);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return Collections.emptyMap();
        }

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                IS_ORDER_CRITERIA, APPROVED_CRITERIA);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    ");
        mapFn.append("    emit(");
        mapFn.append("        this.distributor._id, " + EMIT_VALUE);
        mapFn.append("    );");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultById> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultById.class);

        List<MapReduceResultById> mapReduceResultByIds = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultByIds, mapReduceResults.iterator());

        // AGGRAGATION
        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation project = Aggregation.project(Aggregation.bind("distributor._id", "distributor._id")
                .and("createdBy._id", "createdBy._id").and("customer._id", "customer._id"));
        AggregationOperation group = Aggregation.group("distributor._id").addToSet("createdBy._id").as("salesmanIds")
                .addToSet("customer._id").as("customerIds");
        Aggregation aggregation = Aggregation.newAggregation(match, project, group);

        AggregationResults<AggregationResultById> aggregationResults = mongoTemplate.aggregate(aggregation,
                "VisitAndOrder", AggregationResultById.class);
        List<AggregationResultById> aggregationResultByIds = aggregationResults.getMappedResults();

        HashMap<ObjectId, Integer> nbSalesmanByDate = new HashMap<>();
        HashMap<ObjectId, Integer> nbCustomerByDate = new HashMap<>();
        for (AggregationResultById aggregationResultById : aggregationResultByIds) {
            nbSalesmanByDate.put(aggregationResultById.getId(), aggregationResultById.getNbSalesman());
            nbCustomerByDate.put(aggregationResultById.getId(), aggregationResultById.getNbCustomer());
        }

        HashMap<String, I_OrderSummary> map = new HashMap<>();
        for (MapReduceResultById mapReduceResultById : mapReduceResultByIds) {
            OrderSummary orderSummary = mapReduceResultById.getValue();
            orderSummary.setNbSalesman(nbSalesmanByDate.get(mapReduceResultById.getId()));
            orderSummary.setNbCustomer(nbCustomerByDate.get(mapReduceResultById.getId()));

            map.put(mapReduceResultById.getId().toString(), orderSummary);
        }

        return map;
    }

    @Override
    public Map<String, BigDecimal> getProductSoldMap(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);

        Criteria createdCriteria = null;
        if (createdUserId != null) {
            createdCriteria = Criteria.where(Order.COLUMNNAME_CREATED_BY_ID).is(PO.convertId(createdUserId));
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));
        }

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                createdCriteria, customerCriteria, IS_ORDER_CRITERIA, APPROVED_CRITERIA);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    for (var i = 0; i < this.details.length; i++) {");
        mapFn.append("        var detail = this.details[i];");
        mapFn.append("        ");
        mapFn.append("        emit(");
        mapFn.append("            detail.product._id, ");
        mapFn.append("            { ");
        mapFn.append("                quantity: detail.quantity ");
        mapFn.append("            }");
        mapFn.append("        );");
        mapFn.append("    }");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultById> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultById.class);

        List<MapReduceResultById> mapReduceResultByIds = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultByIds, mapReduceResults.iterator());

        HashMap<String, BigDecimal> map = new HashMap<>();
        for (MapReduceResultById mapReduceResultById : mapReduceResultByIds) {
            OrderSummary orderSummary = mapReduceResultById.getValue();
            map.put(mapReduceResultById.getId().toString(), orderSummary.getQuantity());
        }

        return map;
    }

    @Override
    public I_OrderSummary getOrderSummary(String clientId, String distributorId, String createdUserId,
            String customerId, Period period, OrderDateType orderDateType) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Order.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Order.COLUMNNAME_CREATED_TIME_VALUE, period);

        Criteria createdCriteria = null;
        if (createdUserId != null) {
            createdCriteria = Criteria.where(Order.COLUMNNAME_CREATED_BY_ID).is(PO.convertId(createdUserId));
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Order.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));
        }

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                createdCriteria, customerCriteria, IS_ORDER_CRITERIA, APPROVED_CRITERIA);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    ");
        mapFn.append("    emit(");
        mapFn.append("        this.distributor._id, " + EMIT_VALUE);
        mapFn.append("    );");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultById> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultById.class);

        List<MapReduceResultById> mapReduceResultByIds = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultByIds, mapReduceResults.iterator());

        // AGGRAGATION
        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation project = Aggregation.project(Aggregation.bind("distributor._id", "distributor._id")
                .and("createdBy._id", "createdBy._id").and("customer._id", "customer._id"));
        AggregationOperation group = Aggregation.group("distributor._id").addToSet("createdBy._id").as("salesmanIds")
                .addToSet("customer._id").as("customerIds");
        Aggregation aggregation = Aggregation.newAggregation(match, project, group);

        AggregationResults<AggregationResultById> aggregationResults = mongoTemplate.aggregate(aggregation,
                "VisitAndOrder", AggregationResultById.class);
        List<AggregationResultById> aggregationResultByIds = aggregationResults.getMappedResults();

        HashMap<ObjectId, Integer> nbSalesmanByDate = new HashMap<>();
        HashMap<ObjectId, Integer> nbCustomerByDate = new HashMap<>();
        for (AggregationResultById aggregationResultById : aggregationResultByIds) {
            nbSalesmanByDate.put(aggregationResultById.getId(), aggregationResultById.getNbSalesman());
            nbCustomerByDate.put(aggregationResultById.getId(), aggregationResultById.getNbCustomer());
        }

        HashMap<String, I_OrderSummary> map = new HashMap<>();
        for (MapReduceResultById mapReduceResultById : mapReduceResultByIds) {
            OrderSummary orderSummary = mapReduceResultById.getValue();
            orderSummary.setNbSalesman(nbSalesmanByDate.get(mapReduceResultById.getId()));
            orderSummary.setNbCustomer(nbCustomerByDate.get(mapReduceResultById.getId()));

            map.put(mapReduceResultById.getId().toString(), orderSummary);
        }

        if (map.containsKey(distributorId)) {
            return map.get(distributorId);
        } else {
            return new OrderSummary();
        }
    }

    // #S PRIVATE FUNCTION
    private Map<String, I_OrderSummary> _getOrderSummaryDaily(Criteria criteria) {
        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    var month = this.startTime.month + 1;");
        mapFn.append("    month = month > 9 ? month.toString() : '0' + month.toString();");
        mapFn.append("    ");
        mapFn.append("    var date = this.startTime.date;");
        mapFn.append("    date = date > 9 ? date.toString() : '0' + date.toString();");
        mapFn.append("    ");
        mapFn.append("    var isoDate = this.startTime.year + '-' + month + '-' + date;");
        mapFn.append("    ");
        mapFn.append("    emit(");
        mapFn.append("        isoDate, " + EMIT_VALUE);
        mapFn.append("    );");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultDaily> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultDaily.class);

        List<MapReduceResultDaily> mapReduceResultDailies = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultDailies, mapReduceResults.iterator());

        // AGGRAGATION
        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation project = Aggregation.project(Aggregation.bind("date", "startTime.isoDate")
                .and("createdBy._id", "createdBy._id").and("customer._id", "customer._id"));
        AggregationOperation group = Aggregation.group("date").addToSet("createdBy._id").as("salesmanIds")
                .addToSet("customer._id").as("customerIds");
        Aggregation aggregation = Aggregation.newAggregation(match, project, group);

        AggregationResults<AggregationResultDaily> aggregationResults = mongoTemplate.aggregate(aggregation,
                "VisitAndOrder", AggregationResultDaily.class);
        List<AggregationResultDaily> aggregationResultDailies = aggregationResults.getMappedResults();

        HashMap<String, Integer> nbSalesmanByDate = new HashMap<>();
        HashMap<String, Integer> nbCustomerByDate = new HashMap<>();
        for (AggregationResultDaily aggregationResultDaily : aggregationResultDailies) {
            nbSalesmanByDate.put(aggregationResultDaily.getId(), aggregationResultDaily.getNbSalesman());
            nbCustomerByDate.put(aggregationResultDaily.getId(), aggregationResultDaily.getNbCustomer());
        }

        HashMap<String, I_OrderSummary> map = new HashMap<>();
        for (MapReduceResultDaily mapReduceResultDaily : mapReduceResultDailies) {
            OrderSummary orderSummary = mapReduceResultDaily.getValue();
            orderSummary.setNbSalesman(nbSalesmanByDate.get(mapReduceResultDaily.getId()));
            orderSummary.setNbCustomer(nbCustomerByDate.get(mapReduceResultDaily.getId()));

            map.put(mapReduceResultDaily.getId(), orderSummary);
        }

        return map;
    }
    // #E PRIVATE FUNCTION

    // #S PRIVATE CLASS
    public static class MapReduceResultDaily implements Serializable {

        private static final long serialVersionUID = -1652482571529294287L;

        private String id;
        private OrderSummary value;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public OrderSummary getValue() {
            return value;
        }

        public void setValue(OrderSummary value) {
            this.value = value;
        }

    }

    public static class MapReduceResultById implements Serializable {

        private static final long serialVersionUID = -1652482571529294287L;

        private ObjectId id;
        private OrderSummary value;

        public ObjectId getId() {
            return id;
        }

        public void setId(ObjectId id) {
            this.id = id;
        }

        public OrderSummary getValue() {
            return value;
        }

        public void setValue(OrderSummary value) {
            this.value = value;
        }

    }

    private static class AggregationResultDaily implements Serializable {

        private static final long serialVersionUID = 6632906021427053207L;

        private String id;
        private List<ObjectId> salesmanIds;
        private List<ObjectId> customerIds;

        public String getId() {
            return id;
        }

        public int getNbSalesman() {
            return salesmanIds == null ? 0 : salesmanIds.size();
        }

        public int getNbCustomer() {
            return customerIds == null ? 0 : customerIds.size();
        }

    }

    private static class AggregationResultById implements Serializable {

        private static final long serialVersionUID = 6632906021427053207L;

        private ObjectId id;
        private List<ObjectId> salesmanIds;
        private List<ObjectId> customerIds;

        public ObjectId getId() {
            return id;
        }

        public int getNbSalesman() {
            return salesmanIds == null ? 0 : salesmanIds.size();
        }

        public int getNbCustomer() {
            return customerIds == null ? 0 : customerIds.size();
        }

    }
    // #E

}
