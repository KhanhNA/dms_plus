package com.viettel.persistence.mongo.repository.impl;

import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.ProductCategory;
import com.viettel.repository.common.ProductCategoryRepository;
import com.viettel.repository.common.domain.common.I_Category;

@Repository
public class ProductCategoryRepositoryImpl extends CategoryRepositoryImpl<ProductCategory, I_Category> implements
        ProductCategoryRepository {

    @Override
    protected ProductCategory convertToDomain(I_Category write) {
        return new ProductCategory(write);
    }

}
