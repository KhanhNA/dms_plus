package com.viettel.persistence.mongo.repository.impl;

import java.util.Collections;
import java.util.Set;

import org.springframework.stereotype.Repository;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.ProductImportant;
import com.viettel.repository.common.ProductImportantRepository;

@Repository
public class ProductImportantRepositoryImpl extends AbstractRepository<ProductImportant>
		implements ProductImportantRepository {

	public void saveProductImportant(String clientId, Set<String> productIds) {

		ProductImportant productImportant = _getFirst(clientId, null, null, null, null);
		if (productImportant == null) {
			productImportant = new ProductImportant();
			productImportant.setClientId(PO.convertId(clientId));
			productImportant.setDraft(false);
			productImportant.setActive(true);
		}

		productImportant.setProductIds(PO.convertIds(productIds));

		_save(clientId, productImportant);
	}

	public Set<String> getProductImportant(String clientId) {
		ProductImportant productImportant = _getFirst(clientId, null, null, null, null);
		if (productImportant != null && productImportant.getProductIds() != null) {
			return PO.convertObjectIds(productImportant.getProductIds());
		} else {
			return Collections.emptySet();
		}
	}
}
