package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Product;
import com.viettel.persistence.mongo.repository.common.CacheRepository;
import com.viettel.repository.common.ProductRepository;
import com.viettel.repository.common.domain.product.I_Product;

@Repository
public class ProductRepositoryImpl extends CategoryRepositoryImpl<Product, I_Product> implements ProductRepository {

    @Autowired
    private CacheRepository cacheRepository;

    @Override
    protected Product convertToDomain(I_Product write) {
        return new Product(write);
    }

    @Override
    protected void onChange(String clientId) {
        super.onChange(clientId);
        cacheRepository.setProductActiveListCache(clientId, null);
        cacheRepository.setProductMapCache(clientId, null);
    }

    @Override
    public Map<String, I_Product> getProductMap(String clientId, Boolean active) {
        Map<String, I_Product> productMap = cacheRepository.getProductMapCache(clientId);

        if (productMap == null) {
            List<Product> products = super._getList(clientId, false, null, null, null, null);
            productMap = new HashMap<>();
            for (Product product : products) {
                productMap.put(product.getId(), product);
            }

            cacheRepository.setProductMapCache(clientId, productMap);
        }

        if (active != null) {
            HashMap<String, I_Product> map = new HashMap<>();
            for (Entry<String, I_Product> entry : productMap.entrySet()) {
                if (entry.getValue().isActive() == active) {
                    map.put(entry.getKey(), entry.getValue());
                }
            }

            return map;
        } else {
            return productMap;
        }
    }

    @Override
    public List<I_Product> getAll(String clientId, String distributorId) {
        List<I_Product> products = cacheRepository.getProductActiveListCache(clientId);
        if (products == null) {
            products = super.getAll(clientId, distributorId);
            cacheRepository.setProductActiveListCache(clientId, products);
        }

        return products;
    }

    @Override
    public List<I_Product> getProductsByCategories(String clientId, Collection<String> productCategoryIds) {
        Assert.notNull(productCategoryIds);

        if (productCategoryIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria productCategoryCriteria = Criteria.where(Product.COLUMNNAME_PRODUCT_CATEGORY_ID)
                .in(PO.convertIds(productCategoryIds));

        List<Product> list = super._getList(clientId, false, true, productCategoryCriteria, null, null);

        return PO.convertList(I_Product.class, list);
    }

    // CHECK
    @Override
    public boolean checkUOMUsed(String clientId, String uomId) {
        Assert.notNull(uomId);

        Criteria criteria = Criteria.where(Product.COLUMNNAME_UOM_ID).is(PO.convertId(uomId));

        return _checkUsed(clientId, criteria);
    }

    @Override
    public boolean checkProductCategoryUsed(String clientId, String productCategoryId) {
        Assert.notNull(productCategoryId);

        Criteria criteria = Criteria.where(Product.COLUMNNAME_PRODUCT_CATEGORY_ID).is(PO.convertId(productCategoryId));

        return _checkUsed(clientId, criteria);
    }

}
