package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Promotion;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.PromotionRepository;
import com.viettel.repository.common.domain.promotion.I_Promotion;
import com.viettel.repository.common.domain.promotion.I_PromotionHeader;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

@Repository
public class PromotionRepositoryImpl extends CategoryComplexRepositoryImpl<Promotion, I_PromotionHeader, I_Promotion>
        implements PromotionRepository {

    @Override
    protected Sort getSort() {
        return new Sort(Direction.DESC, PO.COLUMNNAME_DRAFT);
    }

    @Override
    protected Promotion convertToDomain(I_Promotion write) {
        return new Promotion(write);
    }

    @Override
    public List<I_Promotion> getPromotionsAvailableToday(String clientId, Collection<String> distributorIds) {
        SimpleDate today = DateTimeUtils.getToday();

        Criteria fromDateCriteria = Criteria.where(Promotion.COLUMNNAME_START_DATE_VALUE).lte(today.getValue());
        Criteria toDateCriteria = Criteria.where(Promotion.COLUMNNAME_END_DATE_VALUE).gte(today.getValue());
        Criteria distributorCriteria = null;
        if (distributorIds != null) {
            distributorCriteria = CriteriaUtils.orOperator(
                    Criteria.where(Promotion.COLUMNNAME_FOR_ALL_DISTRIBUTOR).is(true),
                    Criteria.where(Promotion.COLUMNNAME_DISTRIBUTOR_IDS).all(PO.convertIds(distributorIds)));
        }

        Criteria criteria = CriteriaUtils.andOperator(fromDateCriteria, toDateCriteria, distributorCriteria);

        Sort sort = new Sort(Direction.DESC, Promotion.COLUMNNAME_START_DATE_VALUE);

        List<Promotion> list = super._getList(clientId, false, true, criteria, null, sort);

        return PO.convertList(I_Promotion.class, list);
    }

    @Override
    public List<I_PromotionHeader> getPromotionHeadersAvailableToday(String clientId,
            Collection<String> distributorIds) {
        List<I_Promotion> list = getPromotionsAvailableToday(clientId, distributorIds);
        return PO.convertList(I_PromotionHeader.class, list);
    }

}
