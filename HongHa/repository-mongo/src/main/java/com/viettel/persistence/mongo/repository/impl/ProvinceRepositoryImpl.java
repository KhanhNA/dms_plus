package com.viettel.persistence.mongo.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Province;
import com.viettel.repository.common.ProvinceRepository;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.entity.Location;
import com.viettel.repository.common.util.LocationUtils;

@Repository
public class ProvinceRepositoryImpl extends CategoryRepositoryImpl<Province, I_Category> implements ProvinceRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    protected Province convertToDomain(I_Category write) {
        return new Province(write);
    }

    @Override
    public I_Category getProvinceByLocation(Location location) {
        Assert.notNull(location);
        Assert.isTrue(LocationUtils.checkLocationValid(location));

        Criteria criteria = Criteria.where("polygons")
                .intersects(new GeoJsonPoint(location.getLongitude(), location.getLatitude()));

        Query mainQuery = new Query();
        mainQuery.addCriteria(criteria);

        return mongoTemplate.findOne(mainQuery, Province.class);
    }

}
