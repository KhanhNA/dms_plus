package com.viettel.persistence.mongo.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.Region;
import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;

@Repository
public class RegionRepositoryImpl extends CategoryRepositoryImpl<Region, I_Region> implements RegionRepository {

	@Autowired
	DistributorRepository distributorRepository;

	@Override
	protected Region convertToDomain(I_Region write) {
		return new Region(write);
	}

	@Override
	public I_Region getRootNode(String clientId) {
		Criteria root = Criteria.where(Region.COLUMNNAME_ROOT).is(true);
		return _getFirst(clientId, false, true, root, null);
	}

	@Override
	public List<I_CategoryEmbed> getDistributorsOfRegion(String clientId, String id) {
		Criteria criteria = Criteria.where(Region.COLUMNNAME_ID).is(id);
		I_Region region = _getFirst(clientId, false, true, criteria, null);
		if (region == null) {
			return Collections.emptyList();
		}

		if (region.getDistributors() != null && region.getDistributors().size() > 0) {
			List<I_CategoryEmbed> result = new ArrayList<>();
			for (I_CategoryEmbed item : region.getDistributors()) {
				result.add(item);
			}
			return result;
		} else {
			return Collections.emptyList();
		}
	}

	@Override
	public boolean setDistributorsOfManager(String clientId, String id, Collection<String> distributorIds) {
		Region region = _getById(clientId, false, true, id);
		List<I_Distributor> lstDistributos = distributorRepository.getListByIds(clientId, distributorIds);

		Set<I_CategoryEmbed> distributors = new HashSet<>();
		for (I_Distributor dis : lstDistributos) {
			I_CategoryEmbed item = new CategoryEmbed(dis);
			distributors.add(item);
		}

		region.setDistributors(distributors);
		_save(clientId, region);
		return true;
	}

}
