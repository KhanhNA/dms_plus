package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.Category;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Route;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.RouteRepository;
import com.viettel.repository.common.domain.customer.I_Route;
import com.viettel.repository.common.domain.user.I_UserEmbed;

@Repository
public class RouteRepositoryImpl extends CategoryRepositoryImpl<Route, I_Route> implements RouteRepository {

    @Override
    protected Route convertToDomain(I_Route write) {
        return new Route(write);
    }
    
    @Override
    public List<I_Route> getAll(String clientId, Collection<String> distributorIds) {
        Criteria distributorCriteria = Criteria.where(Category.COLUMNNAME_DISTRIBUTOR_ID)
                .in(PO.convertIds(distributorIds));

        List<Route> list = _getList(clientId, false, true, distributorCriteria, null, null);

        return PO.convertList(I_Route.class, list);
    }

    @Override
    public List<I_Route> getRoutesBySalesmen(String clientId, String distributorId, Collection<String> salesmanIds) {
        Assert.notNull(distributorId);
        Assert.notNull(salesmanIds);

        if (salesmanIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria distributorCriteria = Criteria.where(Route.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria salesmanCriteria = Criteria.where(Route.COLUMNNAME_SALESMAN_ID).in(PO.convertIds(salesmanIds));

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, salesmanCriteria);

        List<Route> list = super._getList(clientId, false, true, criteria, null, null);

        return PO.convertList(I_Route.class, list);
    }

    // CHECK
    @Override
    public boolean checkSalesmanUsed(String clientId, String distributorId, String salesmanId) {
        Assert.notNull(distributorId);
        Assert.notNull(salesmanId);

        Criteria distributorCriteria = Criteria.where(Route.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria salesmanCriteria = Criteria.where(Route.COLUMNNAME_SALESMAN_ID).is(PO.convertId(salesmanId));

        Criteria criteria = CriteriaUtils.andOperator(distributorCriteria, salesmanCriteria);

        return super._checkUsed(clientId, criteria);
    }

    @Override
    public boolean setSalesman(String clientId, String id, I_UserEmbed salesman) {
        // TODO Auto-generated method stub
        return false;
    }

}
