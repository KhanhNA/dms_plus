package com.viettel.persistence.mongo.repository.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Visit;
import com.viettel.persistence.mongo.domain.embed.CategoryEmbed;
import com.viettel.persistence.mongo.domain.embed.SurveyAnswerContent;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.SurveyAnswerRepository;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.customer.I_CustomerEmbed;
import com.viettel.repository.common.domain.data.I_SurveyAnswer;
import com.viettel.repository.common.domain.data.I_SurveyAnswerContent;
import com.viettel.repository.common.domain.user.I_UserEmbed;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;

@Repository
public class SurveyAnswerRepositoryImpl extends AbstractRepository<Visit> implements SurveyAnswerRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<I_SurveyAnswer> getSurveyAnswerByDistributors(String clientId, String surveyId,
            Collection<String> distributorIds, PageSizeRequest pageSizeRequest) {
        Assert.notNull(clientId);
        Assert.notNull(surveyId);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return Collections.emptyList();
        }

        Criteria ditributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        Criteria surveyCriteria = Criteria
                .where(Visit.COLUMNNAME_SURVEY_ANSWERS + "." + SurveyAnswerContent.COLUMNNAME_SURVEY_ID)
                .is(PO.convertId(surveyId));

        Criteria criteria = CriteriaUtils.andOperator(ditributorCriteria, surveyCriteria);

        List<Visit> visits = super._getList(clientId, null, null, criteria, null, null);

        List<I_SurveyAnswer> list = new ArrayList<I_SurveyAnswer>(visits.size());
        for (Visit visit : visits) {
            if (visit.getSurveyAnswers() != null && !visit.getSurveyAnswers().isEmpty()) {
                for (I_SurveyAnswerContent surveyAnswer : visit.getSurveyAnswers()) {
                    if (surveyAnswer.getSurvey() != null && surveyAnswer.getSurvey().getId() != null
                            && surveyAnswer.getSurvey().getId().equals(surveyId)) {
                        list.add(new I_SurveyAnswer() {

                            private static final long serialVersionUID = 1L;

                            @Override
                            public I_CategoryEmbed getSurvey() {
                                return new CategoryEmbed(surveyAnswer.getSurvey());
                            }

                            @Override
                            public Set<Integer> getOptions() {
                                return surveyAnswer.getOptions();
                            }

                            @Override
                            public I_CategoryEmbed getDistributor() {
                                return visit.getDistributor();
                            }

                            @Override
                            public I_CustomerEmbed getCustomer() {
                                return visit.getCustomer();
                            }

                            @Override
                            public SimpleDate getCreatedTime() {
                                return visit.getCreatedTime();
                            }

                            @Override
                            public I_UserEmbed getCreatedBy() {
                                return visit.getCreatedBy();
                            }
                        });
                        break;
                    }
                }
            }
        }

        return list;
    }

    @Override
    public long countSurveyAnswerByDistributors(String clientId, String surveyId, Collection<String> distributorIds) {
        Assert.notNull(clientId);
        Assert.notNull(surveyId);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return 0l;
        }

        Criteria ditributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        Criteria surveyCriteria = Criteria
                .where(Visit.COLUMNNAME_SURVEY_ANSWERS + "." + SurveyAnswerContent.COLUMNNAME_SURVEY_ID)
                .is(PO.convertId(surveyId));

        Criteria criteria = CriteriaUtils.andOperator(ditributorCriteria, surveyCriteria);

        return super._count(clientId, null, null, criteria);
    }

    @Override
    public Map<Integer, Integer> getOptionCountMap(String clientId, String surveyId,
            Collection<String> distributorIds) {
        Criteria clientCriteria = Criteria.where(Visit.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        Criteria surveyCriteria = Criteria
                .where(Visit.COLUMNNAME_SURVEY_ANSWERS + "." + SurveyAnswerContent.COLUMNNAME_SURVEY_ID)
                .is(PO.convertId(surveyId));

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, surveyCriteria);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("  for (var i = 0; i < this.surveyAnswers.length; i++) {");
        mapFn.append("      var surveyAnswer = this.surveyAnswers[i];");
        mapFn.append("      ");
        mapFn.append("      if (surveyAnswer.survey._id.valueOf() == '").append(surveyId.toString()).append("') {");
        mapFn.append("          for (var j = 0; j < surveyAnswer.options.length; j++) {");
        mapFn.append("              var option = surveyAnswer.options[j];");
        mapFn.append("              emit(option, 1);");
        mapFn.append("          }");
        mapFn.append("      }");
        mapFn.append("  }");
        mapFn.append("}");

        StringBuilder reduceFn = new StringBuilder();
        reduceFn.append("function (key, values) {return Array.sum(values);}");

        MapReduceResults<OptionValue> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), reduceFn.toString(), OptionValue.class);

        HashMap<Integer, Integer> map = new HashMap<>();
        Iterator<OptionValue> optionValues = mapReduceResults.iterator();
        while (optionValues.hasNext()) {
            OptionValue optionValue = optionValues.next();
            map.put(optionValue.getId(), optionValue.getValue());
        }

        return map;
    }

    private static class OptionValue implements Serializable {

        private static final long serialVersionUID = 9025288406511677623L;

        private int id;
        private int value;

        public int getId() {
            return id;
        }

        public int getValue() {
            return value;
        }
    }

}
