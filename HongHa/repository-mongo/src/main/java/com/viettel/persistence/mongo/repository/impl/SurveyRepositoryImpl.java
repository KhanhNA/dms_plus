package com.viettel.persistence.mongo.repository.impl;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.Category;
import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Survey;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.SurveyRepository;
import com.viettel.repository.common.domain.survey.I_Survey;
import com.viettel.repository.common.domain.survey.I_SurveyHeader;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate;
import com.viettel.repository.common.util.DateTimeUtils;

@Repository
public class SurveyRepositoryImpl extends CategoryComplexRepositoryImpl<Survey, I_SurveyHeader, I_Survey>
        implements SurveyRepository {

    @Override
    protected Sort getSort() {
        return new Sort(Direction.DESC, PO.COLUMNNAME_DRAFT);
    }
    
    @Override
    protected Survey convertToDomain(I_Survey write) {
        return new Survey(write);
    }
    
    @Override
    public List<I_Survey> getSurveysAvailable(String clientId) {
        SimpleDate today = DateTimeUtils.getToday();

        Criteria dateCriteria = CriteriaUtils.andOperator(
                Criteria.where(Survey.COLUMNNAME_START_DATE_VALUE).lte(today.getValue()),
                Criteria.where(Survey.COLUMNNAME_END_DATE_VALUE).gte(today.getValue()));

        Sort sort = new Sort(Direction.DESC, Survey.COLUMNNAME_START_DATE_VALUE);

        List<Survey> list = super._getList(clientId, false, true, dateCriteria, null, sort);
        
        return PO.convertList(I_Survey.class, list);
    }

    @Override
    public List<I_SurveyHeader> getSurveysStarted(String clientId, String search, PageSizeRequest pageSizeRequest) {
        SimpleDate today = DateTimeUtils.getToday();

        Criteria statedCriteria = Criteria.where(Survey.COLUMNNAME_START_DATE_VALUE).lte(today.getValue());
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Category.COLUMNNAME_SEARCH_NAME, search);

        Criteria criteria = CriteriaUtils.andOperator(statedCriteria, searchCriteria);

        Sort sort = new Sort(Direction.DESC, Survey.COLUMNNAME_START_DATE_VALUE);

        List<Survey> list = _getList(clientId, false, true, criteria, getPageable(pageSizeRequest), sort);
        
        return PO.convertList(I_SurveyHeader.class, list);
    }

    @Override
    public long countSurveysStarted(String clientId, String search) {
        SimpleDate today = DateTimeUtils.getToday();

        Criteria statedCriteria = Criteria.where(Survey.COLUMNNAME_START_DATE_VALUE).lte(today.getValue());
        Criteria searchCriteria = CriteriaUtils.getSearchLikeCriteria(Category.COLUMNNAME_SEARCH_NAME, search);

        Criteria criteria = CriteriaUtils.andOperator(statedCriteria, searchCriteria);

        return _count(clientId, false, true, criteria);
    }

}
