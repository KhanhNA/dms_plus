package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Target;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.TargetRepository;
import com.viettel.repository.common.domain.category.I_Target;

@Repository
public class TargetRepositoryImpl extends BasicRepositoryImpl<Target, I_Target, I_Target> implements TargetRepository {

    @Override
    protected Target convertToDomain(I_Target write) {
        return new Target(write);
    }
    
    @Override
    public List<I_Target> getTargetsBySalesmen(String clientId, Collection<String> salesmanIds, int month, int year,
            boolean sortBySalesmanName) {
        Criteria dateCriteria = CriteriaUtils.andOperator(Criteria.where(Target.COLUMNNAME_MONTH).is(month),
                Criteria.where(Target.COLUMNNAME_YEAR).is(year));

        Criteria salesmanCriteria = Criteria.where(Target.COLUMNNAME_SALESMAN_ID).in(PO.convertIds(salesmanIds));

        Sort sort = null;
        if (sortBySalesmanName) {
            sort = new Sort(Target.COLUMNNAME_SALESMAN_NAME);
        }

        List<Target> list = super._getList(clientId, false, true,
                CriteriaUtils.andOperator(salesmanCriteria, dateCriteria), null, sort);

        return PO.convertList(I_Target.class, list);
    }

    @Override
    public boolean existsTargetBySalesman(String clientId, String salesmanId, int month, int year) {
        Criteria dateCriteria = CriteriaUtils.andOperator(Criteria.where(Target.COLUMNNAME_MONTH).is(month),
                Criteria.where(Target.COLUMNNAME_YEAR).is(year));

        Criteria salesmanCriteria = Criteria.where(Target.COLUMNNAME_SALESMAN_ID).is(PO.convertId(salesmanId));

        return super._exists(clientId, false, true, CriteriaUtils.andOperator(salesmanCriteria, dateCriteria));
    }

    @Override
    public I_Target getTargetBySalesman(String clientId, String salesmanId, int month, int year) {
        Criteria dateCriteria = CriteriaUtils.andOperator(Criteria.where(Target.COLUMNNAME_MONTH).is(month),
                Criteria.where(Target.COLUMNNAME_YEAR).is(year));

        Criteria salesmanCriteria = Criteria.where(Target.COLUMNNAME_SALESMAN_ID).is(PO.convertId(salesmanId));

        return super._getFirst(clientId, false, true, CriteriaUtils.andOperator(salesmanCriteria, dateCriteria), null);
    }

}
