package com.viettel.persistence.mongo.repository.impl;

import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.UOM;
import com.viettel.repository.common.UOMRepository;
import com.viettel.repository.common.domain.common.I_Category;

@Repository
public class UOMRepositoryImpl extends CategoryRepositoryImpl<UOM, I_Category> implements UOMRepository {

    @Override
    protected UOM convertToDomain(I_Category write) {
        return new UOM(write);
    }

}
