package com.viettel.persistence.mongo.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.User;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.DistributorRepository;
import com.viettel.repository.common.RegionRepository;
import com.viettel.repository.common.UserRepository;
import com.viettel.repository.common.domain.category.I_Distributor;
import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.common.domain.data.I_Region;
import com.viettel.repository.common.domain.user.I_User;
import com.viettel.repository.common.domain.user.I_User.Role;
import com.viettel.repository.common.entity.PageSizeRequest;

@Repository
public class UserRepositoryImpl extends BasicRepositoryImpl<User, I_User, I_User> implements UserRepository {

	@Autowired
	private DistributorRepository distributorRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	protected User convertToDomain(I_User write) {
		return new User(write);
	}

	@Override
	public List<I_User> getAll(String clientId) {
		List<User> list = super._getList(clientId, false, true, null, null, null);
		return PO.convertList(I_User.class, list);
	}

	@Override
	public I_User getDefaultAdmin(String clientId) {
		Criteria defaultAdmin = Criteria.where(User.COLUMNNAME_DEFAULT_ADMIN).is(true);
		return super._getFirst(clientId, null, null, defaultAdmin, null);
	}

	@Override
	public List<I_User> getUserByRole(String clientId, String role) {
		Assert.notNull(role);
		Criteria roleCriteria = Criteria.where(User.COLUMNNAME_ROLE).is(role);
		List<User> list = super._getList(clientId, false, true, roleCriteria, null, null);
		return PO.convertList(I_User.class, list);
	}

	@Override
	public List<I_User> getUserByRole(String clientId, String role, Collection<String> accessibleDistributorIds) {
		Assert.notNull(role);
		Criteria roleCriteria = Criteria.where(User.COLUMNNAME_ROLE).is(role);
		List<User> users = super._getList(clientId, false, true, roleCriteria, null, null);
		List<User> list = new ArrayList<>();
		if (accessibleDistributorIds != null) {
			if (!accessibleDistributorIds.isEmpty()) {
				for (User user : users) {
					Set<String> distributorIds = PO.convertObjectIds(user.getDistributorIds());
					if (distributorIds != null) {
						if (CollectionUtils.containsAny(accessibleDistributorIds, distributorIds)) {
							list.add(user);
						}
						// if (Collections.disjoint(accessibleDistributorIds,
						// distributorIds)) {
						// continue;
						// }
						// list.add(user);
					}
				}
			}
		}
		return PO.convertList(I_User.class, list);
	}

	@Override
	public List<I_Distributor> getDistributorsOfManager(String clientId, String id) {
		User user = super._getById(clientId, false, true, id);

		if (user.getRole().equals(Role.ADMIN)) {
			return distributorRepository.getAll(clientId, null);
		} else if (user.getRole().equals(Role.ADVANCE_SUPERVISOR)) {
			if (user.getRegionIds() == null || user.getRegionIds().isEmpty()) {
				return Collections.emptyList();
			} else {
				List<I_Region> regions = regionRepository.getListByIds(clientId,
						PO.convertObjectIds(user.getRegionIds()));
				if (regions == null || regions.isEmpty()) {
					return Collections.emptyList();
				}
				List<String> distributorIds = new ArrayList<>();
				for (I_Region region : regions) {
					distributorIds = getDistributorForRegionRecursively(clientId, region, distributorIds);
				}
				return distributorRepository.getListByIds(clientId, distributorIds);
			}
		} else if (user.getRole().equals(Role.OBSERVER) || user.getRole().equals(Role.SUPERVISOR)) {
			if (user.getDistributorIds() == null || user.getDistributorIds().isEmpty()) {
				return Collections.emptyList();
			} else {
				return distributorRepository.getListByIds(clientId, PO.convertObjectIds(user.getDistributorIds()));
			}
		} else if (user.getRole().equals(Role.DISTRIBUTOR_ADMIN)) {
			I_Distributor distributor = distributorRepository.getById(clientId, id);
			Assert.notNull(distributor);
			return Collections.singletonList(distributor);
		} else {
			return Collections.emptyList();
		}
	}

	public List<String> getDistributorForRegionRecursively(String clientId, I_Region region,
			List<String> distributorIds) {
		Assert.notNull(distributorIds);
		Assert.notNull(region);
		Set<I_CategoryEmbed> distributors = region.getDistributors();
		if (distributors != null && !distributors.isEmpty()) {
			for (I_CategoryEmbed dis : distributors) {
				distributorIds.add(dis.getId());
			}
		}
		List<I_CategoryEmbed> children = region.getChildren();
		for (I_CategoryEmbed child : children) {
			I_Region childRegion = regionRepository.getById(clientId, false, true, child.getId());
			distributorIds = getDistributorForRegionRecursively(clientId, childRegion, distributorIds);
		}
		return distributorIds;

	}

	@Override
	public List<I_Region> getRegionsOfManager(String clientId, String id) {
		User user = super._getById(clientId, false, true, id);
		if (user.getRole().equals(Role.ADVANCE_SUPERVISOR)) {
			if (user.getRegionIds() == null || user.getRegionIds().isEmpty()) {
				return Collections.emptyList();
			} else {
				return regionRepository.getListByIds(clientId, PO.convertObjectIds(user.getRegionIds()));
			}
		} else {
			return Collections.emptyList();
		}
	}

	@Override
	public boolean setDistributorsOfManager(String clientId, String id, Collection<String> distributorIds) {
		User user = super._getById(clientId, false, true, id);

		if (user.getRole().equals(Role.OBSERVER) || user.getRole().equals(Role.SUPERVISOR)) {
			if (distributorIds == null || distributorIds.isEmpty()) {
				user.setDistributorIds(Collections.emptySet());
			} else {
				user.setDistributorIds(new HashSet<>(PO.convertIds(distributorIds)));
			}

			super._save(clientId, user);
			return true;
		}

		return false;
	}

	@Override
	public boolean setRegionsOfManager(String clientId, String id, Collection<String> regionIds) {
		User user = super._getById(clientId, false, true, id);

		if (user.getRole().equals(Role.ADVANCE_SUPERVISOR)) {
			if (regionIds == null || regionIds.isEmpty()) {
				user.setRegionIds(Collections.emptySet());
			} else {
				user.setRegionIds(new HashSet<>(PO.convertIds(regionIds)));
			}

			super._save(clientId, user);
			return true;
		}

		return false;
	}

	@Override
	public List<I_User> getDistributorAdmins(String clientId, Collection<String> distributorIds) {
		Criteria roleCriteria = Criteria.where(User.COLUMNNAME_ROLE).is(Role.DISTRIBUTOR_ADMIN);

		Criteria distributorCriteria = null;
		if (distributorIds != null) {
			distributorCriteria = Criteria.where(User.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
		}

		Criteria criteria = CriteriaUtils.andOperator(roleCriteria, distributorCriteria);

		List<User> list = super._getList(clientId, false, true, criteria, null, null);
		return PO.convertList(I_User.class, list);
	}

	@Override
	public List<I_User> getSalesmen(String clientId, Collection<String> distributorIds) {
		Assert.notNull(distributorIds);

		if (distributorIds.isEmpty()) {
			return Collections.emptyList();
		}

		Criteria roleCriteria = Criteria.where(User.COLUMNNAME_ROLE).is(Role.SALESMAN);

		Criteria distributorCriteria = Criteria.where(User.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));

		Criteria criteria = CriteriaUtils.andOperator(roleCriteria, distributorCriteria);

		List<User> list = super._getList(clientId, false, true, criteria, null, null);
		return PO.convertList(I_User.class, list);
	}

	@Override
	public boolean updateVanSalesStatus(String clientId, Collection<String> ids, boolean status) {
		if (ids != null && !ids.isEmpty()) {
			Update update = new Update();
			update.set(User.COLUMNNAME_VAN_SALES, status);
			return super._updateMulti(clientId, null, null, ids, update) == ids.size();
		}
		return false;
	}

	@Override
	public List<I_User> getList(String clientId, Boolean draft, Boolean active, String role, String distributorId,
			String searchFullname, String searchUsername, PageSizeRequest pageSizeRequest, boolean sortByFullname) {
		Criteria noDefaultAdmin = Criteria.where(User.COLUMNNAME_DEFAULT_ADMIN).ne(true);

		Criteria roleCriteria = null;
		if (role != null) {
			roleCriteria = CriteriaUtils.getSearchLikeCriteria(User.COLUMNNAME_ROLE, role);
		}

		Criteria distributorCriteria = null;
		if (distributorId != null) {
			distributorCriteria = Criteria.where(User.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
		}

		Criteria searchFullnameCriteria = CriteriaUtils.getSearchLikeCriteria(User.COLUMNNAME_SEARCH_FULLNAME,
				searchFullname);
		Criteria searchUsernameCriteria = CriteriaUtils.getSearchLikeCriteria(User.COLUMNNAME_USERNAME, searchUsername);
		Criteria searchCriteria = CriteriaUtils.orOperator(searchFullnameCriteria, searchUsernameCriteria);

		Criteria criteria = CriteriaUtils.andOperator(noDefaultAdmin, roleCriteria, distributorCriteria,
				searchCriteria);

		Sort sort = null;
		if (sortByFullname) {
			sort = new Sort(User.COLUMNNAME_FULLNAME);
		}

		List<User> list = _getList(clientId, draft, active, criteria, getPageable(pageSizeRequest), sort);
		return PO.convertList(I_User.class, list);
	}

	@Override
	public long count(String clientId, Boolean draft, Boolean active, String role, String distributorId,
			String searchFullname, String searchUsername) {
		Criteria noDefaultAdmin = Criteria.where(User.COLUMNNAME_DEFAULT_ADMIN).ne(true);

		Criteria roleCriteria = null;
		if (role != null) {
			roleCriteria = CriteriaUtils.getSearchLikeCriteria(User.COLUMNNAME_ROLE, role);
		}

		Criteria distributorCriteria = null;
		if (distributorId != null) {
			distributorCriteria = Criteria.where(User.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
		}

		Criteria searchFullnameCriteria = CriteriaUtils.getSearchLikeCriteria(User.COLUMNNAME_SEARCH_FULLNAME,
				searchFullname);
		Criteria searchUsernameCriteria = CriteriaUtils.getSearchLikeCriteria(User.COLUMNNAME_USERNAME, searchUsername);
		Criteria searchCriteria = CriteriaUtils.orOperator(searchFullnameCriteria, searchUsernameCriteria);

		Criteria criteria = CriteriaUtils.andOperator(noDefaultAdmin, roleCriteria, distributorCriteria,
				searchCriteria);

		return _count(clientId, draft, active, criteria);
	}

	@Override
	public boolean checkUsernameExist(String clientId, String id, String username) {
		Criteria notMe = null;
		if (id != null) {
			notMe = Criteria.where(PO.COLUMNNAME_ID).ne(id);
		}

		Assert.notNull(username);
		Criteria usernameCriteria = CriteriaUtils.getSearchExactCriteria(User.COLUMNNAME_USERNAME, username);

		Criteria criteria = CriteriaUtils.andOperator(notMe, usernameCriteria);

		return super._exists(clientId, null, null, criteria);
	}

	@Override
	public boolean checkFullnameExist(String clientId, String id, String fullname) {
		Criteria notMe = null;
		if (id != null) {
			notMe = Criteria.where(PO.COLUMNNAME_ID).ne(id);
		}

		Assert.notNull(fullname);
		Criteria fullnameCriteria = CriteriaUtils.getSearchExactCriteria(User.COLUMNNAME_FULLNAME, fullname);

		Criteria criteria = CriteriaUtils.andOperator(notMe, fullnameCriteria);

		return super._exists(clientId, null, null, criteria);
	}

	@Override
	public I_User findByUsernameFull(String usernameFull) {
		Assert.notNull(usernameFull);
		usernameFull = usernameFull.toLowerCase();

		Query query = new Query()
				.addCriteria(CriteriaUtils.andOperator(Criteria.where(User.COLUMNNAME_USERNAME_FULL).is(usernameFull),
						Criteria.where(PO.COLUMNNAME_ACTIVE).is(true).and(PO.COLUMNNAME_DRAFT).is(false)));

		return mongoTemplate.findOne(query, User.class);
	}

	@Override
	public boolean changePassword(String clientId, String id, String password) {
		Assert.notNull(password);

		User user = super._getById(clientId, null, null, id);
		if (user != null) {
			user.setPassword(password);
			super._save(clientId, user);
			return true;
		}

		return false;
	}

}
