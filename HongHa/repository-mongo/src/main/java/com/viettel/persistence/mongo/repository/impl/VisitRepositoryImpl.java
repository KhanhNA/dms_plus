package com.viettel.persistence.mongo.repository.impl;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Visit;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.VisitRepository;
import com.viettel.repository.common.domain.data.I_VisitHeader;
import com.viettel.repository.common.domain.data.I_VisitPhoto;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.common.entity.SimpleDate.Period;

@Repository
public class VisitRepositoryImpl extends AbstractVisitAndOrderRepository<Visit> implements VisitRepository {

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isVisitCriteria = Criteria.where(Visit.COLUMNNAME_IS_VISIT).is(true);
        Criteria visitedCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITED);
        return CriteriaUtils.andOperator(isVisitCriteria, visitedCriteria);
    }

    @Override
    public List<I_VisitHeader> getVisitByDistributor(String clientId, String distributorId, String salesmanId,
            String customerId, Period period, PageSizeRequest pageSizeRequest, boolean sortAsc) {
        Assert.notNull(distributorId);
        Assert.notNull(period);

        Criteria salesmanCriteria = null;
        if (salesmanId != null) {
            salesmanCriteria = Criteria.where(Visit.COLUMNNAME_SALESMAN_ID).is(PO.convertId(salesmanId));
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));
        }

        Criteria criteria = CriteriaUtils.andOperator(salesmanCriteria, customerCriteria);

        List<Visit> list = super._getList(clientId, distributorId, period, criteria, getPageable(pageSizeRequest),
                sortAsc);

        return PO.convertList(I_VisitHeader.class, list);
    }

    @Override
    public long countVisitedsByDistributor(String clientId, String distributorId, String salesmanId, String customerId,
            Period period) {
        Assert.notNull(distributorId);
        Assert.notNull(period);

        Criteria salesmanCriteria = null;
        if (salesmanId != null) {
            salesmanCriteria = Criteria.where(Visit.COLUMNNAME_SALESMAN_ID).is(PO.convertId(salesmanId));
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));
        }

        Criteria criteria = CriteriaUtils.andOperator(salesmanCriteria, customerCriteria);

        return super._count(clientId, distributorId, period, criteria);
    }

    @Override
    public List<I_VisitPhoto> getVisitPhotoBySalesman(String clientId, String distributorId, String salesmanId,
            Period period) {
        Assert.notNull(distributorId);
        Assert.notNull(salesmanId);
        Assert.notNull(period);

        Criteria visitCriteria = Criteria.where(Visit.COLUMNNAME_IS_VISIT).is(true);
        Criteria visitedCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITED);
        Criteria hasPhoto = Criteria.where(Visit.COLUMNNAME_PHOTO).ne(null);
        Criteria salesmanCriteria = Criteria.where(Visit.COLUMNNAME_SALESMAN_ID).is(PO.convertId(salesmanId));

        Criteria criteria = CriteriaUtils.andOperator(visitCriteria, visitedCriteria, hasPhoto, salesmanCriteria);

        List<Visit> list = _getList(clientId, distributorId, period, criteria, null, true);

        return PO.convertList(I_VisitPhoto.class, list);
    }

}
