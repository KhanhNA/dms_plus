package com.viettel.persistence.mongo.repository.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Visit;
import com.viettel.persistence.mongo.domain.entity.VisitSummary;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.VisitSummaryRepository;
import com.viettel.repository.common.domain.data.I_VisitSummary;
import com.viettel.repository.common.entity.SimpleDate.Period;

@Repository
public class VisitSummaryRepositoryImpl implements VisitSummaryRepository {

    private static final Criteria IS_VISIT_CRITERIA = Criteria.where(Visit.COLUMNNAME_IS_VISIT).is(true);
    private static final Criteria VISITED_CRITERIA = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS)
            .is(Visit.VISIT_STATUS_VISITED);

    //@formatter:off
    private static final String REDUCE_FUNCTION = "" 
            + "function (key, values) {" 
            + "    var result = { "
            + "        nbVisit: 0, " 
            + "        nbVisitWithOrder: 0, " 
            + "        nbVisitErrorDuration: 0, " 
            + "        nbVisitErrorPosition: 0 " 
            + "    };" 
            + "    "
            + "    for(var i = 0; i < values.length; i++) {" 
            + "        var value = values[i];"
            + "        result.nbVisit += value.nbVisit == null ? 0 : value.nbVisit;"  
            + "        result.nbVisitWithOrder += value.nbVisitWithOrder == null ? 0 : value.nbVisitWithOrder;"  
            + "        result.nbVisitErrorDuration += value.nbVisitErrorDuration == null ? 0 : value.nbVisitErrorDuration;" 
            + "        result.nbVisitErrorPosition += value.nbVisitErrorPosition == null ? 0 : value.nbVisitErrorPosition;" 
            + "    };" 
            + "    " 
            + "    return result;" 
            + "}";
    
    private static final String EMIT_VALUE = "" 
            + "        { " 
            + "            nbVisit: 1, "
            + "            nbVisitWithOrder: (this.isOrder ? 1 : 0), " 
            + "            nbVisitErrorDuration: (this.errorDuration ? 1 : 0), "
            + "            nbVisitErrorPosition: (this.locationStatus == 0 ? 0 : 1) " 
            + "        }";

    //@formatter:on

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public Map<String, I_VisitSummary> getVisitSummaryDaily(String clientId, Collection<String> distributorIds,
            Period period) {
        Assert.notNull(clientId);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return Collections.emptyMap();
        }

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_CREATED_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                IS_VISIT_CRITERIA, VISITED_CRITERIA);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    var month = this.startTime.month + 1;");
        mapFn.append("    month = month > 9 ? month.toString() : '0' + month.toString();");
        mapFn.append("    ");
        mapFn.append("    var date = this.startTime.date;");
        mapFn.append("    date = date > 9 ? date.toString() : '0' + date.toString();");
        mapFn.append("    ");
        mapFn.append("    var isoDate = this.startTime.year + '-' + month + '-' + date;");
        mapFn.append("    ");
        mapFn.append("    emit(");
        mapFn.append("        isoDate, " + EMIT_VALUE);
        mapFn.append("    );");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultDaily> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultDaily.class);

        List<MapReduceResultDaily> mapReduceResultDailies = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultDailies, mapReduceResults.iterator());

        // AGGRAGATION
        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation project = Aggregation.project(Aggregation.bind("date", "startTime.isoDate")
                .and("createdBy._id", "createdBy._id").and("customer._id", "customer._id"));
        AggregationOperation group = Aggregation.group("date").addToSet("createdBy._id").as("salesmanIds")
                .addToSet("customer._id").as("customerIds");
        Aggregation aggregation = Aggregation.newAggregation(match, project, group);

        AggregationResults<AggregationResultDaily> aggregationResults = mongoTemplate.aggregate(aggregation,
                "VisitAndOrder", AggregationResultDaily.class);
        List<AggregationResultDaily> aggregationResultDailies = aggregationResults.getMappedResults();

        HashMap<String, Integer> nbSalesmanByDate = new HashMap<>();
        HashMap<String, Integer> nbCustomerByDate = new HashMap<>();
        for (AggregationResultDaily aggregationResultDaily : aggregationResultDailies) {
            nbSalesmanByDate.put(aggregationResultDaily.getId(), aggregationResultDaily.getNbSalesman());
            nbCustomerByDate.put(aggregationResultDaily.getId(), aggregationResultDaily.getNbCustomer());
        }

        HashMap<String, I_VisitSummary> map = new HashMap<>();
        for (MapReduceResultDaily mapReduceResultDaily : mapReduceResultDailies) {
            VisitSummary visitSummary = mapReduceResultDaily.getValue();
            visitSummary.setNbSalesman(nbSalesmanByDate.get(mapReduceResultDaily.getId()));
            visitSummary.setNbCustomer(nbCustomerByDate.get(mapReduceResultDaily.getId()));

            map.put(mapReduceResultDaily.getId(), visitSummary);
        }

        return map;
    }

    @Override
    public Map<String, I_VisitSummary> getVisitSummaryBySalesman(String clientId, String distributorId, Period period) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_START_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                IS_VISIT_CRITERIA, VISITED_CRITERIA);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    emit(");
        mapFn.append("        this.createdBy._id, " + EMIT_VALUE);
        mapFn.append("    );");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultById> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultById.class);

        List<MapReduceResultById> mapReduceResultByIds = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultByIds, mapReduceResults.iterator());

        // AGGRAGATION
        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation project = Aggregation
                .project(Aggregation.bind("createdBy._id", "createdBy._id").and("customer._id", "customer._id"));
        AggregationOperation group = Aggregation.group("createdBy._id").addToSet("customer._id").as("customerIds");
        Aggregation aggregation = Aggregation.newAggregation(match, project, group);

        AggregationResults<AggregationResultById> aggregationResults = mongoTemplate.aggregate(aggregation,
                "VisitAndOrder", AggregationResultById.class);
        List<AggregationResultById> aggregationResultByIds = aggregationResults.getMappedResults();

        HashMap<ObjectId, Integer> nbCustomerByDate = new HashMap<>();
        for (AggregationResultById aggregationResultById : aggregationResultByIds) {
            nbCustomerByDate.put(aggregationResultById.getId(), aggregationResultById.getNbCustomer());
        }

        HashMap<String, I_VisitSummary> map = new HashMap<>();
        for (MapReduceResultById mapReduceResultById : mapReduceResultByIds) {
            VisitSummary visitSummary = mapReduceResultById.getValue();
            visitSummary.setNbCustomer(nbCustomerByDate.get(mapReduceResultById.getId()));

            map.put(mapReduceResultById.getId().toString(), visitSummary);
        }

        return map;
    }

    @Override
    public Map<String, I_VisitSummary> getVisitSummaryByDistributor(String clientId, Collection<String> distributorIds,
            Period period) {
        Assert.notNull(clientId);
        Assert.notNull(distributorIds);

        if (distributorIds.isEmpty()) {
            return Collections.emptyMap();
        }

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).in(PO.convertIds(distributorIds));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_START_TIME_VALUE, period);

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                IS_VISIT_CRITERIA, VISITED_CRITERIA);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    emit(");
        mapFn.append("        this.distributor._id, " + EMIT_VALUE);
        mapFn.append("    );");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultById> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultById.class);

        List<MapReduceResultById> mapReduceResultByIds = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultByIds, mapReduceResults.iterator());

        // AGGRAGATION
        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation project = Aggregation.project(Aggregation.bind("distributor._id", "distributor._id")
                .and("createdBy._id", "createdBy._id").and("customer._id", "customer._id"));
        AggregationOperation group = Aggregation.group("distributor._id").addToSet("createdBy._id").as("salesmanIds")
                .addToSet("customer._id").as("customerIds");
        Aggregation aggregation = Aggregation.newAggregation(match, project, group);

        AggregationResults<AggregationResultById> aggregationResults = mongoTemplate.aggregate(aggregation,
                "VisitAndOrder", AggregationResultById.class);
        List<AggregationResultById> aggregationResultByIds = aggregationResults.getMappedResults();

        HashMap<ObjectId, Integer> nbSalesmanByDate = new HashMap<>();
        HashMap<ObjectId, Integer> nbCustomerByDate = new HashMap<>();
        for (AggregationResultById aggregationResultById : aggregationResultByIds) {
            nbSalesmanByDate.put(aggregationResultById.getId(), aggregationResultById.getNbSalesman());
            nbCustomerByDate.put(aggregationResultById.getId(), aggregationResultById.getNbCustomer());
        }

        HashMap<String, I_VisitSummary> map = new HashMap<>();
        for (MapReduceResultById mapReduceResultById : mapReduceResultByIds) {
            VisitSummary visitSummary = mapReduceResultById.getValue();
            visitSummary.setNbSalesman(nbSalesmanByDate.get(mapReduceResultById.getId()));
            visitSummary.setNbCustomer(nbCustomerByDate.get(mapReduceResultById.getId()));

            map.put(mapReduceResultById.getId().toString(), visitSummary);
        }

        return map;
    }

    @Override
    public I_VisitSummary getVisitSummary(String clientId, String distributorId, String createdUserId,
            String customerId, Period period) {
        Assert.notNull(clientId);
        Assert.notNull(distributorId);

        Criteria clientCriteria = Criteria.where(PO.COLUMNNAME_CLIENT_ID).is(PO.convertId(clientId));
        Criteria ditributorCriteria = Criteria.where(Visit.COLUMNNAME_DISTRIBUTOR_ID).is(PO.convertId(distributorId));
        Criteria periodCriteria = CriteriaUtils.getPeriodCriteria(Visit.COLUMNNAME_START_TIME_VALUE, period);

        Criteria createdCriteria = null;
        if (createdUserId != null) {
            createdCriteria = Criteria.where(Visit.COLUMNNAME_CREATED_BY_ID).is(PO.convertId(createdUserId));
        }

        Criteria customerCriteria = null;
        if (customerId != null) {
            customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));
        }

        Criteria criteria = CriteriaUtils.andOperator(clientCriteria, ditributorCriteria, periodCriteria,
                createdCriteria, customerCriteria, IS_VISIT_CRITERIA, VISITED_CRITERIA);

        Query query = new Query();
        query.addCriteria(criteria);

        StringBuilder mapFn = new StringBuilder();
        mapFn.append("function () {");
        mapFn.append("    emit(");
        mapFn.append("        this.distributor._id, " + EMIT_VALUE);
        mapFn.append("    );");
        mapFn.append("}");

        // REDUCE
        MapReduceResults<MapReduceResultById> mapReduceResults = mongoTemplate.mapReduce(query, "VisitAndOrder",
                mapFn.toString(), REDUCE_FUNCTION, MapReduceResultById.class);

        List<MapReduceResultById> mapReduceResultByIds = new ArrayList<>();
        CollectionUtils.addAll(mapReduceResultByIds, mapReduceResults.iterator());

        // AGGRAGATION
        AggregationOperation match = Aggregation.match(criteria);
        AggregationOperation project = Aggregation.project(Aggregation.bind("distributor._id", "distributor._id")
                .and("createdBy._id", "createdBy._id").and("customer._id", "customer._id"));
        AggregationOperation group = Aggregation.group("distributor._id").addToSet("createdBy._id").as("salesmanIds")
                .addToSet("customer._id").as("customerIds");
        Aggregation aggregation = Aggregation.newAggregation(match, project, group);

        AggregationResults<AggregationResultById> aggregationResults = mongoTemplate.aggregate(aggregation,
                "VisitAndOrder", AggregationResultById.class);
        List<AggregationResultById> aggregationResultByIds = aggregationResults.getMappedResults();

        HashMap<ObjectId, Integer> nbSalesmanByDate = new HashMap<>();
        HashMap<ObjectId, Integer> nbCustomerByDate = new HashMap<>();
        for (AggregationResultById aggregationResultById : aggregationResultByIds) {
            nbSalesmanByDate.put(aggregationResultById.getId(), aggregationResultById.getNbSalesman());
            nbCustomerByDate.put(aggregationResultById.getId(), aggregationResultById.getNbCustomer());
        }

        HashMap<String, I_VisitSummary> map = new HashMap<>();
        for (MapReduceResultById mapReduceResultById : mapReduceResultByIds) {
            VisitSummary visitSummary = mapReduceResultById.getValue();
            visitSummary.setNbSalesman(nbSalesmanByDate.get(mapReduceResultById.getId()));
            visitSummary.setNbCustomer(nbCustomerByDate.get(mapReduceResultById.getId()));

            map.put(mapReduceResultById.getId().toString(), visitSummary);
        }

        if (map.containsKey(distributorId)) {
            return map.get(distributorId);
        } else {
            return new VisitSummary();
        }
    }

    // #S PRIVATE CLASS
    public static class MapReduceResultDaily implements Serializable {

        private static final long serialVersionUID = -1652482571529294287L;

        private String id;
        private VisitSummary value;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public VisitSummary getValue() {
            return value;
        }

        public void setValue(VisitSummary value) {
            this.value = value;
        }

    }

    public static class MapReduceResultById implements Serializable {

        private static final long serialVersionUID = -1652482571529294287L;

        private ObjectId id;
        private VisitSummary value;

        public ObjectId getId() {
            return id;
        }

        public void setId(ObjectId id) {
            this.id = id;
        }

        public VisitSummary getValue() {
            return value;
        }

        public void setValue(VisitSummary value) {
            this.value = value;
        }

    }

    private static class AggregationResultDaily implements Serializable {

        private static final long serialVersionUID = 6632906021427053207L;

        private String id;
        private List<ObjectId> salesmanIds;
        private List<ObjectId> customerIds;

        public String getId() {
            return id;
        }

        public int getNbSalesman() {
            return salesmanIds == null ? 0 : salesmanIds.size();
        }

        public int getNbCustomer() {
            return customerIds == null ? 0 : customerIds.size();
        }

    }

    private static class AggregationResultById implements Serializable {

        private static final long serialVersionUID = 6632906021427053207L;

        private ObjectId id;
        private List<ObjectId> salesmanIds;
        private List<ObjectId> customerIds;

        public ObjectId getId() {
            return id;
        }

        public int getNbSalesman() {
            return salesmanIds == null ? 0 : salesmanIds.size();
        }

        public int getNbCustomer() {
            return customerIds == null ? 0 : customerIds.size();
        }

    }
    // #E

}
