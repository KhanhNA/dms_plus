package com.viettel.persistence.mongo.repository.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import com.viettel.persistence.mongo.domain.PO;
import com.viettel.persistence.mongo.domain.Visit;
import com.viettel.persistence.mongo.util.CriteriaUtils;
import com.viettel.repository.common.VisitingRepository;
import com.viettel.repository.common.domain.data.I_Visit;
import com.viettel.repository.common.domain.data.I_VisitHeader;
import com.viettel.repository.common.entity.SimpleDate.Period;
import com.viettel.repository.common.util.DateTimeUtils;

@Repository
public class VisitingRepositoryImpl extends AbstractVisitAndOrderRepository<Visit> implements VisitingRepository {

    @Override
    protected Criteria getDefaultCriteria() {
        Criteria isVisitCriteria = Criteria.where(Visit.COLUMNNAME_IS_VISIT).is(true);
        return isVisitCriteria;
    }

    @Override
    public I_Visit getById(String clientId, String id) {
        return _getById(clientId, false, true, id);
    }

    @Override
    public List<I_Visit> getListByIds(String clientId, Collection<String> ids) {
        List<Visit> list = _getListByIds(clientId, false, true, ids);
        return PO.convertList(I_Visit.class, list);
    }

    @Override
    public I_VisitHeader getVisitHeaderByCustomerToday(String clientId, String distributorId, String customerId) {
        return getVisitByCustomerToday(clientId, distributorId, customerId);
    }

    @Override
    public I_Visit getVisitByCustomerToday(String clientId, String distributorId, String customerId) {
        Period todayPeriod = DateTimeUtils.getPeriodToday();

        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).is(PO.convertId(customerId));

        List<Visit> visits = super._getList(clientId, distributorId, todayPeriod, customerCriteria, null, false);
        if (visits == null || visits.isEmpty()) {
            return null;
        } else if (visits.size() == 1) {
            return visits.get(0);
        } else {
            for (Visit visit : visits) {
                if (visit.getVisitStatus() == Visit.VISIT_STATUS_VISITED) {
                    return visit;
                }
            }
            return visits.get(0);
        }
    }

    @Override
    public Map<String, I_VisitHeader> getVisitHeaderByCustomerTodayMap(String clientId, String distributorId,
            Collection<String> customerIds) {
        Period todayPeriod = DateTimeUtils.getPeriodToday();

        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(PO.convertIds(customerIds));

        List<Visit> visits = super._getList(clientId, distributorId, todayPeriod, customerCriteria, null, false);

        if (visits == null || visits.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<String, I_VisitHeader> results = new HashMap<String, I_VisitHeader>();
        for (Visit visit : visits) {
            I_VisitHeader tempVisit = results.get(visit.getCustomer().getId());
            if (tempVisit != null) {
                if (tempVisit.getVisitStatus() == Visit.VISIT_STATUS_VISITING
                        && visit.getVisitStatus() == Visit.VISIT_STATUS_VISITED) {
                    results.put(visit.getCustomer().getId(), visit);
                }
            } else {
                results.put(visit.getCustomer().getId(), visit);
            }

        }

        return results;
    }

    @Override
    public boolean checkVisiting(String clientId, String distributorId, Collection<String> customerIds) {
        Period todayPeriod = DateTimeUtils.getPeriodToday();

        Criteria customerCriteria = Criteria.where(Visit.COLUMNNAME_CUSTOMER_ID).in(PO.convertIds(customerIds));
        Criteria visitingCriteria = Criteria.where(Visit.COLUMNNAME_VISIT_STATUS).is(Visit.VISIT_STATUS_VISITING);

        Criteria criteria = CriteriaUtils.andOperator(customerCriteria, visitingCriteria);

        return super._exists(clientId, distributorId, todayPeriod, criteria);
    }

    @Override
    public I_Visit save(String clientId, I_Visit _visit) {
        Visit visit = new Visit(_visit);
        return super._save(clientId, visit);
    }

}
