package com.viettel.repository.mysql.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.viettel.repository.common.domain.common.I_Category;

@Entity
@Table(name = "AREA")
public class MArea extends MCategory {

    private static final long serialVersionUID = 1589628426879646377L;

    public MArea() {
        super();
    }

    public MArea(I_Category category) {
        super(category);
    }

    public MArea(MArea category) {
        super(category);
    }

}
