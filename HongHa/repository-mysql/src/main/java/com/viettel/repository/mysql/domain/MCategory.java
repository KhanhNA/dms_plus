package com.viettel.repository.mysql.domain;

import javax.persistence.Column;

import com.viettel.repository.common.domain.common.I_Category;

public class MCategory extends MPO {

    private static final long serialVersionUID = -306348862044837874L;

    public static final String CN_NAME = "name";
    public static final String CN_CODE = "code";
    
    public static final String CN_DISTRIBUTOR_ID = "distributorId";
    
    @Column(name = "NAME")
    private String name;
    
    @Column(name = "CODE")
    private String code;
    
    @Column(name = "DISTRIBUTOR_ID")
    private Long distributorId;
    
    @Column(name = "DISTRIBUTOR_NAME")
    private String distributorName;
    
    @Column(name = "DISTRIBUTOR_CODE")
    private String distributorCode;

    public MCategory() {
        super();
    }

    public MCategory(MCategory category) {
        super(category);

        this.name = category.getName();
        this.code = category.getCode();
        
        this.distributorId = category.getDistributorId();
        this.distributorName = category.getDistributorName();
        this.distributorCode = category.getDistributorCode();
    }

    public MCategory(I_Category category) {
        super(category);

        this.name = category.getName();
        this.code = category.getCode();
        
        if (category.getDistributor() != null) {
            this.distributorId = convertToLongId(category.getDistributor().getId());
            this.distributorName = category.getDistributor().getName();
            this.distributorCode = category.getDistributor().getCode();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(Long distributorId) {
        this.distributorId = distributorId;
    }

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    public String getDistributorCode() {
        return distributorCode;
    }

    public void setDistributorCode(String distributorCode) {
        this.distributorCode = distributorCode;
    }
    
}
