package com.viettel.repository.mysql.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.util.Assert;

import com.viettel.repository.common.domain.common.I_PO;

public class MPO implements Serializable {

    private static final long serialVersionUID = 1942713230513813413L;
    
    public static final String CN_ID = "id";
    public static final String CN_CLIENT_ID = "clientId";
    public static final String CN_DRAFT = "draft";
    public static final String CN_ACTIVE = "active";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "CLIENT_ID")
    private Long clientId;
    
    @Column(name = "DRAFT")
    private boolean draft;
    
    @Column(name = "ACTIVE")
    private boolean active;
    
    public MPO() {
        this.id = null;
        this.clientId = null;
        this.draft = false;
        this.active = true;
    }
    
    public MPO(MPO po) {
        Assert.notNull(po);
        
        this.id = po.getId();
        this.clientId = po.getClientId();
        this.draft = po.isDraft();
        this.active = po.isActive();
    }
    
    public MPO(I_PO po) {
        Assert.notNull(po);
        
        this.id = convertToLongId(po.getId());
        this.clientId = convertToLongId(po.getClientId());
        this.draft = po.isDraft();
        this.active = po.isActive();
    }
    
    protected Long convertToLongId(String id) {
        return id == null ? null : Long.valueOf(id);
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /******************** OBJECT'S METHODS ********************/
    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass().equals(this.getClass())) {
            MPO po = (MPO) obj;
            if (po.getId() != null && this.getId() != null) {
                return po.getId().equals(this.getId());
            }

            return super.equals(obj);
        }

        return false;
    }
    
    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }

    @Override
    public String toString() {
        return new StringBuilder(this.getClass().getSimpleName()).append("=").append(getId()).toString();
    }
    
    public boolean isClientRootFixed() {
        return false;
    }
    
    public boolean isClientRootInclude() {
        return false;
    }
    
}
