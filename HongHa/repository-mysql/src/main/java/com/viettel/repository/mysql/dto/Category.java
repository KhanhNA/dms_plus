package com.viettel.repository.mysql.dto;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.mysql.domain.MCategory;
import com.viettel.repository.mysql.dto.embed.CategoryEmbed;

public class Category extends PO implements I_Category {

    private static final long serialVersionUID = 6347306050567041289L;

    private String name;
    private String code;
    private CategoryEmbed distributor;
    
    public Category() {
        
    }
    
    public Category(Category category) {
        super(category);
        this.name = category.getName();
        this.code = category.getCode();
        this.distributor = category.getDistributor();
    }
    
    public Category(MCategory category) {
        super(category);
        this.name = category.getName();
        this.code = category.getCode();
        if (category.getDistributorId() != null) {
            CategoryEmbed distributor = new CategoryEmbed();
            distributor.setId(convertToStringId(category.getDistributorId()));
            distributor.setCode(category.getCode());
            distributor.setName(category.getName());
            this.distributor = distributor;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CategoryEmbed getDistributor() {
        return distributor;
    }

    public void setDistributor(CategoryEmbed distributor) {
        this.distributor = distributor;
    }

}
