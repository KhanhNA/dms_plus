package com.viettel.repository.mysql.dto;

import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.mysql.domain.MPO;

public class PO implements I_PO {

    private static final long serialVersionUID = 3579646323206623088L;

    private String id;
    private String clientId;
    private boolean draft;
    private boolean active;
    
    public PO() {
        
    }
    
    public PO(PO po) {
        this.id = po.getId();
        this.clientId = po.getClientId();
        this.draft = po.isDraft();
        this.active = po.isActive();
    }
    
    public PO(MPO entity) {
        this.id = convertToStringId(entity.getId());
        this.clientId = convertToStringId(entity.getClientId());
        this.draft = entity.isDraft();
        this.active = entity.isActive();
    }
    
    protected String convertToStringId(Long id) {
        return id == null ? null : String.valueOf(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
