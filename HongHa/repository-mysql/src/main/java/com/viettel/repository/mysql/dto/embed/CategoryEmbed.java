package com.viettel.repository.mysql.dto.embed;

import com.viettel.repository.common.domain.common.I_CategoryEmbed;
import com.viettel.repository.mysql.domain.MCategory;
import com.viettel.repository.mysql.dto.Category;

public class CategoryEmbed extends POEmbed implements I_CategoryEmbed {

    private static final long serialVersionUID = 3300536783836233880L;
    
    private String name;
    private String code;
    
    public CategoryEmbed() {
        super();
    }
    
    public CategoryEmbed(MCategory category) {
        super(category);
        this.name = category.getName();
        this.code = category.getCode();
    }
    
    public CategoryEmbed(Category category) {
        super(category);
        this.name = category.getName();
        this.code = category.getCode();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
