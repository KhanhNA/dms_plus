package com.viettel.repository.mysql.dto.embed;

import com.viettel.repository.common.domain.common.I_POEmbed;
import com.viettel.repository.mysql.domain.MPO;
import com.viettel.repository.mysql.dto.PO;

public class POEmbed implements I_POEmbed {

    private static final long serialVersionUID = 7873196129403383073L;
    
    private String id;
    
    public POEmbed() {
        
    }
    
    public POEmbed(PO po) {
        this.id = po.getId();
    }
    
    public POEmbed(MPO mpo) {
        this.id = convertToStringId(mpo.getId());
    }
    
    protected String convertToStringId(Long id) {
        return id == null ? null : String.valueOf(id);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
