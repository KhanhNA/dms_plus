package com.viettel.repository.mysql.repo;

import static org.springframework.data.jpa.repository.query.QueryUtils.toOrders;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.jpa.repository.JpaContext;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.viettel.repository.mysql.domain.MPO;
import com.viettel.repository.mysql.util.SpecificationUtils;

public abstract class AbstractRepository<E extends MPO> implements InitializingBean {
    
    public static final long CLIENT_ROOT_ID = 0l;
    
    private static final String ID_MUST_NOT_BE_NULL = "The given id must not be null!";
    
    private static final int DEFAULT_BATCH_SIZE = 10000;
    
    @Autowired
    protected JpaContext context;

    protected EntityManager entityManager;
    
    protected JpaEntityInformation<E, ?> entityInformation;
    
    protected Class<E> domainClass;
    
    protected String tableName;
    
    public AbstractRepository(Class<E> domainClass) {
        this.domainClass = domainClass;
    }
    
    @Override
    public void afterPropertiesSet() throws Exception {
        this.entityManager = context.getEntityManagerByManagedType(domainClass);
        this.entityInformation = JpaEntityInformationSupport.getEntityInformation(domainClass, entityManager);
        this.tableName = getTableName(entityManager, domainClass);
    }

    public E getById(Long clientId, Boolean draft, Boolean active, Long id) {
        
        Assert.notNull(id, ID_MUST_NOT_BE_NULL);
        
        TypedQuery<E> mainQuery = initQuery(clientId, draft, active, SpecificationUtils.equal(MPO.CN_ID, id), null, null);
        List<E> results = mainQuery.getResultList();
        
        return getFirst(results);
    }

    public E getFirst(Long clientId, Boolean draft, Boolean active, Specification<E> spec, Sort sort) {
        
        TypedQuery<E> mainQuery = initQuery(clientId, draft, active, spec, null, sort);
        List<E> results = mainQuery.getResultList();
        
        return getFirst(results);
    }

    public List<E> getList(Long clientId, Boolean draft, Boolean active, Specification<E> spec,
            Pageable pageable, Sort sort) {
        
        TypedQuery<E> mainQuery = initQuery(clientId, draft, active, spec, pageable, sort);
        return mainQuery.getResultList();
    }

    public Long count(Long clientId, Boolean draft, Boolean active, Specification<E> spec) {
        
        TypedQuery<Long> mainQuery = initCountQuery(clientId, draft, active, spec);
        return executeCountQuery(mainQuery);
    }

    public boolean exists(Long clientId, Boolean draft, Boolean active, Long id) {
        
        return this.exists(clientId, draft, active, SpecificationUtils.equal(MPO.CN_ID, id));
    }

    public boolean exists(Long clientId, Boolean draft, Boolean active, Specification<E> spec) {
        
        return count(clientId, draft, active, spec) == 1;
    }

    public E save(Long clientId, E domain) {
        Assert.notNull(domain);
        Assert.notNull(domain.getClientId());
        Assert.notNull(clientId);
        if (!domain.getClientId().equals(clientId)) {
            throw new IllegalArgumentException("client id not valid");
        }

//        if (domain instanceof MCategory) {
//            MCategory categoryDomain = (MCategory) domain;
//            categoryDomain.setSearchCode(searchCode);
//        }

        if (entityInformation.isNew(domain)) {
            entityManager.persist(domain);
        } else {
            domain = entityManager.merge(domain);
        }

        onChange(clientId);

        return domain;
    }

    public boolean delete(Long clientId, Long id) {
        Assert.notNull(id);

        E domain = getById(clientId, true, null, id);
        
        entityManager.remove(domain);

        onChange(clientId);

        return true;
    }

    public boolean delete(Long clientId, Collection<Long> ids) {
        Assert.notEmpty(ids);

        Collection<E> domains = getList(clientId, true, null, SpecificationUtils.in(MPO.CN_ID, ids), null, null);
        
        if (!CollectionUtils.isEmpty(domains)) {
            for (E domain : domains) {
                entityManager.remove(domain);
            }
        }

        onChange(clientId);

        return true;
    }

    public void insertBatch(Long clientId, Collection<E> domains) {
        Assert.notNull(clientId);
        Assert.notEmpty(domains);

        // CHECK
        for (E domain : domains) {
            Assert.notNull(domain);
            Assert.notNull(domain.getClientId());
            if (!domain.getClientId().equals(clientId)) {
                throw new IllegalArgumentException("client id not valid");
            }

//            if (domain instanceof MCategory) {
//
//            }
        }

        onChange(clientId);
        
        int i = 0;
        for (E domain : domains) {
            entityManager.persist(domain);i++;

            if (i % DEFAULT_BATCH_SIZE == 0) {
                entityManager.flush();
                entityManager.clear();
            }
        }
    }

    public int updateMulti(Long clientId, Boolean draft, Boolean active, Collection<Long> ids,
            Map<String, Object> updateMap) {
        
        Assert.notEmpty(ids);
        return this.updateMulti(clientId, draft, active, SpecificationUtils.in(MPO.CN_ID, ids), updateMap);
    }

    /**
     * Please make sure that specified {@link Specification} DO NOT use {@link CriteriaQuery} variable inside
     * {@link Specification#toPredicate(Root, CriteriaQuery, CriteriaBuilder)}. Otherwise, an 
     * {@link NullPointerException} will be thrown
     * 
     * @return the number of entities updated
     */
    public int updateMulti(Long clientId, Boolean draft, Boolean active, Specification<E> spec,
            Map<String, Object> updateMap) {
        
        Assert.notNull(spec);
        
        onChange(clientId);
        
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaUpdate<E> update = cb.createCriteriaUpdate(domainClass);
        
        Root<E> root = update.from(domainClass);

        for (Map.Entry<String, Object> updateEntry : updateMap.entrySet()) {
            update.set(updateEntry.getKey(), updateEntry.getValue());
        }

        update.where(spec.toPredicate(root, null, cb));
        return entityManager.createQuery(update).executeUpdate();
    }

    protected Map<Long, E> getMap(List<E> pos) {
        if (pos == null || pos.isEmpty()) {
            return Collections.emptyMap();
        }

        Map<Long, E> results = new HashMap<Long, E>();
        for (E po : pos) {
            results.put(po.getId(), po);
        }

        return results;
    }

    private E getFirst(List<E> results) {
        
        if (results != null && results.size() > 0) {
            return results.get(0);
        }
        return null;
    }

    protected Specification<E> getDefaultCriteria() {
        return null;
    }

    protected void onChange(Long clientId) {
        // DO NOTHING
    }

    /************************************************************************/

    /**************************** PRIVATE METHODS ***************************/
    /************************************************************************/
    protected final TypedQuery<E> initQuery(Long clientId, Boolean draft, Boolean active, Specification<E> spec, 
            Pageable pageable, Sort sort) {
        Assert.notNull(clientId);

        Specification<E> clientFilterSpec = null;
        if (isIncludeClientRoot()) {
            clientFilterSpec = Specifications
                    .<E>where(SpecificationUtils.equal(MPO.CN_CLIENT_ID, clientId))
                    .or(SpecificationUtils.<E>equal(MPO.CN_CLIENT_ID, CLIENT_ROOT_ID));
        } else {
            clientFilterSpec = SpecificationUtils.equal(MPO.CN_CLIENT_ID, clientId);
        }

        Specification<E> activeSpec = null;
        if (active != null) {
            activeSpec = SpecificationUtils.equal(MPO.CN_ACTIVE, active);
        }

        Specification<E> draftSpec = null;
        if (draft != null) {
            draftSpec = SpecificationUtils.equal(MPO.CN_DRAFT, draft);
        }

        Specification<E> mainSpecs = SpecificationUtils.and(clientFilterSpec, activeSpec, draftSpec,
                getDefaultCriteria(), spec);

        if (pageable != null) {
            if (sort != null) {
                pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
            }
            return getQuery(mainSpecs, pageable);
        }

        return getQuery(mainSpecs, sort);
    }
    
    protected final TypedQuery<Long> initCountQuery(Long clientId, Boolean draft, Boolean active, Specification<E> spec) {
        Assert.notNull(clientId);

        Specification<E> clientFilterSpec = null;
        if (isIncludeClientRoot()) {
            clientFilterSpec = Specifications
                    .<E>where(SpecificationUtils.equal(MPO.CN_CLIENT_ID, clientId))
                    .or(SpecificationUtils.<E>equal(MPO.CN_CLIENT_ID, CLIENT_ROOT_ID));
        } else {
            clientFilterSpec = SpecificationUtils.equal(MPO.CN_CLIENT_ID, clientId);
        }

        Specification<E> activeSpec = null;
        if (active != null) {
            activeSpec = SpecificationUtils.equal(MPO.CN_ACTIVE, active);
        }

        Specification<E> draftSpec = null;
        if (draft != null) {
            draftSpec = SpecificationUtils.equal(MPO.CN_DRAFT, draft);
        }

        Specification<E> mainSpecs = SpecificationUtils.and(clientFilterSpec, activeSpec, draftSpec,
                getDefaultCriteria(), spec);

        return getCountQuery(mainSpecs);
    }

    private boolean isIncludeClientRoot() {
//        return (domainClazz.isAnnotationPresent(ClientRootInclude.class));
        return false;
    }

    /**
     * Creates a new {@link TypedQuery} from the given {@link Specification}.
     * 
     * @param spec can be {@literal null}.
     * @param pageable can be {@literal null}.
     * @return
     */
    protected TypedQuery<E> getQuery(Specification<E> spec, Pageable pageable) {

        Sort sort = pageable == null ? null : pageable.getSort();
        return getQuery(spec, sort);
    }
    
    /**
     * Creates a {@link TypedQuery} for the given {@link Specification} and {@link Sort}.
     * 
     * @param spec can be {@literal null}.
     * @param sort can be {@literal null}.
     * @return
     */
    protected TypedQuery<E> getQuery(Specification<E> spec, Sort sort) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query = builder.createQuery(domainClass);

        Root<E> root = applySpecificationToCriteria(spec, query);
        query.select(root);

        if (sort != null) {
            query.orderBy(toOrders(sort, root, builder));
        }

        return entityManager.createQuery(query);
    }

    /**
     * Creates a new count query for the given {@link Specification}.
     * 
     * @param spec can be {@literal null}.
     * @return
     */
    protected TypedQuery<Long> getCountQuery(Specification<E> spec) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);

        Root<E> root = applySpecificationToCriteria(spec, query);

        if (query.isDistinct()) {
            query.select(builder.countDistinct(root));
        } else {
            query.select(builder.count(root));
        }

        return entityManager.createQuery(query);
    }

    /**
     * Applies the given {@link Specification} to the given {@link CriteriaQuery}.
     * 
     * @param spec can be {@literal null}.
     * @param query must not be {@literal null}.
     * @return
     */
    private <S> Root<E> applySpecificationToCriteria(Specification<E> spec, CriteriaQuery<S> query) {

        Assert.notNull(query);
        Root<E> root = query.from(domainClass);

        if (spec == null) {
            return root;
        }

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        Predicate predicate = spec.toPredicate(root, query, builder);

        if (predicate != null) {
            query.where(predicate);
        }

        return root;
    }

    /**
     * Executes a count query and transparently sums up all values returned.
     * 
     * @param query must not be {@literal null}.
     * @return
     */
    private static Long executeCountQuery(TypedQuery<Long> query) {

        Assert.notNull(query);

        List<Long> totals = query.getResultList();
        Long total = 0L;

        for (Long element : totals) {
            total += element == null ? 0 : element;
        }

        return total;
    }
    
    /**
     * Returns the table name for a given entity type in the {@link EntityManager}.
     */
    private <T> String getTableName(EntityManager em, Class<T> entityClass) {
        /*
         * Check if the specified class is present in the metamodel.
         * Throws IllegalArgumentException if not.
         */
        Metamodel meta = em.getMetamodel();
        EntityType<T> entityType = meta.entity(entityClass);

        //Check whether @Table annotation is present on the class.
        Table t = entityClass.getAnnotation(Table.class);

        return (t == null) ? entityType.getName().toUpperCase() : t.name();
    }
    
}
