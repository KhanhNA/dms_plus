package com.viettel.repository.mysql.repo;

import java.util.Collection;
import java.util.List;

import com.viettel.repository.mysql.domain.MPO;
import com.viettel.repository.mysql.util.SpecificationUtils;

public abstract class BasicRepository<E extends MPO> extends AbstractRepository<E> {

    public BasicRepository(Class<E> domainClass) {
        super(domainClass);
    }

    // DRAFT = FALSE & ACTIVE = TRUE
    public E getById(Long clientId, Long id) {
        return getById(clientId, false, true, id);
    }

    public List<E> getListByIds(Long clientId, Collection<Long> ids) {
        return getListByIds(clientId, false, true, ids);
    }

    // ALL
    @Override
    public E getById(Long clientId, Boolean draft, Boolean active, Long id) {
        return getById(clientId, draft, active, id);
    }

    public List<E> getListByIds(Long clientId, Boolean draft, Boolean active, Collection<Long> ids) {
        return getList(clientId, draft, active, SpecificationUtils.in(MPO.CN_ID, ids), null, null);
    }

    public boolean exists(Long clientId, Boolean draft, Boolean active, Long id) {
        return exists(clientId, draft, active, id);
    }

    public boolean exists(Long clientId, Boolean draft, Boolean active, Collection<Long> ids) {
        return exists(clientId, draft, active, SpecificationUtils.in(MPO.CN_ID, ids));
    }
    
}
