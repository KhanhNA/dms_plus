package com.viettel.repository.mysql.repo;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import com.viettel.repository.common.util.StringUtils;
import com.viettel.repository.mysql.domain.MCategory;
import com.viettel.repository.mysql.domain.MPO;
import com.viettel.repository.mysql.util.SpecificationUtils;

public class CategoryRepository<E extends MCategory> extends BasicRepository<E> {
    
    public CategoryRepository(Class<E> domainClass) {
        super(domainClass);
    }

    // DRAFT = FALSE & ACTIVE = TRUE
    public List<E> getAll(Long clientId, Long distributorId) {
        
        Specification<E> distributorSpec = null;
        if (distributorId != null) {
            distributorSpec = SpecificationUtils.equal(MCategory.CN_DISTRIBUTOR_ID, distributorId);
        }

        return getList(clientId, false, true, distributorSpec, null, null);
    }

    public List<E> getList(Long clientId, Boolean draft, Boolean active, Collection<Long> distributorIds,
            String searchName, String searchCode, Pageable pageable) {
        
        if (CollectionUtils.isEmpty(distributorIds)) {
            return Collections.emptyList();
        }
        
        Specification<E> distributorSpec = SpecificationUtils.in(MCategory.CN_DISTRIBUTOR_ID, distributorIds);
        
        Specification<E> nameSpec = null;
        if (!StringUtils.isEmpty(searchName)) {
            nameSpec = SpecificationUtils.like(MCategory.CN_NAME, searchName);
        }
        
        Specification<E> codeSpec = null;
        if (!StringUtils.isEmpty(searchCode)) {
            codeSpec = SpecificationUtils.equal(MCategory.CN_CODE, searchCode);
        }
        
        Specification<E> searchSpecs = SpecificationUtils.or(nameSpec, codeSpec);

        Specification<E> mainSpecs = SpecificationUtils.and(distributorSpec, searchSpecs);
        
        return getList(clientId, draft, active, mainSpecs, pageable, null); 
    }

    public long count(Long clientId, Boolean draft, Boolean active, Collection<Long> distributorIds,
            String searchName, String searchCode) {
        
        if (CollectionUtils.isEmpty(distributorIds)) {
            return 0;
        }
        
        Specification<E> distributorSpec = SpecificationUtils.in(MCategory.CN_DISTRIBUTOR_ID, distributorIds);
        
        Specification<E> nameSpec = null;
        if (!StringUtils.isEmpty(searchName)) {
            nameSpec = SpecificationUtils.like(MCategory.CN_NAME, searchName);
        }
        
        Specification<E> codeSpec = null;
        if (!StringUtils.isEmpty(searchCode)) {
            codeSpec = SpecificationUtils.equal(MCategory.CN_CODE, searchCode);
        }
        
        Specification<E> searchSpecs = SpecificationUtils.or(nameSpec, codeSpec);

        Specification<E> mainSpecs = SpecificationUtils.and(distributorSpec, searchSpecs);
        
        return count(clientId, draft, active, mainSpecs);
    }

    public boolean checkNameExist(Long clientId, Long id, String name, Long distributorId) {
        Assert.notNull(name);
        
        Specification<E> notMe = null;
        if (id != null) {
            notMe = SpecificationUtils.notEqual(MPO.CN_ID, id);
        }

        Specification<E> nameSpec = SpecificationUtils.equal(MCategory.CN_NAME, name);

        Specification<E> distributorSpec = null;
        if (distributorId != null) {
            distributorSpec = SpecificationUtils.equal(MCategory.CN_DISTRIBUTOR_ID, distributorId);
        }

        Specification<E> mainSpecs = SpecificationUtils.and(notMe, nameSpec, distributorSpec);

        return exists(clientId, null, null, mainSpecs);
    }

    public boolean checkCodeExist(Long clientId, Long id, String code, Long distributorId) {
        Assert.notNull(code);
        Specification<E> notMe = null;
        if (id != null) {
            notMe = SpecificationUtils.notEqual(MPO.CN_ID, id);
        }
        
        Specification<E> codeSpec = SpecificationUtils.equal(MCategory.CN_CODE, code);

        Specification<E> distributorSpec = null;
        if (distributorId != null) {
            distributorSpec = SpecificationUtils.equal(MCategory.CN_DISTRIBUTOR_ID, distributorId);
        }

        Specification<E> mainSpecs = SpecificationUtils.and(notMe, codeSpec, distributorSpec);

        return exists(clientId, null, null, mainSpecs);
    }

    public boolean checkDistributorUsed(Long clientId, Long distributorId) {
        Assert.notNull(distributorId);

        Specification<E> distributorSpec = SpecificationUtils.equal(MCategory.CN_DISTRIBUTOR_ID, distributorId);

        return checkUsed(clientId, distributorSpec);
    }

    // PROTECTED
    protected boolean checkUsed(Long clientId, Specification<E> spec) {
        return exists(clientId, true, null, spec) || exists(clientId, false, true, spec);
    }

}
