package com.viettel.repository.mysql.repo;

import com.viettel.repository.mysql.domain.MArea;

public class MAreaRepository extends CategoryRepository<MArea> {

    public MAreaRepository() {
        super(MArea.class);
    }

}
