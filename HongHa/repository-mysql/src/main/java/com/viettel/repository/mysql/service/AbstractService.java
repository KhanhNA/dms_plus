package com.viettel.repository.mysql.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.mysql.domain.MPO;
import com.viettel.repository.mysql.repo.BasicRepository;

public abstract class AbstractService<DETAIL extends I_PO, WRITE extends I_PO, E extends MPO> {
    
    protected abstract BasicRepository<E> getRepository();
    
    protected abstract DETAIL convertToDto(E domain);
    
    protected abstract E convertToDomain(WRITE dto);
    
    @Transactional
    public DETAIL save(String clientId, WRITE dto) {
        E domain = convertToDomain(dto);
        domain = getRepository().save(getId(clientId), domain);
        return convertToDto(domain);
    }
    
    @Transactional
    public DETAIL saveAndEnable(String clientId, WRITE dto) {
        E domain = convertToDomain(dto);
        domain.setDraft(false);
        domain = getRepository().save(getId(clientId), domain);
        return convertToDto(domain);
    }
    
    @Transactional
    public boolean enable(String clientId, String id) {
        E domain = getRepository().getById(getId(clientId), getId(id));
        if (domain == null) {
            return false;
        }
        domain.setDraft(false);
        domain = getRepository().save(getId(clientId), domain);
        return true;
    }

    @Transactional
    public boolean deactivate(String clientId, String id) {
        E domain = getRepository().getById(getId(clientId), getId(id));
        if (domain == null) {
            return false;
        }
        domain.setActive(false);
        domain = getRepository().save(getId(clientId), domain);
        return true;
    }

    @Transactional
    public boolean activate(String clientId, String id) {
        E domain = getRepository().getById(getId(clientId), getId(id));
        if (domain == null) {
            return false;
        }
        domain.setActive(true);
        domain = getRepository().save(getId(clientId), domain);
        return true;
    }

    @Transactional
    public boolean delete(String clientId, String id) {
        return getRepository().delete(getId(clientId), getId(id));
    }

    @Transactional
    public void insertBatch(String clientId, Collection<WRITE> dtos) {
        Collection<E> domains = convertToDomainList(dtos);
        getRepository().insertBatch(getId(clientId), domains);
    }
    
    // DRAFT = FALSE & ACTIVE = TRUE
    public DETAIL getById(String clientId, String id) {
        return getById(clientId, false, true, id);
    }
    
    public List<DETAIL> getListByIds(String clientId, Collection<String> ids) {
        return getListByIds(clientId, false, true, ids);
    }

    // ALL STATUS
    @Transactional(readOnly = true)
    public DETAIL getById(String clientId, Boolean draft, Boolean active, String id) {
        E domain = getRepository().getById(getId(clientId), draft, active, getId(id));
        if (domain == null) {
            return null;
        }
        return convertToDto(domain);
    }
    
    @Transactional(readOnly = true)
    public List<DETAIL> getListByIds(String clientId, Boolean draft, Boolean active, Collection<String> ids) {
        List<E> domains = getRepository().getListByIds(getId(clientId), draft, active, getIdSet(ids));
        return convertToDtoList(domains);
    }
    
    @Transactional(readOnly = true)
    public boolean exists(String clientId, Boolean draft, Boolean active, String id) {
        return getRepository().exists(getId(clientId), draft, active, getId(id));
    }

    @Transactional(readOnly = true)
    public boolean exists(String clientId, Boolean draft, Boolean active, Collection<String> ids) {
        return getRepository().exists(getId(clientId), draft, active, getIdSet(ids));
    }
    
    // Utility methods
    protected Long getId(String id) {
        return id == null ? null : Long.valueOf(id);
    }
    
    protected Collection<Long> getIdSet(Collection<String> ids) {
        if (ids == null) {
            return null;
        }
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }
        
        Set<Long> idSet = new HashSet<>(ids.size());
        for (String id : ids) {
            idSet.add(Long.valueOf(id));
        }
        return idSet;
    }

    protected List<DETAIL> convertToDtoList(List<E> domains) {
        if (domains == null) {
            return null;
        }
        
        if (domains.isEmpty()) {
            return Collections.emptyList();
        }
        
        List<DETAIL> dtos = new ArrayList<>(domains.size());
        for (int i = 0, size = domains.size(); i < size; i++) {
            dtos.add(convertToDto(domains.get(i)));
        }
        return dtos;
    }

    protected List<E> convertToDomainList(Collection<WRITE> dtos) {
        if (dtos == null) {
            return null;
        }
        
        if (dtos.isEmpty()) {
            return Collections.emptyList();
        }
        
        List<E> domains = new ArrayList<>(dtos.size());
        for (WRITE dto : dtos) {
            domains.add(convertToDomain(dto));
        }
        return domains;
    }
}
