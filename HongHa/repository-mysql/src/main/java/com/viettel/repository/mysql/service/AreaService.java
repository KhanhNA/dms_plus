package com.viettel.repository.mysql.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.viettel.repository.common.AreaRepository;
import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.domain.common.I_PO;
import com.viettel.repository.mysql.domain.MArea;
import com.viettel.repository.mysql.dto.Category;
import com.viettel.repository.mysql.repo.CategoryRepository;
import com.viettel.repository.mysql.repo.MAreaRepository;

public class AreaService extends CategorySimpleService<I_Category, MArea> implements AreaRepository {
    
    @Autowired
    private MAreaRepository repository;

    // TODO Auto-generated method stub
    @Override
    public Class<? extends I_PO> getDomainClass() {
        return null;
    }

    @Override
    protected I_Category convertToDto(MArea domain) {
        return new Category(domain);
    }

    @Override
    protected MArea convertToDomain(I_Category dto) {
        return new MArea(dto);
    }

    @Override
    protected CategoryRepository<MArea> getRepository() {
        return repository;
    }

}
