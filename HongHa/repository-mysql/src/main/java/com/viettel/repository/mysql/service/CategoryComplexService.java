package com.viettel.repository.mysql.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.PageRequest;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.common.entity.PageSizeRequest;
import com.viettel.repository.mysql.domain.MCategory;
import com.viettel.repository.mysql.repo.CategoryRepository;

public abstract class CategoryComplexService<SIMPLE extends I_Category, DETAIL extends I_Category, WRITE extends I_Category, E extends MCategory> 
        extends AbstractService<DETAIL, WRITE, E> {
    
    protected abstract CategoryRepository<E> getRepository();
    
    protected abstract SIMPLE convertToSimpleDto(E domain);
    
    // DRAFT = FALSE & ACTIVE = TRUE
    public List<SIMPLE> getAll(String clientId, String distributorId) {
        
        List<E> domains = getRepository().getAll(getId(clientId), getId(distributorId));
        return convertToSimpleDtoList(domains);
    }

    // ALL STATUS
    public List<SIMPLE> getList(String clientId, Boolean draft, Boolean active, Collection<String> distributorIds,
            String searchName, String searchCode, PageSizeRequest pageSizeRequest) {
        
        PageRequest pageable = null;
        if (pageSizeRequest != null) {
            pageable = new PageRequest(pageSizeRequest.getPage(), pageSizeRequest.getSize());
        }
        List<E> domains = getRepository().getList(
                getId(clientId), draft, active, getIdSet(distributorIds), searchName, searchCode, pageable);
        
        return convertToSimpleDtoList(domains);
    }

    public long count(String clientId, Boolean draft, Boolean active, Collection<String> distributorIds,
            String searchName, String searchCode) {
        
        return getRepository().count(
                getId(clientId), draft, active, getIdSet(distributorIds), searchName, searchCode);
    }

    public boolean checkNameExist(String clientId, String id, String name, String distributorId) {
        
        return getRepository().checkNameExist(getId(clientId), getId(id), name, getId(distributorId));
    }

    public boolean checkCodeExist(String clientId, String id, String code, String distributorId) {
        
        return getRepository().checkCodeExist(getId(clientId), getId(id), code, getId(distributorId));
    }

    public boolean checkDistributorUsed(String clientId, String distributorId) {
        
        return getRepository().checkDistributorUsed(getId(clientId), getId(distributorId));
    }

    protected List<SIMPLE> convertToSimpleDtoList(List<E> domains) {
        if (domains == null) {
            return null;
        }
        
        if (domains.isEmpty()) {
            return Collections.emptyList();
        }
        
        List<SIMPLE> dtos = new ArrayList<>(domains.size());
        for (int i = 0, size = domains.size(); i < size; i++) {
            dtos.add(convertToSimpleDto(domains.get(i)));
        }
        return dtos;
    }
    
}
