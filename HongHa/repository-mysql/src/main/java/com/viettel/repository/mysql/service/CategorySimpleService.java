package com.viettel.repository.mysql.service;

import com.viettel.repository.common.domain.common.I_Category;
import com.viettel.repository.mysql.domain.MCategory;

public abstract class CategorySimpleService<D extends I_Category, E extends MCategory> extends CategoryComplexService<D, D, D, E> {

    @Override
    protected D convertToSimpleDto(E domain) {
        return convertToDto(domain);
    }

}
