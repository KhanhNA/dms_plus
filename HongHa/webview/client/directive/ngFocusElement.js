(function () {
    'use strict';
    var module = angular.module('vt.directive.focus', []);

    module.directive('ngFocusElement', function () {
        return function (scope, element) {
            element.focus();
        };
    });

})();