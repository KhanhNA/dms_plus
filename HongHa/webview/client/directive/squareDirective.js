(function () {
    'use strict';
    var module = angular.module('vt.directive.square', []);
    module.directive('slimScroll', [
        function () {
            return {
                restrict: 'A',
                link: function (scope, ele, attrs) {
                    return ele.slimScroll({
                        height: attrs.scrollHeight || '100%'
                    });
                }
            };
        }
    ]);

    module.directive('highlightActive', [
        function () {
            return {
                restrict: "A",
                controller: [
                    '$scope', '$element', '$attrs', '$location', function ($scope, $element, $attrs, $location) {
                        var highlightActive, links, path;
                        links = $element.find('a');
                        path = function () {
                            return $location.path();
                        };
                        highlightActive = function (links, path) {
                            path = '#' + path;
                            return angular.forEach(links, function (link) {
                                var $li, $link, href, $ul, $parentLi, $lists;
                                $link = angular.element(link);
                                $li = $link.parent('li');
                                href = $link.attr('href');
                                if ($li.hasClass('active')) {
                                    $li.removeClass('active');
                                }

                                $lists = $element.find('ul').parent('li');

                                $ul = $li.parent('ul');
                                $parentLi = null;
                                if ($ul.attr('id') != 'nav') {
                                    $parentLi = $ul.parent('li');
                                }

                                if ((path.indexOf(href) === 0 && path.length == href.length) || (path.indexOf(href + '/') === 0)) {
                                    if ($parentLi != null) {
                                        $parentLi.addClass('active');
                                        $lists.not($parentLi).removeClass('open').find('ul').slideUp();
                                        //Added by hungbq6
                                        if (!$("#app").hasClass("nav-min"))
                                            $parentLi.addClass('open').find('ul').slideDown();
                                    }
                                    return $li.addClass('active');
                                }
                            });
                        };
                        highlightActive(links, $location.path());
                        return $scope.$watch(path, function (newVal, oldVal) {
                            if (newVal === oldVal) {
                                return;
                            }
                            return highlightActive(links, $location.path());
                        });
                    }
                ]
            };
        }
    ]);

    module.directive('collapseNav', [
        function () {
            return {
                restrict: 'A',
                link: function (scope, ele, attrs) {
                    var $a, $aRest, $lists, $listsRest, app;
                    $lists = ele.find('ul').parent('li');
                    $lists.append('<i id="nav_item_icon" class="fa fa-caret-right icon-has-ul"></i>');
                    $lists.append('<i id="nav_item_icon" class="fa fa-caret-down icon-has-ul"></i>');
                    $a = $lists.children('a');
                    $listsRest = ele.children('li').not($lists);
                    $aRest = $listsRest.children('a');
                    app = $('#app');
                    $a.on('click', function (event) {
                        var $parent, $this;
                        if (app.hasClass('nav-min')) {
                            return false;
                        }
                        $this = $(this);
                        $parent = $this.parent('li');

                        $lists.not($parent).removeClass('open').find('ul').slideUp();
                        $parent.toggleClass('open').find('ul').slideToggle();

                        ////Added by hungbq6
                        //$lists.not($parent).find('#nav_item_icon').removeClass('fa-caret-down').addClass('fa-caret-right');
                        //if ($parent.hasClass('open')) {
                        //    $parent.find('#nav_item_icon').removeClass('fa-caret-right').addClass('fa-caret-down');
                        //} else {
                        //    $parent.find('#nav_item_icon').removeClass('fa-caret-down').addClass('fa-caret-right');
                        //}
                        ////End

                        return event.preventDefault();
                    });
                    $aRest.on('click', function (event) {
                        return $lists.removeClass('open').find('ul').slideUp();
                    });
                    return scope.$on('minNav:enabled', function (event) {
                        return $lists.removeClass('open').find('ul').slideUp();
                    });
                }
            };
        }
    ]);

    module.directive('customBackground', function () {
        return {
            restrict: "A",
            controller: [
                '$scope', '$element', '$location', function ($scope, $element, $location) {
                    var addBg, path;
                    path = function () {
                        return $location.path();
                    };
                    addBg = function (path) {
                        $element.removeClass('body-home body-special body-tasks body-lock');
                        switch (path) {
                            case '/':
                                return $element.addClass('body-home');
                            case '/404':
                            case '/500':
                                //case '/pages/signin':
                                //case '/pages/signup':
                                //case '/pages/forgot':
                                return $element.addClass('body-special');
                            //case '/pages/lock-screen':
                            //    return $element.addClass('body-special body-lock');
                            //case '/tasks':
                            //    return $element.addClass('body-tasks');
                        }
                    };
                    addBg($location.path());
                    return $scope.$watch(path, function (newVal, oldVal) {
                        if (newVal === oldVal) {
                            return;
                        }
                        return addBg($location.path());
                    });
                }
            ]
        };
    });

    module.directive('toggleMinNav', [
        '$rootScope', function ($rootScope) {
            return {
                restrict: 'A',
                link: function (scope, ele, attrs) {
                    var $content, $nav, $window, Timer, app, updateClass;
                    app = $('#app');
                    $window = $(window);
                    $nav = $('#nav-container');
                    $content = $('#content');
                    ele.on('click', function (e) {
                        if (app.hasClass('nav-min')) {
                            app.removeClass('nav-min');
                        } else {
                            app.addClass('nav-min');
                            $rootScope.$broadcast('minNav:enabled');
                        }
                        return e.preventDefault();
                    });
                    Timer = void 0;
                    updateClass = function () {
                        var width;
                        width = $window.width();
                        if (width < 768) {
                            return app.removeClass('nav-min');
                        }
                    };
                    return $window.resize(function () {
                        var t;
                        clearTimeout(t);
                        return t = setTimeout(updateClass, 300);
                    });
                }
            };
        }
    ]);

    module.directive('toggleOffCanvas', [
        function () {
            return {
                restrict: 'A',
                link: function (scope, ele, attrs) {
                    return ele.on('click', function () {
                        return $('#app').toggleClass('on-canvas');
                    });
                }
            };
        }
    ]);

    module.directive("flotChart", [function () {
        return {
            restrict: "A",
            scope: {
                data: "=",
                options: "="
            },
            link: function (scope, ele) {
                var data, options, plot;
                return data = scope.data, options = scope.options, plot = $.plot(ele[0], data, options)
            }
        }
    }]);

    module.directive("morrisChart", [function () {
        return {
            restrict: "A",
            scope: {
                data: "="
            },
            link: function (scope, ele, attrs) {
                var colors, data, func, options;
                switch (data = scope.data, attrs.type) {
                    case "line":
                        return colors = void 0 === attrs.lineColors || "" === attrs.lineColors ? null : JSON.parse(attrs.lineColors), options = {
                            element: ele[0],
                            data: data,
                            xkey: attrs.xkey,
                            ykeys: JSON.parse(attrs.ykeys),
                            labels: JSON.parse(attrs.labels),
                            lineWidth: attrs.lineWidth || 2,
                            lineColors: colors || ["#0b62a4", "#7a92a3", "#4da74d", "#afd8f8", "#edc240", "#cb4b4b", "#9440ed"]
                        }, new Morris.Line(options);
                    case "area":
                        return colors = void 0 === attrs.lineColors || "" === attrs.lineColors ? null : JSON.parse(attrs.lineColors), options = {
                            element: ele[0],
                            data: data,
                            xkey: attrs.xkey,
                            ykeys: JSON.parse(attrs.ykeys),
                            labels: JSON.parse(attrs.labels),
                            lineWidth: attrs.lineWidth || 2,
                            lineColors: colors || ["#0b62a4", "#7a92a3", "#4da74d", "#afd8f8", "#edc240", "#cb4b4b", "#9440ed"],
                            behaveLikeLine: attrs.behaveLikeLine || !1,
                            fillOpacity: attrs.fillOpacity || "auto",
                            pointSize: attrs.pointSize || 4
                        }, new Morris.Area(options);
                    case "bar":
                        return colors = void 0 === attrs.barColors || "" === attrs.barColors ? null : JSON.parse(attrs.barColors), options = {
                            element: ele[0],
                            data: data,
                            xkey: attrs.xkey,
                            ykeys: JSON.parse(attrs.ykeys),
                            labels: JSON.parse(attrs.labels),
                            barColors: colors || ["#0b62a4", "#7a92a3", "#4da74d", "#afd8f8", "#edc240", "#cb4b4b", "#9440ed"],
                            stacked: attrs.stacked || null
                        }, new Morris.Bar(options);
                    case "donut":
                        return colors = void 0 === attrs.colors || "" === attrs.colors ? null : JSON.parse(attrs.colors), options = {
                            element: ele[0],
                            data: data,
                            colors: colors || ["#0B62A4", "#3980B5", "#679DC6", "#95BBD7", "#B0CCE1", "#095791", "#095085", "#083E67", "#052C48", "#042135"]
                        }, attrs.formatter && (func = new Function("y", "data", attrs.formatter), options.formatter = func), new Morris.Donut(options)
                }
            }
        }
    }]);

    var compareTo = function() {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function(modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
            }
        };
    };
    module.directive("compareTo", compareTo);

})();