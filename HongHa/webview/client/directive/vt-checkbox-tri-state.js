(function () {
    'use strict';
    var module = angular.module('vt.directive.tri-state', []);

    module.controller('CheckboxTriStateCtrl', ['$scope', function ($scope, $attrs) {
        var ngModelCtrl = {$setViewValue: angular.noop};// nullModelCtrl

        this.init = function (ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_;
        };

        $scope.click = function () {
            if ($attrs == null || $attrs.disabled == null) {
                if (ngModelCtrl.$viewValue == 'T') {
                    ngModelCtrl.$setViewValue('F');
                } else {
                    ngModelCtrl.$setViewValue('T');
                }
            }
        };

        $scope.getStatus = function () {
            return ngModelCtrl.$viewValue == null ? 'F' : ngModelCtrl.$viewValue;
        };

    }]);

    module.directive('vtCheckboxTriState', function () {
        return {
            restrict: 'E',
            scope: {},
            require: ['vtCheckboxTriState', '?ngModel'],
            controller: 'CheckboxTriStateCtrl',
            templateUrl: 'directive/template/vt-checkbox/vt-checkbox-tri-state.html',
            replace: true,
            link: function (scope, element, attrs, ctrls) {
                var CheckboxTriStateCtrl = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                CheckboxTriStateCtrl.init(ngModelCtrl);
            }
        };
    });
})();