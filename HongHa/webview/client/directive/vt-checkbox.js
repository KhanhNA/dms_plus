(function () {
    'use strict';
    var module = angular.module('vt.directive.checkbox', []);

    module.controller('CheckboxCtrl', ['$scope', function ($scope, $attrs) {
        var ngModelCtrl = {$setViewValue: angular.noop}; // nullModelCtrl

        this.init = function (ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_;
        };

        $scope.click = function () {
            if ($attrs == null || $attrs.disabled == null) {
                if (ngModelCtrl.$viewValue) {
                    ngModelCtrl.$setViewValue(false);
                } else {
                    ngModelCtrl.$setViewValue(true);
                }
            }
        };

        $scope.isChecked = function () {
            return ngModelCtrl.$viewValue == true;
        };

    }]);

    module.directive('vtCheckbox', function () {
        return {
            restrict: 'E',
            scope: {
                ngDisabled: '='
            },
            require: ['vtCheckbox', '?ngModel'],
            controller: 'CheckboxCtrl',
            templateUrl: 'directive/template/vt-checkbox/vt-checkbox.html',
            replace: true,
            link: function (scope, element, attrs, ctrls) {
                var CheckboxCtrl = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                CheckboxCtrl.init(ngModelCtrl);
            }
        };
    });
})();