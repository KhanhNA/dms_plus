(function () {
    'use strict';
    var module = angular.module('vt.directive', [
        'vt.directive.enter',
        'vt.directive.focus',
        'vt.directive.square',
        'vt.directive.checkbox',
        'vt.directive.tri-state',
        'vt.directive.file-manager',
        'vt.directive.radio',
        'vt.directive.search-box'
    ]);
})();
