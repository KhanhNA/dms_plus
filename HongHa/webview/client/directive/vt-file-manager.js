(function () {
    'use strict';
    var module = angular.module('vt.directive.file-manager', ['angularFileUpload']);

    module.value('vtFileManagerConfig', {
        uploadUrl: '/',
        getDownloadUrl: function (fileId) {
            return '/' + fileId;
        },
        getDeleteUrl: function (fileId) {
            return '/' + fileId;
        },
        getHeaders: function () {
            return {};
        }
    });

    module.controller('vtSingleFileController', ['$scope', '$element', '$attrs', 'FileUploader', 'vtFileManagerConfig', '$http',
        function ($scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http) {
            var ngModelCtrl = {$setViewValue: angular.noop};// nullModelCtrl

            this.init = function (ngModelCtrl_) {
                ngModelCtrl = ngModelCtrl_;
            };

            $scope.getFileName = function () {
                if (ngModelCtrl.$viewValue != null) {
                    return ngModelCtrl.$viewValue.name;
                }

                return null;
            };

            var inputs = $element.find('input');
            var input = null;
            if (inputs.length > 0) {
                input = $(inputs[0]);
            }

            $scope.getAccepts = function() {
                if ($attrs.accept != null) {
                    return $attrs.accept;
                }

                return "*/*";
            };

            var url = vtFileManagerConfig.getBaseUrl() + 'file';
            var uploader = $scope.uploader = new FileUploader({
                url: url,
                headers: null
            });

            uploader.onAfterAddingAll = function (addedItems) {
                for (var i = 0; i < addedItems.length; i++) {
                    var item = addedItems[i];
                    item.headers = { Authorization: 'Bearer ' + vtFileManagerConfig.getAccessToken() };
                }
                uploader.uploadAll();
            };

            uploader.onSuccessItem = function (fileItem, response, status, headers) {
                fileItem.id = response;

                var file = {
                    fileId: fileItem.id,
                    name: fileItem._file.name
                };

                ngModelCtrl.$setViewValue(file);
                ngModelCtrl.$render();

                if (input != null) {
                    input.val("");
                }
            };
        }]);

    module.directive('vtSingleFile', function () {
        return {
            restrict: 'AE',
            require: ['vtSingleFile', '?ngModel'],
            replace: true,
            scope: {},
            templateUrl: "directive/template/vt-file-manager/vt-single-file.html",
            controller: "vtSingleFileController",
            link: function (scope, element, attrs, ctrls) {
                var vtSingleFileController = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                vtSingleFileController.init(ngModelCtrl);
            }
        };
    });

    module.controller('vtSingleImageController', ['$scope', '$element', '$attrs', 'FileUploader', 'vtFileManagerConfig', '$http',
        function ($scope, $element, $attrs, FileUploader, vtFileManagerConfig, $http) {
            var ngModelCtrl = {$setViewValue: angular.noop};// nullModelCtrl

            this.init = function (ngModelCtrl_) {
                ngModelCtrl = ngModelCtrl_;
            };

            $scope.getFile = function () {
                return ngModelCtrl.$viewValue;
            };

            $scope.getLink = function () {
                var id = $scope.getFile();
                if (id != null) {
                    return vtFileManagerConfig.getBaseUrl() + 'image/' + id;
                }
                return null;
            };

            var sizeType = null;
            if ($attrs.sizeType != null) {
                sizeType = $attrs.sizeType;
            }

            var url = vtFileManagerConfig.getBaseUrl() + 'image';
            if (sizeType != null && ('thumb' === sizeType || 'medium' === sizeType || 'standard' === sizeType)) {
                url = url + "?sizetype=" + sizeType;
            }

            var inputs = $element.find('input');
            var input = null;
            if (inputs.length > 0) {
                for (var i = 0; i < inputs.length; i++) {
                    if (inputs[i].type == 'file') {
                        input = $(inputs[0]);
                        break;
                    }
                }
            }

            var tempFileItem = null;

            var uploader = $scope.uploader = new FileUploader({
                url: url,
                headers: null
            });

            uploader.onAfterAddingAll = function () {
                uploader.headers = { Authorization: 'Bearer ' + vtFileManagerConfig.getAccessToken() };
                uploader.uploadAll();
            };

            uploader.onSuccessItem = function (fileItem, response, status, headers) {
                fileItem.id = response;
                ngModelCtrl.$setViewValue(fileItem.id);
                ngModelCtrl.$render();
                tempFileItem = fileItem;

                if (input != null) {
                    input.val("");
                }
            };

            $scope.removeFile = function () {
                if (!angular.isUndefinedOrNull(ngModelCtrl.$viewValue)) {
                    var deleteURL = vtFileManagerConfig.getBaseUrl() + 'file'
                        + '?id=' + ngModelCtrl.$viewValue
                        + '&access_token=' + vtFileManagerConfig.getAccessToken();

                    $http.delete(deleteURL).
                        success(function (data, status, headers, config) {

                        }).
                        error(function (data, status, headers, config) {

                        });

                    if (tempFileItem != null) {
                        tempFileItem.remove();
                    }

                    ngModelCtrl.$setViewValue(null);
                    ngModelCtrl.$render();

                    if (input != null) {
                        input.val("");
                    }
                }
            };
        }]);

    module.directive('vtSingleImage', function () {
        return {
            restrict: 'AE',
            require: ['vtSingleImage', '?ngModel'],
            replace: true,
            scope: {},
            templateUrl: "directive/template/vt-file-manager/vt-single-image.html",
            controller: "vtSingleImageController",
            link: function (scope, element, attrs, ctrls) {
                var vtSingleImageController = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                vtSingleImageController.init(ngModelCtrl);
            }
        };
    });

})();