(function () {
    'use strict';
    var module = angular.module('vt.directive.radio', []);

    module.controller('RadioCtrl', ['$scope', function ($scope, $attrs) {
        var ngModelCtrl = {$setViewValue: angular.noop};// nullModelCtrl

        var value = null;

        this.init = function (ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_;

            value = $scope.value;
            if ($scope.scopeValue != null) {
                value = $scope.scopeValue;
            }
        };

        $scope.click = function () {
            if ($attrs == null || $attrs.disabled == null) {
                ngModelCtrl.$setViewValue(value);
            }
        };

        $scope.isChecked = function () {
            return ngModelCtrl.$viewValue == value;
        };

    }]);

    module.directive('vtRadio', function () {
        return {
            restrict: 'E',
            scope: {
                value: "@",
                scopeValue: "="
            },
            require: ['vtRadio', '?ngModel'],
            controller: 'RadioCtrl',
            templateUrl: 'directive/template/vt-radio/vt-radio.html',
            replace: true,
            link: function (scope, element, attrs, ctrls) {
                var RadioCtrl = ctrls[0], ngModelCtrl = ctrls[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                RadioCtrl.init(ngModelCtrl);
            }
        };
    });
})();