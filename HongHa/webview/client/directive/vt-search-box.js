(function () {
    'use strict';
    var module = angular.module('vt.directive.search-box', []);

    module.controller('vtSearchBoxController', function ($scope, $attrs) {
        var ngModelCtrl = {$setViewValue: angular.noop};// nullModelCtrl

        var lastText = null;

        this.init = function (ngModelCtrl_) {
            ngModelCtrl = ngModelCtrl_;
            $scope.searchText = null;
        };

        var listener = $scope.$watch(
            ngModelCtrl.$viewValue,
            function () {
                $scope.searchText = ngModelCtrl.$viewValue;
                lastText = $scope.searchText;
                listener();
            }
        );

        $scope.textChanged = function () {
            if (lastText != null
                && lastText.length > 0
                && $scope.searchText != null
                && $scope.searchText.length > 0
                && lastText == $scope.searchText) {
                return false;
            }

            return true;
        };

        $scope.performSearch = function () {
            listener();
            ngModelCtrl.$setViewValue($scope.searchText);
            ngModelCtrl.$render();
            lastText = $scope.searchText;
        };

        $scope.clearText = function () {
            listener();
            lastText = '';
            $scope.searchText = '';
            ngModelCtrl.$setViewValue($scope.searchText);
            ngModelCtrl.$render();
        };

        $scope.onEnter = function () {
            if ($scope.textChanged()) {
                $scope.performSearch();
            } else {
                $scope.clearText();
            }
        };

        $scope.onBlur = function () {
            if ($scope.textChanged()) {
                if ((lastText != null && lastText.length > 0)
                    || ($scope.searchText != null && $scope.searchText.length > 0)) {
                    $scope.performSearch();
                }
            }
        };

    });

    module.directive('vtSearchBox', function () {
        return {
            restrict: 'AE',
            scope: {
                placeholder: '@',
                ngDisabled: '='
            },
            require: ['vtSearchBox', '?ngModel'],
            templateUrl: "directive/template/vt-search-box/vt-search-box.html",
            controller: "vtSearchBoxController",
            link: function (scope, element, attrs, controllers) {

                var searchBoxCtrl = controllers[0], ngModelCtrl = controllers[1];

                if (!ngModelCtrl) {
                    return; // do nothing if no ng-model
                }

                searchBoxCtrl.init(ngModelCtrl);
            }
        };
    });
})();