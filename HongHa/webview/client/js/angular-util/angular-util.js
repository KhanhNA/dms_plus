/**
 * 
 * @param {window} window
 * @param {object} angular
 * @param {undefined} undefined
 * @returns {angular}
 * 
 * @author haizpt
 * @description Dinh nghia them cac tien ich ma angular core chua co
 */

(function(window, angular, undefined) {'use strict';
    var agl = angular || {};
    var ua  = navigator.userAgent;
    
    agl.ISFF     = ua.indexOf('Firefox') != -1;
    agl.ISOPERA  = ua.indexOf('Opera') != -1;
    agl.ISCHROME = ua.indexOf('Chrome') != -1;
    agl.ISSAFARI = ua.indexOf('Safari') != -1 && !agl.ISCHROME;
    agl.ISWEBKIT = ua.indexOf('WebKit') != -1;

    agl.ISIE   = ua.indexOf('Trident') > 0 || navigator.userAgent.indexOf('MSIE') > 0;
    agl.ISIE6  = ua.indexOf('MSIE 6') > 0;
    agl.ISIE7  = ua.indexOf('MSIE 7') > 0;
    agl.ISIE8  = ua.indexOf('MSIE 8') > 0;
    agl.ISIE9  = ua.indexOf('MSIE 9') > 0;
    agl.ISIE10 = ua.indexOf('MSIE 10') > 0;
    agl.ISOLD  = agl.ISIE6 || agl.ISIE7 || agl.ISIE8; // MUST be here
    
    agl.ISIE11UP = ua.indexOf('MSIE') == -1 && ua.indexOf('Trident') > 0;
    agl.ISIE10UP = agl.ISIE10 || agl.ISIE11UP;
    agl.ISIE9UP  = agl.ISIE9 || agl.ISIE10UP;
    agl.IEMobile = ua.match('/IEMobile\/10\.0/');
    
    agl.isNull =
    agl.is_null = function (arg)
    {
        // must BE DOUBLE EQUALS - NOT TRIPLE
        if (arg == null || typeof arg === 'object' && !arg) {
            return true;
        }
        
        return false;
    };
    
    agl.sizeOwnProperty = function(obj){
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };

    agl.isUndefinedOrNull = function(val) {
        return angular.isUndefined(val) || val === null;
    };

    agl.isArrayEmpty = function(val) {
        return angular.isUndefinedOrNull(val) || val.length == 0;
    };

    agl.endsWith = function(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    };

    agl.parseDate = function(isoDateString) {
        if (angular.isUndefinedOrNull(isoDateString)) {
            return null;
        }

        return moment(isoDateString, 'YYYY-MM-DD').toDate();
    };

    agl.formatDate = function(dateValue) {
        if (angular.isUndefinedOrNull(dateValue)) {
            return null;
        }

        return moment(dateValue).format('YYYY-MM-DD');
    };

    agl.displayIsoDate = function(isoDate) {
        var date =  angular.parseDate(isoDate);
        if (date == null) {
            return '';
        }
        return moment(date).format('DD/MM/YYYY');
    };

    agl.parseDateTime = function(isoDateString) {
        if (angular.isUndefinedOrNull(isoDateString)) {
            return null;
        }

        return moment(isoDateString, 'YYYY-MM-DDTHH:mm:ss').toDate();
    };

    agl.formatDateTime = function(dateValue) {
        if (angular.isUndefinedOrNull(dateValue)) {
            return null;
        }

        return moment(dateValue).format('YYYY-MM-DDTHH:mm:ss');
    };

    agl.truncateDate = function(dateValue) {
        if (angular.isUndefinedOrNull(dateValue)) {
            return null;
        }

        dateValue.setHours(0);
        dateValue.setMinutes(0);
        dateValue.setSeconds(0);
        dateValue.setMilliseconds(0);

        return dateValue;
    };

    agl.encodeURI = function(value) {
        return value;
    };

    agl.escapeHTML = function(html) {
        return html.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
    }

})(window, window.angular);
