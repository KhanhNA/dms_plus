/**
 * @author haind25
 */
(function() {
    'use strict';
    
    var tokenModule = angular.module('vt.token', []);

    tokenModule.provider('$token', function () {

        var REQUIRED_AND_MISSING = 'REQUIRED_AND_MISSING';

        var defaultConfigs = {
           'client_id': REQUIRED_AND_MISSING,
           'authorizationEndpoint': REQUIRED_AND_MISSING,
           'scope': '',
           'revokeTokenEndpoint': '',
           'verifyTokenEndpoint': '',
           'logoutUrl': '',
           'redirect_url': '',
           'state_length': 5,
           'refresh': true,
           'min_live_time': 10 * 1000,
           'refresh_interval': 20 * 60 * 1000
        };

        this.extendConfig = function(configExtension) {
            defaultConfigs = angular.extend(defaultConfigs, configExtension);
            return defaultConfigs;
        };
        
        this.set =
        this.Set = function ()
        {
            if (arguments.length !== 1 && typeof arguments[0] === 'object') {
                return this;
            }
            
            if(typeof arguments[0] !== 'string'){
                return this;
            }
            
            var value = typeof arguments[1] === 'undefined' ? null : arguments[1];
            var key = arguments[0];
            defaultConfigs[key] = value;
            
            return this;
        };
        
        this.$get = function ($http, $rootScope, $location, $q, $window, $log, $timeout, $interval) {
            var requiredAndMissing = [];
            angular.forEach(defaultConfigs, function (value, key) {
                if (value === REQUIRED_AND_MISSING) {
                    requiredAndMissing.push(key);
                }
            });

            if (requiredAndMissing.length) {
                throw new Error("TokenProvider is insufficiently configured.  Please " +
                        "configure the following options using " +
                        "$tokenProvider.extendConfig: " + requiredAndMissing.join(", "));
            }

            function setAccessToken(params) {
                // Check if same state
                if (!params['state'] || params['state'] !== $window.localStorage['state']) {
                    $log.debug("State not match, token rejected");
                    return;
                }
                $window.localStorage['access_token'] = params['access_token'];
                $window.localStorage['token_type'] = params['token_type'];
                if (params['expires_in']) {
                    var expiryDate = new Date(new Date().getTime() + (params['expires_in'] * 1000));
                    $window.localStorage['expires_in'] = params['expires_in'];
                    $window.localStorage['expiry_date'] = expiryDate;
                } else {
                    $window.localStorage['expires_in'] = undefined;
                    $window.localStorage['expiry_date'] = undefined;
                }
                if (!params['auto_refresh']) {
                    params['lang'] ? $window.localStorage['lang'] = params['lang'].toLowerCase() : null;
                }
                setTimeoutToRefresh();
            }

            function getAccessToken() {
                if (getTokenExpiredIn() < defaultConfigs.min_live_time) {
                    return undefined;
                }
                return $window.localStorage["access_token"];
            }

            function getTokenExpiredIn() {
                var expiryDate = angular.isUndefined($window.localStorage['expiry_date']) ? null : Date.parse($window.localStorage['expiry_date']);
                return expiryDate != null ? (expiryDate - new Date().getTime()) : 0;
            }

            function setTimeoutToRefresh() {
                if (!defaultConfigs.refresh) {
                    $log.debug("Auto refresh token was turned off");
                    return;
                }

                // OAuth iframe will be remove immediately after token was saved
                // So we no need set a schedule to renew token
                if (isOAuthFrame()) {
                    $log.debug("Skip auto refresh token in OAuth iFrame");
                    return;
                }
                var diff = getTokenExpiredIn();
                // Only schedule to refresh if expire time large than minimum live time
                if (diff >= defaultConfigs.min_live_time && diff <= defaultConfigs.refresh_interval) {
                    // Refresh now
                    $log.debug("Token will expired in " + diff + " milis. Scheduled to renew token after 0 milis");
                    renewTokenAfter(0);
                } else if (diff > defaultConfigs.min_live_time) {
                    // Refresh after configured interval
                    $log.debug("Token will expired in " + diff + " milis. Scheduled to renew token after " + defaultConfigs.refresh_interval + " milis");
                    renewTokenAfter(defaultConfigs.refresh_interval);
                }
            }
            
            function getUserName() {
                var userInfo = getUserInfo();
                if(!userInfo) {
                    return null;
                }
                return userInfo["username"];
            }
            
            function endsWith(str, suffix) {
                return str.indexOf(suffix, str.length - suffix.length) !== -1;
            }
            
            function logout() {
                var accessToken = getAccessToken();
                var username = getUserName();
                
                var doIt = function() {
                    clearToken();
                    clearUserInfo();

                    // Redirect to logout url
                    if (defaultConfigs.logoutUrl
                            && defaultConfigs.logoutUrl.length > 0) {
                        var lang = getUserLanguage();
                        // Passing current user's language to logout url, so the auth server can decide to switch the
                        // language of login page to current user's language
                        if (!angular.isUndefinedOrNull(lang)) {
                            $window.location.href = addUrlParam(defaultConfigs.logoutUrl, 'lang', lang);
                        } else {
                            $window.location.href = defaultConfigs.logoutUrl;
                        }
                    }
                };
                
                if (accessToken && defaultConfigs.revokeTokenEndpoint
                         && defaultConfigs.revokeTokenEndpoint.length > 0
                         && username) {
                    var url = defaultConfigs.revokeTokenEndpoint;

                    if (!endsWith(url,"/")) {
                        url += "/";
                    }
                    
                    url += 'users/' + angular.lowercase(username) + '/tokens/' + accessToken;

                    var configs = {
                        method: 'DELETE',
                        url: url,
                        headers: { 'Authorization': 'Bearer ' + accessToken}
                    };
                    
                    $http(configs).then(function(){
                        $log.info("Token revoked");
                    }, function(e){
                        $log.error("Token revoke error", e);
                    }).finally(function(){
                        doIt();
                    });
                } else {
                    doIt();
                }
                
            }

            /**
             * Add a URL parameter (or changing it if it already exists)
             * @param {string} search  this is typically document.location.search
             * @param {string} key     the key to set
             * @param {string} val     value
             */
            function addUrlParam(search, key, val){
                var newParam = key + '=' + val,
                    params = '?' + newParam;

                // If the "search" string exists, then build params from it
                if (search) {
                    // Try to replace an existance instance
                    params = search.replace(new RegExp('[\?&]' + key + '[^&]*'), '$1' + newParam);

                    // If nothing was replaced, then add the new param to the end
                    if (params === search) {
                        if (params.indexOf('?') == -1) {
                            params += '?';
                        }
                        params += '&' + newParam;
                    }
                }

                return params;
            }
            
            function clearToken(){
                $window.localStorage.removeItem("access_token");
            }
            
            function clearUserInfo() {
                $window.localStorage.removeItem("userInfo");
            }
            
            function getUserInfo() {
                var userInfo = $window.localStorage["userInfo"];
                if(!userInfo) {
                    return null;
                }
                try {
                    return JSON.parse(userInfo);
                } catch (e) {
                    $window.localStorage["userInfo"] = null;
                }
                return null;
            }
            
            function setUserInfo(userInfo) {
                $window.localStorage["userInfo"] = JSON.stringify(userInfo);
            }

            function getUserLanguage() {
                return $window.localStorage["lang"];
            }

            function setUserLanguage(lang) {
                $window.localStorage["lang"] = lang;
            }
            
            function verifyToken(){
                var defer = $q.defer();
                 
                if(angular.isNull(getAccessToken())) {
                   loginHandler();
                }
                
                if(angular.isNull(defaultConfigs.verifyTokenEndpoint)
                         || defaultConfigs.verifyTokenEndpoint.length === 0) {
                    throw new Error("Please config 'verifyTokenEndpoint' before use this function");
                }
                   
                $http.get(defaultConfigs.verifyTokenEndpoint).
                success(function(userinfo) {
                    setUserInfo(userinfo);
                    defer.resolve(userinfo);
                }).
                error(function(error) {
                    defer.reject(error);
                });

                return defer.promise;
            }
            
            function setTokenFromLocation() {
                var search = $location.search();
                setAccessToken(search);

                //remove access_token from url
                $location.search('access_token', null);
                $location.search('token_type', null);
                $location.search('expires_in', null);
                $location.search('flag', null);
                $location.search('lang', null);
                $location.search('auto_refresh', null);
                $location.search('state', null);

                // Self close if is an iframe
                if (isOAuthFrame()) {
                    $log.debug("Token was renewed using iFrame");
                    var iFrame = $window.parent.document.getElementById('oauth_frame');
                    if (iFrame) {
                        iFrame.parentNode.removeChild(iFrame);
                    }
                } else {
                    $log.debug("Token was saved");
                }
            }

            function isOAuthFrame() {
                return $window.parent && $window.parent.document.getElementById('oauth_frame');
            }

            function getRedirectUrl() {
                var redirect_uri = $location.absUrl();
                var search = $location.search();
                if (angular.sizeOwnProperty(search) === 0) {
                    redirect_uri = addUrlParam(redirect_uri, 'flag', 'true');
                }
                return redirect_uri;
            }

            function createRandomString(length) {
                var text = "";
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                for( var i = 0; i < length; i++ ) {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                return text;
            }

            function createRandomState() {
                // Create random state
                var state = createRandomString(defaultConfigs.state_length);
                // Save to storage for further process
                $window.localStorage['state'] = state;
                return state;
            }

            function buildAuthorizationUrl(extendParams) {
                var params = {
                    response_type: "token",
                    client_id: defaultConfigs.client_id,
                    redirect_uri: getRedirectUrl(),
                    scope: defaultConfigs.scope,
                    state: createRandomState()
                };

                params = angular.extend(params, extendParams);

                var url = defaultConfigs.authorizationEndpoint;

                angular.forEach(params, function (value, key) {
                    url = addUrlParam(url, encodeURIComponent(key), encodeURIComponent(value));
                });

                return url;
            }
            
            function loginHandler() {
                login();
            }

            function login() {
                var search = $location.search();
                if(search.access_token) {
                    setTokenFromLocation();
                    return;
                }
                $window.location.href = buildAuthorizationUrl();
            }

            function renewTokenAfter(timeout) {
                $timeout(function() {
                    renewToken();
                }, timeout, false);
            }

            var updateProgressTask;

            function renewToken() {
                // Check if token already renewed
                var tokenExpiredIn = getTokenExpiredIn();
                if (tokenExpiredIn > defaultConfigs.refresh_interval) {
                    return;
                }

                // Check lock, if locked, check last update time
                if ($window.localStorage['refresh_locked']) {
                    var currentTime = new Date().getTime();
                    var lastUpdate = angular.isUndefined($window.localStorage['refresh_last_update']) ? null : Date.parse($window.localStorage['refresh_last_update']);
                    if (lastUpdate > 0) {
                        var diff = currentTime - lastUpdate;
                        // Another tab is holding lock and still working, schedule to check again
                        if (diff > 0 && diff <= 2 * 1000) {
                            $timeout(renewToken, 2 * 1000, false);
                            return;
                        }
                    }
                }
                // Not locked by anyone (only one tab)
                // Or last update time is too old (the tab holding the lock may be closed)
                // So we acquire new lock for current tab

                // Acquired new lock
                $window.localStorage['refresh_locked'] = true;
                $window.localStorage['refresh_last_update'] = new Date();

                // Update the last update continuously to keep the lock
                updateProgressTask = $interval(function() {
                    $window.localStorage['refresh_last_update'] = new Date();
                }, 1000);

                var iFrame = document.createElement('iframe');
                iFrame.setAttribute('src', buildAuthorizationUrl({ 'auto_refresh': 'true'}));
                iFrame.setAttribute('id', 'oauth_frame');
                iFrame.style.display = 'none';
                document.body.appendChild(iFrame);
                $log.debug("Refreshing access token using iFrame");
            }

            if (!isOAuthFrame()) {
                var wrap = function(event) {
                    if (event.key === 'access_token' && event.newValue !== event.oldValue) {
                        var oldToken = event.oldValue;
                        var newToken = event.newValue;
                        // Token was renewed, cancel the lock
                        if (updateProgressTask) {
                            $interval.cancel(updateProgressTask);
                            updateProgressTask = undefined;
                            $window.localStorage['refresh_locked'] = undefined;
                            $window.localStorage['refresh_last_update'] = undefined;
                        }
                        // Schedule to renew
                        setTimeoutToRefresh();
                        // Broadcasting events
                        $rootScope.$broadcast('TokenChanged', oldToken, newToken);
                    }
                };

                // Listening for token change
                if ($window.addEventListener) {
                    $window.addEventListener('storage', wrap, false);
                } else {
                    $window.attachEvent('onstorage', wrap);
                }

                // If access token already stored in localStorage, setup an schedule to refresh
                if (getAccessToken()) {
                    setTimeoutToRefresh();
                }
            }
            
            return {
                login: login,
                logout: logout,
                getAccessToken: getAccessToken,
                loginHandler: loginHandler,
                extendConfig: this.extendConfig,
                clearToken: clearToken,
                verifyToken: verifyToken,
                setTokenFromLocation : setTokenFromLocation,
                clearUserInfo: clearUserInfo,
                getUserInfo: getUserInfo,
                setUserInfo: setUserInfo,
                getUserLanguage: getUserLanguage,
                setUserLanguage: setUserLanguage,
                isOAuthFrame: isOAuthFrame
            };
        };
    });

    tokenModule.run(function($rootScope, $log, $location, $state, $token, NO_AUTH_PATH, $translate) {

        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {

                if($location.search() && $location.search().access_token){
                    $token.setTokenFromLocation();
                }

                // OAuth iframe will be remove immediately after token was saved
                // So we navigate to dummy state and check nothing
                if ($token.isOAuthFrame()) {
                    event.preventDefault();
                    $state.go('404');
                    return;
                }

                // Load prefered language
                var lang = $token.getUserLanguage();
                if (!angular.isUndefinedOrNull(lang)) {
                    $translate.use(lang);
                }

                if ($rootScope.isNoAuthPage()) {
                    // No-auth, skip
                } else {
                    var access_token = $token.getAccessToken();
                    var tokenVerified = $rootScope.tokenVerified;

                    if (access_token) {
                        if ($token.getUserInfo() && tokenVerified) {
                            // It okay - do nothing
                            if (toState.name == 'home') {
                                event.preventDefault();
                                $state.go($rootScope.getHomeState($token.getUserInfo().roleCode));
                            } else {
                                if (!$rootScope.checkStateByRole($token.getUserInfo().roleCode, toState.name)) {
                                    event.preventDefault();
                                    $state.go('404');
                                }
                            }
                        } else {
                            var promise = $token.verifyToken();
                            promise.then(function (userinfo) {
                                $rootScope.tokenVerified = true;
                                if (toState.name == 'home') {
                                    event.preventDefault();
                                    $state.go($rootScope.getHomeState($token.getUserInfo().roleCode));
                                } else {
                                    if (!$rootScope.checkStateByRole($token.getUserInfo().roleCode, toState.name)) {
                                        event.preventDefault();
                                        $state.go('404');
                                    }
                                }
                                $log.log("Token was verified");
                            }, function (error) {
                                $log.log("Cannot verify token: " + error);
                                event.preventDefault();

                                // If error is undefined, this usual because server is offline
                                if (!error) {
                                    $state.go('500');
                                } else if (error.status === 401) {
                                    // Do nothing - this case will be handled in interceptor
                                } else {
                                    $state.go('500');
                                }
                            });
                        }
                    } else {
                        event.preventDefault();
                        $token.loginHandler();
                    }
                }
            });
    });
    
    
    tokenModule.config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push(function ($injector) {
            return {
                request: function (config) {
                    config.headers = config.headers || {};

                    $injector.invoke([
                        '$token', function($token) {
                            if ($token.getAccessToken()) {
                                config.headers.Authorization = 'Bearer ' + $token.getAccessToken();
                            }
                        }
                    ]);

                    return config;
                },
                responseError: function(resp) {
                    if(resp.status === 401) {
                        $injector.invoke([
                          '$token', function($token) {
                            $token.clearToken();
                            $token.clearUserInfo();
                            $token.loginHandler();
                          }
                        ]);
                    }
                    return $injector.get('$q').reject(resp);
                }
            };
        });

        }
    ]);
 
})();
