function initScopeForCategoryDetailPopup($scope, $modalInstance, $filter, logger, record, categoryName, config, Factory, transformBeforeSave, loadDependencies) {

    $scope.init = function() {
        $scope.categoryName = categoryName;
        $scope.who = config.who;
        $scope.useCode = config.useCode;

        var id;

        $scope.isChanged = false;

        if (record) {
            id = record.id;
            $scope.title = $filter('translate')('edit');
            $scope.action = 'edit';
            loadData(id);
        } else {
            $scope.title = $filter('translate')('new');
            $scope.action = 'new';

            $scope.record = {};

            if (loadDependencies != null && typeof(loadDependencies) == 'function') {
                loadDependencies($scope.record);
            }
        }
    };

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    function loadData(id) {
        $scope.proccessing = true;

        Factory.doGet(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: id
            },
            function(data) {
                if (angular.isUndefined(data)) {
                    logger.logError($filter('translate')('loading.error'));
                    $scope.proccessing = false;
                    $scope.cancel();
                } else {
                    $scope.record = data;
                    $scope.proccessing = false;

                    if (loadDependencies != null && typeof(loadDependencies) == 'function') {
                        loadDependencies($scope.record);
                    }
                }
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    $scope.ok = function () {
        //Validate
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            if (transformBeforeSave != null && typeof(transformBeforeSave) == 'function') {
                transformBeforeSave();
            }

            $scope.proccessing = true;
            // Edit record
            if ($scope.action == 'edit') {
                Factory.doPut(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName,
                        'id': $scope.record.id
                    },
                    $scope.record
                    ,
                    function () {
                        $scope.proccessing = false;
                        $modalInstance.close();
                        logger.logSuccess($filter('translate')('edit.success'));
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName
                    },
                    $scope.record
                    ,
                    function () {
                        $scope.proccessing = false;
                        $modalInstance.close();
                        logger.logSuccess($filter('translate')('create.success'));
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        }
    };

    $scope.enable = function () {
        //Validate
        if (angular.isUndefinedOrNull($scope.record)) {
            logger.logError($filter('translate')('data.input.not.valid'));
        } else {
            $scope.proccessing = true;
            // Edit record
            if ($scope.action == 'edit') {
                Factory.doPut(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName,
                        'id': $scope.record.id
                    },
                    $scope.record
                    ,
                    function () {
                        $scope.proccessing = false;
                        enable($scope.record.id);
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName
                    },
                    $scope.record
                    ,
                    function (data) {
                        $scope.proccessing = false;
                        enable(data.id);
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        }
    };

    $scope.isDraft = function() {
        return $scope.record == null || $scope.record.draft == null ||  $scope.record.draft;
    };

    $scope.isEdit = function() {
        return $scope.action == 'edit';
    };

    $scope.isNew = function() {
        return $scope.action == 'new';
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    function enable(domainId) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: domainId,
                action: 'enable'
            },
            null,
            function() {
                $scope.proccessing = false;
                logger.logSuccess($filter('translate')('enable.success'));
                $modalInstance.close();
            },
            function() {
                $scope.proccessing = false;
                logger.logError($filter('translate')('enable.error'));
            }
        );
    }

}
