function initScopeForNameCategoryByDistributorList($scope, $log, $filter, $location, $state, $stateParams, logger, $modal, Factory, STATE_CONFIG, $token, columns, transformAfterLoad) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.config = STATE_CONFIG[path];

        $scope.title = $scope.config.title;
        $scope.categoryName = path;

        var roleCodes = $token.getUserInfo().roleCodes;
        if (roleCodes != null && roleCodes.length > 0) {
            $scope.who = getWho(roleCodes[0]);
        }
    }

    function getWho(roleCode) {
        switch(roleCode) {
            case 'SUPER':
                return 'super-admin';
            case 'AD':
                return 'admin';
            case 'SUP':
                return 'supervisor';
            case 'DIS':
                return 'distributor';
            default:
                return null;
        }
    }

    $scope.init = function() {
        loadConfig();

        if (columns != null) {
            $scope.columns = columns;
        } else {
            $scope.columns = [ {
                    header: 'distributor',
                    property: function(record) {
                        if (record != null && record.distributor != null) {
                            return record.distributor.name;
                        } else {
                            return '';
                        }
                    }
                }
            ];
            $scope.columns.push({ header: 'name', property: 'name' });
            if ($scope.config.useCode) {
                $scope.columns.push({ header: 'code', property: 'code' });
            }
        }

        $scope.proccessing = false;
        $scope.error = false;
        $scope.loaded = false;

        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.global = {};
        $scope.global.searchText = $stateParams.search;
        $scope.global.currentDistributorId = $stateParams.distributor;

        $scope.loadDistributors();
    };

    $scope.loadDistributors = function() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.distributors = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'distributor',
                'subCategory': 'all'
            },
            function(data){
                $scope.distributors = data.list;
                if ($scope.distributors.length > 0) {
                    $scope.distributors.unshift({
                        id: null,
                        name: '-- ' + $filter('translate')('all') + ' --'
                    });

                    $scope.proccessing = false;
                    $scope.refresh();
                } else {
                    $scope.proccessing = false;
                }
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    $scope.changeParams = function() {
        if ($scope.loaded) {
            $scope.currentPage = 1;
            $state.go('.', getParams() );
        }
    };

    $scope.changePage = function() {
        if ($scope.loaded) {
            $state.go('.', getParams() );
        }
    };

    function getParams() {
        return {
            page: $scope.currentPage == 1 ? null : $scope.currentPage ,
            search: $scope.global.searchText == '' ? null : $scope.global.searchText,
            distributor: $scope.global.currentDistributorId
        };
    }

    $scope.refresh = function() {
        $scope.proccessing = true;
        $scope.error = false;

        var params = {
            who: $scope.who,
            category: $scope.categoryName,
            page: ($stateParams.page == null ? 1 : $stateParams.page),
            size: $scope.itemsPerPage,
            distributorId: $scope.global.currentDistributorId
        };

        if ($scope.global.searchText && $scope.global.searchText.length > 0) {
            params.q = angular.encodeURI($scope.global.searchText);
        }

        Factory.doGet(
            params,
            function(data) {
                $scope.records = data.list;
                $scope.totalItems = data.count;

                $scope.currentPage = $stateParams.page == null ? 1 : $stateParams.page;
                if ($scope.currentPage != null && $scope.currentPage > 1 && $scope.totalItems <= $scope.itemsPerPage) {
                    $scope.currentPage = null;
                    $state.go('.', getParams());
                }

                if (transformAfterLoad != null && typeof(transformAfterLoad) == 'function') {
                    transformAfterLoad();
                }

                $scope.proccessing = false;
                $scope.loaded = true;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);

                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    $scope.deleteRecord = function(record) {
        $scope.proccessing = true;

        Factory.doDelete(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: record.id
            },
            function() {
                logger.logSuccess($filter('translate')('delete.success'));
                $scope.proccessing = false;
                $scope.refresh();
            },
            function() {
                logger.logError($filter('translate')('delete.error'));
                $scope.proccessing = false;
            }
        );
    };

    $scope.enableRecord = function(record) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: record.id,
                action: 'enable'
            },
            null,
            function() {
                logger.logSuccess($filter('translate')('enable.success'));
                $scope.proccessing = false;
                $scope.refresh();
            },
            function() {
                logger.logError($filter('translate')('enable.error'));
                $scope.proccessing = false;
            }
        );
    };

    $scope.setActiveRecord = function(record) {
        $scope.proccessing = true;
        var action = record.active ? 'deactivate' : 'activate';

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: record.id,
                action: action
            },
            null,
            function() {
                logger.logSuccess($filter('translate')(action + '.success'));
                $scope.proccessing = false;
                $scope.refresh();
            },
            function(error) {
                logger.logError($filter('translate')(action + '.error'));
                $scope.proccessing = false;
            }
        );
    };

    $scope.editRecord = function(record) {
        if ($scope.config.isUsePopup) {
            var detailTemplate = 'mvc/view/common/category-detail-popup.html';
            if ($scope.config.detailTemplate != null) {
                detailTemplate = $scope.config.detailTemplate;
            }

            var detailCtrl = 'CategoryDetailPopupCtrl';
            if ($scope.config.detailCtrl != null) {
                detailCtrl = $scope.config.detailCtrl;
            }


            var modalInstance = $modal.open({
                templateUrl: detailTemplate,
                controller: detailCtrl,
                resolve: {
                    record: function () {
                        return record;
                    },
                    categoryName: function() {
                        return $scope.categoryName;
                    },
                    config: function() {
                        return $scope.config;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.refresh();
            }, function () {
            });
        } else {
            var params = getParams();
            params.id = record.id;
            $state.go( $scope.categoryName + '-detail', params );
        }
    };

    $scope.createRecord = function() {
        if ($scope.config.isUsePopup) {
            var detailTemplate = 'mvc/view/common/category-detail-popup.html';
            if ($scope.config.detailTemplate != null) {
                detailTemplate = $scope.config.detailTemplate;
            }

            var detailCtrl = 'CategoryDetailPopupCtrl';
            if ($scope.config.detailCtrl != null) {
                detailCtrl = $scope.config.detailCtrl;
            }

            var modalInstance = $modal.open({
                templateUrl: detailTemplate,
                controller: detailCtrl,
                resolve: {
                    record: function () {
                        return null;
                    },
                    categoryName: function() {
                        return $scope.categoryName;
                    },
                    config: function() {
                        return $scope.config;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.refresh();
            }, function () {});
        } else {
            $state.go( $scope.categoryName + '-detail', { id: 'new' } );
        }
    };

    $scope.getPropertyValue = function(column, record) {
        if (column != null && column.property != null) {
            if (typeof(column.property) == 'function') {
                return column.property(record);
            } else {
                return record[column.property];
            }
        }
    }

}
