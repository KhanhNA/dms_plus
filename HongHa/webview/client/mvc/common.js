var app = angular.module('ViettelApp', [
    'ngRoute',
    'ngResource',
    'pascalprecht.translate',
    'vt.token',
    'vt.directive',
    'vt.push',
    'vt.logger',
    'vt.fillter',
    'ui.bootstrap',
    'angular-loading-bar',
    'ui.router',
    'ngIdle',
    'ui.select',
    'ngSanitize',
    'uiGmapgoogle-maps',
    'fcsa-number',
    'dialogs.main'
]);

//CONSTANT
//IP BACKEND
app.constant("ADDRESS_BACKEND", "http://localhost:8080/backend/api/");
app.constant("ADDRESS_OAUTH", "http://localhost:8080/backend/");
app.constant("ADDRESS_SOCKET", "http://localhost:8080/backend/websocket");
app.constant("ADDRESS_PING", "http://localhost:8080/backend/account/ping");

app.constant("NO_AUTH_PATH", ['/404', '/500']);
app.constant("FULL_SCREEN_PATH", ['/404', '/500']);

// Time out: 30 mins
app.constant("NG_IDLE_MAX_IDLE_TIME", 60 * 30);
// Count down time before logout: 8 secs
app.constant("NG_IDLE_TIMEOUT", 8);
// Auto-ping interval: 10 mins
app.constant("NG_IDLE_PING_INTEVAL", 60 * 10);

// Default language
app.constant("DEFAULT_LANGUAGE", "en");

//CONFIG FOR SIMPLE CATEGORY
app.constant("PATTERN", {
    bigdecimal_positive: '/^[0-9]*[\\.]?[0-9]*$/i',
    integer_positive: '/^[0-9]*$/i'
});

//STATE CONFIG
app.constant("STATE_CONFIG", {
    404: {
        isSpecial: true,
        template: 'mvc/view/404.html'
    },
    500: {
        isSpecial: true,
        template: 'mvc/view/500.html'
    },
    'no-function': {
        isSpecial: true,
        template: 'mvc/view/common/no-function-warning.html'
    },
    'change-password': {
        isSpecial: true,
        template: 'mvc/view/common/change-password.html',
        ctrl: 'ChangePasswordCtrl'
    },
    //SUPER ADMIN
    client: {
        isUsePopup: true,
        title: 'menu.client',
        who: 'super-admin'
    },
    'system-config': {
        isSpecial: true,
        template: 'mvc/view/superadmin/client-config.html',
        ctrl: 'ClientConfigCtrl'
    },
    system: {
        isSpecial: true,
        template: 'mvc/view/superadmin/system.html',
        ctrl: 'SystemCtrl'
    },
    //ADMIN
    'admin-homepage': {
        isSpecial: true,
        template: 'mvc/view/report/homepage.html',
        ctrl: 'HomepageCtrl'
    },
    user: {
        isUsePopup: true,
        title: 'menu.user',
        who: 'admin',
        listCtrl: 'UserCtrl',
        detailTemplate: 'mvc/view/admin/user-detail-popup.html',
        detailCtrl: 'UserDetailPopupCtrl'
    },
    salesconfig: {
        isSpecial: true,
        template: 'mvc/view/admin/sales-config.html',
        ctrl: 'SalesConfigCtrl'
    },
    calendarconfig: {
        isSpecial: true,
        template: 'mvc/view/admin/calendar-config.html',
        ctrl: 'CalendarConfigCtrl'
    },
    uom: {
        isUsePopup: true,
        title: 'menu.uom',
        who: 'admin',
        useCode: true
    },
    productcategory: {
        isUsePopup: true,
        title: 'menu.product.category',
        who: 'admin'
    },
    product: {
        isUsePopup: false,
        title: 'menu.product',
        who: 'admin',
        listCtrl: 'ProductCtrl',
        detailTemplate: 'mvc/view/admin/product-detail.html',
        detailCtrl: 'ProductDetailCtrl'
    },
    customertype: {
        isUsePopup: true,
        title: 'menu.customer.type',
        who: 'admin'
    },
    district: {
        isUsePopup: true,
        title: 'menu.district',
        who: 'admin'
    },
    customer: {
        isUsePopup: false,
        title: 'menu.customer',
        who: 'admin',
        listCtrl: 'CustomerCtrl',
        detailTemplate: 'mvc/view/admin/customer-detail.html',
        detailCtrl: 'CustomerDetailCtrl'
    },
    'import-customer': {
        isSpecial: true,
        template: 'mvc/view/admin/import-customer.html',
        ctrl: 'ImportCustomerCtrl'
    },
    'import-product': {
        isSpecial: true,
        template: 'mvc/view/admin/import-product.html',
        ctrl: 'ImportProductCtrl'
    },
    distributor: {
        isUsePopup: false,
        title: 'menu.distributor',
        who: 'admin',
        useCode: true,
        detailTemplate: 'mvc/view/admin/distributor-detail.html',
        detailCtrl: 'DistributorDetailCtrl'
    },
    promotion: {
        isUsePopup: false,
        title: 'menu.promotion',
        who: 'admin',
        listCtrl: 'PromotionCtrl',
        detailTemplate: 'mvc/view/admin/promotion-detail.html',
        detailCtrl: 'PromotionDetailCtrl'
    },
    survey: {
        isUsePopup: false,
        title: 'menu.survey',
        who: 'admin',
        listCtrl: 'SurveyCtrl',
        detailTemplate: 'mvc/view/admin/survey-detail.html',
        detailCtrl: 'SurveyDetailCtrl'
    },
    exhibition: {
        isUsePopup: false,
        title: 'menu.exhibition',
        who: 'admin',
        listCtrl: 'SurveyCtrl',
        detailTemplate: 'mvc/view/admin/exhibition-detail.html',
        detailCtrl: 'ExhibitionDetailCtrl'
    },
    'admin-dashboard-visit': {
        isSpecial: true,
        url: '/admin-dashboard-visit?salesmanid',
        template: 'mvc/view/report/dashboard-visit.html',
        ctrl: 'DashboardVisitCtrl'
    },
    'admin-dashboard-visit-detail': {
        isSpecial: true,
        url: '/admin-dashboard-visit-detail/:id?returnSalesmanId',
        template: 'mvc/view/report/dashboard-visit-detail.html',
        ctrl: 'DashboardVisitDetailCtrl'
    },
    'admin-visit-tracking': {
        isSpecial: true,
        template: 'mvc/view/supervisor/visit-tracking.html',
        ctrl: 'ADVisitTrackingCtrl'
    },
    'admin-sales-report': {
        isSpecial: true,
        url: '/admin-sales-report',
        template: 'mvc/view/report/sales-report.html',
        ctrl: 'SalesReportCtrl'
    },
    'admin-sales-report-daily': {
        isSpecial: true,
        url: '/admin-sales-report-daily?month&year',
        template: 'mvc/view/report/sales-report-daily.html',
        ctrl: 'SalesReportDailyCtrl'
    },
    'admin-sales-report-distributor': {
        isSpecial: true,
        url: '/admin-sales-report-distributor?month&year',
        template: 'mvc/view/report/sales-report-distributor.html',
        ctrl: 'SalesReportDistributorCtrl'
    },
    'admin-sales-report-product': {
        isSpecial: true,
        url: '/admin-sales-report-product?month&year&productCategoryId',
        template: 'mvc/view/report/sales-report-product.html',
        ctrl: 'SalesReportProductCtrl'
    },
    'admin-sales-report-salesman': {
        isSpecial: true,
        url: '/admin-sales-report-salesman?month&year&salesmanId',
        template: 'mvc/view/report/sales-report-salesman.html',
        ctrl: 'SalesReportSalesmanCtrl'
    },
    'admin-visit-report': {
        isSpecial: true,
        url: '/admin-visit-report',
        template: 'mvc/view/report/report-visit-monthly.html',
        ctrl: 'VisitReportCtrl'
    },
    'admin-visit-report-detail': {
        isSpecial: true,
        url: '/admin-visit-report-detail?fromDate&toDate&salesmanId',
        template: 'mvc/view/report/report-visit-monthly-detail.html',
        ctrl: 'VisitReportDetailCtrl'
    },
    'admin-report-survey': {
        isUsePopup: false,
        listTemplate: 'mvc/view/report/report-survey-list.html',
        listCtrl: 'ReportSurveyCtrl',
        detailTemplate: 'mvc/view/report/report-survey-detail.html',
        detailCtrl: 'ReportSurveyDetailCtrl'
    },
    'admin-report-exhibition': {
        isUsePopup: false,
        listTemplate: 'mvc/view/report/report-exhibition-list.html',
        listCtrl: 'ReportExhibitionCtrl',
        detailTemplate: 'mvc/view/report/report-exhibition-detail.html',
        detailCtrl: 'ReportExhibitionDetailCtrl'
    },
    'admin-order-list': {
        isSpecial: true,
        url: '/admin-order-list?fromDate&toDate&distributorId&page',
        template: 'mvc/view/report/order-list.html',
        ctrl: 'OrderListCtrl'
    },
    'admin-order-detail': {
        isSpecial: true,
        url: '/admin-order-detail/:id',
        template: 'mvc/view/report/order-detail.html',
        ctrl: 'OrderDetailCtrl'
    },
    'distributor-edit': {
        isSpecial: true,
        template: 'mvc/view/admin/distributor-edit.html',
        ctrl: 'DistributorEditCtrl'
    },
    'admin-order-report-summary': {
        isSpecial: true,
        url: '/admin-order-report-summary',
        template: 'mvc/view/report/order-report.html',
        ctrl: 'OrderReportCtrl'
    },
    'admin-order-report-detail': {
        isSpecial: true,
        url: '/admin-order-report-detail',
        template: 'mvc/view/report/order-report.html',
        ctrl: 'OrderReportCtrl'
    },
    // Supervisor
    'customer-approve': {
        title: 'menu.approve.customer',
        isUsePopup: false,
        listTemplate: 'mvc/view/supervisor/customer-approve.html',
        listCtrl: 'SUPCustomerApproveCtrl',
        detailTemplate: 'mvc/view/supervisor/customer-approve-detail.html',
        detailCtrl: 'SUPCustomerApproveDetailCtrl'
    },
    'customer-create': {
        isSpecial: true,
        who: 'supervisor',
        template: 'mvc/view/admin/customer-detail.html',
        ctrl: 'CustomerDetailCtrl'
    },
    feedback: {
        title: 'menu.feedback',
        isUsePopup: false,
        listTemplate: 'mvc/view/supervisor/feedback-list.html',
        listCtrl: 'SUPFeedbackListCtrl',
        detailTemplate: 'mvc/view/supervisor/feedback-detail.html',
        detailCtrl: 'SUPFeedbackDetailCtrl'
    },
    'supervisor-homepage': {
        isSpecial: true,
        template: 'mvc/view/report/homepage.html',
        ctrl: 'HomepageCtrl'
    },
    'supervisor-dashboard-visit': {
        isSpecial: true,
        url: '/supervisor-dashboard-visit?salesmanid',
        template: 'mvc/view/report/dashboard-visit.html',
        ctrl: 'DashboardVisitCtrl'
    },
    'supervisor-dashboard-visit-detail': {
        isSpecial: true,
        url: '/supervisor-dashboard-visit-detail/:id?returnSalesmanId',
        template: 'mvc/view/report/dashboard-visit-detail.html',
        ctrl: 'DashboardVisitDetailCtrl'
    },
    'supervisor-sales-report': {
        isSpecial: true,
        url: '/supervisor-sales-report',
        template: 'mvc/view/report/sales-report.html',
        ctrl: 'SalesReportCtrl'
    },
    'supervisor-sales-report-daily': {
        isSpecial: true,
        url: '/supervisor-sales-report-daily?month&year',
        template: 'mvc/view/report/sales-report-daily.html',
        ctrl: 'SalesReportDailyCtrl'
    },
    'supervisor-sales-report-distributor': {
        isSpecial: true,
        url: '/supervisor-sales-report-distributor?month&year',
        template: 'mvc/view/report/sales-report-distributor.html',
        ctrl: 'SalesReportDistributorCtrl'
    },
    'supervisor-sales-report-product': {
        isSpecial: true,
        url: '/supervisor-sales-report-product?month&year&productCategoryId',
        template: 'mvc/view/report/sales-report-product.html',
        ctrl: 'SalesReportProductCtrl'
    },
    'supervisor-sales-report-salesman': {
        isSpecial: true,
        url: '/supervisor-sales-report-salesman?month&year&salesmanId',
        template: 'mvc/view/report/sales-report-salesman.html',
        ctrl: 'SalesReportSalesmanCtrl'
    },
    'supervisor-visit-report': {
        isSpecial: true,
        url: '/supervisor-visit-report',
        template: 'mvc/view/report/report-visit-monthly.html',
        ctrl: 'VisitReportCtrl'
    },
    'supervisor-visit-report-detail': {
        isSpecial: true,
        url: '/supervisor-visit-report-detail?fromDate&toDate&salesmanId',
        template: 'mvc/view/report/report-visit-monthly-detail.html',
        ctrl: 'VisitReportDetailCtrl'
    },
    'supervisor-report-survey': {
        isUsePopup: false,
        listTemplate: 'mvc/view/report/report-survey-list.html',
        listCtrl: 'ReportSurveyCtrl',
        detailTemplate: 'mvc/view/report/report-survey-detail.html',
        detailCtrl: 'ReportSurveyDetailCtrl'
    },
    'supervisor-report-exhibition': {
        isUsePopup: false,
        listTemplate: 'mvc/view/report/report-exhibition-list.html',
        listCtrl: 'ReportExhibitionCtrl',
        detailTemplate: 'mvc/view/report/report-exhibition-detail.html',
        detailCtrl: 'ReportExhibitionDetailCtrl'
    },
    'supervisor-order-list': {
        isSpecial: true,
        url: '/supervisor-order-list?fromDate&toDate&distributorId',
        template: 'mvc/view/report/order-list.html',
        ctrl: 'OrderListCtrl'
    },
    'supervisor-order-detail': {
        isSpecial: true,
        url: '/supervisor-order-detail/:id',
        template: 'mvc/view/report/order-detail.html',
        ctrl: 'OrderDetailCtrl'
    },
    target: {
        isSpecial: true,
        template: 'mvc/view/supervisor/target.html',
        ctrl: 'SUPTargetListCtrl'
    },
    'visit-tracking': {
        isSpecial: true,
        template: 'mvc/view/supervisor/visit-tracking.html',
        ctrl: 'SUPVisitTrackingCtrl'
    },
    // ADMIN + SUPERVISOR
    'route': {
        isUsePopup: false,
        title: 'menu.route',
        listTemplate: 'mvc/view/common/name-category-by-distributor-list.html',
        listCtrl: 'NameCategoryByDistributorListCtrl',
        detailTemplate: 'mvc/view/report/route-detail.html',
        detailCtrl: 'RouteDetailCtrl'
    },
    'customer-schedule': {
        isSpecial: true,
        template: 'mvc/view/report/customer-schedule.html',
        ctrl: 'CustomerScheduleCtrl'
    },
    'import-schedule': {
        isSpecial: true,
        template: 'mvc/view/admin/import-schedule.html',
        ctrl: 'ImportScheduleCtrl'
    },
    // End supervisor
    // DISTRIBUTOR
    'orders-approve': {
        title: 'purchase.order',
        isUsePopup: false,
        listTemplate: 'mvc/view/distributor/order-approve.html',
        listCtrl: 'DISOrderApproveCtrl',
        detailTemplate: 'mvc/view/distributor/order-approve-detail.html',
        detailCtrl: 'DISOrderApproveDetailCtrl'
    },
    'distributor-order-list': {
        isSpecial: true,
        url: '/distributor-order-list?fromDate&toDate&distributorId&page',
        template: 'mvc/view/report/order-list.html',
        ctrl: 'OrderListCtrl'
    },
    'distributor-order-detail': {
        isSpecial: true,
        url: '/distributor-order-detail/:id',
        template: 'mvc/view/report/order-detail.html',
        ctrl: 'OrderDetailCtrl'
    },
    'distributor-display-check-report': {
        isSpecial: true,
        template: 'mvc/view/report/display-check-report.html',
        ctrl: 'DisplayCheckReportCtrl'
    }
});

//STATE CONFIG
app.constant("STATE_BY_ROLE_CONFIG", {
    sm: [
        'no-function'
    ],
    sc: [
        'no-function'
    ],
    super: [
        'client',
        'system-config',
        'system',
        'change-password'
    ],
    ad: [
        'admin-homepage',
        'user',
        'salesconfig',
        'calendarconfig',
        'uom',
        'productcategory',
        'product',
        'district',
        'customertype',
        'customer',
        'feedbacktype',
        'supervisetree',
        'distributor',
        'purchaseorder',
        'promotion',
        'survey',
        'system-config',
        'exhibition',
        'admin-visit-report',
        'admin-sales-report',
        'admin-sales-report-daily',
        'admin-sales-report-distributor',
        'admin-sales-report-product',
        'admin-sales-report-salesman',
        'admin-dashboard-visit',
        'admin-dashboard-visit-detail',
        'admin-visit-tracking',
        'admin-visit-report-detail',
        'admin-order-list',
        'admin-order-detail',
        'admin-report-survey',
        'admin-report-exhibition',
        'change-password',
        'admin-order-report-summary',
        'admin-order-report-detail',
        'distributor-edit',
        'import-customer',
        'import-product',
        'route',
        'customer-schedule',
        'import-schedule'
    ],
    sup: [
        'supervisor-homepage',
        'customer-approve',
        'customer-create',
        'target',
        'feedback',
        'visit-tracking',
        'supervisor-sales-report',
        'supervisor-sales-report-daily',
        'supervisor-sales-report-distributor',
        'supervisor-sales-report-product',
        'supervisor-sales-report-salesman',
        'supervisor-dashboard-visit',
        'supervisor-dashboard-visit-detail',
        'supervisor-visit-report',
        'supervisor-visit-report-detail',
        'supervisor-order-list',
        'supervisor-order-detail',
        'supervisor-report-survey',
        'supervisor-report-exhibition',
        'change-password',
        'route',
        'customer-schedule',
        'import-schedule'
    ],
    dis: [
        'orders-approve-list',
        'orders-approve-detail',
        'change-password',
        'distributor-order-list',
        'distributor-order-detail',
        'distributor-display-check-report'
    ]
});

app.config(function($tokenProvider, $translateProvider, $provide, $urlRouterProvider, $stateProvider, $vtpushProvider,
        ADDRESS_BACKEND, ADDRESS_OAUTH, ADDRESS_SOCKET, STATE_CONFIG, DEFAULT_LANGUAGE) {
    
    // Configure $token service for login management
    var defaultParams = {
        'client_id': 'vinacomin',
        'authorizationEndpoint': ADDRESS_OAUTH + "oauth/authorize",
        'scope': "read",
        'revokeTokenEndpoint': ADDRESS_OAUTH + "oauth",
        'verifyTokenEndpoint': ADDRESS_OAUTH + 'oauth/userinfo',
        'logoutUrl': ADDRESS_OAUTH + 'account/logout',
        'refresh': false
    };
    $tokenProvider.extendConfig(defaultParams);

    // Configure WebSockets
    $vtpushProvider.set('SOCKET_URL', ADDRESS_SOCKET);

    // Configure file manager
    $provide.decorator('vtFileManagerConfig', function($token, ADDRESS_BACKEND) {
        return {
            getBaseUrl: function() {
                return ADDRESS_BACKEND;
            },

            getAccessToken: function() {
                return $token.getAccessToken();
            },

            uploadUrl: ADDRESS_BACKEND + 'image',

            getDownloadUrl: function(fileId) {
                return ADDRESS_BACKEND + 'image/' + fileId;
            },
            getDeleteUrl: function(fileId) {
                return ADDRESS_BACKEND + 'file?id=' + fileId + '&access_token=' + $token.getAccessToken();
            },
            getHeaders: function() {
                return {
                    Authorization: 'Bearer ' + $token.getAccessToken()
                };
            }
        };
    });

    // Configure multi-language support
    $translateProvider.preferredLanguage(DEFAULT_LANGUAGE);
    $translateProvider.useStaticFilesLoader({
        prefix: 'message/',
        suffix: '.json'
    });

    // Configure view states
    $urlRouterProvider.when("", "/");
    $urlRouterProvider.when("/", "/home");
    $urlRouterProvider.otherwise("/404");

    $stateProvider.state('home', { url: '/home' });

    //STATE
    for (var statename in STATE_CONFIG) {
        if (STATE_CONFIG.hasOwnProperty(statename)) {
            var config = STATE_CONFIG[statename];
            if (config.isSpecial) {
                var state = {
                    url: ("/" + statename + '?filter&parentFilter')
                };

                state.templateUrl = config.template;

                if (config.ctrl != null) {
                    state.controller = config.ctrl;

                    if (config.url != null) {
                        state.url = config.url;
                    }
                }

                $stateProvider.state(statename, state);
            } else {
                var stateList = {
                    url: ('/' + statename + '?page&search&distributor'),
                    templateUrl: 'mvc/view/common/category-list.html',
                    controller: 'CategoryListCtrl'
                };

                if (config.url != null) {
                    stateList.url = config.url;
                }

                if (config.listCtrl != null) {
                    stateList.controller = config.listCtrl;
                }
                if (config.listTemplate != null) {
                    stateList.templateUrl = config.listTemplate;
                }

                if (config.isUsePopup) {
                    $stateProvider.state(statename, stateList);
                } else {
                    $stateProvider.state(statename + '-list', stateList);

                    $stateProvider.state(statename + '-detail', {
                        url: ("/" + statename + "/:id?page&search&distributor"),
                        templateUrl: config.detailTemplate,
                        controller: config.detailCtrl
                    });
                }
            }
        }
    }
});

// Configure Idle detector and Auto-ping service to keep session alive
app.config(['KeepaliveProvider', 'IdleProvider', 'NG_IDLE_MAX_IDLE_TIME', 'NG_IDLE_TIMEOUT', 'NG_IDLE_PING_INTEVAL',
        function(KeepaliveProvider, IdleProvider, NG_IDLE_MAX_IDLE_TIME, NG_IDLE_TIMEOUT, NG_IDLE_PING_INTEVAL) {
    // Time out
    IdleProvider.idle(NG_IDLE_MAX_IDLE_TIME);

    // Count down time before logout
    IdleProvider.timeout(NG_IDLE_TIMEOUT);

    // Auto-ping interval
    KeepaliveProvider.interval(NG_IDLE_PING_INTEVAL);
}]);

// GOOGLE MAPS
app.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyCcB41NEvzyRhP5xjGzjTtsMv7Orw2AA8E',
        v: '3.17',
        libraries: 'places,drawing,visualization'
    });
});

app.run(function($rootScope, Idle, NO_AUTH_PATH, FULL_SCREEN_PATH, STATE_BY_ROLE_CONFIG, $location, $token, $translate, DEFAULT_LANGUAGE) {
    // Start listening to user interaction to detect time-out event
    Idle.watch();

    // Notify some view when page ready
    // eg: Remove loading indicator
    $rootScope.loaded = true;

    // Load saved language
    var lang = $token.getUserLanguage();
    if (!angular.isUndefinedOrNull(lang)) {
        $translate.use(lang);
    } else {
        $translate.use(DEFAULT_LANGUAGE);
    }

    $rootScope.isNoAuthPage = function () {
        var path = $location.path();
        return _.contains(NO_AUTH_PATH, path) || /^[\/]?error=.*$/.test(path);
    };

    $rootScope.isFullScreenPage = function() {
        var path = $location.path();
        return _.contains(FULL_SCREEN_PATH, path);
    };

    $rootScope.checkStateByRole = function(roleCode, statename) {
        if (angular.isUndefinedOrNull(roleCode)  || angular.isUndefinedOrNull(statename)) {
            return false;
        }

        if (statename == '404' || statename == '500') {
            return true;
        }

        if (roleCode != null && STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()] != null) {
            if (STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()].indexOf(statename) >= 0) {
                return true;
            }

            if (angular.endsWith(statename, '-detail')) {
                if ( STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()].indexOf(statename.substring(0, statename.length - 7)) >= 0) {
                    return true;
                }
            }

            if (angular.endsWith(statename, '-list')) {
                if ( STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()].indexOf(statename.substring(0, statename.length - 5)) >= 0) {
                    return true;
                }
            }
        }

        return false;
    };

    $rootScope.getHomeState = function(roleCode) {
        if (angular.isUndefinedOrNull(roleCode)) {
            return '404';
        }

        if (roleCode != null) {
            if (STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()] != null && STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()].length > 0) {
                return STATE_BY_ROLE_CONFIG[roleCode.toLowerCase()][0];
            }
        }

        return '404';
    };

});

app.service('ImageService', function(ADDRESS_BACKEND) {
    this.getImageUrl = function(imageId) {
        if (angular.isUndefinedOrNull(imageId)) {
            return undefined;
        }
        return ADDRESS_BACKEND + 'image/' + imageId;
    }
});