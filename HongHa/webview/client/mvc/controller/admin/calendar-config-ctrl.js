app.controller('CalendarConfigCtrl', function($scope, $filter, $token, logger, Factory) {

    function init() {
        $scope.who = 'admin';
        $scope.categoryName = 'calendarconfig';

        $scope.proccessing = false;
        $scope.error = false;

        initSimpleDateModel();

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.isChanged = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': $scope.categoryName
            },
            function(data){
                $scope.calendarConfig = data;

                transformAfterLoad();

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    function transformAfterLoad() {
        if (!angular.isUndefinedOrNull($scope.calendarConfig)){
            if (angular.isUndefinedOrNull($scope.calendarConfig.workingDays)) {
                $scope.calendarConfig.workingDays = [];
            }
            $scope.calendarConfig.sunday = $scope.calendarConfig.workingDays.indexOf(1) >= 0;
            $scope.calendarConfig.monday = $scope.calendarConfig.workingDays.indexOf(2) >= 0;
            $scope.calendarConfig.tuesday = $scope.calendarConfig.workingDays.indexOf(3) >= 0;
            $scope.calendarConfig.wednesday = $scope.calendarConfig.workingDays.indexOf(4) >= 0;
            $scope.calendarConfig.thursday = $scope.calendarConfig.workingDays.indexOf(5) >= 0;
            $scope.calendarConfig.friday = $scope.calendarConfig.workingDays.indexOf(6) >= 0;
            $scope.calendarConfig.saturday = $scope.calendarConfig.workingDays.indexOf(7) >= 0;


            if (angular.isUndefinedOrNull($scope.calendarConfig.everyYearHolidays)) {
                $scope.calendarConfig.everyYearHolidays = [];
            }

            if (angular.isUndefinedOrNull($scope.calendarConfig.exceptionHolidays)) {
                $scope.calendarConfig.exceptionHolidays = [];
            } else {
                var exceptionHolidays = [];
                angular.forEach($scope.calendarConfig.exceptionHolidays, function(exceptionHoliday) {
                    var date = new Date(exceptionHoliday.year, exceptionHoliday.month, exceptionHoliday.date);
                    exceptionHolidays.push( { date: date, opened: false } );
                });
                $scope.calendarConfig.exceptionHolidays = exceptionHolidays;
            }

            if (angular.isUndefinedOrNull($scope.calendarConfig.exceptionWorkingDays)) {
                $scope.calendarConfig.exceptionWorkingDays = [];
            } else {
                var exceptionWorkingDays = [];
                angular.forEach($scope.calendarConfig.exceptionWorkingDays, function(exceptionWorkingDay) {
                    var date = new Date(exceptionWorkingDay.year, exceptionWorkingDay.month, exceptionWorkingDay.date);
                    exceptionWorkingDays.push( { date: date, opened: false } );
                });
                $scope.calendarConfig.exceptionWorkingDays = exceptionWorkingDays;
            }
        }
    }

    function transformBeforeSave() {
        if (!angular.isUndefinedOrNull($scope.calendarConfig)){
            if (!angular.isUndefinedOrNull($scope.calendarConfig.workingDays)) {
                $scope.calendarConfig.workingDays = [];
                if ($scope.calendarConfig.sunday) {$scope.calendarConfig.workingDays.push(1);}
                if ($scope.calendarConfig.monday) {$scope.calendarConfig.workingDays.push(2);}
                if ($scope.calendarConfig.tuesday) {$scope.calendarConfig.workingDays.push(3);}
                if ($scope.calendarConfig.wednesday) {$scope.calendarConfig.workingDays.push(4);}
                if ($scope.calendarConfig.thursday) {$scope.calendarConfig.workingDays.push(5);}
                if ($scope.calendarConfig.friday) {$scope.calendarConfig.workingDays.push(6);}
                if ($scope.calendarConfig.saturday) {$scope.calendarConfig.workingDays.push(7);}
            }

            if (!angular.isUndefinedOrNull($scope.calendarConfig.exceptionHolidays)) {
                var exceptionHolidays = [];
                angular.forEach($scope.calendarConfig.exceptionHolidays, function (exceptionHoliday) {
                    if (exceptionHoliday.date != null) {
                        exceptionHolidays.push({
                            date: exceptionHoliday.date.getDate(),
                            month: exceptionHoliday.date.getMonth(),
                            year: exceptionHoliday.date.getFullYear()
                        });
                    }
                });
                $scope.calendarConfig.exceptionHolidays = exceptionHolidays;
            }

            if (!angular.isUndefinedOrNull($scope.calendarConfig.exceptionWorkingDays)) {
                var exceptionWorkingDays = [];
                angular.forEach($scope.calendarConfig.exceptionWorkingDays, function(exceptionWorkingDay) {
                    if (exceptionWorkingDay.date != null) {
                        exceptionWorkingDays.push({
                            date: exceptionWorkingDay.date.getDate(),
                            month: exceptionWorkingDay.date.getMonth(),
                            year: exceptionWorkingDay.date.getFullYear()
                        });
                    }
                });
                $scope.calendarConfig.exceptionWorkingDays = exceptionWorkingDays;
            }
        }
    }

    $scope.createEveryYearHoliday = function() {
        $scope.calendarConfig.everyYearHolidays.push({});
        $scope.markAsChanged();
    };

    $scope.createExceptionHoliday = function() {
        $scope.calendarConfig.exceptionHolidays.push({ date: null, opened: false });
        $scope.markAsChanged();
    };

    $scope.createExceptionWorkingDay = function() {
        $scope.calendarConfig.exceptionWorkingDays.push({ date: null, opened: false });
        $scope.markAsChanged();
    };

    $scope.deleteFromList = function(list, item) {
        var index = list.indexOf(item);
        if (index >= 0) {
            list.splice(index, 1);
        }
        $scope.markAsChanged();
    };

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
        $scope.markAsChanged();
    };

    $scope.save = function() {
        $scope.proccessing = true;
        $scope.error = false;

        transformBeforeSave();

        Factory.doPut(
            {
                'who': $scope.who,
                'category': $scope.categoryName
            },
            $scope.calendarConfig,
            function(){
                $scope.proccessing = false;
                logger.logSuccess($filter('translate')('save.success'));
                $scope.refresh();
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    function initSimpleDateModel() {
        $scope.simpleDateDates = {
            0: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
            1: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29],
            2: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
            3: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
            4: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
            5: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
            6: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
            7: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
            8: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
            9: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
            10: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
            11: [1 ,2 ,3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
        };

        $scope.simpleDateMonths = [
            { name: 'month.jan', value: 0 },
            { name: 'month.feb', value: 1 },
            { name: 'month.mar', value: 2 },
            { name: 'month.apr', value: 3 },
            { name: 'month.may', value: 4 },
            { name: 'month.jun', value: 5 },
            { name: 'month.jul', value: 6 },
            { name: 'month.aug', value: 7 },
            { name: 'month.sep', value: 8 },
            { name: 'month.oct', value: 9},
            { name: 'month.nov', value: 10},
            { name: 'month.dec', value: 11}
        ];
    }

    init();

});


