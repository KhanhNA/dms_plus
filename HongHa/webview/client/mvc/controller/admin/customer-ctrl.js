app.controller('CustomerCtrl', function($scope, $log, $filter, $location, $state, $stateParams, logger, $modal, Factory, STATE_CONFIG) {

    function init() {
        var columns = [
            { header: 'name', property: 'name' },
            { header: 'code', property: 'code' },
            { header: 'customer.type', property: function(record) { return record.customerType.name; } }
        ];

        initScopeForCategoryList($scope, $log, $filter, $location, $state, $stateParams, logger, $modal, Factory, STATE_CONFIG, columns, null);

        $scope.init();

        $scope.importLink = "#/import-customer"
    }

    init();

});
