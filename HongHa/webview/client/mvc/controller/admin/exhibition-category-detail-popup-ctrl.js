app.controller('ExhibitionCategoryDetailPopupCtrl', function ($scope, $modalInstance, $filter, logger, category) {

    function init() {
        $scope.title = $filter('translate')('exhibition.category');

        if (category == null) {
            $scope.category = { name: null, items : [] };
        } else {
            $scope.originalCategory = category;
            $scope.category = angular.copy(category);
        }
    }

    $scope.ok = function () {
        //Validate
        if (!$scope.form.$valid || !checkValidBeforeSave()) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            if ($scope.originalCategory != null) {
                $scope.originalCategory.name = $scope.category.name;
                $scope.originalCategory.items = $scope.category.items;

                $modalInstance.close();
            } else {
                $modalInstance.close($scope.category);
            }
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.createItem = function() {
        $scope.category.items.push( { name: null } );
    };

    $scope.deleteItem = function(index) {
        $scope.category.items.splice(index, 1);
    };

    function checkValidBeforeSave() {
        if (angular.isUndefinedOrNull($scope.category) || angular.isUndefinedOrNull($scope.category.items)
            || $scope.category.items.length == 0) {
            return false;
        }

        return true;
    }

    init();

});
