app.controller('ExhibitionCtrl', function($scope, $log, $filter, $location, $state, $stateParams, logger, $modal, Factory, STATE_CONFIG) {

    function init() {
        function getDateDisplay(isoDate) {
            var date =  angular.parseDate(isoDate);

            if (date == null) {
                return '';
            }

            return moment(date).format('DD/MM/YYYY');
        }

        var columns = [
            { header: 'name', property: 'name' },
            { header: 'exhibition.start.date', property: function(record) { return getDateDisplay(record.startDate) } },
            { header: 'exhibition.start.date', property: function(record) { return getDateDisplay(record.endDate) } }
        ];

        initScopeForCategoryList($scope, $log, $filter, $location, $state, $stateParams, logger, $modal, Factory, STATE_CONFIG, columns, null);

        $scope.init();
    }

    init();

});
