app.controller('ImportCustomerCtrl', function($scope, $filter, $token, $state, logger, Factory, ADDRESS_BACKEND) {

    function init() {
        $scope.title = $filter('translate')('import.customer.title');

        $scope.who = 'admin';

        loadDistributors();
        loadTemplateId();

        initStep();

        $scope.global = {};
        $scope.global.distributorId = null;
        $scope.global.distributorName = null;
        $scope.global.excel = null;
        $scope.global.confirm = null;
    }

    $scope.isDoneStep = function(stepIndex) {
        return stepIndex < $scope.currentStep;
    };

    $scope.isCurrentStep = function(stepIndex) {
        return $scope.currentStep == stepIndex;
    };

    function initStep() {
        $scope.currentStep = 0;

        $scope.steps = [
            { name: 'import.customer.select.distributor' },
            { name: 'import.customer.upload.file' },
            { name: 'import.customer.confirm' },
            { name: 'import.customer.finalisation' }
        ];
    }

    $scope.isNextDisabled = function() {
        if ($scope.currentStep == 0) {
            return $scope.global.distributorId == null;
        } else if ($scope.currentStep == 1) {
            return $scope.global.excel == null;
        } else if ($scope.currentStep == 2) {
            return false;
        }

        return true;
    };

    $scope.canNext = function() {
        return $scope.currentStep < 3;
    };

    $scope.next = function() {
        if ($scope.currentStep == 2) {
            importCustomer();
        } else if ($scope.currentStep == 1) {
            verify();
        } else if ($scope.currentStep == 0) {
            for (var i = 0; i < $scope.distributors.length; i++) {
                var distributor = $scope.distributors[i];
                if (distributor.id == $scope.global.distributorId) {
                    $scope.global.distributorName = distributor.name;
                }
            }
            $scope.currentStep++;
        } else {
            $scope.currentStep++;
        }
    };

    $scope.canBack = function() {
        return $scope.currentStep < 3;
    };

    $scope.back = function() {
        if ($scope.currentStep == 0) {
            $state.go('customer-list');
        } else {
            $scope.currentStep--;
        }
    };

    $scope.isFinish = function() {
        return $scope.currentStep == 3;
    };

    $scope.getTemplateLink = function() {
        if ($scope.templateId != null) {
            return ADDRESS_BACKEND + 'file?id=' + $scope.templateId + '&access_token=' + $token.getAccessToken();
        }

        return '';
    };

    $scope.getRowErrorData = function(data) {
        if (data == null || data.length == 0) {
            return '<' + $filter('translate')('import.customer.data.empty') + '>';
        } else {
            return data;
        }
    };

    function importCustomer() {
        $scope.global.result = [];

        Factory.doPost(
            {
                'who': $scope.who,
                'category': 'import',
                'subCategory': 'customer',
                'param': 'import',
                'distributorId': $scope.global.distributorId,
                'fileId': $scope.global.excel.fileId
            },
            {},
            function(data){
                $scope.global.result = data;
                $scope.currentStep++;
            },
            function(){
            }
        );
    }

    function verify() {
        $scope.global.confirm = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'import',
                'subCategory': 'customer',
                'param': 'verify',
                'distributorId': $scope.global.distributorId,
                'fileId': $scope.global.excel.fileId
            },
            function(data){
                $scope.global.confirm = data;
                $scope.currentStep++;
            },
            function(){
            }
        );
    }

    function loadTemplateId() {
        $scope.templateId = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'import',
                'subCategory': 'customer',
                'param': 'template'
            },
            function(data){
                $scope.templateId = data.id;
            },
            function(){
            }
        );
    }

    function loadDistributors() {
        $scope.distributors = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'distributor',
                'subCategory': 'all'
            },
            function(data){
                angular.copy(data.list, $scope.distributors);
            },
            function(){
            }
        );
    }

    init();

});


