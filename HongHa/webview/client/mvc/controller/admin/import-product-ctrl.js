app.controller('ImportProductCtrl', function($scope, $filter, $token, $state, logger, Factory, ADDRESS_BACKEND) {

    function init() {
        $scope.title = $filter('translate')('import.product.title');

        $scope.who = 'admin';

        loadTemplateId();

        initStep();

        $scope.global = {};
        $scope.global.distributorId = null;
        $scope.global.distributorName = null;
        $scope.global.excel = null;
        $scope.global.confirm = null;
    }

    $scope.isDoneStep = function(stepIndex) {
        return stepIndex < $scope.currentStep;
    };

    $scope.isCurrentStep = function(stepIndex) {
        return $scope.currentStep == stepIndex;
    };

    function initStep() {
        $scope.currentStep = 0;

        $scope.steps = [
            { name: 'import.product.upload.file' },
            { name: 'import.product.confirm' },
            { name: 'import.product.add.photo' },
            { name: 'import.product.finalisation' }
        ];
    }

    $scope.isNextDisabled = function() {
        if ($scope.currentStep == 0) {
            return $scope.global.excel == null;
        } else if ($scope.currentStep == 1) {
            return ($scope.global.confirm.rowDatas != null &&
                $scope.global.confirm.rowDatas.length >= $scope.global.confirm.total) ||
                $scope.global.confirm.total <= 0;
        } else if ($scope.currentStep == 2) {
            return !$scope.form.$valid;
        }

        return true;
    };

    $scope.canNext = function() {
        return $scope.currentStep < 3;
    };

    $scope.next = function() {
        if ($scope.currentStep == 0) {
            verify();
        } else if ($scope.currentStep == 1) {
            getValidRows();
        } else if ($scope.currentStep == 2) {
            importProduct();
        }
    };

    $scope.canBack = function() {
        return $scope.currentStep < 3;
    };

    $scope.back = function() {
        if ($scope.currentStep == 0) {
            $state.go('product-list');
        } else {
            $scope.currentStep--;
        }
    };

    $scope.isFinish = function() {
        return $scope.currentStep == 3;
    };

    $scope.getTemplateLink = function() {
        if ($scope.templateId != null) {
            return ADDRESS_BACKEND + 'file?id=' + $scope.templateId + '&access_token=' + $token.getAccessToken();
        }

        return '';
    };

    $scope.getRowErrorData = function(data) {
        if (data == null || data.length == 0) {
            return '<' + $filter('translate')('import.product.data.empty') + '>';
        } else {
            return data;
        }
    };

    function importProduct() {
        $scope.global.result = [];

        Factory.doPost (
            {
                'who': $scope.who,
                'category': 'import',
                'subCategory': 'product',
                'param': 'import',
                'fileId': $scope.global.excel.fileId
            },
            { photos : $scope.global.photos },
            function(data) {
                $scope.global.result = data;
                $scope.currentStep++;
            },
            function() {
            }
        );
    }

    function getValidRows() {
        $scope.global.data = [];
        $scope.global.photos = [];

        Factory.doGet (
            {
                'who': $scope.who,
                'category': 'import',
                'subCategory': 'product',
                'param': 'confirm',
                'fileId': $scope.global.excel.fileId
            },
            function(data){
                $scope.global.data = data;
                $scope.global.photos = new Array(data.rowDatas.length);
                $scope.currentStep++;
            },
            function(){
            }
        );
    }

    function verify() {
        $scope.global.confirm = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'import',
                'subCategory': 'product',
                'param': 'verify',
                'fileId': $scope.global.excel.fileId
            },
            function(data){
                $scope.global.confirm = data;
                $scope.currentStep++;
            },
            function(){
            }
        );
    }

    function loadTemplateId() {
        $scope.templateId = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'import',
                'subCategory': 'product',
                'param': 'template'
            },
            function(data){
                $scope.templateId = data.id;
            },
            function(){
            }
        );
    }

    init();

});


