app.controller('ImportScheduleCtrl', function($scope, $filter, $token, $state, $stateParams, logger, Factory, ADDRESS_BACKEND) {

    function init() {
        $scope.title = $filter('translate')('import.schedule.title');

        var roleCodes = $token.getUserInfo().roleCodes;
        if (roleCodes != null && roleCodes.length > 0) {
            if (roleCodes[0] == 'AD') {
                $scope.who = 'admin';
            } else if (roleCodes[0] == 'SUP') {
                $scope.who = 'supervisor';
            }
        }

        loadDistributors();

        initStep();

        $scope.global = {};
        $scope.global.distributorId = null;
        $scope.global.distributorName = null;
        $scope.global.excel = null;
        $scope.global.confirm = null;
    }

    $scope.isDoneStep = function(stepIndex) {
        return stepIndex < $scope.currentStep;
    };

    $scope.isCurrentStep = function(stepIndex) {
        return $scope.currentStep == stepIndex;
    };

    function initStep() {
        $scope.currentStep = 0;

        $scope.steps = [
            { name: 'import.schedule.select.distributor' },
            { name: 'import.schedule.upload.file' },
            { name: 'import.schedule.confirm' },
            { name: 'import.schedule.finalisation' }
        ];
    }

    $scope.isNextDisabled = function() {
        if ($scope.currentStep == 0) {
            return $scope.global.distributorId == null;
        } else if ($scope.currentStep == 1) {
            return $scope.global.excel == null;
        } else if ($scope.currentStep == 2) {
            return false;
        }

        return true;
    };

    $scope.canNext = function() {
        return $scope.currentStep < 3;
    };

    $scope.next = function() {
        if ($scope.currentStep == 2) {
            doImport();
        } else if ($scope.currentStep == 1) {
            verify();
        } else if ($scope.currentStep == 0) {
            for (var i = 0; i < $scope.distributors.length; i++) {
                var distributor = $scope.distributors[i];
                if (distributor.id == $scope.global.distributorId) {
                    $scope.global.distributorName = distributor.name;
                }
            }
            $scope.currentStep++;
        } else {
            $scope.currentStep++;
        }
    };

    $scope.canBack = function() {
        return $scope.currentStep < 3;
    };

    $scope.back = function() {
        if ($scope.currentStep == 0) {
            $scope.returnToParent();
        } else {
            $scope.currentStep--;
        }
    };

    $scope.returnToParent = function() {
        $state.go('customer-schedule', { filter: $stateParams.parentFilter });
    };

    $scope.isFinish = function() {
        return $scope.currentStep == 3;
    };

    $scope.getTemplateLink = function() {
        return ADDRESS_BACKEND + $scope.who + '/schedule/import/template?distributorId=' + $scope.global.distributorId
            + '&access_token=' + $token.getAccessToken();
    };

    $scope.getRowErrorData = function(data) {
        if (data == null || data.length == 0) {
            return '<' + $filter('translate')('import.schedule.data.empty') + '>';
        } else {
            return data;
        }
    };

    function doImport() {
        $scope.global.result = [];

        Factory.doPost(
            {
                'who': $scope.who,
                'category': 'schedule',
                'subCategory': 'import',
                'distributorId': $scope.global.distributorId,
                'fileId': $scope.global.excel.fileId
            },
            {},
            function(data){
                $scope.global.result = data;
                $scope.currentStep++;
            },
            function(){
            }
        );
    }

    function verify() {
        $scope.global.confirm = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'schedule',
                'subCategory': 'import',
                'param': 'verify',
                'distributorId': $scope.global.distributorId,
                'fileId': $scope.global.excel.fileId
            },
            function(data){
                $scope.global.confirm = data;
                $scope.currentStep++;
            },
            function(){
            }
        );
    }

    function loadDistributors() {
        $scope.distributors = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'distributor',
                'subCategory': 'all'
            },
            function(data){
                angular.copy(data.list, $scope.distributors);
            },
            function(){
            }
        );
    }

    init();

});


