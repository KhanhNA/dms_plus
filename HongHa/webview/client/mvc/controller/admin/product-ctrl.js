app.controller('ProductCtrl', function($scope, $log, $filter, $location, $state, $stateParams, logger, $modal, Factory, STATE_CONFIG) {

    function init() {
        var columns = [
            { header: 'name', property: 'name' },
            { header: 'code', property: 'code' },
            { header: 'product.price', property: function(record) { return $filter('number')(record.price, 0) } }
        ];

        initScopeForCategoryList($scope, $log, $filter, $location, $state, $stateParams, logger, $modal, Factory, STATE_CONFIG, columns, null);

        $scope.init();

        $scope.importLink = "#/import-product"
    }

    init();

});
