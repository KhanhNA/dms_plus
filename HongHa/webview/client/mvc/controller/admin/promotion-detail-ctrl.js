app.controller('PromotionDetailCtrl', function ($scope, $log, $filter, $location, $state, logger, $modal, $stateParams, Factory, PATTERN) {

    function init() {
        $scope.title = $filter('translate')('menu.promotion');
        $scope.categoryName = 'promotion';
        $scope.who = 'admin';

        $scope.recordId = $stateParams.id;
        if ($scope.recordId != 'new') {
            $scope.action = 'edit';
        } else {
            $scope.action = 'new';
        }

        $scope.pattern = PATTERN;

        $scope.refresh();
    }

    $scope.refresh = function() {
        initPromotionType();
        $scope.isChanged = false;

        if ($scope.action == 'edit') {
            loadData($scope.recordId);
        } else {
            $scope.record = {};
            $scope.record.startDate = { date: new Date(), opened: false };
            $scope.record.endDate = { date: new Date(), opened: false };
        }
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id:id
            },
            function(data) {
                $scope.record = data;

                transformAfterLoad();

                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    $scope.ok = function (isEnable) {
        //Validate
        if (!$scope.form.$valid || !checkValidBeforeSave()) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;

            transformBeforeSave();

            // Edit record
            if ($scope.action === 'edit') {
                Factory.doPut(
                    {
                        who: $scope.who,
                        category: $scope.categoryName,
                        id: $scope.record.id
                    },
                    $scope.record,
                    function () {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable($scope.record.id);
                        } else {
                            logger.logSuccess($filter('translate')('edit.success'));
                            $scope.refresh();
                        }
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                        transformAfterLoad();
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        who: $scope.who,
                        category: $scope.categoryName
                    },
                    $scope.record,
                    function(data) {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable(data.id);
                        } else {
                            logger.logSuccess($filter('translate')('create.success'));
                            $scope.cancel();
                        }
                    },
                    function(){
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                        transformAfterLoad();
                    }
                );
            }
        }
    };

    $scope.cancel = function () {
        $state.go($scope.categoryName + '-list');
    };

    $scope.isDraft = function() {
        return $scope.record == null || $scope.record.draft == null ||  $scope.record.draft;
    };

    $scope.isEdit = function() {
        return $scope.action == 'edit';
    };

    $scope.isNew = function() {
        return $scope.action == 'new';
    };

    function enable(domainId) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: domainId,
                action: 'enable'
            },
            null,
            function() {
                $scope.proccessing = false;
                if ($scope.action === 'edit') {
                    $scope.refresh();
                } else {
                    $scope.cancel();
                }
                logger.logSuccess($filter('translate')('enable.success'));

            },
            function() {
                $scope.proccessing = false;
                logger.logError($filter('translate')('enable.error'));
            }
        );
    }

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
        $scope.markAsChanged();
    };

    $scope.isPromotionTypeApplied = function(promotionType) {
        return promotionType != null && promotionType.details != null && promotionType.details.length > 0;
    };

    $scope.sortPromotionType = function(promotionType) {
        if ($scope.isPromotionTypeApplied(promotionType)) {
            return 0;
        }

        return 1;
    };

    $scope.editPromotionType = function(promotionType) {
        var modalInstance = $modal.open({
            templateUrl: 'mvc/view/admin/promotion-type-popup.html',
            controller: 'PromotionTypePopupCtrl',
            size: 'lg',
            resolve: {
                promotionType: function () {
                    return promotionType;
                }
            }
        });
        modalInstance.result.then(function () {}, function () {});
        $scope.markAsChanged();
    };

    $scope.clearPromotionType = function(promotionType) {
        promotionType.details = null;
    };

    $scope.getPromotionDetailDisplay = function(type, detail) {
        if (type == 0) {
            var display = '<strong>' +  angular.escapeHTML(detail.condition.product.name) + '</strong>'
                + ' (' + detail.condition.product.code + ')';

            display = display + ' - (' + $filter('translate')('promotion.discount') +
            ' <strong>' + detail.reward.percentage + '%</strong>)';

            if (detail.condition.quantity > 1) {
                display = display + ' - (' + $filter('translate')('promotion.buy.from.quantity') +
                ' ' + detail.condition.quantity + ' ' + detail.condition.product.uom.name + ')';
            }

            return display;
        } else if (type == 1) {
            var display = $filter('translate')('promotion.buy')
            display = display + ' ' + detail.condition.quantity + ' x';
            display = display + ' <strong>' +  angular.escapeHTML(detail.condition.product.name) + '</strong>'
            + ' (' + detail.condition.product.code + ')';

            display = display + ' - ' + $filter('translate')('promotion.reward');
            display = display + ' ' + detail.reward.quantity + ' x';
            if (detail.reward.product != null) {
                display = display + ' <strong>' +  angular.escapeHTML(detail.reward.product.name) + '</strong>'
                + ' (' + detail.reward.product.code + ')';
            } else {
                display = display + ' <strong>' +  angular.escapeHTML(detail.reward.productText) + '</strong>';
            }


            return display;
        }
    };

    function transformAfterLoad() {
        if ($scope.record.startDate == null) {
            $scope.record.startDate = { date: new Date(), opened: false };
        } else {
            var startDateTxt = $scope.record.startDate;
            $scope.record.startDate = { date: new Date(startDateTxt), opened: false };
        }

        if ($scope.record.endDate == null) {
            $scope.record.endDate = { date: new Date(), opened: false };
        } else {
            var endDateTxt = $scope.record.endDate;
            $scope.record.endDate = { date: new Date(endDateTxt), opened: false };
        }

        if (!angular.isUndefinedOrNull($scope.record.details)) {
            for (var i = 0; i < $scope.record.details.length; i++) {
                var promotionDetail = $scope.record.details[i];
                if (!angular.isUndefinedOrNull(promotionDetail)) {
                    var promotionType = $scope.promtionTypes[promotionDetail.type];
                    if (!angular.isUndefinedOrNull(promotionType)) {
                        if (promotionType.details == null) {
                            promotionType.details = [];
                        }

                        promotionType.details.push(promotionDetail);
                    }
                }
            }
        }
    }

    function checkValidBeforeSave() {
        return true;
    }

    function transformBeforeSave() {
        var date = $scope.record.startDate.date;
        $scope.record.startDate = getFormattedDate(date);

        date = $scope.record.endDate.date;
        $scope.record.endDate = getFormattedDate(date);

        $scope.record.details = [];
        for (var i = 0; i < $scope.promtionTypes.length; i++) {
            var promotionType = $scope.promtionTypes[i];
            if (promotionType != null && promotionType.details != null) {
                for (var j = 0; j < promotionType.details.length; j++) {
                    $scope.record.details.push(
                        {
                            type: promotionType.type,
                            condition: {
                                productId: promotionType.details[j].condition.product.id,
                                quantity: promotionType.details[j].condition.quantity
                            },
                            reward: {
                                productId: (promotionType.details[j].reward.product == null ? null : promotionType.details[j].reward.product.id),
                                productText: promotionType.details[j].reward.productText,
                                percentage: promotionType.details[j].reward.percentage,
                                quantity: promotionType.details[j].reward.quantity
                            }
                        }
                    );
                }
            }
        }
    }

    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return year + '-' + month + '-' + day;
    }

    function initPromotionType() {
        $scope.promtionTypes = [
            {
                type: 0,
                name: $filter('translate')('promotion.type.c.product.qty.r.percentage.amt')
            },
            {
                type: 1,
                name: $filter('translate')('promotion.type.c.product.qty.r.product.qty')
            }
        ];
    }

    init();

});



