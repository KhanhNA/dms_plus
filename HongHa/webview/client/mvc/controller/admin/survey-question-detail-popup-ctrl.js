app.controller('SurveyQuestionDetailPopupCtrl', function ($scope, $modalInstance, $filter, logger, question) {

    function init() {
        $scope.title = $filter('translate')('survey.question');

        if (question == null) {
            $scope.question = { name: null, multipleChoice: false, options : [] };
        } else {
            $scope.originalQuestion = question;
            $scope.question = angular.copy(question);
        }
    }

    $scope.ok = function () {
        //Validate
        if (!$scope.form.$valid || !checkValidBeforeSave()) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            if ($scope.originalQuestion != null) {
                $scope.originalQuestion.name = $scope.question.name;
                $scope.originalQuestion.multipleChoice = $scope.question.multipleChoice;
                $scope.originalQuestion.options = $scope.question.options;

                $modalInstance.close();
            } else {
                $modalInstance.close($scope.question);
            }
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    $scope.createOption = function() {
        $scope.question.options.push( { name: null } );
    };

    $scope.deleteOption = function(index) {
        $scope.question.options.splice(index, 1);
    };

    function checkValidBeforeSave() {
        if (angular.isUndefinedOrNull($scope.question) || angular.isUndefinedOrNull($scope.question.options)
            || $scope.question.options.length == 0) {
            return false;
        }

        return true;
    }

    init();

});
