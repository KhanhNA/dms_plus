app.controller('UserDetailPopupCtrl', function ($scope, $modalInstance, $filter, logger, record, categoryName, config, Factory) {

    function init() {
        initScopeForCategoryDetailPopup($scope, $modalInstance, $filter, logger, record, categoryName, config, Factory,
            null, loadDependencies);

        $scope.isChanged = false;

        $scope.init();
    }

    $scope.resetPassword = function() {
        $scope.proccessing = true;

        Factory.doPut(
            {
                'who': $scope.who,
                'category': 'user',
                'id': $scope.record.id,
                'action': 'resetpassword'
            },
            {}
            ,
            function () {
                $scope.proccessing = false;
                logger.logSuccess($filter('translate')('reset.password.success'));
            },
            function () {
                $scope.proccessing = false;
                logger.logError($filter('translate')('reset.password.error'));
            }
        );
    };

    $scope.getRoleDisplay = function() {
        if ($scope.record.role != null && $scope.roles != null) {
            for (var i = 0; i < $scope.roles.length; i++) {
                if ($scope.roles[i].code === $scope.record.role) {
                    return $scope.roles[i].name;
                }
            }
        }
        return null;
    };

    $scope.haveChooseDistributor = function() {
        return $scope.record != null && $scope.record.role != null && $scope.record.role == 'DIS'
            && record.draft != null && !record.draft;
    };

    $scope.haveChooserSalesmen = function() {
        return $scope.record != null && $scope.record.role != null && $scope.record.role == 'SC'
            && record.draft != null && !record.draft;
    };

    function loadDependencies(record) {
        loadRoles();
        loadDistributors();

        if (record != null && record.draft != null && !record.draft) {
            loadSalesmen(record.id);
        }
    }

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    function loadRoles() {
        $scope.roles = [];
        $scope.proccessing = true;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'role'
            },
            function(data) {
                $scope.roles = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    function loadDistributors() {
        $scope.distributors = [];

        $scope.proccessing = true;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'distributor',
                'subCategory': 'all'
            },
            function(data){
                $scope.distributors = data.list;

                if ($scope.distributors == null) {
                    $scope.distributors = [];
                }
            },
            function(){
                $scope.proccessing = false;
            }
        );
    }

    function loadSalesmen(storeCheckerId) {
        $scope.proccessing = true;
        $scope.salesmen = [];
        Factory.doGet(
            {
                who: $scope.who,
                category: 'user',
                subCategory: 'salesman',
                param: 'bystorechecker',
                storeCheckerId: storeCheckerId
            },
            function(data) {
                $scope.salesmen = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    init();

});
