app.controller('CategoryDetailPopupCtrl', function ($scope, $modalInstance, $filter, logger, record, categoryName, config, Factory) {

    function init() {
        initScopeForCategoryDetailPopup($scope, $modalInstance, $filter, logger, record, categoryName, config, Factory,
            null, null);

        $scope.init();
    }

    init();

});
