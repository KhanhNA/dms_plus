app.controller('ChangePasswordCtrl', function($scope, $filter, logger, $state, Factory) {

    function init() {
        $scope.record = {};
    }

    $scope.save = function() {
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;

            // Edit record
            Factory.doPut(
                {
                    'who': 'common',
                    'category': 'system',
                    'action': 'changepassword'
                },
                $scope.record,
                function () {
                    $scope.proccessing = false;

                    logger.logSuccess($filter('translate')('save.success'));
                    $state.go('change-password', {}, { reload: true });
                },
                function (error) {
                    $scope.proccessing = false;
                    if (error.data.meta.error_message == 'invalid.old.password') {
                        logger.logError($filter('translate')('invalid.old.password'));
                    } else {
                        logger.logError($filter('translate')('save.error'));
                    }
                }
            );
        }
    };

    init();

});
