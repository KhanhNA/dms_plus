app.controller('TimeoutWarnCtrl', function($scope, NG_IDLE_TIMEOUT) {

    $scope.max = NG_IDLE_TIMEOUT;
    $scope.countdown = NG_IDLE_TIMEOUT;

    $scope.$on('IdleWarn', function(e, countdown) {
        $scope.countdown = countdown;
    });

    $scope.$on('IdleTimeout', function() {
        $scope.countdown = 0;
    });

});
