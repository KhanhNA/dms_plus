app.controller('DISOrderApproveConfirmDialogCtrl', function ($scope, $filter, $log, logger, $stateParams, $state, $token,
                                                    dialogs, Factory, $modalInstance) {

    function init() {
        $scope.recordId = $stateParams.id;
    }

    $scope.approveAndPrint = function() {
        $modalInstance.close('print');
    };

    $scope.approve = function() {
        $modalInstance.close('approve');
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    init();

});
