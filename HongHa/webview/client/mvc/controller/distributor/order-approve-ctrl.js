app.controller('DISOrderApproveCtrl', function($scope, $token, $log, $filter, $location, $state, logger, $modal, STATE_CONFIG,
                                               Factory) {
    function init() {
        loadConfig();

        $scope.title = $filter('translate')('approve.purchase.order');

        $scope.global = {};

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.tableCtrl = initTableCtrl(reloadData, null, editRecord, null);

        $scope.tableCtrl.refresh();
    }

    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }
        $scope.config = STATE_CONFIG[path];
        $scope.categoryName = path;
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        var queryString = {
            'who': 'distributor',
            'category': 'order',
            'subCategory': 'pending',
            'page': $scope.currentPage,
            'size': $scope.itemsPerPage
        };

        Factory.doGet(
            queryString,
            function(data) {
                $scope.tableCtrl.records = data.list;
                $scope.totalItems = data.count;

                if ($scope.tableCtrl.records == null) {
                    $scope.tableCtrl.records = [];
                    $scope.totalItems = 0;
                }

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function editRecord(record) {
        $state.go( $scope.categoryName + '-detail', { id:record.id } );
    }

    $scope.search = function() {
        reloadData();
    };

    init();

});
