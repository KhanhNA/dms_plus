app.controller('DISOrderApproveDetailCtrl', function ($scope, $filter, $log, logger, $stateParams, $state, $token,
                                                    dialogs, Factory, $modal) {

    function init() {
        $scope.title = $filter('translate')('approve.purchase.order.detail.title');

        $scope.clientName = $token.getUserInfo().clientName;

        $scope.createdDate = {
            'date': null,
            'opened': false
        };

        $scope.deliveryDate = {
            'date': null,
            'opened': false
        };

        $scope.recordId = $stateParams.id;
        if ($scope.recordId != 'new') {
            $scope.action = 'edit';
        } else {
            $scope.action = 'new';
        }

        $scope.refresh();
    }

    $scope.refresh = function() {
        if ($scope.action == 'edit') {
            if ($scope.isChanged) {
                // confirm changed
                var dlg = dialogs.confirm(
                    $filter('translate')('dialog.warning.title'),
                    $filter('translate')('warning.refresh.when.data.changed'),
                    {
                        size: 'sm',
                        backdrop: true
                    }
                );

                dlg.result.then(
                    function(btn){
                        $scope.isChanged = false;
                        loadData($scope.recordId);
                    },
                    function(btn){
                        // Do nothing
                    }
                );
            } else {
                $scope.isChanged = false;
                loadData($scope.recordId);
            }
        } else {
            $scope.isChanged = false;

            $scope.record = {};

            afterLoadData();
        }
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': 'distributor',
                'category': 'order',
                'subCategory': 'pending',
                'param': id
            },
            function(data) {
                $scope.record = data;

                afterLoadData();
                transformAfterLoad();

                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function afterLoadData() {
        $scope.record.statusName = $filter('translate')('purchase.order.status.' + $scope.record.approveStatus);
        $scope.createdDate.date = $scope.record.createdDate == null ? null : angular.parseDateTime($scope.record.createdDate);
        $scope.deliveryDate.date = $scope.record.deliveryDate == null ? null : angular.parseDateTime($scope.record.deliveryDate);
    }

    $scope.getDeliveryTypeName = function (deliveryType) {
        if (deliveryType == 1) {
            return $filter('translate')('purchase.order.delivery.type.in.day');
        } else if (deliveryType == 2) {
            return $filter('translate')('purchase.order.delivery.type.another.day');
        } else {
            return $filter('translate')('purchase.order.delivery.type.immediate');
        }
    };

    function transformAfterLoad() {
        if (!$scope.hasPromotion()) {
            $scope.rewardProducts = undefined;
            return;
        }
        var rewards = [];
        for (var i = 0; i < $scope.record.promotionResults.length; i++) {
            var promotion = $scope.record.promotionResults[i];
            if (promotion.details == undefined || promotion.details.length == 0) {
                continue;
            }
            for (var j = 0; j < promotion.details.length; j++) {
                var detail = promotion.details[j];
                var rewardResult = detail.rewardResult;
                if (rewardResult == undefined
                    || angular.isUndefinedOrNull(rewardResult.quantity)
                    || angular.isUndefinedOrNull(rewardResult.product)
                    ) {
                    continue;
                }
                rewards.push(rewardResult);
            }
        }
        $scope.rewardProducts = rewards;
    }

    $scope.hasPromotion = function() {
        if (angular.isUndefinedOrNull($scope.record)
            || angular.isUndefinedOrNull($scope.record.promotionResults)
            || $scope.record.promotionResults.length == 0) {
            return false;
        }

        return true;
    };

    $scope.checkChanged = function() {
        return $scope.isChanged;
    };

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    $scope.isReadonly = function() {
        return true;
    };

    $scope.canEdit = function () {
        return false;
    };

    $scope.open = function($event, data) {
        if (!$scope.canEdit()) {
            return;
        }

        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
        $scope.markAsChanged();
    };

    $scope.canApproval = function() {
        if (angular.isUndefinedOrNull($scope.record)
                || angular.isUndefinedOrNull($scope.record.details)
                || $scope.record.details.length == 0) {
            return false;
        }

        if ($scope.isChanged) {
            return false;
        }

        return $scope.record.approveStatus == 0;
    };

    $scope.canReject = function() {
        if (angular.isUndefinedOrNull($scope.record)
                || angular.isUndefinedOrNull($scope.record.details)
                || $scope.record.details.length <= 0) {
            return false;
        }

        if ($scope.isChanged) {
            return false;
        }

        return $scope.record.approveStatus == 0;
    };

    $scope.canRefresh = function() {
        if (angular.isUndefinedOrNull($scope.record)) {
            return true;
        }

        return $scope.record.approveStatus == 0;
    };

    $scope.approval = function() {
        if (angular.isUndefinedOrNull($scope.record)
                || $scope.record.approveStatus != 0
                || angular.isUndefinedOrNull($scope.record.details)
                || $scope.record.details.length <= 0) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else if ($scope.isChanged) {
            logger.logError($filter('translate')('error.data.changed'));
        } else {
            var modalInstance = $modal.open({
                templateUrl: 'mvc/view/distributor/order-approve-confirm-dialog.html',
                controller: 'DISOrderApproveConfirmDialogCtrl',
                size: 'md'
            });
            modalInstance.result.then(
                function(btn){
                    $scope.proccessing = true;

                    Factory.doPut(
                        {
                            'who': 'distributor',
                            'category': 'order',
                            'subCategory': 'pending',
                            'param': $scope.record.id,
                            'action': 'approve'
                        },
                        null,
                        function () {
                            $scope.proccessing = false;
                            logger.logSuccess($filter('translate')('approve.success'));

                            $scope.isChanged = false;
                            $scope.cancel();

                            if (btn === 'print') {
                                try {
                                    $scope.printInvoice();
                                } catch(e) {};
                            }
                        },
                        function () {
                            $scope.proccessing = false;
                            logger.logError($filter('translate')('approve.error'));
                        }
                    );
                },
                function(btn){
                    // Do nothing
                }
            );
        }
    };

    $scope.reject = function() {
        if (angular.isUndefinedOrNull($scope.record)
                || $scope.record.approveStatus != 0
                || angular.isUndefinedOrNull($scope.record.details)
                || $scope.record.details.length <= 0) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else if ($scope.isChanged) {
            logger.logError($filter('translate')('error.data.changed'));
        } else {
            var dlg = dialogs.confirm(
                $filter('translate')('purchase.order.dialog.confirm.title'),
                $filter('translate')('purchase.order.confirm.when.reject'),
                {
                    size: 'sm',
                    backdrop: true
                }
            );

            dlg.result.then(
                function(btn){
                    $scope.proccessing = true;

                    Factory.doPut(
                        {
                            'who': 'distributor',
                            'category': 'order',
                            'subCategory': 'pending',
                            'param': $scope.record.id,
                            'action': 'reject'
                        },
                        null,
                        function () {
                            $scope.proccessing = false;
                            logger.logSuccess($filter('translate')('reject.success'));

                            //$scope.refresh();
                            $scope.isChanged = false;
                            $scope.cancel();
                        },
                        function () {
                            $scope.proccessing = false;
                            logger.logError($filter('translate')('reject.error'));
                        }
                    );
                },
                function(btn){
                    // Do nothing
                }
            );
        }
    };

    $scope.cancel = function () {
        if ($scope.isChanged) {
            var dlg = dialogs.confirm(
                $filter('translate')('dialog.warning.title'),
                $filter('translate')('warning.close.when.data.changed'),
                {
                    size: 'sm',
                    backdrop: true
                }
            );

            dlg.result.then(
                function (btn) {
                    $state.go('orders-approve-list');
                },
                function (btn) {
                    // Do nothing
                }
            );
        } else {
            $state.go('orders-approve-list');
        }
    };

    $scope.printInvoice = function () {
        var printContents = document.getElementById("invoice").innerHTML;
        var popupWin = window.open();
        popupWin.document.open();
        popupWin.document.write(
            '<html>' +
            '<head>' +
            '<link rel="stylesheet" type="text/css" href="styles/main.css"/>' +
            '</head>' +
            '<body onload="window.print()" class="print page-invoice">' +
            printContents +
            '</body>' +
            '</html>'),
        popupWin.document.close();
    };

    init();

});
