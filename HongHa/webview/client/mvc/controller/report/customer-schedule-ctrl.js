app.controller('CustomerScheduleCtrl', function($scope, $log, $filter, $token, $stateParams, logger, dialogs, Factory, BusinessService) {

    function init() {
        $scope.title = $filter('translate')('customer.schedule.title');

        $scope.currentUserId = $token.getUserInfo().id;

        $scope.who = BusinessService.getWho();

        $scope.editingMode = false;
        BusinessService.initScopeForChangeStatus($scope);
        $scope.loadStatus = {clientConfig: {}, distributors: {}, routes: {}, data: {}};
        BusinessService.initScopeForLoadStatus($scope, $scope.loadStatus);

        var paramsFilter = $stateParams.filter != null ? JSON.parse($stateParams.filter) : null;

        $scope.filter = {};
        BusinessService.initTableFilter($scope.filter);

        BusinessService.loadClientConfig($scope.who, $scope.loadStatus.clientConfig,
            function(clientConfig) {
                $scope.clientConfig = clientConfig;
                $scope.numberWeekFrequency = $scope.clientConfig.numberWeekOfFrequency;
                $scope.weeks = [];
                for (var i = 0; i < $scope.numberWeekFrequency; i++) {
                    $scope.weeks.push( { label: 'W' + (i + 1), index: i } );
                }
            }
        );

        BusinessService.loadDistributors($scope.who, $scope.loadStatus.distributors,
            function(distributors) {
                $scope.distributors = distributors;
                if ($scope.distributors != null && $scope.distributors.length > 0) {
                    if (paramsFilter.currentDistributorId != null &&
                        _.findIndex($scope.distributors, function(distributor) {
                            return distributor.id == paramsFilter.currentDistributorId;
                        }) > -1) {
                        $scope.filter.currentDistributorId = paramsFilter.currentDistributorId;
                    } else {
                        $scope.filter.currentDistributorId = $scope.distributors[0].id;
                    }

                    $scope.changeDistributor();
                }
            }
        );

        initDayOfWeek();
        initItemsPerPageOption();
    }

    $scope.refresh = function() {
        $scope.isChanged = false;
        $scope.editingMode = false;

        var queryString = {
            'who': $scope.who,
            'category': 'schedule',
            'page': $scope.filter.currentPage,
            'size': $scope.filter.itemsPerPage,
            'distributorId': $scope.filter.currentDistributorId,
            'day' : $scope.filter.currentDay
        };
        if (angular.isUndefinedOrNull($scope.filter.currentRouteId) || $scope.filter.currentRouteId.length == 0) {
            queryString.all = true;
        } else {
            if ($scope.filter.currentRouteId != 'not-schedule') {
                queryString.routeId = $scope.filter.currentRouteId;
            }
        }
        if ($scope.filter.searchText && $scope.filter.searchText.length > 0) {
            queryString.q = angular.encodeURI($scope.filter.searchText);
        }

        BusinessService.loadListData($scope.who, $scope.loadStatus.data,
            function(list, count) {
                $scope.records = list;
                $scope.totalItems = count;
                afterLoad();
            },
            queryString
        );
    };

    $scope.changeDistributor = function() {
        $scope.filter.currentRouteId = null;
        $scope.filter.currentPage = 1;
        $scope.filter.searchText = null;

        initRoute();

        $scope.refresh();
    };

    function initRoute() {
        $scope.routeFilters = [
            {
                id: null,
                name: '-- ' + $filter('translate')('customer.schedule.all') + ' --'
            },
            {
                id: 'not-schedule',
                name: '-- ' + $filter('translate')('customer.schedule.customer.not.scheduled') + ' --'
            }

        ];

        $scope.routes = [
            {
                id: null,
                name: '-- ' + $filter('translate')('customer.schedule.customer.not.scheduled') + ' --'
            }
        ];

        BusinessService.loadRoutesByDistributor($scope.who, $scope.filter.currentDistributorId, $scope.loadStatus.routes,
            function(routes) {
                if (routes != null && routes.length > 0) {
                    $scope.routeFilters = _.union($scope.routeFilters, routes);
                    $scope.routes = _.union($scope.routes, routes);
                }
            }
        );
    }

    $scope.getFilterAsString = function() {
        return JSON.stringify($scope.filter);
    };

    function afterLoad() {
        if (!angular.isUndefinedOrNull($scope.records)) {
            angular.forEach($scope.records, function(customerSchedule) {
                if (!angular.isUndefinedOrNull(customerSchedule.items)) {
                    angular.forEach(customerSchedule.items, function(item) {
                        item.isSunday = (item.days.indexOf(1) >= 0);
                        item.isMonday = (item.days.indexOf(2) >= 0);
                        item.isTuesday = (item.days.indexOf(3) >= 0);
                        item.isWednesday = (item.days.indexOf(4) >= 0);
                        item.isThursday = (item.days.indexOf(5) >= 0);
                        item.isFriday = (item.days.indexOf(6) >= 0);
                        item.isSaturday = (item.days.indexOf(7) >= 0);

                        if ($scope.numberWeekFrequency > 1) {
                            item.weekFrequencys = [];
                            for (var i = 0; i < $scope.numberWeekFrequency; i++) {
                                var weekFrequency = {};
                                weekFrequency.selected = item.weeks.indexOf(i + 1) >= 0;
                                weekFrequency.label = 'W' + (i + 1);
                                weekFrequency.weekNumber = i + 1;
                                item.weekFrequencys.push(weekFrequency);
                            }
                        }

                    });
                }
            });
        }
    }

    function beforeSave() {
        if (!angular.isUndefinedOrNull($scope.records)) {
            angular.forEach($scope.records, function(customerSchedule) {
                if (!angular.isUndefinedOrNull(customerSchedule.items)) {
                    angular.forEach(customerSchedule.items, function(item) {
                        item.days = [];
                        if (item.isSunday) { item.days.push(1); }
                        if (item.isMonday) { item.days.push(2); }
                        if (item.isTuesday) { item.days.push(3); }
                        if (item.isWednesday) { item.days.push(4); }
                        if (item.isThursday) { item.days.push(5); }
                        if (item.isFriday) { item.days.push(6); }
                        if (item.isSaturday) { item.days.push(7); }

                        if ($scope.numberWeekFrequency > 1) {
                            if (!angular.isUndefinedOrNull(item.weekFrequencys)) {
                                item.weeks = [];
                                angular.forEach(item.weekFrequencys, function(weekFrequency) {
                                    if (weekFrequency.selected) { item.weeks.push(weekFrequency.weekNumber); }
                                });
                            }
                        }
                    });
                }
            });
        }
    }

    function checkBeforeSave() {
        if (!angular.isUndefinedOrNull($scope.records)) {
            for (var i = 0; i < $scope.records.length; i++) {
                var customerSchedule = $scope.records[i];

                var hasItem = false;
                if (!angular.isUndefinedOrNull(customerSchedule.items)) {
                    for (var j = 0; j < customerSchedule.items.length; j++) {
                        var item = customerSchedule.items[j];

                        if (item.days != null && item.days.length > 0 && ((item.weeks != null && item.weeks.length > 0) || $scope.numberWeekFrequency <= 1)) {
                            hasItem = true;
                        }
                    }
                }

                var hasRoute = !angular.isUndefinedOrNull(customerSchedule.routeId);
                if ((hasItem && !hasRoute) || (!hasItem && hasRoute)) {
                    return $filter('translate')('customer.schedule.schedule.null');
                }
            }
        }

        return null;
    }

    $scope.addNewItem = function(customerSchedule) {
        var item = {
            isMonday: false,
            isTuesday: false,
            isWednesday: false,
            isThursday: false,
            isFriday: false,
            isSaturday: false,
            isSunday: false
        };

        item.weekFrequencys = [];
        for (var i = 0; i < $scope.numberWeekFrequency; i++) {
            var weekFrequency = {};
            weekFrequency.selected = true;
            weekFrequency.label = 'W' + (i + 1);
            weekFrequency.weekNumber = i + 1;
            item.weekFrequencys.push(weekFrequency);
        }

        customerSchedule.items.push(item);
        $scope.markAsChanged();
    };

    $scope.clearCustomerSchedule = function(customerSchedule) {
        customerSchedule.items = [];
        $scope.addNewItem(customerSchedule);
        $scope.markAsChanged();
    };

    $scope.clearCustomerScheduleItem = function(customerSchedule, item) {
        if (customerSchedule.items.length > 1) {
            customerSchedule.items.splice(customerSchedule.items.indexOf(item), 1);
        } else {
            $scope.clearCustomerSchedule(customerSchedule);
        }
        $scope.markAsChanged();
    };

    $scope.save = function() {
        beforeSave();

        if (checkBeforeSave() != null) {
            logger.logError(checkBeforeSave());
        } else {
            $scope.proccessing = true;
            $scope.error = false;

            Factory.doPut(
                {
                    'who': $scope.who,
                    'category': 'schedule',
                    'subCategory': 'bydistributor',
                    'distributorId' : $scope.filter.currentDistributorId
                },
                $scope.records,
                function(){
                    $scope.proccessing = false;

                    $scope.refresh();

                    logger.logSuccess($filter('translate')('save.success'));
                },
                function(){
                    $scope.proccessing = false;
                    logger.logError($filter('translate')('save.error'));
                }
            );
        }
    };

    $scope.modeChange = function() {
        if (!$scope.editingMode && $scope.isChanged) {
            var dlg = dialogs.confirm(
                $filter('translate')('dialog.title.notice'),
                $filter('translate')('customer.schedule.data.unsaved.changing.mode'),
                {
                    size: 'md',
                    backdrop: false
                }
            );

            dlg.result.then(
                function() {
                    $scope.refresh();
                },
                function() {
                    $scope.editingMode = true;
                }
            );
        }
    };

    function initDayOfWeek() {
        $scope.filter.currentDay = null;

        $scope.dayOfWeeks = [
            { value : null, name : 'all' },
            { value : 2, name : 'day.monday' },
            { value : 3, name : 'day.tuesday' },
            { value : 4, name : 'day.wednesday' },
            { value : 5, name : 'day.thursday' },
            { value : 6, name : 'day.friday' },
            { value : 7, name : 'day.saturday' },
            { value : 1, name : 'day.sunday' }
        ];
    }

    function initItemsPerPageOption() {
        $scope.itemsPerPageOptions = [
            10,
            20,
            30
        ];
    }

    init();

});


