app.controller('DashboardVisitCtrl', function($scope, $log, $filter, $token, $location, $state, $stateParams, logger, Factory) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.title = $filter('translate')('dashboard.visit.title');

        $scope.currentUserId = $token.getUserInfo().id;

        $scope.currentPage = 1;
        $scope.itemsPerPage = 50;
        $scope.maxSize = 5;

        $scope.global = {};
        $scope.global.currentSalesmanId = $stateParams.salesmanid;

        $scope.data = {};
        $scope.data.numberVisits = { actual: 0, plan: 0, percentage: 0 };
        $scope.data.numberVisitErrorDuration = 0;
        $scope.data.numberVisitErrorPosition = 0;

        $scope.locationStatus = {};
        $scope.locationStatus.LOCATION_STATUS_LOCATED = 0;
        $scope.locationStatus.LOCATION_STATUS_TOO_FAR = 1;
        $scope.locationStatus.LOCATION_STATUS_UNLOCATED = 2;
        $scope.locationStatus.LOCATION_STATUS_CUSTOMER_UNLOCATED = 3;

        $scope.approveStatus = {};
        $scope.approveStatus.APPROVE_STATUS_PENDING = 0;
        $scope.approveStatus.APPROVE_STATUS_APPROVED = 1;
        $scope.approveStatus.APPROVE_STATUS_REJECTED = 2;

        initSalesmen();
    }

    function reloadDatas() {
        $scope.data = {};
        $scope.data.numberVisits = { actual: 0, plan: 0, percentage: 0 };
        $scope.data.numberVisitErrorDuration = 0;
        $scope.data.numberVisitErrorPosition = 0;

        if (!angular.isUndefinedOrNull($scope.global.currentSalesmanId)) {
            $scope.proccessing = true;
            $scope.error = false;

            var queryParams = {
                'who': $scope.who,
                'category': 'report',
                'subCategory': 'visit',
                'action': 'daily'
            };
            if ($scope.global.currentSalesmanId == 'all') {
                queryParams.salesmanId = null;
            } else {
                queryParams.salesmanId = $scope.global.currentSalesmanId;
            }

            Factory.doGet(
                queryParams,
                function(data) {
                    $scope.data = data;

                    if (angular.isUndefinedOrNull($scope.data.visits)) {
                        $scope.data.visits = [];
                        $scope.totalItems = 0;
                    } else {
                        $scope.totalItems = $scope.data.visits.length;
                    }
                    $scope.pageChanged();
                    $scope.proccessing = false;
                },
                function(error) {
                    logger.logError($filter('translate')('loading.error'));
                    $log.log(error);
                    $scope.proccessing = false;
                    $scope.error = true;
                }
            );
        }
    }

    function initSalesmen() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.salesmen = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'salesman'
            },
            function(data){
                $scope.salesmen = data.list;
                $scope.salesmen.unshift({
                    id: 'all',
                    fullname: '-- ' + $filter('translate')('dashboard.visit.all') + ' --'
                });

                if (!angular.isUndefinedOrNull($scope.salesmen) && $scope.salesmen.length > 0) {
                    if (angular.isUndefinedOrNull($scope.global.currentSalesmanId)) {
                        $state.go($scope.who + '-dashboard-visit', {salesmanid: $scope.salesmen[0].id}, {location: 'replace', reload: true});
                    } else {
                        if ($scope.global.currentSalesmanId == 'all') {
                            $scope.refresh();
                        } else {
                            var found = false;
                            for (var i = 0; i < $scope.salesmen.length; i++) {
                                if ($scope.global.currentSalesmanId == $scope.salesmen[i].id) {
                                    found = true;
                                }
                            }

                            if (found) {
                                $scope.refresh();
                            } else {
                                $state.go($scope.who + '-dashboard-visit', {salesmanid: $scope.salesmen[0].id}, {location: 'replace', reload: true});
                            }
                        }

                    }
                }

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.changeSalesman = function(salesman) {
        $state.go($scope.who + '-dashboard-visit', {salesmanid: salesman.id}, {reload: true});
    };

    $scope.viewDetail = function(id) {
        $state.go($scope.who + '-dashboard-visit-detail', { id: id, returnSalesmanId: $scope.global.currentSalesmanId });
    };

    $scope.refresh = function() {
        reloadDatas();
    };

    $scope.getDurationFormat = function(duration) {
        var minute = parseInt(duration / 60000);
        var second = parseInt((duration / 1000) % 60);
        return minute + 'm ' + second + 's';
    };

    $scope.getStartEndTime = function(visit) {
        var startDate = angular.parseDateTime(visit.startTime);
        var endDate = angular.parseDateTime(visit.endTime);
        var start = startDate != null ? moment(startDate).format('HH:mm:ss') : '';
        var end = endDate != null ? moment(endDate).format('HH:mm:ss') : '';

        return start + ' - ' + end;
    };

    $scope.pageChanged = function() {
        var displayData = [];
        var offsetStart = ($scope.currentPage - 1) * $scope.itemsPerPage;
        var offsetEnd = offsetStart + $scope.itemsPerPage;
        offsetEnd = offsetEnd > $scope.totalItems ? $scope.totalItems : offsetEnd;
        for (var offset = offsetStart; offset < offsetEnd; offset ++) {
            displayData.push($scope.data.visits[offset]);
        }
        $scope.displayData = displayData;
    };

    $scope.getDistanceDisplay = function(distance) {
        if (angular.isUndefinedOrNull(distance)) {
            return $filter('translate')('dashboard.visit.location.undefined');
        }

        return $filter('number')(distance * 1000, 0) + ' m';
    };

    init();

});
