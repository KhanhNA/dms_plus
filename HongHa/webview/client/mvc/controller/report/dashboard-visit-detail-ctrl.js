app.controller('DashboardVisitDetailCtrl', function ($scope, $log, $filter, $token, $location, $state, $stateParams,
                                                        logger, Factory, ImageService, uiGmapIsReady) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.title = $filter('translate')('dashboard.visit.detail.title');

        $scope.global = {};
        $scope.global.currentVisitId = $stateParams.id;
        $scope.global.returnSalesmanId = $stateParams.returnSalesmanId;
        $scope.global.rotate = 0;

        $scope.approveStatus = {};
        $scope.approveStatus.APPROVE_STATUS_PENDING = 0;
        $scope.approveStatus.APPROVE_STATUS_APPROVED = 1;
        $scope.approveStatus.APPROVE_STATUS_REJECTED = 2;

        $scope.data = {};
        $scope.points = [];
        initMap();

        $scope.clientName = $token.getUserInfo().clientName;

        $scope.refresh();
    }

    function reloadDatas() {
        $scope.data = {};
        $scope.loaded = false;

        if (!angular.isUndefinedOrNull($scope.global.currentVisitId)) {
            $scope.proccessing = true;
            $scope.error = false;

            Factory.doGet(
                {
                    'who': $scope.who,
                    'category': 'visit',
                    'id': $scope.global.currentVisitId
                },
                function (data) {
                    $scope.data = data;
                    $scope.record = data;
                    $scope.data.closingPhotoUrl = ImageService.getImageUrl(data.closingPhoto);
                    transformAfterLoad();
                    loadCustomerInfo();
                    $scope.proccessing = false;
                },
                function (error) {
                    logger.logError($filter('translate')('loading.error'));
                    $log.log(error);
                    $scope.proccessing = false;
                    $scope.error = true;
                }
            );
        }
    }

    function transformAfterLoad() {
        if (!$scope.hasPromotion()) {
            $scope.rewardProducts = undefined;
            return;
        }
        var rewards = [];
        for (var i = 0; i < $scope.record.promotionResults.length; i++) {
            var promotion = $scope.record.promotionResults[i];
            if (promotion.details == undefined || promotion.details.length == 0) {
                continue;
            }
            for (var j = 0; j < promotion.details.length; j++) {
                var detail = promotion.details[j];
                var rewardResult = detail.rewardResult;
                if (rewardResult == undefined
                    || angular.isUndefinedOrNull(rewardResult.quantity)
                    || angular.isUndefinedOrNull(rewardResult.product)
                ) {
                    continue;
                }
                rewards.push(rewardResult);
            }
        }
        $scope.rewardProducts = rewards;
    }

    $scope.hasPromotion = function() {
        if (angular.isUndefinedOrNull($scope.record)
            || angular.isUndefinedOrNull($scope.record.promotionResults)
            || $scope.record.promotionResults.length == 0) {
            return false;
        }

        return true;
    };

    $scope.getDeliveryTypeName = function (deliveryType) {
        if (deliveryType == 1) {
            return $filter('translate')('purchase.order.delivery.type.in.day');
        } else if (deliveryType == 2) {
            return $filter('translate')('purchase.order.delivery.type.another.day');
        } else {
            return $filter('translate')('purchase.order.delivery.type.immediate');
        }
    };

    function loadCustomerInfo() {
        $scope.proccessing = true;
        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'customer',
                'id': $scope.data.customer.id
            },
            function (data) {
                $scope.data.customer = data;
                $scope.proccessing = false;
                $scope.loaded = true;
            },
            function (error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.getVisitTime = function () {
        if (angular.isUndefinedOrNull($scope.data)) {
            return '';
        }

        var startTime = angular.parseDateTime($scope.data.startTime);
        var endTime = angular.parseDateTime($scope.data.endTime);

        if (angular.isUndefinedOrNull(startTime) || angular.isUndefinedOrNull(endTime)) {
            return '';
        }
        var minute = parseInt($scope.data.duration / 60000);
        var second = parseInt(($scope.data.duration / 1000) % 60);
        var duration = minute + 'm ' + second + 's';

        return moment(startTime).format('HH:mm:ss') + ' - ' + moment(endTime).format('HH:mm:ss') + ' (' + duration + ')';
    };

    $scope.showDate = function(isoDateString) {
        var date =  angular.parseDate(isoDateString);

        if (date == null) {
            date =  angular.parseDateTime(isoDateString);
        }

        if (date == null) {
            return '';
        }

        return moment(date).format('DD/MM/YYYY');

    };

    $scope.refresh = function () {
        reloadDatas();
    };

    $scope.back = function() {
        if (angular.isUndefinedOrNull($scope.global.returnSalesmanId)) {
            $state.go($scope.who + '-dashboard-visit', { salesmanid: 'all' });
        } else {
            $state.go($scope.who + '-dashboard-visit', { salesmanid: $scope.global.returnSalesmanId });
        }
    };

    $scope.hasPromotion = function() {
        if (angular.isUndefinedOrNull($scope.data)
            || angular.isUndefinedOrNull($scope.data.promotionResults)
            || $scope.data.promotionResults.length == 0) {
            return false;
        }

        return true;
    };

    $scope.rotateLeft = function() {
        $scope.global.rotate = $scope.global.rotate - 90;

        if ($scope.global.rotate < 0) {
            $scope.global.rotate = 270;
        }
    };

    $scope.rotateRight = function() {
        $scope.global.rotate = $scope.global.rotate + 90;

        if ($scope.global.rotate == 360) {
            $scope.global.rotate = 0;
        }
    };

    $scope.getRotateClass = function() {
        if ($scope.global.rotate == 90) {
            return 'rotate-90';
        }

        if ($scope.global.rotate == 180) {
            return 'rotate-180';
        }

        if ($scope.global.rotate == 270) {
            return 'rotate-270';
        }

        return '';
    };

    function createMarkers() {
        $scope.points = [];
        var markerCustomer = 'images/marker-customer.png';
        var markerSalesman = 'images/marker-salesman.png';

        // Have visit position
        if (!angular.isUndefinedOrNull($scope.record) && !angular.isUndefinedOrNull($scope.record.location)) {
            var point = {
                id: 1,
                latitude: $scope.record.location.latitude,
                longitude: $scope.record.location.longitude,
                icon: markerSalesman
            };

            $scope.points.push(point);
        }

        // Have customer position
        if (!angular.isUndefinedOrNull($scope.record) && !angular.isUndefinedOrNull($scope.record.customerLocation)) {
            var point = {
                id: 2,
                latitude: $scope.record.customerLocation.latitude,
                longitude: $scope.record.customerLocation.longitude,
                icon: markerCustomer
            };

            $scope.points.push(point);
        }

    }

    $scope.activeMap = function(select) {
        $scope.mapActive = select;

        uiGmapIsReady.promise().then(function (maps) {
            $scope.control.refresh();
            createMarkers();
        });
    };

    function initMap() {
        var defaultCenter = {latitude: 21.084269026984, longitude: 105.82028125};
        $scope.map = {
            center: defaultCenter,
            zoom: 13
        };

        $scope.control = {};


    }

    init();

    $scope.printInvoice = function () {
        var popupWin, printContents;
        return printContents = document.getElementById("invoice").innerHTML,
            popupWin = window.open(),
            popupWin.document.open(),
            popupWin.document.write(
                '<html>' +
                '<head>' +
                '<link rel="stylesheet" type="text/css" href="styles/main.css"/>' +
                '</head>' +
                '<body onload="window.print()" class="print page-invoice">' +
                printContents +
                '</body>' +
                '</html>'),
            popupWin.document.close()
    };

});
