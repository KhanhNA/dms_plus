app.controller('DisplayCheckReportCtrl', function($scope, $location, $log, $filter, $token, $location, $state, $stateParams,
                                                 logger, Factory, ADDRESS_BACKEND) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.currentUserId = $token.getUserInfo().id;
        // In days
        $scope.maxPeriodDuration = 7;

        $scope.global = {};

        $scope.global.fromDate = {};
        $scope.global.fromDate.date = new Date();
        $scope.global.fromDate.opened = null;

        $scope.global.toDate = {};
        $scope.global.toDate.date = new Date();
        $scope.global.toDate.opened = null;

        initSalesmen();
    }

    function initSalesmen() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.salesmen = [];
        $scope.global.currentSalesmanId = null;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'salesman'
            },
            function(data) {
                $scope.salesmen = data.list;
                $scope.salesmen.unshift({
                    id: 'all',
                    fullname: '-- ' + $filter('translate')('dashboard.visit.all') + ' --'
                });

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
    };

    $scope.refresh = function() {
        init();
    };

    function checkDates() {
        $scope.global.fromDate.date = angular.truncateDate($scope.global.fromDate.date);
        $scope.global.toDate.date = angular.truncateDate($scope.global.toDate.date);

        if ($scope.global.fromDate.date.getTime() > $scope.global.toDate.date.getTime()) {
            logger.logError($filter('translate')('error.from.date.greater.to.date'));
            return false;
        }

        if ($scope.global.toDate.date.getTime() - $scope.global.fromDate.date.getTime() > $scope.maxPeriodDuration * 24 * 60 * 60 * 1000) {
            logger.logError($filter('translate')('report.display.check.error.period', '{days: ' + $scope.maxPeriodDuration + '}'));
            return false;
        }

        return true;
    }

    $scope.export = function() {
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            if (!checkDates()) {
                return;
            } else {
                var fromDate = moment($scope.global.fromDate.date).format('YYYY-MM-DD');
                var toDate = moment($scope.global.toDate.date).format('YYYY-MM-DD');
                var salesmanId = undefined;
                if ($scope.global.currentSalesmanId != 'all') {
                    salesmanId = $scope.global.currentSalesmanId;
                }
                location.href = ADDRESS_BACKEND +
                    $scope.who + '/report/customer/check/export?' +
                    'fromDate=' + fromDate + '&toDate=' + toDate +
                    (salesmanId ? ('&salesmanId=' + salesmanId) : '') +
                    '&access_token=' + $token.getAccessToken();
            }
        }
    };

    init();

});
