app.controller('OrderReportCtrl', function($scope, $location, $log, $filter, $token, $state, $stateParams,
                                                 logger, Factory, $window, ADDRESS_BACKEND) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        var p = path.split("-");
        $scope.who = p[0];

        $scope.type = p[p.length - 1];

        // In month
        $scope.maxPeriodDuration = 2;
    }

    function init() {
        loadConfig();

        $scope.currentUserId = $token.getUserInfo().id;

        $scope.global = {};

        $scope.global.fromDate = {};
        $scope.global.fromDate.date = new Date();
        $scope.global.fromDate.opened = null;

        $scope.global.toDate = {};
        $scope.global.toDate.date = new Date();
        $scope.global.toDate.opened = null;

        initDistributors();
    }

    function initDistributors() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.distributors = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'distributor',
                'subCategory': 'all'
            },
            function(data){
                $scope.distributors = data.list;

                if ($scope.distributors == null) {
                    $scope.distributors = [];
                }

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
    };

    $scope.refresh = function() {
        init();
    };

    function checkDates() {
        $scope.global.fromDate.date = angular.truncateDate($scope.global.fromDate.date);
        $scope.global.toDate.date = angular.truncateDate($scope.global.toDate.date);

        if ($scope.global.fromDate.date.getTime() > $scope.global.toDate.date.getTime()) {
            logger.logError($filter('translate')('error.from.date.greater.to.date'));
            return false;
        }


        if (moment($scope.global.fromDate.date).add(2, 'months').toDate().getTime() < $scope.global.toDate.date.getTime()) {
            logger.logError($filter('translate')('report.order.check.error.period', '{months: ' + $scope.maxPeriodDuration + '}'));
            return false;
        }

        return true;
    }

    $scope.report = function() {
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            if (!checkDates()) {
                return;
            }
            var fromDate = moment($scope.global.fromDate.date).format('YYYY-MM-DD');
            var toDate = moment($scope.global.toDate.date).format('YYYY-MM-DD');
            var distributorId = $scope.global.currentDistributorId;

            $window.location.href = ADDRESS_BACKEND +
                $scope.who + '/export/order/' + $scope.type + '?' +
                'fromDate=' + fromDate + '&toDate=' + toDate +
                '&distributorId=' + distributorId +
                '&access_token=' + $token.getAccessToken();
        }
    };

    init();

});
