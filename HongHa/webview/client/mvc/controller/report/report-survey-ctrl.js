app.controller('ReportSurveyCtrl', function ($scope, $log, $filter, $location, $state, $token, logger, Factory,
                                             ADDRESS_BACKEND) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.tableCtrl = initTableCtrl(reloadData, null, null, null);

        $scope.tableCtrl.refresh();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'survey',
                'page': $scope.currentPage,
                'size': $scope.itemsPerPage,
                'draft': false
            },
            function (data) {
                $scope.tableCtrl.records = data.list;
                $scope.proccessing = false;
            },
            function (error) {
                logger.logError($filter('translate')('error.loading'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.getDateDisplay = function (survey) {
        var startDate = angular.parseDate(survey.startDate);
        var endDate = angular.parseDate(survey.endDate);
        return moment(startDate).format('DD/MM/YYYY') + ' - ' + moment(endDate).format('DD/MM/YYYY');
    };

    $scope.viewDetail = function (survey) {
        $state.go($scope.who + '-report-survey-detail', {id: survey.id});
    };

    $scope.refresh = function () {
        init();
    };

    $scope.export = function (survey) {
        location.href = ADDRESS_BACKEND + $scope.who + '/report/survey/' + survey.id + '/export?access_token=' + $token.getAccessToken();
    };

    init();
});
