app.controller('ReportSurveyDetailCtrl', function($scope, $log, $filter, $location, $state, $token, logger, $stateParams, Factory) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.surveyId = $stateParams.id;

        $scope.pieOptions = {
            series: {
                pie: {
                    show: true
                }
            },
            legend: {
                show: true
            },
            grid: {
                hoverable: true,
                clickable: false
            },
            colors: ["#60CD9B", "#66B5D7", "#EEC95A", "#E87352"],
            tooltip: !0,
            tooltipOpts: {
                content: "%p.0%, %s",
                defaultTheme: !1
            }
        };

        $scope.barOptions = {
            series: {
                stack: false,
                bars: {
                    show: true,
                    fill: true,
                    barWidth: .3,
                    align: "center",
                    horizontal: false
                }
            },
            legend: {
                show: false
            },
            grid: {
                hoverable: false,
                borderWidth: 1,
                borderColor: "#eeeeee"
            },
            colors: ["#60CD9B", "#66B5D7", "#EEC95A", "#E87352"]
        };

        reloadData();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.surveyResult = {};

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'report',
                'subCategory': 'survey',
                'param': $scope.surveyId
            },
            function(data) {
                $scope.surveyResult = data;

                transformAfterLoad();

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('error.loading'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function transformAfterLoad() {
        if (!angular.isUndefinedOrNull($scope.surveyResult)) {
            if(!angular.isUndefinedOrNull($scope.surveyResult.questions)) {
                for (var i = 0; i < $scope.surveyResult.questions.length; i++) {
                    var question = $scope.surveyResult.questions[i];
                    question.data = [];
                    question.option = {};

                    if(!angular.isUndefinedOrNull(question.options)) {
                        for (var j = 0; j < question.options.length; j++) {
                            var option = question.options[j];

                            question.data.push( {label: angular.escapeHTML(option.name) + '(' + option.result + ')', data: option.result} );
                        }
                        question.option = $scope.pieOptions;
                    }
                }
            }
        }
    }

    $scope.back = function() {
        $state.go($scope.who + '-report-survey-list');
    };

    init();
});
