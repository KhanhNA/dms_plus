app.controller('RouteDetailCtrl', function ($scope, $location, $filter, $log, logger, $stateParams,
                                               $state , $token, Factory) {
    function init() {
        $scope.title = $filter('translate')('menu.route');

        $scope.categoryName = 'route';

        var roleCodes = $token.getUserInfo().roleCodes;
        if (roleCodes != null && roleCodes.length > 0) {
            if (roleCodes[0] == 'AD') {
                $scope.who = 'admin';
            } else if (roleCodes[0] == 'SUP') {
                $scope.who = 'supervisor';
            }
        }

        $scope.recordId = $stateParams.id;
        if ($scope.recordId != 'new') {
            $scope.action = 'edit';
        } else {
            $scope.action = 'new';
        }

        loadDistributor();

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.isChanged = false;

        if ($scope.action == 'edit') {
            loadData($scope.recordId);
        } else {
            $scope.record = {};
            loadSalesmen();
        }
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': $scope.categoryName,
                id: id
            },
            function(data) {
                $scope.record = data;

                if (!angular.isUndefinedOrNull($scope.record.distributor)) {
                    $scope.record.distributorId = $scope.record.distributor.id;
                }

                if (!angular.isUndefinedOrNull($scope.record.salesman)) {
                    $scope.record.salesmanId = $scope.record.salesman.id;
                }

                $scope.proccessing = false;

                loadSalesmen();
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    $scope.ok = function (isEnable) {
        //Validate
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;

            // Edit record
            if ($scope.action === 'edit') {
                Factory.doPut(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName,
                        'id': $scope.record.id
                    },
                    $scope.record,
                    function () {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable($scope.record.id);
                        } else {
                            logger.logSuccess($filter('translate')('edit.success'));
                            $scope.refresh();
                        }
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        'who': $scope.who,
                        'category': $scope.categoryName
                    },
                    $scope.record,
                    function(data) {
                        $scope.proccessing = false;
                        if (isEnable) {
                            enable(data.id);
                        } else {
                            logger.logSuccess($filter('translate')('create.success'));
                            $scope.cancel();
                        }
                    },
                    function(){
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        }
    };

    $scope.cancel = function () {
        var params = {
            page: $stateParams.page ,
            search: $stateParams.search,
            distributor: $stateParams.distributor
        };
        $state.go($scope.categoryName + '-list', params);
    };

    $scope.isDraft = function() {
        return $scope.record == null || $scope.record.draft == null ||  $scope.record.draft;
    };

    $scope.isEdit = function() {
        return $scope.action == 'edit';
    };

    $scope.isNew = function() {
        return $scope.action == 'new';
    };

    function enable(domainId) {
        $scope.proccessing = true;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                id: domainId,
                action: 'enable'
            },
            null,
            function() {
                $scope.proccessing = false;
                if ($scope.action === 'edit') {
                    $scope.refresh();
                } else {
                    $scope.cancel();
                }
                logger.logSuccess($filter('translate')('enable.success'));

            },
            function() {
                $scope.proccessing = false;
                logger.logError($filter('translate')('enable.error'));
            }
        );
    }

    function loadDistributor() {
        $scope.proccessing = true;
        $scope.distributors = [];
        $scope.salesmen = [];

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'distributor',
                'subCategory': 'all'
            },
            function(data) {
                $scope.distributors = data.list;
                if ($scope.distributors == null) {
                    $scope.distributors = [];
                }
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    $scope.distributorChange = function() {
        $scope.markAsChanged();
        $scope.record.salesmanId = null;
        loadSalesmen();
    };

    function loadSalesmen() {
        $scope.salesmen = [];

        if ($scope.record.distributorId != null) {
            $scope.proccessing = true;

            Factory.doGet(
                {
                    'who': $scope.who,
                    'category': 'salesman',
                    'distributorId': $scope.record.distributorId
                },
                function(data) {
                    $scope.salesmen = data.list;
                    if ($scope.salesmen == null) {
                        $scope.salesmen = [];
                    }

                    $scope.salesmen.unshift({
                        id: null,
                        fullname: '--'
                    });

                    $scope.proccessing = false;
                },
                function() {
                    logger.logError($filter('translate')('loading.error'));
                    $scope.proccessing = false;
                    $scope.cancel();
                }
            );
        }
    }

    init();

});



