app.controller('SalesReportCtrl', function($scope, $log, $filter, $token, $location, $state, $stateParams,
                                                        logger, Factory) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.global = {};

        $scope.global.currentMonth = {};
        $scope.global.currentMonth.date = new Date();
        $scope.global.currentMonth.opened = false;

        $scope.global.productCategoryId = null;

        $scope.global.reportType = "daily";

        loadAllProductCategory();
    }

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
    };

    $scope.refresh = function() {
        init();
    };

    $scope.report = function() {
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            if ($scope.global.reportType == "daily") {
                $state.go($scope.who + '-sales-report-daily',
                    {
                        month: $scope.global.currentMonth.date.getMonth(),
                        year: $scope.global.currentMonth.date.getFullYear()
                    });
            } else if ($scope.global.reportType == "distributor") {
                $state.go($scope.who + '-sales-report-distributor',
                    {
                        month: $scope.global.currentMonth.date.getMonth(),
                        year: $scope.global.currentMonth.date.getFullYear()
                    });
            } else if ($scope.global.reportType == "product") {
                $state.go($scope.who + '-sales-report-product',
                    {
                        month: $scope.global.currentMonth.date.getMonth(),
                        year: $scope.global.currentMonth.date.getFullYear(),
                        productCategoryId: $scope.global.productCategoryId
                    });
            } else if ($scope.global.reportType == "salesman") {
                $state.go($scope.who + '-sales-report-salesman',
                    {
                        month: $scope.global.currentMonth.date.getMonth(),
                        year: $scope.global.currentMonth.date.getFullYear()
                    });
            }
        }
    };

    function loadAllProductCategory() {
        $scope.productCategorys = [];

        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'productcategory',
                'subCategory': 'all'
            },
            function(data) {
                $scope.productCategories = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    init();

});
