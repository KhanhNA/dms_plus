app.controller('VisitReportCtrl', function($scope, $location, $log, $filter, $token, $location, $state, $stateParams,
                                                 logger, Factory) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.currentUserId = $token.getUserInfo().id;

        $scope.global = {};

        $scope.global.fromDate = {};
        $scope.global.fromDate.date = new Date();
        $scope.global.fromDate.opened = null;

        $scope.global.toDate = {};
        $scope.global.toDate.date = new Date();
        $scope.global.toDate.opened = null;

        initSalesmen();
    }

    function initSalesmen() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.salesmen = [];
        $scope.global.currentSalesmanId = null;

        Factory.doGet(
            {
                'who': $scope.who,
                'category': 'salesman'
            },
            function(data){
                if (data.list == null) {
                    $scope.salesmen = [];
                } else {
                    angular.copy(data.list, $scope.salesmen);
                }

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.open = function($event, data) {
        $event.preventDefault();
        $event.stopPropagation();
        data.opened = true;
    };

    $scope.refresh = function() {
        init();
    };

    function checkDates() {
        $scope.global.fromDate.date = angular.truncateDate($scope.global.fromDate.date);
        $scope.global.toDate.date = angular.truncateDate($scope.global.toDate.date);

        if ($scope.global.fromDate.date.getTime() > $scope.global.toDate.date.getTime()) {
            return false;
        }

        return true;
    }

    $scope.report = function() {
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            if (!checkDates()) {
                logger.logError($filter('translate')('error.from.date.greater.to.date'));
            } else {
                $state.go($scope.who + '-visit-report-detail',
                    {
                        fromDate: angular.formatDate($scope.global.fromDate.date),
                        toDate: angular.formatDate($scope.global.toDate.date),
                        salesmanId: $scope.global.currentSalesmanId
                    });
            }
        }
    };

    init();

});
