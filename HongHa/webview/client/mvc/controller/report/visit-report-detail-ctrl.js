app.controller('VisitReportDetailCtrl', function ($scope, $location, $log, $filter, $state, $stateParams, logger,
                                                  Factory) {
    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }

        $scope.who = path.split("-")[0];
    }

    function init() {
        loadConfig();

        $scope.global = {};
        $scope.global.fromDate = $stateParams.fromDate;
        $scope.global.toDate = $stateParams.toDate;
        $scope.global.currentSalesmanId = $stateParams.salesmanId;

        reloadData();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.records = [];

        var queryParams = {
            'who': $scope.who,
            'category': 'report',
            'subCategory': 'visit',
            'fromDate': $scope.global.fromDate,
            'toDate': $scope.global.toDate,
            'salesmanId': $scope.global.currentSalesmanId
        };

        Factory.doGet(
            queryParams,
            function (data) {
                $scope.records = data.list;

                if (angular.isUndefinedOrNull($scope.records)) {
                    $scope.records = [];
                }

                $scope.proccessing = false;
            },
            function () {
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.refresh = function () {
        reloadData();
    };

    $scope.back = function () {
        $state.go($scope.who + '-visit-report');
    };

    $scope.getDateDisplay = function () {
        var fromDateO = angular.parseDate($scope.global.fromDate);
        var toDateO = angular.parseDate($scope.global.toDate);

        return $filter('translate')('report.visit.monthly.from.date') + ' ' + moment(fromDateO).format('DD/MM/YYYY')
            + ' '
            + $filter('translate')('report.visit.monthly.to.date') + ' ' + moment(toDateO).format('DD/MM/YYYY');
    };

    $scope.printReport = function () {
        var popupWin, printContents;
        return printContents = document.getElementById("report").innerHTML,
            popupWin = window.open(),
            popupWin.document.open(),
            popupWin.document.write(
                '<html>' +
                '<head>' +
                '<link rel="stylesheet" type="text/css" href="styles/main.css"/>' +
                '</head>' +
                '<body onload="window.print()" class="print page-invoice">' +
                printContents +
                '</body>' +
                '</html>'),
            popupWin.document.close()
    };

    init();

});
