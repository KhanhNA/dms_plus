app.controller('ClientConfigCtrl', function($scope, $filter, $token, logger, Factory, PATTERN) {

    function init() {
        $scope.title = $filter('translate')('client.config');
        $scope.who = 'super-admin';
        $scope.categoryName = 'system-config';

        $scope.pattern = PATTERN;

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.isChanged = false;

        Factory.doGet(
            {
                who: $scope.who,
                category: $scope.categoryName
            },
            function(data){
                $scope.record = data;

                if (angular.isUndefinedOrNull($scope.record)) {
                    $scope.record = {};
                }

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    $scope.save = function() {
        $scope.proccessing = true;
        $scope.error = false;

        delete $scope.record.code;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName
            },
            $scope.record,
            function(){
                $scope.proccessing = false;
                logger.logSuccess($filter('translate')('save.success'));
                $scope.refresh();
            },
            function(error) {
                console.log(error);

                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    $scope.markAsChanged = function() {
        $scope.isChanged = true;
    };

    init();
});


