app.controller('SystemCtrl', function($scope, $filter, $token, logger, Factory) {

    function init() {
        $scope.title = $filter('translate')('super.admin.system.title');
        $scope.who = 'super-admin';
        $scope.categoryName = 'system';
    }

    $scope.initCache = function() {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doPut(
            {
                who: $scope.who,
                category: $scope.categoryName,
                subCategory: 'cache',
                action: 'init'
            },
            $scope.record,
            function(){
                $scope.proccessing = false;
                logger.logSuccess($filter('translate')('save.success'));
                $scope.refresh();
            },
            function() {
                logger.logSuccess($filter('translate')('save.error'));
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    init();
});


