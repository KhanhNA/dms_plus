app.controller('SUPCustomerApproveCtrl', function($scope, $log, $filter, $location, $state, logger, $modal, Factory, STATE_CONFIG) {

    function init() {
        loadConfig();

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.tableCtrl = initTableCtrl(reloadDatas, null, editRecord, null);

        $scope.tableCtrl.refresh();
    }

    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }
        $scope.config = STATE_CONFIG[path];

        $scope.title = $filter('translate')('customer.approve.title');
        $scope.categoryName = path;
    }

    function reloadDatas() {
        $scope.proccessing = true;
        $scope.error = false;

        var queryString = {
            'who': 'supervisor',
            'category': 'customer',
            'subCategory': 'pending',
            'page': $scope.currentPage,
            'size': $scope.itemsPerPage
        };
        if ($scope.searchText && $scope.searchText.length > 0) {
            queryString.q = $scope.searchText;
        }

        Factory.doGet(
            queryString,
            function(data) {
                $scope.tableCtrl.records = data.list;
                initSelectableArray($scope.tableCtrl.records, null);

                $scope.totalItems = data.count;

                $scope.tableCtrl.updateSelectAllStatus();

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function editRecord(record) {
        $state.go( $scope.categoryName + '-detail', { id:record.id } );
    }

    init();

});
