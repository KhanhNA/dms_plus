app.controller('SUPCustomerApproveDetailCtrl', function ($scope, $filter, $log, logger, $stateParams, $state,
                                                      Factory, dialogs, $modal, uiGmapIsReady) {

    function init() {
        $scope.title = $filter('translate')('customer.approve.detail.title');
        $scope.recordId = $stateParams.id;

        $scope.refresh();
    }

    $scope.refresh = function() {
        $scope.isChanged = false;

        loadData($scope.recordId);

        $scope.salesman = {};
    };

    function loadData(id) {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'customer',
                'subCategory': 'pending',
                'param': id
            },
            function(data) {
                $scope.record = data;

                initMap();

                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.approve = function() {
        var dialog = dialogs.confirm(
            $filter('translate')('dialog.title.notice'),
            $filter('translate')('customer.approve.detail.notice.approve'),
            {
                size: 'md',
                backdrop: true
            }
        );

        dialog.result.then(
            function() {
                $scope.proccessing = true;

                Factory.doPut(
                    {
                        'who': 'supervisor',
                        'category': 'customer',
                        'subCategory': 'pending',
                        'param': $scope.recordId,
                        'action': 'approve'
                    },
                    { },
                    function () {
                        $scope.proccessing = false;
                        logger.logSuccess($filter('translate')('customer.approve.detail.notice.approve.success'));

                        // Ask if user want to specify route for this customer
                        var dlg = dialogs.confirm(
                            $filter('translate')('dialog.title.notice'),
                            $filter('translate')('customer.approve.detail.notice.spcify.schedule'),
                            {
                                size: 'md',
                                backdrop: true
                            }
                        );

                        dlg.result.then(
                            function() {
                                openSpecifySchedulePopup($scope.record.id);
                            },
                            function() {
                                $scope.cancel();
                            }
                        );
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        );
    };

    $scope.reject = function() {

        var dlg = dialogs.confirm(
            $filter('translate')('dialog.title.notice'),
            $filter('translate')('customer.approve.detail.notice.reject'),
            {
                size: 'md',
                backdrop: true
            }
        );

        dlg.result.then(
            function() {
                $scope.proccessing = true;
                Factory.doPut(
                    {
                        'who': 'supervisor',
                        'category': 'customer',
                        'subCategory': 'pending',
                        'param': $scope.recordId,
                        'action': 'reject'
                    },
                    { },
                    function () {
                        $scope.proccessing = false;
                        logger.logSuccess($filter('translate')('customer.approve.detail.notice.reject.success'));
                        $scope.cancel();
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        );
    };

    $scope.cancel = function () {
        $state.go('customer-approve-list');
    };

    function openSpecifySchedulePopup(customerId) {
        var modalInstance = $modal.open({
            templateUrl: 'mvc/view/supervisor/visit-schedule-popup.html',
            controller: 'SUPVisitSchedulePopupCtrl',
            size: 'lg',
            resolve: {
                customerId: function () {
                    return customerId;
                }
            }
        });
        modalInstance.result.then(function () {
            $scope.cancel();
        }, function () {
            $scope.cancel();
        });
    }

    function initMap() {
        var defaultCenter = {latitude: 21.084269026984, longitude: 105.82028125};
        var coords = {};// --> Ha Noi

        if (!angular.isUndefined($scope.record)
            && !angular.isUndefined($scope.record.latitude) && angular.isNumber($scope.record.latitude)
            && !angular.isUndefined($scope.record.longitude) && angular.isNumber($scope.record.longitude)) {

            defaultCenter = { latitude: $scope.record.latitude, longitude: $scope.record.longitude };
            coords = { latitude: $scope.record.latitude, longitude: $scope.record.longitude };
        }

        $scope.map = {
            center: defaultCenter,
            zoom: 13
        };

        $scope.marker = {
            id: 0,
            title: 'customer',
            coords: coords,
            options: { draggable: false },
            windowOptions: {
                visible: false
            }
        };

        $scope.control = {};

        uiGmapIsReady.promise().then(function (maps) {
            $scope.control.refresh();
        });
    }

    init();

});
    


