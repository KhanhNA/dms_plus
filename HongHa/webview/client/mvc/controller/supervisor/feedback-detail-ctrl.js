app.controller('SUPFeedbackDetailCtrl', function($scope, $log, $filter, $location, $state, $stateParams, $token, logger, $modal, Factory) {

    function init() {
        $scope.feedbackId = $stateParams.id;
        console.log($scope.feedbackId);
        reloadData();
    }

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        $scope.feedback = {};

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'feedback',
                'id': $scope.feedbackId
            },
            {},
            function(data) {
                $scope.feedback = data;

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('error.loading'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.back = function() {
        $state.go('feedback-list');
    };

    $scope.getTimeDisplay = function(isoTime) {
        return moment(angular.parseDateTime(isoTime)).format('DD/MM/YYYY HH:mm');
    };

    $scope.refresh = function() {
      init();
    };

    init();
});
