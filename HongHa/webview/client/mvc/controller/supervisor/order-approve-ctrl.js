app.controller('SUPOrderApproveCtrl', function($scope, $token, $log, $filter, $location, $state, logger, $modal, STATE_CONFIG,
                                               Factory) {

    function init() {
        loadConfig();

        $scope.title = $filter('translate')('approve.purchase.order');
        $scope.currentUserId = $token.getUserInfo().id;

        $scope.global = {};

        $scope.isApproveView = true;

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;

        $scope.distributors = [];
        $scope.salesmen = [];
        $scope.customers = [];

        $scope.NO_ID_VALUE = 'NoId';

        $scope.global.currentDistributorId = '';
        $scope.global.currentSalesmanId = '';
        $scope.global.currentCustomerId = '';

        initDistributors();

        $scope.tableCtrl = initTableCtrl(reloadDatas, null, editRecord, null);

        $scope.tableCtrl.refresh();
    }

    function loadConfig() {
        var path = $location.path();
        if (path.indexOf("/") == 0) {
            path = path.substring(1, path.length);
        }
        if (path.indexOf("/") > 0) {
            path = path.split("/")[0];
        }
        $scope.config = STATE_CONFIG[path];

        //$scope.title = $filter('translate')($scope.config.title);
        $scope.categoryName = path;
    }

    function initDistributors() {
        $scope.proccessing = true;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'distributor'
            },
            function(data){
                $scope.distributors = data.list;

                if ($scope.distributors.length == 0) {
                    $scope.distributors = [{
                        id: $scope.NO_ID_VALUE,
                        code: 'X',
                        name: $filter('translate')('purchase.order.filter.no.distributor')
                    }];
                } else if ($scope.distributors.length > 1) {
                    $scope.distributors.unshift({
                        id: '',
                        code: 'X',
                        name: $filter('translate')('purchase.order.filter.distributor.clear')
                    });
                }

                $scope.global.currentDistributorId = $scope.distributors[0].id;

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function reloadDatas() {
        if ($scope.global.currentDistributorId == $scope.NO_ID_VALUE
                || $scope.global.currentSalesmanId == $scope.NO_ID_VALUE
                || $scope.global.currentCustomerId == $scope.NO_ID_VALUE) {

            $scope.tableCtrl.records = [];
            $scope.totalItems = 0;

            initSelectableArray($scope.tableCtrl.records, null);

            $scope.tableCtrl.updateSelectAllStatus();

            return;
        }

        $scope.proccessing = true;
        $scope.error = false;

        var queryString = {
            'who': 'supervisor',
            'category': 'order',
            'subCategory': 'pending',
            'page': $scope.currentPage,
            'size': $scope.itemsPerPage,
            'distributorId': $scope.global.currentDistributorId
        };

        Factory.doGet(
            queryString,
            function(data) {
                $scope.tableCtrl.records = data.list;
                $scope.totalItems = data.count;

                if ($scope.tableCtrl.records == null) {
                    $scope.tableCtrl.records = [];
                    $scope.totalItems = 0;
                }

                initSelectableArray($scope.tableCtrl.records, null);

                $scope.tableCtrl.updateSelectAllStatus();

                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function editRecord(record) {
        if ($scope.config.isUsePopup) {
            var modalInstance = $modal.open({
                templateUrl: 'mvc/view/template/simple-category-detail-popup.html',
                controller: 'SimpleCategoryDetailPopupCtrl',
                resolve: {
                    record: function () {
                        return record;
                    },
                    categoryName: function () {
                        return $scope.categoryName
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.tableCtrl.refresh();
            }, function () {
            });
        } else {
            $state.go( $scope.categoryName + '-detail', { id:record.id } );
        }
    }

    $scope.search = function() {
        reloadDatas();
    };

    init();

});
