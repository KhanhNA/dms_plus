app.controller('SUPReportExhibitionSummaryCtrl', function($scope, $log, $filter, $location, $state, $token, logger, $stateParams,
                                                       Factory) {

    function init() {
        $scope.recordId = $stateParams.id;
        $scope.record = {};
        $scope.charts = [];

        reloadData();
    }

    $scope.back = function () {
        $state.go('exhibition-report-list');
    };

    function reloadData() {
        $scope.proccessing = true;
        $scope.error = false;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'report',
                'subCategory': 'exhibition',
                'param': $scope.recordId
            },
            function(data) {
                $scope.record = data;
                $scope.record.time = $filter('user_date_format')($scope.record.startDate) + ' - ' + $filter('user_date_format')($scope.record.endDate);

                var meetRate = data.meetRequirementRates;
                var participateRate = data.participationRates;
                $scope.charts = [
                    {
                        title: '',
                        options: {
                                series: {
                                    pie: {
                                        show: true
                                    }
                                },
                                colors: [
                                    '#e87352',
                                    '#eec95a'
                                ]
                            },
                        data:
                            [{
                                label: "&nbsp;" + $filter('translate')('report.exhibition.summary.participated.caption.rated', '{percen: ' + participateRate + '}'),
                                data: participateRate
                            }, {
                                label: "&nbsp;" + $filter('translate')('report.exhibition.summary.participated.caption.unrated', '{percen: ' + (100 - participateRate) + '}'),
                                data: 100 - participateRate
                            }]
                    },
                    {
                        title: '',
                        options: {
                            series: {
                                pie: {
                                    show: true
                                }
                            },
                            colors: [
                                '#60cd9b',
                                '#66b5d7'
                            ]
                        },
                        data:
                            [{
                                label: "&nbsp;" + $filter('translate')('report.exhibition.summary.meet.req.caption.meet', '{percen: ' + meetRate + '}'),
                                data: meetRate
                            }, {
                                label: "&nbsp;" + $filter('translate')('report.exhibition.summary.meet.req.caption.not.meet', '{percen: ' + (100 - meetRate) + '}'),
                                data: 100 - meetRate
                            }]
                    }
                ];
                $scope.proccessing = false;
            },
            function(error) {
                logger.logError($filter('translate')('error.loading'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    init();
});
