app.controller('SUPTargetDetailPopupCtrl', function ($scope, $modalInstance, $filter, logger, record, date, categoryName,
                                                  Factory, PATTERN, $token) {

    function init() {
        $scope.pattern = PATTERN;
        $scope.currentUserId = $token.getUserInfo().id;

        var id;
        if (record) {
            id = record.id;
            $scope.title = $filter('translate')('edit');
            $scope.action = 'edit';
            loadData(id);
        } else {
            $scope.title = $filter('translate')('new');
            $scope.action = 'new';
            $scope.record = {};
            if (!angular.isUndefined(date)) {
                $scope.record.month = date.month;
                $scope.record.year = date.year;
            }


            loadSalesman();
        }
    }

    function loadData(id) {
        $scope.proccessing = true;
        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'target',
                'id': id
            },
            function(data) {
                if (angular.isUndefined(data)) {
                    logger.logError($filter('translate')('loading.error'));
                    $scope.proccessing = false;
                    $scope.cancel();
                } else {
                    $scope.record = data;
                    $scope.proccessing = false;
                }
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    $scope.ok = function () {
        //Validate
        if (!$scope.form.$valid) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
        } else {
            $scope.proccessing = true;
            // Edit record
            if ($scope.action == 'edit') {
                Factory.doPut(
                    {
                        'who': 'supervisor',
                        'category': 'target',
                        'id': $scope.record.id
                    },
                    $scope.record,
                    function () {
                        $scope.proccessing = false;
                        $modalInstance.close();
                        logger.logSuccess($filter('translate')('edit.success'));
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
            // Create new record
            else {
                Factory.doPost(
                    {
                        'who': 'supervisor',
                        'category': 'target'
                    },
                    $scope.record,
                    function () {
                        $scope.proccessing = false;
                        $modalInstance.close();
                        logger.logSuccess($filter('translate')('create.success'));
                    },
                    function () {
                        $scope.proccessing = false;
                        logger.logError($filter('translate')('save.error'));
                    }
                );
            }
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    function loadSalesman() {
        $scope.proccessing = true;
        $scope.salesmen = [];

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'salesman',
                'subCategory': 'notarget',
                'month': $scope.record.month,
                'year': $scope.record.year
            },
            function(data) {
                $scope.salesmen = data.list;
                $scope.proccessing = false;
            },
            function() {
                logger.logError($filter('translate')('loading.error'));
                $scope.proccessing = false;
                $scope.cancel();
            }
        );
    }

    init();

});
