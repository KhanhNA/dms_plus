app.controller('SUPVisitSchedulePopupCtrl', function($scope, $filter, $token, logger, Factory, customerId, $modalInstance, $log) {

    function init() {
        $scope.title = $filter('translate')('visit.schedule');

        $scope.currentUserId = $token.getUserInfo().id;
        $scope.customerId = customerId;

        $scope.global = {};

        $scope.proccessing = false;
        $scope.error = false;

        $scope.isChanged = false;

        //TODO load from client info
        $scope.numberWeekFrequency = 4;
        $scope.weeks = [];
        for (var i = 0; i < $scope.numberWeekFrequency; i++) {
            $scope.weeks.push( { label: 'W' + (i + 1), index: i } );
        }

        initData();
    }

    $scope.hasSalesman = function() {
        return ($scope.salesmen != null && $scope.salesmen.length > 0);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    function initData() {
        $scope.proccessing = true;
        $scope.data = undefined;
        $scope.global.currentSalesman = {};
        $scope.customerSchedule = {items:[]};
        $scope.addNewItem();

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'customer',
                'id': customerId
            },
            function(data) {
                $scope.data = data;

                initSalesmen($scope.data.distributor.id);

                $scope.proccessing = false;
            },
            function(error){
                logger.logError($filter('translate')('loading.error'));
                $log.log(error);
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    function initSalesmen(distributorId) {
        $scope.proccessing = true;

        Factory.doGet(
            {
                'who': 'supervisor',
                'category': 'salesman',
                'distributorId' : distributorId
            },
            function(data){
                $scope.salesmen = data.list;
                if ($scope.salesmen.length > 0) {
                    $scope.global.currentSalesman = $scope.salesmen[0];
                }

                $scope.proccessing = false;
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    }

    $scope.reloadSalesmen = function($item) {
        initSalesmen($item.id);
    };

    $scope.save = function() {
        if (angular.isUndefinedOrNull($scope.global.currentSalesmanId)
            || angular.isUndefinedOrNull($scope.customerSchedule.items)
            || $scope.customerSchedule.items.length == 0) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
            return;
        }
        $scope.proccessing = true;
        $scope.error = false;

        var requestBody = {
            salesmanId: $scope.global.currentSalesmanId,
            items: transformToVisitScheduleItems()
        };

        Factory.doPut(
            {
                'who': 'supervisor',
                'category': 'visitschedule',
                'subCategory': 'bycustomer',
                'customerId': $scope.customerId
            },
            requestBody,
            function(){
                $scope.proccessing = false;
                logger.logSuccess($filter('translate')('save.success'));
                $scope.cancel();
            },
            function(){
                $scope.proccessing = false;
                $scope.error = true;
            }
        );
    };

    $scope.clearCustomerSchedule = function() {
        $scope.customerSchedule.items = [];
        $scope.addNewItem($scope.customerSchedule);
    };

    $scope.clearCustomerScheduleItem = function(item) {
        if ($scope.customerSchedule.items.length > 1) {
            $scope.customerSchedule.items.splice($scope.customerSchedule.items.indexOf(item), 1);
        } else {
            $scope.clearCustomerSchedule();
        }
    };

    $scope.addNewItem = function() {
        var item = {
            isMonday: false,
            isTuesday: false,
            isWednesday: false,
            isThursday: false,
            isFriday: false,
            isSaturday: false,
            isSunday: false
        };

        item.weekFrequencys = [];
        for (var i = 0; i < $scope.numberWeekFrequency; i++) {
            var weekFrequency = {};
            weekFrequency.selected = true;
            weekFrequency.label = 'W' + (i + 1);
            weekFrequency.weekNumber = i + 1;
            item.weekFrequencys.push(weekFrequency);
        }

        $scope.customerSchedule.items.push(item);
    };

    function transformToVisitScheduleItems() {
        if (angular.isUndefinedOrNull($scope.data)) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
            return;
        }

        if (angular.isUndefinedOrNull($scope.customerSchedule.items)) {
            logger.logError($filter('translate')('error.data.input.not.valid'));
            return;
        }

        var items = [];
        angular.forEach($scope.customerSchedule.items, function(item) {
            item.days = [];
            if (item.isMonday) { item.days.push(2); }
            if (item.isTuesday) { item.days.push(3); }
            if (item.isWednesday) { item.days.push(4); }
            if (item.isThursday) { item.days.push(5); }
            if (item.isFriday) { item.days.push(6); }
            if (item.isSaturday) { item.days.push(7); }
            if (item.isSunday) { item.days.push(8); }

            if (!angular.isUndefinedOrNull(item.weekFrequencys)) {
                item.weeks = [];
                angular.forEach(item.weekFrequencys, function(weekFrequency) {
                    if (weekFrequency.selected) { item.weeks.push(weekFrequency.weekNumber); }
                });
            }
            items.push(item);
        });

        return items;
    }

    init();

});
