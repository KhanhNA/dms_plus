app.factory('Factory',
    function($resource, ADDRESS_BACKEND) {
        return $resource(ADDRESS_BACKEND + ':who/:category/:id/:subCategory/:param/:action/:subAction',
            null,
            {
                doPost: {
                    method: 'POST',
                    headers: {'Content-Type':'application/json'}
                },
                doPut: {
                    method: 'PUT',
                    headers: {'Content-Type':'application/json'}
                },
                doGet: {
                    method: 'GET',
                    isArray: false
                },
                doDelete: {
                    method: 'DELETE'
                }
            }
        );
    }
);
