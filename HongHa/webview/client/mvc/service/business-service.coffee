_function = (Factory, $log, $filter, $location, $state, $stateParams, logger, $modal, $token, STATE_CONFIG) ->

  #  GET STATE WHO
  this.getWho = ->
    roleCodes = $token.getUserInfo().roleCodes

    if roleCodes? and roleCodes.length > 0
      if roleCodes[0] is 'AD' then 'admin'
      else if roleCodes[0] is 'SUP' then 'supervisor'
      else if roleCodes[0] is 'DIS' then 'distributor'
      else null
    else null

  #  GET STATE CONFIG
  this.getStateConfig = ->
    path = $location.path()

    if _.startsWith(path, '/') then path.substring(1, path.length)
    if path.indexOf('/') > 0 then path = path.split("/")[0]

    if path then STATE_CONFIG[path] else null

  #  LOAD SINGLE DATA FROM FACTORY
  this.loadSingleData = (who, status, callback, params) ->
    status.processing = true
    status.error = false

    onSuccess = (data) ->
      status.processing = false
      if callback? and typeof callback is 'function' then callback(if data? then data else {})

    onFailure = (error) ->
      status.processing = false
      status.error = true
      $log.log error

    Factory.doGet params, onSuccess, onFailure

  #  LOAD LIST DATA FROM FACTORY
  this.loadListData = (who, status, callback, params) ->
    status.processing = true
    status.error = false

    console.log status

    onSuccess = (data) ->
      status.processing = false
      list = if data? and data.list? then data.list else []
      count = if data? and data.list? then data.count else 0
      if callback? and typeof callback is 'function' then callback(list, count)

    onFailure = (error) ->
      status.processing = false
      status.error = true
      $log.log error

    Factory.doGet params, onSuccess, onFailure

  #  LOAD CLIENT CONFIG
  this.loadClientConfig = (who, status, callback) ->
    params =
      who: who
      category: 'clientconfig'

    this.loadSingleData who, status, callback, params

  #  LOAD DISTRIBUTOR
  this.loadDistributors = (who, status, callback) ->
    params =
      who: who
      category: 'distributor'
      subCategory: 'all'

    this.loadListData who, status, callback, params

  #  LOAD ROUTE BY DISTRIBUTOR
  this.loadRoutesByDistributor = (who, distributorId, status, callback) ->
    params =
      who: who
      category: 'route'
      distributorId : distributorId
      draft: false
      active: true

    this.loadListData who, status, callback, params

  # INIT SCOPE LOADING STATUS FROM FACTORY
  this.initScopeForLoadStatus = (scope, allStatus) ->
    scope.isProcessing = ->
      for property of allStatus
        status = allStatus[property]
        if status? and status.processing then return true
      return false

    scope.isError = ->
      for property of allStatus
        status = allStatus[property]
        if status? and status.error then return true
      return false

  # INIT SCOPE FOR CHANGE STATUS WHEN EDIT
  this.initScopeForChangeStatus = (scope, statusContainer) ->
    if statusContainer?
      statusContainer.isChanged = false
      scope.markAsChanged = -> statusContainer.isChanged = true
    else
      scope.isChanged = false
      scope.markAsChanged = -> scope.isChanged = true

  # INIT TABLE FILTER
  this.initTableFilter = (filter) ->
    filter.currentPage = 1;
    filter.itemsPerPage = 10;
    filter.maxSize = 5;
    filter.searchText = null;

  return

#***************************************
app.service 'BusinessService', _function

