'use strict';

angular.module('app', [
    # Angular modules
    'ui.router'
    'ngAnimate'
    'ngResource'

    # 3rd Party Modules
    'ui.bootstrap'
    'easypiechart'
    'mgo-angular-wizard'
    'textAngular'
    'ui.tree'
    'ngMap'
    'ngTagsInput'
    'pascalprecht.translate'
    'fcsa-number'
    'ui.select'
    'angular-confirm'
    'ngIdle'

    # System
    'app.config'
    'app.state'
    'app.authentication'
    'app.toast'
    'app.filter'
    'app.push'

    # Directive
    'app.directives'
    'app.directives.file.image'
])

.config [
    'AuthenticationProvider', '$translateProvider', '$provide', 'ADDRESS_BACKEND', 'ADDRESS_OAUTH', 'DEFAULT_LANGUAGE'
    (AuthenticationProvider, $translateProvider, $provide, ADDRESS_BACKEND, ADDRESS_OAUTH, DEFAULT_LANGUAGE) ->
        # Configure Authentication service for login management
        AuthenticationProvider.configure {
            'clientId': 'dmsplus'
            'baseUrl': ADDRESS_OAUTH
            'scope': "read"
            'deniedRoles': ['SM']
            'noAuthStates': ['400', '500', '550']
        }

        # Configure multi-language support
        $translateProvider.preferredLanguage DEFAULT_LANGUAGE
        $translateProvider.useStaticFilesLoader { prefix: 'i18n/', suffix: '.json' }

        # Configure file image chooser
        $provide.decorator('fileImageChooserConfig', [
            'Authentication', 'ADDRESS_BACKEND'
            (Authentication, ADDRESS_BACKEND) ->
                getBaseUrl: -> ADDRESS_BACKEND
                getAccessToken: -> Authentication.getAccessToken()
        ])
]

# Configure Idle detector and Auto-ping service to keep session alive
.config [
    'KeepaliveProvider', 'IdleProvider', 'NG_IDLE_MAX_IDLE_TIME', 'NG_IDLE_TIMEOUT', 'NG_IDLE_PING_INTEVAL'
    (KeepaliveProvider, IdleProvider, NG_IDLE_MAX_IDLE_TIME, NG_IDLE_TIMEOUT, NG_IDLE_PING_INTEVAL) ->
        # Time out
        IdleProvider.idle(NG_IDLE_MAX_IDLE_TIME);

        # Count down time before logout
        IdleProvider.timeout(NG_IDLE_TIMEOUT);

        # Auto-ping interval
        KeepaliveProvider.interval(NG_IDLE_PING_INTEVAL);
]

.run ['$rootScope', 'Authentication', '$translate', 'DEFAULT_LANGUAGE', 'Idle'
    ($rootScope, Authentication, $translate, DEFAULT_LANGUAGE, Idle) ->
        $rootScope.tokenVerified = false

        # Load saved language
        lang = Authentication.getUserLanguage()
        if lang? then $translate.use(lang) else $translate.use(DEFAULT_LANGUAGE)

        $rootScope.$on('TokenVerified', ->
            $translate.use(Authentication.getUserLanguage())
            $rootScope.tokenVerified = true
        )

        # Watch Idle events (except when app run inside iframe to refresh token)
        if not Authentication.isOAuthFrame()
            Idle.watch()
]
