'use strict'

angular.module('app')

.controller('CheckInSearchCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state', '$modal'
    ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $modal) ->
        $scope.title = 'check.in.search.title'

        # FILTER
        $scope.search = ->
            if not $scope.filter.createdById?
                toast.logError($filter('translate')('please.select.created.by'))
            else if $scope.isDisplayFromDateToDate() and not checkDate()
                toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
            else if $scope.isDisplayFromDateToDate() and not checkDateDuration()
                toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
            else if $scope.isDisplayDate() and not ($scope.date? and $scope.date.date?)
                toast.logError($filter('translate')('please.select.date'))
            else
                createdBy = _.find($scope.createdByList, (o) -> o.id is $scope.filter.createdById)

                filter =
                    createdById: $scope.filter.createdById
                    createdByName: if createdBy? then createdBy.fullname else null

                if $scope.isDisplayFromDateToDate()
                    filter.fromDate = globalUtils.createIsoDate($scope.fromDate.date)
                    filter.toDate = globalUtils.createIsoDate($scope.toDate.date)
                    $state.go('check-in-list', { filter: $scope.convertFilterAsString(filter) })
                else
                    filter.date = globalUtils.createIsoDate($scope.date.date)
                    $state.go('check-in-timeline', { filter: $scope.convertFilterAsString(filter) })

        checkDateDuration = ->
            if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
                return false
            return true

        checkDate = ->
            if $scope.fromDate? and  $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
                if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
                    return true
            return false

        $scope.isDisplayDate           = -> $scope.filter.viewOnMap
        $scope.isDisplayFromDateToDate = -> not $scope.filter.viewOnMap

        CtrlInitiatorService.initFilterViewCtrl($scope)
        CtrlInitiatorService.initUseDatePickerCtrl($scope)

        $scope.addInitFunction( ->
            if not $scope.filter.viewOnMap?
                $scope.filter.viewOnMap = false

            if $scope.filter.fromDate? and $scope.filter.toDate?
                $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
                $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
                if not checkDate() or not checkDateDuration()
                    $state.go('404')
            else
                $scope.fromDate = $scope.createDatePickerModel(new Date())
                $scope.toDate = $scope.createDatePickerModel(new Date())

            if $scope.filter.date?
                $scope.date = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.date))
            else
                $scope.date = $scope.createDatePickerModel(new Date())

            $scope.createdByList = []

            LoadingUtilsService.loadSupervisors(
                $scope.who
                $scope.loadStatus.getStatusByDataName('supervisors')
                (list) ->
                    $scope.createdByList = _.sortBy(_.union($scope.createdByList, list), (o) ->  return o.fullname)
            )
            if  $scope.who is 'admin'
                LoadingUtilsService.loadAdvanceSupervisors(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('advanceSupervisors')
                    (list) ->
                        $scope.createdByList = _.sortBy(_.union($scope.createdByList, list), (o) ->  return o.fullname)
                )
        )

        $scope.init()
])


.controller('CheckInListCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state', '$stateParams'
    ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $stateParams) ->
        $scope.title = 'check.in.list.title'

        $scope.defaultBackState = 'check-in-search'
        $scope.getParentFilter = ->
            if not $stateParams.parent? || $stateParams.parent is 'check-in-search'
                parentFilter =
                    viewOnMap: false
                    fromDate: $scope.filter.fromDate
                    toDate: $scope.filter.toDate
                    createdById: $scope.filter.createdById

                return $scope.convertFilterAsString( parentFilter )
            else
                return $stateParams.parentFilter ? $stateParams.filter

        $scope.goToDetail = (record) ->
            $state.go('check-in-detail', { id: record.id, parent: 'check-in-list', filter: $stateParams.filter })

        checkDateDuration = ->
            if moment($scope.fromDate).add(1, 'months').toDate().getTime() < $scope.toDate.getTime()
                return false
            return true

        checkDate = ->
            if $scope.fromDate? and $scope.toDate?
                if $scope.fromDate.getTime() <= $scope.toDate.getTime()
                    return true
            return false

        $scope.isLocationUndefined = (record) -> not (record? and record.location?)
        $scope.isAddressUndefined = (record) -> not (record? and record.location? and record.province?)
        $scope.getLocationDisplay = (record) ->
            if $scope.isLocationUndefined(record)
                $filter('translate')('location.undefined')
            else if $scope.isAddressUndefined(record)
                $filter('translate')('address.undefined')
            else
                record.province.name

        $scope.hasPhoto = (record) -> record? and record.nbPhoto? and record.nbPhoto > 0

        # LOADING
        $scope.getReloadDataParams = (params) ->
            params.category = 'check-in'
            params.fromDate = $scope.filter.fromDate
            params.toDate = $scope.filter.toDate
            params.createdById = $scope.filter.createdById
            return params

        CtrlInitiatorService.initPagingFilterViewCtrl($scope)
        CtrlInitiatorService.initCanBackViewCtrl($scope)

        $scope.addInitFunction( ->
            valid = true

            if $scope.filter.createdById?
                if $scope.filter.fromDate? and $scope.filter.toDate?
                    $scope.fromDate = globalUtils.parseIsoDate($scope.filter.fromDate)
                    $scope.toDate = globalUtils.parseIsoDate($scope.filter.toDate)
                    if not checkDate() and not checkDateDuration()
                        valid = false
                        $state.go('404')
                else
                    valid = false
                    $state.go('404')
            else
                valid = false
                $state.go('404')

            if valid
                $scope.reloadData()
        )

        $scope.init()
])


.controller('CheckInTimelineCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state',
    '$stateParams', 'ADDRESS_BACKEND', '$modal'
    ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state,
        $stateParams, ADDRESS_BACKEND, $modal) ->
        $scope.title = "SET_LATER"

        $scope.defaultBackState = 'check-in-search'
        $scope.getParentFilter = ->
            if not $stateParams.parent? || $stateParams.parent is 'check-in-search'
                parentFilter =
                    viewOnMap: true
                    date: $scope.filter.date
                    createdById: $scope.filter.createdById

                return $scope.convertFilterAsString( parentFilter )
            else
                return $stateParams.parentFilter ? $stateParams.filter

        $scope.goToDetail = (record) ->
            $state.go('check-in-detail', { id: record.id, parent: 'check-in-list', filter: $stateParams.filter })

        $scope.isEmpty = -> not ($scope.records? and $scope.records.length > 0)

        $scope.isLocationUndefined = (record) -> not (record? and record.location?)
        $scope.isAddressUndefined = (record) -> not (record? and record.location? and record.province?)
        $scope.getLocationDisplay = (record) ->
            if $scope.isLocationUndefined(record)
                $filter('translate')('location.undefined')
            else if $scope.isAddressUndefined(record)
                $filter('translate')('address.undefined')
            else
                record.province.name

        $scope.isDisplayPhoto = (record) -> record? and record.photos? and record.photos.length > 0
        $scope.getPhotoLink = (photo) -> ADDRESS_BACKEND + 'image/' + photo

        $scope.getWarningCheckInIcon = (record) -> $scope.isLocationUndefined(record)
        $scope.getNormalCheckInIcon = (record) -> (not $scope.getWarningCheckInIcon(record)) and (not $scope.getCameraCheckInIcon(record))
        $scope.getCameraCheckInIcon = (record) -> (not $scope.getWarningCheckInIcon(record)) and record.photos? and record.photos.length > 0

        $scope.zoomPhoto = (photo) ->
            $modal.open(
                templateUrl: 'views/common/photo-popup.html',
                controller: 'PhotoPopupCtrl',
                windowClass: 'modal-transparent'
                resolve:
                    title: -> null
                    subTitle: -> null
                    photoLink: -> $scope.getPhotoLink(photo)
                backdrop: true
            )

        $scope.getCheckInDetailLink = (checkIn) ->
            return '#/check-in-detail?' +
                    'id=' + checkIn.id + '&' +
                    'parent=check-in-timeline&' +
                    'filter=' + $scope.getFilterAsString()

        #MAP
        showInfoWindow = (latLng) ->
            if $scope.markers?
                for marker, i in $scope.markers
                    if marker.getPosition().lat()is latLng.lat() and marker.getPosition().lng() is latLng.lng()
                        checkIn = $scope.recordsHasMarker[i]

                        content = '<strong> ' + $filter('isoTime')(checkIn.createdTime) + ' </strong><br/>'
                        content = content +
                                '<a class="underline-link" href="' +
                                $scope.getCheckInDetailLink(checkIn) +
                                '">' +
                                $filter('translate')('detail') +
                                '</a>'

                        $scope.infowindow.setContent(content)
                        $scope.infowindow.open($scope.map, marker.getPosition())
                        return

            $scope.infowindow.close()

        staticOnMapReady($scope, (map) ->
            $scope.map = map
            addMarkerToMap()
        )

        addMarkerToMap = ->
            if $scope.map?
                if $scope.markers? and $scope.markers.length > 0
                    bounds = new MyLatLngBounds()

                    for marker in $scope.markers
                        staticAddMarkerToMap($scope.map, marker)
                        bounds.extend(marker.getPosition())

                    $scope.map.fitBounds(bounds)
                else
                    userInfo = CtrlUtilsService.getUserInfo()
                    latLng = new MyLatLng(userInfo.location.latitude, userInfo.location.longitude)
                    $scope.map.setCenter(latLng)
                    $scope.map.setZoom(CtrlUtilsService.getDefaultZoom())

        # LOADING
        $scope.getReloadDataParams = (params) ->
            params.category = 'check-in'
            params.subCategory = 'by-date'
            params.date = $scope.filter.date
            params.createdById = $scope.filter.createdById
            return params

        $scope.reloadData = ->
            params =
                who: $scope.who
                category: 'check-in'
                subCategory: 'by-date'
                date: $scope.filter.date
                createdById: $scope.filter.createdById

            $scope.records = []
            $scope.recordsHasMarker = []
            $scope.markers = []
            $scope.infowindow = new MyInfoWindow()

            CtrlUtilsService.loadListData(params
                $scope.loadStatus.getStatusByDataName('data')
                (list) ->
                    $scope.records = list

                    #MAP
                    for record in $scope.records
                        if record.location?
                            latLng = new MyLatLng(record.location.latitude, record.location.longitude)
                            marker = new MyMarker($filter('isoTime')(record.createdTime))
                            marker.setPosition(latLng)
                            marker.setDraggable(false)
                            marker.addClickListener((latLng) -> showInfoWindow(latLng))

                            $scope.markers.push(marker)
                            $scope.recordsHasMarker.push(record)

                    addMarkerToMap()
                (error) ->
                    if (error.status is 400)
                        $state.go('404')
                    else
                        toast.logError($filter('translate')('loading.error'))
            )

        CtrlInitiatorService.initFilterViewCtrl($scope)
        CtrlInitiatorService.initCanBackViewCtrl($scope)

        $scope.addInitFunction( ->
            valid = true

            if $scope.filter.createdById?
                if $scope.filter.date?
                    $scope.date = globalUtils.parseIsoDate($scope.filter.date)
                    if not $scope.date?
                        valid = false
                        $state.go('404')
                else
                    valid = false
                    $state.go('404')
            else
                valid = false
                $state.go('404')

            if valid
                $scope.reloadData()
                $scope.title = $scope.filter.createdByName + ' - ' + $filter('isoDate')($scope.filter.date)
        )

        $scope.init()
])


.controller('CheckInDetailCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
    '$filter', 'toast', '$state', '$stateParams', 'ADDRESS_BACKEND', '$modal'
    ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state, $stateParams, ADDRESS_BACKEND, $modal) ->
        $scope.title = 'check.in.detail.title'

        $scope.defaultBackState = 'check-in-list'

        CtrlInitiatorService.initFilterViewCtrl($scope)
        CtrlInitiatorService.initCanBackViewCtrl($scope)

        $scope.isLocationUndefined = (record) -> not (record? and record.location?)
        $scope.isAddressUndefined = (record) -> not (record? and record.location? and record.province?)
        $scope.getLocationDisplay = (record) ->
            if $scope.isLocationUndefined(record)
                $filter('translate')('location.undefined')
            else if $scope.isAddressUndefined(record)
                $filter('translate')('address.undefined')
            else
                record.province.name

        $scope.isDisplayMap = -> $scope.record? and $scope.record.location?

        $scope.isDisplayPhoto = -> $scope.record? and $scope.record.photos? and $scope.record.photos.length > 0
        $scope.getPhotoLink = (photo) -> ADDRESS_BACKEND + 'image/' + photo

        #MAP
        staticOnMapReady($scope, (map) ->
            $scope.map = map
            addMarkerToMap()
        )

        addMarkerToMap = ->
            if $scope.map?
                if $scope.marker?
                    staticAddMarkerToMap($scope.map, $scope.marker)

                    $scope.map.panTo($scope.marker.getPosition())
                    $scope.map.setZoom(CtrlUtilsService.getDefaultZoom() + 2)
                else
                    latLng = new MyLatLng(CtrlUtilsService.getUserInfo().location.latitude, CtrlUtilsService.getUserInfo().location.longitude)
                    $scope.map.setCenter(latLng)
                    $scope.map.setZoom(CtrlUtilsService.getDefaultZoom())

        $scope.zoomPhoto = (photo) ->
            $modal.open(
                templateUrl: 'views/common/photo-popup.html',
                controller: 'PhotoPopupCtrl',
                windowClass: 'modal-transparent'
                resolve:
                    title: -> null
                    subTitle: -> null
                    photoLink: -> $scope.getPhotoLink(photo)
                backdrop: true
            )

        # LOADING
        $scope.reloadData = ->
            if $scope.who isnt 'advance_supervisor'
                params =
                    who: $scope.who
                    category: 'check-in'
                    id: $stateParams.id
            else
                params =
                    who: $scope.who
                    category: 'check-in'
                    subcategory : 'item'
                    id: $stateParams.id


            CtrlUtilsService.loadSingleData(params
                $scope.loadStatus.getStatusByDataName('data')
                (record) ->
                    $scope.record = record

                    #MAP
                    if $scope.record.location?
                        latLng = new MyLatLng($scope.record.location.latitude, $scope.record.location.longitude)
                        $scope.marker = new MyMarker($filter('translate')('check.in.location'))
                        $scope.marker.setPosition(latLng)
                        $scope.marker.setDraggable(false)

                    addMarkerToMap()
                (error) ->
                    if (error.status is 400)
                        $state.go('404')
                    else
                        toast.logError($filter('translate')('loading.error'))
            )

        $scope.addInitFunction( ->
            if not $stateParams.id?
                $state.go('404')

            $scope.reloadData()
        )

        $scope.init()
])
