'use strict'

angular.module('app')

.controller('DashboardCtrl', [
    '$scope', '$state', '$stateParams', 'toast', '$filter', 'CtrlUtilsService', 'CtrlInitiatorService','ADDRESS_BACKEND','Authentication'
    ($scope, $state, $stateParams, toast, $filter, CtrlUtilsService, CtrlInitiatorService,ADDRESS_BACKEND,Authentication) ->

        CtrlInitiatorService.initBasicViewCtrl($scope)

        $scope.goToVisitToday = -> $state.go($scope.visitTodayPage, { filter: $stateParams.filter })

        $scope.getComparisonIcon = (before, current) ->
            before ?= 0
            current ?= 0

            if (before < current)
                return 'fa-arrow-circle-up'
            else if (before > current)
                return 'fa-arrow-circle-down'
            else if (before is current)
                return 'fa-arrow-circle-right'

        $scope.isDisplayProgressWarning = ->
            $scope.record? and $scope.record.progressWarnings? and $scope.record.progressWarnings.length > 0

        $scope.isDisplayBestSellers = ->
            $scope.record? and $scope.record.bestSellers? and $scope.record.bestSellers.length > 0

        $scope.isDisplayRegionInfo = -> $scope.who is 'advance_supervisor'

        $scope.exportOrderNotInScheduleToday = ->
            if $scope.record.todayResult.orderNoVisit.actual? and $scope.record.todayResult.orderNoVisit.actual>0
                href = ADDRESS_BACKEND + $scope.who + '/export/not-in-schedule-today'
                href = href + '?access_token=' + Authentication.getAccessToken()
                href = href + '&lang=' + CtrlUtilsService.getUserLanguage()
                location.href = href
            else
                toast.log($filter('translate')('data.empty'))
            return true

        $scope.exportOrderInScheduleToday = ->
            if $scope.record.todayResult.visitHasOrder.actual? and $scope.record.todayResult.visitHasOrder.actual>0
                href = ADDRESS_BACKEND + $scope.who + '/export/in-schedule-today'
                href = href + '?access_token=' + Authentication.getAccessToken()
                href = href + '&lang=' + CtrlUtilsService.getUserLanguage()
                location.href = href
            else
                toast.log($filter('translate')('data.empty'))
            return true

        $scope.exportVisitWrongDurationToday = ->
            if $scope.record.todayResult.visitErrorDuration.actual? and $scope.record.todayResult.visitErrorDuration.actual>0
                href = ADDRESS_BACKEND + $scope.who + '/export/visit/wrong-duration'
                href = href + '?access_token=' + Authentication.getAccessToken()
                href = href + '&lang=' + CtrlUtilsService.getUserLanguage()
                location.href = href
            else
                toast.log($filter('translate')('data.empty'))
            return true

        $scope.exportVisitWrongPositionToday = ->
            if $scope.record.todayResult.visitErrorPosition.actual? and $scope.record.todayResult.visitErrorPosition.actual>0
                href = ADDRESS_BACKEND + $scope.who + '/export/visit/wrong-position'
                href = href + '?access_token=' + Authentication.getAccessToken()
                href = href + '&lang=' + CtrlUtilsService.getUserLanguage()
                location.href = href
            else
                toast.log($filter('translate')('data.empty'))
            return true


        $scope.reloadData = ->
            status = $scope.loadStatus.getStatusByDataName('data')

            $scope.visitTodayPage = "visit-today"

            params =
                who: $scope.who
                category: 'dashboard'

            callbackSuccess = (data) ->
                $scope.record = data

            callbackFailure = ->
                toast.logError($filter('translate')('loading.error'))

            CtrlUtilsService.loadSingleData(params, status, callbackSuccess, callbackFailure)

        $scope.addInitFunction( -> $scope.reloadData())

        $scope.init()
])
