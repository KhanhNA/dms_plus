'use strict'

angular.module('app')

.controller('CheckInExportCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state', '$modal',
    'ADDRESS_BACKEND', 'Authentication'
    ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $modal,
        ADDRESS_BACKEND, Authentication) ->
        $scope.title = 'check.in.export.title'

        # FILTER
        $scope.export = ->
            if not $scope.filter.createdById?
                toast.logError($filter('translate')('please.select.created.by'))
            else if not checkDate()
                toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
            else if not checkDateDuration()
                toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
            else
                href = ADDRESS_BACKEND + $scope.who + '/export/check-in'

                href = href + '?access_token=' + Authentication.getAccessToken()
                if $scope.filter.createdById? and $scope.filter.createdById isnt 'all'
                    href = href + '&createdById=' +  $scope.filter.createdById
                href = href + '&fromDate=' +  globalUtils.createIsoDate($scope.fromDate.date)
                href = href + '&toDate=' +  globalUtils.createIsoDate($scope.toDate.date)
                href = href + '&lang=' + CtrlUtilsService.getUserLanguage()

                location.href = href

        checkDateDuration = ->
            if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
                return false
            return true

        checkDate = ->
            if $scope.fromDate? and  $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
                if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
                    return true
            return false

        CtrlInitiatorService.initFilterViewCtrl($scope)
        CtrlInitiatorService.initUseDatePickerCtrl($scope)

        $scope.addInitFunction( ->
            if $scope.filter.fromDate? and $scope.filter.toDate?
                $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
                $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
                if not checkDate() or not checkDateDuration()
                    $state.go('404')
            else
                $scope.fromDate = $scope.createDatePickerModel(new Date())
                $scope.toDate = $scope.createDatePickerModel(new Date())


            $scope.createdByList = [{ id: 'all', fullname: '-- ' + $filter('translate')('all') + ' --'  }]

            LoadingUtilsService.loadSupervisors(
                $scope.who
                $scope.loadStatus.getStatusByDataName('supervisors')
                (list) ->
                    $scope.createdByList = _.sortBy(_.union($scope.createdByList, list), (o) ->  return o.fullname)
            )
            if  $scope.who is 'admin'
                LoadingUtilsService.loadAdvanceSupervisors(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('advanceSupervisors')
                    (list) ->
                        $scope.createdByList = _.sortBy(_.union($scope.createdByList, list), (o) ->  return o.fullname)
                )
        )

        $scope.init()
])
