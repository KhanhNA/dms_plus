'use strict'

angular.module('app')

.controller('CustomerExportCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state',
        '$modal', 'ADDRESS_BACKEND', 'Authentication'
        ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $modal,
         ADDRESS_BACKEND, Authentication) ->
            $scope.title = 'customer'

            $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'
            $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()
            $scope.isDisplayDate = -> true
            $scope.isDisplayRegion = -> $scope.isUseRegionFilter()
            $scope.isDisplayDistributor = -> $scope.isUseDistributorFilter() # or ($scope.isUseRegionFilter() and not $scope.filter.regionId?)

            # FILTER
            $scope.export = ->
                if $scope.isDisplayDistributor() and not $scope.filter.distributorId?
                    toast.logError($filter('translate')('please.select.distributor'))
#                else if not checkDate()
#                    toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
#                else if not checkDateDuration()
#                    toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
                else
                    href = ADDRESS_BACKEND + $scope.who + '/export/customer'
                    href = href + '?access_token=' + Authentication.getAccessToken()
                    if $scope.filter.distributorId?
                        href = href + '&distributorId=' + $scope.filter.distributorId
                    if $scope.filter.search?
                        href = href + '&search=' + $scope.filter.search
#                    href = href + '&fromDate=' + globalUtils.createIsoDate($scope.fromDate.date)
#                    href = href + '&toDate=' + globalUtils.createIsoDate($scope.toDate.date)
                    href = href + '&lang=' + CtrlUtilsService.getUserLanguage()
                    location.href = href

            CtrlInitiatorService.initFilterViewCtrl($scope)
            CtrlInitiatorService.initUseDatePickerCtrl($scope)

#            checkDateDuration = ->
#                if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
#                    return false
#                return true
#
#            checkDate = ->
#                if $scope.fromDate? and $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
#                    if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
#                        return true
#                return false

            $scope.changeRegion = ->
                LoadingUtilsService.loadDistributorByRegion(
                    $scope.who
                    $scope.filter.regionId
                    $scope.loadStatus.getStatusByDataName('distributor')
                    (list) ->
                        $scope.distributors = _.union(
                            [
                                {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )
                )

            $scope.addInitFunction(->
                $scope.filter.search = null
                $scope.filter.distributorId = null
#                if $scope.filter.fromDate? and $scope.filter.toDate?
#                    $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
#                    $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
#                    if not checkDate() or not checkDateDuration()
#                        $state.go('404')
#                else
#                    $scope.fromDate = $scope.createDatePickerModel(new Date())
#                    $scope.toDate = $scope.createDatePickerModel(new Date())
                if $scope.isUseRegionFilter()
                    LoadingUtilsService.loadAccessibleRegions(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('regions')
                        (list) ->
                            $scope.regions = list
                            if not $scope.filter.regionId? and $scope.regions.length > 0
                                $scope.filter.regionId = $scope.regions[0].id
                                $scope.changeRegion()
                    )
                else if $scope.isUseDistributorFilter()
                    LoadingUtilsService.loadDistributors(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('distributors')
                        (list) ->
                            $scope.distributors = _.union(
                                [
                                    {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                                ]
                                list
                            )
                    )
            )

            $scope.init()
    ])
