'use strict'

angular.module('app')

.controller('FeedbackExportCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state',
        '$modal',
        'ADDRESS_BACKEND', 'Authentication'
        ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $modal,
         ADDRESS_BACKEND, Authentication) ->
            $scope.title = 'feedback.export.title'
            $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'
            $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()

            # FILTER
            $scope.export = ->
                if not checkDate()
                    toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
                else if not checkDateDuration()
                    toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
                else
                    href = ADDRESS_BACKEND + $scope.who + '/export/feedback'

                    href = href + '?access_token=' + Authentication.getAccessToken()
                    href = href + '&lang=' + CtrlUtilsService.getUserLanguage()
                    href = href + '&fromDate=' + globalUtils.createIsoDate($scope.fromDate.date)
                    href = href + '&toDate=' + globalUtils.createIsoDate($scope.toDate.date)
                    if $scope.filter.distributorId? and $scope.filter.distributorId isnt 'all'
                        href = href + '&distributorId=' + $scope.filter.distributorId

                    location.href = href

            checkDateDuration = ->
                if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
                    return false
                return true

            checkDate = ->
                if $scope.fromDate? and $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
                    if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
                        return true
                return false

            $scope.isDisplayDate = -> true
            $scope.isDisplayDistributor = -> $scope.isUseDistributorFilter()

            CtrlInitiatorService.initFilterViewCtrl($scope)
            CtrlInitiatorService.initUseDatePickerCtrl($scope)

            $scope.regionFilterChange = ->
                LoadingUtilsService.loadDistributorByRegion(
                    $scope.who
                    $scope.filter.regionId
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = _.union(
                            [
                                {id: 'all_' + $scope.filter.regionId, name: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )

                        if $scope.distributors.length > 0
                            $scope.filter.distributorId = $scope.distributors[0].id
                )

            $scope.addInitFunction(->
                if $scope.filter.fromDate? and $scope.filter.toDate?
                    $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
                    $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
                    if not checkDate() or not checkDateDuration()
                        $state.go('404')
                else
                    $scope.fromDate = $scope.createDatePickerModel(new Date())
                    $scope.toDate = $scope.createDatePickerModel(new Date())

                if not $scope.filter.salesmanId?
                    $scope.filter.distributorId = 'all'

                if $scope.isUseRegionFilter()
                    LoadingUtilsService.loadAccessibleRegions(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('regions')
                        (list) ->
                            $scope.regions = list
                            if not $scope.filter.regionId? and $scope.regions.length > 0
                                $scope.filter.regionId = $scope.regions[0].id
                            $scope.regionFilterChange()
                    )
                else if $scope.isUseDistributorFilter()
                    LoadingUtilsService.loadDistributors(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('distributors')
                        (list) ->
                            $scope.distributors = _.union(
                                [
                                    {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                                ]
                                list
                            )
                    )
            )

            $scope.init()
    ])
