'use strict'

angular.module('app')

.controller('OrderExportCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state',
        '$modal',
        'ADDRESS_BACKEND', 'Authentication'
        ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $modal,
         ADDRESS_BACKEND, Authentication) ->
            $scope.title = 'order.export.title'
            $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'
            $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()

            # FILTER
            $scope.export = ->
                if $scope.isDisplayRegion() and not $scope.filter.regionId?
                    toast.logError($filter('translate')('please.select.region'))
                else if $scope.isDisplayDistributor() and not $scope.filter.distributorId?
                    toast.logError($filter('translate')('please.select.distributor'))
                else if not checkDate()
                    toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
                else if not checkDateDuration()
                    toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
                else
                    href = ADDRESS_BACKEND + $scope.who + '/export/order'
                    if $scope.filter.isDetail
                        href = href + '/detail'

                    href = href + '?access_token=' + Authentication.getAccessToken()
                    href = href + '&distributorId=' + $scope.filter.distributorId
                    href = href + '&fromDate=' + globalUtils.createIsoDate($scope.fromDate.date)
                    href = href + '&toDate=' + globalUtils.createIsoDate($scope.toDate.date)
                    href = href + '&lang=' + CtrlUtilsService.getUserLanguage()

                    if $scope.filter.salesmanId? and $scope.filter.salesmanId isnt 'all'
                        href = href + '&salesmanId=' + $scope.filter.salesmanId

                    if $scope.filter.customerId?
                        href = href + '&customerId=' + $scope.filter.customerId

                    location.href = href

            checkDateDuration = ->
                if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
                    return false
                return true

            checkDate = ->
                if $scope.fromDate? and $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
                    if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
                        return true
                return false

            $scope.isDisplayDate = -> true
            $scope.isDisplayRegion = -> $scope.isUseRegionFilter()
            $scope.isDisplayDistributor = -> $scope.isUseDistributorFilter()  and  (not $scope.isUseRegionFilter() or $scope.filter.regionId?)
            $scope.isDisplaySalesman = -> $scope.filter.distributorId?
            $scope.isDisplayCustomer = -> $scope.filter.distributorId?


            $scope.changeRegion = ->
                LoadingUtilsService.loadDistributorByRegion(
                    $scope.who
                    $scope.filter.regionId
                    $scope.loadStatus.getStatusByDataName('distributor')
                    (list) ->
                        $scope.distributors = _.union(
                            [
                                {id: 'all_' + $scope.filter.regionId, name: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )
                        if $scope.distributors.length > 0
                            $scope.filter.distributorId = $scope.distributors[0].id

                        $scope.changeDistributor()
                )

            $scope.changeDistributor = ->
                $scope.filter.salesmanId = 'all'
                loadSalesmen()
                $scope.clearCustomer()

            loadSalesmen = ->
                LoadingUtilsService.loadSalesmenByDistributor(
                    $scope.who
                    $scope.filter.distributorId
                    $scope.loadStatus.getStatusByDataName('salesmen')
                    (list) ->
                        $scope.salesmen = _.union(
                            [
                                {id: 'all', fullname: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )
                )

            $scope.openSelectCustomer = ->
                modalInstance = $modal.open(
                    templateUrl: 'views/business/customer/customer-select-popup.html',
                    controller: 'CustomerSelectPopupCtrl',
                    resolve:
                        distributorId: -> $scope.filter.distributorId
                        distributorName: -> _.find($scope.distributors, (o) -> o.id is $scope.filter.distributorId).name
                    backdrop: true
                )
                modalInstance.result.then((customer) ->
                    $scope.filter.customerId = customer.id
                    $scope.filter.customerName = customer.name
                )

            $scope.clearCustomer = ->
                $scope.filter.customerId = null
                $scope.filter.customerName = ('-- ' + $filter('translate')('all') + ' --')

            CtrlInitiatorService.initFilterViewCtrl($scope)
            CtrlInitiatorService.initUseDatePickerCtrl($scope)

            $scope.addInitFunction(->
                if not $scope.filter.isDetail?
                    $scope.filter.isDetail = false

                if $scope.filter.fromDate? and $scope.filter.toDate?
                    $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
                    $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
                    if not checkDate() or not checkDateDuration()
                        $state.go('404')
                else
                    $scope.fromDate = $scope.createDatePickerModel(new Date())
                    $scope.toDate = $scope.createDatePickerModel(new Date())

                if not $scope.filter.salesmanId?
                    $scope.filter.salesmanId = 'all'

                if not $scope.filter.customerId?
                    $scope.filter.customerName = ('-- ' + $filter('translate')('all') + ' --')

                if $scope.isUseRegionFilter()
                    LoadingUtilsService.loadAccessibleRegions(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('regions')
                        (list) ->
                            $scope.regions = _.union(
                                [
                                    {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                                ]
                                list
                            )

                    )
                else if $scope.isUseDistributorFilter()
                    LoadingUtilsService.loadDistributors(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('distributors')
                        (list) ->
                            $scope.distributors = _.union(
                                [
                                    {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                                ]
                                list
                            )
                    )
            )

            $scope.init()
    ])
