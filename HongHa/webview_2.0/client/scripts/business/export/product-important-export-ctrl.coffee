'use strict'

angular.module('app')

.controller('ProductImportantExportCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state',
        '$modal',
        'ADDRESS_BACKEND', 'Authentication'
        ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $modal,
         ADDRESS_BACKEND, Authentication) ->
            $scope.title = 'product.important'

            # FILTER
            $scope.export = ->
#                if not $scope.filter.categoryId?
#                    toast.logError($filter('translate')('please.select.category'))
                href = ADDRESS_BACKEND + $scope.who + '/export/product-important'

                href = href + '?access_token=' + Authentication.getAccessToken()

                if $scope.filter.categoryId?
                    href = href + '&categoryId=' + $scope.filter.categoryId

                href = href + '&lang=' + CtrlUtilsService.getUserLanguage()

                location.href = href

            CtrlInitiatorService.initFilterViewCtrl($scope)

            $scope.addInitFunction(->
                $scope.filter.categoryId = null

                LoadingUtilsService.loadProductCategories(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('categories')
                    (list)-> $scope.categories = list
                );
            )

            $scope.init()
    ])
