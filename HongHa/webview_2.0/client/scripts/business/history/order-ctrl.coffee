'use strict'

angular.module('app')

.controller('OrderSearchCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state', '$modal'
    ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $modal) ->
        $scope.title = 'order'
        $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'
        $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()
        $scope.mustSelectDistributor  = -> $scope.isUseDistributorFilter() and not $scope.filter.distributorId?

        # FILTER
        $scope.search = ->
            if $scope.filter.searchByCode
                if not $scope.filter.code?
                    toast.logError($filter('translate')('error.data.input.not.valid'))
                else
                    params =
                        who: $scope.who
                        category: 'order'
                        subCategory: 'by-code'
                        code: $scope.filter.code

                    CtrlUtilsService.loadSingleData(
                        params
                        $scope.loadStatus.getStatusByDataName('distributor')
                        (record) ->
                            if record? and record.id?
                                $state.go('order-detail', { id: record.id, parent: 'order-search' })
                            else
                                message = $filter('translate')('order') + ' ' + $scope.filter.code + ' ' + $filter('translate')('not.found')
                                toast.logError(message)
                        ->
                            $state.go('500')
                    )
            else
                if $scope.isDisplayDistributor() and not $scope.filter.distributorId?
                    toast.logError($filter('translate')('please.select.distributor'))
                else if not checkDate()
                    toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
                else if not checkDateDuration()
                    toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
                else
                    distributor = _.find($scope.distributors, (o) -> o.id is $scope.filter.distributorId)

                    filter =
                        fromDate: globalUtils.createIsoDate($scope.fromDate.date)
                        toDate: globalUtils.createIsoDate($scope.toDate.date)
                        regionId: $scope.filter.regionId
                        distributorId: $scope.filter.distributorId
                        distributorName: if distributor? then distributor.name else null

                    if $scope.filter.salesmanId? and $scope.filter.salesmanId isnt 'all'
                        filter.salesmanId = $scope.filter.salesmanId
                        filter.salesmanName = _.find($scope.salesmen, (o) -> o.id is $scope.filter.salesmanId).fullname

                    if $scope.filter.customerId?
                        filter.customerId = $scope.filter.customerId
                        filter.customerName = $scope.filter.customerName

                    $state.go('order-list', { filter: $scope.convertFilterAsString(filter) })

        checkDateDuration = ->
            if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
                return false
            return true

        checkDate = ->
            if $scope.fromDate? and  $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
                if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
                    return true
            return false

        $scope.isDisplayCode        = -> $scope.filter.searchByCode
        $scope.isDisplayDate        = -> not $scope.filter.searchByCode
        $scope.isDisplayDistributor = -> $scope.isUseDistributorFilter() and not $scope.filter.searchByCode
        $scope.isDisplaySalesman    = -> (not $scope.isUseDistributorFilter() or $scope.filter.distributorId?) and not $scope.filter.searchByCode
        $scope.isDisplayCustomer    = -> (not $scope.isUseDistributorFilter() or $scope.filter.distributorId?) and not $scope.filter.searchByCode

        $scope.regionFilterChange = ->
            LoadingUtilsService.loadDistributorByRegion(
                $scope.who
                $scope.filter.regionId
                $scope.loadStatus.getStatusByDataName('distributors')
                (list) ->
                    $scope.distributors = list
                    if $scope.distributors.length > 0
                        $scope.filter.distributorId = $scope.distributors[0].id
                        $scope.changeDistributor()
            )

        $scope.changeDistributor = ->
            $scope.filter.salesmanId = 'all'
            loadSalesmen()
            $scope.clearCustomer()

        loadSalesmen = ->
            LoadingUtilsService.loadSalesmenByDistributor(
                $scope.who
                $scope.filter.distributorId
                $scope.loadStatus.getStatusByDataName('salesmen')
                (list) ->
                    $scope.salesmen = _.union(
                        [
                            { id: 'all', fullname: '-- ' + $filter('translate')('all') + ' --' }
                        ]
                        list
                    )
            )

        $scope.openSelectCustomer = ->
            modalInstance = $modal.open(
                templateUrl: 'views/business/customer/customer-select-popup.html',
                controller: 'CustomerSelectPopupCtrl',
                resolve:
                    distributorId:   -> if $scope.isUseDistributorFilter() then $scope.filter.distributorId else null
                    distributorName: -> if $scope.isUseDistributorFilter() then _.find($scope.distributors, (o) -> o.id is $scope.filter.distributorId).name else null
                backdrop: true
            )
            modalInstance.result.then( (customer) ->
                $scope.filter.customerId = customer.id
                $scope.filter.customerName = customer.name
            )

        $scope.clearCustomer = ->
            $scope.filter.customerId = null
            $scope.filter.customerName = ('-- ' + $filter('translate')('all') + ' --')

        CtrlInitiatorService.initFilterViewCtrl($scope)
        CtrlInitiatorService.initUseDatePickerCtrl($scope)

        $scope.addInitFunction( ->
            if not $scope.filter.searchByCode?
                $scope.filter.searchByCode = false

            if $scope.filter.fromDate? and $scope.filter.toDate?
                $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
                $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
                if not checkDate() or not checkDateDuration()
                    $state.go('404')
            else
                $scope.fromDate = $scope.createDatePickerModel(new Date())
                $scope.toDate = $scope.createDatePickerModel(new Date())

            if not $scope.filter.salesmanId?
                $scope.filter.salesmanId = 'all'

            if not $scope.filter.customerId?
                $scope.filter.customerName = ('-- ' + $filter('translate')('all') + ' --')

            if $scope.isUseRegionFilter()
                LoadingUtilsService.loadAccessibleRegions(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('regions')
                    (list) ->
                        $scope.regions = list
                        if not $scope.filter.regionId? and $scope.regions.length > 0
                            $scope.filter.regionId = $scope.regions[0].id
                            $scope.regionFilterChange()
                )
            else if $scope.isUseDistributorFilter()
                LoadingUtilsService.loadDistributors(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = list
                        if not $scope.filter.distributorId? and $scope.distributors.length > 0
                            $scope.filter.distributorId = $scope.distributors[0].id
                            $scope.changeDistributor()
                )

            if not $scope.mustSelectDistributor()
                if $scope.isUseRegionFilter()
                    LoadingUtilsService.loadDistributorByRegion(
                        $scope.who
                        $scope.filter.regionId
                        $scope.loadStatus.getStatusByDataName('distributors')
                        (list) ->
                            $scope.distributors = list
                    )
                LoadingUtilsService.loadSalesmenByDistributor(
                    $scope.who
                    $scope.filter.distributorId
                    $scope.loadStatus.getStatusByDataName('salesmen')
                    (list) ->
                        $scope.salesmen = list
                )

            if $scope.isDisplaySalesman()
                loadSalesmen()
        )

        $scope.init()
])


.controller('OrderListCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state', '$stateParams'
    ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $stateParams) ->
        $scope.title = 'order'
        $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'

        $scope.defaultBackState = 'order-search'
        $scope.getParentFilter = ->
            if not $stateParams.parent? || $stateParams.parent is 'order-search'
                parentFilter =
                    searchByCode: false
                    fromDate: $scope.filter.fromDate
                    toDate: $scope.filter.toDate
                    regionId: $scope.filter.regionId
                    distributorId: $scope.filter.distributorId
                    salesmanId: $scope.filter.salesmanId
                    customerId: $scope.filter.customerId
                    customerName: $scope.filter.customerName

                return $scope.convertFilterAsString( parentFilter )
            else
                return $stateParams.parentFilter ? $stateParams.filter

        $scope.goToDetail = (record) -> $state.go('order-detail', { id: record.id, parent: 'order-list', filter: $stateParams.filter })

        checkDateDuration = ->
            if moment($scope.fromDate).add(1, 'months').toDate().getTime() < $scope.toDate.getTime()
                return false
            return true

        checkDate = ->
            if $scope.fromDate? and $scope.toDate?
                if $scope.fromDate.getTime() <= $scope.toDate.getTime()
                    return true
            return false

        $scope.isDisplaySalesman = -> $scope.filter.salesmanId? and $scope.filter.salesmanId isnt 'all'
        $scope.isDisplayCustomer = -> $scope.filter.customerId?

        # LOADING
        $scope.getReloadDataParams = (params) ->
            params.category = 'order'
            params.fromDate = $scope.filter.fromDate
            params.toDate = $scope.filter.toDate

            if $scope.isUseDistributorFilter()
                params.distributorId = $scope.filter.distributorId

            if $scope.isDisplaySalesman()
                params.salesmanId = $scope.filter.salesmanId

            if $scope.isDisplayCustomer()
                params.customerId = $scope.filter.customerId

            return params

        CtrlInitiatorService.initPagingFilterViewCtrl($scope)
        CtrlInitiatorService.initCanBackViewCtrl($scope)

        $scope.addInitFunction( ->
            valid = true

            if $scope.filter.fromDate? and $scope.filter.toDate?
                $scope.fromDate = globalUtils.parseIsoDate($scope.filter.fromDate)
                $scope.toDate = globalUtils.parseIsoDate($scope.filter.toDate)
                if not checkDate() and not checkDateDuration()
                    valid = false
                    $state.go('404')
            else
                valid = false
                $state.go('404')

            if valid
                $scope.reloadData()
        )

        $scope.init()
])


.controller('OrderDetailCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
    '$filter', 'toast', '$state', '$stateParams'
    ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state, $stateParams) ->
        $scope.title = 'order.approval.title'

        $scope.defaultBackState = 'order-search'
        $scope.getParentFilter = ->
            if not $stateParams.parent? || $stateParams.parent is 'order-search'
                return $scope.convertFilterAsString( { searchByCode: true, code: $scope.record.code } )
            else
                return $stateParams.parentFilter ? $stateParams.filter

        CtrlInitiatorService.initFilterViewCtrl($scope)
        CtrlInitiatorService.initCanBackViewCtrl($scope)

        $scope.printOrder = ->  CtrlUtilsService.printOrder('order')

        # LOADING
        $scope.reloadData = ->
            params =
                who: $scope.who
                category: 'order'
                id: $stateParams.id

            CtrlUtilsService.loadSingleData(params
                $scope.loadStatus.getStatusByDataName('data')
                (record) ->
                    $scope.record = record
                (error) ->
                    if (error.status is 400)
                        $state.go('404')
                    else
                        toast.logError($filter('translate')('loading.error'))
            )

        $scope.addInitFunction( ->
            if not $stateParams.id?
                $state.go('404')

            $scope.reloadData()
        )

        $scope.init()
])
