'use strict'

angular.module('app')

.controller('VisitSearchCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state', '$modal'
    ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $modal) ->
        $scope.title = 'visit'
        $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'
        $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()
        $scope.mustSelectDistributor  = -> $scope.isUseDistributorFilter() and not $scope.filter.distributorId?

        # FILTER
        $scope.search = ->
            if $scope.isDisplayDistributor() and not $scope.filter.distributorId?
                toast.logError($filter('translate')('please.select.distributor'))
            else if not checkDate()
                toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
            else if not checkDateDuration()
                toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
            else
                filter =
                    fromDate: globalUtils.createIsoDate($scope.fromDate.date)
                    toDate: globalUtils.createIsoDate($scope.toDate.date)
                    regionId: $scope.filter.regionId
                    distributorId: $scope.filter.distributorId
                    distributorName: _.find($scope.distributors, (o) -> o.id is $scope.filter.distributorId).name

                if $scope.filter.salesmanId? and $scope.filter.salesmanId isnt 'all'
                    filter.salesmanId = $scope.filter.salesmanId
                    filter.salesmanName = _.find($scope.salesmen, (o) -> o.id is $scope.filter.salesmanId).fullname

                if $scope.filter.customerId?
                    filter.customerId = $scope.filter.customerId
                    filter.customerName = $scope.filter.customerName

                $state.go('visit-list', { filter: $scope.convertFilterAsString(filter) })

        checkDateDuration = ->
            if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
                return false
            return true

        checkDate = ->
            if $scope.fromDate? and  $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
                if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
                    return true
            return false

        $scope.isDisplayDate        = -> true
        $scope.isDisplayDistributor = -> $scope.isUseDistributorFilter()
        $scope.isDisplaySalesman    = -> $scope.filter.distributorId?
        $scope.isDisplayCustomer    = -> $scope.filter.distributorId?

        $scope.regionFilterChange = ->
            LoadingUtilsService.loadDistributorByRegion(
                $scope.who
                $scope.filter.regionId
                $scope.loadStatus.getStatusByDataName('distributors')
                (list) ->
                    $scope.distributors = list
                    if $scope.distributors.length > 0
                        $scope.filter.distributorId = $scope.distributors[0].id
                        $scope.changeDistributor()
            )

        $scope.changeDistributor = ->
            $scope.filter.salesmanId = 'all'
            loadSalesmen()
            $scope.clearCustomer()

        loadSalesmen = ->
            LoadingUtilsService.loadSalesmenByDistributor(
                $scope.who
                $scope.filter.distributorId
                $scope.loadStatus.getStatusByDataName('salesmen')
                (list) ->
                    $scope.salesmen = _.union(
                        [
                            { id: 'all', fullname: '-- ' + $filter('translate')('all') + ' --' }
                        ]
                        list
                    )
            )

        $scope.openSelectCustomer = ->
            modalInstance = $modal.open(
                templateUrl: 'views/business/customer/customer-select-popup.html',
                controller: 'CustomerSelectPopupCtrl',
                resolve:
                    distributorId:   -> $scope.filter.distributorId
                    distributorName: -> _.find($scope.distributors, (o) -> o.id is $scope.filter.distributorId).name
                backdrop: true
            )
            modalInstance.result.then( (customer) ->
                $scope.filter.customerId = customer.id
                $scope.filter.customerName = customer.name
            )

        $scope.clearCustomer = ->
            $scope.filter.customerId = null
            $scope.filter.customerName = ('-- ' + $filter('translate')('all') + ' --')

        CtrlInitiatorService.initFilterViewCtrl($scope)
        CtrlInitiatorService.initUseDatePickerCtrl($scope)

        $scope.addInitFunction( ->

            console.log $scope.filter

            if $scope.filter.fromDate? and $scope.filter.toDate?
                $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
                $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
                if not checkDate() or not checkDateDuration()
                    $state.go('404')
            else
                $scope.fromDate = $scope.createDatePickerModel(new Date())
                $scope.toDate = $scope.createDatePickerModel(new Date())

            if not $scope.filter.salesmanId?
                $scope.filter.salesmanId = 'all'

            if not $scope.filter.customerId?
                $scope.filter.customerName = ('-- ' + $filter('translate')('all') + ' --')


            if $scope.isUseRegionFilter()
                LoadingUtilsService.loadAccessibleRegions(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('regions')
                    (list) ->
                        $scope.regions = list
                        if not $scope.filter.regionId? and $scope.regions.length > 0
                            $scope.filter.regionId = $scope.regions[0].id
                            $scope.regionFilterChange()
                )
            else if $scope.isUseDistributorFilter()
                LoadingUtilsService.loadDistributors(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = list
                        if not $scope.filter.distributorId? and $scope.distributors.length > 0
                            $scope.filter.distributorId = $scope.distributors[0].id
                            $scope.changeDistributor()
                )

            if not $scope.mustSelectDistributor()
                if $scope.isUseRegionFilter()
                    LoadingUtilsService.loadDistributorByRegion(
                        $scope.who
                        $scope.filter.regionId
                        $scope.loadStatus.getStatusByDataName('distributors')
                        (list) ->
                            $scope.distributors = list
                    )
                LoadingUtilsService.loadSalesmenByDistributor(
                    $scope.who
                    $scope.filter.distributorId
                    $scope.loadStatus.getStatusByDataName('salesmen')
                    (list) ->
                        $scope.salesmen = list
                )

            if $scope.isDisplaySalesman()
                loadSalesmen()
        )

        $scope.init()

])


.controller('VisitListCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state', '$stateParams'
    ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $stateParams) ->
        $scope.title = 'visit'
        $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'

        $scope.defaultBackState = 'visit-search'
        $scope.getParentFilter = ->
            if not $stateParams.parent? || $stateParams.parent is 'visit-search'
                parentFilter =
                    fromDate: $scope.filter.fromDate
                    toDate: $scope.filter.toDate
                    regionId: $scope.filter.regionId
                    distributorId: $scope.filter.distributorId
                    salesmanId: $scope.filter.salesmanId
                    customerId: $scope.filter.customerId
                    customerName: $scope.filter.customerName

                return $scope.convertFilterAsString( parentFilter )
            else
                return $stateParams.parentFilter ? $stateParams.filter

        $scope.goToDetail = (record) -> $state.go('visit-detail', { id: record.id, parent: 'visit-list', filter: $stateParams.filter })

        checkDateDuration = ->
            if moment($scope.fromDate).add(1, 'months').toDate().getTime() < $scope.toDate.getTime()
                return false
            return true

        checkDate = ->
            if $scope.fromDate? and $scope.toDate?
                if $scope.fromDate.getTime() <= $scope.toDate.getTime()
                    return true
            return false

        $scope.isDisplaySalesman = -> $scope.filter.salesmanId? and $scope.filter.salesmanId isnt 'all'
        $scope.isDisplayCustomer = -> $scope.filter.customerId?

        $scope.getDurationDisplay = (duration) ->
            if duration?
                minute = parseInt(duration / 60000)
                second = parseInt((duration / 1000) % 60)
                minute + 'm ' + second + 's'
            else ''

        $scope.getStartEndDisplay = (record) ->
            if record? and record.startTime? and record.endTime?
                $filter('isoTime')(record.startTime) + ' - ' + $filter('isoTime')(record.endTime)
            else ''

        $scope.isDistanceNormal = (record) -> record.locationStatus? and record.locationStatus is 0
        $scope.isDistanceTooFar = (record) -> record.locationStatus? and record.locationStatus is 1
        $scope.isLocationSalesmanUndefined = (record) -> record.locationStatus? and record.locationStatus is 2
        $scope.isLocationCustomerUndefined = (record) -> record.locationStatus? and record.locationStatus is 3
        $scope.getDistanceDisplay = (distance) -> if distance? then $filter('number')(distance * 1000, 0) + ' m' else ''

        $scope.isNoOrder = (record) -> not (record.hasOrder? and record.hasOrder)
        $scope.isOrderApproved = (record) -> not $scope.isNoOrder(record) and record.approveStatus is 1
        $scope.isOrderPending = (record) -> not $scope.isNoOrder(record) and record.approveStatus is 0
        $scope.isOrderRejected = (record) -> not $scope.isNoOrder(record) and record.approveStatus is 2

        # LOADING
        $scope.getReloadDataParams = (params) ->
            params.category = 'visit'
            params.fromDate = $scope.filter.fromDate
            params.toDate = $scope.filter.toDate

            if $scope.isUseDistributorFilter()
                params.distributorId = $scope.filter.distributorId

            if $scope.isDisplaySalesman()
                params.salesmanId = $scope.filter.salesmanId

            if $scope.isDisplayCustomer()
                params.customerId = $scope.filter.customerId

            return params

        CtrlInitiatorService.initPagingFilterViewCtrl($scope)
        CtrlInitiatorService.initCanBackViewCtrl($scope)

        $scope.addInitFunction( ->
            valid = true

            if $scope.filter.fromDate? and $scope.filter.toDate?
                $scope.fromDate = globalUtils.parseIsoDate($scope.filter.fromDate)
                $scope.toDate = globalUtils.parseIsoDate($scope.filter.toDate)
                if not checkDate() and not checkDateDuration()
                    valid = false
                    $state.go('404')
            else
                valid = false
                $state.go('404')

            if valid
                $scope.reloadData()
        )

        $scope.init()
])
