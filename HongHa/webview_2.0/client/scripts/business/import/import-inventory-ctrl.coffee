'use strict'

angular.module('app')

.controller('ImportInventoryCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
        '$filter', 'toast', '$state', '$stateParams', 'ADDRESS_BACKEND', 'Authentication'
        ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state, $stateParams, ADDRESS_BACKEND, Authentication) ->
            $scope.title = 'import.inventory.update.title'
            $scope.defaultBackState = 'inventory'

            initStep = ->
                $scope.currentStep = 0
                $scope.steps = [
                    { name: $filter('translate')('import.step.select.time') + ' & ' + $filter('translate')('import.step.upload.file') }
                    { name: 'import.step.confirm' }
                    { name: 'import.step.finalisation' }
                ]

            $scope.isDoneStep = (stepIndex) -> stepIndex < $scope.currentStep
            $scope.isCurrentStep = (stepIndex) -> $scope.currentStep is stepIndex

            $scope.canNext = ->
                $scope.currentStep? and $scope.currentStep >= 0 and not $scope.isFinish()
            $scope.isNextDisabled = ->
                if $scope.canNext()
                    if $scope.currentStep is 0
                        return not $scope.getIsoTime()? and not $scope.global.excel?
                    else if $scope.currentStep is 1
                        return $scope.global.confirm.rowDatas.length > 0
                return true
            $scope.nextStep = ->
                if $scope.canNext()
                    if $scope.currentStep is 0
                        verify()
                    else if $scope.currentStep is 1
                        doImport()

            $scope.canBack = ->
                $scope.currentStep? and $scope.currentStep >= 0 and not $scope.isFinish()
            $scope.backStep = () ->
                if $scope.currentStep is 0
                    $scope.back()
                else
                    $scope.currentStep--

            $scope.isFinish = -> $scope.currentStep >= 2
            $scope.finish = -> $state.go('inventory-detail', {id: $scope.global.result.id }) if $scope.isFinish()

            $scope.getTemplateLink = () ->
                ADDRESS_BACKEND +
                $scope.who + '/inventory/template?' +
                'access_token=' + Authentication.getAccessToken() +
                '&lang=' + CtrlUtilsService.getUserLanguage()

            $scope.getRowErrorData = (data) ->
                if _.isEmpty(data)
                    return '<' + $filter('translate')('import.data.empty') + '>'
                else
                    return data

            verify = ->
                $scope.global.confirm = {}

                status = $scope.loadStatus.getStatusByDataName('data')

                params =
                    who: $scope.who
                    category: 'inventory'
                    param: 'verify'
                    fileId: $scope.global.excel.fileId
                    time: $scope.getIsoTime()

                onSuccess = (data) ->
                    $scope.global.confirm = data
                    $scope.currentStep++

                onFailure = (error) ->
                    if error.status is 400
                        status.error = false
                        toast.logError($filter('translate')(error.data.meta.error_message))

                CtrlUtilsService.loadSingleData(params, status, onSuccess, onFailure)

            doImport = ->
                $scope.global.result = {}

                status = $scope.loadStatus.getStatusByDataName('data')

                params =
                    who: $scope.who
                    category: 'inventory'
                    param: 'import'
                    fileId: $scope.global.excel.fileId
                    time: $scope.getIsoTime()

                onSuccess = (data) ->
                    $scope.global.result = data
                    $scope.currentStep++

                CtrlUtilsService.doPost(params, status, { photos : $scope.global.photos }, onSuccess, null, 'save')

            CtrlInitiatorService.initUseDatePickerCtrl($scope)
            CtrlInitiatorService.initCanBackViewCtrl($scope)

            $scope.getIsoTime = ->
                $scope.global.date.time.setSeconds(0)
                return globalUtils.createIsoTime($scope.global.date.date, $scope.global.date.time)

            $scope.addInitFunction( ->
                initStep()

                $scope.global = {}
                $scope.global.date = $scope.createDatePickerModel(new Date())
                $scope.global.date.time = new Date()
                $scope.global.excel = null
                $scope.global.confirm = null
                $scope.global.result = null
            )

            $scope.init()

    ])
