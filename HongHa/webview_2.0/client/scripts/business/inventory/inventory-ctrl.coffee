'use strict'

angular.module('app')

.controller('InventoryCtrl', [
    '$scope', 'CtrlCategoryInitiatorService'
    ($scope, CtrlCategoryInitiatorService) ->
        $scope.title = 'inventory.update.list.title'
        $scope.categoryName = 'inventory'
        $scope.usePopup = false
        $scope.isBigData = true
        $scope.isUseDistributorFilter = -> false
        $scope.isUseRegionFilter = -> false

        $scope.importState = 'import-inventory'

        CtrlCategoryInitiatorService.initCategoryListViewCtrl($scope)

        $scope.init()

])

.controller('InventoryDetailCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService', 'ADDRESS_BACKEND',
    'toast', '$filter'
    ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService, ADDRESS_BACKEND, toast, $filter) ->
        $scope.title = 'inventory.update.title'
        $scope.categoryName  = 'inventory'
        $scope.isIdRequire = -> true
        $scope.defaultBackState = 'inventory'

        # AFTER LOAD
        $scope.onLoadSuccess = ->
            if $scope.isNew() or $scope.isDraft()
                LoadingUtilsService.loadProductsByDistributor(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('products')
                    (list) ->
                        productQuantityMap = {}
                        if $scope.record? and $scope.record.details?
                            for product in $scope.record.details
                                productQuantityMap[product.id] = product

                        categories = []
                        categoryMap = {}
                        for product in list
                            if not categoryMap[product.productCategory.id]?
                                categories.push(product.productCategory)

                                categoryMap[product.productCategory.id] = {}
                                categoryMap[product.productCategory.id].name = product.productCategory.name
                                categoryMap[product.productCategory.id].products = []

                            product.isCategory = false
                            product.quantity = if productQuantityMap[product.id]? then productQuantityMap[product.id].quantity else null

                            categoryMap[product.productCategory.id].products.push(product)

                        categories = _.sortBy(categories, (o) -> return o.name )
                        $scope.products = []
                        for category in categories
                            $scope.products = _.concat(
                                $scope.products,
                                { isCategory: true, name: categoryMap[category.id].name },
                                categoryMap[category.id].products
                            )
                )

            if $scope.isNew()
                $scope.date = $scope.createDatePickerModel(new Date())
                $scope.date.time = new Date()

            if $scope.record?
                if $scope.record.time?
                    $scope.date = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.record.time))
                    $scope.date.time = globalUtils.parseIsoTime($scope.record.time)

                if not $scope.isDraft()
                    categories = []
                    categoryMap = {}
                    for product in $scope.record.details
                        if not categoryMap[product.productCategory.id]?
                            categories.push(product.productCategory)

                            categoryMap[product.productCategory.id] = {}
                            categoryMap[product.productCategory.id].name = product.productCategory.name
                            categoryMap[product.productCategory.id].products = []

                        product.isCategory = false

                        categoryMap[product.productCategory.id].products.push(product)

                    categories = _.sortBy(categories, (o) -> return o.name )
                    $scope.products = []
                    for category in categories
                        $scope.products = _.concat(
                            $scope.products,
                            { isCategory: true, name: categoryMap[category.id].name },
                            _.sortBy(categoryMap[category.id].products, (o) -> return o.name )
                        )

        $scope.getPhotoLink = (photoId) -> ADDRESS_BACKEND + 'image/' + photoId

        $scope.getIsoTime = ->
            $scope.date.time.setSeconds(0)
            globalUtils.createIsoTime($scope.date.date, $scope.date.time)

        $scope.checkBeforeSave = ->
            if not $scope.date.date?
                toast.logError($filter('translate')('error.data.input.not.valid'))
                return false
            else
                return true

        $scope.getObjectToSave = ->
            objectToSave = {}
            objectToSave.time = $scope.getIsoTime()
            objectToSave.details = []
            for product in $scope.products
                if not product.isCategory and product.quantity? and product.quantity >= 0
                    objectToSave.details.push({ productId: product.id, quantity: product.quantity })

            return objectToSave

        CtrlInitiatorService.initUseDatePickerCtrl($scope)
        CtrlCategoryInitiatorService.initCategoryDetailViewCtrl($scope)

        $scope.init()
])
