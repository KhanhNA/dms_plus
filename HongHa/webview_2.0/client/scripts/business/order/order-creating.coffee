'use strict'

angular.module('app')

.controller('OrderCreatingCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'LoadingUtilsService',
        '$filter', 'toast', '$state', '$stateParams', '$modal'
        ($scope, CtrlUtilsService, CtrlInitiatorService, LoadingUtilsService, $filter, toast, $state, $stateParams, $modal) ->
            $scope.openSelectCustomer = ->
                modalInstance = $modal.open(
                    templateUrl: 'views/business/customer/customer-select-popup.html',
                    controller: 'CustomerSelectPopupCtrl',
                    resolve:
                        distributorId: -> null
                        distributorName: -> null
                    backdrop: true
                )
                modalInstance.result.then((customer) ->
                    $scope.order.customerId = customer.id
                    $scope.order.customerName = customer.name
                )

            $scope.clearCustomer = ->
                $scope.order.customerId = null
                $scope.order.customerName = null

            $scope.deliveryTypes = [
                {id: 0, name: $filter('translate')('order.delivery.immediate')}
                {id: 1, name: $filter('translate')('order.delivery.same.day')}
                {id: 2, name: $filter('translate')('order.delivery.other.day')}
            ]

            $scope.addProduct = ->
                modalInstance = $modal.open(
                    templateUrl: 'views/business/order/product-select-for-order-popup.html',
                    controller: 'ProductSelectPopupForOrderCtrl',
                    resolve:
                        products: -> $scope.products
                        productSelectedIds: -> $scope.productSelectedIds
                    backdrop: true
                )
                modalInstance.result.then((product) ->
                    if !_.includes($scope.productSelectedIds, product.id)
                        $scope.order.details.push({product: product, productId: product.id, quantity: 1})
                        $scope.productSelectedIds.push(product.id)
                )

            $scope.removeProduct = (detail) ->
                console.log detail.productId
                $scope.order.details.splice(_.indexOf($scope.order.details, detail), 1)
                $scope.productSelectedIds.splice(_.indexOf($scope.productSelectedIds, detail.productId), 1)

            $scope.isProductListEmpty = -> _.isEmpty($scope.order.details)
            $scope.getAmount = (detail) -> detail.product.price * detail.quantity

            checkBeforeCalculatePromotion = ->
                if not $scope.order.salesmanId?
                    toast.logError($filter('translate')('please.select.salesman'))
                    return false
                else if not $scope.order.customerId?
                    toast.logError($filter('translate')('please.select.customer'))
                    return false
                else if $scope.order.deliveryType is 2 and not $scope.deliveryDate.date?
                    toast.logError($filter('translate')('order.creating.please.select.delivery.date'))
                    return false
                else if $scope.isProductListEmpty()
                    toast.logError($filter('translate')('order.creating.please.select.at.least.one.product'))
                    return false

                return true

            $scope.calculate = ->
                if checkBeforeCalculatePromotion()
                    $scope.orderCalculated = null

                    $scope.order.deliveryTime = globalUtils.createIsoDate($scope.deliveryDate.date) + 'T00:00:00'

                    params =
                        who: $scope.who
                        category: 'order'
                        subCategory: 'calculate'

                    status = $scope.loadStatus.getStatusByDataName('calculate.promotion')

                    onSuccess = (data) ->
                        $scope.orderCalculated = data

                    CtrlUtilsService.doPost(params, status, $scope.order, onSuccess, null,
                        'order.creating.calculate.promotion')

            $scope.back = -> $scope.orderCalculated = null

            $scope.save = ->
                params =
                    who: $scope.who
                    category: 'order'

                status = $scope.loadStatus.getStatusByDataName('save')

                onSuccess = (data) -> $state.go('order-detail', {id: data.id, parent: 'order-creating'})

                CtrlUtilsService.doPost(params, status, $scope.order, onSuccess, null, 'order.creating.save')

            # INIT
            CtrlInitiatorService.initUseDatePickerCtrl($scope)

            $scope.addInitFunction(->
                LoadingUtilsService.loadProductsByDistributor(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('product')
                    (list) -> $scope.products = list
                )

                LoadingUtilsService.loadSalesmenByDistributor(
                    $scope.who
                    null
                    $scope.loadStatus.getStatusByDataName('salesmen')
                    (list) -> $scope.salesmen = list
                )

                $scope.order = {
                    deliveryType: 0
                    discountAmt: 0
                    details: []
                }
                $scope.productSelectedIds = []
                $scope.deliveryDate = $scope.createDatePickerModel(new Date())

                $scope.orderCalculated = null
            )

            $scope.init()

    ])

.controller('ProductSelectPopupForOrderCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', '$modalInstance', 'products', 'productSelectedIds',
        'toast', '$filter'
        ($scope, CtrlUtilsService, CtrlInitiatorService, $modalInstance, products, productSelectedIds, toast, $filter) ->
            $scope.title = 'product'
            $scope.productSelectedIds = productSelectedIds ? []
            $scope.isSelected = (product) -> _.includes($scope.productSelectedIds, product.id)
            $scope.products =
                if _.isEmpty(products)
                then []
                else _.filter(products, (product) -> not $scope.isSelected(product))

            $scope.searchText = null
            $scope.pagingData = {
                "pagingMaxSize": 5
                "itemsPerPage": 10
            }

            $scope.search = ->
                productsSearched = _.filter($scope.products, (product) ->
                    if $scope.searchText? and $scope.searchText.length > 0
                        searchText = $scope.searchText.toLowerCase()
                        return product.code.indexOf(searchText) isnt -1
                    else
                        return true
                )

                $scope.pagingData.count = productsSearched.length
                $scope.pagingData.currentPage = 1
                $scope.productsSearched = productsSearched
                $scope.changePage()

            $scope.changePage = ->
                startIndex = (($scope.pagingData.currentPage - 1) * $scope.pagingData.itemsPerPage)
                if startIndex > $scope.pagingData.count - 1
                    return
                endIndex = (($scope.pagingData.currentPage - 1) * $scope.pagingData.itemsPerPage) + 9
                endIndex = if endIndex > $scope.pagingData.count then $scope.pagingData.count else endIndex
                $scope.records = _.slice($scope.productsSearched, startIndex, endIndex)

            $scope.search()

            $scope.select = (product) -> $modalInstance.close(product) if not $scope.isSelected(product)
            $scope.cancel = -> $modalInstance.dismiss('cancel')
            $scope.isEmpty = -> _.isEmpty($scope.products)

            CtrlInitiatorService.initBasicViewCtrl($scope)

            $scope.init()
    ])
