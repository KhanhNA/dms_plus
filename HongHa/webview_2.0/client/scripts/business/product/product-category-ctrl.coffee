'use strict'

angular.module('app')

.controller('ProductCategoryCtrl', [
        '$scope', 'CtrlCategoryInitiatorService'
        ($scope, CtrlCategoryInitiatorService) ->
            $scope.title = 'product.category.title'
            $scope.categoryName = 'product-category'
            $scope.usePopup = true
            $scope.isUseDistributorFilter = -> false
            $scope.isUseRegionFilter = -> false
            $scope.useEditButton =  false

            CtrlCategoryInitiatorService.initCategoryListViewCtrl($scope)

            $scope.init()
])
