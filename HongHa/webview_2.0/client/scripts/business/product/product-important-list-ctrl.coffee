'use strict'

angular.module('app')

.controller('ProductImportantListCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService', '$filter', 'toast', '$state', '$stateParams'
        ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService, $filter, toast, $state, $stateParams) ->
            $scope.title = 'product.important.title'

            # LOADING
            $scope.reloadData = ->
                $scope.datas = []

                params =
                    who: $scope.who
                    category: 'product-important'

                CtrlUtilsService.loadListData(params
                    $scope.loadStatus.getStatusByDataName('data')
                    (records) ->
                        productCategoryMap = _.groupBy(records, (product) -> product.productCategory.name )
                        $scope.datas = []
                        for productCategory of productCategoryMap
                            $scope.datas.push({ name: productCategory, products: productCategoryMap[productCategory]})
                        $scope.datas = _.orderBy($scope.datas, ['name'], ['asc'])

                    ->
                        toast.logError($filter('translate')('loading.error'))
                )

            $scope.isEmpty = -> _.isEmpty($scope.datas)

            # SAVE
            $scope.checkBeforeSave = ->
                if $scope.isFormValid('form')
                    return true
                else
                    toast.logError($filter('translate')('error.data.input.not.valid'))
                    return false

            $scope.getObjectToSave = ->
                saveData = {productIds: []}

                for data in $scope.datas
                    for product in data.products
                        if product.important
                            saveData.productIds.push(product.id)

                return saveData

            $scope.getSaveParams = (params) ->
                params.category = 'product-important'
                return params

            $scope.onSaveSuccess = -> # DO NOTHING
            $scope.onSaveFailure = -> # DO NOTHING
            $scope.isPost = -> false

             # INIT
            CtrlInitiatorService.initCanSaveViewCtrl($scope)

            $scope.addInitFunction(->
                $scope.reloadData()
            )

            $scope.init()

    ])
