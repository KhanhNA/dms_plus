'use strict'

angular.module('app')

.controller('RegionHierarchyCtrl',[
        '$scope', '$state', '$stateParams', 'toast', '$filter', 'CtrlUtilsService', 'CtrlInitiatorService','CtrlCategoryInitiatorService','LoadingUtilsService'
        ($scope  , $state, $stateParams, toast, $filter, CtrlUtilsService, CtrlInitiatorService,CtrlCategoryInitiatorService,LoadingUtilsService) ->
            $scope.title = 'region.menu'
            $scope.categoryName  = 'region'
            $scope.subCategory ='hierarchy'
            $scope.isIdRequire = -> false
            $scope.editing = false
            $scope.creating = false


            # Load Tree hierarchy
            $scope.reloadData = ->
                status = $scope.loadStatus.getStatusByDataName('data')
                params =
                    who: $scope.who
                    category: 'region'
                    subCategory :'hierarchy'

                callbackSuccess = (data) ->
                    $scope.record = data.list
                    $scope.info = angular.copy(data.list[0])
                    $scope.editing = false
                    $scope.creating = false

                callbackFailure = ->
                    toast.logError($filter('translate')('loading.error'))

                CtrlUtilsService.loadSingleData(params, status, callbackSuccess, callbackFailure)


            $scope.isReadonly = ->
                not ($scope.editing || $scope.creating)

            $scope.chooseRegion = (object) ->
                if  $scope.isReadonly()
                    $scope.info = angular.copy(object)
                    $scope.info.children = []
                    for child in object.children
                        temp = {"id":"","name":"","code":""}
                        temp.id = child.id
                        temp.name = child.name
                        $scope.info.children.push(temp)

            $scope.loadCategory = ->
                if $scope.info?
                    LoadingUtilsService.loadRegions(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('regions')
                        (list) -> $scope.regions = list
                    )
                else
                    $scope.regions = []


            $scope.create = ->
                $scope.editing = false
                $scope.creating = true
                $scope.info = {}
                $scope.loadCategory()

            $scope.edit =  ->
                $scope.editing = true
                $scope.creating = false
                if $scope.info?
                    if $scope.info.parent? and $scope.info.parent.id?
                        $scope.info.parentRegionId =  $scope.info.parent.id
                $scope.loadCategory()


            $scope.back = ->
                if($scope.creating)
                    $scope.info = angular.copy($scope.record[0])
                $scope.info.parentRegionId = undefined
                $scope.editing = false
                $scope.creating = false

            currentCheckBeforeSave = $scope.checkBeforeSave
            $scope.checkBeforeSave = ->
                if $scope.isFormValid('form')
                    if not jQuery.isFunction(currentCheckBeforeSave) or currentCheckBeforeSave()
                        return true
                    else
                        return false
                else
                    toast.logError($filter('translate')('error.data.input.not.valid'))
                    return false

            $scope.getSaveParams  = (params) ->
                params.category = $scope.categoryName
                if $scope.editing then params.id = $scope.info.id
                return params

            $scope.getObjectToSave = $scope.getObjectToSave ? -> $scope.info
            $scope.onSaveSuccess = ->
                $scope.editing = false;
                $scope.creating = false;
                $scope.reloadData()
            $scope.onSaveFailure = ->
            $scope.isPost = -> !$scope.isReadonly() and $scope.creating

            # ENABLE
            $scope.enable = ->
                if not $scope.isChanged() or $scope.checkBeforeSave()
                    loadStatusOfData = $scope.loadStatus.getStatusByDataName('enable')

                    requestBody = null
                    if $scope.isChanged()
                        requestBody = $scope.getObjectToSave()

                    params = { who: $scope.who }
                    params = $scope.getSaveParams(params)
                    if $scope.creating
                        params.action = 'enable'

                    onSuccess = (data) ->
                        $scope.changeStatus.reset()
                        $scope.onSaveSuccess(data)

                    onFailure = (error) ->
                        $scope.onSaveFailure(error)

                    if $scope.isPost()
                        CtrlUtilsService.doPost(params, loadStatusOfData, requestBody, onSuccess, onFailure, 'enable')
                    else
                        CtrlUtilsService.doPut(params, loadStatusOfData, requestBody, onSuccess, onFailure, null)

            $scope.goToRegionDistributors = ->
                $state.go('user-advance-supervisor-region-distributors', { id: $scope.info.id, filter: $stateParams.filter })

            CtrlInitiatorService.initCanSaveViewCtrl($scope)

            $scope.addInitFunction( -> $scope.reloadData())

            $scope.init()
    ])


.controller('RegionDistributorsCtrl',[
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
        '$filter', 'toast', '$state', '$stateParams', '$confirm'
        ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
         $filter, toast, $state, $stateParams, $confirm) ->
            $scope.title = 'region.distributors'

            # LOADING
            $scope.reloadData = ->
                $scope.records = []

                params =
                    who: $scope.who
                    category: 'region'
                    id: $stateParams.id
                    param: 'distributors'

                CtrlUtilsService.loadListData(params
                    $scope.loadStatus.getStatusByDataName('data')
                    (records) -> $scope.records = records
                    -> toast.logError($filter('translate')('loading.error'))
                )

            $scope.isEmpty = -> _.isEmpty($scope.records)

            # SAVE
            $scope.checkBeforeSave = ->
                return true

            $scope.getObjectToSave = ->
                saveData = { list: [] }

                for record in $scope.records
                    saveData.list.push(record.id) if record.selected

                return saveData

            $scope.getSaveParams = (params) ->
                params.category = 'region'
                params.id = $stateParams.id
                params.param = 'distributors'
                return params

            $scope.moreThan20 = -> $scope.records? && $scope.records.length > 20

            $scope.onSaveSuccess = -> # DO NOTHING
            $scope.onSaveFailure = -> # DO NOTHING
            $scope.isPost = -> false

            # INIT
            # CtrlInitiatorService.initCanBackViewCtrl($scope)
            $scope.back = ->
                if $scope.isChanged()
                    $confirm( { text: $filter('translate')('confirm.dialog.are.you.sure.back') } ).then( ->
                        $state.go( 'region-hierarchy', { id: $stateParams.id, filter: $stateParams.filter } )
                    )
                else
                    $state.go('region-hierarchy', { id: $stateParams.id, filter: $stateParams.filter } )

            CtrlInitiatorService.initCanSaveViewCtrl($scope)

            $scope.addInitFunction(->
                $scope.reloadData()
            )

            $scope.init()

    ])
