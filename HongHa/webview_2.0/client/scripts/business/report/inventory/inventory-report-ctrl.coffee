'use strict'

angular.module('app')

.controller('InventoryReportCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
        '$filter', 'toast', '$state'
        ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state) ->
            $scope.title = 'inventory.report.title'
            $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'
            $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()

            $scope.goToReport = ->
                if $scope.isDisplayDistributor() and (not $scope.filter.forAllDistributor? or $scope.filter.forAllDistributor is false) and (not $scope.filter.distributorIds? or $scope.filter.distributorIds.length == 0)
                    toast.logError($filter('translate')('please.select.distributor'))
                else if $scope.isDisplayProduct() and not $scope.filter.productId?
                    toast.logError($filter('translate')('please.select.product'))
                else
                    if $scope.filter.reportType is 'distributor'
                        $state.go('inventory-report-distributor', { filter: $scope.getFilterAsString() })
                    if $scope.filter.reportType is 'product'
                        $state.go('inventory-report-product', { filter: $scope.getFilterAsString() })
                return true

            $scope.isDisplayDistributor = -> $scope.filter.reportType is 'distributor' and $scope.isUseDistributorFilter()
            $scope.isDisplayProduct = -> $scope.filter.reportType is 'product'
            $scope.isDisplayDistributorAll = -> $scope.isDisplayDistributor() and $scope.filter.forAllDistributor

            CtrlInitiatorService.initFilterViewCtrl($scope)
            CtrlInitiatorService.initUseDatePickerCtrl($scope)

            $scope.regionFilterChange = ->
                LoadingUtilsService.loadDistributorByRegion(
                    $scope.who
                    $scope.filter.regionId
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = list
                )

            $scope.addInitFunction( ->
                if not $scope.filter.reportType?
                    $scope.filter.reportType = 'distributor'

                LoadingUtilsService.loadProductsByDistributor(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('products')
                    (list) ->
                        $scope.products = _.union(
                            [
                                {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )
                )

                if $scope.isUseRegionFilter()
                    LoadingUtilsService.loadAccessibleRegions(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('regions')
                        (list) ->
                            $scope.regions = _.union(
                                [
                                    {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                                ]
                                list
                            )
                            if not $scope.filter.regionId? and $scope.regions.length > 0
                                $scope.filter.regionId = $scope.regions[0].id
                            $scope.regionFilterChange()
                    )
                else if $scope.isUseDistributorFilter()
                    LoadingUtilsService.loadDistributors(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('distributors')
                        (list) -> $scope.distributors = list
                    )
            )

            $scope.init()
    ])

.controller('InventoryReportDistributorCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
    '$filter', 'toast', '$state', '$stateParams', 'ADDRESS_BACKEND', 'Authentication'
    ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state, $stateParams, ADDRESS_BACKEND, Authentication) ->
        $scope.title = 'inventory.report.title'
        $scope.defaultBackState = 'inventory-report'

        $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'

        CtrlInitiatorService.initCanBackViewCtrl($scope)
        CtrlInitiatorService.initFilterViewCtrl($scope)

        $scope.getLastInventoryUpdateTime = ->
            if $scope.record? and $scope.record.lastInventory?
            then $filter('isoDateTimeWithoutSecond')($scope.record.lastInventory.time)
            else 'N/A'
        $scope.getLastInventoryUpdateTime = (lastInventory)->
            if lastInventory?
            then $filter('isoDateTimeWithoutSecond')(lastInventory.time)
            else 'N/A'

        $scope.getPhotoLink = (photoId) -> ADDRESS_BACKEND + 'image/' + photoId
        $scope.getQuantityDisplay = (quantity) -> if quantity? then $filter('number')(quantity, 0) else 'N/A'

        $scope.export = ->
            params =
                who: $scope.who
                category: 'export'
                subCategory: 'inventory-report'
                param: 'by-distributor'

            requestBody = {distributorIds: []}

            if $scope.filter.distributorIds?
                for id in $scope.filter.distributorIds
                    requestBody.distributorIds.push(id)

            if $scope.isUseDistributorFilter()
                if $scope.filter.forAllDistributor
                    params.way = 'all'
                else
                    params.way = 'specify'

            params.lang = CtrlUtilsService.getUserLanguage()
            CtrlUtilsService.doPut(params
                $scope.loadStatus.getStatusByDataName('data')
                requestBody
                (record) ->
                    console.log record
                    href = ADDRESS_BACKEND + '/file'
                    href = href + '?access_token=' + Authentication.getAccessToken()
                    href = href + '&id=' +  record.id
                    location.href = href
                (error) ->
                    console.log error
                'get.excel'
            )

        # LOADING
        $scope.reloadData = ->
            params =
                who: $scope.who
                category: 'report'
                subCategory: 'inventory'
                param: 'by-distributor'

            requestBody = {distributorIds: []}
            if $scope.filter.distributorIds?
                for id in $scope.filter.distributorIds
                    requestBody.distributorIds.push(id)
            if $scope.filter.regionId?
                params.regionId = $scope.filter.regionId

            if $scope.isUseDistributorFilter()
                if $scope.filter.forAllDistributor
                    params.way = 'all'
                else
                    params.way = 'specify'

            CtrlUtilsService.doPut(params
                $scope.loadStatus.getStatusByDataName('data')
                requestBody
                (record) ->
                    $scope.record = record

                    $scope.products = []

                    if $scope.record.products?
                        categories = []
                        categoryMap = {}
                        for product in $scope.record.products
                            if not categoryMap[product.productCategory.id]?
                                categories.push(product.productCategory)

                                categoryMap[product.productCategory.id] = {}
                                categoryMap[product.productCategory.id].name = product.productCategory.name
                                categoryMap[product.productCategory.id].products = []

                            product.isCategory = false

                            categoryMap[product.productCategory.id].products.push(product)

                        categories = _.sortBy(categories, (o) -> return o.name )
                        for category in categories
                            $scope.products = _.concat(
                                $scope.products,
                                { isCategory: true, name: categoryMap[category.id].name },
                                _.sortBy(categoryMap[category.id].products, (o) -> return o.name )
                            )

                (error) ->
                    if (error.status is 400)
                        $state.go('404')
                    else
                        toast.logError($filter('translate')('loading.error'))

                'get.data'
            )

        $scope.addInitFunction( ->
            if $scope.filter?
                if $scope.isUseDistributorFilter() and  not $scope.filter.forAllDistributor  and $scope.filter.distributorIds.length ==0
                    $state.go('404')
                else
                    $scope.reloadData()
            else
                $state.go('404')
        )

        $scope.init()
])

.controller('InventoryReportProductCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
    '$filter', 'toast', '$state', '$stateParams', 'ADDRESS_BACKEND', 'Authentication'
    ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state, $stateParams, ADDRESS_BACKEND, Authentication) ->
        $scope.title = 'inventory.report.title'
        $scope.defaultBackState = 'inventory-report'

        CtrlInitiatorService.initCanBackViewCtrl($scope)
        CtrlInitiatorService.initFilterViewCtrl($scope)

        $scope.export = ->
            href = ADDRESS_BACKEND + $scope.who + '/export/inventory-report/by-product'
            href = href + '?access_token=' + Authentication.getAccessToken()
            href = href + '&productId=' +  $scope.filter.productId
            href = href + '&lang=' + CtrlUtilsService.getUserLanguage()
            location.href = href

        $scope.getLastInventoryUpdateTimeForSingleProduct = (distributor) ->
            if distributor? and distributor.lastInventory?
            then $filter('isoDateTimeWithoutSecond')(distributor.lastInventory.time)
            else 'N/A'

        $scope.getLastInventoryUpdateTimeForMultipleProduct = (lastInventory)->
            if lastInventory?
            then $filter('isoDateTimeWithoutSecond')(lastInventory.time)
            else 'N/A'

        $scope.getQuantityDisplay = (quantity) -> if quantity? then $filter('number')(quantity, 0) else 'N/A'
        $scope.getPhotoLink = (photoId) -> ADDRESS_BACKEND + 'image/' + photoId
        # LOADING
        $scope.reloadData = ->
            params =
                who: $scope.who
                category: 'report'
                subCategory: 'inventory'
                param: 'by-product'
                productId: $scope.filter.productId

            CtrlUtilsService.loadSingleData(params
                $scope.loadStatus.getStatusByDataName('data')
                (record) ->
                    $scope.record = record
                    $scope.products = []

                    if $scope.record.distributors?
                        $scope.record.distributors = _.sortBy($scope.record.distributors, (o) -> return o.name )
                    else
                        $scope.record.distributors = []

                    if $scope.record.products?
                        categories = []
                        categoryMap = {}
                        for product in $scope.record.products
                            if not categoryMap[product.productCategory.id]?
                                categories.push(product.productCategory)

                                categoryMap[product.productCategory.id] = {}
                                categoryMap[product.productCategory.id].name = product.productCategory.name
                                categoryMap[product.productCategory.id].products = []

                            product.isCategory = false

                            categoryMap[product.productCategory.id].products.push(product)

                        categories = _.sortBy(categories, (o) -> return o.name )
                        for category in categories
                            $scope.products = _.concat(
                                $scope.products,
                                { isCategory: true, name: categoryMap[category.id].name },
                                _.sortBy(categoryMap[category.id].products, (o) -> return o.name )
                            )
                (error) ->
                    if (error.status is 400)
                        $state.go('404')
                    else
                        toast.logError($filter('translate')('loading.error'))
            )

        $scope.addInitFunction( ->
            if $scope.filter?
                if not $scope.filter.productId?
                    $state.go('404')
                else
                    $scope.reloadData()
            else
                $state.go('404')
        )

        $scope.init()
])
