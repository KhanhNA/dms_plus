'use strict'

chartOptions = {
    colors: ["#26A69A", "#607D8B", "#26C6DA"]
    tooltip: {show: true}
    tooltipOpts:
        defaultTheme: false
    grid:
        hoverable: true
        clickable: true
        tickColor: "#f9f9f9"
        borderWidth: 1
        borderColor: "#eeeeee"
    yaxes: [{}, {position: "right"}]
}

angular.module('app')

.controller('SalesReportCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
        '$filter', 'toast', '$state'
        ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
         $filter, toast, $state) ->
            $scope.title = 'sales.report.title'
            $scope.isUseDistributorFilter = -> $scope.who isnt 'distributor'
            $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()
            $scope.isUseFilterImportantProduct = -> CtrlUtilsService.getWho() isnt 'admin' and CtrlUtilsService.getWho() isnt 'supporter'

            checkDateDuration = ->
                if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
                    return false
                return true

            checkDate = ->
                if $scope.fromDate? and $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
                    if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
                        return true
                return false

            $scope.goToReport = ->
                if not checkDate()
                    toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
                else if not checkDateDuration()
                    toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
                else if $scope.isDisplayDistributorAll() and not $scope.filter.distributorWithAllId?
                    toast.logError($filter('translate')('please.select.distributor'))
                else if $scope.isDisplayProductCategory() and not $scope.filter.productCategoryId?
                    toast.logError($filter('translate')('please.select.category'))
                else
                    $scope.filter.fromDate = globalUtils.createIsoDate($scope.fromDate.date)
                    $scope.filter.toDate = globalUtils.createIsoDate($scope.toDate.date)

                    if  not $scope.filter.distributorId? and $scope.filter.reportType is 'distributor'
                        toast.logWarning($filter('translate')('warning.request.take.time'))
                    if $scope.filter.reportType is 'daily'
                        $state.go('sales-report-daily', {filter: $scope.getFilterAsString()})
                    else if $scope.filter.reportType is 'distributor'
                        $state.go('sales-report-distributor', {filter: $scope.getFilterAsString()})
                    else if $scope.filter.reportType is 'product'
                        $state.go('sales-report-product', {filter: $scope.getFilterAsString()})
                    else if $scope.filter.reportType is 'salesman'
                        $state.go('sales-report-salesman', {filter: $scope.getFilterAsString()})


            $scope.isDisplayDistributor = ->  _.includes(['daily', 'product','salesman'], $scope.filter.reportType) && $scope.isUseDistributorFilter()
            $scope.isDisplayDistributorAll = -> false
            $scope.isDisplayRegion = -> $scope.isUseRegionFilter() && $scope.filter.reportType isnt 'distributor'
            $scope.isDisplayProductCategory = -> $scope.filter.reportType is 'product'
            $scope.isDisplayProductImportant = -> $scope.filter.reportType is 'product'

            CtrlInitiatorService.initFilterViewCtrl($scope)
            CtrlInitiatorService.initUseDatePickerCtrl($scope)

            $scope.regionFilterChange = ->
                LoadingUtilsService.loadDistributorByRegion(
                    $scope.who
                    $scope.filter.regionId
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = _.union(
                            [
                                {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )
                )

            $scope.addInitFunction(->
                if not $scope.filter.reportType?
                    $scope.filter.reportType = 'daily'

                if $scope.filter.fromDate? and $scope.filter.toDate?
                    $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
                    $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
                    if not checkDate() or not checkDateDuration()
                        $state.go('404')
                else
                    $scope.fromDate = $scope.createDatePickerModel(new Date())
                    $scope.toDate = $scope.createDatePickerModel(new Date())

                if not $scope.filter.isImportantProduct?
                    $scope.filter.isImportantProduct = false

                LoadingUtilsService.loadProductCategories(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('productCategories')
                    (list) ->
                        $scope.productCategories = list
                        $scope.productCategoriesWithAll = _.union(
                            [
                                {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )
                )

                if $scope.isUseRegionFilter()
                    LoadingUtilsService.loadAccessibleRegions(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('regions')
                        (list) ->
                            $scope.regions = list
                            if not $scope.filter.regionId? and $scope.regions.length > 0
                                $scope.filter.regionId = $scope.regions[0].id

                            LoadingUtilsService.loadDistributorByRegion(
                                $scope.who
                                $scope.filter.regionId
                                $scope.loadStatus.getStatusByDataName('distributors')
                                (list) ->
                                    $scope.distributors =  _.union(
                                        [
                                            {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                                        ]
                                        list
                                    )
                            )
                    )
                else if $scope.isUseDistributorFilter()
                    LoadingUtilsService.loadDistributors(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('distributors')
                        (list) ->
                            $scope.distributors =  _.union(
                                [
                                    {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                                ]
                                list
                            )
                    )
            )

            $scope.init()
    ])

.controller('SalesReportDetailCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
        '$filter', 'toast', '$state', '$stateParams'
        ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
         $filter, toast, $state) ->
            $scope.title = '' # UPDATE LATER
            $scope.defaultBackState = 'sales-report'

            CtrlInitiatorService.initCanBackViewCtrl($scope)
            CtrlInitiatorService.initFilterViewCtrl($scope)

            tooltip = (label, xval, yval) -> label + ' - ' + $scope.nameMap[xval - 1] + ': ' + $filter('number')(yval,
                0)

            afterLoad = ->
                revenues = []
                nbOrders = []
                ticks = []
                $scope.nameMap = []

                $scope.total = {revenue: 0, subRevenue: 0, nbOrder: 0}
                for dto, i in $scope.records
                    revenues.push([(i + 1), dto.salesResult.revenue])
                    nbOrders.push([(i + 1), dto.salesResult.nbOrder])

                    $scope.total.revenue += dto.salesResult.revenue
                    $scope.total.subRevenue += dto.salesResult.subRevenue
                    $scope.total.nbOrder += dto.salesResult.nbOrder

                    if $scope.filter.reportType is 'daily'
                        if (i + 1) is 1 or (i + 1) % 5 is 0
                            ticks.push([(i + 1), '' + (i + 1)])
                        else
                            ticks.push([(i + 1), ''])
                        $scope.nameMap.push((i + 1) + '/' + moment($scope.date).format('MM/YYYY'))
                    else if $scope.filter.reportType is 'distributor'
                        ticks.push([(i + 1), globalUtils.getChartLabel(dto.name)])
                        $scope.nameMap.push(globalUtils.escapeHTML(dto.name))
                    else if $scope.filter.reportType is 'product'
                        ticks.push([(i + 1), globalUtils.getChartLabel(dto.name)])
                        $scope.nameMap.push(globalUtils.escapeHTML(dto.name))
                    else if $scope.filter.reportType is 'salesman'
                        ticks.push([(i + 1), globalUtils.getChartLabel(dto.fullname)])
                        $scope.nameMap.push(globalUtils.escapeHTML(dto.fullname))

                $scope.chart.data = [
                    data: revenues
                    label: $filter('translate')('revenue')
                ,
                    data: nbOrders
                    label: $filter('translate')('order')
                    yaxis: 2
                ]

                for data, i in $scope.chart.data
                    if i is 0 and revenues.length < 20
                        data.bars = {
                            show: true
                            fill: true
                            barWidth: .1
                            align: 'center'
                        }
                    else
                        data.lines = {
                            show: true
                            fill: false
                            fillColor: {colors: [{opacity: 0}, {opacity: 0.3}]}
                        }
                        data.points = {
                            show: true
                            lineWidth: 2
                            fill: false
                            fillColor: "#ffffff"
                            symbol: "circle"
                            radius: 5
                        }
                        data.lines.fill = true if i is 0

                $scope.chart.options.xaxis = {autoscaleMargin: .10, ticks: ticks}

            $scope.isDisplayChart = -> $scope.chart.data? and $scope.chart.data[0].data? and $scope.chart.data[0].data.length > 1

            # LOADING
            $scope.reloadData = ->
                params =
                    who: $scope.who
                    category: 'report'
                    subCategory: 'sales'
                    fromDate: $scope.filter.fromDate
                    toDate: $scope.filter.toDate

                if $scope.filter.reportType is 'daily'
                    params.param = 'daily'
                    params.distributorId = $scope.filter.distributorId
                    params.regionId = $scope.filter.regionId
                else if $scope.filter.reportType is 'distributor'
                    params.param = 'by-distributor'
                else if $scope.filter.reportType is 'product'
                    params.param = 'by-product'
                    params.regionId = $scope.filter.regionId
                    params.productCategoryId = $scope.filter.productCategoryId
                    params.distributorId = $scope.filter.distributorId
                    params.isImportantProduct = $scope.filter.isImportantProduct
                else if $scope.filter.reportType is 'salesman'
                    params.param = 'by-salesman'
                    if $scope.who isnt 'distributor'
                        params.distributorId = $scope.filter.distributorId

                CtrlUtilsService.loadSingleData(params
                    $scope.loadStatus.getStatusByDataName('data')
                    (data) ->
                        $scope.records = data.list
                        afterLoad()
                    (error) ->
                        if (error.status is 400)
                            $state.go('404')
                        else
                            toast.logError($filter('translate')('loading.error'))
                )

            checkFilter = ->
                if $scope.filter.reportType is 'product' and not $scope.filter.productCategoryId?
                    return false

                if $scope.filter.reportType is 'salesman' and not $scope.filter.distributorId? and $scope.who isnt 'distributor'
                    return false

                return true

            $scope.addInitFunction(->
                $scope.chart = {}
                $scope.chart.options = angular.copy(chartOptions)
                $scope.chart.options.tooltip.content = tooltip

                if $scope.filter? and $scope.filter.fromDate? and $scope.filter.toDate?
                    $scope.fromDate = globalUtils.parseIsoDate($scope.filter.fromDate)
                    $scope.toDate = globalUtils.parseIsoDate($scope.filter.toDate)

                    if not checkFilter()
                        $state.go('404')
                    else
                        reportTitle = null
                        if $scope.filter.reportType is 'daily'
                            reportTitle = $filter('translate')('sales.report.daily.title')
                        else if $scope.filter.reportType is 'distributor'
                            reportTitle = $filter('translate')('sales.report.distributor.title')
                        else if $scope.filter.reportType is 'product'
                            reportTitle = $filter('translate')('sales.report.product.title')
                        else if $scope.filter.reportType is 'salesman'
                            reportTitle = $filter('translate')('sales.report.salesman.title')

                        $scope.title = reportTitle

                        $scope.reloadData()
                else
                    $state.go('404')
            )

            $scope.init()
    ])
