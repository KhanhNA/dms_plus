'use strict'

chartOptions = {
    colors: ["#26A69A", "#607D8B", "#26C6DA"]
    tooltip: {show: true}
    tooltipOpts:
        defaultTheme: false
    grid:
        hoverable: true
        clickable: true
        tickColor: "#f9f9f9"
        borderWidth: 1
        borderColor: "#eeeeee"
}

angular.module('app')

.controller('VisitReportCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
    '$filter', 'toast', '$state'
    ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state) ->
        $scope.title = 'visit.report.title'
        $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'
        $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()

        checkDateDuration = ->
            if moment($scope.fromDate.date).add(1, 'months').toDate().getTime() < $scope.toDate.date.getTime()
                return false
            return true

        checkDate = ->
            if $scope.fromDate? and $scope.fromDate.date? and $scope.toDate and $scope.toDate.date?
                if $scope.fromDate.date.getTime() <= $scope.toDate.date.getTime()
                    return true
            return false

        $scope.goToReport = ->
            if not checkDate()
                toast.logError($filter('translate')('from.date.cannot.be.greater.than.to.date'))
            else if not checkDateDuration()
                toast.logError($filter('translate')('max.duration.between.from.date.and.to.date.is.1.month'))
            else if $scope.isDisplayDistributorAll() and not $scope.filter.distributorWithAllId?
                toast.logError($filter('translate')('please.select.distributor'))
            else
                $scope.filter.fromDate = globalUtils.createIsoDate($scope.fromDate.date)
                $scope.filter.toDate = globalUtils.createIsoDate($scope.toDate.date)

                if $scope.isDisplayDistributorAll()
                        $scope.filter.distributorId = $scope.filter.distributorWithAllId

                if ($scope.isDisplayDistributorAll() and not $scope.filter.distributorId?) or $scope.filter.reportType is 'distributor'
                    toast.logWarning($filter('translate')('warning.request.take.time'))

                if $scope.filter.reportType is 'daily'
                    $state.go('visit-report-daily', {filter: $scope.getFilterAsString()})
                else if $scope.filter.reportType is 'distributor'
                    $state.go('visit-report-distributor', {filter: $scope.getFilterAsString()})
                else if $scope.filter.reportType is 'salesman'
                    $state.go('visit-report-salesman', {filter: $scope.getFilterAsString()})

        $scope.isDisplayDistributor = -> false #$scope.filter.reportType is 'salesman'
        $scope.isDisplayDistributorAll = -> _.includes(['daily','salesman'], $scope.filter.reportType)
        $scope.isDisplayRegion = -> $scope.isDisplayDistributor() or $scope.isDisplayDistributorAll()

        CtrlInitiatorService.initFilterViewCtrl($scope)
        CtrlInitiatorService.initUseDatePickerCtrl($scope)


        $scope.regionFilterChange = ->
            if $scope.isUseRegionFilter()
                LoadingUtilsService.loadDistributorByRegion(
                    $scope.who
                    $scope.filter.regionId
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = list
                        $scope.distributorsWithAll = _.union(
                            [
                                {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )

                        if $scope.distributors.length > 0
                            if $scope.isDisplayDistributor()
                                $scope.filter.distributorId = $scope.distributors[0].id
                            else if $scope.isDisplayDistributorAll()
                                $scope.filter.distributorWithAllId = 'all'
                )

        $scope.addInitFunction(->
            if not $scope.filter.reportType?
                $scope.filter.reportType = 'daily'

            if $scope.filter.fromDate? and $scope.filter.toDate?
                $scope.fromDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.fromDate))
                $scope.toDate = $scope.createDatePickerModel(globalUtils.parseIsoDate($scope.filter.toDate))
                if not checkDate() or not checkDateDuration()
                    $state.go('404')
            else
                $scope.fromDate = $scope.createDatePickerModel(new Date())
                $scope.toDate = $scope.createDatePickerModel(new Date())

#            $scope.filter.distributorWithAllId = 'all'

            if $scope.isUseRegionFilter()
                LoadingUtilsService.loadAccessibleRegions(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('regions')
                    (list) ->
                        $scope.regions = list
                        if not $scope.filter.regionId? and $scope.regions.length > 0
                            $scope.filter.regionId = $scope.regions[0].id

                        LoadingUtilsService.loadDistributorByRegion(
                            $scope.who
                            $scope.filter.regionId
                            $scope.loadStatus.getStatusByDataName('distributors')
                            (list) ->
                                $scope.distributors = list
                                $scope.distributorsWithAll = _.union(
                                    [
                                        {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                                    ]
                                    list
                                )

                                if $scope.distributors.length > 0 && not $scope.filter.distributorId?
                                    if $scope.isDisplayDistributor()
                                        $scope.filter.distributorId = $scope.distributors[0].id
                                    else if $scope.isDisplayDistributorAll()
                                        $scope.filter.distributorWithAllId = 'all'
                        )
                )
            else if $scope.isUseDistributorFilter()
                LoadingUtilsService.loadDistributors(
                    $scope.who
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = list
                        $scope.distributorsWithAll = _.union(
                            [
                                {id: 'all', name: '-- ' + $filter('translate')('all') + ' --'}
                            ]
                            list
                        )
                )
        )

        $scope.init()
])

.controller('VisitReportDetailCtrl', [
    '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
    '$filter', 'toast', '$state', '$stateParams'
    ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state, $stateParams) ->
        $scope.title = '' # UPDATE LATER
        $scope.defaultBackState = 'visit-report'
        tooltip = (label, xval, yval, flotItem) -> label + ' - ' + $scope.nameMap[xval - 1] + ': ' + $filter('number')(yval, 0)

        CtrlInitiatorService.initCanBackViewCtrl($scope)
        CtrlInitiatorService.initFilterViewCtrl($scope)

        afterLoad = ->
            nbVisits = []
            nbVisitErrorDurations = []
            nbVisitErrorPositions = []
            ticks = []
            $scope.nameMap = []

            $scope.total = {nbVisit: 0, nbVisitErrorDuration: 0, nbVisitErrorPosition: 0}
            for dto, i in $scope.records
                nbVisits.push([(i + 1), dto.visitResult.nbVisit])
                nbVisitErrorDurations.push([(i + 1), dto.visitResult.nbVisitErrorDuration])
                nbVisitErrorPositions.push([(i + 1), dto.visitResult.nbVisitErrorPosition])

                $scope.total.nbVisit += dto.visitResult.nbVisit
                $scope.total.nbVisitErrorDuration += dto.visitResult.nbVisitErrorDuration
                $scope.total.nbVisitErrorPosition += dto.visitResult.nbVisitErrorPosition

                if $scope.filter.reportType is 'daily'
                    if (i + 1) is 1 or (i + 1) % 5 is 0
                        ticks.push([(i + 1), '' + (i + 1)])
                    else
                        ticks.push([(i + 1), ''])
                    $scope.nameMap.push((i + 1) + '/' + moment($scope.date).format('MM/YYYY'))
                else if $scope.filter.reportType is 'distributor'
                    ticks.push([(i + 1), globalUtils.getChartLabel(dto.name)])
                    $scope.nameMap.push(globalUtils.escapeHTML(dto.name))
                else if $scope.filter.reportType is 'salesman'
                    ticks.push([(i + 1), globalUtils.getChartLabel(dto.fullname)])
                    $scope.nameMap.push(globalUtils.escapeHTML(dto.fullname))

            $scope.chart.data = [
                data: nbVisits
                label: $filter('translate')('visit')
            ,
                data: nbVisitErrorPositions
                label: $filter('translate')('visit.error.duration')
            ]

            for data, i in $scope.chart.data
                if i is 0 and nbVisits.length < 20
                    data.bars = {
                        show: true
                        fill: true
                        barWidth: .1
                        align: 'center'
                    }
                else
                    data.lines = {
                        show: true
                        fill: false
                        fillColor: {colors: [{opacity: 0}, {opacity: 0.3}]}
                    }
                    data.points = {
                        show: true
                        lineWidth: 2
                        fill: false
                        fillColor: "#ffffff"
                        symbol: "circle"
                        radius: 5
                    }
                    data.lines.fill = true if i is 0

            $scope.chart.options.xaxis = {autoscaleMargin: .10, ticks: ticks}

        $scope.isDisplayChart = -> $scope.chart.data? and $scope.chart.data[0].data? and $scope.chart.data[0].data.length > 1

        # LOADING
        $scope.reloadData = ->
            params =
                who: $scope.who
                category: 'report'
                subCategory: 'visit'
                fromDate: $scope.filter.fromDate
                toDate: $scope.filter.toDate

            if $scope.filter.reportType is 'daily'
                params.param = 'daily'
                params.distributorId = $scope.filter.distributorId
                params.regionId = $scope.filter.regionId
            else if $scope.filter.reportType is 'distributor'
                params.param = 'by-distributor'
            else if $scope.filter.reportType is 'salesman'
                params.param = 'by-salesman'
                params.distributorId = $scope.filter.distributorId

            CtrlUtilsService.loadSingleData(params
                $scope.loadStatus.getStatusByDataName('data')
                (data) ->
                    $scope.records = data.list
                    afterLoad()
                (error) ->
                    if (error.status is 400)
                        $state.go('404')
                    else
                        toast.logError($filter('translate')('loading.error'))
            )

        checkFilter = ->
            if $scope.filter.reportType is 'salesman' and not $scope.filter.distributorId?
                return false

            return true

        $scope.addInitFunction(->
            $scope.chart = {}
            $scope.chart.options = angular.copy(chartOptions)
            $scope.chart.options.tooltip.content = tooltip

            if $scope.filter? and $scope.filter.fromDate? and $scope.filter.toDate?
                $scope.fromDate = globalUtils.parseIsoDate($scope.filter.fromDate)
                $scope.toDate = globalUtils.parseIsoDate($scope.filter.toDate)

                if not checkFilter()
                    $state.go('404')
                else
                    reportTitle = null
                    if $scope.filter.reportType is 'daily'
                        reportTitle = $filter('translate')('visit.report.daily.title')
                    else if $scope.filter.reportType is 'distributor'
                        reportTitle = $filter('translate')('visit.report.distributor.title')
                    else if $scope.filter.reportType is 'salesman'
                        reportTitle = $filter('translate')('visit.report.salesman.title')

                    $scope.title = reportTitle

                    $scope.reloadData()
            else
                $state.go('404')
        )

        $scope.init()
])
