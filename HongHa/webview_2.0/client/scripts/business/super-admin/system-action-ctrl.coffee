angular.module('app')

.controller('SystemActionCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService'
        ($scope, CtrlUtilsService, CtrlInitiatorService) ->
            $scope.title = 'system.action.title'

            CtrlInitiatorService.initBasicViewCtrl($scope)

            $scope.resetCache = ->
                status = $scope.loadStatus.getStatusByDataName('data')

                params =
                    who: $scope.who
                    category: 'system'
                    action: 'reset-cache'

                CtrlUtilsService.doPut(params, status, null, null, null, 'reset.cache')

            $scope.generateBigData = ->
                status = $scope.loadStatus.getStatusByDataName('data')

                params =
                    who: $scope.who
                    category: 'system'
                    action: 'generate-big-data'

                CtrlUtilsService.doPost(params, status, null, null, null, 'generate.big.data')

            $scope.init()
    ])
