'use strict'

angular.module('app')

.controller('VisitTodayCtrl', [
        '$scope', 'CtrlUtilsService', 'CtrlInitiatorService', 'CtrlCategoryInitiatorService', 'LoadingUtilsService',
        '$filter', 'toast', '$state', '$stateParams'
        ($scope, CtrlUtilsService, CtrlInitiatorService, CtrlCategoryInitiatorService, LoadingUtilsService,
        $filter, toast, $state, $stateParams) ->
            $scope.title = 'visit.today.title'
            $scope.isUseDistributorFilter = -> CtrlUtilsService.getWho() isnt 'distributor'
            $scope.isUseRegionFilter = -> $scope.who is 'advance_supervisor' and $scope.isUseDistributorFilter()
            $scope.mustSelectDistributor = -> $scope.isUseDistributorFilter() and not $scope.filter.distributorId?

            $scope.getDurationDisplay = (duration) ->
                if duration?
                    minute = parseInt(duration / 60000)
                    second = parseInt((duration / 1000) % 60)
                    return minute + 'm ' + second + 's'
                else return ''

            $scope.getStartEndDisplay = (record) ->
                if record? and record.startTime? and record.endTime?
                    return $filter('isoTime')(record.startTime) + ' - ' + $filter('isoTime')(record.endTime)
                else return ''

            $scope.isDistanceNormal = (record) -> record.locationStatus? and record.locationStatus is 0
            $scope.isDistanceTooFar = (record) -> record.locationStatus? and record.locationStatus is 1
            $scope.isLocationSalesmanUndefined = (record) -> record.locationStatus? and record.locationStatus is 2
            $scope.isLocationCustomerUndefined = (record) -> record.locationStatus? and record.locationStatus is 3
            $scope.getDistanceDisplay = (distance) -> if distance? then $filter('number')(distance * 1000, 0) + ' m' else ''

            $scope.isNoOrder = (record) -> not (record.hasOrder? and record.hasOrder)
            $scope.isOrderApproved = (record) -> not $scope.isNoOrder(record) and record.approveStatus is 1
            $scope.isOrderPending = (record) -> not $scope.isNoOrder(record) and record.approveStatus is 0
            $scope.isOrderRejected = (record) -> not $scope.isNoOrder(record) and record.approveStatus is 2

            $scope.goToDetail = (record) ->  $state.go('visit-detail', { id: record.id, filter: $stateParams.filter })

            $scope.filterChange = ->
                $scope.pagingData.currentPage = 1
                $scope.pageChange()


            # FILTER
            $scope.regionFilterChange = ->
                $scope.filter.distributorId = null
                LoadingUtilsService.loadDistributorByRegion(
                    $scope.who
                    $scope.filter.regionId
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = list
                        if  $scope.distributors.length > 0
                            $scope.filter.distributorId = $scope.distributors[0].id
                        $scope.distributorFilterChange()
                )

            $scope.distributorFilterChange = ->
                $scope.filter.salesmanId = null
                LoadingUtilsService.loadSalesmenByDistributor(
                    $scope.who
                    $scope.filter.distributorId
                    $scope.loadStatus.getStatusByDataName('salesmen')
                    (list) ->
                        $scope.salesmen = _.union(
                            [
                                { id: 'all', fullname: '-- ' + $filter('translate')('all') + ' --'  }
                            ]
                            list
                        )
                        # CHECK DISTRIBUTOR
                        $scope.filter.salesmanId = $scope.filter.salesmanId ? 'all'
                        $scope.filterChange()
                )

            $scope.reloadDistributor = ()->
                LoadingUtilsService.loadDistributorByRegion(
                    $scope.who
                    $scope.filter.regionId
                    $scope.loadStatus.getStatusByDataName('distributors')
                    (list) ->
                        $scope.distributors = list

                        if  $scope.distributors.length > 0 && not $scope.filter.distributorId?
                            $scope.filter.distributorId = $scope.distributors[0].id

                        $scope.reloadSalesman()
                )

            $scope.reloadSalesman = ()->
                LoadingUtilsService.loadSalesmenByDistributor(
                    $scope.who
                    $scope.filter.distributorId
                    $scope.loadStatus.getStatusByDataName('salesmen')
                    (list) ->
                        $scope.salesmen = _.union(
                            [
                                { id: 'all', fullname: '-- ' + $filter('translate')('all') + ' --'  }
                            ]
                            list
                        )
                        if not $scope.filter.salesmanId?
                            $scope.filter.salesmanId = $scope.salesmen[0].id
                        $scope.reloadData()
                )


            # LOADING
            $scope.getReloadDataParams = (params) ->
                params.category = 'visit'
                params.subCategory = 'today'
                if $scope.isUseRegionFilter()
                    params.regionId = $scope.filter.regionId

                if $scope.isUseDistributorFilter()
                    params.distributorId = $scope.filter.distributorId

                if $scope.filter.salesmanId? and $scope.filter.salesmanId isnt 'all'
                    params.salesmanId = $scope.filter.salesmanId

                # Load Summary
                summaryParams = angular.copy(params)
                summaryParams.param = 'summary'
                summaryLoadStatus = $scope.loadStatus.getStatusByDataName('summary')
                $scope.summary = {}
                summaryCallback = (data) -> $scope.summary = data
                CtrlUtilsService.loadSingleData(summaryParams, summaryLoadStatus, summaryCallback, null)
                return params

            CtrlInitiatorService.initPagingFilterViewCtrl($scope)

            $scope.addInitFunction( ->
                if $scope.isUseRegionFilter()
                    LoadingUtilsService.loadAccessibleRegions(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('regions')
                        (list) ->
                            $scope.regions = list
                            if not $scope.filter.regionId? and $scope.regions.length > 0
                                $scope.filter.regionId = $scope.regions[0].id
                            $scope.reloadDistributor()
                    )
                else if $scope.isUseDistributorFilter()
                    LoadingUtilsService.loadDistributors(
                        $scope.who
                        $scope.loadStatus.getStatusByDataName('distributors')
                        (list) ->
                            $scope.distributors = list
                            if not $scope.filter.distributorId? and $scope.distributors.length > 0
                                $scope.filter.distributorId = $scope.distributors[0].id
                            $scope.reloadSalesman()
                    )

                else  $scope.reloadData()
            )

            $scope.init()

])
