'use strict'

angular.module('app')

.controller('PhotoPopupCtrl', [
    '$scope', '$modalInstance', 'title', 'subTitle', 'photoLink', '$filter'
    ($scope, $modalInstance, title, subTitle, photoLink, $filter) ->
        $scope.title = if title? then $filter('translate')(title) else null
        $scope.subTitle = if subTitle? then $filter('translate')(subTitle) else null
        $scope.photoLink = photoLink

        $scope.isDisplayFooter = -> $scope.isDisplayTitle() or $scope.isDisplaySubTitle()
        $scope.isDisplayTitle = -> $scope.title?
        $scope.isDisplaySubTitle = -> $scope.subTitle?

        $scope.cancel = -> $modalInstance.dismiss('cancel')
])
