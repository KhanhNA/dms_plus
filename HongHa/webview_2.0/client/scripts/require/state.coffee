'use strict'

angular.module('app.state')

.constant('STATES',
    [
        # SUPER ADMIN
            name: 'client'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'ClientCtrl'
            roles: ['SUPER', 'SUPPORTER']
        ,
            name: 'client-detail'
            templateUrl: 'views/business/super-admin/client-detail.html'
            controller: 'ClientDetailCtrl'
            roles: ['SUPER', 'SUPPORTER']
        ,
            name: 'import-master-data'
            templateUrl: 'views/business/import/import-master-data.html'
            controller: 'ImportMasterDataCtrl'
            roles: ['SUPER', 'SUPPORTER']
        ,
            name: 'system-config'
            templateUrl: 'views/business/super-admin/system-config.html'
            controller: 'SystemConfigCtrl'
            roles: ['SUPER']
        ,
            name: 'system-action'
            templateUrl: 'views/business/super-admin/system-action.html'
            controller: 'SystemActionCtrl'
            roles: ['SUPER']
        ,
        # DASHBOARD
            name: 'dashboard'
            templateUrl: 'views/business/dashboard/dashboard.html'
            controller: 'DashboardCtrl'
            roles: ['AD', 'OBS', 'SUP','A_SUP']
        ,
        # SYSTEM
            name: 'user'
            templateUrl: 'views/business/system/user.html'
            controller: 'UserCtrl'
            roles: ['AD']
        ,
            name: 'user-detail'
            templateUrl: 'views/business/system/user-detail.html'
            controller: 'UserDetailCtrl'
            roles: ['AD']
        ,
            name: 'user-observer-distributors'
            templateUrl: 'views/business/system/user-observer-distributors.html'
            controller: 'UserObserverDistributorsCtrl'
            roles: ['AD']
        ,
            name: 'user-advance-supervisor-regions'
            templateUrl: 'views/business/system/user-advance-supervisor-regions.html'
            controller: 'UserAdvanceSupervisorRegionsCtrl'
            roles: ['AD']
        ,
            name: 'distributor'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'DistributorCtrl'
            roles: ['AD']
        ,
            name: 'distributor-detail'
            templateUrl: 'views/business/system/distributor-detail.html'
            controller: 'DistributorDetailCtrl'
            roles: ['AD']
        ,
            name: 'client-config'
            templateUrl: 'views/business/system/client-config.html'
            controller: 'ClientConfigCtrl'
            roles: ['AD']
        ,
            name: 'calendar-config'
            templateUrl: 'views/business/system/calendar-config.html'
            controller: 'CalendarConfigCtrl'
            roles: ['AD']
        ,
            name: 'client-action'
            templateUrl: 'views/business/system/client-action.html'
            controller: 'ClientActionCtrl'
            roles: ['AD']
        ,
        # REGION
            name : 'region-hierarchy'
            templateUrl:'views/business/region/region-hierarchy.html'
            controller : 'RegionHierarchyCtrl'
            roles :['AD']
        ,
            name : 'user-advance-supervisor-region-distributors'
            templateUrl:'views/business/region/region-distributors.html'
            controller : 'RegionDistributorsCtrl'
            roles :['AD']
        ,
        # PRODUCT
            name: 'uom'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'UomCtrl'
            roles: ['AD']
        ,
            name: 'product-category'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'ProductCategoryCtrl'
            roles: ['AD']
        ,
            name: 'product'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'ProductCtrl'
            roles: ['AD']
        ,
            name: 'product-detail'
            templateUrl: 'views/business/product/product-detail.html'
            controller: 'ProductDetailCtrl'
            roles: ['AD']
        ,
            name: 'import-product'
            templateUrl: 'views/business/import/import-product.html'
            controller: 'ImportProductCtrl'
            roles: ['AD']
        ,
            name: 'product-important'
            templateUrl: 'views/business/product/product-important-list.html'
            controller: 'ProductImportantListCtrl'
            roles: ['AD']
        ,
            name: 'promotion'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'PromotionCtrl'
            roles: ['AD']
            modules: ['PROMOTION']
        ,
            name: 'promotion-detail'
            templateUrl: 'views/business/product/promotion-detail.html'
            controller: 'PromotionDetailCtrl'
            roles: ['AD']
            modules: ['PROMOTION']
        ,
            name: 'import-promotion'
            templateUrl: 'views/business/import/import-promotion.html'
            controller: 'ImportPromotionCtrl'
            roles: ['AD']
            modules: ['PROMOTION']
        ,
        # CUSTOMER
            name: 'customer-type'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'CustomerTypeCtrl'
            roles: ['AD']
        ,
            name: 'area'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'AreaCtrl'
            roles: ['AD']
        ,
            name: 'customer'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'CustomerCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
            name: 'customer-detail'
            templateUrl: 'views/business/customer/customer-detail.html'
            controller: 'CustomerDetailCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
            name: 'import-customer'
            templateUrl: 'views/business/import/import-customer.html'
            controller: 'ImportCustomerCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
        # CUSTOMER SCHEDULE
            name: 'route'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'RouteCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
            name: 'route-detail'
            templateUrl: 'views/business/schedule/route-detail.html'
            controller: 'RouteDetailCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
            name: 'customer-schedule'
            templateUrl: 'views/business/schedule/customer-schedule.html'
            controller: 'CustomerScheduleCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
            name: 'import-customer-schedule'
            templateUrl: 'views/business/import/import-customer-schedule.html'
            controller: 'ImportCustomerScheduleCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
        # SURVEY
            name: 'survey'
            templateUrl: 'CATEGORY_LIST_TEMPLATE'
            controller: 'SurveyCtrl'
            roles: ['AD']
            modules: ['SURVEY']
        ,
            name: 'survey-detail'
            templateUrl: 'views/business/survey/survey-detail.html'
            controller: 'SurveyDetailCtrl'
            roles: ['AD']
            modules: ['SURVEY']
        ,
        # TARGET
            name: 'target'
            templateUrl: 'views/business/target/target.html'
            controller: 'TargetCtrl'
            roles: ['SUP', 'A_SUP']
        ,
            name: 'target-detail'
            templateUrl: 'views/business/target/target-detail.html'
            controller: 'TargetDetailCtrl'
            roles: ['SUP', 'A_SUP']
        ,
        # VISIT TODAY
            name: 'visit-today'
            templateUrl: 'views/business/visit-today/visit-today.html'
            controller: 'VisitTodayCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
        ,
            name: 'visit-today-map'
            templateUrl: 'views/business/visit-today/visit-today-map.html'
            controller: 'VisitTodayMapCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
        ,
            name: 'visit-detail'
            templateUrl: 'views/business/visit-today/visit-detail.html'
            controller: 'VisitDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
        # ORDER CREATING
            name: 'order-creating'
            templateUrl: 'views/business/order/order-creating.html'
            controller: 'OrderCreatingCtrl'
            roles: ['DIS']
        ,
        # ORDER APPROVAL
            name: 'order-pending'
            templateUrl: 'views/business/approval/order-pending.html'
            controller: 'OrderPendingCtrl'
            roles: ['AD', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'order-approval'
            templateUrl: 'views/business/approval/order-approval.html'
            controller: 'OrderApprovalCtrl'
            roles: ['AD', 'SUP', 'DIS', 'A_SUP']
        ,
        # CUSTOMER APPROVAL
            name: 'customer-pending'
            templateUrl: 'views/business/approval/customer-pending.html'
            controller: 'CustomerPendingCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
            name: 'customer-approval'
            templateUrl: 'views/business/approval/customer-approval.html'
            controller: 'CustomerApprovalCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
            name: 'customer-schedule-single'
            templateUrl: 'views/business/schedule/customer-schedule-single.html'
            controller: 'CustomerScheduleSingleCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
        # FEEDBACK
            name: 'feedback'
            templateUrl: 'views/business/feedback/feedback.html'
            controller: 'FeedbackCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'feedback-detail'
            templateUrl: 'views/business/feedback/feedback-detail.html'
            controller: 'FeedbackDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
        # ORDER
            name: 'order-search'
            templateUrl: 'views/business/history/order-search.html'
            controller: 'OrderSearchCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'order-list'
            templateUrl: 'views/business/history/order-list.html'
            controller: 'OrderListCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'order-detail'
            templateUrl: 'views/business/history/order-detail.html'
            controller: 'OrderDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
        # VISIT
            name: 'visit-search'
            templateUrl: 'views/business/history/visit-search.html'
            controller: 'VisitSearchCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
        ,
            name: 'visit-list'
            templateUrl: 'views/business/history/visit-list.html'
            controller: 'VisitListCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
        ,
        # VISIT PHOTO
            name: 'visit-photo'
            templateUrl: 'views/business/visit-photo/visit-photo.html'
            controller: 'VisitPhotoCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
        # CHECK IN
            name: 'check-in-search'
            templateUrl: 'views/business/check-in/check-in-search.html'
            controller: 'CheckInSearchCtrl'
            roles: ['AD', 'A_SUP']
            modules: ['CHECK_IN']
        ,
            name: 'check-in-list'
            templateUrl: 'views/business/check-in/check-in-list.html'
            controller: 'CheckInListCtrl'
            roles: ['AD', 'A_SUP']
            modules: ['CHECK_IN']
        ,
            name: 'check-in-timeline'
            templateUrl: 'views/business/check-in/check-in-timeline.html'
            controller: 'CheckInTimelineCtrl'
            roles: ['AD', 'A_SUP']
            modules: ['CHECK_IN']
        ,
            name: 'check-in-detail'
            templateUrl: 'views/business/check-in/check-in-detail.html'
            controller: 'CheckInDetailCtrl'
            roles: ['AD','A_SUP']
            modules: ['CHECK_IN']
        ,
        # SALES REPORT
            name: 'sales-report'
            templateUrl: 'views/business/report/sales/sales-report.html'
            controller: 'SalesReportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'sales-report-daily'
            templateUrl: 'views/business/report/sales/sales-report-daily.html'
            controller: 'SalesReportDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'sales-report-distributor'
            templateUrl: 'views/business/report/sales/sales-report-distributor.html'
            controller: 'SalesReportDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'sales-report-product'
            templateUrl: 'views/business/report/sales/sales-report-product.html'
            controller: 'SalesReportDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'sales-report-salesman'
            templateUrl: 'views/business/report/sales/sales-report-salesman.html'
            controller: 'SalesReportDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
        # VISIT REPORT
            name: 'visit-report'
            templateUrl: 'views/business/report/visit/visit-report.html'
            controller: 'VisitReportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
        ,
            name: 'visit-report-daily'
            templateUrl: 'views/business/report/visit/visit-report-daily.html'
            controller: 'VisitReportDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
        ,
            name: 'visit-report-distributor'
            templateUrl: 'views/business/report/visit/visit-report-distributor.html'
            controller: 'VisitReportDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
        ,
            name: 'visit-report-salesman'
            templateUrl: 'views/business/report/visit/visit-report-salesman.html'
            controller: 'VisitReportDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
        ,
        # SURVEY REPORT
            name: 'survey-report'
            templateUrl: 'views/business/report/survey/survey-report.html'
            controller: 'SurveyReportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
            modules: ['SURVEY']
        ,
            name: 'survey-report-detail'
            templateUrl: 'views/business/report/survey/survey-report-detail.html'
            controller: 'SurveyReportDetailCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
            modules: ['SURVEY']
        ,
        # PERFORMANCE REPORT
            name: 'performance-report'
            templateUrl: 'views/business/report/performance/performance-report-daily.html'
            controller: 'PerformanceReportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'performance-report-salesman'
            templateUrl: 'views/business/report/performance/performance-report-salesman.html'
            controller: 'PerformanceReportSalesmanCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'performance-report-daily-salesman'
            templateUrl: 'views/business/report/performance/performance-report-daily-salesman.html'
            controller: 'PerformanceReportDailySalesmanCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
        # INVENTORY REPORT
            name: 'inventory-report'
            templateUrl: 'views/business/report/inventory/inventory-report.html'
            controller: 'InventoryReportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
            modules: ['INVENTORY']
        ,
            name: 'inventory-report-distributor'
            templateUrl: 'views/business/report/inventory/inventory-report-distributor.html'
            controller: 'InventoryReportDistributorCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
            modules: ['INVENTORY']
        ,
            name: 'inventory-report-product'
            templateUrl: 'views/business/report/inventory/inventory-report-product.html'
            controller: 'InventoryReportProductCtrl'
            roles: ['AD', 'OBS', 'SUP', 'A_SUP']
            modules: ['INVENTORY']
        ,
        # EXPORT
            name: 'order-export'
            templateUrl: 'views/business/export/order-export.html'
            controller: 'OrderExportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'visit-export'
            templateUrl: 'views/business/export/visit-export.html'
            controller: 'VisitExportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'feedback-export'
            templateUrl: 'views/business/export/feedback-export.html'
            controller: 'FeedbackExportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'exchange-return-export'
            templateUrl: 'views/business/export/exchange-return-export.html'
            controller: 'ExchangeReturnExportCtrl'
            roles: ['AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
            modules: ['EXCHANGE_RETURN']
        ,
            name: 'check-in-export'
            templateUrl: 'views/business/export/check-in-export.html'
            controller: 'CheckInExportCtrl'
            roles: ['AD', 'A_SUP']
            modules: ['CHECK_IN']
        ,
            name: 'product-important-export'
            templateUrl: 'views/business/export/product-important-export.html'
            controller: 'ProductImportantExportCtrl'
            roles: ['OBS', 'SUP', 'DIS', 'A_SUP']
        ,
            name: 'customer-export'
            templateUrl: 'views/business/export/customer-export.html'
            controller: 'CustomerExportCtrl'
            roles: ['AD', 'SUP', 'A_SUP']
        ,
        # CHANGE PASSWORD
            name: 'change-password'
            templateUrl: 'views/system/change-password.html'
            controller: 'ChangePasswordCtrl'
            roles: ['SUPPORTER', 'AD', 'OBS', 'SUP', 'DIS', 'A_SUP']
        ,
        # DISTRIBUTOR PRICE LIST
#            name: 'distributor-price-list'
#            templateUrl: 'views/business/price-list/distributor-price-list.html'
#            controller: 'DistributorPriceListCtrl'
#            roles: ['DIS']
#        ,
#            name: 'import-price-list'
#            templateUrl: 'views/business/import/import-distributor-price-list.html'
#            controller: 'ImportDistributorPriceListCtrl'
#            roles: ['DIS']
#        ,
        # VAN SALES
            name: 'van-sales'
            templateUrl: 'views/business/van-sales/van-sales.html'
            controller: 'VanSalesCtrl'
            roles: ['DIS']
        ,
        # INVENTORY
            name: 'inventory'
            templateUrl: 'views/business/inventory/inventory-list.html'
            controller: 'InventoryCtrl'
            roles: ['DIS']
            modules: ['INVENTORY']
        ,
            name: 'inventory-detail'
            templateUrl: 'views/business/inventory/inventory-detail.html'
            controller: 'InventoryDetailCtrl'
            roles: ['DIS']
            modules: ['INVENTORY']
        ,
            name: 'import-inventory'
            templateUrl: 'views/business/import/import-inventory.html'
            controller: 'ImportInventoryCtrl'
            roles: ['DIS']
            modules: ['INVENTORY']
    ]
)
