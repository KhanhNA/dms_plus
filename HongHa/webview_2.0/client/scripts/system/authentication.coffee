'use strict'

typeIsArray = Array.isArray || (value) -> return {}.toString.call(value) is '[object Array]'

angular.module('app.authentication', [])

.provider 'Authentication', ->
    configs =
        'baseUrl': null
        'clientId': null
        'scope': null
        'stateLength': 5
        'autoRefresh': true
        'tokenMinLiveTime': 5 * 60 * 1000
        'tokenCheckInterval': 5 * 1000
        'refreshMaxWait': 2 * 1000
        'refreshCheckLockInterval': 2 * 1000
        'deniedRoles': null
        'noAuthStates': null

    configure: (params) ->
        if not params?
            throw new Error('Invalid configuration')

        if not params.baseUrl
            throw new Error('Parameter \'baseUrl\' must be configured')

        if not params.clientId
            throw new Error('Parameter \'clientId\' must be configured')

        if params.deniedRoles? and not typeIsArray params.deniedRoles
            throw new Error('\'deniedRoles\' must be an array')

        if params.noAuthStates? and not typeIsArray params.noAuthStates
            throw new Error('\'noAuthStates\' must be an array')

        angular.extend(configs, params)

    set: () ->
        if arguments.length != 1 and typeof arguments[0] == 'object'
            return this
        if typeof arguments[0] != 'string'
            return this
        value = if typeof arguments[1] == 'undefined' then null else arguments[1]
        key = arguments[0]
        configs[key] = value
        this

    $get: ['$http', '$rootScope', '$location', '$q', '$window', '$log', '$timeout', '$interval', '$state', ($http, $rootScope, $location, $q, $window, $log, $timeout, $interval, $state) ->
        updateProgressTask = undefined
        renewTokenTask = undefined
        currentRenewTokenTaskTimeout = 0
        verifyTokenPromise = null
        tokenVerified = false
        preventOtherLogin = false

        # Public methods
        saveAccessTokenFromUrl = () ->
            clearToken()

            search = $location.search()

            # Keep old UserInfo if token was set from auto_refresh
            if not search['auto_refresh']? or not search['auto_refresh']
                clearUserInfo()

            saveAccessToken(search)

            #remove access_token from url
            $location.search 'access_token', null
            $location.search 'token_type', null
            $location.search 'expires_in', null
            $location.search 'flag', null
            $location.search 'lang', null
            $location.search 'auto_refresh', null
            $location.search 'state', null

            # Self close if is an iframe
            if isOAuthFrame()
                $log.debug 'Token was renewed using iFrame'
                removeOAuthFrame()
            else
                $log.debug 'Token was saved'

        getAccessToken = () ->
            if getTokenExpiredIn() < configs.tokenMinLiveTime
                return undefined
            return $window.localStorage['access_token']

        verifyToken = () ->
            if not verifyTokenPromise?
                defer = $q.defer()
                if tokenVerified
                    defer.resolve()
                else
                    if not getAccessToken()?
                        defer.reject()
                        login()
                    else
                        $http.get(getVerifyTokenUrl())
                        .success((userInfo) ->
                            tokenVerified = true
                            #$rootScope.tokenVerified = true
                            setUserInfo userInfo
                            $log.debug('Token was verified')
                            # Broadcasting events
                            $rootScope.$broadcast 'TokenVerified'
                            defer.resolve userInfo
                            verifyTokenPromise = null
                        ).error (error) ->
                            tokenVerified = false
                            $log.debug 'Cannot verify token: ' + error
                            defer.reject error
                            verifyTokenPromise = null
                verifyTokenPromise = defer.promise

            verifyTokenPromise

        getUserInfo = () ->
            userInfo = $window.localStorage['userInfo']
            if !userInfo
                return null
            try
                return angular.fromJson(userInfo)
            catch e
                $window.localStorage['userInfo'] = null
            return null

        getRoleCode = () ->
            userInfo = getUserInfo()
            if userInfo?
                return userInfo['roleCode']
            return null

        getModules = () ->
            userInfo = getUserInfo()
            if userInfo?
                return userInfo['modules']
            return null

        getUserLanguage = () ->
            $window.localStorage['lang']

        setUserLanguage = (lang) ->
            $window.localStorage['lang'] = lang

        login = () ->
            clearUserInfo()
            clearToken()

            search = $location.search()
            if search.access_token?
                saveAccessTokenFromUrl()
                return

            # If already in login process, prevent to call login() again (so OAuth state not generate again,
            # sometime cause bug on safari)
            if preventOtherLogin? and preventOtherLogin
                return
            $window.location.href = buildAuthorizationUrl()
            preventOtherLogin = true

        logout = () ->
            accessToken = getAccessToken()
            lang = getUserLanguage()

            if accessToken
                revokeToken(accessToken).then(
                    () ->
                        $log.info 'Token revoked'
                    (e) ->
                        $log.error('Token revoke error', e)
                ).finally(
                    () ->
                        afterLogout(lang)
                )
            else
                afterLogout(lang)

        changePassword = (oldPassword, newPassword) ->
            defer = $q.defer()
            params = {
                newPassword: newPassword
                oldPassword: oldPassword
            }

            $http.put(getChangePasswordUrl(), params)
            .success((data) ->
                # Broadcasting events
                $rootScope.$broadcast 'PasswordChanged'
                defer.resolve data
            ).error (error) ->
                defer.reject error
            defer.promise

        isNoAuthPage = (toState) ->
            toState? and configs.noAuthStates? and toState.name in configs.noAuthStates

        isRoleDenied = () ->
            configs.deniedRoles? and isInAnyRole(configs.deniedRoles)

        isAuthenticated = () ->
            tokenVerified

        isInAnyRole = (roles) ->
            if not roles?
                return true

            roleToCheck = getRoleCode()

            if !isAuthenticated() or not roleToCheck
                return false

            for role in roles
                if role is roleToCheck then return true

            return false

        isOAuthFrame = () ->
            $window.parent and $window.parent.document.getElementById('oauth_frame')

        # Private methods
        saveAccessToken = (params) ->
            # Check if same state
            if not params['state']?
                $log.debug 'State not exist, token rejected'
                return

            # Raw state parameter contains both state to verify and returning url
            rawState = decodeURIComponent(params['state'])
            # State to verify
            state = rawState.substring(0, configs.stateLength)
            # Returning url
            returnUrl = rawState.substring(configs.stateLength + 1)

            storedState = $window.localStorage['state'];
            $window.localStorage.removeItem 'state'

            # Verify with stored state
            if state isnt storedState
                $log.debug 'State not match, token rejected'
                return


            $window.localStorage['access_token'] = params['access_token']
            $window.localStorage['token_type'] = params['token_type']
            if params['expires_in']
                expiryDate = new Date((new Date).getTime() + params['expires_in'] * 1000)
                $window.localStorage['expires_in'] = params['expires_in']
                $window.localStorage['expiry_date'] = expiryDate
            else
                $window.localStorage.removeItem 'expires_in'
                $window.localStorage.removeItem 'expiry_date'

            if !params['auto_refresh']
                if params['lang'] then $window.localStorage['lang'] = params['lang'].toLowerCase()

                # Check token validity with configured parameter
                if configs.autoRefresh
                    expiredIn = getTokenExpiredIn()
                    if expiredIn <= configs.tokenMinLiveTime
                        $log.error("Token validity must be > tokenMinLiveTime")
                        configs.tokenMinLiveTime = parseInt(expiredIn / 3)
                        $log.error("The tokenMinLiveTime parameter was automatic set to " + configs.tokenMinLiveTime)

                # Schedule to refresh
                setTimeoutToRefresh()
                # Redirect
                if returnUrl?
                    $location.url(returnUrl)

        revokeToken = (token) ->
            url = joinUrl(getRevokeTokenUrl(), '/' + token)
            request =
                method: 'DELETE'
                url: url
                headers:
                    'Authorization': 'Bearer ' + token

            $http(request)

        afterLogout = (lang) ->
            clearToken()
            clearUserInfo()
            # Redirect to logout url
            # Passing current user's language to logout url, so the auth server can decide to switch the
            # language of login page to current user's language
            if lang?
                $window.location.href = addParamToUrl(getLogoutUrl(), 'lang', lang)
            else
                $window.location.href = getLogoutUrl()


        clearToken = () ->
            $window.localStorage.removeItem 'access_token'
            $window.localStorage.removeItem 'expires_in'
            $window.localStorage.removeItem 'expiry_date'
            $window.localStorage.removeItem 'token_type'

        setUserInfo = (userInfo) ->
            $window.localStorage['userInfo'] = angular.toJson userInfo

        clearUserInfo = () ->
            $window.localStorage.removeItem 'userInfo'
            $window.localStorage.removeItem 'lang'

        getTokenExpiredIn = () ->
            expiryDate = if angular.isUndefined($window.localStorage['expiry_date']) then null else Date.parse($window.localStorage['expiry_date'])
            if expiryDate != null then expiryDate - (new Date).getTime() else 0

        renewTokenAfter = (timeout) ->
            if timeout is 0
                renewToken()
            else
                if renewTokenTask?
                    if timeout isnt currentRenewTokenTaskTimeout
                        $interval.cancel(renewTokenTask)
                        currentRenewTokenTaskTimeout = timeout
                        renewTokenTask = $interval (() ->
                            renewToken()
                        ), timeout
                else
                    currentRenewTokenTaskTimeout = timeout
                    renewTokenTask = $interval (() ->
                        renewToken()
                    ), timeout

        renewToken = ->
            tokenExpiredIn = getTokenExpiredIn()
            # If token already remove, just stop
            if not tokenExpiredIn?
                return

            # Check if token already renewed or not expire yet.
            # If token not expire yet, schedule to check after check_token_interval.
            if tokenExpiredIn > configs.tokenMinLiveTime
                renewTokenAfter(configs.tokenCheckInterval)
                return

            # Check lock, if locked, check last update time
            if $window.localStorage['refresh_locked']
                currentTime = (new Date).getTime()
                lastUpdate = if $window.localStorage['refresh_last_update']? then Date.parse($window.localStorage['refresh_last_update']) else null
                if lastUpdate > 0
                    diff = currentTime - lastUpdate
                    # Another tab is holding lock and still working, schedule to check again
                    if diff > 0 and diff <= configs.refreshMaxWait
                        renewTokenAfter(configs.refreshCheckLockInterval)
                        return

            # Not locked by anyone (only one tab)
            # Or last update time is too old (the tab holding the lock may be closed)
            # So we acquire new lock for current tab
            # Acquired new lock and remove oauth iframe if exist
            $window.localStorage['refresh_locked'] = true
            $window.localStorage['refresh_last_update'] = new Date()
            removeOAuthFrame()
            if updateProgressTask?
                $interval.cancel updateProgressTask
                updateProgressTask = undefined

            # Update the last update continuously to keep the lock
            updateProgressTask = $interval((->
                $window.localStorage['refresh_last_update'] = new Date()
            ), 1000)

            iFrame = document.createElement('iframe')
            iFrame.setAttribute 'src', buildAuthorizationUrl('auto_refresh': 'true')
            iFrame.setAttribute 'id', 'oauth_frame'
            iFrame.style.display = 'none'
            document.body.appendChild iFrame

            $log.debug 'Refreshing access token using iFrame'

            # Continuous check if dead lock or timeout
            renewTokenAfter(configs.tokenCheckInterval)

        setTimeoutToRefresh = () ->
            if !configs.autoRefresh
                $log.debug 'Auto refresh token was turned off'
                return
            # OAuth iframe will be remove immediately after token was saved
            # So we no need set a schedule to renew token
            if isOAuthFrame()
                $log.debug 'Skip auto refresh token in OAuth iFrame'
                return
            # Continuous check token expire time
            renewTokenAfter(0)

        buildAuthorizationUrl = (extendParams) ->
            params =
                response_type: 'token'
                client_id: configs.clientId
                redirect_uri: getRedirectUrl()
                scope: configs.scope
                state: createState()

            params = angular.extend(params, extendParams)
            url = getAuthorizationUrl()

            angular.forEach params, (value, key) ->
                url = addParamToUrl(url, encodeURIComponent(key), encodeURIComponent(value))

            return url

        getRedirectUrl = () ->
            redirect_uri = $state.href('callback', {}, {absolute: true})
            redirect_uri + '?'

        createState = () ->
            # Returned state will include current url and state to verify
            url = $location.url()
            state = randomString(configs.stateLength)
            # Save to storage for further process
            $window.localStorage['state'] = state

            encodeURIComponent(state + '_' + url)

        removeOAuthFrame = () ->
            iFrame = $window.parent.document.getElementById('oauth_frame')
            if iFrame
                iFrame.parentNode.removeChild iFrame

        randomString = (length) ->
            text = ''
            possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
            i = 0
            while i < length
                text += possible.charAt(Math.floor(Math.random() * possible.length))
                i++
            return text

        endsWith = (str, suffix) ->
            str.indexOf(suffix, str.length - (suffix.length)) != -1

        addParamToUrl = (url, key, val) ->
            newParam = key + '=' + val
            params = '?' + newParam
            if not url
                return params

            # If the "search" string exists, then build params from it
            # Try to replace an existing instance
            params = url.replace(new RegExp('[?&]' + key + '[^&]*'), '$1' + newParam)
            # If nothing was replaced, then add the new param to the end
            if params is url
                if params.indexOf('?') == -1
                    return params + '?' + newParam
                if not endsWith(params, '&')
                    params += '&'
                return params + newParam

            return params

        getAuthorizationUrl = () ->
            return joinUrl(configs.baseUrl, '/oauth/authorize')

        getRevokeTokenUrl = () ->
            return joinUrl(configs.baseUrl, '/oauth/revoke')

        getVerifyTokenUrl = () ->
            return joinUrl(configs.baseUrl, '/oauth/userinfo')

        getChangePasswordUrl = () ->
            return joinUrl(configs.baseUrl, '/oauth/password')

        getLogoutUrl = () ->
            return joinUrl(configs.baseUrl, '/account/logout')

        joinUrl = (base, path) ->
            if endsWith(base, '/')
               return base.slice(0, -1) + path
            return base + path

        if not isOAuthFrame()
            wrap = (event) ->
                if event.key == 'access_token' and event.newValue != event.oldValue
                    oldToken = event.oldValue
                    newToken = event.newValue
                    # Token was renewed, cancel the lock
                    if updateProgressTask
                        $interval.cancel updateProgressTask
                        updateProgressTask = undefined
                        $window.localStorage['refresh_locked'] = undefined
                        $window.localStorage['refresh_last_update'] = undefined
                    # Broadcasting events
                    $rootScope.$broadcast 'TokenChanged', oldToken, newToken

            # Listening for token change
            if $window.addEventListener
                $window.addEventListener 'storage', wrap, false
            else
                $window.attachEvent 'onstorage', wrap
            # If access token already stored in localStorage, setup an schedule to refresh
            if getAccessToken()
                setTimeoutToRefresh()

        return {
            saveAccessTokenFromUrl: saveAccessTokenFromUrl
            getAccessToken: getAccessToken
            verifyToken: verifyToken
            getUserInfo: getUserInfo
            getRoleCode: getRoleCode
            getModules: getModules
            getUserLanguage: getUserLanguage
            setUserLanguage: setUserLanguage
            login: login
            logout: logout
            changePassword: changePassword
            isNoAuthPage: isNoAuthPage
            isAuthenticated: isAuthenticated
            isInAnyRole: isInAnyRole
            isRoleDenied: isRoleDenied
            isOAuthFrame: isOAuthFrame
        }
    ]

.factory('authorization', ['$rootScope', '$state', 'Authentication', ($rootScope, $state, Authentication) ->
        return {
            authorize: () ->
                toState = $rootScope.toState

                if not Authentication.isNoAuthPage(toState)
                    Authentication.verifyToken().then(() ->
                        isAuthenticated = Authentication.isAuthenticated()
                        # Should never happen
                        if not isAuthenticated
                            # User is not authenticated then redirect user to login page
                            Authentication.login()
                        else if Authentication.isRoleDenied()
                            $state.go('550')
                        else if toState.data.roles && toState.data.roles.length == 1 && toState.data.roles[0] is 'ANY'
                            # It's okay
                        else if toState.data.roles && toState.data.roles.length > 0 and not Authentication.isInAnyRole(toState.data.roles)
                            # User is signed in but not authorized for desired state
                            $state.go('404')

                    )
            }
    ]
)

.config [
    '$httpProvider', 'ADDRESS_BACKEND', 'ADDRESS_OAUTH', 'ADDRESS_WEBSOCKETS'
    ($httpProvider, ADDRESS_BACKEND, ADDRESS_OAUTH, ADDRESS_WEBSOCKETS) ->

        isBackendUrl = (url) ->
            if not url?
                return false
            return url.indexOf(ADDRESS_BACKEND) == 0 || url.indexOf(ADDRESS_OAUTH) == 0 || url.indexOf(ADDRESS_WEBSOCKETS) == 0

        $httpProvider.interceptors.push ['$injector', ($injector) ->

            request: (config) ->
                config.headers = config.headers or {}
                if isBackendUrl(config.url)
                    $injector.invoke [
                        'Authentication', (Authentication) ->
                            if Authentication.getAccessToken()?
                                config.headers.Authorization = 'Bearer ' + Authentication.getAccessToken()
                    ]
                return config

            responseError: (resp) ->
                # If token rejected, ask user to login
                if resp.status == 401
                    $injector.invoke [
                        'Authentication', (Authentication) ->
                            Authentication.login()
                    ]

                # May be server down
                if resp.status is -1 and isBackendUrl(resp.config.url)
                    $injector.invoke [
                        '$state', ($state) ->
                            $state.go('500')
                    ]

                return $injector.get('$q').reject(resp)
        ]
]

.run [
    '$rootScope', '$log', '$location', '$state', 'Authentication', 'authorization'
    ($rootScope, $log, $location, $state, Authentication, authorization) ->

        $rootScope.$on '$locationChangeStart', (event) ->
            search = $location.search()
            if search? and search.access_token?
                Authentication.saveAccessTokenFromUrl()
                event.preventDefault()

        $rootScope.$on '$stateChangeStart', (event, toState) ->

            # OAuth iframe will be remove immediately after token was saved
            # So we navigate to dummy state and check nothing
            if Authentication.isOAuthFrame()
                event.preventDefault()
                return

            $rootScope.toState = toState

            if not Authentication.isNoAuthPage(toState)
                if Authentication.getAccessToken()? and Authentication.getUserInfo()?
                    authorization.authorize()

]
