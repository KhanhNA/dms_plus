'use strict'

angular.module('app')
.controller('LangCtrl', [
    '$scope', '$translate', 'Authentication'
    ($scope, $translate, Authentication) ->

        # Get current language used by angular-translate
        $scope.lang = $translate.use()

        $scope.$watch(
            () ->
                $translate.use()
            (newLang) ->
                $scope.lang = newLang
        )

        # When use choose language from header menu
        $scope.setLang = (lang) ->
            $translate.use(lang)
            $scope.lang = lang
            Authentication.setUserLanguage(lang)

        $scope.getFlag = () ->
            lang = $scope.lang
            switch lang
                when 'vi' then return 'flags-vietnam'
                when 'zh' then return 'flags-china'
                when 'pt' then return 'flags-portugal'
                when 'es' then return 'flags-spain'
                else return 'flags-british'

        $scope.isSupportedLanguage = (lang) ->
            if Authentication?
                userInfo = Authentication.getUserInfo()
                if userInfo? and userInfo.languages?
                    return _.includes(userInfo.languages, lang)

            return false

        $scope.isShowLangInDropDown = (lang) ->
            if ($scope.isSupportedLanguage(lang))
                if $scope.lang isnt lang
                    return true
            return false
])
