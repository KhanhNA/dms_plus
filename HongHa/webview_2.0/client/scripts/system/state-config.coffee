'use strict'

angular.module('app.state', [])

.constant('CATEGORY_LIST_TEMPLATE', 'views/common/category-list.html')
.constant('CATEGORY_POPUP_TEMPLATE', 'views/common/category-popup.html')
.constant('CATEGORY_POPUP_CTRL', 'NameCategoryPopupCtrl')

.controller('HomeCtrl', [
        '$scope', '$state' ,'Authentication', 'HOME_STATE'
        ($scope, $state, Authentication, HOME_STATE) ->
            if Authentication?
                roleCode = Authentication.getRoleCode()
                if roleCode?
                    home = HOME_STATE[roleCode]
                    if home?
                        $state.go(home)
                    #else
                    #    $state.go('550')
                else
                    console.log('roleCode is null')
            else
                console.log('Authentication service is null')
])

.config [
    '$stateProvider', '$urlRouterProvider', 'STATES', 'CATEGORY_LIST_TEMPLATE'
    ($stateProvider, $urlRouterProvider, STATES, CATEGORY_LIST_TEMPLATE) ->
        $urlRouterProvider.when '', '/home'
        $urlRouterProvider.otherwise(($injector, $location) ->
            $state = $injector.get('$state')
            if $location.url() is '/' then $state.go('home') else $state.go('404')
        )

        # Allow any authenticated user
        $stateProvider.state('home', {
            url: '/home'
            controller: 'HomeCtrl'
            data: { roles: ['ANY'], modules: ['ANY'] }
            resolve: {
                authorize: ['authorization', (authorization) ->
                    return authorization.authorize()
                ]
            }
        })

        # Does not require authentication
        $stateProvider.state('callback', { url: '/callback' })
        $stateProvider.state '404', { url: '/404', templateUrl: 'views/pages/404.html' }
        $stateProvider.state '500', { url: '/500', templateUrl: 'views/pages/500.html' }
        $stateProvider.state '550', { url: '/550', templateUrl: 'views/pages/550.html' }

        getTemplateUrl = (url) ->
            switch url
                when 'CATEGORY_LIST_TEMPLATE' then CATEGORY_LIST_TEMPLATE
                else url

        addState = (state) ->
            if state? and state.name?
                url = (if state.url? then state.url else ('/' + state.name))
                $stateProvider.state state.name, {
                    url: url + '?filter&id&parent&parentFilter'
                    templateUrl: getTemplateUrl(state.templateUrl)
                    controller: state.controller
                    data: {
                        roles: state.roles
                        modules: state.modules
                    }
                    resolve: {
                        authorize: ['authorization', (authorization) ->
                            return authorization.authorize()
                        ]
                    }
                }

        if STATES? and STATES.length > 0
            addState(state) for state in STATES
]
