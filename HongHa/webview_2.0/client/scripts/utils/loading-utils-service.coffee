'use strict'

class LoadingUtilsService
    constructor: (@CtrlUtilsService) ->

    #  LOAD SUPERVISOR
    loadSupervisors: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'supervisor'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.fullname )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    #  LOAD region
    loadRegions: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'region'
            subCategory: 'all'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    loadAccessibleRegions : (who,loadStatusOfData,callback) ->
        params =
            who: who
            category : 'region'
            subCategory: 'all-region'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    loadDistributorByRegion : (who,regionId,loadStatusOfData,callback) ->
        params =
            who: who
            category : 'distributor'
            regionId : regionId

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    #   LOAD Advance Supervisor
    loadAdvanceSupervisors: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'advance-supervisor'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.fullname )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    #  LOAD Observers
    loadObservers: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'observer'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.fullname )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    #  LOAD PRODUCT CATEGORY
    loadProductCategories: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'product-category'
            subCategory: 'all'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    #  LOAD UOM
    loadUOMs: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'uom'
            subCategory: 'all'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    #  LOAD PRODUCT CATEGORY
    loadCustomerTypes: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'customer-type'
            subCategory: 'all'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    #  LOAD DISTRIBUTOR
    loadDistributors: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'distributor'
            subCategory: 'all'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    #  LOAD PRODUCT
    loadProductsByDistributor: (who, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'product'
            subCategory: 'all'

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    loadAreasByDistributor: (who, distributorId, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'area'
            subCategory: 'all'
            distributorId : distributorId

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    loadRoutesByDistributor: (who, distributorId, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'route'
            subCategory: 'all'
            distributorId : distributorId

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.name )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    loadSalesmenByDistributor: (who, distributorId, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'salesman'
            distributorId : distributorId

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.fullname )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

    loadStoreCheckersByDistributor: (who, distributorId, loadStatusOfData, callback) ->
        params =
            who: who
            category: 'store-checker'
            distributorId : distributorId

        _callback = (list, count) ->
            _list = _.sortBy(list, (o) ->  return o.fullname )
            if jQuery.isFunction(callback) then callback(_list, count)

        @CtrlUtilsService.loadListData(params, loadStatusOfData, _callback)

# **********************************************************************************************************************
angular.module('app')
.service('LoadingUtilsService', [
    'CtrlUtilsService'
    LoadingUtilsService
])
