"use strict"
module = angular.module("app.push", [ "app.authentication" ])
module.service("PushService", [ "$rootScope", "$q", "Authentication", "$log", "ADDRESS_WEBSOCKETS", "$timeout", ($rootScope, $q, Authentication, $log, ADDRESS_WEBSOCKETS, $timeout) ->

    MAX_RETRY_COUNT = 5
    RETRY_INTERVAL = 5 * 1000
    IDLE_RETRY_INTERVAL = 60 * 1000

    subscribes = []
    socket =
        sockJsClient: null
        stompClient: null
    connecting = false
    restarting = false
    retryCount = 0

    registerSubscribe = (topic) ->
        defer = $q.defer()
        subscribes.push
            topic: topic
            defer: defer


        # If connection already open, subscribe topic
        if socket.stompClient?
            subscribeStomp topic, defer

        defer.promise

    start = ->
        if Authentication.getUserInfo?
            startInternal()
        else
            $rootScope.$on('TokenVerified', () ->
                startInternal()
            )

    startInternal = ->
        if connecting
            return
        if socket.stompClient? or socket.sockJsClient?
            try
                socket.sockJsClient.onclose = null
                socket.stompClient.disconnect()

            socket.sockJsClient = null
            socket.stompClient = null

        sockJsClient = new SockJS(ADDRESS_WEBSOCKETS, {}, {})
        stompClient = Stomp.over(sockJsClient)

        accessToken = Authentication.getAccessToken()
        userId = if Authentication.getUserInfo()? then Authentication.getUserInfo().id else null
        # If not have user info, schedule to retry
        if not accessToken? or not userId?
            $log.debug("No user/token info")
            retryAfter(RETRY_INTERVAL)
            return

        connecting = true
        stompClient.connect(Authentication.getUserInfo().id, Authentication.getAccessToken(),
            ->
                retryCount = 0
                socket.sockJsClient = sockJsClient
                socket.stompClient = stompClient

                # Subscribe all topic
                i = 0
                while i < subscribes.length
                    subscribeStomp subscribes[i].topic, subscribes[i].defer
                    i++

                if restarting
                    restarting = false
                    $rootScope.$broadcast 'PushReconnected'
                else
                    $rootScope.$broadcast 'PushConnected'

                connecting = false
            , (error) ->
                connecting = false
                if retryCount < MAX_RETRY_COUNT
                    retryAfter(RETRY_INTERVAL)
                else
                    retryAfter(IDLE_RETRY_INTERVAL)

        )

    retryAfter = (interval) ->
        retryCount++
        $log.debug("Fail to connect, schedule to retry after " + interval + " milis")
        $timeout (->
            $log.debug("Trying to connect[time=" + retryCount + "]")
            startInternal()
        ), interval

    restart = ->
        restarting = true
        start()

    send = (destination, message) ->
        socket.stompClient.send destination, {}, angular.toJson(message)

    subscribeStomp = (topic, defer) ->
        socket.stompClient.subscribe topic, (data) ->
            defer.notify angular.fromJson(data.body)

    $rootScope.$on("TokenChanged", () ->
        if socket.stompClient? then
        $log.debug("Token changed, reconnect to websockets endpoint")
        restart()
    )

    return {
        registerSubscribe: registerSubscribe
        start: start
        restart: restart
        send: send
    }
])
